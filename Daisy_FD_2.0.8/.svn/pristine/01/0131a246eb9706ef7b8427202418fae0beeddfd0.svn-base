/**
 * Unit Tests for the CreateDiaryItemCommand
 * 
 * author - Brian Bason
 */
describe("AppendNoteEntryToNoteCommand - Test Suite", function() {

	describe("Successful cases for AppendNoteEntryToNoteCommand Suite", function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// mock manager
			var dummyManager = (function() {

				// counts calls to methods
				var calls = {
					appendNoteEntry : 0
				};

				var service = function() {
				};

				service.prototype.appendNoteEntryToNote = function(noteEntry, noteId, successCallback, errorCallback) {

					calls.appendNoteEntry++;
					noteEntry.set({'id' : calls.appendNoteEntry}, {'silent' : true});
					successCallback(noteEntry);

				};

				service.prototype.getCalls = function(functionName) {
					return calls[functionName];
				};

				return service;

			})();

			// configure the application context for the unit
			// test
			app = new Rocketboots.Application({

				modelClass : diary.model.DiaryModel,

				mapEvents : function() {
					// map events needed for the test
				},

				mapServices : function() {
					// map the services needed for the test
					this.mapService(ServiceName.DIARYITEMMANAGER, dummyManager);
				}

			});

			// init the application context
			app.initialize();

		});

		it("should be able to invoke the command", function() {

			var day = new Rocketboots.date.Day();

			day.addDays(-1);
			var noteEntryAttrs = {
				timestamp: day.getDate(),
				text: "CreateDiaryItemCommand Test Entry"
			};
			var noteEntry = new diary.model.NoteEntry(noteEntryAttrs);

			var noteAttrs = {
				id: 4,
				date: day.getDate()
			};
			var note = new diary.model.Note(noteAttrs);

			app.model.get(ModelName.DIARYITEMS).add(note);
			
			// execute the command
			var command = new diary.controller.AppendNoteEntryToNoteCommand();
			command.execute(noteEntry, 4);

			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("appendNoteEntry")).toEqual(1);

			// check that app model was modified
			expect(note.get('entries').length).toEqual(1);
			
			// get the note entry that was appended
			var appendedNoteEntry = note.get('entries').at(0);
			expect(appendedNoteEntry.get('id')).toEqual(1);
			expect(appendedNoteEntry.get('text')).toEqual("CreateDiaryItemCommand Test Entry");
			
		});

	});

});