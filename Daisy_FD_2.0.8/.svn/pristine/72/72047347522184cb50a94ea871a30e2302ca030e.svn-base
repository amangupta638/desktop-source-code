<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Content Panel.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="../../../Support/content-style.css" type="text/css"/>
</head>

<body class="panel">
	<!-- InstanceBeginEditable name="content" -->
	<div style="width:845px; clear:both;color:#575757;font-family: Arial;">
	<h1 style="padding-bottom:5px;font-size:16pt;"><strong>Memory Techniques</strong></h1>
    <img src="Images/memory-technique.jpg" alt="" width="188" height="133" style="float:left;padding-right:20px;padding-bottom:10px;">
    <p>Like most people, you probably think you’ve got a poor memory. You may just have some less-than-effective habits when it comes to taking in and processing information. Try using some of these techniques which will help you store, then easily retrieve information.</p>
    <p><strong>CONCENTRATE</strong> You can’t remember something if you don’t pay enough attention to it. Keep your mind on what you are doing.</p>
    <p><strong>LINK</strong> Link what you are learning to information you already know.</p>
    <p><strong>SUMMARISE</strong> Make summaries using your own words. This shows you understand. Summaries should be short and to the point, capturing key elements. Don’t just rely on words – use diagrams, mindmaps, drawings and flow charts.</p>
    <p><strong>UNDERSTAND</strong> Focus on understanding basic ideas rather than memorising isolated details. Explain it to someone else in your own words.</p>
    <p><strong>REVISE</strong> Revise what you have learned within 24 hours of learning it and keep revising it. This is far more effective than “cramming”.</p>
    <p><strong>MEMORY BY ASSOCIATION</strong> You are more likely to remember something out of the ordinary. Creating strange images in your head helps you remember. Be creative, clever, even outrageous in creating pictures in your mind. The richer the association, the better the memory.</p>
    <p><strong>MNEMONICS</strong> This is a technique for memorising lists of facts and keywords. An acronym makes a word using the first letter from each word that needs to be remembered. An acrostic is an invented sentence where the first letter of each word is a cue to an idea you need to remember. For example, the way to remember the order of the G-Clef notes on sheet music would be: Every Good Boy Deserves Fun (E.G.B.D.F.).</p>
    <p><strong>RHYME</strong> You are more likely to remember the words of a song or a poem rather than a paragraph from a book. That’s because rhyme often sticks in your mind. Do you remember the rhyme for the “ei” or “ie” rule? “I before E except after C or when sounded as A as in neighbour or weigh.”</p>
    <p><strong>CHUNKING INFORMATION</strong> Arranging a long list in smaller units or categories make it easier to remember. This works well for remembering long lists of numbers. The human mind can remember about 7 things at a time, so if you are trying to remember a bank account number it is easier for you to remember chunks of it rather than the whole string. For example, to remember the number, <strong>681 964 000 21466</strong> try the following:</p>
    <p><strong>“68”</strong> <em>(the year your dad was born)</em> <br>
	<strong>“1964”</strong> <em>(the year the Beatles came to Australia)</em> <br>
	<strong>“000”</strong> <em>(the emergency number)</em> <br>
	<strong>“214”</strong> <em>(your birthday – 21st April)</em> <br>
	<strong>“66”</strong> <em>(your best friend’s house number)</em></p>
    <p><strong>FLASH CARDS</strong> Write out what you want to learn on a 75mm X 127mm card. Carry it around with you everywhere. (Even the act of making the cards is helpful in learning the material because you are re-writing it). You can test yourself on certain sorts of information by putting half of the item on one side of the card, and then the rest on the other. For example, you can practise vocabulary in a foreign language, learn formulae or have an acronym one side with the answer on the other side. Once you have the flash cards, you have a resource that you can use to test your memory.</p>
    <p><strong>A final word on memory……….</strong><br>
	The fastest learning and the best recall happens when you are <strong>motivated</strong> to learn, and what you are learning <strong>makes sense</strong> to you. Consider……….<br>
	<ul style="margin-left:-20px; margin-top:0;">
    <li style="list-style-position:outside">how the knowledge you are learning can be usefully applied, and</li>
	<li style="list-style-position:outside">why it’s in your course, and</li>
	<li style="list-style-position:outside">how it fits into the overall pattern of the subject area</li>
	</ul>
    Only then it will be easier to learn and recall.</p>
    
    </div>

<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
