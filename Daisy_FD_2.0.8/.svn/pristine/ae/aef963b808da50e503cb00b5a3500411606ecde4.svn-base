/* CSS Document */

@theme-frame-color: #0055a5; // old colour
//@theme-frame-color: #773141; // Maroon
//@theme-frame-color: #000066; // Dark blue
//@theme-frame-color: #006500; // Dark green

@theme-link-color: #FFFFFF;
@theme-secondary-link-color: #CCCCCC; // was blue-ish #abc3da

body {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
}

a {
	text-decoration: none;
}

.frame {
	background-color: @theme-frame-color;
	position: fixed;
	top: 0px;
	left: 0px;
	right: 0px;
	bottom: 0px;
	font-family: "Diary Font";

	& > header {
		width: 100%;
		height:77px;
		background: url(images/title_bar.png) no-repeat;
		cursor: pointer;

		& > .logo {
			position:fixed;
			width: 224px;
			height:65px;
			display:block;
		/*//	background: url(images/school_logo.png) no-repeat;*/
			left: 13px;
			top: 12px;

			img {
				max-width: 224px;
				max-height: 65px;
			}
		}

		& > nav > ul {
			padding: 0;
			margin: 0;
			top: 29px;
			height: 48px;
			left: 285px;
			position:fixed;

			& > li {
				display: inline-block;
				list-style-type: none;

				& > a {
					display: inline-block;
					padding: 14px 18px 0px 18px;
					height: 45px;
					margin-top: 3px; // leave room for border
					color: @theme-link-color;
					font-size:13px;
					font-weight: bold;
					cursor:pointer;
					text-shadow: black -1px -1px 3px;

					background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(255,255,255, 0.15)), to(rgba(0,0,0, 0.4)));
					// white inner border top-right, black inner border bottom-left, top outer border
					-webkit-box-shadow:
						-1px 1px 1px rgba(255, 255, 255, 0.2) inset,
						1px -1px 1px rgba(0, 0, 0, 0.2) inset,
						0 -3px 0 0 rgba(255, 255, 255, 0.1);

					&:hover {
						background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(255,255,255, 0.4)), to(rgba(100, 100, 100, 0.4)));
					}

					&.selected {
						color: @theme-frame-color;
						text-shadow: none;
						cursor: default;
						background-image: none;
						background-color: @theme-link-color;
					}
				}

				&:first-of-type > a {
					margin-left: 3px; // leave room for border
					border-top-left-radius: 8px;
					// white inner border top-right, black inner border bottom-left, top/left outer border
					-webkit-box-shadow:
						-1px 1px 1px rgba(255, 255, 255, 0.2) inset,
						1px -1px 1px rgba(0, 0, 0, 0.2) inset,
						-2px -1px 0 2px rgba(255, 255, 255, 0.1);
				}

				&:last-of-type > a {
					margin-right: 3px; // leave room for border
					border-top-right-radius: 8px;
					// white inner border top-right, black inner border bottom-left, top/left outer border
					-webkit-box-shadow:
						-1px 1px 1px rgba(255, 255, 255, 0.2) inset,
						1px -1px 1px rgba(0, 0, 0, 0.2) inset,
						2px -1px 0 2px rgba(255, 255, 255, 0.1);
				}

			}

		}

		& > .search {
			z-index: 300;
			position: absolute;
			display: inline-block;
			left: 732px;
			width: 236px;
			height: 36px;
			top: 31px;

			& > input {
				position: absolute;
				left: 0;
				right: 0;
				top: 0;
				bottom: 0;
				margin: 3px;
				font-size: 14px;
				font-family: "Diary Font";
				vertical-align: middle;
				color: #888888;
				border-style: none;
				outline: none;
				padding-left: 32px;
				background: #FFFFFF url(images/search_magnifier.png) no-repeat 11px 6px;
				border-radius: 15px;

				// glow & inner glow
				-webkit-box-shadow:
					inset 4px 4px 4px #D5D5D5,
					0 0 1px 3px rgba(255, 255, 255, 0.3);

				&:focus {
					color: black;
					// glow & inner glow
					-webkit-box-shadow:
						inset 4px 4px 4px #D5D5D5,
						0 0 3px 3px rgba(255, 255, 255, 0.5);
				}
			}

			& >.loadingIndicator {
				position: absolute;
				left: 30px;
				top: 17px;
				display: none;
			}

			& .loading >input {
				background-image: none;
			}

			& .loading>.loadingIndicator {
				display: block;
			}

		}

		& > input::-webkit-input-placeholder {
			color: @theme-secondary-link-color;
		}

	}

	.container {
		position: fixed;
		top: 77px;
		bottom: 50px;
		left: 9px;
		right: 9px;
		background-color: #ffffff;
		border-top-right-radius: 6px;

		.content {
			position: fixed;
			top: 77px;
			left: 13px;
			right: 14px;
			bottom: 50px;
			overflow:auto;
		}
	}

	& > footer {
		position: fixed;
		padding: 0;
		margin: 0;
		bottom: 6px;
		height: 44px;
		left: 8px;
		right: 9px;
		position:fixed;
		background: -webkit-gradient(linear, left top, left bottom, from(rgba(255,255,255, 0.15)), to(rgba(0,0,0, 0.4)));
		-webkit-box-shadow: 0 1px 0 rgba(0, 0, 0, 0.5) inset;
		border-bottom-left-radius: 5px;
		border-bottom-right-radius: 5px;

		& > nav > ul {
			padding: 0;
			margin: 0;
			bottom: 6px;
			height: 44px;
			left: 9px;
			right: 9px;
			position:fixed;

			& > li {
				display: inline-block;
				list-style-type: none;
				text-transform:uppercase;

				a {
					display: inline-block;
					padding: 13px 16px 0px 16px;
					height: 31px;
					color: @theme-secondary-link-color;
					font-size:13px;
					cursor:pointer;
					font-family: "Diary Font";
					font-weight: bold;
					text-shadow: black -1px -1px 2px;
					// white border top-right, black border bottom-left
					-webkit-box-shadow:
						-1px 1px 1px rgba(255, 255, 255, 0.2) inset,
						1px -1px 1px rgba(0, 0, 0, 0.2) inset;

					&.selected, &:hover.selected {
						background-image: none;
						background-color: @theme-link-color;
						color: @theme-frame-color;
						text-shadow: none;
						cursor:default;
					}

					&:hover {
						background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(255,255,255, 0.4)), to(rgba(100, 100, 100, 0.4)));
						color: @theme-link-color; /*#cbe3fa; */
					}
				}

				&:first-of-type > a {
					border-bottom-left-radius: 5px;
				}
			}
		}

		& > .iconTab {
			float: right;
			margin: 0;
			height: 100%;
			width: 139px;
			height: 44px;
			background: url('images/footer_icon_tab.png') no-repeat;

			& > .icon {
				position: relative;
				margin: 0;
				background: url('images/footer_icon.png') no-repeat;
				width: 103px;
				height: 50px;
				margin-left: auto;
				margin-right: auto;
				top: -10px;
			}
		}

	}

}

#versionNumber {
	position: absolute;
	left: 16px;
	font-size: 10px;
	bottom: 5px;
	color: #aaa;
	z-index: 1;
}
