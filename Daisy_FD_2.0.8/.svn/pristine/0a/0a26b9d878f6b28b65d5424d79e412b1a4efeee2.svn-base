/**
 * The manager for Downloadable Content.  The idea of the manager is to abstract the application from
 * the data access storage methods.  All the business logic that is related to a Task needs to
 * be in here
 *
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DownloadableContentManager = (function() {

    /**
     * Constructs a Downloadable Content Manager.
     *
     * @constructor
     */
    var _manager = function() {
        this._contentPackageDelegate = app.locator.getService(ServiceName.CONTENTPACKAGEDELEGATE);
		this._downloadContentDelegate = app.locator.getService(ServiceName.DOWNLOADCONTENTDELEGATE);
    };

    /**
     * Gets the school configuration file for the registered token
     */
    _manager.prototype.getSchoolConfigurationFromRemoteLocation = function(successCallback, errorCallback) {
        this._downloadContentDelegate.downloadAndInstallSchoolConfiguration(successCallback, errorCallback);
    }

	/**
	 * Gets the content package file for the registered token
	 */
	_manager.prototype.getContentPackageFromRemoteLocation = function(packageName, successCallback, errorCallback) {
		this._downloadContentDelegate.downloadAndInstallContentPackage(packageName, successCallback, errorCallback);
	}

	/**
	 * Deletes the content directory
	 */
	_manager.prototype.deleteAllDirectories = function() {
		this._downloadContentDelegate.deleteAllDirectories();
	}

	/**
	 * Gets the locally stored content packages
	 */
	_manager.prototype.getStoredContentPackages = function(successCallback, errorCallback) {
		this._contentPackageDelegate.getContentPackages(successCallback, errorCallback);
	};


	/**
	 * Deletes a locally stored package
	 */
	_manager.prototype.deleteStoredContentPackages = function(successCallback, errorCallback, contentPackages) {
		this._contentPackageDelegate.deleteContentPackages(successCallback, errorCallback, contentPackages);
	};

	/**
	 * Adds a content package to the local store
	 */
	_manager.prototype.addStoredContentPackage = function(contentPackage, successCallback, errorCallback) {
		this._contentPackageDelegate.addContentPackage(contentPackage,
				// success
				function(contentPackageId){
					contentPackage.set('id', contentPackageId);
					successCallback(contentPackage);
				},

				// error
				errorCallback
		);
	};



	/**
     * Gets all the contents from the local archive file
     *
     * @param {!string} archiveLocation The location of the zip file that contains the metadata and packages of the content for approval
     * @param {!function()} successCallback The function that is to be called if the call was successful
     * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
     */
    _manager.prototype.getContentFromArchive = function(archiveLocation, successCallback, errorCallback) {

        // check that all the required details were given
        if (!archiveLocation) {
            throw new Error("Archive file location must be specified, archiveLocation is not optional");
        }

        if (!successCallback) {
            throw new Error("Success callback must be specified, callback is not optional");
        }

		var archive = new air.File();
		archive.nativePath = archiveLocation;

		if (archive.exists) {
			// then we need to get the content file and the table of contents file
			this._downloadContentDelegate.downloadContent(archive.url, "../sandbox",

					// success
					function() {
						var sandboxFolder,
								manifestFile;

						try {
							sandboxFolder = air.File.applicationStorageDirectory.resolvePath("sandbox");
							if (!sandboxFolder.exists) {
								throw new Error("Sandbox folder was not found in application folder");
							}

							// now we need to read the manifest file, which is the file that has all
							// the content that is to be downloaded
							manifestFile = sandboxFolder.resolvePath("manifest.ddm");

							// if it is not found we cannot do anything about it but fail the process
							if (!manifestFile.exists) {
								throw new Error("File manifest file was not found in application sandbox folder");
							}

							successCallback();

						} catch (error) {
							// log the error
							//console.log("Error while trying to read articles manifest file from local storage");
							//console.log(error);
							errorCallback && errorCallback("Error while trying to read articles manifest file");
						}
					},

					// error
					errorCallback);
		} else {
			errorCallback && errorCallback("Archive file " + archiveLocation + " does not exist");
		}
    };

    // return the generated manager
    return _manager;

})();