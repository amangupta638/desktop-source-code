/**
 * Unit Tests for the Registration Manager
 * 
 * author - Jane Sivieng
 */
describe("Registration Manager - Test Suite", function() {
	
	describe("Successful cases for Registration Manager Suite" , function() {
		
		var dummyCloudServiceManager = (function() {
			
			var service = function() {};
								
			return service;
			
		})();
		

		var dummySystemConfigManager = (function() {
			
			var service = function() {};
								
			return service;
			
		})();
		
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.THECLOUDSERVICEMANAGER, dummyCloudServiceManager);
	        	   this.mapService(ServiceName.SYSTEMCONFIGURATIONMANAGER, dummySystemConfigManager);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			app.model = {};
			app.model.get = function(modelname) {
				return dummySystemConfigRegistered;
			};
						
			// init the application context
			app.initialize();
			
		});
		
		describe("Cases where the application is running in production mode", function() {
			var dummyAppState = {
				get : function(attrName) {
					return diary.model.ApplicationState.MODE.PRODUCTION;
				}
			};

			describe("Cases where the application has been registered with a token", function() {

				var dummySystemConfigRegistered = {
					'get' : function(attrName) {
						if (attrName == "token") {
							return "TOKEN";
						}
						return null;
					}
				};
						
				// setup the env for the the unit test
				beforeEach(function() {
									
					app.model = {};
					app.model.get = function(modelname) {
						if (modelname == ModelName.APPLICATIONSTATE) {
							return dummyAppState;
						}
						return dummySystemConfigRegistered;
					};
												
				});

				it("should be able to determine if an app is registered with a token", function() {
					   
					// holds the registration manager
					var manager = new diary.service.RegistrationManager();
					
					expect(manager.applicationIsRegistered()).toEqual(true);
								
				});
			});
			
			describe("Cases where the application has NOT been registered with a token", function() {
				var dummySystemConfigUnregistered = {
					'get' : function(attrName) {
						return null;
					}
				};
					
				// setup the env for the the unit test
				beforeEach(function() {
									
					app.model = {};
					app.model.get = function(modelname) {
						if (modelname == ModelName.APPLICATIONSTATE) {
							return dummyAppState;
						}
						return dummySystemConfigUnregistered;
					};
												
				});

				it("should be able to determine if an app is not registered with a token", function() {
					   
					// holds the registration manager
					var manager = new diary.service.RegistrationManager();
					
					expect(manager.applicationIsRegistered()).toEqual(false);
								
				});
			});
			
		});
		
		describe("Cases where the application is NOT running in production mode", function() {

			var dummyAppState = {
				get : function(attrName) {
					return diary.model.ApplicationState.MODE.DEMO;
				}
			};
			
			// setup the env for the the unit test
			beforeEach(function() {
								
				app.model = {};
				app.model.get = function(modelname) {
					return dummyAppState;
				};
											
			});

			it("should be able to start an application as registered if it's not in production mode", function() {
				   
				// holds the registration manager
				var manager = new diary.service.RegistrationManager();
				
				expect(manager.applicationIsRegistered()).toEqual(true);
							
			});

		});
				
	});

		
});