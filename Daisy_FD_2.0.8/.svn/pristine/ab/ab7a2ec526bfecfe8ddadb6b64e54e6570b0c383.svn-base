/**
 * $Id: View.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.view) === "undefined") {
	Rocketboots.view = {};
}


Rocketboots.view.View = (function() {

	var view = Backbone.View.extend({
		
		// Defaults
		isVisible: true,
		childViews: null,
		lastVisibleChildView: null,
		isDirty: false,
		isAlwaysDirty: false,
		
		// Methods
		
		initialize: function () {
			
			this.childViews = {};
			
		},
		
		setViewVisible: function(visible) {
			
			// Update visibility of self
			this.isVisible = visible;
			
			// Update visibility of child views
			for(var childViewName in this.childViews) {
				var childView = this.childViews[childViewName];
		
				if (typeof(childView.setViewVisible) != "undefined") {
				
					if (visible) {
						if (childViewName == this.lastVisibleChildView) {
							childView.setViewVisible(true);				
						}
					}
					else {
						childView.setViewVisible(false);
					}
					
				}
	       			
			}
			
		},
		
		setChildViewVisibleByName: function(childViewName, visible) {
		
			var childView = this.childViews[childViewName];

			if (typeof(childView) === "undefined") return;
			
			if (typeof(childView.setViewVisible) != "undefined") {
				childView.setViewVisible(visible);
			}
		},
		
		hideAllViewsExcept: function(childViewName) {
		
			for(var existingCachedChildView in this.childViews) {
				var view = this.childViews[existingCachedChildView];
				
				if (typeof(view.setViewVisible) != "undefined") {
					view.setViewVisible(existingCachedChildView == childViewName);
				}				
			}
			
		},
		
		
		
		/* OVERRIDES */
		
		delegateEvents: function () {
			Backbone.View.prototype.delegateEvents.call(this);
			_.forEach(this.childViews, function (childView) {
				if (childView.isVisible && childView.delegateEvents) {
					childView.delegateEvents();
				}
			});
		},
		
		/*unbind the views when removed from DOM and memory*/		
		undelegateEvents: function () {
			Backbone.View.prototype.undelegateEvents.call(this);
			_.forEach(this.childViews, function (childView) {
				if (childView.isVisible && childView.undelegateEvents) {					
					childView.undelegateEvents();
				}
			});
		},
		
		/*Performance fix: Remove view from the DOM*/		
		remove: function () {
			console.log("######### VIEW REMOVE METHOD...");
			Backbone.View.prototype.undelegateEvents.call(this);
			Backbone.View.prototype.remove.call(this);
			this.$el.remove();
			 
		}
		
		
		
	});
	
	return view;

})();