/**
 * Unit Tests for the School Profile Delegate
 * 
 * author - Brian Bason
 */
describe("School Profile SQL Delegate - Test Suite", function() {
	
	// holds the dummy app database layer that is required for testing
	var dummyApplicationDatabase = (function() {
		
		var service = function() {};
		
		service.prototype.runQuery = function(text, parameters, successCallback, errorCallback) {
			
			// return the dummy SQL Statement
			successCallback(new air.SQLStatement());
			
		};
		
		return service;
		
	})();
	
	// setup the env for the the unit test
	beforeEach(function() {
		
		// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
		air = new Object();

		air.SQLStatement = function() {
		};
		
		air.SQLStatement.prototype.execute = function() {
		};
		
		// configure the application context for the unit test
		app = new Rocketboots.Application({
	           
           mapEvents: function() {
               // map events needed for the test
           },
           
           mapServices: function() {
        	   // map the services needed for the test
        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
           },
           
           registerTemplates: {
               // register templates, if needed
           }
	           
	    });
		
		// init the application context
		app.initialize();
		
	});
	
	describe("Get a school profile record" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : [{ 'id' : 1,
									'name' : "Test School",
									'schoolLogo' : "file://var/lib/images/logo.gif",
									'schoolImage' : "file://var/lib/images/image.gif",
									'schoolMotto' : "The school motto",
									'missionStatement' : "The school mission statement",
									'diaryTitle' : "The Diary 2012",
									'includeEmpower' : true }] };
									
			};
			
		});
	
		it("should be able to return the school profile", function() {
		   
			// holds the school profile delegate that is being tested
			var delegate = new diary.service.SchoolProfileSQLDelegate();
			
			// get the school profile from the delegate
			delegate.getSchoolProfile(success, error);
			
			function success(schoolProfile) {
			
				// the delegate should have returned a school profile
				expect(schoolProfile).not.toBeNull();
				expect(schoolProfile instanceof diary.model.SchoolProfile).toBeTruthy();
				
				// the profile that was returned should be the same as the dummy data			
				expect(schoolProfile.id).toEqual(1);
				expect(schoolProfile.get("name")).toEqual("Test School");					
				expect(schoolProfile.get("schoolLogo")).toEqual("file://var/lib/images/logo.gif");					
				expect(schoolProfile.get("schoolImage")).toEqual("file://var/lib/images/image.gif");
				expect(schoolProfile.get("schoolMotto")).toEqual("The school motto");					
				expect(schoolProfile.get("missionStatement")).toEqual("The school mission statement");					
				expect(schoolProfile.get("diaryTitle")).toEqual("The Diary 2012");
				expect(schoolProfile.get("includeEmpower")).toBeTruthy();
				expect(schoolProfile.get("contentMapping") instanceof diary.model.ContentMapping).toBeTruthy();
				
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
		});
		
	});
	
	describe("Get no record of school profile" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : null };
				
			};
			
		});
	
		it("should be able to return a null for an nonexistent profile", function() {
		   
			// holds the school profile delegate that is being tested
			var delegate = new diary.service.SchoolProfileSQLDelegate();
			
			// the success function
			function success(schoolProfile) {
				// the delegate should have returned a NULL since no school profile
				// exists
				expect(schoolProfile).toBeNull();
			}
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// get the school profile from the delegate
			delegate.getSchoolProfile(success, error);
			
		});
		
	});
	
	describe("Failure while trying to get school profile" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummyApplicationDatabase = (function() {
			
			var service = function() {};
			
			service.prototype.runQuery = function(text, parameters, successCallback, errorCallback) {
				
				// simulate an error
				errorCallback();
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
			air = new Object();

			air.SQLStatement = function() {
			};
			
			air.SQLStatement.prototype.execute = function() {
			};
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});

	});
		
});