@echo off
set PAUSE_ERRORS=1
call bat\SetupSDK.bat
call bat\SetupApplication.bat
 
echo. Generating .exe directory
 
set AIR_TARGET=
set OPTIONS=
call adt -package -target bundle 2015_Planner air\DaisyAppExeMigrated1.air
 
pause