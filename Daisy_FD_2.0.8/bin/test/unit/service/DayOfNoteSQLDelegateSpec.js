/**
 * Unit Tests for the DayOfNote SQL Delegate
 * 
 * author - Justin Judd
 */
describe("DayOfNote SQL Delegate - Test Suite", function() {

	describe("Success Cases", function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// mock database service that executes the mock statement
			var mockService = (function() {

				var service = function() {
				};

				service.prototype.runQuery = function(text, parameters, success, error) {
					success(new mockStatement());
				};

				return service;

			})();

			// mock statement that returns a test data set
			var mockStatement = (function() {

				var statement = function() {
				};

				statement.prototype.getResult = function() {
					return {
						'data' : [ {
							'id' : 1,
							'date' : new Rocketboots.date.Day(2012, 0, 1).getKey(),
							'overrideCycleDay' : null,
							'includeInCycle' : false,
							'isGreyedOut' : true,
							'title' : 'New Years Day'
						}, {
							'id' : 2,
							'date' : new Rocketboots.date.Day(2012, 0, 26).getKey(),
							'overrideCycleDay' : null,
							'includeInCycle' : false,
							'isGreyedOut' : true,
							'title' : 'Australia Day'
						} ]
					};
				};

				return statement;

			})();

			// configure the application context for the unit test
			app = new Rocketboots.Application({
				mapEvents : function() {
				},

				mapServices : function() {
					// map the services needed for the test
					this.mapService(ServiceName.APPDATABASE, mockService);
				},

				registerTemplates : {}
			});

			// init the application context
			app.initialize();

		});

		it("should be able to return all DayOfNote records", function() {

			var delegate = new diary.service.DayOfNoteSQLDelegate();

			function success(daysOfNote) {
				var day;

				// the delegate should have returned a DayOfNoteCollection
				expect(daysOfNote).not.toBeNull();
				expect(daysOfNote instanceof
						diary.collection.DayOfNoteCollection).toBeTruthy();

				// the collection that was returned should contain the dummy
				// data
				expect(daysOfNote.length).toEqual(2);

				// check day 1
				day = daysOfNote.at(0);
				expect(day.get("date").getKey()).toEqual(new Rocketboots.date.Day(2012, 0, 1).getKey());
				expect(day.get("overrideCycleDay")).toBeNull();
				expect(day.get("includeInCycle")).toBeFalsy();
				expect(day.get("isGreyedOut")).toBeTruthy();
				expect(day.get("title")).toEqual("New Years Day");

				// check day 2
				day = daysOfNote.at(1);
				expect(day.get("date").getKey()).toEqual(new Rocketboots.date.Day(2012, 0, 26).getKey());
				expect(day.get("overrideCycleDay")).toBeNull();
				expect(day.get("includeInCycle")).toBeFalsy();
				expect(day.get("isGreyedOut")).toBeTruthy();
				expect(day.get("title")).toEqual("Australia Day");
			}

			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// method to test
			delegate.getAllDaysOfNote(success, error);

		});

	});

	describe("Failure Cases", function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// mock database service that executes the mock statement
			var mockService = (function() {

				var service = function() {
				};

				service.prototype.runQuery = function(text, parameters, success, error, errorMessage) {
					error(errorMessage);
				};

				return service;

			})();

			// configure the application context for the unit test
			app = new Rocketboots.Application({
				mapEvents : function() {
				},

				mapServices : function() {
					// map the services needed for the test
					this.mapService(ServiceName.APPDATABASE, mockService);
				},

				registerTemplates : {}
			});

			// init the application context
			app.initialize();

		});

		it("should be able to generate error for failure", function() {

			var delegate = new diary.service.DayOfNoteSQLDelegate();

			// the success function
			function success(daysOfNote) {
				throw "Test failed - operation should not have been successful";
			}

			// the error function
			function error(message) {
				expect(message).toEqual("Error while trying to read DayOfNote records");
			}

			// method to test
			delegate.getAllDaysOfNote(success, error);

		});

	});

});
