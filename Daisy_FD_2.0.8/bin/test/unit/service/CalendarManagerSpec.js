/**
 * Unit Tests for the Calendar Manager
 * 
 * author - Brian Bason
 */
describe("Calendar Manager - Test Suite", function() {
	
	describe("Creation of Calendar with no Days of Note Suite" , function() {
		
		// holds the dummy day of note manager
		var dummyDayOfNoteManager = (function() {
			
			return function() {
				
				this.getAllDaysOfNote = function(successCallback, errorCallback) {
					
					// we will create an empty day of note simulating that no special days exist
					var dayOfNotes = new diary.collection.DayOfNoteCollection();
					
					// return all the day of notes
					successCallback(dayOfNotes);
					
				};
				
			};
				
		})();
		
		// holds the dummy time table manager
		var dummyTimetableManager = (function() {
			
			return function() {
				
				this.getTimetables = function(startDay, endDay, subjects, successCallback, errorCallback) {
										
					// create a timetable collection
					var timeTables = new diary.collection.TimetableCollection();
					
					// fill in a timetable starting from 1st Feb of the year
					timeTables.add(new diary.model.Timetable({ "startDay" : new Rocketboots.date.Day(startDay.getFullYear(), 1, 1),
														       "cycleLength" : 20 }));
					
					// return the timetable
					successCallback(timeTables);
					
				};
				
			};
				
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
				
		       modelClass:	diary.model.DiaryModel,
		       
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.DAYOFNOTEMANAGER, dummyDayOfNoteManager);
	        	   this.mapService(ServiceName.TIMETABLEMANAGER, dummyTimetableManager);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
			var year = 2012,
				startDate = new Rocketboots.date.Day(year, 0, 1),
				endDate = new Rocketboots.date.Day(year, 11, 31),
				appState = app.model.get("applicationState");
			
			appState.set("year", 2012);
			appState.set("startDate", startDate);
			appState.set("endDate", endDate);
			
		});
		
		it("should be able to return a calendar for the year when no Days of Note exist", function() {
		   
			// current year
			var currentYear = 2012;
			
			// holds the calendar manager
			var manager = new diary.service.CalendarManager();
			
			// the success function
			function success(calendar) {
				
				var cycle = 1;
				var cycleDay = 1;
				
				// the manager should have returned a calendar
				expect(calendar instanceof diary.model.Calendar).toBeTruthy();
				
				// check that 366 days were return
				expect(calendar.get('calendarDays').length).toEqual(366);
				
				// go through all the created Calendar days for the calendar and check
				// that each one of them is correct
				_.forEach(calendar.get('calendarDays').toArray(), function(day) {
					
					// check if it is the weekend
					var isWeekend = day.get('day').getDay() == 6 || day.get('day').getDay() == 0;
					
					if (isWeekend || day.get('day').getMonth() == 0) {
						// the day should not be part of the cycle since it is Jan
						expect(day.get('cycle')).toBeNull();
						expect(day.get('cycleDay')).toBeNull();
						expect(day.get('title')).toBeNull();
						expect(day.get('isGreyedOut')).toBeTruthy();
						expect(day.get('timetable')).toBeNull();
					} else {
						// the day should be part of the cycle
						expect("Cycle" + day.get("cycle")).toEqual("Cycle" + cycle);									
						expect(day.get("cycleDay")).toEqual(cycleDay++);
						expect(day.get('title')).toBeNull();
						expect(day.get('isGreyedOut')).toBeFalsy();
						expect(day.get('timetable') instanceof diary.model.Timetable).toBeTruthy();
						
						if (cycleDay > 20) {
							cycle++;
							cycleDay = 1;
						}
					}
					
				});
				
			}
			
			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// try to get the calendar
			manager.getCalendar(currentYear, new diary.collection.SubjectCollection(), success, error);
			
		});
		
	});
	
	describe("Creation of Calendar with same Days of Note Suite" , function() {
		
		// holds the dummy day of note manager
		var dummyDayOfNoteManager = (function() {
			
			return function() {
				
				this.getAllDaysOfNote = function(successCallback, errorCallback) {
					
					// we will create an empty day of note simulating that no special days exist
					var dayOfNotes = new diary.collection.DayOfNoteCollection();
					
					// create a day of note with override cycle
					// 7th Feb 2012 (TUE)
					dayOfNotes.add(new diary.model.DayOfNote({ 'date' : new Rocketboots.date.Day(2012, 1, 7),
															  'overrideCycleDay' : 7 }));
					
					// create a day of note with include in cycle
					// 3rd Mar 2012 (SAT)
					dayOfNotes.add(new diary.model.DayOfNote({ 'date' : new Rocketboots.date.Day(2012, 2, 3),
															  'includeInCycle' : true,
															  'isGreyedOut' : false }));
					
					// create a day of note with is greyed out
					// 9th Mar 2012 (FRI)
					dayOfNotes.add(new diary.model.DayOfNote({ 'date' : new Rocketboots.date.Day(2012, 2, 9),
															  'includeInCycle' : false,
															  'isGreyedOut' : true }));

					// create a day of note with a title
					// 16th Mar 2012 (FRI)
					dayOfNotes.add(new diary.model.DayOfNote({ 'date' : new Rocketboots.date.Day(2012, 2, 16),
															  'includeInCycle' : false,
															  'isGreyedOut' : true,
															  'title' : "Parents Day" }));

					// create a day of note with a resetcycle flag set
					// 23/04/2012 (MON)
					dayOfNotes.add(new diary.model.DayOfNote({ 'date' : new Rocketboots.date.Day(2012, 3, 23),
																'title' : "Start Term 2",
																'overrideCycle' : 1 }));


					// create a day of note with a resetcycle flag set and an override cycle day set
					// 17th July 2012 (TUE)
					dayOfNotes.add(new diary.model.DayOfNote({ 'date' : new Rocketboots.date.Day(2012, 6, 17),
																'title' : "Start Term 3",
																'overrideCycleDay' : 2,
																'overrideCycle' : 1 }));


					// return all the day of notes
					successCallback(dayOfNotes);
					
				};
				
			};
				
		})();
		
		// holds the dummy time table manager
		var dummyTimetableManager = (function() {
			
			return function() {
				
				this.getTimetables = function(startDay, endDay, subjects, successCallback, errorCallback) {
										
					// create a timetable collection
					var timeTables = new diary.collection.TimetableCollection();
					
					// fill in a timetable starting from 1st Feb of the year
					timeTables.add(new diary.model.Timetable({ "startDay" : new Rocketboots.date.Day(startDay.getFullYear(), 1, 1),
														       "cycleLength" : 20 }));
					
					// return the timetable
					successCallback(timeTables);
					
				};
				
			};
				
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
				
			   modelClass:	diary.model.DiaryModel,
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.DAYOFNOTEMANAGER, dummyDayOfNoteManager);
	        	   this.mapService(ServiceName.TIMETABLEMANAGER, dummyTimetableManager);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
			var year = 2012,
				startDate = new Rocketboots.date.Day(year, 0, 1),
				endDate = new Rocketboots.date.Day(year, 11, 31),
				appState = app.model.get("applicationState");
			
			appState.set("year", 2012);
			appState.set("startDate", startDate);
			appState.set("endDate", endDate);
			
		});
		
		it("should be able to return a calendar for the year with some Day of Notes", function() {
		   
			// holds the calendar manager
			var manager = new diary.service.CalendarManager();
			
			// the success function
			function success(calendar) {
				
				var cycle = 1;
				var cycleDay = 1;

				var timetableStartDay = new Rocketboots.date.Day(2012, 1, 1);

				// six days to check
				var overrideCycleDate = new Date(2012, 1, 7, 0, 0, 0);
				var includeInCycleDate = new Date(2012, 2, 3, 0, 0, 0);
				var greyedOutDate = new Date(2012, 2, 9, 0, 0, 0);
				var titleDate = new Date(2012, 2, 16, 0, 0, 0);
				var term2Date = new Date(2012, 3, 23, 0, 0, 0);
				var term3Date = new Date(2012, 6, 17, 0, 0, 0);

				// the manager should have returned a calendar
				expect(calendar instanceof diary.model.Calendar).toBeTruthy();
				
				// go through all the created Calendar days for the calendar and check
				// that each one of them is correct
				_.forEach(calendar.get('calendarDays').toArray(), function(day) {
					
					// determine if it is a weekend
					var isWeekend = (day.get('day').getDay() == 6 || day.get('day').getDay() == 0);
					
					// we need to check specific days one by one (four in all)
					if (day.get('day').getTime() == overrideCycleDate.getTime()) {
						cycleDay = 7;
					}
					
					if (day.get('day').getTime() == includeInCycleDate.getTime()) {
						// the day should be included in the cycle
						expect("Cycle" + day.get("cycle")).toEqual("Cycle" + cycle);									
						expect(day.get('cycleDay')).toEqual(cycleDay);
						expect(day.get('title')).toBeNull();
						expect(day.get('isGreyedOut')).toBeFalsy();
						expect(day.get('timetable') instanceof diary.model.Timetable).toBeTruthy();
						
					} else if (day.get('day').getTime() == greyedOutDate.getTime()) {
						// the day is a greyed out day
						expect(day.get('cycle')).toBeNull();
						expect(day.get('cycleDay')).toBeNull();
						expect(day.get('title')).toBeNull();
						expect(day.get('isGreyedOut')).toBeTruthy();
						expect(day.get('timetable')).toBeNull();
						
					} else if (day.get('day').getTime() == titleDate.getTime()) {
						// the day is a title day
						expect(day.get('cycle')).toBeNull();
						expect(day.get('cycleDay')).toBeNull();
						expect(day.get('title')).toEqual("Parents Day");
						expect(day.get('isGreyedOut')).toBeTruthy();
						expect(day.get('timetable')).toBeNull();

					} else if (day.get('day').getTime() == term2Date.getTime()) {
						cycle = 1;
						cycleDay = 1;
						// the day is a cycle day
						expect(day.get('cycle')).toEqual(cycle);
						expect(day.get('cycleDay')).toEqual(cycleDay);
						expect(day.get('title')).toEqual("Start Term 2");
						expect(day.get('isGreyedOut')).toBeFalsy();
						expect(day.get('timetable') instanceof diary.model.Timetable).toBeTruthy();

					} else if (day.get('day').getTime() == term3Date.getTime()) {
						cycle = 1;
						cycleDay = 2;
						// the day is a cycle day
						expect(day.get('cycle')).toEqual(cycle);
						expect(day.get('cycleDay')).toEqual(cycleDay);
						expect(day.get('title')).toEqual("Start Term 3");
						expect(day.get('isGreyedOut')).toBeFalsy();
						expect(day.get('timetable') instanceof diary.model.Timetable).toBeTruthy();

					} else if (isWeekend || day.get('day').getMonth() == 0) {
						// the day should not be part of the cycle since it is Jan or the weekend
						expect(day.get('cycle')).toBeNull();
						expect(day.get('cycleDay')).toBeNull();
						expect(day.get('title')).toBeNull();
						expect(day.get('isGreyedOut')).toBeTruthy();
						expect(day.get('timetable')).toBeNull();
					} else {
						// the day should be part of the cycle
						expect(day.get("cycle")).toEqual(cycle);									
						expect(day.get('title')).toBeNull();
						expect(day.get('isGreyedOut')).toBeFalsy();
						expect(day.get('timetable') instanceof diary.model.Timetable).toBeTruthy();
						
					}

					if (!isWeekend && day.get('day').getKey() >= timetableStartDay.getKey()) {
						++cycleDay;
					}
					
					if (cycleDay > 20) {
						cycle++;
						cycleDay = 1;
					}
					
				});
				
			}
			
			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// try to get the calendar
			manager.getCalendar(2012, new diary.collection.SubjectCollection(), success, error);
			
		});
		
	});
		
});