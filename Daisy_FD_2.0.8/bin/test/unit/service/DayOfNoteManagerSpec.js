/**
 * Unit Tests for the Day Of Note Manager
 * 
 * author - John Pearce
 */
describe("Day of Note Manager - Test Suite", function() {
	
	describe("Successful cases for Day of Note Manager Suite" , function() {
		
		// holds the delegate that is being used before testing
		var dummyDayOfNoteDelegate = (function() {
			
			return function() {
				
				this.getAllDaysOfNote = function(successCallback, errorCallback) {
					
					var daysOfNote = new diary.collection.DayOfNoteCollection();
					
					daysOfNote.add(new diary.model.DayOfNote({
						'date' : new Rocketboots.date.Day()
					}));
					
					daysOfNote.add(new diary.model.DayOfNote({
						'date' : new Rocketboots.date.Day()
					}));
					
					// return days of note
					successCallback(daysOfNote);
					
				};
				
			};
				
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		       
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.DAYOFNOTEDELEGATE, dummyDayOfNoteDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
	           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should be able to return days of note", function() {
		   
			// holds the task manager
			var manager = new diary.service.DayOfNoteManager();
			
			// the success function
			function success(daysOfNote) {
				// the manager should have returned a collection of days of note
				expect(daysOfNote instanceof diary.collection.DayOfNoteCollection).toBeTruthy();
				expect(daysOfNote.size()).toEqual(2);
			}
			
			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// try to get the results
			manager.getAllDaysOfNote(success, error);
			
		});
		
	});
	
});