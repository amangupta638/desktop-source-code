/**
 * Unit Tests for the Schema Manager
 * 
 * author - Brian Bason
 */
describe("Diary Schema Manager - Test Suite", function() {
	
	beforeEach(function() {
		// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
		air = new Object();

		air.SQLStatement = function() {
		};
		
		air.SQLStatement.prototype.execute = function() {
			
		};
		
	});
	
	describe("Install all schema updates" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummyApplicationDatabase = (function() {
			
			var _service = function() {};
			
			_service.prototype.runScripts = function(scripts, sqlConnection, successCallback, errorCallback) {
				
			};
			
			// overwrite the run query function to throw an error simulating
			// no schema in the DB
			_service.prototype.runQuery = function(text, parameters, sqlConnection, successCallback, errorCallback) {
				// throw an exception to simulate no schema
				throw new Error("Schema doesn't exist");
			};
			
			
			return _service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
	
		it("should be able to install all schema updates", function() {
		   
			// holds the manager for the schema updates
			var manager = new diary.service.DiarySchemaManager();
			
			// the test function
			var testFunction = function() {
				// execute the schema updates
				manager.installSchemaUpdates();
			};
			
			// evaluate the result of the schema update process
			expect(testFunction).not.toThrow();
			
		});
		
	});
	
	describe("Install from particular schema" , function() {

		var simulatedSchemaVersion = 0;
		
		// holds the dummy app database layer that is required for testing
		var dummyApplicationDatabase = (function() {
			
			var _service = function() {};
			
			_service.prototype.runScripts = function(scripts, sqlConnection, successCallback, errorCallback) {
				
			};
			
			// overwrite the run query function to return back a schema version
			_service.prototype.runQuery = function(text, parameters, sqlConnection, successCallback, errorCallback) {
				return {'getResult': function () {
					return {'data' : [{'schemaVersion' : simulatedSchemaVersion}]}
				}};
			};
			
			
			return _service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});

		it("should be able to install from unversioned database", function() {

			// holds the manager for the schema updates
			var manager = new diary.service.DiarySchemaManager();

			simulatedSchemaVersion = null;

			// the test function
			var testFunction = function() {
				// execute the schema updates
				manager.installSchemaUpdates();
			};

			// evaluate the result of the schema update process
			expect(testFunction).not.toThrow();
			expect(manager.isSchemaVersionSupported()).toBeTruthy();

		});

		it("should be not able to install from unsupported schema version 1", function() {

			// holds the manager for the schema updates
			var manager = new diary.service.DiarySchemaManager();

			simulatedSchemaVersion = 1;

			// the test function
			var testFunction = function() {
				// execute the schema updates
				manager.installSchemaUpdates();
			};

			// evaluate the result of the schema update process
			expect(testFunction).toThrow();
			expect(manager.isSchemaVersionSupported()).toBeFalsy();

		});
	
		it("should be able to install from schema version 3", function() {
		   
			// holds the manager for the schema updates
			var manager = new diary.service.DiarySchemaManager();

			simulatedSchemaVersion = 3;
			
			// the test function
			var testFunction = function() {
				// execute the schema updates
				manager.installSchemaUpdates();
			};
			
			// evaluate the result of the schema update process
			expect(testFunction).not.toThrow();
			expect(manager.isSchemaVersionSupported()).toBeTruthy();

		});
		
	});
		
});