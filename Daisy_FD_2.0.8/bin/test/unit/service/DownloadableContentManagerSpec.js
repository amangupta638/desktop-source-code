/**
 * Unit Tests for the Downloadable Content Manager
 * 
 * author - Jane Sivieng
 */
describe("Downloadable Content Manager - Test Suite", function() {
	// holds the dummy download content delegate that is required for testing
	var dummyDownloadContentDelegate = (function() {
		
		var service = function() {};
		
		service.prototype.downloadContent = function(remotePath, localPath, successCallback, errorCallback) {
			
			// call the success callback
			successCallback();				
		};
		
		return service;
		
	})();

    var dummyManifestManager = (function() {

        var manager = function() {};

        return manager;

    })();

    var dummyContentPackageDelegate = (function() {

        var service = function() {};

        return service;

    })();

    var dummyPath = function(pathName) {
		
		var path = {};
		
		path.exists = true;
		
		path.resolvePath = function(pathName) {
			return dummyPath(pathName);
		};
		
		return path;
		
	};

	beforeEach(function() {
		errorCallback = function(error) {
			console.log(error);
		};
		
		// Mock out the air.File, air.FileStream classes
		air = new Object();
		
		air.File = {};		
		air.File.applicationStorageDirectory = dummyPath();
						
		air.FileStream = function() {};
		air.FileStream.prototype.open = function(file, mode) {};
		air.FileStream.prototype.close = function() {};
		air.FileStream.prototype.readUTFBytes = function(fileBytes) {
			return fileBytes;
		};									
		
		// configure the application context for the unit test
		app = new Rocketboots.Application({
	           
           mapEvents: function() {
               // map events needed for the test
           },
           
           mapServices: function() {
        	   // map the services needed for the test
        	   this.mapService(ServiceName.DOWNLOADCONTENTDELEGATE, dummyDownloadContentDelegate);
        	   this.mapService(ServiceName.MANIFESTMANAGER, dummyManifestManager);
               this.mapService(ServiceName.CONTENTPACKAGEDELEGATE, dummyContentPackageDelegate);
           },
           
           registerTemplates: {
               // register templates, if needed
           }
	           
	    });
		
		// init the application context
		app.initialize();
		
		manager = new diary.service.DownloadableContentManager();			
	});


			
});