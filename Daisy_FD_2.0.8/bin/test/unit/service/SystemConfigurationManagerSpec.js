/**
 * Unit Tests for the System Configuration Manager
 * 
 * author - Jane Sivieng
 */
describe("System Configuration Manager - Test Suite", function() {
	
	describe("Successful read of system configuration" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummySystemConfigDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the system configuration
			service.prototype.getSystemConfiguration = function(successCallback, errorCallback) {
				
				var systemConfiguration = new diary.model.SystemConfiguration({
					 								 'id' : 1,
					 								 'token' : "TOKEN"
													});
				
				successCallback(systemConfiguration);
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.SYSTEMCONFIGURATIONDELEGATE, dummySystemConfigDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		
		it("should be able to return the system configuration", function() {
		   
			// holds the student manager
			var manager = new diary.service.SystemConfigurationManager();

			// the success function
			function success(systemConfiguration) {
			
				// the manager should have returned a system configuration
				expect(systemConfiguration instanceof diary.model.SystemConfiguration).toBeTruthy();
				
				// the profile that was returned should be the same as the dummy data			
				expect(systemConfiguration.id).toEqual(1);
				expect(systemConfiguration.get("token")).toEqual("TOKEN");			
			};
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// get the system configuration from the manager
			manager.getSystemConfiguration(success, error);
		});
		
		it("should be able to populate the system configuration", function() {
		   
			// holds the system configuration that is to be updated
			var systemConfiguration = new diary.model.SystemConfiguration();
			
			// holds the student manager
			var manager = new diary.service.SystemConfigurationManager();

			// the success function
			function success() {
				
				// the profile that was returned should be the same as the dummy data			
				expect(systemConfiguration.id).toEqual(1);
				expect(systemConfiguration.get("token")).toEqual("TOKEN");
				
			};
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// get the system configuration from the manager
			manager.getSystemConfiguration(success, error, systemConfiguration);
		});
		
	});

	describe("No system configuration is found in data source" , function() {
			
		// holds the dummy app database layer that is required for testing
		var dummySystemConfigDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the system configuration
			service.prototype.getSystemConfiguration = function(successCallback, errorCallback) {
				
				successCallback(null);
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.SYSTEMCONFIGURATIONDELEGATE, dummySystemConfigDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should generate error if no system configuration is found", function() {
		   
			// holds the student manager
			var manager = new diary.service.SystemConfigurationManager();

			// success function
			function success() {
				throw "Test failed - Should have produced an error";
			}
			
			// error function
			function error(message) {
				expect(message).toEqual("No system configuration was found, system configuration should be defined");
			}
			
			// get the system configuration from the manager
			manager.getSystemConfiguration(success, error);
			
		});
		
	});
	
	describe("Error while trying to read system configuration from data source" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummySystemConfigDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the school profile
			service.prototype.getSystemConfiguration = function(success, error) {
				
				error("Error while trying to get system configuration");
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.SYSTEMCONFIGURATIONDELEGATE, dummySystemConfigDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should generate error if an error occurs while trying to read system configuration", function() {
		   
			// holds the student manager
			var manager = new diary.service.SystemConfigurationManager();
			
			// the success function
			function success() {
				throw "Test failed - Should have produced an error";
			}
			
			// the error function
			function error(message) {
				expect(message).toEqual("Error while trying to get system configuration");
			}
			
			// try to get the system configuration
			manager.getSystemConfiguration(success, error);
			
		});
		
	});
	
	describe("Trying to set system configuration into the data source" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummySystemConfigDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the update system configuration
			service.prototype.updateSystemConfiguration = function(systemConfiguration, success, error) {
				
				success();
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.SYSTEMCONFIGURATIONDELEGATE, dummySystemConfigDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should be able to update system configuration", function() {
		   
			// holds the student manager
			var manager = new diary.service.SystemConfigurationManager();
			
			// create a dummy profile
			var systemConfiguration = new diary.model.SystemConfiguration({ 'token' : "TOKEN" });
			
			// the success function
			var success = jasmine.createSpy();

			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// try to get the system configuration
			manager.setSystemConfiguration(systemConfiguration, success, error);

			expect(success).toHaveBeenCalled();
		});
		
	});
		
});