/**
 * Unit Tests for the InitApplicationCommand
 * 
 * @author John Pearce
 */
describe("Init Application Command - Test Suite", function() {

	var simulatedSchemaVersionSupported;
	var simulatedConfirmDeleteUserData;
	var confirmMessage = "Your application has been upgraded from an unsupported version. This occurs when upgrading the 2012 diary to 2013 or later.\n\n" +
		"To continue you must DELETE ALL 2012 DATA.\n" +
		"Are you sure you want to continue?";

	var dummyConfirm = function (message) {
		console.log("CONFIRM DIALOG (simulated): " + message, simulatedConfirmDeleteUserData);
		return simulatedConfirmDeleteUserData;
	};
	var realConfirm = window.confirm;

	// the dummy service for the schema manager
	var dummySchemaManager = (function() {

		return function() {

			this.installSchemaUpdates = function() { };
			this.isSchemaVersionSupported = function () { return simulatedSchemaVersionSupported; }
			this.dropDatabase = function () {};

		};

	})();

	// the dummy service for the demo data manager
	var dummyDemoDataManager = (function() {

		return function() {

			this.installDemoData = function() { };
			this.installDefaultData = function() { };

		};

	})();

	// the dummy service for the default data manager
	var dummyDataManager = (function() {

		return function() {

			this.installDefaultData = function() { };

		};

	})();

	// the dummy service for the database manager
	var dummyDatabaseManager = (function() {

		return function() {

			this.setDatabase = function() { };
			this.databaseExists = function() { return true; };

		};

	})();

	var dummyContentManager = (function () {

		return function () {
			this.deleteAllDirectories = function () {};
		}

	})();


	// setup the env for the the unit test
	beforeEach(function() {

		// configure the application context for the unit test
		app = new Rocketboots.Application({

		   mapEvents: function() {
			   // map events needed for the test
		   },

		   mapServices: function() {
			   // map the services needed for the test
			   this.mapService(ServiceName.SCHEMAMANAGER, dummySchemaManager);
			   this.mapService(ServiceName.DEMODATAMANAGER, dummyDemoDataManager);
			   this.mapService(ServiceName.DEFAULTDATAMANAGER, dummyDataManager);
			   this.mapService(ServiceName.APPDATABASE, dummyDatabaseManager);
			   this.mapService(ServiceName.CONTENTMANAGER, dummyContentManager);
		   },

		   registerTemplates: {
			   // register templates, if needed
		   }

		});

		app.model = {};
		app.model.get = function(modelname) {
			if (modelname == ModelName.APPLICATIONSTATE)
				return new diary.model.DiaryModel();
			if (modelname == ModelName.SYSTEMCONFIGURATION)
				return new diary.model.SystemConfiguration();
			else
				return {};
		};

		// init the application context
		app.initialize();

		// mock views
		app.view.tokenView = {};
		app.view.dialogView = {};
		app.view.calendarView = {};
		app.view.containerChromeView = {};
		app.view.notificationView = {};

		// application arguments
		window.args = {
			ManifestLocation : null,
			ArchiveLocation : null,
			DemoArchiveLocation : null,
			MacAddresses : null
		};

		// AIR dependencies
		window.air = air || {};
		air.NativeApplication = {
			'nativeApplication': {
				'applicationDescriptor': "<xml><versionLabel>test application version</versionLabel><versionNumber>0.0.0</versionNumber></xml>"
			}
		};

		window.confirm = dummyConfirm;

		simulatedSchemaVersionSupported = true;
		simulatedConfirmDeleteUserData = false;

	});

	afterEach(function () {
		window.confirm = realConfirm;
	});

	/*
	 * invoke the command
	 */
	it("should be able to invoke the command", function() {

		// holds the command
		var command = new diary.controller.InitApplicationCommand();

		// execute the command
		var testFunction = function() {
			command.execute();
		};

		// no error should be generated
		expect(testFunction).not.toThrow();

	});

	it("should display error and close when bad schema version and user cancels", function() {

		// holds the command
		var command = new diary.controller.InitApplicationCommand();
		var schemaManager = app.locator.getService(ServiceName.SCHEMAMANAGER);

		simulatedSchemaVersionSupported = false;
		simulatedConfirmDeleteUserData = false;

		// execute the command
		var testFunction = function() {
			command.execute();
		};

		spyOn(window, "confirm").andCallThrough();
		spyOn(schemaManager, "dropDatabase");

		// no error should be generated
		expect(testFunction).toThrow();
		expect(window.confirm).toHaveBeenCalledWith(confirmMessage);
		expect(schemaManager.dropDatabase).not.toHaveBeenCalled();

	});

	it("should display error and drop database when bad schema version and user confirms", function() {

		// holds the command
		var command = new diary.controller.InitApplicationCommand();
		var schemaManager = app.locator.getService(ServiceName.SCHEMAMANAGER);
		var contentManager = app.locator.getService(ServiceName.CONTENTMANAGER);

		simulatedSchemaVersionSupported = false;
		simulatedConfirmDeleteUserData = true;

		// execute the command
		var testFunction = function() {
			command.execute();
		};

		spyOn(window, "confirm").andCallThrough();
		spyOn(schemaManager, "dropDatabase");
		spyOn(contentManager, "deleteAllDirectories");

		// no error should be generated
		expect(testFunction).not.toThrow();
		expect(window.confirm).toHaveBeenCalledWith(confirmMessage);
		expect(schemaManager.dropDatabase).toHaveBeenCalled();
		expect(contentManager.deleteAllDirectories).toHaveBeenCalled();

	});

});