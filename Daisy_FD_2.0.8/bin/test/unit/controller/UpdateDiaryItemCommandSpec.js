/**
 * Unit Tests for the UpdateDiaryItemCommand
 * 
 * author - Justin Judd
 */
describe("UpdateDiaryItemCommand - Test Suite", function() {

	describe("Successful cases for UpdateDiaryItemCommand Suite", function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// mock manager
			var dummyManager = (function() {

				// counts calls to methods
				var calls = {
					updateDiaryItem : 0
				};

				var service = function() {
				};

				service.prototype.updateDiaryItem = function(diaryItem, attachmentsToAdd, attachmentsToRemove,
						successCallback, errorCallback) {

					calls.updateDiaryItem++;
					successCallback(diaryItem);

				};

				service.prototype.getCalls = function(functionName) {
					return calls[functionName];
				};

				return service;

			})();

			// configure the application context for the unit
			// test
			app = new Rocketboots.Application({

				modelClass : diary.model.DiaryModel,

				mapEvents : function() {
					// map events needed for the test
				},

				mapServices : function() {
					// map the services needed for the test
					this.mapService(ServiceName.DIARYITEMMANAGER, dummyManager);
				}

			});

			// init the application context
			app.initialize();

		});

		it("should be able to invoke the command (task)", function() {

			var day = new Rocketboots.date.Day();

			// add an item to the model
			day.addDays(1);
			var assignedDay = new Rocketboots.date.Day().addDays(-1),
				assignedTime = new Rocketboots.date.Time(12, 30),
				dueDay = new Rocketboots.date.Day(),
				dueTime = new Rocketboots.date.Time(13, 0),
				attrs = {
					id: 1,
					type: diary.model.DiaryItem.TYPE.HOMEWORK,
					assignedDay: assignedDay,
					assignedTime: assignedTime,
					dueDay: dueDay,
					dueTime: dueTime,
					title: "UpdateDiaryItemCommand Test (Homework)",
					subject: new diary.model.Subject({'id': 1, 'title': "English", 'colour': "red"})
				};
			var item = new diary.model.Task(attrs);
			app.model.get(ModelName.DIARYITEMS).add(item);

			// update the model
			var newAttrs = {
				dueDay: new Rocketboots.date.Day().addDays(2),
				title: "UpdateDiaryItemCommand Test (Homework) Updated"
			};
			var newItem = item.clone();
			newItem.set("dueDay", newAttrs.dueDay);
			newItem.set("title", newAttrs.title);

			// execute the command
			var command = new diary.controller.UpdateDiaryItemCommand();
			command.execute(newItem);

			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("updateDiaryItem")).toEqual(1);

			// check item was updated in the app model
			var items = app.model.get(ModelName.DIARYITEMS);
			expect(items.length).toEqual(1);
			expect(items.get(1).get("title")).toEqual(newAttrs.title);
			expect(items.get(1).get("assignedDay").getKey()).toEqual(assignedDay.getKey());
			expect(items.get(1).get("assignedTime").toInteger()).toEqual(assignedTime.toInteger());
			expect(items.get(1).get("dueDay").getKey()).toEqual(newAttrs.dueDay.getKey());
			expect(items.get(1).get("dueTime").toInteger()).toEqual(dueTime.toInteger());

		});

		it("should be able to invoke the command (note)", function() {

			var day = new Rocketboots.date.Day();
			var items = app.model.get(ModelName.DIARYITEMS);

			// add a note to the model
			day.addDays(1);
			var entry1Attrs = {
				id: 1,
				timestamp: day.getDate(),
				text: "UpdateDiaryItemCommand Test 1 Entry"
			};
			var entry1 = new diary.model.NoteEntry(entry1Attrs);

			var note1Attrs = {
				id: 1,
				atDay: new Rocketboots.date.Day(),
				entries: new diary.collection.NoteEntryCollection([entry1])
			};
			var note1 = new diary.model.Note(note1Attrs);
			items.add(note1);

			// add a new entry

			// first, create a new entry
			day.addDays(2);
			var entry2Attrs = {
				id: 2,
				timestamp: day.getDate(),
				text: "UpdateDiaryItemCommand Test 2 Entry"
			};
			var entry2 = new diary.model.NoteEntry(entry2Attrs);

			// second, copy the note
			var note2 = items.get(1).clone();

			// third, copy the entry collection
			var entries = new diary.collection.NoteEntryCollection(
					items.get(1).get("entries").models);

			// fourth, add the new entry to the copy
			entries.add(entry2);

			// finally, replace the copy's entries
			note2.set("entries", entries);

			// execute the command
			var command = new diary.controller.UpdateDiaryItemCommand();
			command.execute(note2);

			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("updateDiaryItem")).toEqual(1);

			// check item was updated in the app model
			var newItems = app.model.get(ModelName.DIARYITEMS);
			expect(newItems.length).toEqual(1);

			var newNote = newItems.get(1);
			var newEntries = newNote.get("entries");
			expect(newEntries.length).toEqual(2);

			var newEntry = newEntries.get(2);
			expect(newEntry.get("timestamp")).toEqual(entry2Attrs.timestamp);
			expect(newEntry.get("text")).toEqual(entry2Attrs.text);

		});

		it("should not affect other models", function() {

			// add two items to the model
			var attrs1 = {
				id: 1,
				type: diary.model.DiaryItem.TYPE.HOMEWORK,
				assignedDay: new Rocketboots.date.Day().addDays(-1),
				assignedTime: new Rocketboots.date.Time(12, 30),
				dueDay: new Rocketboots.date.Day(),
				dueTime: new Rocketboots.date.Time(13, 0),
				title: "UpdateDiaryItemCommand Test (Homework)",
				subjectName: "English"
			};
			var item1 = new diary.model.DiaryItem(attrs1);
			app.model.get(ModelName.DIARYITEMS).add(item1);

			var attrs2 = {
				id: 2,
				type: diary.model.DiaryItem.TYPE.ASSIGNMENT,
				assignedDay: new Rocketboots.date.Day().addDays(1),
				assignedTime: new Rocketboots.date.Time(12, 30),
				dueDay: new Rocketboots.date.Day().addDays(2),
				dueTime: new Rocketboots.date.Time(13, 0),
				title: "UpdateDiaryItemCommand Test (Assignment)",
				subjectName: "Maths"
			};
			var item2 = new diary.model.DiaryItem(attrs2);
			app.model.get(ModelName.DIARYITEMS).add(item2);

			// update one item
			var newAttrs = {
				dueDay: new Rocketboots.date.Day().addDays(3),
				title: "UpdateDiaryItemCommand Test (Homework) Updated"
			};
			var newItem = item1.clone();
			newItem.set("dueDay", newAttrs.dueDay);
			newItem.set("title", newAttrs.title);

			// execute the command
			var command = new diary.controller.UpdateDiaryItemCommand();
			command.execute(newItem);

			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("updateDiaryItem")).toEqual(1);

			// check the correct item was updated
			var items = app.model.get(ModelName.DIARYITEMS);
			expect(items.length).toEqual(2);
			expect(items.get(1).get("type")).toEqual(attrs1.type);
			expect(items.get(1).get("dueDay")).toEqual(newAttrs.dueDay);
			expect(items.get(1).get("title")).toEqual(newAttrs.title);
			expect(items.get(2).get("type")).toEqual(attrs2.type);
			expect(items.get(2).get("dueDay")).toEqual(attrs2.dueDay);
			expect(items.get(2).get("title")).toEqual(attrs2.title);

		});

	});

});