/**
 * Unit Tests for the UpdateStudentProfileCommand
 * 
 * author - Justin Judd
 */
describe("UpdateStudentProfileCommand - Test Suite", function() {

	/**
	 * Suite of tests for proper operation of the UpdateStudentProfileCommand
	 */
	describe("Successful cases for UpdateStudentProfileCommand Suite" , function() {

		// the dummy student manager
		var dummyStudentManager = (function() {
			
			var calls = {
					setStudentProfile: 0
			};

			var service = function() {
			};

			// the dummy service of the student profile
			service.prototype.setStudentProfile = function(student,
					successCallback, errorCallback) {

				calls.setStudentProfile++;
				successCallback(student);

			};
			
			service.prototype.getCalls = function(functionName) {
				return calls[functionName];
			};

			return service;

		})();
		
		// setup the env for the the unit test
		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

				modelClass:	diary.model.DiaryModel,
				
				mapEvents: function() {
					// map events needed for the test
				},

				mapServices: function() {
					// map the services needed for the test
					this.mapService(ServiceName.STUDENTMANAGER, dummyStudentManager);
				}

			});

			// init the application context
			app.initialize();

		});

		/*
		 * invoke the command
		 */
		it("should be able to invoke the command", function() {

			var student = new diary.model.StudentProfile();

			// execute the command
			var command = new diary.controller.UpdateStudentProfileCommand(student);
			command.execute();

			// check mock service calls
			var service = app.locator.getService(ServiceName.STUDENTMANAGER);
			expect(service.getCalls("setStudentProfile")).toEqual(1);

		});

	});

});