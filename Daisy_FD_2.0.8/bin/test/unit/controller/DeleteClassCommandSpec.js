/**
 * Unit Tests for the DeleteClassCommand
 * 
 * author - Justin Judd
 */
describe("DeleteClassCommand - Test Suite", function() {

	describe("Successful cases for DeleteClassCommand Suite", function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// mock manager
			var dummyManager = (function() {

				// counts calls to methods
				var calls = {
					deleteClass : 0,
					deleteSubject : 0
				};

				var service = function() {
				};

				service.prototype.removeClassFromTimetable = function(timetable, classModel, successCallback, errorCallback) {
					calls.deleteClass++;
					timetable.get("classes").remove(classModel);
					successCallback(timetable);
				};
				
				service.prototype.deleteSubject = function(subject, successCallback, errorCallback) {
					calls.deleteSubject++;
					successCallback(true);
				};

				service.prototype.getCalls = function(functionName) {
					return calls[functionName];
				};
				
				service.prototype.incrementCalls = function(functionName) {
					calls[functionName]++;
				};

				return service;

			})();

			// configure the application context for the unit test
			app = new Rocketboots.Application({

				modelClass : diary.model.DiaryModel,

				mapEvents : function() {
					// map events needed for the test
				},

				mapServices : function() {
					// map the services needed for the test
					this.mapService(ServiceName.TIMETABLEMANAGER, dummyManager);
				}

			});

			// init the application context
			app.initialize();

		});
		
		it("should be able to invoke the command and delete a class", function() {
			
			var classDeletedEventRaised = false;
			
			function classDeleted(classId) {
				
				// check that the class id was specified
				expect(classId).toEqual(1);
				classDeletedEventRaised = true;
			}
			
			var testSubject = new diary.model.Subject({ 'id': 1,
				'title': "Test Subject",
				'colour': "Red" });

			app.model.get(ModelName.SUBJECTS).add(testSubject);

			var classModel = new diary.model.Class({ id: 1,
													 periodId: 2,
													 subject: testSubject,
													 room: "Test Room 1",
													 cycleDay: 3 });
			
			// create a dummy timetable
			var timetable = new diary.model.Timetable({ 'id' : 1,
														'startDay' : new Rocketboots.date.Day(),
														'cycleLength' : 10 });
			
			timetable.get("classes").add(classModel);
			
			app.model.set({'timetable' : timetable});
			
			// listen to the class created command
			app.context.on(EventName.CLASSDELETED, classDeleted, this);
			
			// execute the command
			var command = new diary.controller.DeleteClassCommand();
			command.execute(timetable, 1);
			
			expect(classDeletedEventRaised).toBeTruthy();
			
			// check the timetable details
			timetableModel = app.model.get(ModelName.TIMETABLE);
			expect(timetableModel.get('classes').length).toEqual(0);
			
			// check the subjects details
			subjects = app.model.get(ModelName.SUBJECTS);
			expect(subjects.length).toEqual(0);

			// check mock service calls
			var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);
			expect(service.getCalls("deleteClass")).toEqual(1);
			expect(service.getCalls("deleteSubject")).toEqual(1);

		});
		
		it("should be able to invoke the command twice", function() {
			
			var classDeletedEventRaised = 0,
				service = app.locator.getService(ServiceName.TIMETABLEMANAGER);;
			
			function classDeleted(classId) {
				
				// check that the class id was specified
				expect(classId == 1 || classId == 2).toBeTruthy();
				classDeletedEventRaised++;
			}
			
			service.deleteSubject = function(subject, successCallback, errorCallback) {
				
				// the first time it is called return false
				// the second time return true to simulate that the subject is no longer being used
				// by any subject
				this.incrementCalls('deleteSubject');
				if (this.getCalls('deleteSubject') == 1) {
					successCallback(false);
				} else {
					successCallback(true);
				}
			};
			
			var testSubject = new diary.model.Subject({ 'id': 1,
														'title': "Test Subject",
														'colour': "Red" });

			app.model.get(ModelName.SUBJECTS).add(testSubject);

			var classModel1 = new diary.model.Class({ id: 1,
													  periodId: 2,
													  subject: testSubject,
													  room: "Test Room 1",
													  cycleDay: 3 });
			
			var classModel2 = new diary.model.Class({ id: 2,
				  									  periodId: 2,
				  									  subject: testSubject,
				  									  room: "Test Room 1",
				  									  cycleDay: 4 });
			
			// create a dummy timetable
			var timetable = new diary.model.Timetable({ 'id' : 1,
														'startDay' : new Rocketboots.date.Day(),
														'cycleLength' : 10 });
			
			timetable.get("classes").add(classModel1);
			timetable.get("classes").add(classModel2);
			
			app.model.set({'timetable' : timetable});
			
			// listen to the class created command
			app.context.on(EventName.CLASSDELETED, classDeleted, this);
			
			// execute the command
			var command1 = new diary.controller.DeleteClassCommand();
			command1.execute(timetable, 1);
			
			expect(classDeletedEventRaised).toEqual(1);
			
			// check the timetable details
			timetableModel = app.model.get(ModelName.TIMETABLE);
			expect(timetableModel.get('classes').length).toEqual(1);
			
			// check the subjects details
			subjects = app.model.get(ModelName.SUBJECTS);
			expect(subjects.length).toEqual(1);
			
			var command2 = new diary.controller.DeleteClassCommand();
			command2.execute(timetable, 2);
			
			expect(classDeletedEventRaised).toEqual(2);
			
			// check the timetable details
			timetableModel = app.model.get(ModelName.TIMETABLE);
			expect(timetableModel.get('classes').length).toEqual(0);
			
			// check the subjects details
			subjects = app.model.get(ModelName.SUBJECTS);
			expect(subjects.length).toEqual(0);

			// check mock service calls
			expect(service.getCalls("deleteClass")).toEqual(2);

		});

	});

});