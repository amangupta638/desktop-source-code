/**
 * $Id: DaySpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
describe("Day - Test Suite", function() {

	var START_YEAR = 1900,
		END_YEAR = 2100;
		
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	var daysInMonths = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
		daysInMonthsInLeapYear = [ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		
	var isALeapYear = function(year) {
		return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
	};
	
	var monthDays = function(year, month) {
		return isALeapYear(year) ? daysInMonthsInLeapYear[month] : daysInMonths[month];
	};

	/**
	 * Suite of tests which should create a Day
	 */
	describe("Successful creation of Day" , function() {
		
		beforeEach(function() {
			
		});
	
		/*
		 * creation of a Day with default values
		 */
		it("should be able to create a Day with default values as today", function() {
		   
			// create the test task that we will use for the test
			var now = new Date();
			
			var day = new Rocketboots.date.Day();
			
			var jsDate = day.getDate();
			
			expect(jsDate.getFullYear()).toEqual(now.getFullYear());					
			expect(jsDate.getMonth()).toEqual(now.getMonth());					
			expect(jsDate.getDate()).toEqual(now.getDate());					
		});

		/*
		 * creation of a Day from a Date
		 */
		it("should be able to create a Day on specified date", function() {
		   
			// create the test task that we will use for the test
			var year = 2012,
				month = 2,
				dayInMonth = 15,
				day = new Rocketboots.date.Day(year, month, dayInMonth);
			
			var jsDate = day.getDate();
			
			expect(jsDate.getFullYear()).toEqual(year);					
			expect(jsDate.getMonth()).toEqual(month);					
			expect(jsDate.getDate()).toEqual(dayInMonth);					
		});
				
		/*
		 * creation of a Day from a Date
		 */
		it("should be able to create a Day based on a Date", function() {
		   
			// create the test task that we will use for the test
			var testDate = new Date(2012, 2, 15);
			
			var day = new Rocketboots.date.Day(testDate);
			
			var jsDate = day.getDate();
			
			expect(jsDate.getFullYear()).toEqual(testDate.getFullYear());					
			expect(jsDate.getMonth()).toEqual(testDate.getMonth());					
			expect(jsDate.getDate()).toEqual(testDate.getDate());					
		});

		/*
		 * creation of a Day from another Day
		 */
		it("should be able to create a Day from another Day", function() {
		   
			// create the test task that we will use for the test
			var originalDate = new Date(2012, 2, 19),
				testDay = new Rocketboots.date.Day(originalDate),
				testDate = testDay.getDate();
			
			var day = new Rocketboots.date.Day(testDay);
			
			var jsDate = day.getDate();
			
			expect(jsDate.getFullYear()).toEqual(testDate.getFullYear());					
			expect(jsDate.getMonth()).toEqual(testDate.getMonth());					
			expect(jsDate.getDate()).toEqual(testDate.getDate());					
		});
	});

	/**
	 * Suite of tests which should demonstrate leap year functionality
	 */
	describe("Successfully retrieve Day information" , function() {
		
		beforeEach(function() {
			
		});
	

		it("should be able to retrieve correct Month", function() {
		   
			for(var year=START_YEAR; year<=END_YEAR; year++) {
				for(var month=0; month<12; month++) {
					var day = new Rocketboots.date.Day(year, month, 1);
	
					expect(day.getMonth()).toEqual(month);							
				}
			}
		});

		it("should be able to retrieve correct Month name", function() {
			
			for(var year=START_YEAR; year<=END_YEAR; year++) {
				for(var month=0; month<12; month++) {
					var day = new Rocketboots.date.Day(year, month, 1);
	
					expect(day.getMonthName()).toEqual(months[month]);							
				}
			}
		});
	
		it("should be able to retrieve correct short Month name", function() {
		
			for(var year=START_YEAR; year<=END_YEAR; year++) {
				for(var month=0; month<12; month++) {
					var day = new Rocketboots.date.Day(year, month, 1);
	
					expect(day.getMonthShortName()).toEqual(shortMonths[month]);							
				}
			}
		});
			
		it("should be able to retrieve correct day key", function() {
		   
			for(var year=START_YEAR; year<=END_YEAR; year+=13) {
				for(var month=0; month<12; month++) {
					for(var dayInMonth=1; dayInMonth<=monthDays(year, month); dayInMonth++) {
						var day = new Rocketboots.date.Day(year, month, dayInMonth);
						
						// Create the expected key in the format YYYYMMDD
						
						var expectedMonth = "" + (month + 1),
							expectedKey = "" + year;
						
						if (expectedMonth < 10) expectedKey += "0";
						expectedKey += ("" + expectedMonth);
						
						if (dayInMonth < 10) expectedKey += "0";
						expectedKey += ("" + dayInMonth);
	
						expect(day.getKey()).toEqual(expectedKey);							
					}
					
				}
			}
		});

		it("should be able to parse the correct date from a day key", function() {
			   
			for(var year=START_YEAR; year<=END_YEAR; year+=13) {
				for(var month=0; month<12; month++) {
					for(var dayInMonth=1; dayInMonth<=monthDays(year, month); dayInMonth++) {
						// Create the expected key in the format YYYYMMDD
						
						var expectedMonth = "" + (month + 1),
							expectedKey = "" + year;
						
						if (expectedMonth < 10) expectedKey += "0";
						expectedKey += ("" + expectedMonth);
						
						if (dayInMonth < 10) expectedKey += "0";
						expectedKey += ("" + dayInMonth);
	
						var day = Rocketboots.date.Day.parseDayKey(expectedKey);
						
						expect(day.getKey()).toEqual(expectedKey);							
					}
					
				}
			}
		});
		
						
	});
	
	describe("Successfully return new Days based on an existing Day", function() {
	
		beforeEach(function() {
			
		});
	
		it("should be able to return the last day of the year", function() {
			
			for(var year=START_YEAR; year <= END_YEAR; year++) {
				var day = new Rocketboots.date.Day(year, 5, 15);
				var endOfYear = day.lastDayOfYear();
				
				expect(endOfYear.getFullYear()).toEqual(year);							
				expect(endOfYear.getMonth()).toEqual(11);							
				expect(endOfYear.getDayInMonth()).toEqual(31);							
			}
			
		});
		
		it("should be able to return the first day of the year", function() {
			
			for(var year=START_YEAR; year <= END_YEAR; year++) {
				var day = new Rocketboots.date.Day(year, 5, 16);
				var endOfYear = day.firstDayOfYear();
				
				expect(endOfYear.getFullYear()).toEqual(year);							
				expect(endOfYear.getMonth()).toEqual(0);							
				expect(endOfYear.getDayInMonth()).toEqual(1);							
			}
			
		});
		
		it("should be able to return the first day of the month", function() {
		
			for(var year=START_YEAR; year<=END_YEAR; year+=13) {
				for(var month=0; month<12; month++) {
					for(var dayInMonth=1; dayInMonth<=monthDays(year, month); dayInMonth++) {
						var day = new Rocketboots.date.Day(year, month, dayInMonth);
						var firstDay = day.firstDayOfMonth();
	
						expect(firstDay.getFullYear()).toEqual(year);							
						expect(firstDay.getMonth()).toEqual(month);							
						expect(firstDay.getDayInMonth()).toEqual(1);	
									
					}
					
				}
			}
		});

		it("should be able to return the last day of the month", function() {
		
			for(var year=START_YEAR; year<=END_YEAR; year+=13) {
				for(var month=0; month<12; month++) {
					var monthDayCount = monthDays(year, month);
					for(var dayInMonth=1; dayInMonth<=monthDayCount; dayInMonth++) {
						var day = new Rocketboots.date.Day(year, month, dayInMonth);
						var lastDay = day.lastDayOfMonth();
	
						expect(lastDay.getFullYear()).toEqual(year);							
						expect(lastDay.getMonth()).toEqual(month);							
						expect(lastDay.getDayInMonth()).toEqual(monthDayCount);	

					}
					
				}
			}
		});
				
	});
	
	/**
	 * Suite of tests which should demonstrate leap year functionality
	 */
	describe("Successfully handle Year functions" , function() {
		
		beforeEach(function() {
			
		});
	
		/*
		 * creation of a Day with default values
		 */
		it("should be able to identify Leap Years", function() {
	
			for(var year=START_YEAR; year<=END_YEAR; year++) {
				var date = new Date(year, 1, 1);
				var day = new Rocketboots.date.Day(date);
				var shouldBeLeapYear = isALeapYear(year);
				
				expect(day.isLeapYear()).toEqual(shouldBeLeapYear);					
				
			}
						
		});
	
	});
	
});