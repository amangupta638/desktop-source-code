/**
 * $Id: ArrayUtilSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
describe("RocketBoots Array Utilities", function() {

	var baseArray = null;

	beforeEach(function() {
		baseArray = ["A", "B"];
	});

	it("should be able to push array objects", function() {

		var array = ['c', 'd'];

		var returnValue = Rocketboots.util.ArrayUtil.pushAll(baseArray, array);

		expect(returnValue).toEqual(baseArray, "Should return modified array");
		expect(returnValue.length).toEqual(4, "Should have added items");
		expect(baseArray.length).toEqual(4, "Should modify original array");
		expect(baseArray[0]).toEqual("A");
		expect(baseArray[1]).toEqual("B");
		expect(baseArray[2]).toEqual('c');
		expect(baseArray[3]).toEqual('d');
	});

	it("should be able to push an argument list", function() {

		var args = null;

		(function () {
			args = arguments;
		})(1, 123);

		var returnValue = Rocketboots.util.ArrayUtil.pushAll(baseArray, args);

		expect(returnValue).toEqual(baseArray, "Should return modified array");
		expect(returnValue.length).toEqual(4, "Should have added items");
		expect(baseArray.length).toEqual(4, "Should modify original array");
		expect(baseArray[0]).toEqual("A");
		expect(baseArray[1]).toEqual("B");
		expect(baseArray[2]).toEqual(1);
		expect(baseArray[3]).toEqual(123);
	});

	it("should be able to push object values, ignoring keys", function() {

		var object = {
			"key" : "value1",
			"key2" : 123
		};

		var returnValue = Rocketboots.util.ArrayUtil.pushAll(baseArray, object);

		expect(returnValue).toEqual(baseArray, "Should return modified array");
		expect(returnValue.length).toEqual(4, "Should have added items");
		expect(baseArray.length).toEqual(4, "Should modify original array");
		expect(baseArray[0]).toEqual("A");
		expect(baseArray[1]).toEqual("B");
		expect(baseArray[2] === 123 || baseArray[2] === "value1").toBeTruthy();
		expect(baseArray[3] === 123 || baseArray[3] === "value1").toBeTruthy();
	});

});