/**
 * Unit Tests for the Subject Model
 * 
 * author - John Pearce
 */
describe("Subject - Test Suite", function() {
	
	/**
	 * Suite of tests which should create a subject
	 */
	describe("Successful creation of Subject Suite", function() {
		
		beforeEach(function() {
			
		});
		
		/*
		 * creation of a subject with default values
		 */
		it("should be able to create a subject with default values", function() {
			
			// create the model that we will use for the test
			var subject = new diary.model.Subject();
			
			expect(subject.get("title")).toBeNull();
			expect(subject.get("colour")).toBeNull();
			
		});
		
		/*
		 * creation of a subject with given values
		 */
		it("should be able to create a subject with given values", function() {
		   
			var title = "2U Maths",
				colour = "blue";
			
			// create the test subject that we will use for the test
			var subject = new diary.model.Subject({
				'title' : title,
				'colour' : colour
			});
			
			expect(subject.get("title")).toEqual(title);
			expect(subject.get("colour")).toEqual(colour);
			
		});
			
	});
	
	/**
	 * Suite of tests which should stop the subject from being created
	 */
	describe("Unsuccessful creation of Subject Suite" , function() {
		
		// holds the attributes that are to be used for each test
		var attrs;
		// holds the subject that is to be used for testing
		var subject = null;
		
		beforeEach(function() {
			// holds the attributes that are to be assigned during save
			attrs = {
				title: "4U English",
				colour: "brown"
			};
			
			// create the test diary item that we will use for the test
			subject = new diary.model.Subject();
		});
		
		/*
		 * title not specified
		 */
		it("should fail when subject title is not specified", function() {

			// update the attributes for the test
			attrs.title = undefined;
			
			expect(subject.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("No title specified");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
		/*
		 * colour not specified
		 */
		it("should fail when subject colour is not specified", function() {

			// update the attributes for the test
			attrs.colour = undefined;
			
			expect(subject.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("No colour specified");
				}
			})).toBeFalsy("Should have failed to save");
			
		});

		/*
		 * title type
		 */
		it("should fail when subject title is not a string", function() {

			// update the attributes for the test
			attrs.title = new Date();
			
			expect(subject.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("Title must be of type string");
				}
			})).toBeFalsy("Should have failed to save");
			
		});

		/*
		 * colour type
		 */
		it("should fail when subject colour is not a string", function() {

			// update the attributes for the test
			attrs.colour = [];
			
			expect(subject.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("Colour must be of type string");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
		/*
		 * title type
		 */
		it("should fail when subject title is zero length", function() {

			// update the attributes for the test
			attrs.title = "";
			
			expect(subject.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("Title string must not be empty");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
		/*
		 * colour type
		 */
		it("should fail when subject colour is zero length", function() {

			// update the attributes for the test
			attrs.colour = "";
			
			expect(subject.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("Colour string must not be empty");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
	});
		
});