/**
 * Unit Tests for the Content Item Model
 * 
 * author - Brian Bason
 */
describe("Content Item - Test Suite", function() {
	
	describe("Successful creation of Content Item Suite" , function() {
		
		beforeEach(function() {
			
		});
	
		it("should be able to create a content menu item", function() {
		   
			// test properties
			var title = "Test Table Of Content Item";
			var url = "http://google.com";
			
			// create the test content menu item that we will use for the test
			var contentItem = new diary.model.ContentItem({ 'title' : title,
															'url' : url });
			
			expect(contentItem.get("title")).toEqual(title);					
			expect(contentItem.get("url")).toEqual(url);
			expect(contentItem.get("fromDate")).toBeNull();
			expect(contentItem.get("toDate")).toBeNull();
			expect(contentItem.get("subItems")).toBeNull();
			
		});
		
		it("should be able to add a sub item to the content item", function() {
			   
			// test properties
			var title = "Test Content Item";
			var url = "http://google.com";
			var subItems = "fabbc9e2378a49c29ebae91b704ef4d3";
			
			// create the test content menu item that we will use for the test
			var contentItem = new diary.model.ContentItem({ 'title' : title,
															'url' : url,
															'subItems' : subItems });
			
			expect(contentItem.get("title")).toEqual(title);					
			expect(contentItem.get("url")).toEqual(url);
			expect(contentItem.get("fromDate")).toBeNull();
			expect(contentItem.get("toDate")).toBeNull();
			expect(contentItem.get("subItems")).toEqual(subItems);
			
		});
			
	});
	
	describe("Unsuccessful creation of Content Menu Item Suite" , function() {
		
		beforeEach(function() {
			
		});
		
		it("should fail when creating item with no details", function() {

			function failFunction() {
				new diary.model.ContentItem();
			}
			
			expect(failFunction).toThrow("Title must be specified, title cannot be NULL");
			
		});
		
		it("should fail when creating item with no title", function() {

			function failFunction() {
				new diary.model.ContentItem({ 'url' : "URL of item" });
			}
			
			expect(failFunction).toThrow("Title must be specified, title cannot be NULL");
			
		});
		
		it("should fail when creating item with no location", function() {

			function failFunction() {
				new diary.model.ContentItem({ 'title' : "Title of item" });
			}
			
			expect(failFunction).toThrow("URL must be specified, url cannot be NULL");
			
		});
		
	});
		
});