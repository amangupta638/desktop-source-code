/**
 * Unit Tests for the System Configuration Model
 * 
 * author - Jane Sivieng
 */
describe("System Configuration - Test Suite", function() {
	
	/**
	 * Suite of tests which should create a System Configuration
	 */
	describe("Successful creation of System Configuration Suite" , function() {
		
		beforeEach(function() {
			
		});
	
		/*
		 * creation of a System Configuration with default values
		 */
		it("should be able to create a System Configuration with default values", function() {
		   
			// create the test System Configuration that we will use for the test
			var systemConfiguration = new diary.model.SystemConfiguration();
			
			expect(systemConfiguration.get("token")).toBeNull();

		});
		
		/*
		 * creation of a System Configuration with given values
		 */
		it("should be able to create a System Configuration with given values", function() {
		   
			// holds the token for the school configuration
			var token = "ABCDEFG";
			
			// create the test System Configuration that we will use for the test
			var systemConfiguration = new diary.model.SystemConfiguration({	"token" : token });
			
			expect(systemConfiguration.get("token")).toEqual(token);					
		});
			
	});
	
	describe("Unsuccessful validation of System Configuration Suite" , function() {
		
		// holds the SystemConfiguration that is to be used for testing
		var systemConfiguration,
			attrs;
		
		beforeEach(function() {
			
			// holds the token for the school configuration
			var token = "ABCDEFG";
			
			// holds the attributes that are to be assigned during save
			attrs = {"token" : token };
			
			// create the test System Configuration that we will use for the test
			systemConfiguration = new diary.model.SystemConfiguration();
		});
		
		it("should fail when token is not a string", function() {

			// update the attributes for the test
			attrs.token = 123;
			
			expect(systemConfiguration.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("token must be a string");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
	});
		
});