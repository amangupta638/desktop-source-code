/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: DiaryItemSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

// TODO: test isWithinDay()
// TODO: test isWithinTimeFrame()
describe("Diary Item model", function() {

	var model = null,		// test model
		eventSpy = null;	// spy for events

	describe("when created with default values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.DiaryItem();

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'title' to default value", function() {
			expect(model.get("title")).toEqual("(unknown)");
		});

		it("should set 'description' to NULL", function() {
			expect(model.get("description")).toEqual(null);
		});

		it("should set 'attachments' to an empty collection", function() {
			expect(model.get("attachments").toArray()).toEqual(new diary.collection.AttachmentCollection().toArray());
		});

		it("should set 'created' to today", function() {
			expect(model.get("created").toString()).toEqual(new Date().toString());
		});

		describe("when updated with valid values", function() {

			it("should set 'title' to 'test value'", function() {
				model.set("title", "test value");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("title")).toEqual("test value");
			});

			it("should set 'description' to 'test value'", function() {
				model.set("description", "test value");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("description")).toEqual("test value");
			});

			// TODO: add tests for attachments

		});

	});

	describe("when created with valid values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Task({
				"title" : "This is the title",
				"description" : "This is the description."
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'title' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("title")).toEqual("This is the title");
		});

		it("should set 'description' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("description")).toEqual("This is the description.");
		});

		// TODO: add tests for attachments

	});

	describe("when updated with invalid values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Task({
				"title" : "This is the title",
				"description" : "This is the description."
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should require a non-null 'title'", function() {
			model.set("title", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Title must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("title")).toEqual("This is the title");
		});

		it("should require a string 'title'", function() {
			model.set("title", []);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Title must be a valid string");

			// confirm model attr wasn't updated
			expect(model.get("title")).toEqual("This is the title");
		});

		it("should require a non-empty 'title'", function() {
			model.set("title", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Title must be specified and cannot be empty");

			// confirm model attr wasn't updated
			expect(model.get("title")).toEqual("This is the title");
		});

		it("should require a string 'description'", function() {
			model.set("description", []);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Description must be a valid string");

			// confirm model attr wasn't updated
			expect(model.get("description")).toEqual("This is the description.");
		});

		it("should require non-empty 'attachments'", function() {
			model.set("attachments", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Attachments must be a valid collection of Attachments");

			// confirm model attr wasn't updated
			expect(model.get("attachments").toArray()).toEqual(new diary.collection.AttachmentCollection().toArray());
		});

		it("should require a valid 'attachments' collection", function() {
			model.set("attachments", "English");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Attachments must be a valid collection of Attachments");

			// confirm model attr wasn't updated
			expect(model.get("attachments").toArray()).toEqual(new diary.collection.AttachmentCollection().toArray());
		});

	});

});
