/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: TaskSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

// TODO: test initialize()
describe("Task model", function() {

	var model = null,		// test model
		eventSpy = null;	// spy for events

	describe("when created with default values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Task();

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'type' to " + diary.model.DiaryItem.TYPE.ASSIGNMENT, function() {
			expect(model.get("type")).toEqual(diary.model.DiaryItem.TYPE.ASSIGNMENT);
		});

		it("should set 'assignedDay' to current day", function() {
			var today = new Rocketboots.date.Day();
			expect(model.get("assignedDay").getKey()).toEqual(today.getKey());
		});

		it("should set 'assignedTime' to current time", function() {
			var now = new Rocketboots.date.Time();
			expect(model.get("assignedTime").toInteger()).toEqual(now.toInteger());
		});

		it("should set 'dueDay' to tomorrow", function() {
			var tomorrow = new Rocketboots.date.Day().addDays(1);
			expect(model.get("dueDay").getKey()).toEqual(tomorrow.getKey());
		});

		it("should set 'dueTime' to null", function() {
			expect(model.get("dueTime")).toEqual(null);
		});

		it("should set 'completed' to null", function() {
			expect(model.get("completed")).toEqual(null);
		});

		it("should set 'subject' to an empty Subject", function() {
			expect(model.get("subject") instanceof diary.model.Subject).toBeTruthy();
		});

		describe("when updated with valid values", function() {

			it("should set 'type' to 'HOMEWORK'", function() {
				model.set("type", "HOMEWORK");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("type")).toEqual("HOMEWORK");
			});

			it("should set 'type' to 'ASSIGNMENT'", function() {
				model.set("type", "ASSIGNMENT");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("type")).toEqual("ASSIGNMENT");
			});

			it("should set 'assignedDay' to '" + new Rocketboots.date.Day() + "'", function() {
				var today = new Rocketboots.date.Day();
				model.set("assignedDay", today);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("assignedDay").getKey()).toEqual(today.getKey());
			});

			it("should set 'assignedDay' to '" + new Rocketboots.date.Day().addDays(-1) + "'", function() {
				var yesterday = new Rocketboots.date.Day().addDays(-1);
				model.set("assignedDay", yesterday);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("assignedDay").getKey()).toEqual(yesterday.getKey());
			});

			it("should set 'assignedDay' to '" + new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7) + "'", function() {
				var bday = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7);
				model.set("assignedDay", bday);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("assignedDay").getKey()).toEqual(bday.getKey());
			});

			it("should set 'assignedTime' to the current time", function() {
				var now = new Rocketboots.date.Time();
				model.set("assignedTime", now);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("assignedTime").toInteger()).toEqual(now.toInteger());
			});

			it("should set 'assignedTime' to a valid time", function() {
				var time = new Rocketboots.date.Time(12,30);
				model.set("assignedTime", time);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("assignedTime").toInteger()).toEqual(time.toInteger());
			});

			it("should set 'dueDay' to '" + new Rocketboots.date.Day().addDays(1) + "'", function() {
				var tomorrow = new Rocketboots.date.Day().addDays(1);
				model.set("dueDay", tomorrow);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("dueDay").getKey()).toEqual(tomorrow.getKey());
			});

			it("should set 'dueTime' to the current time", function() {
				var now = new Rocketboots.date.Time();
				model.set("dueTime", now);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("dueTime").toInteger()).toEqual(now.toInteger());
			});

			it("should set 'dueTime' to a valid time", function() {
				var time = new Rocketboots.date.Time(12,30);
				model.set("dueTime", time);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("dueTime").toInteger()).toEqual(time.toInteger());
			});

			it("should set 'completed' to '" + new Date() + "'", function() {
				var today = new Date();

				model.set("completed", today);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("completed")).toEqual(today);
			});

			it("should set 'completed' to '" + new Date(1975, 2, 7) + "'", function() {
				var importantDay = new Date(1975, 2, 7);

				model.set("completed", importantDay);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("completed")).toEqual(importantDay);
			});

			it("should set 'subject' to '" + new diary.model.Subject() + "'", function() {
				var subject = new diary.model.Subject();

				model.set("subject", subject);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("subject")).toEqual(subject);
			});

			it("should set 'subject' to '" + new diary.model.Subject({ title: "Test Subject", colour: "Red" }) + "'", function() {
				var subject = new diary.model.Subject({ title: "Test Subject", colour: "Red" });

				model.set("subject", subject);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("subject")).toEqual(subject);
			});

		});

	});

	describe("when created with valid values", function() {

		var assignedDay = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7),
			assignedTime = new Rocketboots.date.Time(13,40),
			dueDay = new Rocketboots.date.Day(2003, Rocketboots.date.Day.AUGUST, 23),
			dueTime = new Rocketboots.date.Time(19,20),
			today = new Date(),
			subject = new diary.model.Subject({ title: "Test Subject", colour: "Red" });

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Task({
				"type" : diary.model.DiaryItem.TYPE.ASSIGNMENT,
				"assignedDay" : assignedDay,
				"assignedTime" : assignedTime,
				"dueDay" : dueDay,
				"dueTime" : dueTime,
				"completed" : today,
				"subject" : subject
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'type' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("type")).toEqual(diary.model.DiaryItem.TYPE.ASSIGNMENT);
		});

		it("should set 'assignedDay' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("assignedDay").getKey()).toEqual(assignedDay.getKey());
		});

		it("should set 'assignedTime' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("assignedTime").toInteger()).toEqual(assignedTime.toInteger());
		});

		it("should set 'dueDay' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("dueDay").getKey()).toEqual(dueDay.getKey());
		});

		it("should set 'dueTime' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("dueTime").toInteger()).toEqual(dueTime.toInteger());
		});

		it("should set 'completed' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("completed")).toEqual(today);
		});

		it("should set 'subject' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("subject")).toEqual(subject);
		});

	});

	describe("when updated with invalid values", function() {

		var assignedDay = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7),
			assignedTime = new Rocketboots.date.Time(13,40),
			dueDay = new Rocketboots.date.Day(2003, Rocketboots.date.Day.AUGUST, 23),
			dueTime = new Rocketboots.date.Time(19,20),
			today = new Date(),
			subject = new diary.model.Subject({ title: "Test Subject", colour: "Red" });

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Task({
				"type" : diary.model.DiaryItem.TYPE.HOMEWORK,
				"assignedDay" : assignedDay,
				"assignedTime" : assignedTime,
				"dueDay" : dueDay,
				"dueTime" : dueTime,
				"completed" : today,
				"subject" : subject
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should require a non-null 'type'", function() {
			model.set("type", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Type must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("type")).toEqual(diary.model.DiaryItem.TYPE.HOMEWORK);
		});

		it("should require a non-empty 'type'", function() {
			model.set("type", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Type must be one of: ASSIGNMENT,HOMEWORK");

			// confirm model attr wasn't updated
			expect(model.get("type")).toEqual(diary.model.DiaryItem.TYPE.HOMEWORK);
		});

		it("should require a valid 'type' value", function() {
			model.set("type", "Student");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Type must be one of: ASSIGNMENT,HOMEWORK");

			// confirm model attr wasn't updated
			expect(model.get("type")).toEqual(diary.model.DiaryItem.TYPE.HOMEWORK);
		});

		it("should require a valid 'type' value", function() {
			model.set("type", "teacher");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Type must be one of: ASSIGNMENT,HOMEWORK");

			// confirm model attr wasn't updated
			expect(model.get("type")).toEqual(diary.model.DiaryItem.TYPE.HOMEWORK);
		});

		it("should require a non-null 'assignedDay'", function() {
			model.set("assignedDay", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Assigned Day must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("assignedDay").getKey()).toEqual(assignedDay.getKey());
		});

		it("should require a non-empty 'assignedDay'", function() {
			model.set("assignedDay", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Assigned Day must be a valid Day");

			// confirm model attr wasn't updated
			expect(model.get("assignedDay").getKey()).toEqual(assignedDay.getKey());
		});

		it("should require a valid 'assignedDay' value", function() {
			model.set("assignedDay", "Student");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Assigned Day must be a valid Day");

			// confirm model attr wasn't updated
			expect(model.get("assignedDay").getKey()).toEqual(assignedDay.getKey());
		});

		it("should require a non-null 'assignedTime'", function() {
			model.set("assignedTime", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Assigned Time must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("assignedTime").toInteger()).toEqual(assignedTime.toInteger());
		});

		it("should require a non-empty 'assignedTime'", function() {
			model.set("assignedTime", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Assigned Time must be a valid Time");

			// confirm model attr wasn't updated
			expect(model.get("assignedTime").toInteger()).toEqual(assignedTime.toInteger());
		});

		it("should require a valid 'assignedTime' value", function() {
			model.set("assignedTime", "Student");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Assigned Time must be a valid Time");

			// confirm model attr wasn't updated
			expect(model.get("assignedTime").toInteger()).toEqual(assignedTime.toInteger());
		});

		it("should require a non-null 'dueDay'", function() {
			model.set("dueDay", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Due Day must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("dueDay")).toEqual(dueDay);
		});

		it("should require a non-empty 'dueDay'", function() {
			model.set("dueDay", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Due Day must be a valid Day");

			// confirm model attr wasn't updated
			expect(model.get("dueDay")).toEqual(dueDay);
		});

		it("should require a valid 'dueDay' value", function() {
			model.set("dueDay", "Student");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Due Day must be a valid Day");

			// confirm model attr wasn't updated
			expect(model.get("dueDay")).toEqual(dueDay);
		});

		it("should require a non-empty 'dueTime'", function() {
			model.set("dueTime", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Due Time must be a valid Time");

			// confirm model attr wasn't updated
			expect(model.get("dueTime").toInteger()).toEqual(dueTime.toInteger());
		});

		it("should require a valid 'dueTime' value", function() {
			model.set("dueTime", "Student");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Due Time must be a valid Time");

			// confirm model attr wasn't updated
			expect(model.get("dueTime").toInteger()).toEqual(dueTime.toInteger());
		});

		it("should require a non-empty 'completed'", function() {
			model.set("completed", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Date Completed must be a valid timestamp");

			// confirm model attr wasn't updated
			expect(model.get("completed")).toEqual(today);
		});

		it("should require a valid 'completed' value", function() {
			model.set("completed", "Student");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Date Completed must be a valid timestamp");

			// confirm model attr wasn't updated
			expect(model.get("completed")).toEqual(today);
		});

		it("should require a non-empty 'subject'", function() {
			model.set("subject", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Subject must be a valid Subject");

			// confirm model attr wasn't updated
			expect(model.get("subject")).toEqual(subject);
		});

		it("should require a valid 'subject' value", function() {
			model.set("subject", "English");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Subject must be a valid Subject");

			// confirm model attr wasn't updated
			expect(model.get("subject")).toEqual(subject);
		});

	});

	describe("when assigned yesterday at 10:00", function() {

		describe("when due tomorrow at 10:30", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-1),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(1),
					"dueTime" : new Rocketboots.date.Time(10, 30)
				});
			});

			// isWithinDay() specs

			it("should be within the day: yesterday", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				expect(model.isWithinDay(day)).toBeTruthy();
			});

			it("should be within the day: tomorrow", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				expect(model.isWithinDay(day)).toBeTruthy();
			});

			it("should not be within the day: two days ago", function() {
				var day = new Rocketboots.date.Day().addDays(-2);
				expect(model.isWithinDay(day)).toBeFalsy();
			});

			it("should not be within the day: today", function() {
				var day = new Rocketboots.date.Day();
				expect(model.isWithinDay(day)).toBeFalsy();
			});

			// isWithinTimeFrame() specs

			it("should be within the time frame: yesterday, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should be within the time frame: yesterday, 09:00-10:01", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(10, 01);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should be within the time frame: yesterday, 10:00-11:00", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(10, 0);
				var endTime = new Rocketboots.date.Time(11, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should not be within the time frame: yesterday, 09:00-10:00", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(10, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be within the time frame: yesterday, 10:20-10:40", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(10, 20);
				var endTime = new Rocketboots.date.Time(10, 40);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should be within the time frame: tomorrow, 10:20-10:40", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(10, 20);
				var endTime = new Rocketboots.date.Time(10, 40);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should be within the time frame: tomorrow, 10:20-10:31", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(10, 20);
				var endTime = new Rocketboots.date.Time(10, 31);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should be within the time frame: tomorrow, 10:30-10:40", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(10, 30);
				var endTime = new Rocketboots.date.Time(10, 40);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should not be within the time frame: tomorrow, 10:20-10:30", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(10, 20);
				var endTime = new Rocketboots.date.Time(10, 30);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be within the time frame: tomorrow, 09:00-10:00", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(10, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be within the time frame: two days ago, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day().addDays(-2);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be within the time frame: today, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day();
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			// isDueWithinTimeFrame() specs

			it("should be due within the time frame: tomorrow", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				expect(model.isDueWithinTimeFrame(day)).toBeTruthy();
			});

			it("should not be due within the time frame: today", function() {
				var day = new Rocketboots.date.Day();
				expect(model.isDueWithinTimeFrame(day)).toBeFalsy();
			});

			it("should not be due within the time frame: two days from now", function() {
				var day = new Rocketboots.date.Day().addDays(2);
				expect(model.isDueWithinTimeFrame(day)).toBeFalsy();
			});

			it("should be due within the time frame: tomorrow, 10:20-10:40", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(10, 20);
				var endTime = new Rocketboots.date.Time(10, 40);
				expect(model.isDueWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should be due within the time frame: tomorrow, 10:20-10:31", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(10, 20);
				var endTime = new Rocketboots.date.Time(10, 31);
				expect(model.isDueWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should be due within the time frame: tomorrow, 10:30-10:40", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(10, 30);
				var endTime = new Rocketboots.date.Time(10, 40);
				expect(model.isDueWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should not be due within the time frame: tomorrow, 10:20-10:30", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(10, 20);
				var endTime = new Rocketboots.date.Time(10, 30);
				expect(model.isDueWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be due within the time frame: two days ago, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day().addDays(-2);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isDueWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be due within the time frame: today, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day();
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isDueWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

		});

		describe("when due tomorrow (no due time)", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-1),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(1)
				});
			});

			// isWithinDay() specs

			it("should be within the day: yesterday", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				expect(model.isWithinDay(day)).toBeTruthy();
			});

			it("should be within the day: tomorrow", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				expect(model.isWithinDay(day)).toBeTruthy();
			});

			it("should not be within the day: two days ago", function() {
				var day = new Rocketboots.date.Day().addDays(-2);
				expect(model.isWithinDay(day)).toBeFalsy();
			});

			it("should not be within the day: today", function() {
				var day = new Rocketboots.date.Day();
				expect(model.isWithinDay(day)).toBeFalsy();
			});

			// isWithinTimeFrame() specs

			it("should be within the time frame: yesterday, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should be within the time frame: yesterday, 09:00-10:01", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(10, 01);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should be within the time frame: yesterday, 10:00-11:00", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(10, 0);
				var endTime = new Rocketboots.date.Time(11, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
			});

			it("should not be within the time frame: yesterday, 09:00-10:00", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(10, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be within the time frame: yesterday, 10:20-10:40", function() {
				var day = new Rocketboots.date.Day().addDays(-1);
				var startTime = new Rocketboots.date.Time(10, 20);
				var endTime = new Rocketboots.date.Time(10, 40);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be within the time frame: tomorrow, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be within the time frame: two days ago, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day().addDays(-2);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be within the time frame: today, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day();
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			// isDueWithinTimeFrame() specs

			it("should be due within the time frame: tomorrow", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				expect(model.isDueWithinTimeFrame(day)).toBeTruthy();
			});

			it("should not be due within the time frame: today", function() {
				var day = new Rocketboots.date.Day();
				expect(model.isDueWithinTimeFrame(day)).toBeFalsy();
			});

			it("should not be due within the time frame: two days from now", function() {
				var day = new Rocketboots.date.Day().addDays(2);
				expect(model.isDueWithinTimeFrame(day)).toBeFalsy();
			});

			it("should not be due within the time frame: tomorrow, 10:20-10:40", function() {
				var day = new Rocketboots.date.Day().addDays(1);
				var startTime = new Rocketboots.date.Time(10, 20);
				var endTime = new Rocketboots.date.Time(10, 40);
				expect(model.isDueWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be due within the time frame: two days ago, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day().addDays(-2);
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isDueWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

			it("should not be due within the time frame: today, 09:00-14:00", function() {
				var day = new Rocketboots.date.Day();
				var startTime = new Rocketboots.date.Time(9, 0);
				var endTime = new Rocketboots.date.Time(14, 0);
				expect(model.isDueWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
			});

		});

	});

	describe("when completed", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Task({
				"assignedDay" : new Rocketboots.date.Day().addDays(-1),
				"assignedTime" : new Rocketboots.date.Time(10, 0),
				"dueDay" : new Rocketboots.date.Day().addDays(1),
				"dueTime" : new Rocketboots.date.Time(10, 30),
				"completed" : new Date()
			});
		});

		it("should appear completed", function() {
			expect(model.isCompleted()).toBeTruthy();
		});

	});

	describe("when not completed", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Task({
				"assignedDay" : new Rocketboots.date.Day().addDays(-1),
				"assignedTime" : new Rocketboots.date.Time(10, 0),
				"dueDay" : new Rocketboots.date.Day().addDays(1),
				"dueTime" : new Rocketboots.date.Time(10, 30),
				"completed" : null
			});
		});

		it("should not appear completed", function() {
			expect(model.isCompleted()).toBeFalsy();
		});

	});

	describe("when due yesterday (no time)", function() {

		describe("when not completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(-1),
					"completed" : null
				});
			});

			it("should appear overdue", function() {
				expect(model.isOverdue()).toBeTruthy();
			});

		});

		describe("when completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(-1),
					"completed" : new Date()
				});
			});

			it("should not appear overdue", function() {
				expect(model.isOverdue()).toBeFalsy();
			});

		});

	});

	describe("when due yesterday at 10:30", function() {

		describe("when not completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(-1),
					"dueTime" : new Rocketboots.date.Time(10, 30),
					"completed" : null
				});
			});

			it("should appear overdue", function() {
				expect(model.isOverdue()).toBeTruthy();
			});

		});

		describe("when completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(-1),
					"dueTime" : new Rocketboots.date.Time(10, 30),
					"completed" : new Date()
				});
			});

			it("should not appear overdue", function() {
				expect(model.isOverdue()).toBeFalsy();
			});

		});

	});

	describe("when due today (no time)", function() {

		describe("when not completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day(),
					"completed" : null
				});
			});

			it("should appear overdue", function() {
				expect(model.isOverdue()).toBeTruthy();
			});

		});

		describe("when completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day(),
					"completed" : new Date()
				});
			});

			it("should not appear overdue", function() {
				expect(model.isOverdue()).toBeFalsy();
			});

		});

	});

	describe("when due today (in an hour)", function() {

		describe("when not completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day(),
					"dueTime" : new Rocketboots.date.Time().addMinutes(60),
					"completed" : null
				});
			});

			it("should not appear overdue", function() {
				expect(model.isOverdue()).toBeFalsy();
			});

		});

		describe("when completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day(),
					"dueTime" : new Rocketboots.date.Time().addMinutes(60),
					"completed" : new Date()
				});
			});

			it("should not appear overdue", function() {
				expect(model.isOverdue()).toBeFalsy();
			});

		});

	});

	describe("when due tomorrow (no time)", function() {

		describe("when not completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(1),
					"completed" : null
				});
			});

			it("should not appear overdue", function() {
				expect(model.isOverdue()).toBeFalsy();
			});

		});

		describe("when completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(1),
					"completed" : new Date()
				});
			});

			it("should not appear overdue", function() {
				expect(model.isOverdue()).toBeFalsy();
			});

		});

	});

	describe("when due tomorrow at 10:30", function() {

		describe("when not completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(1),
					"dueTime" : new Rocketboots.date.Time(10, 30),
					"completed" : null
				});
			});

			it("should not appear overdue", function() {
				expect(model.isOverdue()).toBeFalsy();
			});

		});

		describe("when completed", function() {

			beforeEach(function() {
				// create a new, valid model
				model = new diary.model.Task({
					"assignedDay" : new Rocketboots.date.Day().addDays(-2),
					"assignedTime" : new Rocketboots.date.Time(10, 0),
					"dueDay" : new Rocketboots.date.Day().addDays(1),
					"dueTime" : new Rocketboots.date.Time(10, 30),
					"completed" : new Date()
				});
			});

			it("should not appear overdue", function() {
				expect(model.isOverdue()).toBeFalsy();
			});

		});

	});

});
