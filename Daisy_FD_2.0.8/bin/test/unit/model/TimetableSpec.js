/**
 * Unit Tests for the Timetable Model
 * 
 * author - John Pearce
 */
describe("Timetable - Test Suite", function() {

	var cycleDaysTen = [
			{
				label: "Monday A",
				shortLabel: "MoA"
			},
			{
				label: "Tuesday A",
				shortLabel: "TuA"
			},
			{
				label: "Wednesday A",
				shortLabel: "WeA"
			},
			{
				label: "Thursday A",
				shortLabel: "ThA"
			},
			{
				label: "Friday A",
				shortLabel: "FrA"
			},
			{
				label: "Monday B",
				shortLabel: "MoB"
			},
			{
				label: "Tuesday B",
				shortLabel: "TuB"
			},
			{
				label: "Wednesday B",
				shortLabel: "WeB"
			},
			{
				label: "Thursday B",
				shortLabel: "ThB"
			},
			{
				label: "Friday B",
				shortLabel: "FrB"
			}
		];
	
	/**
	 * Suite of tests which should create a timetable
	 */
	describe("Successful creation of Timetable Suite", function() {
		
		beforeEach(function() {
			
		});
		
		/*
		 * creation of a timetable with default values
		 */
		it("should be able to create a timetable with default values", function() {
		   
			// create the test timetable that we will use for the test
			var timetable = new diary.model.Timetable();
			
			expect(timetable.get("startDay")).toBeNull();
			expect(timetable.get("name")).toBeNull();
			expect(timetable.get("cycleLength")).toEqual(0);
			expect(timetable.get("periods")).not.toBeNull();
			expect(timetable.get("periods").length).toEqual(0);
			expect(timetable.get("classes")).not.toBeNull();
			expect(timetable.get("classes").length).toEqual(0);
			expect(timetable.get("cycleLabel")).toBeNull();
			expect(timetable.get("cycleDays")).toBeNull();

		});
		
		/*
		 * creation of a timetable with given values
		 */
		it("should be able to create a timetable with given values", function() {
		   
			var startDay = new Rocketboots.date.Day(),
				name = "Semester 2",
				periods = new diary.collection.PeriodCollection(),
				classes = new diary.collection.ClassCollection(),
				cycleLength = 10,
				cycleLabel = "test cycle";
			
			// create the test timetable that we will use for the test
			var timetable = new diary.model.Timetable({ "startDay" : new Rocketboots.date.Day(startDay),
														"name" : name,
														"periods" : periods,
														"classes" : classes,
														"cycleLength" : cycleLength,
														"cycleLabel" : cycleLabel,
														"cycleDays" : cycleDaysTen
													});
			
			expect(timetable.get("startDay").getKey()).toEqual(startDay.getKey());
			expect(timetable.get("name")).toEqual(name);
			expect(timetable.get("periods")).toEqual(periods);
			expect(timetable.get("periods").length).toEqual(0);
			expect(timetable.get("classes")).toEqual(classes);
			expect(timetable.get("classes").length).toEqual(0);
			expect(timetable.get("cycleLength")).toEqual(cycleLength);
			expect(timetable.get("cycleLabel")).toEqual(cycleLabel);
			expect(timetable.get("cycleDays")).toEqual(cycleDaysTen);
			
			periods.add(new diary.model.Period({
				'startTime': Rocketboots.date.Time.parseTime(new Date())
			}));
			classes.add(new diary.model.Class({
				'cycleDay': 1
			}));
			
			expect(timetable.get("periods").length).toEqual(1);
			expect(timetable.get("classes").length).toEqual(1);
			
		});
		
	});
	
	/**
	 * Suite of tests to test the methods on timetables
	 */
	describe("Methods of Timetable Suite", function () {
		
		it("subjectAt method should return no results", function () {
			
			var timetable = new diary.model.Timetable({
				'startDay': new Rocketboots.date.Day(),
				'cycleLength': 1
			});
			
			expect(timetable.subjectAt(1, new diary.model.Period())).toBeNull();
			
		});
		
		it("subjectAt method should throw with invalid parameters", function () {
			
			var timetable = new diary.model.Timetable({
				'startDay': new Rocketboots.date.Day(),
				'cycleLength': 1
			});
			
			expect(function () {
				timetable.subjectAt(2, new diary.model.Period());
			}).toThrow("Cycle day must be positive and within cycle length (2,1)");
			
		});
		
		it("subjectAt method should return a result", function () {
			
			var period = new diary.model.Period({
					'id': 1,
					'startTime': Rocketboots.date.Time.parseTime(new Date())
				}),
				timetable = new diary.model.Timetable({
					'startDay': new Rocketboots.date.Day(),
					'cycleLength': 3
				});
			
			// add a single period
			timetable.get('periods').add(period);
			
			// add two classes, both on the same cycle day and period
			timetable.get('classes').add(new diary.model.Class({
				'periodId': 1,
				'cycleDay': 1,
				'subject': new diary.model.Subject({
					'id': 1,
					'title': "Test subject 1",
					'colour': "Test colour 1"
				})
			}));
			timetable.get('classes').add(new diary.model.Class({
				'periodId': 1,
				'cycleDay': 2,
				'subject': new diary.model.Subject({
					'id': 2,
					'title': "Test subject 2",
					'colour': "Test colour 2"
				})
			}));
			
			expect(timetable.subjectAt(1, period).id).toEqual(1);
			expect(timetable.subjectAt(2, period).id).toEqual(2);
			expect(timetable.subjectAt(3, period)).toBeNull();
			
		});
		
		it("periodsForCycleDay should return results", function () {
			
			var timetable = new diary.model.Timetable({
					'startDay': new Rocketboots.date.Day(),
					'cycleLength': 3
				}),
				periodObjects = [
					// standard periods every day
					{
						'startTime': new Rocketboots.date.Time(8,0)
					}, {
						'startTime': new Rocketboots.date.Time(9, 0)
					}, {
						'startTime': new Rocketboots.date.Time(12, 0),
						'endTime': new Rocketboots.date.Time(13, 0)
					},
					// 11am period on 1st and last day of cycle only
					{
						'startTime': Rocketboots.date.Time(11, 0),
						'cycleDay': 1
					}, {
						'startTime': Rocketboots.date.Time(11, 0),
						'cycleDay': 3
					}, 
					// extra 7am period on 1st day of cycle
					{
						'startTime': Rocketboots.date.Time(7, 0),
						'cycleDay': 1
					}
				];
			
			// Add test data periods to timetable
			_.forEach(periodObjects, function (periodObject) {
				timetable.get('periods').add(new diary.model.Period(periodObject));
			});
			
			expect(timetable.periodsForCycleDay(1).length).toEqual(2);
			expect(timetable.periodsForCycleDay(2).length).toEqual(3);
			expect(timetable.periodsForCycleDay(3).length).toEqual(1);
			
		});

		it("should be able to getCycleDayLabel", function () {

			var timetable = new diary.model.Timetable({
				'startDay': new Rocketboots.date.Day(),
				'cycleLength': 10,
				'cycleDays': cycleDaysTen
			});

			expect(timetable.getCycleDayLabel(1)).toEqual("Monday A");
			expect(timetable.getCycleDayLabel(4)).toEqual("Thursday A");
			expect(timetable.getCycleDayLabel(10)).toEqual("Friday B");
		});
		
		it("should be able to getCycleDayShortLabel", function () {

			var timetable = new diary.model.Timetable({
				'startDay': new Rocketboots.date.Day(),
				'cycleLength': 10,
				'cycleDays': cycleDaysTen
			});

			expect(timetable.getCycleDayShortLabel(1)).toEqual("MoA");
			expect(timetable.getCycleDayShortLabel(4)).toEqual("ThA");
			expect(timetable.getCycleDayShortLabel(10)).toEqual("FrB");
		});

	});
	
	/**
	 * Suite of tests which should stop the timetable from being created
	 */
	describe("Unsuccessful creation of Timetable Suite" , function() {
		
		// holds the attributes that are to be used for each test
		var attrs = null;
		// holds the timetable that is to be used for testing
		var timetable = null;
		
		beforeEach(function() {
			// holds the attributes that are to be assigned during save
			attrs = {"startDay": new Rocketboots.date.Day(),
					 "periods" : new diary.collection.PeriodCollection(),
					 "classes" : new diary.collection.ClassCollection(),
					 "cycleLength" : 10,
					 "cycleDays" : cycleDaysTen
			};
			
			// create the test diary item that we will use for the test
			timetable = new diary.model.Timetable();
			timetable.url = "test";
		});
	
		/*
		 * timetable start date not specified
		 */
		it("should fail when timetable start date is not specified", function() {

			// update the attributes for the test
			attrs.startDay = undefined;
			
			expect(timetable.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("No start day specified");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * timetable start date is not date
		 */
		it("should fail when timetable start date is not a date", function() {

			// update the attributes for the test
			attrs.startDay = "30-Mar-1909";
			
			expect(timetable.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Start day must be a valid Day");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * timetable periods must be specified
		 */
		it("should fail when timetable periods is not specified", function () {
			
			attrs.periods = null;
			
			expect(timetable.save(attrs, {
				error : function (model, response) {
					expect(response).toEqual("Periods must be specified");
				}
			})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * timetable periods must be of a period-collection type
		 */
		it("should fail when timetable periods is not a period collection", function () {
			
			attrs.periods = "hello world!";
			
			expect(timetable.save(attrs, {
				error : function (model, response) {
					expect(response).toEqual("Periods must be a collection of periods");
				}
			})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * timetable classes must be specified
		 */
		it("should fail when timetable classes is not specified", function () {
			
			attrs.classes = null;
			
			expect(timetable.save(attrs, {
				error : function (model, response) {
					expect(response).toEqual("Classes must be specified");
				}
			})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * timetable classes must be of a class-collection type
		 */
		it("should fail when timetable classes is not a class collection", function () {
			
			attrs.classes = new Date();
			
			expect(timetable.save(attrs, {
				error : function (model, response) {
					expect(response).toEqual("Classes must be a collection of class objects");
				}
			})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * timetable cycle length not specified
		 */
		it("should fail when timetable cycle length is not specified", function() {

			// update the attributes for the test
			attrs.cycleLength = undefined;
			
			expect(timetable.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("No cycle length specified");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * cycle length is negative
		 */
		it("should fail when timetable cycle length is negative", function() {

			// update the attributes for the test
			attrs.cycleLength = -1;
			
			expect(timetable.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Cycle length must be positive");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * cycle length is negative
		 */
		it("should fail when timetable cycle length is zero", function() {

			// update the attributes for the test
			attrs.cycleLength = 0;
			
			expect(timetable.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Cycle length must be positive");
					 }})).toBeFalsy("Should have failed validation");
			
		});

		it("should fail when cycle length doesn't match cycle days provided", function () {

			attrs.cycleLength = 5;

			expect(timetable.set(attrs, {
				error: function (model, response) {
					expect(response).toEqual("There must be the same number of cycle days as the cycle length");
				}
			})).toBeFalsy("Should have failed validation");
		});

		it("should fail when a cycle day label is missing", function () {

			attrs.cycleDays[3].label = "";

			expect(timetable.set(attrs, {
				error: function (model, response) {
					expect(response).toEqual("Cycle day label for day 4 must be a non-empty string");
				}
			})).toBeFalsy("Should have failed validation");
		});

		it("should fail when a cycle day short label is missing", function () {

			attrs.cycleDays[3].shortLabel = 3;

			expect(timetable.set(attrs, {
				error: function (model, response) {
					expect(response).toEqual("Cycle day short label for day 4 must be a non-empty string");
				}
			})).toBeFalsy("Should have failed validation");
		});
		
		/*
		 * period and classes combination invalid
		 */
// TODO: fix this test
//		it("should fail when timetable periods and classes combination invalid", function() {
//
//			// update the attributes for the test...
//			
//			// add a single period
//			attrs.periods.add(new diary.model.Period({
//				'id': 1,
//				'startTime': new Date(),
//				'endTime': new Date(new Date().getTime() + 60000)
//			}));
//			
//			// add two classes, both on the same cycle day and period
//			attrs.classes.add(new diary.model.Class({
//				'periodId': 1,
//				'cycleDay': 1,
//				'subject': new diary.model.Subject({
//					'title': "Test subject 1",
//					'colour': "Test colour 1"
//				})
//			}));
//			attrs.classes.add(new diary.model.Class({
//				'periodId': 1,
//				'cycleDay': 1,
//				'subject': new diary.model.Subject({
//					'title': "Test subject 2",
//					'colour': "Test colour 2"
//				})
//			}));
//			
//			expect(timetable.save(attrs, {
//				error : function(model, response) {
//					expect(response).toEqual("No cycle day and period combination may contain more than 1 subject");
//				}
//			})).toBeFalsy("Should have failed validation");
//			
//		});
		
		/*
		 * periods matching start times
		 */
// TODO: fix this test
//		it("should fail when timetable periods have same start time on a day", function() {
//
//			// update the attributes for the test...
//			
//			// add overlapping periods
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 14), // 2pm
//				'endTime': new Date(0, 0, 0, 15), // 3pm
//				'cycleDay': 2
//			}));
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 14), // 2pm
//				'endTime': new Date(0, 0, 0, 15) // 3pm
//			}));
//			
//			expect(timetable.save(attrs, {
//				error : function(model, response) {
//					expect(response).toEqual("Periods must be in ascending start time order, without duplicate start times");
//				}
//			})).toBeFalsy("Should have failed validation");
//			
//		});
		
		/*
		 * periods overlapping
		 */
// TODO: fix this test
//		it("should fail when timetable periods overlap on a day", function() {
//			
//			// update the attributes for the test...
//			
//			// add overlapping periods
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 14), // 2pm
//				'endTime': new Date(0, 0, 0, 15) // 3pm
//			}));
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 14, 30), // 2:30pm
//				'endTime': new Date(0, 0, 0, 15, 30), // 3:30pm
//				'cycleDay': 5
//			}));
//			
//			expect(timetable.save(attrs, {
//				error : function(model, response) {
//					expect(response).toEqual("Periods must not overlap");
//				}
//			})).toBeFalsy("Should have failed validation");
//			
//		});
		
		/*
		 * last period must have end time
		 */
// TODO: fix this test
//		it("should fail when timetable last period is missing end time", function() {
//			
//			// update the attributes for the test...
//			
//			// add overlapping periods
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 14, 30) // 2:30pm
//			}));
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 14), // 2pm
//				'endTime': new Date(0, 0, 0, 14, 15) // 2:15pm
//			}));
//			
//			expect(timetable.save(attrs, {
//				error : function(model, response) {
//					expect(response).toEqual("Last period in a given day must have an end time");
//				}
//			})).toBeFalsy("Should have failed validation");
//			
//		});
		
		/*
		 * last period must have end time (again, but with more periods)
		 */
// TODO: fix this test
//		it("should fail when timetable last period on a day is missing end time", function() {
//			
//			// update the attributes for the test...
//			
//			// add normal periods
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 14) // 2pm
//			}));
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 15) // 3pm
//			}));
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 16), // 4pm
//				'endTime': new Date(0, 0, 0, 17) // 5pm
//			}));
//			
//			// add erroneous period at end of a day mid-week
//			attrs.periods.add(new diary.model.Period({
//				'startTime': new Date(0, 0, 0, 17), // 5pm
//				'cycleDay': 8
//			}));
//			
//			expect(timetable.save(attrs, {
//				error : function(model, response) {
//					expect(response).toEqual("Last period in a given day must have an end time");
//				}
//			})).toBeFalsy("Should have failed validation");
//			
//		});
		
	});
		
});