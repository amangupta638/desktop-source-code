/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: NoteSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Note model", function() {

	var model = null,		// test model
		eventSpy = null;	// spy for events

	describe("when created with default values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Note();

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'atDay' to today", function() {
			var today = new Rocketboots.date.Day();
			expect(model.get("atDay").getKey()).toEqual(today.getKey());
		});

		it("should set 'atTime' to NULL", function() {
			expect(model.get("atTime")).toEqual(null);
		});

		it("should set 'entries' to an empty collection", function() {
			expect(model.get("entries").toArray()).toEqual(new diary.collection.NoteEntryCollection().toArray());
		});

		describe("when updated with valid values", function() {

			it("should set 'atDay' to '" + new Rocketboots.date.Day() + "'", function() {
				model.set("atDay", new Rocketboots.date.Day());

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("atDay").getKey()).toEqual(new Rocketboots.date.Day().getKey());
			});

			it("should set 'atDay' to '" + new Rocketboots.date.Day().addDays(1) + "'", function() {
				model.set("atDay", new Rocketboots.date.Day().addDays(1));

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("atDay").getKey()).toEqual(new Rocketboots.date.Day().addDays(1).getKey());
			});

			it("should set 'atDay' to '" + new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7) + "'", function() {
				model.set("atDay", new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7));

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("atDay").getKey()).toEqual(new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7).getKey());
			});

			it("should set 'atTime' to '" + new Rocketboots.date.Time(10, 24) + "'", function() {
				var time = new Rocketboots.date.Time(10, 24);
				model.set("atTime", time);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("atTime").toString()).toEqual(time.toString());
			});

			// TODO: add tests for entries

		});

	});

	describe("when created with valid values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Note({
				"atDay" : new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7),
				"atTime" : new Rocketboots.date.Time(4, 20),
				"entries" : new diary.collection.NoteEntryCollection()
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'atDay' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			var day = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7);
			expect(model.get("atDay").getKey()).toEqual(day.getKey());
		});

		it("should set 'atTime' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			var time = new Rocketboots.date.Time(4, 20);
			expect(model.get("atTime").toString()).toEqual(time.toString());
		});

		it("should set 'entries' to provided value", function() {
			expect(model.get("entries").toArray()).toEqual(new diary.collection.NoteEntryCollection().toArray());
		});

		// TODO: add tests for entries

	});

	describe("when updated with invalid values", function() {
		
		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Note({
				"atDay" : new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7),
				"atTime" : new Rocketboots.date.Time(4, 20),
				"entries" : new diary.collection.NoteEntryCollection()
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should require non-null 'atDay'", function() {
			model.set("atDay", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Day must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			var day = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7);
			expect(model.get("atDay")).toEqual(day);
		});

		it("should require valid 'atDay'", function() {
			model.set("atDay", new Date());

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Day must be a valid Day");

			// confirm model attr wasn't updated
			var day = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7);
			expect(model.get("atDay")).toEqual(day);
		});

		it("should require valid 'atTime'", function() {
			model.set("atTime", new Date());

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Time must be a valid Time");

			// confirm model attr wasn't updated
			var time = new Rocketboots.date.Time(4, 20);
			expect(model.get("atTime").toString()).toEqual(time.toString());
		});

		it("should require non-null 'entries'", function() {
			model.set("entries", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Note Entries must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("entries").toArray()).toEqual(new diary.collection.NoteEntryCollection().toArray());
		});

		it("should require non-empty 'entries'", function() {
			model.set("entries", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Note Entries must be a valid collection of Note Entries");

			// confirm model attr wasn't updated
			expect(model.get("entries").toArray()).toEqual(new diary.collection.NoteEntryCollection().toArray());
		});

		it("should require a valid 'entries' collection", function() {
			model.set("entries", "English");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Note Entries must be a valid collection of Note Entries");

			// confirm model attr wasn't updated
			expect(model.get("entries").toArray()).toEqual(new diary.collection.NoteEntryCollection().toArray());
		});

		// TODO: add tests for entries

	});

	describe("when on yesterday at 10:00", function() {

		var atDay =  new Rocketboots.date.Day().addDays(-1),
			atTime = new Rocketboots.date.Time(10, 0);

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Note({
				"atDay" : atDay,
				"atTime" : atTime,
				"entries" : new diary.collection.NoteEntryCollection()
			});
		});

		// isWithinDay() specs

		it("should be within the day: yesterday", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			expect(model.isWithinDay(day)).toBeTruthy();
		});

		it("should not be within the day: two days ago", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		it("should not be within the day: today", function() {
			var day = new Rocketboots.date.Day();
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		// isWithinTimeFrame() specs

		it("should be within the time frame: yesterday, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
		});

		it("should be within the time frame: yesterday, 09:00-10:01", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(10, 01);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
		});

		it("should be within the time frame: yesterday, 10:00-11:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(10, 0);
			var endTime = new Rocketboots.date.Time(11, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
		});

		it("should not be within the time frame: yesterday, 09:00-10:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(10, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: yesterday, 10:20-10:40", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(10, 20);
			var endTime = new Rocketboots.date.Time(10, 40);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: two days ago, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: today, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day();
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

	});

	describe("when on yesterday (no time)", function() {

		var atDay =  new Rocketboots.date.Day().addDays(-1);

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Note({
				"atDay" : atDay,
				"entries" : new diary.collection.NoteEntryCollection()
			});
		});

		// isWithinDay() specs

		it("should be within the day: yesterday", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			expect(model.isWithinDay(day)).toBeTruthy();
		});

		it("should not be within the day: two days ago", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		it("should not be within the day: today", function() {
			var day = new Rocketboots.date.Day();
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		// isWithinTimeFrame() specs

		it("should not be within the time frame: yesterday, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: two days ago, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: today, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day();
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

	});

});
