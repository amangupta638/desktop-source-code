/**
 * Unit Tests for the Notification Model
 * 
 * author - Brian Bason
 */
describe("Notification - Test Suite", function() {
	
	/**
	 * Suite of tests which should create a notification
	 */
	describe("Successful creation of Notification Suite" , function() {
		
		beforeEach(function() {
			
		});
	
		/*
		 * creation of a notification with default values
		 */
		it("should be able to create a notification with default values", function() {
		   
			// create the test notification that we will use for the test
			var notification = new diary.model.Notification();
			
			expect(notification.get("title")).toBeNull();					
			expect(notification.get("message")).toBeNull();					
			expect(notification.get("type")).toBeNull();		
			
		});
		
		/*
		 * creation of a notification with given values
		 */
		it("should be able to create a notification with given values", function() {
		   
			// holds the title of the notification
			var title = "Notification Message";
			// holds the message
			var message = "The message of the notification goes in here - yeehhhhh";
			
			// create the test notification that we will use for the test
			var notification = new diary.model.Notification({	"title" : title,
															"message" : message,
															"type" : diary.model.Notification.NotificationType.REMINDER
										  		  		  });
			
			expect(notification.get("title")).toEqual(title);					
			expect(notification.get("message")).toEqual(message);					
			expect(notification.get("type")).toEqual(diary.model.Notification.NotificationType.REMINDER);
			
		});
		
	});
	
	/**
	 * Suite of tests which should stop the notification from being created
	 */
	describe("Unsuccessful creation of notification Suite" , function() {
		
		// holds the attributes that are to be used for each test
		var attrs;
		// holds the notification that is to be used for testing
		var notification = null;
		
		beforeEach(function() {
			// holds the attributes that are to be assigned during save
			attrs = {"title" : "Test notification",
					 "message" : "This is a test message for a notification",
					 "type" : diary.model.Notification.NotificationType.INFO};
			
			// create the test notification that we will use for the test
			notification = new diary.model.Notification();
			notification.url = "test";
		});
	
		/*
		 * notification title not specified
		 */
		it("should fail when notification title is not specified", function() {

			// update the attributes for the test
			attrs.title = undefined;
			
			expect(notification.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Title must be specified, title cannot be NULL");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * notification title is not string
		 */
		it("should fail when notification title is not a string", function() {

			// update the attributes for the test
			attrs.title = 1;
			
			expect(notification.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Title must be of type string");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * notification message not specified
		 */
		it("should fail when notification message is not specified", function() {

			// update the attributes for the test
			attrs.message = undefined;
			
			expect(notification.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Message must be specified, message cannot be NULL");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * notification message is not string
		 */
		it("should fail when notification message is not a string", function() {

			// update the attributes for the test
			attrs.message = 1;
			
			expect(notification.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Message must be of type string");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * notification type not specified
		 */
		it("should fail when notification type is not specified", function() {

			// update the attributes for the test
			attrs.type = undefined;
			
			expect(notification.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Type must be specified, type cannot be NULL");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * notification type is not valid
		 */
		it("should fail when type is not one of the valid types", function() {

			// update the attributes for the test
			attrs.type = "INVALID";
			
			expect(notification.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Type is not valid, type must be one of [" + diary.model.Notification.NotificationType.REMINDER + "," + diary.model.Notification.NotificationType.INFO + "," + diary.model.Notification.NotificationType.HOMEWORK + "," + diary.model.Notification.NotificationType.ASSIGNMENT + "]");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
	});
		
});