/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: MessageSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Message model", function() {

	var model = null,		// test model
		eventSpy = null;	// spy for events

	describe("when created with default values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Message();

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'description' to default value", function() {
			expect(model.get("description")).toEqual("(unknown)");
		});

		it("should set 'atDay' to today", function() {
			var today = new Rocketboots.date.Day();
			expect(model.get("atDay").getKey()).toEqual(today.getKey());
		});

		it("should set 'atTime' to NULL", function() {
			expect(model.get("atTime")).toEqual(null);
		});

		it("should set 'messageFrom' to default value", function() {
			expect(model.get("messageFrom")).toEqual(diary.model.Message.FROM.TEACHER);
		});

		it("should set 'messageFromText' to '(unknown)'", function() {
			expect(model.get("messageFromText")).toEqual('(unknown)');
		});

		it("should set 'responseRequired' to false", function() {
			expect(model.get("responseRequired")).toBeFalsy();
		});

		describe("when updated with valid values", function() {

			it("should set 'title' to 'Mrs. Smith'", function() {
				model.set("title", "Mrs. Smith");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("title")).toEqual("Mrs. Smith");
			});

			it("should set 'title' to '123'", function() {
				model.set("title", "123");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("title")).toEqual("123");
			});

			it("should set 'description' to 'Mrs. Smith'", function() {
				model.set("description", "Mrs. Smith");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("description")).toEqual("Mrs. Smith");
			});

			it("should set 'description' to '123'", function() {
				model.set("description", "123");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("description")).toEqual("123");
			});

			it("should set 'atDay' to '" + new Rocketboots.date.Day() + "'", function() {
				model.set("atDay", new Rocketboots.date.Day());

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("atDay").getKey()).toEqual(new Rocketboots.date.Day().getKey());
			});

			it("should set 'atDay' to '" + new Rocketboots.date.Day().addDays(1) + "'", function() {
				model.set("atDay", new Rocketboots.date.Day().addDays(1));

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("atDay").getKey()).toEqual(new Rocketboots.date.Day().addDays(1).getKey());
			});

			it("should set 'atDay' to '" + new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7) + "'", function() {
				model.set("atDay", new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7));

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("atDay").getKey()).toEqual(new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7).getKey());
			});

			it("should set 'atTime' to '" + new Rocketboots.date.Time(10, 24) + "'", function() {
				var time = new Rocketboots.date.Time(10, 24);
				model.set("atTime", time);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("atTime").toString()).toEqual(time.toString());
			});

			it("should set 'messageFrom' to 'PARENT'", function() {
				model.set("messageFrom", "PARENT");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("messageFrom")).toEqual("PARENT");
			});

			it("should set 'messageFrom' to 'TEACHER'", function() {
				model.set("messageFrom", "TEACHER");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("messageFrom")).toEqual("TEACHER");
			});

			it("should set 'messageFromText' to 'Mrs. Smith'", function() {
				model.set("messageFromText", "Mrs. Smith");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("messageFromText")).toEqual("Mrs. Smith");
			});

			it("should set 'messageFromText' to '123'", function() {
				model.set("messageFromText", "123");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("messageFromText")).toEqual("123");
			});

			it("should set 'responseRequired' to 'true'", function() {
				model.set("responseRequired", true);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("responseRequired")).toBeTruthy();
			});

			it("should set 'responseRequired' to 'false'", function() {
				model.set("responseRequired", false);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("responseRequired")).toBeFalsy();
			});

		});

	});

	describe("when created with valid values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Message({
				"title" : "Your Mom",
				"description" : "This is the message.",
				"atDay" : new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7),
				"atTime" : new Rocketboots.date.Time(4, 20),
				"messageFrom" : diary.model.Message.FROM.PARENT,
				"messageFromText" : "Mrs. Jones",
				"responseRequired" : true
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'title' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("title")).toEqual("Your Mom");
		});

		it("should set 'description' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("description")).toEqual("This is the message.");
		});

		it("should set 'atDay' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			var day = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7);
			expect(model.get("atDay").getKey()).toEqual(day.getKey());
		});

		it("should set 'atTime' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			var time = new Rocketboots.date.Time(4, 20);
			expect(model.get("atTime").toString()).toEqual(time.toString());
		});

		it("should set 'messageFrom' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("messageFrom")).toEqual(diary.model.Message.FROM.PARENT);
		});

		it("should set 'messageFromText' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("messageFromText")).toEqual("Mrs. Jones");
		});

		it("should set 'responseRequired' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("responseRequired")).toBeTruthy();
		});

	});

	describe("when updated with invalid values", function() {
		var validMessageFromValues = [];

		for (var type in diary.model.Message.FROM) {
			validMessageFromValues.push(type);
		}

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Message({
				"title" : "Your Mom",
				"description" : "This is the message.",
				"atDay" : new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7),
				"atTime" : new Rocketboots.date.Time(4, 20),
				"messageFrom" : diary.model.Message.FROM.PARENT,
				"messageFromText" : "Mrs. Jones",
				"responseRequired" : true
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should require non-null 'title'", function() {
			model.set("title", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Attention must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("title")).toEqual("Your Mom");
		});

		it("should require a string 'title'", function() {
			model.set("title", []);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Attention must be a valid string");

			// confirm model attr wasn't updated
			expect(model.get("title")).toEqual("Your Mom");
		});

		it("should require non-empty 'title'", function() {
			model.set("title", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Attention must be specified and cannot be empty");

			// confirm model attr wasn't updated
			expect(model.get("title")).toEqual("Your Mom");
		});

		it("should require non-null 'description'", function() {
			model.set("description", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("description")).toEqual("This is the message.");
		});

		it("should require a string 'description'", function() {
			model.set("description", []);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message must be a valid string");

			// confirm model attr wasn't updated
			expect(model.get("description")).toEqual("This is the message.");
		});

		it("should require non-empty 'description'", function() {
			model.set("description", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message must be specified and cannot be empty");

			// confirm model attr wasn't updated
			expect(model.get("description")).toEqual("This is the message.");
		});

		it("should require non-null 'atDay'", function() {
			model.set("atDay", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Day must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			var day = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7);
			expect(model.get("atDay")).toEqual(day);
		});

		it("should require valid 'atDay'", function() {
			model.set("atDay", new Date());

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Day must be a valid Day");

			// confirm model attr wasn't updated
			var day = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7);
			expect(model.get("atDay")).toEqual(day);
		});

		it("should require valid 'atTime'", function() {
			model.set("atTime", new Date());

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Time must be a valid Time");

			// confirm model attr wasn't updated
			var time = new Rocketboots.date.Time(4, 20);
			expect(model.get("atTime").toString()).toEqual(time.toString());
		});

		it("should require non-null 'messageFrom'", function() {
			model.set("messageFrom", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message From must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("messageFrom")).toEqual(diary.model.Message.FROM.PARENT);
		});

		it("should require non-empty 'messageFrom'", function() {
			model.set("messageFrom", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message From must be one of: " + validMessageFromValues);

			// confirm model attr wasn't updated
			expect(model.get("messageFrom")).toEqual(diary.model.Message.FROM.PARENT);
		});

		it("should require a valid 'messageFrom' value", function() {
			model.set("messageFrom", "Student");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message From must be one of: " + validMessageFromValues);

			// confirm model attr wasn't updated
			expect(model.get("messageFrom")).toEqual(diary.model.Message.FROM.PARENT);
		});

		it("should require a valid 'messageFrom' value", function() {
			model.set("messageFrom", "teacher");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message From must be one of: " + validMessageFromValues);

			// confirm model attr wasn't updated
			expect(model.get("messageFrom")).toEqual(diary.model.Message.FROM.PARENT);
		});

		it("should require non-null 'messageFromText'", function() {
			model.set("messageFromText", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message From Text must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("messageFromText")).toEqual("Mrs. Jones");
		});

		it("should require a string 'messageFromText'", function() {
			model.set("messageFromText", []);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message From Text must be a valid string");

			// confirm model attr wasn't updated
			expect(model.get("messageFromText")).toEqual("Mrs. Jones");
		});

		it("should require non-empty 'messageFromText'", function() {
			model.set("messageFromText", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Message From Text must be specified and cannot be empty");

			// confirm model attr wasn't updated
			expect(model.get("messageFromText")).toEqual("Mrs. Jones");
		});

		it("should require non-null 'responseRequired'", function() {
			model.set("responseRequired", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Response Required must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("responseRequired")).toBeTruthy();
		});

		it("should require non-empty 'responseRequired'", function() {
			model.set("responseRequired", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Response Required must be either 'true' or 'false'");

			// confirm model attr wasn't updated
			expect(model.get("responseRequired")).toBeTruthy();
		});

	});

	describe("when on yesterday at 10:00", function() {

		var atDay =  new Rocketboots.date.Day().addDays(-1),
			atTime = new Rocketboots.date.Time(10, 0);

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Message({
				"title" : "Your Mom",
				"description" : "This is the message.",
				"atDay" : atDay,
				"atTime" : atTime,
				"messageFrom" : diary.model.Message.FROM.PARENT,
				"messageFromText" : "Mrs. Jones"
			});
		});

		// isWithinDay() specs

		it("should be within the day: yesterday", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			expect(model.isWithinDay(day)).toBeTruthy();
		});

		it("should not be within the day: two days ago", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		it("should not be within the day: today", function() {
			var day = new Rocketboots.date.Day();
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		// isWithinTimeFrame() specs

		it("should be within the time frame: yesterday, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
		});

		it("should be within the time frame: yesterday, 09:00-10:01", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(10, 01);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
		});

		it("should be within the time frame: yesterday, 10:00-11:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(10, 0);
			var endTime = new Rocketboots.date.Time(11, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
		});

		it("should not be within the time frame: yesterday, 09:00-10:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(10, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: yesterday, 10:20-10:40", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(10, 20);
			var endTime = new Rocketboots.date.Time(10, 40);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: two days ago, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: today, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day();
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

	});

	describe("when on yesterday (no time)", function() {

		var atDay =  new Rocketboots.date.Day().addDays(-1);

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Message({
				"title" : "Your Mom",
				"description" : "This is the message.",
				"atDay" : atDay,
				"atTime" : null,
				"messageFrom" : diary.model.Message.FROM.PARENT,
				"messageFromText" : "Mrs. Jones"
			});
		});

		// isWithinDay() specs

		it("should be within the day: yesterday", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			expect(model.isWithinDay(day)).toBeTruthy();
		});

		it("should not be within the day: two days ago", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		it("should not be within the day: today", function() {
			var day = new Rocketboots.date.Day();
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		// isWithinTimeFrame() specs

		it("should not be within the time frame: yesterday, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: two days ago, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: today, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day();
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

	});

});
