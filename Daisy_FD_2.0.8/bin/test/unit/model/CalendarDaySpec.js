/**
 * Unit Tests for the Calendar Day Model
 * 
 * author - Brian Bason
 */
describe("Calendar Day - Test Suite", function() {
	
	describe("Successful creation of Calendar Day Suite" , function() {
	
		it("should be able to create a calendar day with just the date", function() {
		   
			// holds the date of the calendar that is being tested
			// NOTE - The date is going to be set to 1st Jan 1970 which
			// will be have the following details
			// isWeekend = false
			// weekDay = Thursday
			// dayOfMonth = 1
			// month = 1
			var calendarDay = new Rocketboots.date.Day(1970, Rocketboots.date.Day.JANUARY, 1);
			
			// create the test calendar that we will use for the test
			var day = new diary.model.CalendarDay({ 'day' : calendarDay });
			
			expect(day.get("id")).toEqual(calendarDay.getTime());
			expect(day.get("day").getKey()).toEqual(calendarDay.getKey());
			expect(day.get("cycle")).toBeNull();					
			expect(day.get("cycleDay")).toBeNull();
			expect(day.get("title")).toBeNull();	
			expect(day.get("isGreyedOut")).toBeFalsy();				
			expect(day.get("isWeekend")).toBeFalsy();					
			expect(day.get("weekday")).toEqual("Thursday");					
			expect(day.get("dayOfMonth")).toEqual(1);					
			expect(day.get("month")).toEqual(1);
			expect(day.get("timetable")).toBeNull();
			
		});
		
		it("should be able to create a calendar day with given values", function() {
		   
			// holds the date of the calendar that is being tested
			// NOTE - The date is going to be set to 1st Jan 1970 which
			// will be have the following details
			// isWeekend = false
			// weekDay = Thursday
			// dayOfMonth = 1
			// month = 1
			var calendarDay = new Rocketboots.date.Day(1970, Rocketboots.date.Day.JANUARY, 1), 
				cycleDay = 3;

			
			// create the test task that we will use for the test
			var day = new diary.model.CalendarDay({ "day" : calendarDay,
													"cycle" : 2,
													"cycleDay" : cycleDay,
													"title" : "Test Title",
													"isGreyedOut" : true,
													"timetable" : new diary.model.Timetable() });
			
			expect(day.get("id")).toEqual(calendarDay.getTime());	
			expect(day.get("day").getKey()).toEqual(calendarDay.getKey());			
			expect(day.get("cycle")).toEqual(2);					
			expect(day.get("cycleDay")).toEqual(cycleDay);
			expect(day.get("title")).toEqual("Test Title");
			expect(day.get("isGreyedOut")).toBeTruthy();				
			expect(day.get("isWeekend")).toBeFalsy();					
			expect(day.get("weekday")).toEqual("Thursday");					
			expect(day.get("dayOfMonth")).toEqual(1);					
			expect(day.get("month")).toEqual(1);
			expect(day.get("timetable") instanceof diary.model.Timetable).toBeTruthy();
			
		});
			
	});
	
	describe("Unsuccessful creation of Calendar Day Suite" , function() {
		
		it("should not be able to create a calendar day with no date", function() {
			   
			// the test function
			var testFunction = function() {
				new diary.model.CalendarDay();
			};
			
			// this should throw an exception
			expect(testFunction).toThrow("Day must be specified, date is not optional");	
		});
		
	});
		
});