function editPage(htmlId) {
	htmlId = htmlId || 'generated_html';
	$('body').append('<div id="' + htmlId + '"></div>');

	$('#' + htmlId).append('<form id="survey_edit_form" action="save" method="POST" class="survey_edit_form">' +
			'<input id="name_input" value="Survey Name">' +
			'<input id="description_textarea" value="This is a test survey">' +
			'<input id="description_textarea" value="This is a test survey">' +
			'<input name="options" id="hidden_options" type="hidden">' +
			'<input name="surveyId" type="hidden" value="32">' +
			'<select id="theme_selector">' +
			'<option value="Default">Default</option>' +
			'<option value="Charcoal">Default</option>' +
			'<option value="Default">Default</option>' +
			'</select>' +
			'<select id="theme_selector">' +
			'<option value="Default">Default</option>' +
			'<option value="Charcoal">Default</option>' +
			'<option value="Default">Default</option>' +
			'</select>' +
			'<select id="page_mode_selector">' +
			'<option value="oneQuestionPerPage">One Question Per Page</option>' +
			'<option value="multiPage" disabled="disabled">Multi-Page</option>' +
			'<option value="singlePage" disabled="disabled">Single Page</option>' +
			'</select>' +
			'<select id="transition_selector">' +
			'<option value="none">None</option>' +
			'<option value="slideLeft" disabled="disabled">Slide Left</option>' +
			'<option value="slideRight" disabled="disabled">Slide Right</option>' +
			'<option value="fade" disabled="disabled">Fade</option>' +
			'</select>' +
			'<input id="redirect_input" type="text" value="http://" />' +
			'</form>');

	$('#' + htmlId).append('<select id="add_question_selector">' +
			'<option value="0">Add New Question...</option>' +
			'<option value="textfield">Text Field</option>' +
			'<option value="paragraph">Paragraph</option>' +
			'<option value="checkbox">Checkboxes</option>' +
			'<option value="radio">Radio</option>' +
			'<option value="select">Select List</option>' +
			'<option value="information">Information</option>' +
			'</select>');

	$('#' + htmlId).append('<div id="form_div"></div>');

	$('#' + htmlId).append('<ul>' +
			'<li id="save_link"><a href="#">Save</a></li>' +
			'<li id="cancel_link"><a href="#">Cancel</a></li>' +
			'<li id="preview_link"><a href="#">Preview</a></li>' +
			'<li><a class="toggle_show_hide_all" href="#">Show All</a></li>' +
			'<li><a class="toggle_show_hide_all hide" href="#">Hide All</a></li>' +
			'</ul>');

	$('#' + htmlId).append('<div id="cancel_dialog" title="Unsaved changes" style="display:none;">' +
			'<span>Are you sure you want to leave this page?</span>' +
			'</div>');
}

