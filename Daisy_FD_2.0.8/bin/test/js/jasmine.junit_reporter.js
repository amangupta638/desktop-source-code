(function() {
    
    if (! jasmine) {
        throw new Exception("jasmine library does not exist in global namespace!");
    }
    
    function elapsed(startTime, endTime) {
        return (endTime - startTime)/1000;
    }
    
    function ISODateString(d) {
        function pad(n) { return n < 10 ? '0'+n : n; }
        
        return d.getFullYear() + '-'
            + pad(d.getMonth()+1) +'-'
            + pad(d.getDate()) + 'T'
            + pad(d.getHours()) + ':'
            + pad(d.getMinutes()) + ':'
            + pad(d.getSeconds());
    }
    
    /**
     * Generates JUnit XML for the given spec run.
     * Allows the test results to be used in java based CI
     * systems like CruiseControl and Hudson.
     */
    var JUnitXmlReporter = function() {
        this.started = false;
        this.finished = false;
        this.reports = [];
    };
    
    JUnitXmlReporter.prototype = {
        reportRunnerResults: function(runner) {
            this.finished = true;
        },
        
        reportRunnerStarting: function(runner) {
            this.started = true;
        },
        
        reportSpecResults: function(spec) {
            spec.endTime = new Date();
        },
        
        reportSpecStarting: function(spec) {
            spec.startTime = new Date();
            
            if (! spec.suite.startTime) {
                spec.suite.startTime = new Date();
            }
        },
        
        reportSuiteResults: function(suite) {
            var output = [],
                fileName = 'TEST-' + suite.description.replace(/\s/g, '') + '.xml',
                results = suite.results(),
                items = results.getItems(),
                item,
                spec,
                expectedResults,
                trace,
                i,
                j,
                failedCount = 0,	// ADDED AS PART OF FIX
                passedCount = 0,	// ADDED AS PART OF FIX
                totalCount = 0;		// ADDED AS PART OF FIX
                
            if (!suite.startTime) {
            	suite.startTime = new Date();
            }
            
            suite.endTime = new Date();
            
            /*
             * FIX To count the number of test cases rather than the assertions (expects)
             * being done by jasmine.  Eventually we might need to update the code to also
             * group together the suites that are related.
             * 
             * author - Brian Bason
             */
            for (key in items) {
            	// if the case has failed or been skipped than we will consider
            	// it as failed otherwise we will count it as passed
            	if (!items[key].skipped && items[key].failedCount == 0) {
            		passedCount++;
            	} else {
            		failedCount++;
            	};
            	
            	// increment the count
            	totalCount++;
            };
            
            output.push('<?xml version="1.0" encoding="UTF-8" ?>');
            output.push('<testsuite name="' + suite.description + '" errors="0" failures="' 
                + failedCount + '" tests="' + totalCount + '" time="' 
                + elapsed(suite.startTime, suite.endTime) + '" timestamp="' + ISODateString(suite.startTime) + '">');
            
            for (i = 0; i < items.length; i++) {
                item = items[i];
                spec = suite.specs()[i];
                
                if (spec) {
                	
	                output.push(' <testcase classname="' + suite.description + '" name="' 
	                    + item.description + '" time="' + elapsed(spec.startTime, spec.endTime) + '">');
	                
	                if (!item.passed()) {
	                    expectedResults = item.getItems();
	                    
	                    for (j = 0; j < expectedResults.length; j++) {
	                        trace = expectedResults[j].trace;
	                        
	                        if (trace instanceof Error) {
	                            output.push('<failure>' + trace.message + '</failure>');
	                            break;
	                        }
	                    }
	                }
	                
	                output.push('</testcase>');
                }
            }
            
            output.push('</testsuite>');
            
            this.reports.push({
                "name": suite.description,
                "results": {
                    "passed": passedCount,
                    "failed": failedCount,
                    "total": totalCount
                },
                "filename": 'TEST-' + suite.description.replace(/\s/g, '') + '.xml',
                "text": output.join('')
            });
        },
        
        log: function(str) {
            var console = jasmine.getGlobal().console;
            
            if (console && console.log) {
                console.log(str);
            }
        }
    };
    
    // export public
    jasmine.JUnitXmlReporter = JUnitXmlReporter;
})();