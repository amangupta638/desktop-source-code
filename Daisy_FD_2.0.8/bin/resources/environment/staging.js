Environment = {
    "name": "Staging",
    "debug": false,
    "remoting": {
		"serverUrl": "http://www.e-planner.com.au",
		"updateUrl": "http://www.e-planner.com.au/client/update.xml"
    }
};
