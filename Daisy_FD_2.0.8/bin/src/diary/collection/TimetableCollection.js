/**
 * The model that represents a collection of TIMETABLEs in a application.
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.TimetableCollection = (function() {

	/**
	 * Creates a collection of Timetables
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

		initialize: function () {
			this.on("change:startDay", function () {this.sort();}, this);
		},
		
		model : diary.model.Timetable,
		
		/**
		 * The function that is used to keep the collection sorted.  The timetable collection
		 * will be sorted by the start date of the timetable so that we will have the list of timetables
		 * sorted in the natural order
		 * 
		 * @param {diary.model.Timetable} timetable The timetable that is to be sorted in the collection
		 * 
		 * @return {integer} The timestamp of the start date that represents the timetable
		 */
		comparator : function(timetable) {
			// the sort field is the start date of the timetable
			return timetable.get('startDay').getKey();
		},

		/**
		 * Get the timetable which is current at the date specified.
		 * @param date {!Rocketboots.date.Day} The day at which the returned timetable will be current. May return null.
		 */
		getCurrentAt : function (day) {
			if (this.length == 0) return null;

			var currentTimetable = null;

			// find the last timetable (since they are ordered) with a start date before the date given
			this.each(function (timetable) {
				if(currentTimetable == null){
					if (timetable.get('startDay').getKey() <= day.getKey()
								&& timetable.get('endDay').getKey() >= day.getKey()) {
						currentTimetable = timetable;
					} else if (timetable.get('startDay').getKey() > day.getKey()) {
						currentTimetable = timetable;
					}
				}
			});

			return currentTimetable;
		},
		
		getNextSemester : function (day) {
			if (this.length == 0) return null;

			var currentTimetable = null;

			// find the last timetable (since they are ordered) with a start date before the date given
			this.each(function (timetable) {
				if(currentTimetable == null){
					if (timetable.get('startDay').getKey() <= day.getKey()
							&& timetable.get('endDay').getKey() >= day.getKey()) {
						currentTimetable = timetable;
					} else if (timetable.get('startDay').getKey() > day.getKey()) {
						currentTimetable = timetable;
					}
				}
			});

			return currentTimetable;
		},
		
		getSemesterAt : function (name) {
			if (this.length == 0) return null;

			var currentTimetable = null;

			// find the last timetable (since they are ordered) with a start date before the date given
			this.each(function (timetable) {
				
				 if(timetable.get('name')== name) 
					currentTimetable = timetable;
				
			});

			return currentTimetable;
		}
	
	});
	
})();