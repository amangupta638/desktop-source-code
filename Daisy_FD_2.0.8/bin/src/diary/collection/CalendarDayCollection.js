/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: CalendarDayCollection.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.collection = diary.collection || {};

(function() {
	"use strict";

	/** @class */
	diary.collection.CalendarDayCollection = Backbone.Collection.extend(
		/** @lends diary.collection.CalendarDayCollection.prototype */
		{
			/** @constructs */
			initialize : function() {
				this.on("change:day", function() { this.sort(); }, this);
			},

			model : diary.model.CalendarDay,

			/**
			 * The function that is used to keep the collection sorted.
			 *
			 * @param {!diary.model.CalendarDay} calendarDay The day that is to be sorted in the collection
			 *
			 * @return {string} A sortable string representing the day
			 */
			comparator : function(calendarDay) {
				return calendarDay.get('day').getKey();
			}
		}
	);

})();
