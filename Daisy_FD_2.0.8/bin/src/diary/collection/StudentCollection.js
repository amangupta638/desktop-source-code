/**
 * The model that represents a collection of students
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.StudentCollection = (function() {

	/**
	 * Creates a collection of Classes
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

	
		
		model : diary.model.Student,
		
	
	});
	
})();