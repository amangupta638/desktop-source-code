/**
 * The model that represents a collection of reports
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.ReportCollection = (function() {

	/**
	 * Creates a collection of reports
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

		model : diary.model.Report,
		
		isReportTabActive: function() {
			var isTeacher = false;
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				isTeacher = true;
			}
			var reports = _.filter(this.toArray(), function(report) {
				return report && report.get('isSchoolActive') == 1 && report.get('isReportActive') == 1
						&& ((isTeacher && report.get('isTeacher') == 1) || (!isTeacher && report.get('isStudent') == 1));
			});
			
			console.log('ReportCollection : ' + reports.length);
			return reports;
		},
		
		getActiveReports: function() {
			var isTeacher = false;
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				isTeacher = true;
			}
			var reports = _.filter(this.toArray(), function(report) {
				return report && report.get('isSchoolActive') == 1 && report.get('isReportActive') == 1
						&& ((isTeacher && report.get('isTeacher') == 1) || (!isTeacher && report.get('isStudent') == 1));
			});
			
			//alert('ReportCollection : ' + reports.length);
			return new diary.collection.DiaryItemCollection(reports);
		}
	
	});
	
})();