/**
 * The model that represents a collection of Content Packages in a manifest file.
 * 
 * author - Phil Haeusler
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.ContentPackageCollection = (function() {

	/**
	 * Creates a collection of Content Items
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({
		model: diary.model.ContentPackage
	});
	
})();