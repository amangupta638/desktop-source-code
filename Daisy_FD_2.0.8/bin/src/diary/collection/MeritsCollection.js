/**
 * The model that represents a collection of class and user for my class.
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.MeritsCollection = (function() {

	/**
	 * Creates a collection of Merits
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({
		model : diary.model.MeritType
	});
	
})();