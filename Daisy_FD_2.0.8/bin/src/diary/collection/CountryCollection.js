/**
 * The model that represents a collection of country
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.CountryCollection = (function() {

	/**
	 * Creates a collection of Countries
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({
		model : diary.model.Country,
	});
	
})();