var diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.InboxItemCollection = (function() {

	return Backbone.Collection.extend({
		
		initialize: function () {
			this.on("change:sentDate", function () {this.sort();}, this);
		},
		
		model: diary.model.InboxItem,

		getMessagess: function() {
			var inboxItems = _.filter(this.toArray(), function(inboxItem) {
				return inboxItem && inboxItem.get('type') != 'News' && inboxItem.get('type') != 'Announcement';
			});
			return new diary.collection.InboxItemCollection(inboxItems);
		},
		
		getUnreadCount: function() {
			var now = new Date();
		    var month = now.getMonth();
		    var calendar = app.model.get(ModelName.CALENDAR);
		 	var calendarDays = calendar.getMonthDays(month+1,now.getFullYear()).toArray().reverse() ;
		 	if(month != 0){
		 		calendarDays = calendarDays.concat(calendar.getMonthDays(month,now.getFullYear()).toArray().reverse());
		 	}
		 	var localDate = null;
		 	if(calendarDays.length > 0){
		 		localDate = calendarDays[calendarDays.length-1].get('day');
		 	}
		 	
		 	if (app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat') == "USA"){
		 		if(month > 0){
		 			month = month -1;
		 		}
			 	localDate = new Rocketboots.date.Day(now.getFullYear(),month,1);
		 	}
					
			var inboxItems = _.filter(this.toArray(), function(inboxItem) {
				return inboxItem && (inboxItem.get('type') == 'News' || inboxItem.get('type') == 'Announcement')
						&& inboxItem.get('readStatus') == 0
						&& inboxItem.getUnreadNotificationCount(inboxItem.get('sentDate'),localDate);
			});
			return inboxItems.length;
		},
		
		getUnreadNotifications: function() {
			var inboxItems = _.filter(this.toArray(), function(inboxItem) {
				return inboxItem && (inboxItem.get('type') == 'News' || inboxItem.get('type') == 'Announcement')
						&& inboxItem.get('readStatus') == 0
						&& inboxItem.isAssignedToday(inboxItem.get('sentDate'));
			});
			return inboxItems.length;
		},
		
		getInboxItems: function() {
			var inboxItems = _.filter(this.toArray(), function(inboxItem) {
				return inboxItem && inboxItem.get('parentId') == 0 
				&& inboxItem.get('deletedStatus') == 0;
			});
			return new diary.collection.InboxItemCollection(inboxItems);
		},
		
		getFilterredInboxItems: function() {
			
			var isNews = false;
			var isAnnouncement = false;
			var isMessage = false;
			
			if($('#inbox_filter_news').attr('checked')) {
				isNews = true;
			}
			
			if($('#inbox_filter_announcement').attr('checked')) {
				isAnnouncement = true;
			}
			
			if($('#inbox_filter_message').attr('checked')) {
				isMessage = true;
			}
			
			var inboxItems;
			
			if(isNews || isAnnouncement || isMessage){
				inboxItems = _.filter(this.toArray(), function(inboxItem) {
					
					if(isNews && isAnnouncement && isMessage){
						return inboxItem && inboxItem.get('parentId') == 0 
								&& inboxItem.get('deletedStatus') == 0;
					} else if(isAnnouncement && isMessage){
						return inboxItem && inboxItem.get('parentId') == 0 
								&& inboxItem.get('type') != 'News'
								&& inboxItem.get('deletedStatus') == 0;
					} else if(isMessage && isNews){
						return inboxItem && inboxItem.get('parentId') == 0 
								&& inboxItem.get('type') != 'Announcement'
								&& inboxItem.get('deletedStatus') == 0;
					} else if(isNews && isAnnouncement){
						return inboxItem && inboxItem.get('parentId') == 0 
								&& (inboxItem.get('type') == 'Announcement' || inboxItem.get('type') == 'News')
								&& inboxItem.get('deletedStatus') == 0;
					} else if(isAnnouncement){
						return inboxItem && inboxItem.get('parentId') == 0 
								&& inboxItem.get('type') == 'Announcement'
								&& inboxItem.get('deletedStatus') == 0;
					} else if(isMessage){
						return inboxItem && inboxItem.get('parentId') == 0 
								&& (inboxItem.get('type') != 'Announcement' && inboxItem.get('type') != 'News')
								&& inboxItem.get('deletedStatus') == 0;
					} else if(isNews){
						return inboxItem && inboxItem.get('parentId') == 0 
								&& inboxItem.get('type') == 'News'
								&& inboxItem.get('deletedStatus') == 0;
					} 
				});
			} else {
				inboxItems = _.filter(this.toArray(), function(inboxItem) {
					return inboxItem && inboxItem.get('parentId') == 0 
							&& inboxItem.get('deletedStatus') == 0;
				});
			}
			
			//alert("getFilterredInboxItems : "+inboxItems.length);
			return new diary.collection.InboxItemCollection(inboxItems);
		},
		
		getInboxItem: function(type, id) {
			if(type == 'Message'){
				var inboxItems = _.filter(this.toArray(), function(inboxItem) {
					return inboxItem && inboxItem.get('type') != 'News' 
					&& inboxItem.get('type') != 'Announcement' 
					&& inboxItem.get('itemId') == id
					&& inboxItem.get('deletedStatus') == 0;
				});
				return inboxItems[0];
			} else {
				var inboxItems = _.filter(this.toArray(), function(inboxItem) {
					return inboxItem && inboxItem.get('type') == type 
					&& inboxItem.get('itemId') == id;
				});
				return inboxItems[0];
			} 
		},
		get : function(id) {
			
				var inboxItems = _.filter(this.toArray(), function(inboxItem) {
					return inboxItem && inboxItem.get('itemId') == id;
				});
				return inboxItems[0];
			
		},
		
		comparator : function(inboxItem) {
			var dayKey=inboxItem.get('sentDate');
			dateTime = dayKey.split(" ");

			var date = dateTime[0].split("-");
				var yyyy = date[0];
				var mm = date[1];
				var dd = date[2];

			var time = dateTime[1].split(":");
				var hr = time[0];
				var min = time[1];
				var sec = time[2];
		
			
			var dateToFormat = yyyy + "/" + mm + "/" + dd + " " + hr + ":" + min + ":" + sec;
			var time=new Date(inboxItem.get('sentDate'))
			var newDate=new Date(dateToFormat);
			//var newDate=new Date(yyyy,mm,dd,hr,min,sec);
			
			return newDate;
		}
	
	
	});
	
})();
