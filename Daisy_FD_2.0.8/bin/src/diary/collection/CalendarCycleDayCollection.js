/**
 * The model that represents a collection of Calendar Cycle Days
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.CalendarCycleDayCollection = (function() {

	/**
	 * Creates a collection of calendarCycleDays
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

		model : diary.model.CalendarCycleDay,
		
		isReportTabActive: function() {
			var isTeacher = false;
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				isTeacher = true;
			}
			var reports = _.filter(this.toArray(), function(report) {
				return report && report.get('isSchoolActive') == 1 && report.get('isReportActive') == 1
						&& ((isTeacher && report.get('isTeacher') == 1) || (!isTeacher && report.get('isStudent') == 1));
			});
			
			console.log('CalendarCycleDayCollection : ' + reports.length);
			return reports;
		}
	
	});
	
})();