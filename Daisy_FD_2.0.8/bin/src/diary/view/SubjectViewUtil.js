diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};

/**
 * A collection of utility methods to be used by the subject view.
 * created by Jane Sivieng
 * @type {Object}
 */
diary.view.SubjectViewUtil = {
    'periodWidth' : 120,
    'periodsPanelWidth' : 85,
    'heightPerHour' : 60 * 2,

	/**
	 * Used for the timetable list panel.
	 * @param timetables {diary.collection.TimetableCollection} A timetable collection
	 * @param displayedTimetable {?diary.model.Timetable} The currently selected timetable. Optional.
	 * @return {Object} An object used by the timetable list panel template.
	 */
    'mapTimetables': function (timetables, displayedTimetable) {
		var timetablesObj = [];
	
		timetables.each(function (timetable, index) {
			timetablesObj.push({
				'id': timetable.id,
				'name': (timetable.get('name') ? timetable.get('name') : "Timetable " + (1 + index)),
				'description': "from " + diary.view.SubjectViewUtil.formatDate(timetable.get('startDay')),
				'selected': timetable == displayedTimetable,
				'current': timetable == timetables.getCurrentAt(new Rocketboots.date.Day())
			});
		});
	
		return timetablesObj;
	},
	
	'mapMultiCampusTimetables': function (timetables, displayedTimetable) {
		var view = this;
		var timetablesObj = [];
		var timetable1Checked = false, timetable2Checked = false, timetable3Checked = false;
		
		console.log("semester1Ids mapMultiCampusTimetables : " + semester1Ids);
    	console.log("semester2Ids mapMultiCampusTimetables : " + semester2Ids);
    	console.log("semester3Ids mapMultiCampusTimetables : " + semester3Ids);
    	
		timetables.each(function (timetable, index) {
			for (var i = 0; i < semester1Ids.length; i++) {
				if(timetable.get('id') == semester1Ids[i]){
					if(!timetable1Checked){
						var name = getMultiCampusSemesterName(timetable);
						timetablesObj.push({
							'id': timetable.id,
							'name': name, //(timetable.get('name') ? timetable.get('name') : "Timetable " + (1 + index)),
							'description': getMultiCampusSemesterDate(timetable), //"from " + diary.view.SubjectViewUtil.formatDate(timetable.get('startDay')),
							'selected': timetable == displayedTimetable,
							'current': timetable == timetables.getCurrentAt(new Rocketboots.date.Day())
						});
					}
					timetable1Checked = true;
				}
			}
			
			for (var i = 0; i < semester2Ids.length; i++) {
				if(timetable.get('id') == semester2Ids[i]){
					if(!timetable2Checked){
						var name = getMultiCampusSemesterName(timetable);
						timetablesObj.push({
							'id': timetable.id,
							'name': name, //(timetable.get('name') ? timetable.get('name') : "Timetable " + (1 + index)),
							'description': getMultiCampusSemesterDate(timetable), //"from " + diary.view.SubjectViewUtil.formatDate(timetable.get('startDay')),
							'selected': timetable == displayedTimetable,
							'current': timetable == timetables.getCurrentAt(new Rocketboots.date.Day())
						});
					}
					timetable2Checked = true;
				}
			}
			
			for (var i = 0; i < semester3Ids.length; i++) {
				if(timetable.get('id') == semester3Ids[i]){
					if(!timetable3Checked){
						var name = getMultiCampusSemesterName(timetable);
						timetablesObj.push({
							'id': timetable.id,
							'name': name, //(timetable.get('name') ? timetable.get('name') : "Timetable " + (1 + index)),
							'description': getMultiCampusSemesterDate(timetable), //"from " + diary.view.SubjectViewUtil.formatDate(timetable.get('startDay')),
							'selected': timetable == displayedTimetable,
							'current': timetable == timetables.getCurrentAt(new Rocketboots.date.Day())
						});
					}
					timetable3Checked = true;
				}
			}
			
			
		});

		return timetablesObj;
	},

	/**
	 * Format a date appropriate for displaying on the subject view.
	 * @param date {!Rocketboots.date.Day} The date to display.
	 * @return {String} The human-readable representation of the date.
	 */
	'formatDate': function (date) {
	//console.log("formatDate="+date);
		if (!(date instanceof Rocketboots.date.Day)) {
			date = new Rocketboots.date.Day(date);
		}
	//console.log("date");
	console.log(diary.view.DiaryCalendarViewUtil.daysOfWeek[date.getDate().getDay()]
			+ ", " + date.getDayInMonth()
			+ " " + monthNames[date.getMonth()]);
		
		var schoolFormat = app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat');
	    if (schoolFormat == "USA"){
			return monthNames[date.getMonth()]
			+ " " + date.getDayInMonth();
		} else {
			return  date.getDayInMonth()
			+ " " + monthNames[date.getMonth()];
		}			
		
	},
	
	'getActualTimetableAcrossCampuses' : function(cycleDay,timetable,calendarDay){
		
		var timetableid = 0;
		var sqlCalendarDay = "";
		var semester1IdsStr = _.map(semester1Ids, function (timetable) {
			return timetable;
		}).join(",");
		
		var semester2IdsStr = _.map(semester2Ids, function (timetable) {
			return timetable;
		}).join(",");
		
		var semester3IdsStr = _.map(semester3Ids, function (timetable) {
			return timetable;
		}).join(",");
		
//		var timetableList = "1";
//		
//		if(semester2IdsStr && semester2IdsStr != ""){
//			timetableList = semester2IdsStr;
//			for (var i = 0; i < semester1Ids.length; i++) {
//				if(semester1Ids[i] == timetable.get('id')){
//					timetableList = semester1IdsStr;
//					break;
//				}
//			}
//		}
		
		var timetableList = "1";
		timetableList = semester1IdsStr;
		
		for (var i = 0; i < semester2Ids.length; i++) {
			if(semester2Ids[i] == timetable.get('id')){
				timetableList = semester2IdsStr;
				break;
			}
		}
	
		for (var i = 0; i < semester3Ids.length; i++) {
			if(semester3Ids[i] == timetable.get('id')){
				timetableList = semester3IdsStr;
				break;
			}
		}
		
		if(calendarDay){
			sqlCalendarDay = "and t.startDate <= "+calendarDay.getKey();
		}
		
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"select p.*, c.*, c.timetable as timetableid from period p left outer join class c on c.periodId = p.id left outer join timetable t on t.id = c.timetable where t.id in ("+ timetableList +") "+ sqlCalendarDay +" and c.cycleday = "+cycleDay,

				// params
				{},

				// success
				function(statement) {
					var values=_.map(statement.getResult().data, function(item) {
	                     return item;
	                 });
					
					if(values[0]){
						timetableid = values[0].timetableid;
					} 
//					else if(timetableList[0]){
//						timetableid = timetableList[0];
//					}
				},

				// error
	            function error()
	            {
	               // console.log("update User id error");
	            }
		);
		
		return timetableid;
	},
	
	'getActualTimetableAcrossCampusesForDay' : function(cycleDay,timetable,calendarDay){
		
		var timetableid = 0;
		var sqlCalendarDay = "";
		
		if(calendarDay){
			sqlCalendarDay = " t.startDate <= "+calendarDay.getKey() + " and t.endDate >= "+calendarDay.getKey() + " and ";
		}
		
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"select p.*, c.* from period p left outer join class c on c.periodId = p.id left outer join timetable t on t.id = c.timetable where "+ sqlCalendarDay +" c.cycleday = "+cycleDay,

				// params
				{},

				// success
				function(statement) {
					var values=_.map(statement.getResult().data, function(item) {
	                     return item;
	                 });
					
					if(values[0]){
						timetableid = values[0].timetable;
					}
				},

				// error
	            function error()
	            {
	               // console.log("update User id error");
	            }
		);
		
		return timetableid;
	},
	
	'mapTimetable': function(timetable) {

		if (!timetable) return null;

		var timetableObj = {
			cycledays: [],
            timeScale: [],
            width: 0,
            emptyGrids : [],
			hasTimetable: true
		};

        var periodCount = timetable.get('periods').toArray().length;
		//alert("cout " + cout);
		if(periodCount != 0)
			timetableObj.timeScale = this.mapTimeScale(timetable);
		
        var gridSize = 0;
        // map cycledays
		for (var i = 1; i <= timetable.get('cycleLength'); i++) {
			var cycleDayObj = this.mapCycleDay(i, timetable, timetableObj.timeScale);
			timetableObj.cycledays.push(cycleDayObj);
			
			if(cycleDayObj && cycleDayObj.periods && cycleDayObj.periods.length > gridSize){
				gridSize = cycleDayObj.periods.length;
			}
			
			cycleDayObj = null;
		}
		
		//Mustache template is logic less, hence written logic to show grid of max no of rows and columns.
		for (var count = 0; count < timetable.get('cycleLength'); count++) {
			var i = 0, obj, maxSize;
			maxSize = gridSize - timetableObj.cycledays[count].periods.length;
			while (i < maxSize) {
				obj = {
					'obj' : i
				};
				timetableObj.cycledays[count].emptyGrids.push(obj);
				i++;
			}
		}

        timetableObj.width =  this.periodWidth * timetableObj.cycledays.length + this.periodsPanelWidth;

		return timetableObj;
	},

	'mapMultiCampusTimetable': function(timetable) {

		if (!timetable) return null;
		var timetables = app.model.get(ModelName.CALENDAR).get('timetables');
		var timetableObj = {
			cycledays: [],
            timeScale: [],
            width: 0,
            emptyGrids : [],
			hasTimetable: true
		};

		var periodCount = timetable.get('periods').toArray().length;
		//alert("cout " + cout);
		if(periodCount != 0)
			timetableObj.timeScale = this.mapTimeScale(timetable);
		
        var cycleLength = getMaximumCycleLength(timetable);
        var gridSize = 0;
        
        // map cycledays
		for (var i = 1; i <= cycleLength; i++) {
			var timetableid = this.getActualTimetableAcrossCampuses(i,timetable);
			console.log("mapTimetable timetableid : " + timetableid);
			
			var localTimetable = timetables.get(timetableid) ? timetables.get(timetableid) : timetable;
			var cycleDayObj = this.mapCycleDay(i, localTimetable, timetableObj.timeScale, timetableid);
			timetableObj.cycledays.push(cycleDayObj);
			
			if(cycleDayObj && cycleDayObj.periods && cycleDayObj.periods.length > gridSize){
				gridSize = cycleDayObj.periods.length;
			}
			
			cycleDayObj = null;
		}

		//Mustache template is logic less, hence written logic to show grid of max no of rows and columns.
		for (var count = 0; count < cycleLength; count++) {
			var i = 0, obj, maxSize;
			maxSize = gridSize - timetableObj.cycledays[count].periods.length;
			while (i < maxSize) {
				obj = {
					'obj' : i
				};
				timetableObj.cycledays[count].emptyGrids.push(obj);
				i++;
			}
		}

        timetableObj.width =  this.periodWidth * timetableObj.cycledays.length + this.periodsPanelWidth;
        
		return timetableObj;
	},

	'getPeriodAfter' : function (periodCollection, period) {
		var periodIndex = periodCollection ? periodCollection.indexOf(period) : -1;
		return (periodIndex != -1 && periodIndex + 1 < periodCollection.length) ? periodCollection.at(periodIndex + 1) : null;
	},

	/**
	 * Get the height of a period for display in subject view.
	 *
	 * The height of the period includes any gaps at the end of the period before the subsequent period.
	 * Since it is possible that periods may have gaps in between them, the end time of the period used for this calculation
	 * is the start time of the following period.
	 *
	 * @param period {diary.model.Period} The period for which to calculate a height.
	 * @param nextPeriod {?diary.model.Period} The period which immediately follows the given period on the same day.
	 * @return {Number} The height of the period in pixels.
	 */
    'mapPeriodHeight' : function(period, nextPeriod) {
		var endTime = nextPeriod ? nextPeriod.get('startTime') : period.get('endTime');

		return Math.floor((endTime.toDate().getTime() - period.get('startTime').toDate().getTime()) /
			3600000 * this.heightPerHour) - 2;
	},

    'mapTimeScale' : function(timetable) {
        var startTime = null, endTime = null,
            timeIncrements = [];

        _.forEach(timetable.get('periods').toArray(), function(p) {
            if (startTime == null || p.get('startTime').toInteger() < startTime.toInteger()) {
                startTime = p.get('startTime');
            }
            if (endTime == null || p.get('endTime').toInteger() > endTime.toInteger()) {
                endTime = p.get('endTime');
            }
        });

        var first = true;
        for (var i = startTime.toDate(); i < endTime.toDate(); i.setMinutes(i.getMinutes() + 5)) {
            var timeLabel = {
                'time-15': (i.getMinutes() % 15) == 0,
                'hour': i.getMinutes() == 0,
                'periodStart' : first,
                'top' : first
            };

            if (first) {
                // no label
                first = false;
				// round down the next time to the nearest 5 mins
				i.setMinutes(i.getMinutes() - (i.getMinutes() % 5));
            } else {
                if (timeLabel['hour']) {
                    timeLabel.label = diary.view.DiaryCalendarViewUtil.formatTime(i);
                } else if (timeLabel['time-15']) {
                    timeLabel.label = i.getMinutes();
                }
            }
            timeIncrements.push(timeLabel);
        }

        _.last(timeIncrements).label = ""; // remove label of last timescale increment;

        return {
            'time' : timeIncrements,
            'height' : this.mapPeriodHeight(new diary.model.Period({'startTime' : startTime, 'endTime' : endTime})),
            'startTime' : startTime,
            'endTime' : endTime
        };
    },
	
	'mapCycleDay' : function(cycleDay, timetable, timeScale, timetableid) {
    	
        var util = this;
		var cycleDayObj = {
			'dayIndex'	: cycleDay, 
			'cycleName'		: timetable.getCycleDayLabel(cycleDay) || ('Day ' + cycleDay),
            'needsTopPadding' : false,
            'paddingTop' : 0,
            'needsBottomPadding' : false,
            'multiCampuses' : false,
            'paddingBottom' : 0,
            'emptyGrids' : [],
            'periods'	: []
		};
		
		console.log("cycleDay " + cycleDay + " timetableid " +timetableid);
		var periods = timetable.periodsForCycleDay(cycleDay,timetableid);
		
		var number_of_Campuses = app.model.get(ModelName.SCHOOLPROFILE).get('number_of_Campuses');
		if(number_of_Campuses > 1){
			cycleDayObj.multiCampuses = true;
		} else {
			cycleDayObj.multiCampuses = false;
		}
		
		//console.log("cycleDay : " + cycleDay + " periods : " + periods.length);
		if(periods.length == 0){
			if(number_of_Campuses > 1){
				cycleDayObj.cycleName = getCycleDayLable(timetableid,cycleDay,timetable); 
			} else {
				//cycleDayObj.cycleName = "Day";
			}
		}
		var isOdd = true;
        var first = true;
		periods.each(function(period) {
            var cssClass = "";
            cssClass = isOdd ? "odd" : "even";
            isOdd = !isOdd;
            var periodObj = diary.view.SubjectViewUtil.mapPeriod(period, util.getPeriodAfter(periods, period), timetable, cssClass, cycleDay);
            cycleDayObj.periods.push(periodObj);

            // periods are returned in sorted order, so we can take the first and calculate the difference between
            // it's start time and the timescale's start time
            if (first) {
                var diff = period.get('startTime').toDate().getTime() - timeScale.startTime.toDate().getTime();

//                console.log('top padding diff', diff);
                cycleDayObj['needsTopPadding'] = diff != 0;
                cycleDayObj['paddingTop'] = Math.floor(diff / 3600000 * util.heightPerHour) - 2;
                first = false;
            } else  {
                //TODO: make this more efficient so it's not calculated for every period
                var diff = timeScale.endTime.toDate().getTime() - period.get('endTime').toDate().getTime();
//                console.log('bottom padding diff', diff);
                cycleDayObj['needsBottomPadding'] = diff != 0;
                cycleDayObj['paddingBottom'] = Math.floor(diff / 3600000 * util.heightPerHour) - 2;
            }
			
			periodObj = null;
		});
		
		periods = null;
		return cycleDayObj;
	},

    'mapPeriod' : function(period, nextPeriod, timetable, cssClass, cycleDay) {
		if (!period || !timetable) return null;

        var util = this;
        var periodObj = {};

        periodObj.cssClass = cssClass;
        periodObj.dayIndex = cycleDay;
        periodObj.periodIndex = period && period.get('id');
        periodObj['empty'] = true;

        periodObj.height = util.mapPeriodHeight(period, nextPeriod);
        periodObj.label = diary.view.DiaryCalendarViewUtil.formatTime(period.get('startTime'));
        periodObj.name = period.get('title');
        periodObj['isBreak'] = period.get('isBreak');

		// if period length is less than 15 mins
		if (period.get('endTime').toDate().getTime() - period.get('startTime').toDate().getTime() < 900000) {
			periodObj.cssClass += " tiny";
			//console.log("tiny period");
		}


		var classObject = timetable.classAt(cycleDay, period),
            subject = classObject ? classObject.get('subject') : null,
            room = classObject ? classObject.get('room') : null;
			//console.log("classObject");
			//console.log(classObject);
			//console.log("room");
			//console.log(room);
        // vary what we display depending on the height of the cell
        /* if (periodObj.height < 30) {
            // try to put everything on one line
            if (subject) periodObj.name += ": " + subject.get('title');
            if (room) periodObj.name += ", " + room;
        } else if (periodObj.height < 45) {
            // try to put everything on two lines
            if (subject) periodObj.name += ": " + subject.get('title');
            if (room) periodObj.room = room;
        } else { */
            // let it take as much space as required
            
            var obj = null;
            if (subject && period){
            	obj = getObjectByValues(app.model.get(ModelName.ALLCLASSES), subject.get('id'), 'subjectId',cycleDay,'cycleDay', period.get('id'), 'periodId');
            }
            
            if (subject) periodObj.subject = (obj != null && obj != undefined ) ? obj.get('name') : subject.get('classTitle');
            if (room) periodObj.room = room;
        //}

        if (classObject) {
            if (subject) {
				//periodObj.colour = subject.get('colour');
				//	alert(subject.get('colour').toUpperCase());
				var hexCode = isHexCode((subject.get('colour')))?getHexCode(subject.get('colour').toString().toUpperCase()):subject.get('colour');
			   //	alert("hexCode "+ hexCode);
			   var lighter = ColorCodes(hexCode);
			//  var style = 'background:linear-gradient(to bottom, '+lighter+' 10% , '+hexCode+' 35% ); background:0,1 0 0 0, 2 0 0 0,3 0 1 0; background-radius:3; width:200px; height:200px; float:left; border-radius:4px;';
			//var style =   'background: '+lighter+'; background: -moz-linear-gradient(top, '+lighter+' 10%, '+hexCode+' 35%); background: -webkit-gradient(linear, left top, left bottom, color-stop(10%,'+lighter+'), color-stop(35%,'+hexCode+')); background: -webkit-linear-gradient(top, '+lighter+' 10%,'+hexCode+' 35%); background: -o-linear-gradient(top, '+lighter+' 10%,'+hexCode+' 35%); background: -ms-linear-gradient(top, '+lighter+' 10%,'+hexCode+' 35%); background: linear-gradient(to bottom, '+lighter+' 10%,'+hexCode+' 35%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\''+lighter+'\', endColorstr=\''+hexCode+'\',GradientType=2 ); border-radius:4px;';
			var style = 'background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ebebeb), to(#d3d2d2));  background-image: -webkit-linear-gradient(top, #ebebeb, #d3d2d2); background-image: -o-linear-gradient(top, #ebebeb, #d3d2d2);width: 105px; height: 65px; margin: 4px; background-color: #b2b1b1;border-bottom:solid 2px #b3b3b3;border-top: 6px solid '+hexCode+';';
							              //  alert("sldfjsda " + periodObj.colour);
		    periodObj.colour = style;
	        periodObj.fontcolors = getContrastYIQ(hexCode);
            }
            periodObj.classId = classObject.get('id');
            periodObj['empty'] = false;
        }
		
		classObject = null;
		obj = null;
        return periodObj;
    }

};