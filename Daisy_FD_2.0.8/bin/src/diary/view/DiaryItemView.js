diary = typeof (diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};
var editor, editor2, process, oncecall = true, assignTaskDetails, SelectedClassForTask, selectedStudentArr = [];
var isDateSelect = true;


diary.view.DiaryItemView = (function() {

	var _MODE = {
		'UPDATE' : 1,
		'CREATE' : 2,
		'READONLY' : 3
	};

	var view = Rocketboots.view.View.extend({

		events : {
			"change .tableFieldLayout .type" 					: "updateDiaryItemType",
			"change .tableFieldLayout .subject" 				: "resetDefaultTimeFields",
			"change .tableFieldLayout .allDay" 					: "updateVisibleTimeFields",
			"change .startTimeRow select" 						: "updateOrDefaultEndTime",
			"change .attachmentsRow .attachment" 				: "addAttachment",
			"click .attachmentList .attachmentRemove" 			: "removeAttachment",
			"click .editButtonContainer .cancel" 				: "closeDialog",
			"click .editButtonContainer .delete" 				: "deleteDiaryItem",
			"click .editButtonContainer .save" 					: "saveDiaryItem",
			"click ul.attachments > li.attachment .attachmentName" 	: "openAttachment",
			"blur .taskTable input" 							: "updateSliderValue",
			"click .radio" 										: "assignTask",
			"click #assignTaskClasslist .classes_tbl_lt .classList" 					: "showStudent",
			"click #allStudent"									: "checkAll",
			"click .studentChk"									: "checkStudent",
			"click #nextBtn"									: "showTaskDetails",
			"click #backAssignItem"								: "backToAddEditItem",
			"click #backTaskDetails"							: "backToAssignStudent",
			"click #sendItem"									: "sendDiaryItem",
			"click .tableFieldLayout .completed"				: "updateSlider"
		},

		initialize : function(options) {

			Rocketboots.view.View.prototype.initialize.call(this);
			//console.log('DiaryItemView.initialize()');

			this._appState = app.model.get(ModelName.APPLICATIONSTATE);

			this.attachmentsToAdd = [];
			this.attachmentsToRemove = new diary.collection.AttachmentCollection();
			this.diaryItemType = "";
			this.editing = false;
			this.mode = _MODE.UPDATE;
			this.calendarDay = null;
			this.periodStartTime = null;
			this.assignTo = null;
			this.periodEndTime = null;
			
			this.arrDates = new Array();		// Define array of dates to highlight

			// the default values object is an object with the following attributes, this is ONLY used for creation
			// of new diary items
			// subject - The subject to which the diary item is to be linked
			// title - The title to use for the diary item being created
			// description - The description to be used for the diary item being created
			this.defaultValues = null;

			this.parseAdditionalParams(options);
			this.render();
		},

		parseAdditionalParams : function(options) {
			if ( typeof (options.el) !== "undefined") {
				this.el = options.el;
				this.classes = options.classes
			}
			this.editorref = options.editorref;
		},

		render : function() {//alert(this.process)
			//console.log("render diaryItemView");
			var view = this;
			var minutes = [];
			var i = 0;
			var minute;
			//create array of minutes to display on UI
			while (i < 60) {
				minute = {
					'minute' : i
				};
				minutes.push(minute);
				i++;
			}

			var hours = [];
			var j = 0;
			var hour;
			//create array of hours to display on UI
			while (j <= 23) {
				hour = {
					'hour' : j
				};
				hours.push(hour);
				j++;
			}

			var studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
			var isTeacher = studentProfile.get("isTeacher");
			console.log("isTeacher : "+isTeacher);
			// render diary item popup 
			//change for defect 308 change subject list to class list
			//console.log("app.model.get(ModelName.CLASSES) : "+ JSON.stringify(app.model.get(ModelName.CLASSES)));
			//console.log("app.model.get(ModelName.CLASSES)= undefine "+(app.model.get(ModelName.CLASSES) == undefined));
			
			if(app.model.get(ModelName.CLASSES) == undefined){
				getAllClasses();
			}
			
			//getAllClasses();
			//console.log("app.model.get(ModelName.CLASSES)=  "+  JSON.stringify(app.model.get(ModelName.CLASSES)));
			
			var viewParams = {
				//'subjects' : app.model.get(ModelName.SUBJECTS).toArray(),
				'classes' : app.model.get(ModelName.CLASSES) != undefined ? app.model.get(ModelName.CLASSES).toJSON() : null,
				'minutes' : minutes,
				'hours' : hours,
				'isTeacher' : isTeacher
			};
			var content = app.render('diaryItemFormEdit', viewParams);
			
			$(view.el).html(content);
			
			/* $('#banner-fade').bjqs({
				height      : 120,
				width       : 120,
				automatic   : false,
				animduration : 0,
				responsive  : true
			}); */
			
			view.inputs = {
				'type' : $(".type", view.el),
				'title' : $(".title", view.el),
				'date' : $(".date", view.el),
				'dateHolder' : $(".dateHolder", view.el),
				'allDay' : $(".allDayContainer .allDay", view.el),
				'startTime' : $(".startTime", view.el),
				'endTime' : $(".endTime", view.el),
				'endTimeContainer' : $(".endTimeContainer", view.el),
				'assignedDate' : $(".assignedDate", view.el),
				'assignedDateHolder' : $(".assignedDateHolder", view.el),
				'assignedTime' : $(".assignedTime", view.el),
				'completed' : $(".completed", view.el),
				'responseRequired' : $(".responseRequired", view.el),
				'messageFromText' : $(".messageFromText", view.el),
				'description' : $(".description", view.el),
				'subject' : $(".subject", view.el),
				'comments' : $(".comments", view.el),
				'studentSelect' : $(".studentSelect", view.el),
				'attachment' : $("input.attachment", view.el),
				'noteEntry' : $(".noteEntry", view.el),
				'deleteBtn' : $(".delete", view.el),
				'percentage' : $("#progessTd .input_tag", view.el),
				'priority' : $(".priority", view.el),
				'webLinkDesc' : $(".webLinkDesc", view.el),
				'estimatedHours' : $(".estimatedHours", view.el),
				'estimatedMinutes' : $(".estimatedMinutes", view.el),
				'assignedBy' : $(".assignedBy", view.el),
				'assignedDay' : $(".assignedDay", view.el),
				'taskTitle' : $(".taskTitle", view.el),
				'selectAllStudents' : $(".studentsRow #selectAll", view.el),
				
				'assignedToMe' : $("#assignedToMe", view.el),
				'assignedToStudent' : $("#assignedToStudent", view.el),
				'rowAssignToStudent' : $("#rowAssignToStudent", view.el),
				'weight' : $(".weight", view.el),
				'saveBtn' : $("#saveItemBtn",view.el),
				'selectedClass' : $(".classes_tbl_lt", view.el)
			};

			view.rows = {
				'title' : $(".titleRow", view.el),
				'date' : $(".dateRow", view.el),
				'assignedDate' : $(".assignedDateRow", view.el),
				'webLink' : $(".webLink", view.el),
				'description' : $(".descriptionRow", view.el),
				'subject' : $(".subjectRow", view.el),
				'priorityRow' : $(".priorityRow", view.el),
				'rowAssignToStudent' : $("#rowAssignToStudent", view.el),
				'studentsRow' : $(".studentsRow", view.el),
				'commentsRow' : $(".commentsRow", view.el),
				'meritsRow' : $(".meritsRow", view.el),
				'typeRow' : $(".typeRow", view.el),
				'attachments' : $(".attachmentsRow", view.el),
				'completed' : $(".completedRow", view.el),
				'responseRequired' : $(".responseRequiredRow", view.el),
				'messageFromText' : $(".messageFromTextRow", view.el),
				'noteEntry' : $(".noteEntryRow", view.el),
				'descriptionTable' : $(".bottompopup", view.el),
				'assignedByRow' : $(".assignedByRow", view.el),
				'assignedDayRow' : $(".assignedDayRow", view.el),
				'isNotifyRow' : $(".isNotifyRow", view.el),
				'taskTitleRow' : $(".taskTitleRow", view.el)
			};

			// init all the time picker

			view.inputs.startTime.timepicker({
				'step' : 15
			});
			view.inputs.endTime.timepicker({
				'step' : 15
			});
			view.inputs.assignedTime.timepicker({
				'step' : 15
			});

			var calendarDates = view._appState.getCalendarDates();
			
			// init datepicker for date
			//console.log("render : datepicker Init");
			view.inputs.date.datepicker({
				'showOn' : "button",
				'buttonImage' : "assets/frontpage/calendar.png",
				'buttonImageOnly' : true,
				'minDate' : calendarDates.startDate,
				'maxDate' : calendarDates.endDate,
				'defaultDate' : calendarDates.currentDate,
				'changeYear' : true,
				'changeMonth' : true,
				'duration' : 0,
				'showOptions' : {
					direction : 'down'
				},
				'onSelect' : function(dateText, inst) {
				
					//formatDate Requires a new date object to work
					var selectedDate = $(this).datepicker("getDate");
							
					view.updateDateHolder(selectedDate, view.inputs.dateHolder);
					isDateSelect = true;
					view.resetDefaultTimeFields();
				},
				'beforeShowDay' : function(date, inst) {
				
					var arrCount = 0;
					var arrLen = 0;
					
					var classId = null;
					var currentDate = 0; 
					var className = '';
					
					classId = view.inputs.subject.val();
					
					view.fetchClassDates(date);
					
					currentDate = date.getDate();
					
					arrLen = view.arrDates.length;
					
					for (arrCount = 0; arrCount < arrLen; arrCount++)
					{
						if (view.arrDates[arrCount].getTime() == date.getTime())
						{
							className = 'highlight_date_' + classId;
						}
					}
					
					return [true, className, ''];
				}
				
			});
			
			

			// init datepicker for assigned date
			//console.log("render assignedDate.datepicker");
			view.inputs.assignedDate.datepicker({
				'showOn' : "button",
				'buttonImage' : "assets/frontpage/calendar.png",
				'buttonImageOnly' : true,
				'minDate' : calendarDates.startDate,
				'maxDate' : calendarDates.endDate,
				'defaultDate' : calendarDates.currentDate,
				'changeYear' : true,
				'changeMonth' : true,
				'duration' : 0,
				'showOptions' : {
					direction : 'down'
				},
				'onSelect' : function(dateText, inst) {
					//formatDate Requires a new date object to work
					var selectedDate = $(this).datepicker("getDate");
					view.updateDateHolder(selectedDate, view.inputs.assignedDateHolder);
					isDateSelect = true;
					view.resetDefaultTimeFields();
				}
			});

			view.dialog = $(view.el).dialog({
				modal : true,
				width : 780,
				autoOpen : false,
				open : function(event, ui) {
					if (view._appState.get('windowHeight')) {
						$("div.ui-widget-overlay").height(view._appState.get('windowHeight') + "px");
					}
				},
				close : function(event, ui) {
					view.editing = false;
					//	view.resetFormFields();
					app.model.get(ModelName.APPLICATIONSTATE).set('modalDialogVisible', false);
				}
			});
			//console.log("create editor check ref : "+this.editorref)
			if (this.editorref == "editor1") {

				if (CKEDITOR.instances['editor1']) {
					CKEDITOR.remove(CKEDITOR.instances['editor1']);
					$("#cke_editor1").remove();
				}
				
				if (CKEDITOR.instances['editor2']) {
					CKEDITOR.remove(CKEDITOR.instances['editor2']);
					$("#cke_editor2").remove();
				}

				//console.log("ckeditor called")

				var abc = $('#dayDiaryItemFormEditPopup .ckeditor');
				if ($('#dayDiaryItemFormEditPopup .ckeditor').length > 1) {
					$(abc[0]).remove();
				}
				//console.log("create editor 1");
				CKEDITOR.replace('editor1', {
					on : {
						instanceReady : function(evt) {
							// Hide the editor top bar.
							editor = CKEDITOR.instances.editor1;
							var writer = editor.dataProcessor.writer;
							 
							// The character sequence to use for every indentation step.
							writer.indentationChars = '\t';
							 
							// The way to close self closing tags, like <br />.
							writer.selfClosingEnd = ' />';
							 
							// The character sequence to be used for line breaks.
							writer.lineBreakChars = '\n';
							 
							// The writing rules for the <p> tag.
							writer.setRules( 'p',
							    {
							        // Indicates that this tag causes indentation on line breaks inside of it.
							        indent : true,
							 
							        // Inserts a line break before the <p> opening tag.
							        breakBeforeOpen : true,
							 
							        // Inserts a line break after the <p> opening tag.
							        breakAfterOpen : true,
							 
							        // Inserts a line break before the </p> closing tag.
							        breakBeforeClose : false,
							 
							        // Inserts a line break after the </p> closing tag.
							        breakAfterClose : true
							    });
							var abc = $(".cke_editor iframe").contents().find("body").html();
							//	abc.css("font-size","5px").css("color","red")
							$(".cke_editor iframe").contents().find("body").css("padding", "0px").css("font-size", "11px").css("line-height", "18px").css("padding", "0 3px 0 3px").css("margin", "0px")
							$(".cke_editor iframe").contents().find("p").css("padding", "0px").css("line-height", "18px").css("margin", "0px")
						}
					}
				});
				//console.log("create editor 2");
				CKEDITOR.replace('editor2', {
					on : {
						instanceReady : function(evt) {
							// Hide the editor top bar.
							editor2 = CKEDITOR.instances.editor2;
							var writer = editor2.dataProcessor.writer;
							 
							// The character sequence to use for every indentation step.
							writer.indentationChars = '\t';
							 
							// The way to close self closing tags, like <br />.
							writer.selfClosingEnd = ' />';
							 
							// The character sequence to be used for line breaks.
							writer.lineBreakChars = '\n';
							 
							// The writing rules for the <p> tag.
							writer.setRules( 'p',
							    {
							        // Indicates that this tag causes indentation on line breaks inside of it.
							        indent : true,
							 
							        // Inserts a line break before the <p> opening tag.
							        breakBeforeOpen : true,
							 
							        // Inserts a line break after the <p> opening tag.
							        breakAfterOpen : true,
							 
							        // Inserts a line break before the </p> closing tag.
							        breakBeforeClose : false,
							 
							        // Inserts a line break after the </p> closing tag.
							        breakAfterClose : true
							    });
							var abc = $(".cke_editor iframe").contents().find("body").html();
							//	abc.css("font-size","5px").css("color","red")
							$(".cke_editor iframe").contents().find("body").css("padding", "0px").css("font-size", "11px").css("line-height", "18px").css("padding", "0 3px 0 3px").css("margin", "0px")
							$(".cke_editor iframe").contents().find("p").css("padding", "0px").css("line-height", "18px").css("margin", "0px")

							// console.log("ckeditor done")
						}
					}
				});

			}
		},

		/*
		 * FUNCTIONS FOR UPDATING THE HTML FORM
		 */
		updateDiaryItemType : function(addMerit) {
			
			console.log("DiaryItemView::updateDiaryItemType()");
			var view = this;
			
			if(addMerit === true){
				view.inputs.type.val("merit");
			}
			
			var type = view.inputs.type.val(), isMessage = (type == "messageFromTeacher" || type == "messageFromParent"), isTask = (type == "assignment" || type == "homework");
			
			if(type == 'merit'){
				$("#assignedToMe").attr("checked",true);
				$("#saveItemBtn").text("Save");
			} else if(!view.diaryItem){
				
				if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
					$("#assignedToStudent").attr("checked",true);
					$("#saveItemBtn").text("Next");
				}
			}
			
			view.inputs.type.removeClass(view.diaryItemType).addClass(type);
			
//			if((view.diaryItemType == "assignment" || view.diaryItemType == "homework") && (type == "note" || type == "event")){
//				$(".title", view.el).val("");
//			} else if((view.diaryItemType == "note") && (type == "assignment" || type == "homework" || type == "event")){
//				$(".title", view.el).val("");
//			} else if((view.diaryItemType == "event") && (type == "assignment" || type == "homework" || type == "note")){
//				$(".title", view.el).val("");
//			}
			
			view.diaryItemType = type;

			// custom field titles
			var titles = {
				'title' : $("th", view.rows.title),
				'description' : $("th", view.rows.description),
				'date' : $("th", view.rows.date)
			};

			if (isTask) {
				titles.date.html("Due");
			} else {
				titles.date.html("Date");
			}

			if (isMessage) {
				titles.title.html("Attention");
				titles.description.html("Message");
			} else {
				titles.title.html("Title");
				titles.description.html("Description");
			}

			if (type == "note") {
				$(".noteClass").html("Note");
				if (view.inputs.title.val() == "") {
					//default value for a note title was 'Note' but removed it to fix defect EZTEST-212.
					view.inputs.title.val("");
				}
			} else if (type == "event") {
				$(".noteClass").html("Description");
			} else if (view.inputs.title.val() == "Note") {
				view.inputs.title.val("");
			}
			
			view.applyCSSToPopup(isTask,type);

			diary.view.DiaryItemViewUtil.updateVisibleRows(view.rows, view.inputs, type, true);
			if (isMessage) {
				view.inputs.messageFromText.focus().select();
			} else {
				view.inputs.title.focus().select();
			}
			
			/* $('#banner-fade').bjqs({
				height      : 120,
				width       : 120,
				automatic   : false,
				animduration : 0,
				responsive  : true
			}); */
	
			view.updatehighlightDateStyle();
			
		},
		
		applyCSSToPopup : function(isTask,type) {
			
			if (!isTask) {
				$(".taskTable").hide();
				$(".tableFieldLayout").css("width", "96%");
				$(".ui-dialog").css("width", "450px !important");
				$(".ui-dialog").css("left", "32% !important");
				$(".ui-dialog").css("top", "20% !important");
				$(".cke_browser_webkit").css("width", "330px !important");
				$(".cke_browser_webkit").css("height", "70px !important");
				//console.log("cke_top_editor2 hide");
				$("#cke_top_editor2").hide();
				$("#cke_editor2").css("width", "0px !important");
				$("#cke_editor2").css("border", "none");
				var studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
				var isTeacher = studentProfile.get("isTeacher");
				console.log("isTeacher : "+isTeacher);
				if(isTeacher){
					console.log("ifff");
					if (type == 'note'){
						$(".ui-dialog").css("height", "315px !important");
					}
					else if (type == 'merit'){
						$(".ui-dialog").css("height", "525px !important");
						$(".ui-dialog").css("top", "15% !important");
					}
					else {
						$(".ui-dialog").css("height", "380px !important");
					}
				}
				else{
					console.log("student");
					if (type == 'note'){	
						$(".ui-dialog").css("height", "290px !important");
					}
					else if (type == 'merit'){	
						$(".ui-dialog").css("height", "510px !important");
						$(".ui-dialog").css("top", "15% !important");
					}
					else {	
						$(".ui-dialog").css("height", "340px !important");
					}
				}
					
				} 
				else {
					$(".taskTable").show();
					$(".tableFieldLayout").css("width", "47.2%");
					$(".ui-dialog").css("width", "58% !important");
					$(".ui-dialog").css("left", "20% !important");
					$(".ui-dialog").css("right", "0% !important");
					$(".ui-dialog").css("top", "80px !important");
					$(".ui-dialog").css("height", "600px !important");
					var width = 2 * $(".taskTable").width();
					//console.log("cke_top_editor1 show");
					$("#cke_top_editor1").show();
					$("#cke_editor1").css("width", "0px !important");
					$("#cke_editor1").css("border", "none");
					$(".cke_browser_webkit").css("width", width+"px !important");
					$(".cke_wrapper").css("width", width+"px !important");
					$(".cke_browser_webkit").css("height", "70px !important");
			}
			
				$(".ui-dialog").css("padding", "0px !important");
				$(".ui-dialog").css("border", "none");
				$(".ui-dialog").css("background", "none");
				$(".ui-dialog").css("-webkit-box-shadow", "none");
				$(".ui-dialog").css("-webkit-border-radius", "0px");
			
		},
		
		updateVisibleTimeFields : function() {
			//console.log("DiaryItemView::updateVisibleTimeFields()");
			var view = this;

			if (view.inputs.allDay.is(":checked")) {

				diary.view.DiaryItemViewUtil.hideObjects(view.inputs, ['startTime', 'endTimeContainer'], true);

			} else {

				var fieldsToShow = ['startTime'];

				// show the endTime also if it is an EVENT
				if (view.inputs.type.val() == 'event') {
					fieldsToShow.push('endTimeContainer');
				}

				// show the startTime and endTime
				diary.view.DiaryItemViewUtil.showObjects(view.inputs, fieldsToShow, true);
				view.updateOrDefaultStartTime();
			}
		},

		updateOrDefaultStartTime : function() {
			var view = this, start = diary.view.DiaryItemViewUtil.getTime(view.inputs, 'startTime');

			if (start.getHour() == 0 && start.getMinute() == 0) {
				// set a sensible start time
				var now = new Date();
				var nowMinutes = now.getMinutes();
				if (nowMinutes % 5 != 0) {
					now.setMinutes(nowMinutes + (5 - nowMinutes % 5));
					// set to nearest 5 minutes
				}
				if (isDateSelect)
				{	
					diary.view.DiaryItemViewUtil.setTime(view.inputs, Rocketboots.date.Time.parseTime(now), 'startTime');
				}
				view.updateOrDefaultEndTime();
			}
		},

		updateOrDefaultEndTime : function() {
			var view = this, startTime = diary.view.DiaryItemViewUtil.getTime(view.inputs, 'startTime'), endTime = diary.view.DiaryItemViewUtil.getTime(view.inputs, 'endTime');

			if ((endTime.getHour() == 0 && endTime.getMinute() == 0) || endTime.toInteger() < startTime.toInteger()) {
				// set the end time to 15 minutes after the startTime
				var newEndTime = startTime.addMinutes(15);

				// the new calculated end time went over midnight so we will max the time
				if (newEndTime.toInteger() < startTime.toInteger()) {
					// set to max time
					newEndTime = new Rocketboots.date.Time(23, 55);
				}

				diary.view.DiaryItemViewUtil.setTime(view.inputs, newEndTime, 'endTime');
			}
		},

		resetDefaultTimeFields : function() {
			//console.log("DiaryItemView::resetDefaultTimeFields()");
			var view = this, type = view.inputs.type.val(), classId = view.inputs.subject.val();
			
			if (_.isEmpty(classId)) {
				//	console.log("no subject");
				return;
			}
			
			if(type == "merit"){
				view.updateStudentsList(classId);
			}

			var day = new Rocketboots.date.Day(new Date(view.inputs.date.val())), 
					calendarModel = app.model.get(ModelName.CALENDAR), 
					calendarDay = calendarModel.getDay(day.getMonth() + 1, day.getDayInMonth(), day.getFullYear()), 
					timetable = calendarDay.get('timetable'), period;
			
			var timetableid = diary.view.SubjectViewUtil.getActualTimetableAcrossCampuses(calendarDay.get('cycleDay'),timetable);
			if(timetableid){
				var timetables = app.model.get(ModelName.CALENDAR).get('timetables');
				timetable = timetables.get(timetableid);
			}
			// if we have a timetable we will check the periods
			if (timetable != null && (view.inputs.type.val() != 'event')) {
				
				var subjectId = null;
				var classObj = null;
				
				if (app.model.get(ModelName.CLASSES) != undefined)
				{
					classObj = getObjectByValue(app.model.get(ModelName.CLASSES), classId, 'uniqueId');
					
					if (classObj != null)
					{
						subjectId = classObj.get('subjectId');
					}
				}

				period = timetable.getFirstPeriodWithSubjectOnCycleDay(subjectId, calendarDay.get('cycleDay'));
				
				if (period == null) {
					
					//Added for June enhancements
					var assignedTime = Rocketboots.date.Time.parseTime(new Date()); //view.periodStartTime;
					if(assignedTime == undefined || assignedTime == null || assignedTime == ''){
						assignedTime = Rocketboots.date.Time.parseTime(new Date());
					}
					
					//view.inputs.allDay.attr("checked", "checked");		// All day check is not required. Insted, display current time.
					diary.view.DiaryItemViewUtil.setTime(view.inputs, assignedTime, 'startTime');
					//Added for June enhancements
					diary.view.DiaryItemViewUtil.setTime(view.inputs, assignedTime, 'assignedTime');
					this.arrDates.length = 0;
					return;
				} else {

					view.inputs.allDay.removeAttr("checked");
					
					
					if (isDateSelect)
					{
						diary.view.DiaryItemViewUtil.setTime(view.inputs, period.get('startTime'), 'startTime');
						//Added for June enhancements
						diary.view.DiaryItemViewUtil.setTime(view.inputs, period.get('startTime'), 'assignedTime');
						diary.view.DiaryItemViewUtil.setTime(view.inputs, period.get('endTime'), 'endTime');
					}
					/* else
					{
						//view.inputs.startTime.val('');
						var currTime1 = new Date();
						diary.view.DiaryItemViewUtil.setTime(view.inputs, Rocketboots.date.Time.parseTime(currTime1), 'startTime');
					} */
					
				}

				view.updateVisibleTimeFields();
			} else if (timetable == null && (view.inputs.type.val() != 'event')) {
				var assignedTime = view.periodStartTime;
				if(assignedTime == undefined || assignedTime == null){
					assignedTime = Rocketboots.date.Time.parseTime(new Date());
				}
				
				diary.view.DiaryItemViewUtil.setTime(view.inputs, assignedTime, 'startTime');
				this.arrDates.length = 0;
				return;
			} 
			
			
			// reset class date array when class is changed
			this.arrDates.length = 0;
			isDateSelect = true;
			
		},

		updateStudentsList : function(classId,diaryItemId) {
			
			var classUsers = app.model.get(ModelName.CLASSUSERS);
			var students = []; var toList = [];
			
			var diaryItemUserIds = [];			
			if(diaryItemId){
				var diaryItemUsers = getDiaryItemUser(diaryItemId);
				
				for (var i = 0; i < diaryItemUsers.length; i++) {
					diaryItemUserIds.push(diaryItemUsers.at(i).get('UniqueID'));
				}
			}
			
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				if(classId){
					classUsers = getObjectByValue(classUsers, classId, 'class');
				}
				students = classUsers ? classUsers.get("users") : [];
				
				for (var i = 0; students && (i < students.length); i++) {
					var obj1 = new Object();
					
					obj1.UserName = students.at(i).get('name') + " "+students.at(i).get('lastName')
					obj1.type = 'student'
					if($.inArray(students.at(i).get('UniqueId'), diaryItemUserIds) != -1){
						obj1.selected = true
					} else {
						obj1.selected = false
					}
					obj1.email = students.at(i).get('email')
					obj1.UserId = students.at(i).get('UniqueId')
							
					toList.push(obj1);
				}
			} else {
				var obj1 = new Object();
				
				obj1.UserName = app.model.get(ModelName.STUDENTPROFILE).get('name') + " "+app.model.get(ModelName.STUDENTPROFILE).get('lastName')
				obj1.type = 'student'
				obj1.selected = true
				obj1.email = app.model.get(ModelName.STUDENTPROFILE).get('email')
			  	obj1.UserId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID')
			  			
			  	toList.push(obj1);
			}
			
			var viewParams = {
					'toList' : toList,
					'type' : "teacher"
			};
			var content = app.render('studentsToList',viewParams);
			$("#divTolist").html(content);
			var receiverlist = new MSDList("toList");
			receiverlist.render();
			
			$(".studentsRow .MSDList_ListTableBody .MSDList_ListCheckBox").css({
				"margin-top" : "-2px !important",
				"height" : "10px"
			});
						
		},
		
		updateDateHolder : function(selectedDate, dateHolder) {

			//console.log("DiaryItemView::updateDateHolder()");
			//console.log("selectedDate=");
			//console.log(selectedDate);
			// console.log(isNaN(selectedDate));
			if (isNaN(selectedDate)) {
				selectedDate = selectedDate.getTime();
			}

			var view = this, holderText = "";
			// default to no text

			// if no date is selected we will blank the display date
			if (selectedDate) {
				//Set a new var with different format to use
				var school = app.model.get(ModelName.SCHOOLPROFILE);
				var schoolFormat = school.get('RegionalFormat');
				//console.log("school Format=="+schoolFormat);
				console.log("selectedDate : "+selectedDate);
				if (schoolFormat == "USA")
					holderText = $.datepicker.formatDate("mm/dd/yy", selectedDate);
				else
					holderText = $.datepicker.formatDate("dd/mm/yy", selectedDate);
			}
			//console.log("holderText="+holderText)	;
			dateHolder.html(holderText);
		},

		addAttachment : function(event) {

			var view = this, element = $(event.currentTarget), filePath = element.val();
			extension = element.val().split('.').pop();
			
			if ($.inArray(extension.toLowerCase(), ['gif', 'png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'rtf', 'xls', 'xlsx', 'ppt', 'pptx', 'txt', 'bmp','gdoc']) == -1) {
				if(extension != ''){
					openMessageBox('Invalid file format! Supported file formats are gif, png, jpg, jpeg, pdf, doc, docx, rtf, xls, xlsx, ppt, pptx, txt, bmp, gdoc.');
				}
				filePath = '';
			}
			if (filePath && filePath.length > 0) {
				// ensure the file isn't already in the list
				{
					var alreadyAttached = false;
					/* _.forEach(view.attachmentsToAdd, function(existingPath) {
						if (existingPath == filePath) {
							alreadyAttached = true;
						}
					}); */
					
					if (alreadyAttached) {
						return;
					}
				}

				var totalAttachments = parseInt($('ul.attachments li').length);
				// add the file to the list
				if(totalAttachments < 3){
					view.attachmentsToAdd.push(filePath);
				} else {
					openMessageBox("Max 3 attachments allowed.");
					return;
				}
				//alert(999)
				// add row to alert(filePath+"         filePath")the DOM
				
				$("ul.attachments", view.el).append(app.render('diaryItemFormEditAttachment', {
					'path' : filePath,
					'displayName' : diary.view.air.ViewFilesystemUtil.displayNameForFilePath(filePath),
					'iconSource' : diary.view.air.ViewFilesystemUtil.loadFileIcon(filePath, {
						width : 16,
						height : 16
					}).source,
					'editable' : true
				}));

				// add event handlers
				$("ul.attachments .attachmentRemove", view.el).on("click", function(event) {
					view.removeAttachment.call(view, event);
				});

				// set the browse dialog's text to empty
				element.val("");
			}

		},

		removeAttachment : function(event) {

			var view = this, element = $(event.currentTarget).closest(".attachment"), attachmentId = element.attr('attachmentId'), attachmentPath = element.attr('attachmentPath');
			aatchname = $(event.currentTarget).closest(".attachment").find(".attachmentName").text();

			// if we have an attachment ID, we need to remove from the list of existing attachments
			if (attachmentId) {
				view.attachmentsToRemove.add(view.diaryItem.get('attachments').get(attachmentId));
				
				view.diaryItem.get('attachments').get(attachmentId).set('fileSize',0)

				// remove row from the DOM
				$("ul.attachments .attachment[attachmentId=\"" + attachmentId + "\"]", view.el).remove();
			}
			// if we have an attachment path, we need to remove from the list of attachments to add
			else if (attachmentPath) {
				for (var i = 0; i < view.attachmentsToAdd.length; ++i) {

					var compfile = diary.view.air.ViewFilesystemUtil.displayNameForFilePath(view.attachmentsToAdd[i]);

					if (aatchname == compfile) {
						view.attachmentsToAdd.splice(i, 1);
						break;
					}
				}

				// remove row from the DOM

				var pathdef = $(event.currentTarget).closest(".attachment").attr("attachmentPath");

				if (pathdef == attachmentPath) {
					$(event.currentTarget).closest(".attachment").remove();
				}

			}

		},

		openAttachment : function(event) {
			var view = this, target = $(event.currentTarget).closest(".attachment"), attachmentId = target.attr('attachmentId'), attachmentPath = target.attr('attachmentPath'), attachment;

			if (attachmentId && view.diaryItem) {
				attachment = view.diaryItem.get('attachments').get(attachmentId);
				attachment && app.context.trigger(EventName.OPENATTACHMENT, view.diaryItem, attachment);
			} else if (attachmentPath) {
				//alert("Please save the attachments before opening them");
			}
		},

		/**
		 * Show the Diary Item form as a dialog
		 *
		 * @param diaryItemId The identifier of the diary item that is being edited (if an edit operation)
		 * @param {!Rocketboots.date.Day} calendarDay The calendar day for which the diary item form is being shown
		 * @param {?Rocketboots.date.Time} periodStartTime The start of the period for which the diary item form is being shown
		 * @param {?Rocketboots.date.Time} periodEndTime The end of the period for which the diary item form is being shown
		 * @param {?object} defaultValues The default values that are to be used for creating new diay items
		 * @param initialAttachments A list of attachments that are to be linked to the diary item
		 */
		showDialog : function(diaryItemId, calendarDay, periodStartTime, periodEndTime, defaultValues, createdBy, initialAttachments, addTask,fromWeek,addMerit,itemId,studentId,classId, meritComment, passTypeId) {
			$("#divAddEditItem").hide();
			$(".isNotifyRow .isNotify").removeAttr("disabled");
			selectedStudentArr = [];
			var view = this, diaryItem = null;
			if(fromWeek){
				console.log('this is week view');
			} else {
				calendarDay = app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay');
			}
			//console.log("calendarDay 2 : "+calendarDay+" !view.editing : "+(!view.editing));
			if (!view.editing) {

				// set the values as passed by the calling
				// view
				//console.log("defaultValues : "+JSON.stringify(defaultValues));
				view.calendarDay = calendarDay;
				view.periodStartTime = periodStartTime;
				view.periodEndTime = periodEndTime;
				view.defaultValues = defaultValues;

				if (diaryItemId != null) {
					// get the diary item for the specified identifier
					diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId);
					//console.log("in show dialogue diaryItem"+JSON.stringify(diaryItem));
					if(createdBy == undefined){
						createdBy = diaryItem.get('createdBy');
					}

					if ( diaryItem instanceof diary.model.Note || diaryItem instanceof diary.model.Message) {
						return;
						// we don't edit notes from this view
					}
				}

				view.diaryItem = diaryItem != null ? diaryItem.clone() : null;
				console.log("diaryItem : "+diaryItem)
				if (diaryItem == null) {// we are creating a new item
					view.renderPercentSlider(0);
					view.mode = _MODE.CREATE;
					$(".ui-dialog-title").html("Add Item");
					$(".editButtonContainer .midheadtxt").html("Add Item");
					$(".ckedoitorDisableCover").remove();
					$(".cke_browser_webkit").css("width", "700px !important");
				} else {
					//console.log('showDialog called else'+diaryItem.get('progress'));
					//console.log("diaryItem.get('locked')=="+diaryItem.get('locked'));
					
					
					view.renderPercentSlider(diaryItem.get('progress'));
					if (createdBy != UserId) {
						view.mode = _MODE.READONLY;
						console.log("diaryItem.get('type') : "+(diaryItem.get('type') == "EVENT"))
						if(diaryItem.get('type') == 'EVENT'){
							$(".ui-dialog-title").html("Event");
							$(".editButtonContainer .midheadtxt").html("Event");
							$("#saveItemBtn").text("Save");
						}
						else{
							$(".ui-dialog-title").html("Assigned Item");
							$(".editButtonContainer .midheadtxt").html("Assigned Item");
						}
						
						if((diaryItem.get('type') == 'EVENT') || (diaryItem.get('type') == 'NOTE')){
							$("#cke_editor2").css("position", "relative");

							var widths = $("#cke_editor2").width();

							var heights = $("#cke_editor2 iframe").height();

							$("#cke_editor2").append("<div class='ckedoitorDisableCover'>")
							$(".ckedoitorDisableCover").css({
								"position" : "absolute",
								"left" : "0px",
								"top" : "0px",
								"width" : "700px",
								"height" : heights + "px",
								"z-index" : "99999999",
								"backgrond" : "rgba(255,2555,255,0)"
							})
							
						} else {
							$("#cke_editor1").css("position", "relative");

							var widths = $("#cke_editor1").width();

							var heights = $("#cke_editor1 iframe").height();

							$("#cke_editor1").append("<div class='ckedoitorDisableCover'>")
							$(".ckedoitorDisableCover").css({
								"position" : "absolute",
								"left" : "0px",
								"top" : "0px",
								"width" : "700px",
								"height" : heights + "px",
								"z-index" : "99999999",
								"backgrond" : "rgba(255,2555,255,0)"
							})
							
						}
						

					} else {	
						console.log("edit own Diary item diaryItem.get('type') : "+diaryItem.get('type'));
						view.mode = _MODE.UPDATE;
						if(diaryItem.get('type') == 'EVENT'){
							
							$("#saveItemBtn").text("Save");
						}
					
						$(".editButtonContainer .midheadtxt").html("Edit Item");
						
						view.inputs.deleteBtn.show();
						$(".ckedoitorDisableCover").remove();
					}

				}
				
				//alert("addMerit1 "+addMerit);
				view.resetFormFields(addMerit,itemId,studentId,classId,meritComment,passTypeId);

				if (clicksave == true) {
				} else {

					view.dialog.dialog('open');
					
				}

				setTimeout(function() {
					
					view.initFormFields(view.diaryItem, addTask, addMerit,itemId,studentId,classId,meritComment, passTypeId);
					if (clicksave == true) {
						clicksave = false;
						setTimeout(function() {
							if (checkedInput) {

								view.dialog.dialog('open');
								//view.dialog.dialog('close');

								$(".ui-dialog #dayDiaryItemFormEditPopup").find(".completed").attr("checked", "checked");

							} else {
								$(".ui-dialog #dayDiaryItemFormEditPopup").find(".completed").removeAttr("checked")

							}
							$(".ui-dialog  #dayDiaryItemFormEditPopup .editButtonContainer .save").trigger('click')

						}, 80);
					} else {
						
						view.dialog.dialog('open');
					}
					
				
					
				}, 50);

				var timetables = app.model.get(ModelName.CALENDAR).get('timetables'),
					timetable = timetables.getNextSemester(calendarDay);
				
				// If no timetable is current, display the first one
				if (!timetable && timetables.length > 0) {
					timetable = timetables.at(0);
				}
				
				var number_of_Campuses = app.model.get(ModelName.SCHOOLPROFILE).get('number_of_Campuses');
				var classes;
				if(number_of_Campuses && number_of_Campuses > 1){
					getMultiCampusSemesters(calendarDay.getKey());
					classes = app.model.get(ModelName.CLASSES).toArray();
				} else{
					classes = app.model.get(ModelName.CLASSES).getCurrentSemClasses(timetable.get('id')).toArray();
				}
				
				//console.log("subject array : "+JSON.stringify(app.model.get(ModelName.SUBJECTS)));
				var options = '<OPTION value="0">-- No classes --</OPTION>';
				
				//alert(classId);
				if(diaryItem && diaryItem.get('type') == 'MERIT' && diaryItem.get('classId')){
					//alert('if '+diaryItem.get('classId'));
					for (var i = 0; i < classes.length; i++) {
						if(classes[i].attributes.uniqueId == diaryItem.get('classId'))
							options += '<OPTION value="' + classes[i].attributes.uniqueId + '">' + classes[i].attributes.name + '</OPTION>'
					}
					
				} else {
					//alert('else');
					for (var i = 0; i < classes.length; i++) {
						options += '<OPTION value="' + classes[i].attributes.uniqueId + '">' + classes[i].attributes.name + '</OPTION>'
					}
				}
				$("#dayDiaryItemFormEditPopup .subject").html(options);
				
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.VIEW_TASK, view.diaryItem);

				app.model.get(ModelName.APPLICATIONSTATE).set('modalDialogVisible', true);

				if ( typeof (initialAttachments) == "string") {
					initialAttachments = [initialAttachments];
				}

				if ( typeof (initialAttachments) != "undefined") {

					_.forEach(initialAttachments, function(filePath) {

						view.attachmentsToAdd.push(filePath);

						// add row to the DOM
						$("ul.attachments", view.el).append(app.render('diaryItemFormEditAttachment', {
							'path' : filePath,
							'displayName' : diary.view.air.ViewFilesystemUtil.displayNameForFilePath(filePath),
							'iconSource' : diary.view.air.ViewFilesystemUtil.loadFileIcon(filePath, {
								width : 16,
								height : 16
							}).source,
							'editable' : true
						}));
					});
				}

				view.editing = true;

			}
			// change for defect EZTEST-383
			
			if( diaryItem != null && diaryItem.get('type') != 'EVENT' && diaryItem.get('type') != 'NOTE' && diaryItem.get('type') != 'MERIT'){
				$("#divAddEditItem").show();
			}
			
			if(addMerit){
				
			} else if( diaryItem == null){
				$("#divAddEditItem").show();
			}
			console.log("show dialogue diaryItem : "+JSON.stringify(diaryItem))
			var studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
			var isTeacher = studentProfile.get("isTeacher");
			if(diaryItem != null && !(diaryItem.get("assignedtoMe"))){
				
				//alert("inside saveItemBtn "+isTeacher);
				if(isTeacher && !(diaryItem instanceof diary.model.Merit)){
					$("#assignedToStudent").attr("checked",true);
					$("#saveItemBtn").text("Next");
					$("#assignedToMeDiv").hide();
					
					$("#selectedClass").val(diaryItem.get('classId'));
					
				}
			} else if(diaryItem != null && (diaryItem.get("assignedtoMe"))){
				//alert("else if ");
				$("#assignedToMe").attr("checked",true);
			} else{
				//alert("else " + view.inputs.type.val());
				if(isTeacher){
					//$("#assignedToMe").attr("checked",true);
					$("#assignedToStudent").attr("checked",true);
					$("#saveItemBtn").text("Next");
				}
			}
			
			if(!window.monitors.network.available)
			{
				$("#assignedToStudentDiv").hide();
			}
			else
			{
				$("#assignedToStudentDiv").show();
			}
			
		},

		closeDialog : function() {
			var view = this;

			view.dialog.dialog('close');
			view.editing = false;
		},

		renderPercentSlider : function(sliderVal) {
			//console.log("sliderVal : "+sliderVal);
			var view = this;
			$("#percentSlider").slider({
				orientation : "horizontal",
				range : "min",
				max : 100, // max value of the slider
				value : sliderVal,
				step : 1,
				animate : "fast",
				min : 0,
				change : function(event, ui) {
					newVal = ui.value;
					//console.log("newVal : "+newVal);
					$("#progressVal").val(newVal+"%");
					//$(".input_tag").css("left", newVal+"%")
					if(newVal == 100){
						view.inputs.completed.prop('checked', true);
					} else {
						view.inputs.completed.prop('checked', false);
					}
				}
			});
		},

		deleteDiaryItem : function() {
			var hel = confirm("Are you sure to delete this item ?");

			if (hel) {
				var view = this;
				
				var name = $(".headerNavigation").find(".selected").attr('data-link'); 
				//console.log('DeleteDiaryItemCommand.execute ' + JSON.stringify(view.diaryItem));
				if(name == 'myclasses'){
					var meritItemId = $('#tbMeritItemId').val();
					var meritStudentId = $('#tbMeritStudentId').val();
					
					deleteTaskBasedMerit(meritItemId,meritStudentId);
					view.closeDialog();
				} else {
					if (view.diaryItem) {
						app.context.trigger(EventName.DELETEDIARYITEM, view.diaryItem);
						view.closeDialog();
					}
				}
			} else
				return;
		},

		saveDiaryItem : function(event) {
			var btnVal = $(event.currentTarget).text();
			var view = this;
			var type = view.inputs.type.val();
				process = "save";
				view.inputs.title.val(view.inputs.title.val().trim())
				view.inputs.description.val(view.inputs.description.val().trim())
				// Replace the <textarea id="editor1"> with an CKEditor instance.
				if (view.inputs.title.val().trim() == "" && type != "merit") {
					openMessageBox("Please Enter the Title");
					return;
				}

				var timeAttrs = diary.view.DiaryItemViewUtil.getTimeAttributes(view.inputs);
				if (type == "assignment" || type == "homework") {
					if (view.inputs.subject.val() == null || view.inputs.subject.val().trim() == "0") {
						openMessageBox("Please Select the class");
						return;
					}
					
					var webLinkDesc = view.inputs.webLinkDesc.val().trim();
					/* if (webLinkDesc != "" && webLinkDesc != undefined) {
						if(!(webLinkDesc.substr(0,4) == 'www.')){
							alert("Please Enter Weblink Starting With www.");
							return;
						} else {
							view.inputs.webLinkDesc.val(webLinkDesc);
						}
					} */
					
					view.inputs.webLinkDesc.val(webLinkDesc);
					
					var dueDay = new Rocketboots.date.Day(new Date(view.inputs.date.val()));
					var assignedDay = new Rocketboots.date.Day(new Date(view.inputs.assignedDate.val()));
					var dueTime = timeAttrs.startTime;
					var assignedTime = timeAttrs.assignedTime;
					
					var dueTimeString = (dueTime != null) ? dueTime.toString() : "2359";
					if (dueDay.getKey() + dueTimeString < assignedDay.getKey() + assignedTime.toString()) {
						openMessageBox("Due Day and Time must be after Assigned Day and Time");
						return;
					}
				}
				
				if (type == "merit") {
					
					if (view.inputs.subject.val() == null || view.inputs.subject.val().trim() == "0") {
						openMessageBox("Please Select the class");
						return;
					}
					
					if($(".toList").val() == ""){
						openMessageBox("Please select at least one student");
						return;
					}
										
				}
				
				if (type == "event") {
					var startTime = timeAttrs.startTime;
					var endTime = timeAttrs.endTime;
					if (startTime && endTime && startTime.toInteger() > endTime.toInteger()) {
						openMessageBox("Start time should not be after end time");
						return;
					}
				}
				
				var weight = view.inputs.weight.val();
				if(weight != ""){
					if(isNaN(weight) || !(weight >= 0 && weight <= 100)){
						openMessageBox("Enter Numbers only from 0 to 100 in Weighting field");
						return;
					}
				} 

				// Get editor contents
				// http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-getData

				var c = $("#cke_editor2 iframe")
				$("body").append("<div class='dummy' />")
				$(".dummy").css("display", "none")
				$(".dummy").html(c.contents().find("body").text());
				
				if (view.inputs.type.val() == "note") {
					var description = $(".dummy").text();
					if (description == "") {
						openMessageBox("Please Enter Note Description");
						$(".dummy").remove();
						return;
					}
				} 
				
				var c1 = $("#cke_editor1 iframe")
				$("body").append("<div class='dummy1' />")
				$(".dummy1").css("display", "none")
				$(".dummy1").html(c1.contents().find("body").text());
				
				view.inputs.description.val($(".dummy1").text())
				view.inputs.noteEntry.val($(".dummy").text())
				$(".dummy").remove();
				$(".dummy1").remove();
			
			if (btnVal == "Save") {
				
				if(type == "merit" && !(window.monitors.network.available)){
					alert('Network is not available. Please assign merit later!');
					return;
				} 
				
				if (view.mode == _MODE.UPDATE || view.mode == _MODE.READONLY) {
					view.updateDiaryItem();
				} else if (view.mode == _MODE.CREATE) {
					view.createDiaryItem();
				}

			} else {
				//if event or note then resize next window
			
				var timetables = app.model.get(ModelName.CALENDAR).get('timetables'),
					timetable = timetables.getCurrentAt(view.calendarDay);
				
				// If no timetable is current, display the first one
				if (!timetable && timetables.length > 0) {
					timetable = timetables.at(0);
				}
					
				this.applyCSSToPopup(true,'assignment');
//				var classes = app.model.get(ModelName.CLASSES).getCurrentSemClasses(timetable.get('id'));
				var classes;
				var number_of_Campuses = app.model.get(ModelName.SCHOOLPROFILE).get('number_of_Campuses');
				if(number_of_Campuses && number_of_Campuses > 1){
					getMultiCampusSemesters(view.calendarDay.getKey());
					classes = app.model.get(ModelName.CLASSES);
				} else{
					classes = app.model.get(ModelName.CLASSES).getCurrentSemClasses(timetable.get('id'));
				}
				var classId = $(".subject option:selected").val();
				var className = $(".subject option:selected").text();
				SelectedClassForTask = className;
				//alert("app.model.get(ModelName.CLASSES) : "+JSON.stringify(app.model.get(ModelName.CLASSES)));
				//console.log("app.model.get(ModelName.CLASSES) : "+JSON.stringify(app.model.get(ModelName.CLASSES)));
				assignTaskDetails = {
						"details"		: null,
						"assignedBy"	: null,
						"assignedTo"	: null,
				}
				
				console.log("(type != homework && type != assignment) : "+(type != "homework" && type != "assignment"));
				//console.log("classId"+classId);
				
				if(classId != 0 || (type != "homework" && type != "assignment")){
				
					//if task then just display selected class else (in case of notes and events) all classes
					
					if (type == "homework" || type == "assignment") {
					  classes = getObjectByValue(classes, classId, 'uniqueId');
					}
					
					var params = {
						"classes" : classes ? classes.toJSON() : []
					}
					//console.log("params assignDiaryItemView : "+JSON.stringify(params));
					view.assignDiaryItemView = new diary.view.assignDiaryItemView(params, view);
					var content = app.render('assignDiaryItemView', params);
					$("#divAssignStudent").html("");
					$("#divAssignStudent").html(content);
					
					$("#divAddEditItem").hide();
					$("#divAssignStudent").show();
					
				//	console.log("diaryItem : "+JSON.stringify(view.diaryItem)+"diaryItem uniqueid : "+view.diaryItem.get("uniqueId"));
					if (view.mode == _MODE.UPDATE) {
						if (type != "homework" && type != "assignment") {
							classId = view.diaryItem.get("classId");
						}
						console.log("classId : "+classId)
						$("#classList"+classId).trigger('click');
					}
					else{
						$("#assignTaskClasslist .classes_tbl_lt").children('tbody').children('tr:first').trigger('click');
					}
				}
				else{
					openMessageBox("Please select Class");
				}
			}
		},
		showStudent : function(event) {
			var view = this;
			var type = view.inputs.type.val();
			//console.log("app.model.get(ModelName.CLASSUSERS) : "+JSON.stringify(app.model.get(ModelName.CLASSUSERS)));
			var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
			var classUsers = app.model.get(ModelName.CLASSUSERS);
			console.log("$(event.currentTarget) : "+$(event.currentTarget).attr("class"))
			$("#assignTaskClasslist .classes_tbl_lt .selected").addClass("classList");
			$("#assignTaskClasslist .classes_tbl_lt .selected").removeClass("selected");
			$(".classList").find(".arrowClass").attr("src", "assets/myClasses/images/rightarrow.png");
			$(event.currentTarget).removeClass("classList");
			$(event.currentTarget).addClass("selected");
			$(event.currentTarget).find(".arrowClass").attr("src", "assets/myClasses/images/whiterightarrow.png");
			var classId = $(event.currentTarget).find(".classId").val();
			SelectedClassForTask = $(event.currentTarget).find(".className").html();
			//TODO: take from local
			//var classUserList = getClassUsersLocal(currentUserId);

			var classUsers = getObjectByValue(classUsers, classId, 'class');
			console.log("classUsers length : "+classUsers);
			var classUserList  = {};
			if(classUsers != null){
				
				var studentCnt = 0;
				var studentLen = 0;
				
				var studentImage = null;
				var studentImageToDisplay = null;	
				
			
				 studentLen = classUsers.get("users").length;
				 
				 for (studentCnt = 0; studentCnt < studentLen; studentCnt++)
				 {
					 studentImage = classUsers.get("users").at(studentCnt).get('imageUrl');
					 
					 if (studentImage != null && studentImage != '')
					 {
						 studentImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + studentImage;
					 }
					 else
					 {
						 studentImageToDisplay = 'assets/myClasses/images/people.png';
					 }
					 
					 classUsers.get("users").at(studentCnt).set('profileImageToDisplay', studentImageToDisplay);
				 }
			
				classUserList = {
					'students' : classUsers.get("users").toJSON()
				}
				$('.studentList', this.el).html(app.render('assignToStudentView', classUserList));
				//if in update mode then check already assigned users
				//console.log("diaryItemUsers.length"+view.mode);
				
				if(view.diaryItem){
					view.mode = _MODE.UPDATE;
				}
				
				if (view.mode == _MODE.UPDATE) {
				
					if(classId == view.diaryItem.get("classId")){
						var diaryItemUsers = null;
						//console.log("view.diaryItem : "+view.diaryItem.get("uniqueId"));
						if (type == "homework" || type == "assignment") {
							diaryItemUsers = getDiaryItemUser(view.diaryItem.get("uniqueId"));
						}
						else{
							diaryItemUsers = getEventUsers(view.diaryItem.get("uniqueId"));
						}
						
						if($("#selectedClass").val() != ""){
							//check if already selected class and class selected now is not same
							//console.log("classId 1: "+classId);
							//console.log("classId 2: "+$("#selectedClass").val());
							
						}
						
						for (var indx = 0; indx < diaryItemUsers.length; indx++) {
							var user = diaryItemUsers.at(indx);
							selectedStudentArr.push(user.get("UniqueID"));
						}
					}
					else{
						selectedStudentArr = [];
					}
					$("#selectedClass").val(classId);
				}
				else{
					
					// already selected students
					if($("#selectedClass").val() != ""){
						//check if already selected class and class selected now is not same
						if($("#selectedClass").val() != classId){
							selectedStudentArr = [];
						}
					}
					$("#selectedClass").val(classId);
					//console.log("already selected : "+$("#selectedClass").val());
				}
			
				for(var len = 0; len < selectedStudentArr.length; len++){
					$(".studentList input:checkbox").each(function(){
					    var $this = $(this);
					    if($this.attr("id") == selectedStudentArr[len]){
					    	$this.prop("checked", true);
					    }
					});
				}
			}
			else{
					console.log("no users for class");
				    classUserList = {
						'students' : []
					}
				    $('.studentList', this.el).html(app.render('assignToStudentView', classUserList));
			}
		},
		checkAll : function(event){
			//console.log("checkAll..");
			if($(event.currentTarget).is(":checked")){
				selectedStudentArr = [];
				$(".studentList input:checkbox").each(function(){
				    var $this = $(this);
				    $this.prop("checked", true);
				    selectedStudentArr.push($(this).attr("id"));
				});
			}
			else{
				selectedStudentArr = [];
				$(".studentList input:checkbox").each(function(){
				    var $this = $(this);
				    $this.prop("checked", false);
				});
			}
			$("selectedStudent").val(selectedStudentArr);
		},
		checkStudent : function(event){
			//console.log("checkStudent.."+$(event.currentTarget).is(":checked"));
			
			if($(event.currentTarget).is(":checked")){
			
				selectedStudentArr.push($(event.currentTarget).attr("id"));
			}
			else{
				selectedStudentArr.push($(this).attr("id"));
				var index = selectedStudentArr.indexOf($(event.currentTarget).attr("id"));
			
				selectedStudentArr.splice(index, 1);
				selectedStudentArr = selectedStudentArr.filter(function(a){return typeof a !== 'undefined';})
			
			}
			var totalStudent = 0;
			var selectedStudentCounter = 0;
			$("#studentListToAssign .studentChk").each(function(){
				totalStudent++;
		    	var $this = $(this);
		    	 if($this.is(":checked")){
			    	selectedStudentCounter++;
		    	 }
			});
			
			$("selectedStudent").val(selectedStudentArr);
			if(totalStudent == selectedStudentCounter){
				$("#allStudent").prop("checked", true);
			}
			else{
				$("#allStudent").prop("checked", false);
			}
		},
		showTaskDetails : function(){
			
			var view = this, diaryItem, attributes, type = view.inputs.type.val();
			assignTaskDetails.assignedTo = [];
			var selectedStudentCounter = 0;
			$("#studentListToAssign input:checkbox").each(function(){
				
			    	var $this = $(this);
			    	console.log("$this.find(studentName)"+$("#studentName"+$this.attr("id")).text());
			    	 if($this.is(":checked") && $this.attr("id") != "allStudent"){
				    	selectedStudentCounter++;
				    	var selectedStudent = {
								    			"studentName" : $("#studentName"+$this.attr("id")).text(),
								    			"id" : $this.attr("id")
											   };
				    	assignTaskDetails.assignedTo.push(selectedStudent);
			    	 }
			    
			});
			if(selectedStudentCounter > 0){
				
			
					if (type == "homework" || type == "assignment") {
						diaryItem = new diary.model.Task({
							'subject' : ((view.defaultValues) ? view.defaultValues.subject : null)
						});
					} else if (type == "event") {
						diaryItem = new diary.model.Event();
					} else if (type == "messageFromTeacher" || type == "messageFromParent") {
						diaryItem = new diary.model.Message({
							'type' : "message",
							'messageFrom' : (type == "messageFromTeacher") ? diary.model.Message.FROM.TEACHER : diary.model.Message.FROM.PARENT
						});
					} else if (type == "note") {
						diaryItem = new diary.model.Note();
					} else {
						//console.log('Trying to create invalid diary item type');
						return;
					}
					
					if (view.mode == _MODE.UPDATE) {
						
						var oldDiaryItem = view.diaryItem;
						diaryItem = oldDiaryItem.clone(), 
						attributes = diary.view.DiaryItemViewUtil.getAttributesForSave(view.inputs, diaryItem, assignTaskDetails.assignedTo);
	
					} else if (view.mode == _MODE.CREATE) {
						
						attributes = diary.view.DiaryItemViewUtil.getAttributesForSave(view.inputs, diaryItem, view.calendarDay, view.periodStartTime, assignTaskDetails.assignedTo);
						
					}
					var result = diaryItem.set(attributes, {
						// validation
						error : function(something, error) {
							
						}
					});
					diaryItem.set("assignTo",assignTaskDetails.assignedTo);
					view.assignTo = assignTaskDetails.assignedTo;
					
					var dueTime = view.inputs.startTime.text();
					//diaryItem.assignedBy =  (app.model.get(ModelName.STUDENTPROFILE)).get('name') +" "+ (app.model.get(ModelName.STUDENTPROFILE)).get('lastName');
					//console.log("new Rocketboots.date.Day(new Date(view.inputs.date.val())) : "+view.inputs.date.val()+"diaryItem.get('dueTime') : "+$("#dueStartTime").val());
					var date = view.inputs.date.val().split("/"); 
					var dueDate = null; //date[1]+"/"+date[0]+"/"+date[2];
					
					if (app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat') == "USA")
						dueDate = date[0]+"/"+date[1]+"/"+date[2];
					else
						dueDate = date[1]+"/"+date[0]+"/"+date[2];
					
					$("#divAssignDiaryItem").hide();
					var dueTime = "";
					//console.log("$(#dueStartTime).length : "+$("#dueStartTime").css('display'));
					//console.log("$(#dueStartTime).val : "+$("#dueStartTime").val());
					//console.log("$(#dueStartTime).val : "+$("#dueStartTime").length);
					
					if($("#dueStartTime").css("display") != 'none'){
						dueTime = $("#dueStartTime").val();
					}
					//console.log("diaryItem details: "+JSON.stringify(diaryItem));
					var estimatedTime = diaryItem.get("estimatedHours");
					if(diaryItem.get("estimatedHours")){
						if(diaryItem.get("estimatedHours") == '1'){
							estimatedTime = diaryItem.get("estimatedHours") +" hr " + diaryItem.get("estimatedMinutes") + " min" ;
						}
						else{
							estimatedTime = diaryItem.get("estimatedHours") +" hrs " + diaryItem.get("estimatedMinutes") + " min" ;
						}
					}
					
					
					assignTaskDetails = {
							"details"		: diaryItem.toJSON(),
							"selectedClass"	: SelectedClassForTask,
							"dueDayTime"	: dueDate +" " + dueTime,
							"assignedBy"	: (app.model.get(ModelName.STUDENTPROFILE)).get('name') +" "+ (app.model.get(ModelName.STUDENTPROFILE)).get('lastName'),
							"estimatedTime"	: estimatedTime
					}
					
					var params = {
						"details" : assignTaskDetails
					};
					console.log("assignTaskDetails : "+JSON.stringify(assignTaskDetails));
					$('#divTaskDetails', this.el).html(app.render('assignTaskDetailsView', assignTaskDetails));
					console.log("show task details");
					$("#divAssignDiaryItem").hide();
					$("#divTaskDetails").show();
					if (type == "event" || type == "note") {
						//hide estimated time details
						$("#rowEstimatedTime").hide();
					}
					if (type == "note") {
						//hide estimated time details
						$("#rowDueTime").hide();
					}
					
			}
			else{
				openMessageBox("please select student");
			}
			
		},
		convertDate : function(date){
			console.log("date"+date);
			var formatedTime = "" ,formatedDate = "";
			if(date != null || date != undefined || date != '')
			{
			    dateTime = date.split(" ");
			
			    var date = dateTime[0].split("-");
			    var yyyy = date[0];
			    var mm = date[1];
			    var dd = date[2];
			    
			   formatedDate =  dd + '-' + mm + '-'+ yyyy;
			   var time = dateTime[1].split(":");
			    var hour = time[0];
			    var minute = time[1];
			    var includeMinutes = true;
			    var minuteString = (minute == 0 ? (includeMinutes ? ":00" : "") : ":" + (minute < 10 ? "0" : "") + minute);
			
			   formatedTime = (hour < 12 ?
					(hour == 0 ? 12 : hour) + minuteString + " AM" : hour - (hour == 12 ? 0 : 12) + minuteString + " PM" );
			}
			return formatedDate + formatedTime;
		},
		backToAssignStudent : function(){
			$("#divAssignDiaryItem").show();
			$("#divTaskDetails").hide();
		},
		backToAddEditItem : function(){
			//back to Add/edit item, if note/event then hide neccessary field 
			
			this.updateDiaryItemType();
			
			$("#divAddEditItem").show();	
			var width = 2 * $(".taskTable").width();
	    	$(".cke_browser_webkit").css("width", width+" !important");
			$("#divAssignStudent").hide();
		},
		sendDiaryItem : function(){
			var view = this;
			
			if (view.mode == _MODE.UPDATE) {
				view.updateDiaryItem();
			} else if (view.mode == _MODE.CREATE) {
				view.createDiaryItem();
			}
		},
		updateSliderValue : function() {
			var fieldVal = $("#progessTd .input_tag").val().replace("%","");
			//console.log("fieldVal : "+fieldVal)
			this.renderPercentSlider(fieldVal);
		},
		updateSlider : function(event) {
			var isCompleted = $(event.target).is(":checked");
			var view = this;
			if(isCompleted){
				view.inputs.completed.prop('checked', true);
				this.renderPercentSlider(100);
			} else {
				view.inputs.completed.prop('checked', false);
				this.renderPercentSlider(0);
			}
		},
		
		createDiaryItem : function() {
			//console.log("DiaryItemView::createDiaryItem()");
			var view = this, diaryItem, attrs, type = view.inputs.type.val();

			if (type == "homework" || type == "assignment") {
				diaryItem = new diary.model.Task({
					'subject' : ((view.defaultValues) ? view.defaultValues.subject : null)
				});
			} else if (type == "event") {
				diaryItem = new diary.model.Event();
			} else if (type == "messageFromTeacher" || type == "messageFromParent") {
				diaryItem = new diary.model.Message({
					'type' : "message",
					'messageFrom' : (type == "messageFromTeacher") ? diary.model.Message.FROM.TEACHER : diary.model.Message.FROM.PARENT
				});
			} else if (type == "note") {
				diaryItem = new diary.model.Note();
			} else if (type == "merit") {
				diaryItem = new diary.model.Merit();
			} else {
				//console.log('Trying to create invalid diary item type');
				return;
			}

			attrs = diary.view.DiaryItemViewUtil.getAttributesForSave(view.inputs, diaryItem, view.calendarDay, view.periodStartTime);

			// validate new attributes
			var result = diaryItem.set(attrs, {
				// validation
				error : function(something, error) {
					// console.log(error);
					//alert(error);
				}
			});
			
			//alert('before set' + type);
			if(type == 'merit'){ 
				//alert("attrs.assignTo " + JSON.stringify(attrs.assignTo));
				diaryItem.set("assignTo",attrs.assignTo);
			} else {
				diaryItem.set("assignTo",view.assignTo);
			}
					
			//alert("length="+view.attachmentsToAdd.length);
			var flag = 1;
			var filenameDisplay = "";
			var filename = [];
			var totalFileSize = 0;
			//console.log("attachmentsToAdd");
			//console.log(view.attachmentsToAdd.length);
			for (var i = 0; i < view.attachmentsToAdd.length; i++) {
				file = new air.File(view.attachmentsToAdd[i]);
				totalFileSize = totalFileSize + file.size;
				if (totalFileSize > 15728640) {

					 //alert("size of file "+i+"greater than 2 MB");
					flag = 0;
					var attachments = $(".attachments .attachment");
					//console.log(attachments);
					$(attachments).each(function(k, j) {
						// console.log("attachmentpath="+$(j).attr("attachmentpath"));
						// console.log("view.attachmentsToAdd[i]="+($(j).attr("attachmentpath")==view.attachmentsToAdd[i]));
						if ($(j).attr("attachmentpath") == view.attachmentsToAdd[i]) {
							$(this).attr("style", "color:red");
							filename.push($(this).find(".attachmentName").text());
						}
					});

				}
			}
			//console.log( filename)
			for (var i = 0; i < filename.length; i++) {
				if (i == (filename.length - 1)) {
					filenameDisplay += filename[i];
				} else {
					filenameDisplay += filename[i] + ",  "
				}
			}
			//console.log(filenameDisplay);
			// update diary item
			if (result && view.attachmentsToAdd.length <= 3 && flag) {
				if($('#isMeritCreate').val() == "true"){
					addTaskBasedMerit();
				} else {
					app.context.trigger(EventName.CREATEDIARYITEM, diaryItem, view.attachmentsToAdd);
				}
				view.closeDialog();
			} else {

				if (!flag) {
					openMessageBox("File(s) size cannot exceed 15 MB");
					filename = "";
					flag = 1;
				} else if (!result) {
					if (view.inputs.type.val() == "messageFromTeacher" || view.inputs.type.val() == "messageFromParent") {
						openMessageBox("Please Fill The From Field ");
						return;
					} else if (view.inputs.type.val() == "assignment" || view.inputs.type.val() == "event" || view.inputs.type.val() == "") {
						openMessageBox("Assigned Date can't be later than Due Date");
						return;
					}

					return;
					view.closeDialog();
				} else
					openMessageBox("Max 3 attachments allowed.");
			}

		},

		updateDiaryItem : function() {
			var view = this;
			
			var oldDiaryItem = view.diaryItem;
			console.log("oldDiaryItem.get('locked') 111: "+oldDiaryItem.get('createdBy'))
			if (!( oldDiaryItem instanceof diary.model.Event || oldDiaryItem instanceof diary.model.Task || oldDiaryItem instanceof diary.model.Merit)) {
				//console.log("Error: invalid diary item passed to update diary item");
				return;
			} else if (oldDiaryItem.get('locked') == true) {
				console.log(" trying to update locked diary item");
				return;
			}
		
			var diaryItem = oldDiaryItem.clone(), attrs = diary.view.DiaryItemViewUtil.getAttributesForSave(view.inputs, diaryItem);
			var result = diaryItem.set(attrs, {
				// validation
				error : function(something, error) {
					//console.log(error);
					//alert(error);
				}
			});
			
			//diaryItem.set("assignTo",view.assignTo);
			if(oldDiaryItem instanceof diary.model.Merit){ 
				diaryItem.set("assignTo",attrs.assignTo);
			} else {
				diaryItem.set("assignTo",view.assignTo);
			}
			
			console.log("edit data for merits = "+JSON.stringify(diaryItem));
			
			/* TODO
			view.attachmentsToAddOld = [];
			_.forEach(diaryItem.get('attachments').toArray(), function(attachment) {
				view.attachmentsToAddOld.push(attachment.originalFilePath);
			}); 
			*/
			//alert('attachmentsToAddOld update '+view.attachmentsToAddOld.length);
			
			var flag = 1;
			var filenameDisplay = "";
			var filename = [];
			var totalFileSize = 0;
			
			_.forEach(diaryItem.get('attachments').toArray(), function (attachment) {
				totalFileSize = totalFileSize + parseInt(attachment.get('fileSize')); 
			});
			
			for (var i = 0; i < view.attachmentsToAdd.length; i++) {
				file = new air.File(view.attachmentsToAdd[i]);
				// console.log("file.size="+file.size);
				totalFileSize = totalFileSize + file.size;
				if (totalFileSize > 15728640) {

					//alert("size of file "+i+"greater than 2 MB");
					flag = 0;
					var attachments = $(".attachments .attachment");
					//console.log(attachments);
					$(attachments).each(function(k, j) {
						//console.log("attachmentpath="+$(j).attr("attachmentpath"));
						//console.log("view.attachmentsToAdd[i]="+($(j).attr("attachmentpath")==view.attachmentsToAdd[i]));
						if ($(j).attr("attachmentpath") == view.attachmentsToAdd[i]) {
							$(this).attr("style", "color:red");
							filename.push($(this).find(".attachmentName").text());

						}
					});

				}
			}
			
			console.log(filename)
			for (var i = 0; i < filename.length; i++) {
				if (i == (filename.length - 1)) {
					filenameDisplay += filename[i];
				} else {
					filenameDisplay += filename[i] + ",  "
				}
			}
		
			if (result && flag && view.attachmentsToAdd.length <= 3) {

				// save the new details of the diary item
				
				app.context.trigger(EventName.UPDATEDIARYITEM, diaryItem, view.attachmentsToAdd, view.attachmentsToRemove);
				
				// raise an ADD USAGE LOG Event if a task is being updated and it was marked as completed
				if ( oldDiaryItem instanceof diary.model.Task && !oldDiaryItem.isCompleted() && diaryItem.isCompleted()) {
					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.TASK_COMPLETED, diaryItem);
				}
				view.closeDialog();
				
			} else {
				if (!flag) {
					openMessageBox("File(s) size cannot exceed 15 MB");
					filename = "";
					flag = 1;
				} else if (!result) {
					if (view.inputs.type.val() == "messageFromTeacher" || view.inputs.type.val() == "messageFromParent") {
						openMessageBox("Please Fill The From Field ");
						return;
					} else if (view.inputs.type.val() == "assignment" || view.inputs.type.val() == "event" || view.inputs.type.val() == "") {
						openMessageBox("Assigned Date can't be later than Due Date");
						return;
					}
					//view.closeDialog();
					return;
				} else
					openMessageBox("Max 3 attachments allowed.");
			}

		},
		
		mapDiaryItemType : function (diaryItemType) {
			
			return {
				'name' 		: diaryItemType.get('name'),
				'formTemplate' 	: diaryItemType.get('formTemplate'),
				'isActive' 	: app.model.get(ModelName.DIARYITEMTYPES).isTypeActive(diaryItemType.get('name')),
				'UniqueID' 	: diaryItemType.get('UniqueID'),
				'id' 	: diaryItemType.get('id')
			};
		},

		resetFormFields : function(addMerit,itemId,studentId,classId,meritComment) {
			//console.log("DiaryItemView::resetFormFields()");
			var view = this, midnight = new Rocketboots.date.Time(0, 0);
			var diaryItemTypesModel = app.model.get(ModelName.DIARYITEMTYPES);
			
			var allDiaryItemTypes = [];
			for(var i = 0 ; i < diaryItemTypesModel.length; i++){
				var diaryItemType = diaryItemTypesModel.at(i);
				if(!(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")) && addMerit != true && diaryItemType.get('formTemplate').toLowerCase() == 'merit'){
					//alert('skipped');
				} else {
					allDiaryItemTypes.push(this.mapDiaryItemType(diaryItemType));
				}
			}
			
			// reset type
			view.inputs.type.html(app.render('diaryItemFormEditTypes', {
				/* 'homeworkTask' : true,
				'assignmentTask' : true,
				'event' : true,
				'message' : true,
				'note' : true,
				'isAssignmentActive' : diaryItemTypesModel.isDiaryItemTypeActive('Assignment'),
				'isHomeworkActive' : diaryItemTypesModel.isDiaryItemTypeActive('Homework'),
				'isNoteActive' : diaryItemTypesModel.isDiaryItemTypeActive('Note'),
				'isEventActive' : diaryItemTypesModel.isDiaryItemTypeActive('Event'),
				'asignmentFormTemplate' : diaryItemTypesModel.getFormTemplate('Assignment'),
				'homeworkFormTemplate' : diaryItemTypesModel.getFormTemplate('Homework'),
				'noteFormTemplate' : diaryItemTypesModel.getFormTemplate('Note'),
				'eventFormTemplate' : diaryItemTypesModel.getFormTemplate('Event'),
				'eventName' : diaryItemTypesModel.getNameForDiaryItem('Event'),
				'assignmentName' : diaryItemTypesModel.getNameForDiaryItem('Assignment'),
				'homeworkName' : diaryItemTypesModel.getNameForDiaryItem('Homework'),
				'noteName' : diaryItemTypesModel.getNameForDiaryItem('Note'), */
				'allDiaryItemTypes' : allDiaryItemTypes
			}));
			
			view.updateStudentsList(null);
			//alert("addMerit2 "+addMerit);
			view.updateDiaryItemType(addMerit);
			
			/* $('#banner-fade').bjqs({
				height      : 120,
				width       : 120,
				automatic   : false, 
				animduration : 0,
				responsive  : true
			}); */

			// reset fields as for a task
			diary.view.DiaryItemViewUtil.updateVisibleRows(view.rows, view.inputs, 'task', false);

			// reset date fields
			//console.log("view.calendarDay : "+view.calendarDay+" view.calendarDay.getDate() : "+view.calendarDay.getDate());
			
			var calendarDates = view._appState.getCalendarDates(view.calendarDay ? view.calendarDay.getDate() : null);
			//console.log("calendarDates.currentDate="+calendarDates.currentDate+" calendarDates : "+calendarDates);
			view.inputs.date.datepicker("setDate", calendarDates.currentDate);
			//console.log("resetFormFields 1 :  "+calendarDates.currentDate);
			view.updateDateHolder(calendarDates.currentDate, view.inputs.dateHolder);
			view.inputs.assignedDate.datepicker("setDate", calendarDates.currentDate);
			//console.log("resetFormFields 2 : calendarDates.assignedDateHolder="+calendarDates.currentDate);
			view.updateDateHolder(calendarDates.currentDate, view.inputs.assignedDateHolder);

			view.inputs.allDay.prop('checked', false);
			view.inputs.completed.prop('checked', false);
			view.inputs.responseRequired.prop('checked', false);
			view.inputs.assignedToStudent.prop('checked', false);
			view.inputs.assignedToMe.prop('checked', false);
			
			var studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
			var isTeacher = studentProfile.get("isTeacher");
			if(isTeacher){
				$("#assignedToMeDiv").show();
			}
			
			// reset time fields to 12:00am
			diary.view.DiaryItemViewUtil.setTime(view.inputs, midnight, 'startTime');
			diary.view.DiaryItemViewUtil.setTime(view.inputs, midnight, 'endTime');
			diary.view.DiaryItemViewUtil.setTime(view.inputs, midnight, 'assignedTime');

			// reset text fields
			view.inputs.title.val("");
			setTimeout(function() {
				editor.setData("");
				var abc = $(".cke_editor iframe").contents().find("body").html();
				$(".cke_editor iframe").contents().find("body").css("padding", "0px").css("font-size", "11px").css("line-height", "18px").css("padding", "0 3px 0 3px").css("margin", "0px")
				$(".cke_editor iframe").contents().find("body").find("p").css("padding", "0px").css("line-height", "18px").css("margin", "0px")
			}, 10);
			
			setTimeout(function() {
				editor2.setData("");
				var abc = $(".cke_editor iframe").contents().find("body").html();
				$(".cke_editor iframe").contents().find("body").css("padding", "0px").css("font-size", "11px").css("line-height", "18px").css("padding", "0 3px 0 3px").css("margin", "0px")
				$(".cke_editor iframe").contents().find("body").find("p").css("padding", "0px").css("line-height", "18px").css("margin", "0px")
			}, 10);

			view.inputs.description.val("");
			view.inputs.subject.val("");
			view.inputs.messageFromText.val("");
			view.inputs.noteEntry.val("");
			view.inputs.webLinkDesc.val("");
			view.inputs.comments.val("");
			$(".mertiTitle").html("Merit/Concern");
			
			view.inputs.taskTitle.html("");
			view.rows.taskTitleRow.hide();
			view.inputs.assignedBy.html("");
			view.inputs.assignedDay.html("");
			$(".isNotifyRow .isNotify").removeAttr("disabled");
			$(".ckedoitorDisableCover2").remove();
			
			//Params for task based merits.
			$('#isMeritCreate').val("false");
			$('#meritItemId').val("");
			$('#meritStudentId').val("");
			
			$(".isNotifyRow .isNotify").prop("checked",false);
			$("li.bjqs-next").show();
			$("li.bjqs-prev").show();	
			$(".studentsRow #selectAllDiv").show();
			
			view.inputs.saveBtn.text("Save");
			
			$("select.priority option").filter(function() {
				//alert('inside priority');
				return $(this).text().toLowerCase() == "Low".toLowerCase();
			}).attr('selected', true);

			$("select.estimatedMinutes option").filter(function() {
				return $(this).text() == "0";
			}).attr('selected', true);

			$("select.estimatedHours option").filter(function() {
				return $(this).text() == "0";
			}).attr('selected', true);

			view.inputs.percentage.val(0+"%");
			view.inputs.weight.val("");

			// reset attachments
			view.inputs.attachment.val("");

			// hide delete button
			view.inputs.deleteBtn.hide();
			$("#divTaskDetails").hide();
			//$("#divAddEditItem").show();
			$("#divAssignStudent").hide();
			$(".lockItem").hide();
			$("#rowAssignToStudent").show();
			
			// remove attachment files
			$("ul.attachments .attachment", view.el).remove();
		},

		initTimeFields : function(diaryItem) {
			//console.log("DiaryItemView::initTimeFields()");
			var view = this;

			if ( diaryItem instanceof diary.model.Task) {

				//alert('onTheDay : '+diaryItem.get('onTheDay'));
				if (diaryItem.get('onTheDay') == 1) {
					// this is an all day event
					view.inputs.allDay.prop("checked", true);
				} else {
					diary.view.DiaryItemViewUtil.setTime(view.inputs, diaryItem.get('dueTime'), 'startTime');
				}

				diary.view.DiaryItemViewUtil.setTime(view.inputs, diaryItem.get('assignedTime'), 'assignedTime');

			} else if ( diaryItem instanceof diary.model.Event) {

				if ((diaryItem.get('startTime') == null || diaryItem.get('startTime') == "0000") && (diaryItem.get('endTime') == null)) {
					// this is an all day event
					view.inputs.allDay.prop("checked", true);
				} else {
					diary.view.DiaryItemViewUtil.setTime(view.inputs, diaryItem.get('startTime'), 'startTime');
					diary.view.DiaryItemViewUtil.setTime(view.inputs, diaryItem.get('endTime'), 'endTime');
				}

			}
		},

		initFormFields : function(diaryItem, addTask, addMerit,meritItemId,meritStudentId,meritClassId,meritComment,passTypeId) {
			
			console.log("DiaryItemView::initFormFields() : "+JSON.stringify(diaryItem));

			var view = this, day = null, assignedDay = null, calendarDates;

			// determine if we need to populate the
			// the form fields from the existing diary item or we
			// have to populate with default values
			if (diaryItem != null) {

				view.initTimeFields(diaryItem);

				view.inputs.title.val(diaryItem.get('title'));
				var diaryItemTypesModel = app.model.get(ModelName.DIARYITEMTYPES);
				
				var allDiaryItemTypes = [];
				
				if ( diaryItem instanceof diary.model.Task) {
					
					var diaryItemType = diaryItemTypesModel.get(diaryItem.get('localDairyItemTypeId')) ? diaryItemTypesModel.get(diaryItem.get('localDairyItemTypeId')).toJSON() : null;
					/*alert("diaryItemType : " + diaryItem.get('localDairyItemTypeId'));
						{
							'name' 		: "Assignment",
							'formTemplate' 	: "assignment",
							'isActive' 	: true
						}; */
					allDiaryItemTypes.push(diaryItemType);
					
					var type = diaryItem.get('type').toLowerCase(), classId = diaryItem.get('classId'), 
						viewParams = {
						/* 'isAssignmentActive' : diaryItemTypesModel.isDiaryItemTypeActive('Assignment'),
						'isHomeworkActive' : diaryItemTypesModel.isDiaryItemTypeActive('Homework'),
						'asignmentFormTemplate' : diaryItemTypesModel.getFormTemplate('Assignment'),
						'homeworkFormTemplate' : diaryItemTypesModel.getFormTemplate('Homework'),
						'assignmentName' : diaryItemTypesModel.getNameForDiaryItem('Assignment'),
						'homeworkName' : diaryItemTypesModel.getNameForDiaryItem('Homework'),
						'name' 		: diaryItemTypesModel.getNameForDiaryItem('Assignment'),
						'formTemplate' 	: diaryItemTypesModel.getFormTemplate('Assignment'),
						'isActive' 	: true, */
						'allDiaryItemTypes' : allDiaryItemTypes
					};
					// initFormFields change : to set selected class
					if (classId) {
						view.inputs.subject.val(classId);
					}

					if (diaryItem.isCompleted()) {
						view.inputs.completed.prop("checked", true);
					}

					/* if (type == "assignment") {
						viewParams.assignment = true;
						viewParams.assignmentTask = true;
						viewParams.homeworkTask = false;
					} else if (type == "homework") {
						viewParams.homework = true;
						viewParams.homeworkTask = true;
						viewParams.assignmentTask = false;
					} */

					var priority = diaryItem.get('priority');
					$("select.priority option").filter(function() {
						//alert('inside priority');
						return $(this).text().toLowerCase() == priority.toLowerCase();
					}).attr('selected', true);

					var estimatedMinutes = parseInt(diaryItem.get('estimatedMinutes'),10);
					$("select.estimatedMinutes option").filter(function() {
						return $(this).text() == estimatedMinutes;
					}).attr('selected', true);

					var estimatedHours = parseInt(diaryItem.get('estimatedHours'),10);
					$("select.estimatedHours option").filter(function() {
						return $(this).text() == estimatedHours;
					}).attr('selected', true);

					view.inputs.percentage.val(0+"%");
					if (diaryItem.get('progress') != 0) {
						view.inputs.percentage.val(diaryItem.get('progress')+"%");
					}
					
					view.inputs.webLinkDesc.val(diaryItem.get('webLinkDesc'));
					
					view.inputs.weight.val(diaryItem.get('weight'));
					view.inputs.type.html(app.render('diaryItemFormEditTypes', viewParams));

					diary.view.DiaryItemViewUtil.updateVisibleRows(view.rows, view.inputs, 'task', false);

					assignedDay = diaryItem.get('assignedDay');

					day = diaryItem.get('dueDay');

				} else if ( diaryItem instanceof diary.model.Merit) {
					console.log("diaryItem instanceof diary.model.Merit");
					
					var diaryItemType = diaryItemTypesModel.get(diaryItem.get('localDairyItemTypeId')) ? diaryItemTypesModel.get(diaryItem.get('localDairyItemTypeId')).toJSON() : null;
					
					/* var diaryItemType = {
							'name' 		: "Event",
							'formTemplate' 	: "event",
							'isActive' 	: true
						}; */
					allDiaryItemTypes.push(diaryItemType);
					
					view.inputs.type.html(app.render('diaryItemFormEditTypes', {
						/* 'event' : true,
						'isEventActive' : diaryItemTypesModel.isDiaryItemTypeActive('Event'),
						'eventFormTemplate' : diaryItemTypesModel.getFormTemplate('Event'),
						'eventName' : diaryItemTypesModel.getNameForDiaryItem('Event') */
						'allDiaryItemTypes' : allDiaryItemTypes
					}));

					var classId = diaryItem.get('classId');
					if (classId) {
						view.inputs.subject.val(classId);
						var diaryItemId = diaryItem.get("uniqueId");
						view.updateStudentsList(classId,diaryItemId);
					}
					view.inputs.comments.val(diaryItem.get('description'));
					
					
					diary.view.DiaryItemViewUtil.updateVisibleRows(view.rows, view.inputs, 'merit', false);

					//alert(diaryItem.get('assignedDay') + " "+diaryItem.get('dueDay')+ " "+diaryItem.get('startDay'));
					day = diaryItem.get('assignedDay');
					
					manageIsNotify(diaryItem.get("uniqueId"));
					
					view.mode = _MODE.READONLY;
					
					if(diaryItem.get('assignedDay'))
						view.inputs.assignedDay.html(diary.view.DiaryCalendarViewUtil.formatShortDate(diaryItem.get('assignedDay')));
					view.inputs.assignedBy.html(diaryItem.get('creatorUserName'));
					//if(!(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher"))){
						//$(".mertiTitle").html("Merit Assigned");
					//}
					
					var meritType = diaryItem.get('merit_type');
					if(meritType && meritType.toLowerCase() == "demerit"){
						$(".mertiTitle").html("Concern");
					} else {
						$(".mertiTitle").html("Merit Assigned");
					}
					
					$(".ui-dialog-title").html("Merit");
					$(".editButtonContainer .midheadtxt").html("Merit");
					
				} else if ( diaryItem instanceof diary.model.Event) {
					console.log("diaryItem instanceof diary.model.Event");
					
					var diaryItemType = diaryItemTypesModel.get(diaryItem.get('localDairyItemTypeId')) ? diaryItemTypesModel.get(diaryItem.get('localDairyItemTypeId')).toJSON() : null;
					
					/* var diaryItemType = {
							'name' 		: "Event",
							'formTemplate' 	: "event",
							'isActive' 	: true
						}; */
					allDiaryItemTypes.push(diaryItemType);
					
					view.inputs.type.html(app.render('diaryItemFormEditTypes', {
						/* 'event' : true,
						'isEventActive' : diaryItemTypesModel.isDiaryItemTypeActive('Event'),
						'eventFormTemplate' : diaryItemTypesModel.getFormTemplate('Event'),
						'eventName' : diaryItemTypesModel.getNameForDiaryItem('Event') */
						'allDiaryItemTypes' : allDiaryItemTypes
					}));

					diary.view.DiaryItemViewUtil.updateVisibleRows(view.rows, view.inputs, 'event', false);

					day = diaryItem.get('startDay');

				} else if ( diaryItem instanceof diary.model.Note || diaryItem instanceof diary.model.Message) {

					// console.log('error: we do not edit notes from this view');

					day = diaryItem.get('atDay');

				} else {

					throw new Error("Diary Item is not supported");

				}

				// populate the day of the diary item
				calendarDates = view._appState.getCalendarDates(day.getDate());

				view.inputs.date.datepicker("setDate", calendarDates.currentDate);
				//console.log("initFormFields  1 :"+calendarDates.currentDate);
				view.updateDateHolder(calendarDates.currentDate, view.inputs.dateHolder);

				// populate the assigned day
				calendarDates = view._appState.getCalendarDates( assignedDay ? assignedDay.getDate() : null);

				view.inputs.assignedDate.datepicker("setDate", calendarDates.currentDate);
				//console.log("initFormFields  2 :"+calendarDates.currentDate);
				view.updateDateHolder(calendarDates.currentDate, view.inputs.assignedDateHolder);

				// Initialise attachments
				{
					// clear view variables
					view.attachmentsToAdd = [];
					view.attachmentsToRemove = new diary.collection.AttachmentCollection();

					// remove existing
					$("ul.attachments .attachment", view.el).remove();

					// add attachments
					_.forEach(diaryItem.get('attachments').toArray(), function(attachment) {

						// load icon
						var icon = diary.view.air.ViewFilesystemUtil.loadFileIcon(attachment.get('localFilePath'), {
							width : 16,
							height : 16
						});
						if (icon.error) {
							//console.log("Failed to get icon for attachment \"" + attachment.get('displayName') + "\"", icon.error);
						}

						// add row
						$("ul.attachments", view.el).append(app.render('diaryItemFormEditAttachment', {
							'id' : attachment.id,
							'displayName' : attachment.get('displayName'),
							'iconSource' : icon.source,
							'editable' : view.mode != _MODE.READONLY
						}));

					});
				}
				if ( diaryItem instanceof diary.model.Event) {
					view.inputs.noteEntry.val(diaryItem.get('description'));
				} else {
					view.inputs.description.val(diaryItem.get('description'));
				}
				setTimeout(function() {

					editor.setData(diaryItem.get('description'));
					var abc = $(".cke_editor iframe").contents().find("body").html();

					$(".cke_editor iframe").contents().find("body").css("font-size", "11px").css("line-height", "18px").css("margin", "0px").css("padding", "0 3px 0 3px")
					$(".cke_editor iframe").contents().find("p").css("padding", "0px").css("line-height", "18px").css("margin", "0px")

				}, 30);
				setTimeout(function() {

					editor2.setData(diaryItem.get('description'));
					var abc = $(".cke_editor iframe").contents().find("body").html();

					$(".cke_editor iframe").contents().find("body").css("font-size", "11px").css("line-height", "18px").css("margin", "0px").css("padding", "0 3px 0 3px")
					$(".cke_editor iframe").contents().find("p").css("padding", "0px").css("line-height", "18px").css("margin", "0px")

				}, 30);

			} else {
				
				// determines if the specified day is in the past
				// or not
				var isPastDay, isCurrentDay, calendarDay, periods, assignedTime = null, today = new Rocketboots.date.Day(new Date()), currentTime = Rocketboots.date.Time.parseTime(new Date());

				// determine if the day is in the past or not
				isPastDay = view.calendarDay.getKey() < today.getKey();
				isCurrentDay = view.calendarDay.getKey() == today.getKey();

				// this is a new diary item so we will populate the fields
				// with the default values
				if (view.defaultValues) {

					// if we know the title and the description we will use them
					// to populate the fields
					if (view.defaultValues.title) {
						view.inputs.title.val(view.defaultValues.title);
					}

					if (view.defaultValues.description) {
						view.inputs.description.val(view.defaultValues.description);
					}

					// if we know the subject we will populate the fields
					//change : initFormFields defaultValues
					if (view.defaultValues.subject) {
						view.inputs.subject.val(view.defaultValues.subject.get('classId'));
					}

				}

				if(meritClassId){
					
					if(addMerit){
						view.mode = _MODE.READONLY;
					}
					//Params for task based merits.
					$('#isMeritCreate').val("true");
					$('#meritItemId').val(meritItemId);
					$('#meritStudentId').val(meritStudentId);
					
					$('#tbMeritItemId').val(meritItemId);
					$('#tbMeritStudentId').val(meritStudentId);
					
					if(meritComment)
						view.inputs.comments.val(meritComment);
			
					//alert("isMeritCreate : "+$('#isMeritCreate').val());
					
					var classes = app.model.get(ModelName.CLASSES).toArray();
				
					var options = '';
					for (var i = 0; i < classes.length; i++) {
						if(classes[i].attributes.uniqueId == meritClassId){
							options += '<OPTION value="' + classes[i].attributes.uniqueId + '">' + classes[i].attributes.name + '</OPTION>'
						}
					}
					$("#dayDiaryItemFormEditPopup .subject").html(options);
					
					if(meritStudentId){
					
						var classUsers = app.model.get(ModelName.CLASSUSERS);
						var students = []; var toList = [];
						
						if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
							if(meritClassId){
								classUsers = getObjectByValue(classUsers, meritClassId, 'class');
							}
							students = classUsers ? classUsers.get("users") : [];
							
							for (var i = 0; students && (i < students.length); i++) {
								if(students.at(i).get('UniqueId') == meritStudentId){
									var obj1 = new Object();
									
									obj1.UserName = students.at(i).get('name') + " "+students.at(i).get('lastName')
									obj1.type = 'student'
									obj1.selected = true
									obj1.email = students.at(i).get('email')
									obj1.UserId = students.at(i).get('UniqueId')
											
									toList.push(obj1);
								}
							}
						} else {
							var obj1 = new Object();
							
							obj1.UserName = app.model.get(ModelName.STUDENTPROFILE).get('name') + " "+app.model.get(ModelName.STUDENTPROFILE).get('lastName')
							obj1.type = 'student'
							obj1.selected = true
							obj1.email = app.model.get(ModelName.STUDENTPROFILE).get('email')
							obj1.UserId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID')
									
							toList.push(obj1);
						}
						
						var viewParams = {
								'toList' : toList,
								'type' : "teacher"
						};
						var content = app.render('studentsToList',viewParams);
						$("#divTolist").html(content);
						var receiverlist = new MSDList("toList");
						receiverlist.render();
						
					}
				}
				
				view.attachmentsToAdd = [];
				
				// POPULATE the day of the diary item
				view.inputs.date.datepicker("setDate", view.calendarDay.getDate());
				//console.log("initFormFields  3 :");
				view.updateDateHolder(view.calendarDay.getDate(), view.inputs.dateHolder);

				// POPULATE the assigned day
				// if the specified date is in the past we will use the specified date
				// otherwise use the current day
				if (isPastDay) {

					// use the specified day
					view.inputs.assignedDate.datepicker("setDate", view.calendarDay.getDate());
					//console.log("initFormFields  4 :");
					view.updateDateHolder(view.calendarDay.getDate(), view.inputs.assignedDateHolder);

				} else {

					// default to the current day (today)
					calendarDates = view._appState.getCalendarDates();
					view.inputs.assignedDate.datepicker("setDate", calendarDates.currentDate);
					//console.log("initFormFields  5 :");
					view.updateDateHolder(new Rocketboots.date.Day(calendarDates.currentDate).getDate(), view.inputs.assignedDateHolder);

				}

				// POPULATE the start and end times of the form with the period / time slot start and end
				// times if they are known otherwise mark as all day
				if (!(view.periodStartTime && view.periodEndTime) && !addTask) {

					// this is an all day
					var currTime1 = new Date();
					view.inputs.allDay.prop("checked", true);
					//diary.view.DiaryItemViewUtil.setTime(view.inputs, view.periodStartTime, 'startTime');
					diary.view.DiaryItemViewUtil.setTime(view.inputs, Rocketboots.date.Time.parseTime(currTime1), 'startTime');
				} else {

					diary.view.DiaryItemViewUtil.setTime(view.inputs, view.periodStartTime, 'startTime');
					diary.view.DiaryItemViewUtil.setTime(view.inputs, view.periodEndTime, 'endTime');

				}
				
				// POPULATE the assigned time to be used
				{

					// if toady or past day was selected use the period start time as the assigned time
					if (view.periodStartTime && (isPastDay || isCurrentDay)) {

						assignedTime = view.periodStartTime;

						// if past day selected and period start time is not known default to midnight
					} else if (isPastDay) {

						assignedTime = new Rocketboots.date.Time(0, 0);

						// the assigned time could not be determined so we will try to compute
						// the time from the current active period
					} else {

						// determine the current period / time slot
						if (isPastDay) {
							calendarDay = app.model.get(ModelName.CALENDAR).getDay(view.calendarDay.getMonth() + 1, view.calendarDay.getDate(), view.calendarDay.getFullYear());
						} else {
							calendarDay = app.model.get(ModelName.CALENDAR).getDay(today.getMonth() + 1, today.getDayInMonth(), today.getFullYear());
						}

						if (calendarDay.get('timetable') && calendarDay.get('cycleDay') != null) {

							// get the periods for the day
							periods = calendarDay.get('timetable').periodsForCycleDay(calendarDay.get('cycleDay')).toArray();

							// determine the assigned time that is to be used from the
							// retrieved periods
							for (var indx = 0; indx < periods.length; indx++) {

								var periodStartTime = periods[indx].get('startTime'), periodEndTime = periods[indx].get('endTime');

								// if it is the current period we will default to
								if (currentTime.toInteger() >= periodStartTime.toInteger() && currentTime.toInteger() < periodEndTime.toInteger()) {
									//alert('assignedTime in loop ' + periodStartTime);
									//assignedTime = periodStartTime;
									break;
								}

							}

						}

					}

					// the assigned time could not be determined as either
					// we don't have any periods for today or we are not in
					// a cycle day period (i.e. before school or after school)
					if (assignedTime == null) {
						assignedTime = currentTime;
					}

					diary.view.DiaryItemViewUtil.setTime(view.inputs, assignedTime, 'assignedTime');
					if(addTask){
						diary.view.DiaryItemViewUtil.setTime(view.inputs, assignedTime, 'startTime');
						diary.view.DiaryItemViewUtil.setTime(view.inputs, assignedTime, 'endTime');
					}
				}

			}

			// hide or show the form as per the type of diary item being shown
			view.updateDiaryItemType(addMerit);
			
			//alert("edit merit flow");
			getMerits();
			
			var meritsModel = app.model.get(ModelName.MERITS);
			
			var merits = [];
			var isCurrentMerit = false;
			
			
			var passtype_id;
			if(diaryItem){
				passtype_id = diaryItem.get('passtype_id');
			} else if(passTypeId){
				passtype_id = passTypeId;
			} else {
				passtype_id = $(".addedMerit").attr("passtype_id");
			}
			
			for(var i = 0 ; i < meritsModel.length; i++){
				
				var merit = meritsModel.at(i);
				//console.log(" inside "+ (passtype_id && (passtype_id == merit.get('UniqueID'))));
				if(passtype_id && (passtype_id == merit.get('UniqueID'))){
					isCurrentMerit = true;
				} else if(!passtype_id && i == 0){
					isCurrentMerit = true;
				} else {
					isCurrentMerit = false;
				}
				
				if(isCurrentMerit){
					merits.unshift(this.mapMerit(merit,isCurrentMerit));
				} else {
					merits.push(this.mapMerit(merit,isCurrentMerit));
				}
			}
			
			
			$("#banner-fade").html(app.render('meritSliderData', {
				'merits' : merits
			}));
			
			//alert("merits "+JSON.stringify(merits));
			
			//To keep
			$('#banner-fade').bjqs({
				height      : 120,
				width       : 120,
				automatic   : false,
				animduration : 0,
				showmarkers     : false
			});
			
			
			$(".studentsRow .MSDList_ListTableBody .MSDList_ListCheckBox").css({
				"margin-top" : "-2px !important",
				"height" : "10px"
			});
			
			if((diaryItem && diaryItem.get('type') == "MERIT") || passTypeId) 
			{ 
				//To disable task based merit isnotify
				$(".isNotifyRow .isNotify").attr("disabled", "disabled");
				
				$(".studentsRow .MSDList_ListTableBody .MSDList_ListCheckBox").attr("disabled", "disabled");
						
				view.mode = _MODE.READONLY;
				$("li.bjqs-next").hide();
				$("li.bjqs-prev").hide();
			} else {
				
				if(addMerit && meritItemId && (app.model.get(ModelName.STUDENTPROFILE).get("isTeacher"))){
					
					//To add task based Merit.
					view.mode = _MODE.CREATE;
				}
				
				
				$("li.bjqs-next").show();
				$("li.bjqs-prev").show();
			}
			
			// disable or enable input fields
			for (var key in view.inputs) {

				var $input = view.inputs[key], isDateInput = key == 'date' || key == 'assignedDate';

				if (view.mode == _MODE.READONLY) {

					// if the input is a date picker we need to disable/enable
					// it differently then other inputs
					if (isDateInput) {
						$input.datepicker("disable");
					} else {
						$input.attr("disabled", "disabled");
					}
					
					view.inputs.completed.attr('disabled', false);
					$(".studentsRow #selectAll").attr('disabled', true);
					
					if(!(app.model.get(ModelName.STUDENTPROFILE).get('isTeacher'))){
						$(".studentsRow .MSDList").append("<div class='ckedoitorDisableCover2'>");
						$(".ckedoitorDisableCover2").css({
							"position" : "absolute",
							"left" : "0px",
							"top" : "0px",
							"width" : "200px",
							"height" : "25px",
							"z-index" : "99999999",
							"backgrond" : "rgba(255,2555,255,0)"
						});
					}
					

				} else {

					// if the input is a date picker we need to disable/enable
					// it differently then other inputs
					if (isDateInput) {
						$input.datepicker("enable");
					} else {
						$input.removeAttr("disabled");
					}
					$(".studentsRow #selectAll").attr('disabled', false);
					$(".ckedoitorDisableCover").remove();
				}

			}
			
			
			if(addMerit){
				$(".studentsRow #selectAllDiv").hide();
				$(".subjectRow .subject").attr("disabled", "disabled");
				view.inputs.type.attr("disabled", "disabled");
				
				if(!(diaryItem && diaryItem.get('type') == "MERIT") || !(app.model.get(ModelName.STUDENTPROFILE).get('isTeacher'))){
					$(".studentsRow .MSDList").append("<div class='ckedoitorDisableCover2'>");
					$(".ckedoitorDisableCover2").css({
						"position" : "absolute",
						"left" : "0px",
						"top" : "0px",
						"width" : "200px",
						"height" : "25px",
						"z-index" : "99999999",
						"backgrond" : "rgba(255,2555,255,0)"
					});
				}
				
				if(meritItemId){
					
					if(app.model.get(ModelName.STUDENTPROFILE).get('isTeacher')){
						$(".ui-dialog-title").html("Merit");
						$(".editButtonContainer .midheadtxt").html("Merit");
						
						manageIsNotify(meritItemId,meritStudentId);
					} else {
						app.locator.getService(ServiceName.APPDATABASE).runQuery(
							 // query
							//"SELECT title, creatorUserName, assignedDay, passtype_id FROM diaryItem WHERE UniqueID = "+meritItemId,
							"SELECT di.title, teac.name as creatorUser, di.creatorUserName, diu.meritAssignedDate, diu.isnotify, " +
							" di.passtype_id, diu.passtype_name, lov.Type FROM diaryItem di "+
							" LEFT OUTER JOIN diaryItemUser diu on diu.item_id = di.UniqueID " +
							" LEFT OUTER JOIN listOfValues lov on diu.passtype_id = lov.UniqueID " +
							" LEFT OUTER JOIN myTeacher teac on teac.teacher_id = di.createdBy WHERE di.UniqueID = "+meritItemId,
							// params
							{},

							 function success(statement)
							 {
								var detail=_.map(statement.getResult().data, function(item) {
									return item;
								});
							
								if(detail.length)
								{
									view.inputs.taskTitle.html(detail[0].title);
									view.inputs.assignedBy.html(detail[0].creatorUser ? detail[0].creatorUser : detail[0].creatorUserName);
									view.rows.taskTitleRow.show();
									if(detail[0].meritAssignedDate){
										view.inputs.assignedDay.html(diary.view.DiaryCalendarViewUtil.formatShortDate(Rocketboots.date.Day.parseDayKey(detail[0].meritAssignedDate)));
									}
									if(detail[0].isnotify){
										$(".isNotifyRow .isNotify").prop("checked",true);
									} else {
										$(".isNotifyRow .isNotify").prop("checked",false);
									}
									
									$(".ui-dialog-title").html("Merit");
									$(".editButtonContainer .midheadtxt").html("Merit");
									
									if(detail[0].Type && detail[0].Type.toLowerCase() == "demerit"){
										$(".mertiTitle").html("Concern");
									} else if(detail[0].Type && detail[0].Type.toLowerCase() == "merit"){
										$(".mertiTitle").html("Merit Assigned");
									}
									
								}
								
							 },
							 // error
							 function error()
							{
							}
						);
					}
				}
				
			}
			
			if(!(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher"))){
				$(".studentsRow #selectAllDiv").hide();
			} 
			//Added temp fix to disable/enable comments field in Merit
			/* else {
				if(view.inputs.comments.val() == ""){
					view.inputs.comments.removeAttr("disabled");
				}
			} */
			
			if (view.mode == _MODE.READONLY) {
				//console.log("locked item");
				// also hide attachments row
				//view.rows.attachments.hide();
				$("#rowAssignToStudent").hide();
				// and hide buttons
				//$(".editButtonContainer", view.el).hide();

			} else {
				var createdBy = "";
				if (diaryItem != null){
					createdBy = diaryItem.get('createdBy');
				}
				
				$(".editButtonContainer", view.el).show();

				if (view.mode == _MODE.UPDATE) {
					// show delete button
					if (createdBy == UserId) {
						view.inputs.deleteBtn.show();
					} else {
						//TODO: Show lock sign
					}
				}
			}
			
			$("#divAddEditItem").show();
		},
		
		mapMerit : function (merit,isCurrentMerit) {
			
			return {
				'Description' 	: merit.get('Description'),
				'iconName' 		: "app-storage:/MeritIcons/"+merit.get('iconName'),
				'isActive' 		: merit.get('isActive'),
				'UniqueID' 		: merit.get('UniqueID'),
				'DiaryItemType' : merit.get('DiaryItemType'),
				'Type' 			: merit.get('Type'),
				'Value' 		: merit.get('Value'),
				'isCurrentMerit' : isCurrentMerit,
				'id' 			: merit.get('id')
			};
			
		},
		
		assignTask : function(event) {

			var val = $(event.currentTarget).val();
			if (val == "me") {
				$("#saveItemBtn").text("Save");
			} else {
				$("#saveItemBtn").text("Next");
			}
		}, 
		fetchClassDates: function(currDate) {
			
			var displayForAllDay = false;
			
			var donIndex = 0;
			var strDONDate = '';
			
			//Added donOvrCycleDay = 1 as each date was starting form cycle day 0.
			var donOvrCycleDay = 1;
			var dtDONDate = null;
			
			var count = 0;
			var arrClassCnt = 0;
			var arrClassLen = 0;
			
			var arrSemDtLen = 0;
			
			var selClassId = '';
			var currMonth = 0;
			var currYear = 0;
			
			var firstDate = null;
			var lastDate = null;
			
			
			var ttStartDate = null;
			var ttEndDate = null;
			
			var strStartDate = null;
			var strEndDate = null;
			
			var semStartDate = null;
			var semEndDate = null;
			
			var dtStartDate = null;
			var dtEndDate = null;
			var arrSemesterDate = new Array();
			
			var arrDON = null;
			var arrClass = null;
			
			var arrClassCycleDays = new Array();
			
			var arrDatesCycle = new Array();
			var dateObj = null;
			
			
			var timeTableId = '';
			var ttCycleDays = '';
			var cycleLength = 0;
			
			var arrClassDates = new Array();
			
			var arrHldCount = 0;
			var arrHldLen = 0;
			
			var strHolidayDate = null;
			var dtHoliday = null;
			
			var arrHolidays = null;
			var arrHlDate = new Array();
			
			selClassId = this.inputs.subject.val();
			
//			if (currDate.getDate() == 1)
//			{
//				this.arrDates.length = 0;
//			}
//				
//			if (this.arrDates.length == 0 && currDate.getDate() == 1 && (selClassId != null && selClassId != 0))
//			{
//				selClassId = this.inputs.subject.val();
//				
//				currYear = currDate.getFullYear();
//				currMonth = (currDate.getMonth() + 1);
//				
//				firstDate = currYear + (currMonth < 10 ? ('0' + currMonth) : ('' + currMonth)) + '01';
//				lastDate = currYear + (currMonth < 10 ? ('0' + currMonth) : ('' + currMonth)) + new Date(currYear, currDate.getMonth() + 1, 0).getDate();
//				
//				var selectedDay = new Rocketboots.date.Day(),
//						timetables = app.model.get(ModelName.CALENDAR).get('timetables'),
//						timetable = timetables.getCurrentAt(selectedDay);
//				
//				var semStartDay =  (timetable && timetable.get('startDay')) ?  timetable.get('startDay').getKey() : 0;
//				
//				app.locator.getService(ServiceName.APPDATABASE).runQuery(
//					// Query to find day of notes
//					"SELECT * FROM dayOfNote WHERE overrideCycleDay != 0 AND includeInCycle = 1 and date >= :semStartDay order by date",
//					
//					// Parameters
//					{
//						'semStartDay' : semStartDay
//					},
//					
//					function success(statement)
//	            	{
//						arrDON = _.map(statement.getResult().data, function(item) {
//	                        return item;
//	                    });
//						
//					},
//	            	// error
//	            	function error()
//	            	{
//	                
//	            	}
//				
//				);
//				
//				
//				
//				app.locator.getService(ServiceName.APPDATABASE).runQuery(
//					
//					// Query to fetch timetable details, cycle days, cycle length for given subject 
//					"SELECT DISTINCT c.timetable, c.cycleDay, tt.cycleDays, tt.cycleLength, tt.startDate, tt.EndDate FROM class c, timetable tt WHERE c.UniqueId = :uniqueId AND c.timetable = tt.id",
//					
//					// Parameters
//					{
//						uniqueId: selClassId
//						//startDate: firstDate,
//						//endDate: firstDate
//					},
//					
//					function success(statement)
//	            	{
//	            		arrClass = _.map(statement.getResult().data, function(item) {
//	                        return item;
//	                    });
//	            		
//	            		arrClassLen = arrClass.length;
//	            		
//	            		if (arrClassLen > 0)
//	            		{
//	            			timeTableId = arrClass[0].timetable;
//							ttCycleDays = arrClass[0].cycleDays;
//							cycleLength = arrClass[0].cycleLength;
//							ttStartDate = timetable ? timetable.get('startDay').getKey() : arrClass[0].startDate;
//							ttEndDate   = timetable ? timetable.get('endDay').getKey() : arrClass[arrClassLen - 1].EndDate;
//							
//	            		}
//	            		
//	            		if (ttStartDate != null)
//	            		{
//	            			ttStartDate = ttStartDate + '';
//		            		ttEndDate = ttEndDate + '';
//	            			
//	            			strStartDate = ttStartDate.substr(0, 4) + "/" + ttStartDate.substr(4, 2) + "/" + ttStartDate.substr(6, 2);
//	            			strEndDate = ttEndDate.substr(0, 4) + "/" + ttEndDate.substr(4, 2) + "/" + ttEndDate.substr(6, 2);
//	            			
//	            			semStartDate = new Date(strStartDate);
//	            			semEndDate = new Date(strEndDate);
//	            			
//	            			dtStartDate = new Date(semStartDate);
//	            			dtEndDate = new Date(semEndDate);
//	            		}
//	            		
//	            		//alert('currDate = ' + currDate)
//	            		//dtStartDate = new Date(currDate.getFullYear(), currDate.getMonth(), 1, 0, 0, 0, 0);
//	            		//dtEndDate = new Date(currDate.getFullYear(), currDate.getMonth(), 31, 0, 0, 0, 0);
//	            		
//	            		/*
//	            		if (semStartDate != null && (semStartDate.getTime() > dtStartDate.getTime()))
//	            		{
//	            			dtStartDate = new Date(semStartDate);
//	            		}
//	            		
//	            		if (semEndDate != null && (dtEndDate.getTime() > semEndDate.getTime()))
//	            		{
//	            			dtEndDate = new Date(semEndDate);
//	            		}
//	            		*/
//	            		
//	            		//alert('dtStartDate = ' + dtStartDate)
//	            		//alert('dtEndDate = ' + dtEndDate)
//	            		
//	            		//alert('dtStartDate = ' + dtStartDate)
//	            		
//	            		
//	            		if (dtStartDate != null && dtEndDate != null)
//	            		{
//	            			while(dtStartDate <= dtEndDate) 
//	            			{
//	            				if (dtStartDate.getDay() >= 1 && dtStartDate.getDay() <= 5)
//	            				{	
//	            					arrSemesterDate.push(dtStartDate);
//	            				}
//	            				
//	            				dtStartDate = new Date(new Date(dtStartDate).setDate(new Date(dtStartDate).getDate() + 1));
//	            			}
//	            			
//	            		}
//	            		
//	            		//console.log('arrSemesterDate = ' + arrSemesterDate)
//	            		
//	            		//alert('arrClassLen = ' + arrClassLen)
//	            		
//	            		for (arrClassCnt = 0; arrClassCnt < arrClassLen; arrClassCnt++)
//	            		{
//	            			if (arrClass[arrClassCnt].cycleDay == 0)
//	            			{
//	            				displayForAllDay = true;
//	            			}
//	            			arrClassCycleDays[arrClassCnt] = arrClass[arrClassCnt].cycleDay; 
//	            		}
//	            		
//	            		if (displayForAllDay)
//	            		{
//	            			arrClassCycleDays = new Array();
//	            			for (arrClassCnt = 0; arrClassCnt < cycleLength; arrClassCnt++)
//		            		{
//	            				arrClassCycleDays[arrClassCnt] = (arrClassCnt + 1);
//		            		}
//	            		}
//	            		
//	            		//alert('cycle days = ' + arrClassCycleDays)
//	            		
//	            		//alert('arrSemesterDate = ' + arrSemesterDate)
//	            		
//	            		//alert('  arrClassDates = ' +  arrClassDates)
//	            		
//	            		if (arrSemesterDate != null && arrSemesterDate.length > 0)
//	            		{
//	            			arrSemDtLen = arrSemesterDate.length;
//	            		}
//	            		
//	            		donIndex = 0;
//	            		
//	            		
//	            		
//	            		for (count = 0; count < arrSemDtLen; count++)
//	            		{
//	            			if(arrDON[donIndex]){
//		            			strDONDate = arrDON[donIndex].date.toString().substr(0, 4) + "/" + arrDON[donIndex].date.toString().substr(4, 2) + "/" + arrDON[donIndex].date.toString().substr(6, 2);
//		            			
//		            			//alert('strDONDate = ' + strDONDate)
//		            			
//		            			dtDONDate = new Date(strDONDate);
//		            			
//		            			if (dtDONDate.getTime() == arrSemesterDate[count].getTime())
//		            			{
//		            				donOvrCycleDay =  arrDON[donIndex].overrideCycleDay;
//		            				donIndex = donIndex + 1;
//		            			}
//	            			}
//	            			
//	            			dateObj = new Object();
//	            			
//	            			//console.log(" **** DATE = " + arrSemesterDate[count] + " *** donOvrCycleDay = " + donOvrCycleDay)
//	            			
//	            			dateObj.date = arrSemesterDate[count];
//	            			dateObj.cycleDay = donOvrCycleDay;
//	            			
//	            			arrDatesCycle[arrDatesCycle.length] = dateObj;
//            				
//            				donOvrCycleDay = donOvrCycleDay + 1;
//            				
//            				if (donOvrCycleDay > cycleLength)
//            				{
//            					donOvrCycleDay = 1;
//            				}
//	            			
//	            			
//	            		}
//	            		
//	            		for (count = 0; count < arrDatesCycle.length; count++)
//	            		{
//	            			//console.log('date = ' + arrDatesCycle[count].date +  '  :: cycle day = ' + arrDatesCycle[count].cycleDay )
//	            			
//	            			if (arrClassCycleDays.indexOf(arrDatesCycle[count].cycleDay) != -1)
//	            			{
//	            				arrClassDates[arrClassDates.length] = arrDatesCycle[count].date;
//	            			}
//	            		}
//	            		
//	            		//alert('changed  arrClassDates = ' +  arrClassDates)
//	            	},
//					// error
//	            	function error()
//	            	{
//	                
//	            	}
//	            	
//				);
//				
//				// Find out holidays in given period
//				app.locator.getService(ServiceName.APPDATABASE).runQuery(
//				
//					// Query to find holidays in given month
//					//"SELECT date FROM dayOfNote WHERE (isGreyedOut = 1 OR (isGreyedOut = 0 AND overrideCycleDay NOT IN (" + excludeOverrideCycle + "))) AND date BETWEEN :startDate AND :endDate",
//					//Added includeInCycle = 0 condition as this date should not be highlighted.
//					"SELECT date FROM dayOfNote WHERE (isGreyedOut = 1 OR includeInCycle = 0) AND date BETWEEN :startDate AND :endDate",	
//					
//					// Parameters
//					{
//						startDate: firstDate,
//						endDate: lastDate
//					},
//					// Success function
//					function success(statement)
//	            	{
//						arrHolidays = _.map(statement.getResult().data, function(item) {
//	                        return item;
//	                    }); 
//						
//						if (arrHolidays != null)
//						{
//							arrHldLen = arrHolidays.length;
//						}
//						
//						for (arrHldCount = 0; arrHldCount < arrHldLen; arrHldCount++)
//	            		{
//							arrHlDate[arrHldCount] = arrHolidays[arrHldCount].date; 
//	            		}
//						
//						arrHldLen = 0;
//						
//						if (arrHlDate != null && arrHlDate.length > 0)
//						{
//							arrHldLen = arrHlDate.length;
//							
//							arrClassCnt = 0; 
//							arrClassLen = arrClassDates.length;
//							
//							for (arrHldCount = 0; arrHldCount < arrHldLen; arrHldCount++)
//							{
//								strHolidayDate = arrHlDate[arrHldCount].toString().substr(0, 4) + "/" + arrHlDate[arrHldCount].toString().substr(4, 2) + "/" + arrHlDate[arrHldCount].toString().substr(6, 2);
//								
//								dtHoliday = new Date(strHolidayDate);
//								
//								for (arrClassCnt = 0; arrClassCnt < arrClassLen; arrClassCnt++)
//			            		{
//			            			if (dtHoliday.getTime() == arrClassDates[arrClassCnt].getTime())
//			            			{
//			            				arrClassDates.splice(arrClassCnt, 1);
//			            				
//			            				arrClassLen = arrClassDates.length;
//			            			}
//			            		}
//								
//							}
//						}
//						
//						//this.arrDates = arrClassDates;
//						
//						
//	            	},
//					// error
//	            	function error()
//	            	{
//	                
//	            	}
//						
//					
//				);
//				
//				//alert('currDate.getDate() = ' + currDate.getDate());
//				this.arrDates = arrClassDates;
//			}
			//alert("fetchClassDates 1 "+selClassId);
			//this.arrDates = new Array();
			if (this.arrDates.length == 0 && currDate.getDate() == 1 && (selClassId != null && selClassId != 0))
			{
				selClassId = this.inputs.subject.val();
				var semesterStartDate, semesterEndDate, timetableId;
				var selectedDate = app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay').getKey();
				
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
						
					// Query to fetch timetable details, cycle days, cycle length for given subject 
						"select distinct t.startdate, t.enddate, t.id as timetableId from class c left outer join timetable t on t.id = c.timetable where c.campusid in (select distinct(class.campusid) from class where class.uniqueId = "+ selClassId +") and c.uniqueId = "+ selClassId+ " and t.startdate  <= "+selectedDate+" and t.enddate >= "+selectedDate,
					
					// Parameters
					{
						//endDate: firstDate
					},
					
					function success(statement)
	            	{
						var semDates = _.map(statement.getResult().data, function(item) {
	                        return item;
	                    });
						
						if(semDates[0])
	                    {
							semesterStartDate = semDates[0].startDate;
							semesterEndDate = semDates[0].EndDate;
							timetableId = semDates[0].timetableId;
	                    }
	            	},
					// error
	            	function error()
	            	{
	                
	            	}
				);
				
				//alert("fetchClassDates 2 "+semesterStartDate);
				//alert("fetchClassDates 3 "+semesterEndDate);
				
				if(semesterStartDate && semesterEndDate && timetableId){
					app.locator.getService(ServiceName.APPDATABASE).runQuery(
						// Query to find day of notes
						"select ccd.* from calendarcycleday ccd ,class c  where ccd.campusid = c.campusid and c.UniqueID = "+ selClassId +" and "+
						"ccd.date >= "+semesterStartDate+" and ccd.date <= "+semesterEndDate+" and ccd.cycleday in (select cycleday from class where class.UniqueID = "+ selClassId +" and class.timetable = "+timetableId+")",
						
						// Parameters
						{
							//'semStartDay' : semStartDay
						},
						
						function success(statement)
		            	{
							arrCalDates = _.map(statement.getResult().data, function(item) {
		                        return item;
		                    });
							
							for (count = 0; count < arrCalDates.length; count++)
		            		{
		            			if (arrCalDates[count].date)
		            			{
		            				
		            				strDate = arrCalDates[count].date.toString().substr(0, 4) + "/" + arrCalDates[count].date.toString().substr(4, 2) + "/" + arrCalDates[count].date.toString().substr(6, 2);
			            			classDate = new Date(strDate);
			            			arrClassDates[arrClassDates.length] = classDate;
		            				//console.log('arrCalDates = '+classDate);
		            			}
		            		}
							
						},
		            	// error
		            	function error()
		            	{
		                
		            	}
					
					);
				}
				this.arrDates = arrClassDates;
			}
		},
		updatehighlightDateStyle: function() {
			
			var classCnt = 0;
			var classLen = 0;
			
			var styleString = '';
			
			var classesObj = null;
			
			getAllClasses();
			//console.log("app.model.get(ModelName.CLASSES)=  "+  JSON.stringify(app.model.get(ModelName.CLASSES)));
			classesObj = app.model.get(ModelName.CLASSES);
			
			classLen = classesObj.length;
			
			styleString = styleString + ' <style> ';
			
			for (classCnt = 0; classCnt < classLen; classCnt++)
			{
				styleString = styleString + ' .highlight_date_' + classesObj.at(classCnt).get('uniqueId') + ' a ';
				styleString = styleString + '{background: ' + classesObj.at(classCnt).get('Code') + ' !important;}';
			}
			
			styleString = styleString + ' </style> ';
			$("#highlightDateStyle").html(styleString);
		}
		
		
	});

	return view;

})(); 