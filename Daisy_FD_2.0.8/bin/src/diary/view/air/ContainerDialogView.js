diary = diary || {};
diary.view = diary.view || {};
diary.view.air = diary.view.air || {};

diary.view.air.ContainerDialogView = (function() {
	
	var view = Backbone.View.extend({

		viewShown: false,
		updateHtmlLoader: null,
        isAppError: false,
        settings : {},
        retrying: false,

		initialize: function() {
			//console.log('ContainerDialogView.initialize()');

			diary.view.ThemeUtil.registerThemeListener(view);

			this.render();
		},
	
		showWindow: function(_options) {
			//console.log('ContainerDialogView.showWindow()');
			
			var view = this;

            view.settings = $.extend({
                title : "",
                message : "",
                isError : false,
                disableClose: false,
                actions : {},
                retryCallback : null
            }, _options);

            view.isAppError = view.settings.isError;
            view.retrying = false;

			if (view.dialogHtmlLoader != null) return;
			
			var windowInitOptions = new air.NativeWindowInitOptions();
			windowInitOptions.type = air.NativeWindowType.LIGHTWEIGHT;
			windowInitOptions.systemChrome = air.NativeWindowSystemChrome.NONE;
			windowInitOptions.transparent = true;

            var tokenWidth = 520,
				tokenHeight = 507,
				tokenX = (air.Capabilities.screenResolutionX - tokenWidth) / 2,
				tokenY = (air.Capabilities.screenResolutionY - tokenHeight) / 2,
				bounds = new air.Rectangle(tokenX, tokenY, tokenWidth, tokenHeight),
				initialUrl = new air.URLRequest("dialog.html");
		
			view.dialogHtmlLoader = air.HTMLLoader.createRootWindow(
				false,
				windowInitOptions,
				false,
				bounds
			);
			
			view.clippedMask = new au.com.productdynamics.view.ClippedRoundedWindow(view.dialogHtmlLoader);

			view.dialogHtmlLoader.window.nativeWindow.alwaysInFront = true;

            var updateHtmlLoaderComplete = function(event) {
                try {
                   // console.log('ContainerDialogView.updateHtmlLoaderComplete()');

					if (!view.dialogHtmlLoader) {
						//console.log('ContainerDialogView has already been closed');
						return;
					}
					
                    view.dialogHtmlLoader.removeEventListener("complete", updateHtmlLoaderComplete);

					// Required for ThemeUtil
					view.document = view.dialogHtmlLoader.window.document;
                    view.viewShown = true;
                    view.render();

                    _.defer(function() {
                        view.dialogHtmlLoader.window.displayMessage(view.settings.message, view.settings.isError);
                    });

                    view.dialogHtmlLoader.window.nativeWindow.visible = true;

                    view.dialogHtmlLoader.window.addEventListener('startDrag', function(event) {
                        view.dragView.apply(view, [event]);
                    });

                    view.dialogHtmlLoader.window.addEventListener('closeWindow', function(event) {
                        if (view.settings.disableClose) return;
                        view.hideWindow();
                        if (view.isAppError) {
                            window.nativeWindow.close();
                        }
                    });

                    view.dialogHtmlLoader.window.addEventListener('retry', function(event) {
                        if (view.retrying) return;

                        if (typeof(view.settings.retryCallback) === "function") {
                            view.settings.retryCallback();
                            view.retrying = true;
                        }
                    });

                } catch (e) {
                    //console.log(new Date(), e);
                }
            };

            //console.log("Adding onComplete event listener", new Date());

            view.dialogHtmlLoader.addEventListener("complete", updateHtmlLoaderComplete);

            view.dialogHtmlLoader.load(initialUrl);

        },
		
		hideWindow: function() {
           // console.log("ContainerDialogView.hideWindow()");

			var view = this;

			if (view.dialogHtmlLoader == null) return;

            view.dialogHtmlLoader.window.nativeWindow.close();
			view.dialogHtmlLoader = null;
			view.viewShown = false;
		},
		
		showBusy: function() {
			view.dialogHtmlLoader.window.showBusy();
		},
		
		hideBusy: function() {
			view.dialogHtmlLoader.window.hideBusy();
		},
		
		render: function() {
			//console.log('ContainerDialogView.render()');
			
			var view = this;

			if (!view.viewShown) return;
			
			var params = {
                "title" : view.settings.title,
                "showRetry" : _.has(view.settings.actions, 'retry'),
                "showClose" : _.has(view.settings.actions, 'close')
            };
			var content = app.render('containerDialog', params);
			view.dialogHtmlLoader.window.injectContent(content);

			diary.view.ThemeUtil.renderThemeFromSchoolProfile(view);
		},
		
		dragView: function(event) {
			var view = this;
			view.dialogHtmlLoader.window.nativeWindow.startMove();
		}
	});

	return view;

})();
