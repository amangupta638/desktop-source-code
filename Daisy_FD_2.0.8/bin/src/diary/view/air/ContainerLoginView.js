diary = diary || {};
diary.view = diary.view || {};
diary.view.air = diary.view.air || {};

diary.view.air.ContainerLoginView = (function() {
	
	var view = Backbone.View.extend({

		viewShown: false,
		loginHtmlLoader: null,
		internetAvailable: false,
		internetAvailable: false,
		
		initialize: function(){
			
			diary.view.ThemeUtil.registerThemeListener(this);
			this.render();
		},
		showLogin: function(requestType) {
		
			var view = this;
			view.handledClose = false;
			//requestType = requestType || diary.view.air.ContainerTokenView.REQUESTTYPE.REGISTER;
			//view.isUpdateRequest = requestType == diary.view.air.ContainerTokenView.REQUESTTYPE.UPDATE;
			view.isUpdateRequest = requestType;
			//alert("requestType : "+view.isUpdateRequest);
			
			if (view.loginHtmlLoader != null) return;
			
			var windowInitOptions = new air.NativeWindowInitOptions();
			windowInitOptions.type = air.NativeWindowType.LIGHTWEIGHT;
			windowInitOptions.systemChrome = air.NativeWindowSystemChrome.NONE;
			windowInitOptions.transparent = true;
			
			var loginWidth = air.Capabilities.screenResolutionX-430,
			loginHeight = air.Capabilities.screenResolutionY-200,
			loginX = (air.Capabilities.screenResolutionX - loginWidth) / 2,
			loginY = (air.Capabilities.screenResolutionY - loginHeight) / 2,
			bounds = new air.Rectangle(loginX, loginY, loginWidth, loginHeight),
			initialUrl = new air.URLRequest("login.html");
			
			view.loginHtmlLoader = air.HTMLLoader.createRootWindow(
				false,
				windowInitOptions,
				false,
				bounds
			);
			
			view.clippedMask = new au.com.productdynamics.view.ClippedRoundedWindow(view.loginHtmlLoader);

			view.loginHtmlLoader.window.nativeWindow.alwaysInFront = true;
			
			function loginHtmlLoaderComplete(event) {
				view.loginHtmlLoader.removeEventListener("complete", loginHtmlLoaderComplete);
				// Required for ThemeUtil
				view.document = view.loginHtmlLoader.window.document;
						
				view.render();

				view.loginHtmlLoader.window.nativeWindow.visible = true;
				view.loginHtmlLoader.window.addEventListener('processLogin', function(event) {
					view.processLogin.apply(view, [event]);
				});
				view.loginHtmlLoader.window.addEventListener('closeLogin', function(event) {
					view.hideLogin();
					if (!view.isUpdateRequest) {
						view.handledClose = true;
						window.nativeWindow.close();						
					}
				});

				view.loginHtmlLoader.window.addEventListener('startDrag', function(event) {
					view.dragView.apply(view, [event]);
				});

			}
			
			function loginHtmlLoaderClose(event) {
				if (!view.handledClose) {	// If this isn't an explicitly handled close
					if (!view.isUpdateRequest) {	// and if it isn't an Update Token request
						air.NativeApplication.nativeApplication.exit();
					}
				}
			}
			
			view.loginHtmlLoader.addEventListener("complete", loginHtmlLoaderComplete);
			view.loginHtmlLoader.parent.stage.nativeWindow.addEventListener("close", loginHtmlLoaderClose);
			//alert("view.viewShown : "+view.viewShown);
			if(!view.viewShown){
				view.loginHtmlLoader.load(initialUrl);
				view.viewShown = true;	
			}
			
		},
		
		
		hideLogin: function() {
			
			var view = this;

			if (view.loginHtmlLoader == null) return;
			 app.context.off(EventName.NETWORKSTATUSCHANGE, view.onNetworkStatusChange);
			 view.handledClose = true;
			 view.loginHtmlLoader.window.nativeWindow.close();
			 view.loginHtmlLoader = null;
			 view.viewShown = false;
		},
		
		render: function() {
			var view = this;
			if (!view.viewShown) return;
			var viewParams = {
					'registered' : view.isUpdateRequest
			};
			var loginContent = app.render('loginView', viewParams);
			//alert("view.loginHtmlLoader.window : "+view.loginHtmlLoader.window.injectContent);
			view.loginHtmlLoader.window.injectContent(loginContent);
		},
		
		showprogress: function(element)
		{
		  var view = this;
            if (!view.viewShown) return;
            view.loginHtmlLoader.window.injectProgress(element);
		},
		processLogin: function(event) {
			var view = this;
			var email = event.email;
			//view.loginHtmlLoader.window.displayMessage('');
			view.loginHtmlLoader.window.disableForm();
			function processLoginSuccess(message) {
				//view.hideLogin();
				//alert("processLoginSuccess");
				view.loginHtmlLoader.window.displayMessage(message, true);
				//app.context.off(EventName.USERREGISTERED, processLoginSuccesss);
				//app.context.off(EventName.USERREGISTRATIONFAILED, processLoginFailed);
				view.loginHtmlLoader.window.enableForm(view.internetAvailable);
				
				app.context.off(EventName.NETWORKSTATUSCHANGE, view.onNetworkStatusChange);
                if (view.isUpdateRequest) {
                    app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.UPDATE_USER, email);
                    app.context.trigger(EventName.RESETAPPLICATION);
                } else {
                    app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.REGISTER_USER, email);
                    app.context.trigger(EventName.STARTAPPLICATION);
                }
            }
			
			function processLoginFailed(message) {
				$(".mainLogin #busy").hide();
				//alert("processLoginFailed");
				app.context.off(EventName.USERREGISTERED, processLoginSuccess);
				app.context.off(EventName.USERREGISTRATIONFAILED, processLoginFailed);
				view.loginHtmlLoader.window.displayMessage(message, true);
				view.loginHtmlLoader.window.enableForm(view.internetAvailable);
			}
			
			app.context.on(EventName.USERREGISTERED, processLoginSuccess);
			app.context.on(EventName.USERREGISTRATIONFAILED, processLoginFailed);
			app.context.trigger(EventName.REGISTERUSER, event.email, event.password, event.domain); 
		},
		dragView: function(event) {
			var view = this;
			view.loginHtmlLoader.window.nativeWindow.startMove();
		},
		onNetworkStatusChange : function(available) {
			var view = this;
			view.internetAvailable = true;
			if (view.loginHtmlLoader.window.displayMessage) {
				if (view.internetAvailable) {
					view.loginHtmlLoader.window.displayMessage("", false);
				} else {
					view.loginHtmlLoader.window.displayMessage("Please connect to the internet to continue registration", true);
				}
				view.loginHtmlLoader.window.enableForm(view.internetAvailable);
			}
		}
	});

	return view;

})();
