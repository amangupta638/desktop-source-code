diary = diary || {};
diary.view = diary.view || {};
 
diary.view.MyClassesView = (function() {
		var reminderParam,
		var assignedTasks, //classTaskList, attrValue, attribute,
		
		function getObjectByValue(classTaskList, attrValue, attribute){
			//alert("collection : "+JSON.stringify(classTaskList.at(0)));
			var obj;
			//alert("attrValue : "+attrValue +"attribute : "+attribute)
			for(var len = 0 ; len < classTaskList.length; len++){
				if(classTaskList.at(len).get(attribute) == attrValue){
					obj =  classTaskList.at(len);
				}
			}
			return obj;
		}
	var myClassesView = Rocketboots.view.View.extend({
		
			
		events: {
			"click #myClassClassList .classes_tbl_lt .classList"		    		: "showTask",		// hide overdue items
			"click #doneBtn"		    						: "showPopup",
			"click .taskListRow"								: "showStudent",
			"click #cancelPopupBtn"								: "cancelPopup",
			"click #remindBtn"									: "remind",
			"click #cancelBtn"									: "cancelRemind",
			"click #allStudent"									: "checkAll",
			"click #sendReminderBtn"		    				: "sendReminder",
			"change #selectTaskType"							: "filterTasks",
			"click .addMerit"		    						: "addMerit",
			"click .addedMerit"		    						: "addMerit"
			
				
			},
		
		initialize: function(options) {
			this.render();
			
			var renderClass = function(){
				
				var objectSelectedClass = $("#myClassClassList .classes_tbl_lt .selected");
				var idSelectedClass = objectSelectedClass.attr("id");
				objectSelectedClass.addClass("classList");
				objectSelectedClass.removeClass("selected");
				
				$("#myClassClassList #"+idSelectedClass).trigger("click");
			};
			app.listenTo(app.model.get(ModelName.DIARYITEMS),"reset", renderClass);
			
		},				
		render: function() {
			var diaryItemTypesModel = app.model.get(ModelName.DIARYITEMTYPES);
		//	console.log("diaryItemTypesModel : "+JSON.stringify(diaryItemTypesModel));
			var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
			var diaryItemType = {
				"name" : null,
				"formTemplate" : null
			};
			
			var diaryItemTypes = [];
			$.each(diaryItemTypesModel,function(index,diaryItemTypeModel){
				//console.log("diaryItemTypesModel : "+JSON.stringify(diaryItemTypesModel.at(index)));
				if(diaryItemTypesModel.at(index).get("formTemplate") == "assignment" || 
				   diaryItemTypesModel.at(index).get("formTemplate") == "homework"	){
					if(diaryItemTypesModel.at(index).get("isActive")){
						var diaryItemType = {
								"name" : diaryItemTypesModel.at(index).get("name"),
								"formTemplate" : diaryItemTypesModel.at(index).get("formTemplate"),
								"id" 	: diaryItemTypesModel.at(index).get('id')
							};
						diaryItemTypes.push(diaryItemType);
					}
				}
				
				
            }); 
			
			
			
			var params = {
				"diaryItemTypes" : diaryItemTypes
			};
			
			var c = app.render('myClassesView',params);
			
			
			//getClassForTeacher(currentUserId);
			getAllClasses();
			
			var selectedDay = new Rocketboots.date.Day(),
				timetables = app.model.get(ModelName.CALENDAR).get('timetables'),
				timetable = timetables.getNextSemester(selectedDay);
			
			// If no timetable is current, display the first one
			if (!timetable && timetables.length > 0) {
				timetable = timetables.at(0);
			}
			
			var number_of_Campuses = app.model.get(ModelName.SCHOOLPROFILE).get('number_of_Campuses');
			var classes;
			if(number_of_Campuses && number_of_Campuses > 1){
				getMultiCampusSemesters((new Rocketboots.date.Day()).getKey());
				classes = app.model.get(ModelName.CLASSES); 
			} else {
				if(timetable){
					//alert("tt "+timetable.get('id'));
					classes = app.model.get(ModelName.CLASSES).getCurrentSemClasses(timetable.get('id'));
				}
			}
			
			$(this.el).html(c);	
			var classes = {
				"classes" : classes != null ? classes.toJSON() : {}
			}
			
			var diaryItems={"diaryItems":new Array(10)};
			
			//alert("classes : "+JSON.stringify(classUsers));
			$('.classes', this.el).html(app.render('myClassesSubjectList',classes));
			$('.taskList', this.el).append(app.render('myClassesTaskList',diaryItems));
			
			//$('.studentListClass', this.el).append(app.render('myClassesStudentList',classes));
			
		},
		
		addMerit : function(event){
			
			var date = new Date();
			app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(date.getFullYear(), date.getMonth(), date.getDate()));
			var view = this,
				params = {},
				target = $(event.currentTarget),
				diaryItemId = target.attr('diaryItemId') || null;
				
			var itemId = target.attr('itemId');
			var studentId = target.attr('studentId');
			var classId = target.attr('classId');
			var meritComment = target.attr('comment');
			var passtype_id = target.attr('passtype_id');
			
			if (diaryItemId == null) {
				params = view.getDiaryItemAttributesFromElement(target);
			}

			diaryItemForm.showDialog(diaryItemId,
	                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
	                                          params.startTime,
	                                          params.endTime,
	                                          { 'subject' : params.subject },'',null, params.addTask,false,true,itemId,studentId,classId,meritComment,passtype_id);
	                                          
            $(".editButtonContainer .save").hide();
            $(".editButtonContainer .lockItem").hide();
            $(".editButtonContainer .delete").hide();
            $(".ui-dialog-titlebar").hide();
			$(".ui-dialog-title").html("Merit");
			
			if(passtype_id){
				$(".isNotifyRow").hide();
				
				if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
					$(".editButtonContainer .delete").show();
				}
			
			} else {
				$(".editButtonContainer .save").show();
				if(app.model.get(ModelName.SCHOOLPROFILE).get("allowParents") == 1){
					$(".isNotifyRow").show();
				}
				$(".comments").removeAttr("disabled");
			}
		    event.stopPropagation();
			
		},
		
		getDiaryItemAttributesFromElement: function (element) {
			var view = this,
				params = {},
				target = $(element);

			// try to get the dayKey
			var dayKey = target.attr("dayKey");
			if (dayKey) {
				params.date = Rocketboots.date.Day.parseDayKey(dayKey).getDate();
			} else {
				var currentDate= new Date();
				var date = new Rocketboots.date.Day(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
				params.date = date.getDate();
			}

			// try to get period
			var period = target.attr("period");
			if (period) {
				params.period = parseInt(period, 10);
			}

			// try to get start/end times
			{
			    var startTimeString = target.attr("startTime"),
					endTimeString = target.attr("endTime"),
					startTime = startTimeString ? Rocketboots.date.Time.parseTime(startTimeString) : null,
					endTime = endTimeString ? Rocketboots.date.Time.parseTime(endTimeString) : null,
					isMidnight = function (time) {
						return time.getHour() == 0 && time.getMinute() == 0;
					};

				if (startTime && endTime) {
					// don't use midnight times, instead taking a 1 hour segment closest to the following/previous period
					if (isMidnight(startTime)) {
						startTime = new Rocketboots.date.Time(endTime.getHour() - 1, endTime.getMinute());
					}
					if (isMidnight(endTime)) {
						endTime = new Rocketboots.date.Time(startTime.getHour() + 1, startTime.getMinute());
					}
				}

				if (startTime) params.startTime = startTime;
				if (endTime) params.endTime = endTime;
			}

			// try to get subject
			var subjectId = target.attr("subjectId");
			var subject = subjectId && app.model.get(ModelName.SUBJECTS).get(subjectId);

			if (subject) {
				params.subject = subject;
			}
			
			params.addTask = true;
			
			params.type = "merit";
			
			return params;
		},
		
		showTask : function(event){
		 	$("#selectTaskType").show();
		 	$('#selectTaskType').val('0');
			var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
		 	var classUsers = app.model.get(ModelName.CLASSUSERS);
			$(".classes_tbl_lt .selected").addClass("classList");
			$(".classes_tbl_lt .selected").removeClass("selected");
			$(".classList").find(".arrowClass").attr("src", "assets/myClasses/images/rightarrow.png");
			$(event.currentTarget).removeClass("classList");
			$(event.currentTarget).addClass("selected");
			$(event.currentTarget).find(".arrowClass").attr("src", "assets/myClasses/images/whiterightarrow.png");
			var classId = $(event.currentTarget).find(".classId").val();
			var classTasks = getObjectByValue(classUsers, classId, 'class');
			var diaryItemCollection = getDiaryItemForClass(classId, currentUserId);
			
			assignedTasks = diaryItemCollection;
			
			var diaryItems = new Array(10);
			$("#emptyRows").hide();
			for(var i = 0 ; i < diaryItemCollection.length; i++){
				task = diaryItemCollection.at(i);
				diaryItems[i] = (diary.view.DiaryCalendarViewUtil.mapDiaryItem(task));
			}
			
			console.log("diaryItems321:"+JSON.stringify(diaryItems));
			
			var diaryItemsList ={
					'diaryItems' : diaryItems
			}
			$('.taskList', this.el).html(app.render('myClassesTaskList',diaryItemsList));
			var studentList ={
					'students' : []
			}
			$(".studentListClass").hide();
			
			this.showClassStudents(event);
			//$('.studentListClass', this.el).html(app.render('myClassesStudentList',studentList));
		},
		showStudent : function(event){
			
			var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
			var classUsers = app.model.get(ModelName.CLASSUSERS);
			$(".classes_tbl_rt .selected").addClass("taskListRow");
			$(".classes_tbl_rt .selected").removeClass("selected");
			 $(".taskListRow").find(".arrowTask").attr("src", "assets/myClasses/images/rightarrow.png");
			$(event.currentTarget).removeClass("taskListRow");
			$(event.currentTarget).addClass("selected");
			 $(event.currentTarget).find(".arrowTask").attr("src", "assets/myClasses/images/whiterightarrow.png");
				
			var itemId = $(event.currentTarget).find(".itemId").val();
			var classId = $(event.currentTarget).find(".classId").val();
			var studentCollection = getDiaryItemUser(itemId, classId);
		
			var studentCnt = 0;
			var studentLen = 0;
			
			var studentImage = null;
			var studentImageToDisplay = null;	
			
		
			 studentLen = studentCollection.length;
			 
			 for (studentCnt = 0; studentCnt < studentLen; studentCnt++)
			 {
				 studentImage = studentCollection.at(studentCnt).get('imageUrl');
				 
				 if (studentImage != null && studentImage != '')
				 {
					 studentImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + studentImage;
				 }
				 else
				 {
					 studentImageToDisplay = 'assets/myClasses/images/people.png';
				 }
				 
				 studentCollection.at(studentCnt).set('profileImageToDisplay', studentImageToDisplay);
			 }
		
			var studentList ={
					'students' : studentCollection.toJSON(),
					'showRemindBtn' : true,
					'itemId' : itemId					
			}
			
			$('.studentListClass', this.el).html(app.render('myClassesStudentList',studentList));
			$(".studentListClass").show();
		},
		
		showClassStudents : function(event){
			
			var classUsers = app.model.get(ModelName.CLASSUSERS);
			$(".classes_tbl_rt .selected").addClass("taskListRow");
			$(".classes_tbl_rt .selected").removeClass("selected");
			$(".taskListRow").find(".arrowTask").attr("src", "assets/myClasses/images/rightarrow.png");
			$(event.currentTarget).removeClass("taskListRow");
			$(event.currentTarget).addClass("selected");
			$(event.currentTarget).find(".arrowTask").attr("src", "assets/myClasses/images/whiterightarrow.png");
				
			var classId = $(event.currentTarget).find(".classId").val();
			var studentCollection = getAllClassUsers(classId);
		
			var studentCnt = 0;
			var studentLen = 0;
			
			var studentImage = null;
			var studentImageToDisplay = null;	
			
		
			 studentLen = studentCollection.length;
			 
			 for (studentCnt = 0; studentCnt < studentLen; studentCnt++)
			 {
				 studentImage = studentCollection.at(studentCnt).get('imageUrl');
				 
				 if (studentImage != null && studentImage != '')
				 {
					 studentImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + studentImage;
				 }
				 else
				 {
					 studentImageToDisplay = 'assets/myClasses/images/people.png';
				 }
				 
				 studentCollection.at(studentCnt).set('profileImageToDisplay', studentImageToDisplay);
			 }
		
			var studentList ={
					'students' : studentCollection.toJSON(),
					'showRemindBtn' : false
			}
			
			$('.studentListClass', this.el).html(app.render('myClassesStudentList',studentList));
			$(".studentListClass").show();
		},
		
		showPopup : function(){
			var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
			// get id of student
			var student={};
			student.selectedStudId=[];
			student.selectedStudName=[];
			// check selected students
			$(".studentListClass input:checkbox").each(function(){
			    var $this = $(this);
			    if($this.is(":checked") && $this.attr("id") != "allStudent"){
			    	student.selectedStudId.push($this.attr("id"));
			    	student.selectedStudName.push($this.attr("name"));
			    }
			});
			
			if(student.selectedStudId.length > 0){
				//get details of selected item 
				var assignedDayTask = $(".classes_tbl_rt .selected").find(".assignedDay").val();
				var diaryIemId = $(".classes_tbl_rt .selected").find(".itemId").val();
				var dueDayTask = $(".classes_tbl_rt .selected").find(".dueDay").val();
				var typeTask = $(".classes_tbl_rt .selected").find(".dairyItemTypeName").val();
				var subject = $(".classes_tbl_rt .selected").find(".subject").val();
				var titleTask = $(".classes_tbl_rt .selected").find(".titleTask").html();
				
				reminderParam = {
						"assignedDayTask":assignedDayTask,
						"dueDayTask":dueDayTask,
						"typeTask":typeTask,
						"titleTask":titleTask,
						"subject":subject,
						"userId" : currentUserId,
						"DairyitemId" : diaryIemId,
						"studentIdList" : student.selectedStudId,
						"studentNameList" : student.selectedStudName,
						"appId": "",
						"createdDate" : ""
						
				};
				
				$('#popupDiv', this.el).html(app.render('popupReminder',reminderParam));
				$('#light3').show();
		        $('#myClassesFade').show();		
	            		
			}
			else{
				openMessageBox("Please select atleast one student");
			}
		
		},
		remind : function(){
		
			$(".studentCheckBox").each(function(){
				 $(this).prop("checked", false);
			    $(this).show();
			});
			$("#allStudent").show();
			$('#remindBtn').hide();
			$('#doneBtn').show();
			$('#cancelBtn').show();
			$("#selectAllLabel").show();
			//hide progress and reminder date
			$(".progress").each(function(){
			    $(this).hide();
			});
			$(".reminderDiv").each(function(){
			    $(this).hide();
			});
			
			//To hide add merit Button.
			$(".addMerit").each(function(){
			    $(this).hide();
			});
			
			$(".addedMerit").each(function(){
			    $(this).hide();
			});
			
		},
		cancelRemind : function(){
			$('#doneBtn').hide();
			$('#cancelBtn').hide();
			$('#remindBtn').show();
			$("#selectAllLabel").hide();
			$(".studentCheckBox").each(function(){
			    $(this).hide();
			});
			$("#allStudent").hide();
			$(".progress").each(function(){
			    $(this).show();
			});
			$(".reminderDiv").each(function(){
			    $(this).show();
			});
			
			//To show add merit Button.
			$(".addMerit").each(function(){
			    $(this).show();
			});
			
			$(".addedMerit").each(function(){
			    $(this).show();
			});
		},
		cancelPopup : function(){
			  $('#light3').hide();
	          $('#myClassesFade').hide();
		},
		checkAll : function(event){
			if($(event.currentTarget).is(":checked")){
				$("input:checkbox").each(function(){
				    var $this = $(this);
				    $this.prop("checked", true);
				});
			}
			else{
				$("input:checkbox").each(function(){
				    var $this = $(this);
				    $this.prop("checked", false);
				});
			}
		},
		sendReminder : function(){
			
			var msg = $("#reminderMsg").val();
			if(msg != ""){
				reminderParam.reminderMsg = msg;
				
				// insert message into message table
				var inboxItems = new diary.model.InboxItem();
				var attrs = this.getAttributesForSave();
				console.log("attrs : "+JSON.stringify(attrs))
				var	result = inboxItems.set(attrs);
				
				if(attrs && result){
				
					var arrReceiverId = inboxItems.get("receiverId");
					var arrReceiverName = inboxItems.get("receiverName");
					console.log("arrReceiverId : "+arrReceiverId);
					console.log("arrReceiverName : "+arrReceiverName);
					var receiverIds = "";
					var receiverNames = "";
					
					for(var index = 0; index < arrReceiverId.length ; index++){
						if(index < (arrReceiverId.length - 1)){
							receiverIds = receiverIds + arrReceiverId[index] + ",";
							receiverNames = receiverNames + arrReceiverName[index]+ ",";
						}
						else{
							receiverIds = receiverIds + arrReceiverId[index];
							receiverNames = receiverNames + arrReceiverName[index];
						}
					}
					inboxItems.set("receiverId", receiverIds);
					inboxItems.set("receiverName", receiverNames);
				}
				var msgId = this.insertMessage(inboxItems);	
				reminderParam.appId = msgId;
				
				
				console.log("reminderParam : "+JSON.stringify(reminderParam));
				sendReminder(reminderParam);
				$('#light3').hide();
		         $('#myClassesFade').hide();
		    
			}
			else{
				openMessageBox("Please enter message to send");
			}
			
				
		},
		insertMessage : function(inboxItem){
		var view = this;
			var msgId;
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
	                 // query
		                "INSERT INTO Messages( ParentId, UserId, ReceiverId, Subject, Description, IpAddress, created, isSync, UniqueID, UserName, ReceiverName, ParentEmails) "+
						" VALUES (:ParentId, :UserId, :ReceiverId, :Subject, :Description, :IpAddress, :created, :isSync, :UniqueID, :userName, :receiverName, :parentEmails)",
		                // params
		                {
		                    'ParentId' 	: inboxItem.get('parentId'),
		                    'UserId' 	: inboxItem.get('userId'),
		                    'ReceiverId' : inboxItem.get('receiverId'),
		                    'Subject' 	: inboxItem.get('title'),
		                    'Description' : inboxItem.get('description'),
		                    'IpAddress' : inboxItem.get('IpAddress'),
		                    'created' 	: inboxItem.get('sentDate'),
		                    'isSync'	: 0,
		                    'UniqueID'	: 0,
		                    'userName' 		: inboxItem.get('userName'),
		                    'receiverName'	: inboxItem.get('receiverName'),
		                    'parentEmails' : inboxItem.get("emailId")
		                },
		
						// success
						function(statement) {
		                    msgId = statement.getResult().lastInsertRowID;
		                    inboxItem.set("itemId",msgId);
		                    inboxItem.set("sentDate",convertToLocalDate(inboxItem.get('sentDate')));
							app.model.get(ModelName.INBOXITEMS).add(inboxItem);
							//console.log("app.model.get(ModelName.INBOXITEMS) : "+app.model.get(ModelName.INBOXITEMS));
							app.context.trigger(EventName.FETCHINBOX); 
							view.resetFields();
						},
						function error(e)
						{
							openMessageBox("error inserting message"+e);
						}
				);
				return msgId;
		},
		getAttributesForSave : function() {
			var attrs = {};
			var studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
			
			var now = new Date(); 
			var date = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
			var monthnow = parseInt(new String(((date.getMonth())+1)));
			monthnow = monthnow == 0 ? "00" : (monthnow < 10 ? "0" : "") + monthnow;
			var minuteString = date.getMinutes();
			minuteString = minuteString == 0 ? "00" : (minuteString < 10 ? "0" : "") + minuteString;
			var secondString = date.getSeconds();
			secondString = secondString == 0 ? "00" : (secondString < 10 ? "0" : "") + secondString;
			var hourString = date.getHours();
			hourString = hourString == 0 ? "00" : (hourString < 10 ? "0" : "") + hourString;
			var dayString = date.getDate();
			dayString = dayString == 0 ? "00" : (dayString < 10 ? "0" : "") + dayString;
			
			var createdDate = date.getFullYear() +"-"+monthnow+"-"+ dayString +" "+hourString+":"+minuteString+":"+secondString;
			reminderParam.createdDate = createdDate;
			attrs.title = "Reminder";
			attrs.type = "Reminder";
			attrs.parentId = 0;
			attrs.userId = UserId;
			attrs.userName = studentProfile.get('name')+ ' '+ studentProfile.get('lastName');
		
			attrs.receiverId = reminderParam.studentIdList;
			attrs.receiverName = reminderParam.studentNameList;
			console.log("reminderParam.reminderMsg : "+reminderParam.reminderMsg);
			attrs.description = reminderParam.reminderMsg;
			attrs.IpAddress =  app.model.get(ModelName.SYSTEMCONFIGURATION).get('macAddresses');
			attrs.sentDate = createdDate;
			attrs.emailId = "";
			return attrs;
		},
		resetFields : function(){
			console.log("resetFields:");
			$(".studentListClass input:checkbox").each(function(){
			    var $this = $(this);
			    $this.prop("checked", false);
			});
			this.cancelRemind();
		},
		filterTasks : function(){
			var selectedTask = $(".taskList .classes_tbl_rt .selected").attr("id");
			console.log("selectedTask : "+selectedTask);
			var filteredTask;
			
			var selectedType = $('#selectTaskType :selected');
			var selectedTypeId = selectedType.attr('localtype_id');
			var taskType = selectedType.val();
			if(taskType == '0'){
				filteredTask = assignedTasks.getAssignmentHomework();
				
			}
			else if(taskType == 'assignment'){
				filteredTask = assignedTasks.getAssignment(selectedTypeId);
			}
			else if(taskType == 'homework'){
				filteredTask = assignedTasks.getHomework(selectedTypeId);
			}
			
			var diaryItems = new Array(10);
			for(var i = 0 ; i < filteredTask.length; i++){
				task = filteredTask.at(i);
				diaryItems[i] = (diary.view.DiaryCalendarViewUtil.mapDiaryItem(task));
			}
			
			var diaryItemsList ={
					'diaryItems' :diaryItems
			}
			$('.taskList', this.el).html(app.render('myClassesTaskList',diaryItemsList));
			console.log("check visible : "+($("#"+selectedTask).length));
			if(!($("#"+selectedTask).length)){
				$(".studentListClass").hide();
			}
			else{
				$("#"+selectedTask).trigger("click");
			}
		}
	});
	
	return myClassesView;

})();



