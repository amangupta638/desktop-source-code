diary = diary || {};
diary.view = diary.view || {};
var lastviews="";

var packageId = null;
var title = null;
var arrContent = null;
var imageURL = null;


diary.view.ContentView = (function() {

	var view = Backbone.View.extend({
		
		events: {
			//"click .contentNavigation .contentNavPanel .contentNavTab"		: "togglePanelState"
		},
		
		initialize: function(options) {//alert("initialize")
			//console.log('ContentView.initialize()');

			//this.contentViewClass = options.contentViewClass;
			this.parseAdditionalParams(options);
			this.contentViewRendered = false;
			this.render();
		},
		
		parseAdditionalParams: function(options) {
			if (typeof(options.el) !== "undefined") {
				this.el = options.el;
				this.tabDetails = options.tabDetails;
				this.name=options.name;
			}
		},
		
		render: function() {
		
			var view = this,
			article = this.tabDetails.selectedArticle;
			
			var pageParams = {};
			var ele=this.el;var $el;
			
			//alert('ContentView :: pageParams = ' + JSON.stringify(pageParams));
			
			$(view.el).html(app.render('contentNavigationFrame', pageParams) );	
			
			_.defer(function() {
					// update the previous and next pointers
					view.updatePreviousNextPointers(article);
			});
				
			tabname=view.name;
			matchname=diary.model.ContentItem.baseTitle;
			
			packageId = article.get('packageId');
			
			if (packageId == ContentSetting.ABOUTDAISYPACKAGEID)
			{
				if (article.get('url').indexOf('html') != -1)
				{
					contents = readFile(article.get('url'));
				}
				else
				{
					contents = '<img src="' + article.get('url') + '"/>'
				}
				//imageURL = "/content/" + ContentSetting.ABOUTDAISYPACKAGEID + "/images/";
				
				//contents = readFile(article.get('url'));
				//contents = '<img src="' + article.get('url') + '"/>'
				//contents = contents.replace(/images\//g, imageURL);
				
				$el = $('.contentNavigationBody div.article', ele);
				diary.view.ArticleViewUtil.renderArticle(contents, $el); 
			}
			else if (packageId == ContentSetting.EDUCATIONALPACKAGEID)
			{
				contents = readFile(article.get('url'));
				
				imageURL = "/content/" + ContentSetting.EDUCATIONALPACKAGEID + "/images/";
				contents = contents.replace(/images\//g, imageURL);
				
				$el = $('.contentNavigationBody div.article', ele);
				diary.view.ArticleViewUtil.renderArticle(contents, $el); 
			}
			else if (packageId == ContentSetting.EDUCATIONALJRPACKAGEID)
			{
				contents = readFile(article.get('url'));
				
				imageURL = "/content/" + ContentSetting.EDUCATIONALJRPACKAGEID + "/images/";
				contents = contents.replace(/images\//g, imageURL);
				
				$el = $('.contentNavigationBody div.article', ele);
				diary.view.ArticleViewUtil.renderArticle(contents, $el); 
			}
			else if (packageId == ContentSetting.EDUCATIONALSRPACKAGEID)
			{
				contents = readFile(article.get('url'));
				
				imageURL = "/content/" + ContentSetting.EDUCATIONALSRPACKAGEID + "/images/";
				contents = contents.replace(/images\//g, imageURL);
				
				$el = $('.contentNavigationBody div.article', ele);
				diary.view.ArticleViewUtil.renderArticle(contents, $el); 
			}
			else if (packageId == ContentSetting.EDUCATIONALELEMPACKAGEID)
			{
				contents = readFile(article.get('url'));
				
				imageURL = "/content/" + ContentSetting.EDUCATIONALELEMPACKAGEID + "/images/";
				contents = contents.replace(/images\//g, imageURL);
				
				$el = $('.contentNavigationBody div.article', ele);
				diary.view.ArticleViewUtil.renderArticle(contents, $el); 
			}
			else if (packageId == ContentSetting.SCHOOLINFOPACKAGEID) 
			{
				title = article.get('title');
				
				imageURL = "app-storage:/data/";
				
				var user_type = "allStudents";
				if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
					user_type = "allTeachers";
				}
					
				// Find out holidays in given period
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
				
					// Query to find holidays in given month
					"SELECT Content FROM contentStorage WHERE ContentType = 'SchoolContent' AND Title = :title AND " + user_type + " = 1",	
					
					// Parameters
					{
						title: title
					},
					// Success function
					function success(statement)
		        	{
						arrContent = _.map(statement.getResult().data, function(item) {
		                    return item.Content;
		                }); 
						
						if (arrContent != null)
						{
							contents = arrContent[0];
						}
						
					},
					// error
		        	function error()
		        	{
		            
		        	}
						
					
				);
				
				if (contents != null  && contents != '')
				{	
					contents = contents.replace(/data\//g, imageURL);
				}
				
				$el = $('.contentNavigationBody div.article', ele);
				diary.view.ArticleViewUtil.renderArticle(contents, $el); 
			}
			else 
			{
				
				if ( matchname.indexOf(tabname)>-1) {
					
					var contentViewParams = {};
					//contentViewParams.el = $('.contentNavigationBody', ele);
	
					//Performance fix
					//new diary.view.FrontPageView(contentViewParams).render();//commented as new objects were being created.
					var fpView = new diary.view.FrontPageView(contentViewParams); 
					fpView.render();	
							
				}else {
					
					 checker=false;
					 
					 banners=app.model.get(ModelName.RENDERCONTENTMODEL);
					 
					 if(banners.length>0) {
						
						banners.each(function(abc)	{		
							titlebase=abc.get("Title").replace("amp;","");
		
							if((titlebase)!= "undefined") {
							
								if(titlebase.indexOf(tabname)>-1) {
									var contents=abc.get("Content")
									checker=true;
									
							 		$el = $('.contentNavigationBody div.article', ele);
							
									diary.view.ArticleViewUtil.renderArticle(contents, $el); 	
									
									$el = null;
									ele = null;
									contents = null;
									
								}
							}else{
								
								contents="<h1>Doesn't have content page</h1>";
							
						 		$el = $('.contentNavigationBody div.article', this.el); 	
								
								diary.view.ArticleViewUtil.renderArticle(contents, $el); 
								
							}
						});
					
							if(checker==false) {
								
								contents="<h1>Doesnt have content !!!!</h1>";
							
								$el = $('.contentNavigationBody div.article', ele);
								diary.view.ArticleViewUtil.renderArticle(contents, $el); 		
							}
							
						} else {
							
							contents="<h1>Doesnt have content !!!!</h1>";
							$el = $('.contentNavigationBody div.article', this.el);
							diary.view.ArticleViewUtil.renderArticle(contents, $el); 	
						}
							
					}
					
			
				}
	
			},

			updatePreviousNextPointers: function(article) {
				
				// get the list of articles
				var articles = this.tabDetails.articles.toArray(),
					selectedArticleIndx = 0,
					previousPointer,
					nextPointer;
				
				// determine which article we are going to display
				selectedArticleIndx = this.tabDetails.articles.indexOf(article);
				
				
				previousPointer = $(".contentNavigation .contentNavigationPrevious a");
				nextPointer = $(".contentNavigation .contentNavigationNext a");
				
				// if we did not find the article then it is not part of the table of contents
				if (selectedArticleIndx != -1) {
					// populate the next and previous tabs
					if (selectedArticleIndx == 0) {
						previousPointer.hide();
					} else {
						var previousArticle = articles[selectedArticleIndx - 1];
						
						previousPointer.html("<strong>Prev:</strong> " + previousArticle.get('title'));
						previousPointer.show();
					}
					
					if (selectedArticleIndx == articles.length -1) {
						console.log("Hide next");
						nextPointer.hide();
					} else {
						var nextArticle = articles[selectedArticleIndx + 1];
						
						nextPointer.html("<strong>Next:</strong> " + nextArticle.get('title'));
						nextPointer.show();
					}
				} else {
					// hide all items which are not required
					previousPointer.hide();
					nextPointer.hide();
				}
				
			},
			
			togglePanelState: function(event) {
			
				var contentPanel = $(event.currentTarget).parent();
				if (contentPanel.hasClass("panelMinimised")) {
					contentPanel.removeClass("panelMinimised");
				}
				else {
					contentPanel.addClass("panelMinimised");
				}
			
			}
		
	});
	
	return view;

})();
