diary = diary || {};
diary.view = diary.view || {};
var onceClicked = false;
diary.view.SubjectView = (function() {
	var debug = true;
	
	var view = Rocketboots.view.View.extend({

		events: {
			"click .timetable .cycleday .period"					:	"clickWithinPeriod",

//			"click .timetable .cycleday .period input"				: 	"clickWithinPeriodInput",
//			"blur .timetable .cycleday .period input"				:	"periodNameBlur",
//			"keyup .timetable .cycleday .period input"				:	"periodNameChange",
//			"keydown .timetable .cycleday .period input"			:	"periodNameKeyDown",

//			"click .subjectPanel .subjects .subject .subjectcolour"	:	"chooseSubjectColour",
//			"focus .subjectPanel .subjects .subject input"			:	"subjectPanelNameFocus",
			"click .subjectPanel .subjects .subject input"			:	"subjectPanelNameClicked",
			"keydown .subjectPanel .subjects .subject input"		:	"subjectPanelNameKeyDown",
			"blur .subjectPanel .subjects .subject input"			:	"subjectPanelNameBlur",
			"keyup .subjectPanel .subjects .subject input"			:	"subjectPanelNameChange",
			"click .sidebar > .tasks"								: 	"panelTabClicked"
		},
		
		initialize: function(options) {
			onceClicked = true;
			var view = this, 
				refreshSubjectPanel = function() {
					debug && console.log("Refreshing subjects panel only");
					view.renderSubjectPanel();
				},
				setTimetableToday = function() {
					view.setTimetableForToday();
				};
			
			//debug && console.log('SubjectView.initialize()');
			
			// get the required models
			view._subjects = app.model.get(ModelName.SUBJECTS);

			//app.model.get(ModelName.SUBJECTS).on("add", refreshSubjectPanel, this);
			//app.model.get(ModelName.SUBJECTS).on("change", refreshSubjectPanel, this);
			//app.model.get(ModelName.SUBJECTS).on("remove", refreshSubjectPanel, this);
			
			app.listenTo(app.model.get(ModelName.SUBJECTS), "add", refreshSubjectPanel);
			app.listenTo(app.model.get(ModelName.SUBJECTS), "change", refreshSubjectPanel);
			app.listenTo(app.model.get(ModelName.SUBJECTS), "remove", refreshSubjectPanel);

			// If the list of timetables change, ensure the displayed timetable is still the correct one.
			//app.model.get(ModelName.CALENDAR).get('timetables').on("reset", view.setTimetableForToday, this);
			app.listenTo(app.model.get(ModelName.CALENDAR).get('timetables'), "reset", setTimetableToday);
		
			app.context.on(EventName.CLASSCREATED, this.refreshClass, this);
			app.context.on(EventName.CLASSDELETED, this.refreshClass, this);

			app.context.on(EventName.UPDATESUBJECTFAILED, view.revertSubjectName, this);
			
			this.selectedSubjectIndex = -1;
			this.nextFocus = null;
			this.nextSubjectFocusId = null;

			view.render();
			view.setTimetableForToday();
		},

		/**
		 * This will display the timetable as appropriate for the current date.
		 */
		setTimetableForToday: function () {
			renderTimetable = true;
			var view = this,
				selectedDay = new Rocketboots.date.Day(),
				timetables = app.model.get(ModelName.CALENDAR).get('timetables'),
				timetable = timetables.getCurrentAt(selectedDay);

			// If no timetable is current, display the first one
			if (!timetable && timetables.length > 0) {
				timetable = timetables.at(0);
			}

			//debug && console.log("SubjectView.setTimetableForToday - " + selectedDay + ", " + timetable);
			view.setTimetable(timetable);
			timetables = null;
		},

		setTimetable: function (timetable) {
			var view = this;

			if (view._timetable == timetable) return;

			// Remove event listeners
			if (view._timetable) {
				view._timetable.get('classes').off("change", view.refreshClass, view);
			}

			// Add event listeners
			if (timetable) {
				// we will listen to the change event of the classes to render the details of the class again
				timetable.get('classes').on("change", view.refreshClass, view);
			}

			// Store new timetable
			view._timetable = timetable;
			
			// Re-render
			//console.log("re render view : ");
			
			if(!renderTimetable || onceClicked){
				view.renderTimetable();
				view.renderSubjectPanel(); // the list of subjects may have changed with the new timetable
			}
			onceClicked = false;
			
			view.renderTimetableList(); // the timetable list should display the selected timetable

			// the class edit form should be updated with the new timetable
			view.classForm.parseAdditionalParams({
				timetable : view._timetable
			});
		},

		refreshClass: function (classObj) {
			var view = this;
			console.log("Refreshing individual class only");
			view.renderClass(classObj);
			view.renderSubjectPanel(); // TODO: figure out how to not do this every time
			
			//if (app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")) {
				//getClassForTeacher(app.model.get(ModelName.STUDENTPROFILE).get("UniqueID"));
			//} else {
				getAllClasses();
			//}
		},
		
		panelTabClicked : function (event) {
			this.switchToPanel($(event.currentTarget));
		},
		
		switchToPanel: function(panel) {
            var result;
            result = diary.view.DiaryCalendarViewUtil.togglePanel(panel);

            app.model.get(ModelName.APPLICATIONSTATE).set({ 'dayViewPanelVisible' : result.dayViewPanelVisible });
            app.model.get(ModelName.APPLICATIONSTATE).set({ 'dayViewActivePanel' : result.dayViewActivePanel });

            this.renderTimetableList();
            this.renderSubjectPanel();
        },
		
		parseAdditionalParams: function(options) {
			if (typeof(options.el) !== "undefined") {
				this.el = options.el;
			}
		},
		
		render: function() {
			var view = this;
			
			//debug && console.log('SubjectView.render()');

			var colourPalette = [];
			// map colour palette to array 
			for (var colourKey in diary.model.Subject.Colours) {			
				colourPalette.push({ 'id' : colourKey, 'colour' : diary.model.Subject.Colours[colourKey] });
			}

			var c = app.render('subjectView', {
				'colourPalette' : colourPalette
			});
			$(this.el).html(c);	
			
			
			$("#subjectColourPicker", this.el).on("click", ".subjectcolour", function(event) {
				var colourIndex = $(event.currentTarget).attr("colourIndex");
				app.popup.hide($("#subjectColourPicker"));
				view.changeSubjectColour(view.selectedSubjectIndex, colourIndex);
			});
			
			view.renderClassEditForm();
			//view.renderSubjectPanel();						
			view.renderTimetable();
			//view.renderTimetableList();
            
            var viewParams = {
				'sidebarMinimised'	 	: true
			};
            $('.subjectView', this.el).after(app.render('sidebar', viewParams));
            // Add an event handler to the list elements of the timetable picker
			$(".subjectTimetableList", this.el).on("click", ".item", function (event) { 
				renderTimetable = false;
				view.timetableSelected(event); 
			});
			
		},
		
		renderSubjectPanel: function() {
			var view = this;
			
			view._subjects = app.model.get(ModelName.SUBJECTS);		
			view.availableSubjects = _.map(view._subjects.toArray(), function(subject) { 
				return subject.get('title'); 
			});			

            var subjects = [];
            _.forEach(view._subjects.toArray(), function(subject) {
                if (view._timetable && view._timetable.hasClassesOfSubject(subject)) {
					if(isHexCode(subject.attributes.colour))
					                  {
					                   subject.attributes.colour = getHexCode((subject.attributes.colour).toString().toUpperCase());
					                  }
                    subjects.push(subject.attributes);
                }
            });

			var c = app.render('subjectViewSubjectPanel', {
                'subjects' : subjects,
                'hasSubjects' : subjects.length > 0
            });
            //console.log("subjectPanel");
           // console.log(c);
			$(".subjectPanel", this.el).html(c);
			
			_.defer(function() {
				if (view.nextSubjectFocusId != null) {
					$(".subjectPanel .subject[subjectId=" + view.nextSubjectFocusId + "] > input[name=subject]", this.el).focus();
				}
				view.nextSubjectFocusId = null;
			});
				
			c = null;
		},
		
		renderTimetable: function() {
			var view = this;
			if(resetTimetable && !onceClicked){
				view.setTimetableForToday();
				resetTimetable = false;
			}
			if(view.timetableId){
				view._timetable = app.model.get(ModelName.CALENDAR).get('timetables').get(view.timetableId);
				view.timetableId = null;
			} 
			var number_of_Campuses = app.model.get(ModelName.SCHOOLPROFILE).get('number_of_Campuses');
            var viewParams = (number_of_Campuses && number_of_Campuses > 1) ? diary.view.SubjectViewUtil.mapMultiCampusTimetable(view._timetable) : diary.view.SubjectViewUtil.mapTimetable(view._timetable);
			var content = app.render('subjectViewTimetable', viewParams);
			
			$(".timetable", view.el).html(content);
			
			if(view._timetable){
				var description = "From " + diary.view.SubjectViewUtil.formatDate(view._timetable.get('startDay'));
				var semName = view._timetable.get('name');
				if(number_of_Campuses && number_of_Campuses > 1){
					description = getMultiCampusSemesterDate(view._timetable);
					semName = getMultiCampusSemesterName(view._timetable);
				}
				$(".semesterTitle",this.el).html(semName);
				$(".semesterFrom",this.el).html(description);
			}
			
			content = null;
			viewParams = null;
		},

		renderTimetableList: function () {
			var view = this;
			var timetables = app.model.get(ModelName.CALENDAR).get('timetables');
			var number_of_Campuses = app.model.get(ModelName.SCHOOLPROFILE).get('number_of_Campuses');
			var content = app.render('subjectViewTimetableList', {
				'timetables' : (number_of_Campuses && number_of_Campuses > 1) ? diary.view.SubjectViewUtil.mapMultiCampusTimetables(timetables, view._timetable) : diary.view.SubjectViewUtil.mapTimetables(timetables, view._timetable),
				'year' : app.model.get(ModelName.CALENDAR).get('year'),
				'hasTimetables' : timetables.length > 0
			});
			//console.log("timetable");
			//console.log(diary.view.SubjectViewUtil.mapTimetables(timetables, view._timetable));
			
			$(".subjectTimetableList", view.el).html(content);

			content = null;
			timetables = null;
			//debug && console.log("SubjectView.renderTimetableList", diary.view.SubjectViewUtil.mapTimetables(timetables, view._timetable));
		},

        renderClassEditForm: function() {
       // alert("renderClassEditForm");
            var view = this,
                viewParams = {
                    timetable : view._timetable
                };

            viewParams.el = $('#classFormEditPopup', this.el);
			viewParams.classes = 'classFormEditPopup'
            view.classForm = new diary.view.ClassView(viewParams);
        },
		
		renderClass: function(classObj) {
			
			//console.log("renderClass");
			//console.log(classObj);
			getClassesForMappingSubjects();
			var view = this,
                viewParams,
                period,
				periodCell = null,
                classId,
                classOnAllday = null;
		
			if (classObj instanceof diary.model.Class) {
//				viewParams.colour = classObj.get('subject').get('colour');
//				viewParams.subject = classObj.get('subject').get('title');
//				viewParams.room = classObj.get('room');
//				viewParams['empty?'] = false;
				if(classObj.get('cycleDay') == '0'){
					classOnAllday = classObj.get('periodId');
				}
				
				periodCell = $("#D" + classObj.get('cycleDay') + "P" + classObj.get('periodId'), view.el);
				periodCell.attr("classId", classObj.get('id'));
            } else if (typeof classObj === "number") {
				periodCell = $("div.period[classId=" + classObj + "]", view.el);
                periodCell.removeAttr("classId");
			}

			if (periodCell.length > 0) {
                var periodId = periodCell.attr('data-period');
                period = (view._timetable ? view._timetable.getPeriod(periodId) : null);
				//console.log('Getting period cell - periodId=' + periodId + ' ' + period);
				
				if (typeof(periodId) == "undefined") {
					//console.log(periodCell.html());
				}
				
				viewParams = diary.view.SubjectViewUtil.mapPeriod(period,
					diary.view.SubjectViewUtil.getPeriodAfter(view._timetable.periodsForCycleDay(period), period),
					view._timetable, "", periodCell.attr('data-cycleday'));

//                if (period) {
//                    viewParams.label = diary.view.DiaryCalendarViewUtil.formatTime(period.get('startTime'));
//                    viewParams.name = period.get('title');
//                   // console.log(period.get('title'));
//                }
//
                periodCell.empty();
               // alert("viewParams");
                //console.log("subjectViewTimetablePeriod");
               // console.log(viewParams);
				periodCell.html(app.render("subjectViewTimetablePeriod", viewParams));

				_.defer(function() {
					if (typeof(view.nextFocus) == 'string' && view.nextFocus == "room") {
						//debug && console.log("now focusing on next room");
						$("input[name=room]", periodCell).focus();
					} else if (view.nextFocus && view.nextFocus.focus) {
						//debug && console.log("now focusing on next period subject");
						view.nextFocus.focus();
					}					
					view.nextFocus = null;
					debug && console.log("setting updating on ", periodCell.attr("classId"), false);
					periodCell.data("changing", false);
				});
			} else if( classOnAllday != null){
				view.render();
			}
		},
		
		/**
		 * Handle clicking within a period
		 */
		clickWithinPeriod: function(event) {
			var isBreak = $(event.currentTarget).attr('isBreak');
			var classId = $(event.currentTarget).attr('classid');
			var number_of_Campuses = app.model.get(ModelName.SCHOOLPROFILE).get('number_of_Campuses');
//			if(isBreak == 'true'){
//				openMessageBox('You cannot add or edit classes in recess');
//				return;
//			} else if(number_of_Campuses > 1 && classId == ""){
//				//openMessageBox('You cannot add or edit classes in recess');
//				return;
//			} 
			//Blocking self created classes now
			if(classId == ""){
				//openMessageBox('You cannot add or edit classes in recess');
				return;
			} else if(classId != "" && isBreak == 'true'){
				openMessageBox('You cannot add or edit classes in recess');
				return;
			} else {
				var view = this;
				view.nextFocus = null;
	
	            view.showClassForm(event);
	            $(".ui-dialog-titlebar").hide();
            }
            
//			var input = $("input[name=subject]:first", $(event.currentTarget));
//            if (input.length) $.createSelection(input, 0, input.val().length);
		},

        /*
         * Class View/Edit Form Dialog
         */

        showClassForm: function(event) {
            var view = this,
                params = {},
                target = $(event.currentTarget),
                classId = target.attr('classId') || null,
                classObj = (classId == null || view._timetable == null ? null : view._timetable.getClass(classId));

            if (classId == null || classObj == null || typeof(classObj) == "undefined") {
                params.periodId = parseInt(target.attr('data-period'), 10);
                params.cycleDay = parseInt(target.attr('data-cycleday'), 10);
            }
            //console.log("showClassForm classId="+classId);
			if(classId)
			{
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT * FROM ClassUser WHERE classId = :classId",

            // params
            {
			'classId' :classId
            },
			// success
			function(statement) {
			
				var detail=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    
                 if(detail.length)
                 {
                 		var userid=detail[0].userId ;
                 		var createdBy=detail[0].createdBy;
                 		$(".classHeader").html("Edit Class");
                 		
                 		if(userid == createdBy)
                 		{
                 			//console.log("createdBy Match");
                 		 view.classForm.showDialog(classId, params);
                 		 $(".addClassTable :input").attr("readonly", false);
                 		 $(".lockContainer .delete").show();
                 		 $(".lockContainer .lockItem").hide();
                 		 $(".editButtonContainer .save").show();
                 		}
                 		else
                 		{
                 			//console.log("createdBy Match FALSE");
                 			view.classForm.showDialog(classId, params,true);
                 			//TODO:add lock icon
                 			$(".addClassTable :input").attr("readonly", true);
                 			$(".lockContainer .delete").hide();
                 			$(".lockContainer .lockItem").show();
                 			$(".editButtonContainer .save").show();
                 			
                 		}
                 }
			
			},
			
			// error
			function error()
			{
				console.log("Error while trying to create ClassUser item");
				
			}
	);
	}
	else
	{
		//console.log("inside else");
		this.classForm.showDialog(classId, params);
		$(".addClassTable :input").attr("readonly", false);
		$(".classHeader").html("Add Class");
        $(".editButtonContainer .save").show();
        $(".lockContainer .delete").hide();
        $(".lockContainer .lockItem").hide();
	}
			
			
			/*==========================*/
           // this.classForm.showDialog(classId, params);
            event.stopPropagation();
        },
		
//		clickWithinPeriodInput: function(event) {
//			var view = this;
//			view.nextFocus = null;
//
//			var input = $(event.target);
//			$.createSelection(input, 0, input.val().length, true);
//
//			// stop this from bubbling up and calling clickWithinPeriod too
//			event.stopPropagation();
//		},
//
		inputValueChanged: function(newValue, oldValue) {
			return newValue.trim().toLowerCase() != oldValue.toLowerCase();
		},
//
//
//		/**
//		 * Subject label lost focus.  Subject label must be persisted if it has changed
//		 */
//		periodNameBlur: function(event) {
//			var view = this,
//				input = $(event.currentTarget);
//
//			input.val(input.val().trim());
//
//			if (view.inputValueChanged(input.val(), input.attr("originalValue"))) {
//				var fieldType = input.attr('name'),
//					periodCell = input.closest(".period"),
//					cycleday = parseInt(periodCell.attr('data-cycleday'), 10),
//					period = parseInt(periodCell.attr('data-period'), 10);
//
//				if (periodCell.data("changing")) {
//					//debug && console.log("ignoring this blur event as periodCell", cycleday, period, " is already updating");
//					return;
//				}
//				if (cycleday && period) {
//					//debug && console.log("setting updating on ", periodCell.attr("classId"), true);
//
//					periodCell.data("changing", true);
//					if (fieldType == 'subject') {
//						view.changePeriod(cycleday, period, input, input.next("[name=room]"));
//					} else {
//						view.changePeriod(cycleday, period, input.prev("[name=subject]"), input);
//					}
//				}
//			} else {
//				input.val(input.attr("originalValue"));
//			}
//		},
//
//		/**
//		 * Subject label keypress/change.  Handle Escape and Return functionality
//		 */
//		periodNameChange: function(event) {
//			var view = this,
//				input = $(event.currentTarget);
//
//			if (event.keyCode == 27) {
//				input.val( input.attr("originalValue") );
//				input.blur();
//			}
//			else if (event.keyCode == 13) {
//				view.nextInput(input).focus();
//			}
//		},
//
//		periodNameKeyDown: function(event) {
//			var view = this,
//				input = $(event.currentTarget);
//
//			if (event.keyCode == 9) { // tab
//				var fieldType = input.attr("name");
//				if (fieldType == 'subject') {
//					view.nextFocus = "room";
//					if (view.inputValueChanged(input.val(), input.attr("originalValue"))) {
//						input.blur();
//						// this is going to call changePeriod
//						//debug && console.log("preventing default tabbing");
//						event.preventDefault && event.preventDefault();
//						return false;
//					} else {
//						input.val(input.attr("originalValue"));
//					}
//				} else if (fieldType == 'room') {
//					view.nextFocus = view.nextInput(input);
//				}
//			}
//
//		},
//
//		changePeriod: function(cycleDay, period, $subjectTitle, $room) {
//			var view = this,
//				subjectTitle = $subjectTitle.val().trim(),
//				room = $room.length > 0 ? $room.val().trim() : "";
////			//debug && console.log('change class for day ' + cycleDay + ' period ' + period, subjectTitle, room);
//
//			var classObj = view._timetable.classAt(cycleDay, view._timetable.get('periods').get(period));
//			if (classObj) {
//				if (subjectTitle != "") {
//					app.context.trigger(EventName.UPDATECLASS, view._timetable, classObj.get('id'), subjectTitle, room);
//				} else {
//					view.nextFocus = view.nextInput($room);
//					app.context.trigger(EventName.DELETECLASS, view._timetable, classObj.get('id'));
//				}
//			} else {
//				if (subjectTitle != "") {
//					app.context.trigger(EventName.CREATECLASS, view._timetable, period, cycleDay, subjectTitle, room);
//				} else {
//					// ignore and reset
//					$room && $room.val("");
//				}
//			}
//		},
//
		chooseSubjectColour: function(event) {
	
			var view = this;
			view.selectedSubjectIndex = parseInt($(event.currentTarget).parents(".subject:first").attr("subjectId"), 10);
						
			app.popup.show( $("#subjectColourPicker"), $(event.currentTarget), "bottom", null, event );

		},
		
		changeSubjectColour: function(subjectIndex, colourIndex) {
			var view = this;
			//debug && console.log('Change subject index ' + subjectIndex + ' to colour index ' + colourIndex);
			var subject = new diary.model.Subject(view._subjects.get(subjectIndex).attributes); 
			subject.set("colour", diary.model.Subject.Colours[colourIndex]);
			app.context.trigger(EventName.UPDATESUBJECT, subject);
		},
		
		subjectPanelNameClicked: function(event) {
			var view = this;
			view.nextFocus = null;

			var input = $(event.target);
			$.createSelection(input, 0, input.val().length, true);

			// stop this from bubbling up and calling clickWithinPeriod too 
			event.stopPropagation();
		},
		
		subjectPanelNameKeyDown: function(event) {
			var view = this,
				input = $(event.currentTarget);

			if (event.keyCode == 9) { // tab
				view.nextSubjectFocusId = view.nextSubjectPanelSubjectId(input);
				if (view.inputValueChanged(input.val(), input.attr("originalValue"))) {
					// this is going to call changePeriod
					//debug && console.log("preventing default tabbing");
					event.preventDefault && event.preventDefault();						
					input.blur();
					return false;
				} else {
					input.val(input.attr("originalValue"));
				}
			}

		},
	
		/**
		 * Subject label lost focus.  Subject label must be persisted if it has changed
		 */	
		subjectPanelNameBlur: function(event) {			
			var view = this,
				input = $(event.currentTarget);
			
			input.val(input.val().trim());

			if (view.inputValueChanged(input.val(), input.attr("originalValue"))) {
				var selectedSubjectIndex = parseInt($(event.currentTarget).parents(".subject:first").attr("subjectId"), 10);
				view.changeSubjectName(selectedSubjectIndex, input.val(), input);
			} else {
				input.val(input.attr("originalValue"));
			}
		},
		
		/**
		 * Subject label keypress/change.  Handle Escape and Return functionality
		 */
		subjectPanelNameChange: function(event) {
			var input = $(event.currentTarget);
				
			if (event.keyCode == 27) {
				input.val( input.attr("originalValue") );
				input.blur();
			}
			else if (event.keyCode == 13) {
				var subjectInputs = $(".subjectPanel .subjects .subject input", this.el);
				var selectedSubjectIndex = subjectInputs.index(input);
				selectedSubjectIndex++;
				if (selectedSubjectIndex >= subjectInputs.length) selectedSubjectIndex = 0;
				subjectInputs[selectedSubjectIndex].focus();
			}
		},
		
		/**
		 * Change subject name
		 */
		changeSubjectName: function(subjectIndex, subjectName, $input) {
			var view = this;
			//debug && console.log('change subject name index ' + subjectIndex + ' = ' + subjectName);
			
			var subject = view._subjects.get(subjectIndex).clone();
			if (subject.set("title", subjectName)) {
				view.nextSubjectFocusId = view.nextSubjectPanelSubjectId($input);
				//debug && console.log("next subject focus id = ", view.nextSubjectFocusId );
				app.context.trigger(EventName.UPDATESUBJECT, subject);			
			} else {
				//debug && console.log("reverting to original value", $input.attr('originalValue'));
				$input.val($input.attr('originalValue')).focus();
			}

		}, 
		
		revertSubjectName: function(subjectId) {
			var view = this; 
			
			var $subjectInput = $("div[subjectId=" + subjectId + "] input[name=subject]", view.el);
            $subjectInput.val($subjectInput.attr("originalValue"));

			_.defer(function() {
				$.createSelection($subjectInput, 0, $subjectInput.val().length, true);
			});
		}, 
		
		
//		nextInput: function(input) {
//			var subjectInputs = $(".timetable .cycleday .period input:visible", this.el);
//			var selectedSubjectIndex = subjectInputs.index(input);
//			selectedSubjectIndex++;
//			if (selectedSubjectIndex >= subjectInputs.length) selectedSubjectIndex = 0;
//
//			return subjectInputs[selectedSubjectIndex];
//
//		},
		
		nextSubjectPanelSubjectId: function(input) {
			var subjectInputs = $(".subjects input[name=subject]:visible", this.el);			
			var selectedSubjectIndex = subjectInputs.index(input);
			selectedSubjectIndex++;
			if (selectedSubjectIndex >= subjectInputs.length) selectedSubjectIndex = 0;
			var nextSubjectId = $(subjectInputs[selectedSubjectIndex]).parents("div.subject:first").attr("subjectId");
			return nextSubjectId;
		},

		// Update the current timetable, after a timetable was selected from the timetable selector
		timetableSelected: function (event) {
			//console.log("timetableSelected");
			var view = this,
				timetableId = $(event.currentTarget).attr('timetableId'),
				timetable = app.model.get(ModelName.CALENDAR).get('timetables').get(timetableId);
			view.timetableId = timetableId;
			//console.log('timetableSelected ');
			if (timetableId && timetable) {
				view.setTimetable(timetable);
			}

			//debug && console.log("SubjectView.timetableSelected", timetableId, $(event.currentTarget).attr("class"));
		}

	});
	
	return view;

})();