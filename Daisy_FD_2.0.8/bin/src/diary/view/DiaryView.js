diary = diary || {};
diary.view = diary.view || {};
var diaryItemForm="",oncecall=true;
diary.view.DiaryView = (function() {
	
	var view = Rocketboots.view.View.extend({
		
		events: {
			"click .cal_tbw_right .cal_filter_task ul li.col .tasks"	: "filterClicked",
			"click .cal_tbw_right .cal_filter_task ul li.col .event"	: "filterClicked",
			"click .cal_tbw_right .cal_filter_task ul li.col .note"		: "filterClicked"
		},
		
		initialize: function(options) {
			//console.log('DiaryView.initialize()');
			
			Rocketboots.view.View.prototype.initialize.call(this);
			
			var diaryItemsChanged = function() {
				console.log("diaryItemsChanged");
				var name = $(".headerNavigation").find(".selected").attr('data-link'); 
				if(name == 'home'){
					$(".headerNavigation").find(".selected").click()
				}
				// let all the views that relate on the diary item collection know that the collection has changed
				// so that they can perform all the work to update the necessary details
				app.context.trigger(EventName.REFRESHDIARYITEMS);
				// render the diary again
				//this.renderDiary();
			};
			
			var diaryItemsReset = function() {
				console.log("diaryItemsReset");
				app.context.trigger(EventName.REFRESHCALENDAR);
				
				var diaryItemTypesModel = app.model.get(ModelName.DIARYITEMTYPES);
				var pageParams = {
					'isTaskActive' : (diaryItemTypesModel.isDiaryItemTypeActive('Assignment') || diaryItemTypesModel.isDiaryItemTypeActive('Homework')),
					'isNoteActive' : diaryItemTypesModel.isDiaryItemTypeActive('Note'),
					'isEventActive' : diaryItemTypesModel.isDiaryItemTypeActive('Event')
				};
	
				$('.cal_tbw_right').html(app.render('diaryFiltersView', pageParams));
			
			/*	var view = this.childViews[this.viewName];
				if (typeof(view) !== "undefined") {
					view.isDirty = true;
				}
	
				this.renderDiary();*/
			};
			
			// we need to eventual get mapped to the school profile change event so that we can update our property once it changes
			//app.model.on('change:' + ModelName.SCHOOLPROFILE, this.schoolProfileChanged, this);
			app.listenTo(app.model, 'change:' + ModelName.SCHOOLPROFILE, this.schoolProfileChanged);
			//app.model.get(ModelName.APPLICATIONSTATE).on('change:selectedDay', this.renderBanner, this);
			app.listenTo(app.model.get(ModelName.APPLICATIONSTATE),'change:selectedDay', this.renderBanner);
			
			app.context.on(EventName.REFRESHCALENDAR, this.renderDiary, this);
			
			// the events that are fired if the Diary Item Collection is changed
			// NOTE - I think this is the best option to make it more clear to what events
			// the view updates itself
			//app.model.get(ModelName.DIARYITEMS).on("add", diaryItemsChanged, this);
			app.model.get(ModelName.DIARYITEMS).on("change", diaryItemsChanged, this);
			//app.model.get(ModelName.DIARYITEMS).on("remove", diaryItemsChanged, this);
			//app.model.get(ModelName.DIARYITEMS).on("reset", diaryItemsReset, this);
			//app.model.get(ModelName.CALENDAR).get('timetables').on("reset", diaryItemsReset, this);
			
			//app.listenTo(app.model.get(ModelName.DIARYITEMS),"add", diaryItemsChanged);
			//app.listenTo(app.model.get(ModelName.DIARYITEMS), "change", diaryItemsChanged);
			//app.listenTo(app.model.get(ModelName.DIARYITEMS), "remove", diaryItemsChanged);
			//app.listenTo(app.model.get(ModelName.DIARYITEMS),"reset", diaryItemsReset);
			//app.listenTo(app.model.get(ModelName.CALENDAR).get('timetables'), "reset", diaryItemsReset);
			
			
			
			// school profile may not have changed, so we will force the banner to re-render 
//			app.context.on(EventName.CONTENTITEMSRELOADED, function() {
//				this.renderBanner(true);
//			}, this);
					
			this.currentArticle = null;
			// get the school profile from the model
			this.schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);
			
			this.parseAdditionalParams(options);
			var diaryItemTypesModel = app.model.get(ModelName.DIARYITEMTYPES);
			
			var pageParams = {
				'empowerContentEnabled' : this.schoolProfile.get("includeEmpower"),
				'isTaskActive' : (diaryItemTypesModel.isDiaryItemTypeActive('Assignment') || diaryItemTypesModel.isDiaryItemTypeActive('Homework')),
				'isNoteActive' : diaryItemTypesModel.isDiaryItemTypeActive('Note'),
				'isEventActive' : diaryItemTypesModel.isDiaryItemTypeActive('Event')
			};

			$(this.el).html(
				app.render('diaryView', pageParams)
			);

			
			this.render();
			
		},
		
		schoolProfileChanged: function() {
			// get the school profile from the model
			this.schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);
			this.renderBanner();
		},
		
		parseAdditionalParams: function(options) {
			//console.log('DiaryView.parseAdditionalParams viewName=' + options.viewName);
			this.diaryViewClass = options.diaryViewClass;
			this.viewName = options.viewName;
			
		},
		
		filterClicked: function() {
			this.renderDiary();
		},
		
		render: function() {
			//console.log('DiaryView.render()');
					
			this.renderBanner();

			// render the requested diary view
			this.renderDiary();
			
			
		},
		
		renderBanner: function(forceRefresh) {
			this.schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);
			
			var empowerContentEnabled = this.schoolProfile.get("includeEmpower");
			forceRefresh = forceRefresh || false;
			
			var articleContent;
			//console.log("DiaryView.renderBanner()", empowerContentEnabled);
			
			// render the empower content if the school is
			// subscribed to it
			if (empowerContentEnabled) {
				
				// get the article that we are going to render
				// NOTE - It is being assumed that the date from and to of the article are going to be
				// specified for the article banners

				//console.log("selectedDay", app.model.get(ModelName.APPLICATIONSTATE).get("selectedDay"));


			var dates =app.model.get(ModelName.APPLICATIONSTATE).get("selectedDay");

			var banners=app.model.get(ModelName.RENDERCONTENTMODEL);
			
			if(banners && banners!= "undefined" && banners.length>0 )
			{
				_.forEach(banners, function(banner) {
					if(banner.get("ContentType")=="Panel")
					{
						var startdate=Rocketboots.date.Day.parseDateString(banner.get("StartDate"))
						var enddate=Rocketboots.date.Day.parseDateString(banner.get("EndDate"))
						//alert(startdate)
						if(dates.getTime() >= startdate.getTime() && dates.getTime() <= enddate.getTime())
						{//alert(1111)
							articleContent=banner.get("Content")
						}
					}
				});
			}
			
			/*banners.each(function(abc)	{
				//alert(abc.get("ContentType"))
				if(abc.get("ContentType")=="Panel")
				{
					var startdate=Rocketboots.date.Day.parseDateString(abc.get("StartDate"))
					var enddate=Rocketboots.date.Day.parseDateString(abc.get("EndDate"))
					//alert(startdate)
					if(dates.getTime() >= startdate.getTime() && dates.getTime() <= enddate.getTime())
					{//alert(1111)
						articleContent=abc.get("Content")
					}
				}
	
	
			});
			*/

			diary.view.ArticleViewUtil.renderArticle(articleContent, $('.empowerContentBody', this.el));
 
 
 
 
/*				var article = this.getEmpowerBannerArticle(app.model.get(ModelName.APPLICATIONSTATE).get("selectedDay"));
				
				alert(article)
				//console.log("article=");
				//console.log(article);
				//console.log("this.currentArticle");
				//console.log(this.currentArticle);
				if (this.currentArticle != article || forceRefresh) {
					this.currentArticle = article;
					//console.log("here");
					//console.log(this.currentArticle);
					// perform an ajax call with the url of the article so that
					// we can display the article contents on the page
					if(this.currentArticle) {
						//alert("renderBanner")
					    diary.view.ArticleViewUtil.renderArticle(article, $('.empowerContentBody', this.el));
					} else {
						$('.empowerContentBody', this.el).html("");
					}
				}*/
				
			}
			
		
			
			
			
			/*alert("renderBanner")
			var empowerContentEnabled = this.schoolProfile.get("includeEmpower");
			forceRefresh = forceRefresh || false;
			
			//console.log("DiaryView.renderBanner()", empowerContentEnabled);
			
			// render the empower content if the school is
			// subscribed to it
			if (empowerContentEnabled) {
				
				// get the article that we are going to render
				// NOTE - It is being assumed that the date from and to of the article are going to be
				// specified for the article banners

				//console.log("selectedDay", app.model.get(ModelName.APPLICATIONSTATE).get("selectedDay"));

				var article = this.getEmpowerBannerArticle(app.model.get(ModelName.APPLICATIONSTATE).get("selectedDay"));
				//console.log("article=");
				//console.log(article);
				//console.log("this.currentArticle");
				//console.log(this.currentArticle);
				if (this.currentArticle != article || forceRefresh) {
					this.currentArticle = article;
					//console.log("here");
					//console.log(this.currentArticle);
					// perform an ajax call with the url of the article so that
					// we can display the article contents on the page
					if(this.currentArticle) {
					    diary.view.ArticleViewUtil.renderArticle(article, $('.empowerContentBody', this.el));
					} else {
						$('.empowerContentBody', this.el).html("");
					}
				}
				
			}
			
		*/
		banners = null;
		},
		
		renderDiary: function() {
			console.log("renderDiary :this.childViews=");
			//console.log(this.childViews);
			_.forEach(this.childViews, function(childView) {
				childView.$el.hide();
			});

			var view = this.childViews[this.viewName];
			
			if (typeof(view) === "undefined") {
				//console.log("creating view for the first time", this.viewName);
				var diaryViewParams = {};
				diaryViewParams.el = $("<div></div>");
				view = this.childViews[this.viewName] = new this.diaryViewClass(diaryViewParams);
			} else {
				if (view.isDirty || view.isAlwaysDirty) {
					//console.log("Re-rendering dirty view", this.viewName);
					view.render();
					
					// Fix: performance: remove other views
//					var clickedView = this.viewName;
//					_.forEach(this.childViews, function (childView) {
//					 	if (clickedView===view){
//					 		console.log("################################# Don't Remove view= "+childView + " - "+clickedView);
//					 	}
//					 	else {
//					 		console.log("################################# Removing view= "+childView + " - "+clickedView);
//							//childView.remove();
//							//childView.undelegateEvents();
//					 	}	
//					});
//					
					
					view.isDirty = false;
				}
			}
			
			$(".diaryView", this.el).append(view.el);
			
			view.$el.show();
		//	$(".diaryView  #dayDiaryItemFormEditPopup").remove();
	
			
			if(oncecall)
			{
				oncecall=false;
				$("div[id*='dayDiaryItemFormEditPopup']").parents(".ui-dialog").remove();
				
				viewParams={}
				viewParams.el = $('#dayDiaryItemFormEditPopup', this.el);
				viewParams.editorref="editor1";
				viewParams.classes="dayDiaryItemFormEditPopup";
				diaryItemForm = new diary.view.DiaryItemView(viewParams);
			}
		},
		
		/**
		 * 
		 * @param {Rocketboots.date.Day} forDate The date for which the article is required
		 * NOTE - This is assuming that both the from date and to date are specified for the article
		 * 
		 * @return {?diary.model.ContentItem} The article that is to be displayed, if no article is found a NULL value will be returned
		 */
		getEmpowerBannerArticle: function(forDate) {
			
			// get the list of banner articles that we can display
			var bannerArticles,
				article;
			
			if (this.schoolProfile.get("contentMapping") && this.schoolProfile.get("contentMapping").get("calendarBanners")) {
				
				// get the list of all the banner articles
				bannerArticles = this.schoolProfile.get("contentMapping").get("calendarBanners").toArray();
				
				// we will try to find an article which is enabled within the specified
				// date
				article = _.find(bannerArticles, function(article) {
					// check if the article is enabled within the specified date
					return forDate.getTime() >= article.get("fromDate").getTime() && forDate.getTime() <= article.get("toDate").getTime();
				});
			
			} else {
				
				// no article could be determined
				article = null;
				
			}
			
			return article;
		} 	
		
	});
	
	/**
	 * Gets the date out of the element if one found.  The function is assuming that the passed element has an attribute
	 * with the name of dayKey
	 * 
	 * @param {!Object} element The object that is being searched for the day key
	 * 
	 * @param {?Date} The parsed date from the passed object, NULL if the object doesn't have the dayKey attribute
	 */
	view.determineDate = function(element) {
		
		// holds the date of the given element
		var date = null;
		
		// if the date key attribute is specified we will use it
		// to determine the date of the clicked element
		if (element.attr("dayKey")) {
			
			var dayKey = element.attr("dayKey");
			// parse the date that is in the element
			date = Rocketboots.date.Day.parseDayKey(dayKey);
			
		}
		
		// return the date representation of the element
		return date;
		
	};
	
	/**
	 * Navigates to the Day View
	 */
	view.navigateToDay = function(event) {
		var element = $(event.currentTarget),
	    selectedDate = diary.view.DiaryView.determineDate(element);
		
		// if the date key attribute is specified we will use it
		// to determine the date of the clicked element
		if (selectedDate) {
			app.context.trigger(EventName.VIEWDAY, selectedDate);
		}
	};
	
	/**
	 * Change Selected Date
	 */
	view.selectDay = function(event) {
		var element = $(event.currentTarget),
	    selectedDate = diary.view.DiaryView.determineDate(element);
		
		// if the date key attribute is specified we will use it
		// to determine the date of the clicked element
		if (selectedDate) {
			app.model.get(ModelName.APPLICATIONSTATE).setSelectedDay(selectedDate);
		}
	};
	
	/**
	 * Append a note entry to the specified note
	 * 
	 * @param {!number} noteId - The identifier of the note
	 * @param {!string} noteText - The text that is being appended to the note
	 */
	view.appendNote = function(noteId, noteText) {
		
		// check that both are specified
		if (noteId && noteText) {
			// first we will trim the note text
			noteText.trim();
			
			// check that a valid note was entered if not ignore it
			if (noteText.length > 0) {
				
				// create the note entry
				var noteEntry = new diary.model.NoteEntry({ 'timestamp' : new Date(),
														    'text': noteText });
				
				// we will save the note entry]
				app.context.trigger(EventName.APPENDNOTEENTRY, noteEntry, noteId);
			}
		}
		
	};
	
	
	return view;

})();