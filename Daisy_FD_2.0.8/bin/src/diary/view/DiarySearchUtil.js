diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};

/**
 * A collection of utility methods to be used by the main view for search.
 * created by Jane Sivieng
 * @type {Object}
 */
diary.view.DiarySearchUtil = {
		
	'mapSearchResults': function(searchResults) {
		var resultsObj = {};
		//console.log("searchResults "+JSON.stringify(searchResults));
		_.forEach(searchResults.get('results').toArray(), function(searchResultSection) {
			var key = searchResultSection.get('heading');
			var sectionResults = resultsObj[key] || [];
						
			resultsObj[key] = sectionResults.concat(diary.view.DiarySearchUtil.mapSearchResultSection(searchResultSection));
		});
		
		var resultsArray = [];
		
		for (var key in resultsObj) {
			resultsArray.push({
				'heading' : key,
				'resultItems' : resultsObj[key]				
			});
		}
		
		return {
			'results' : resultsArray
		};
	}, 
	
	'mapSearchResultSection': function(searchResultSection) {
		var resultItems = [];
					
		_.forEach(searchResultSection.get('sectionResults'), function(sectionResult) {

			var resultItem = null;
			
			
			if (sectionResult instanceof diary.model.DiaryItem || 
				sectionResult instanceof diary.model.ContentItem || 
				sectionResult instanceof diary.model.Subject ||
				sectionResult instanceof diary.model.Class) {
				
				
				resultItem = diary.view.DiarySearchUtil.mapSectionResult(
						sectionResult, 
						searchResultSection.get('itemType'), 
						searchResultSection.get('resultsType'));
			} // else if something else we'll map it too :)
			
			
			if (resultItem) {
				resultItems.push(resultItem);
			}
			
			
		});
		
		return resultItems;
	}, 
	
	'mapSectionResult' : function(sectionResult, itemType, resultsType) {
		//console.log("sectionResult : "+JSON.stringify(sectionResult));
		var resultItem = null;
		//console.log("itemType : "+itemType);
		if(itemType == "class"){
			resultItem = {
				'title': sectionResult.get('name'), 
				'itemType': itemType, // task, event, note, school, empower, banner
				'messageFrom': sectionResult.get('messageFrom'), // task, event, note, school, empower, banner
				'resultsType': resultsType, // diaryItem or contentItem
				'itemId': sectionResult.get('classId')			
			};
			
		}
		
		else{
			
			
			resultItem = {
				'title': sectionResult.get('title'), 
				'itemType': itemType, // task, event, note, school, empower, banner
				'messageFrom': sectionResult.get('messageFrom'), // task, event, note, school, empower, banner
				'resultsType': resultsType, // diaryItem or contentItem
				'itemId': sectionResult.get('id')			
			};
		}
		
		
		// bit of a hack but we need the specific diary item type if this is a task
		if (sectionResult instanceof diary.model.Task) {
			resultItem.itemType = sectionResult.get('type');
		}
		
		return resultItem;
	}	

};