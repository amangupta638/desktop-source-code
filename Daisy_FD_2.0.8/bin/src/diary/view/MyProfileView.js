diary = diary || {};
diary.view = diary.view || {};
var grades, country, states = {}, modeEdit = false,isStudent, myTeachers, ckEditInstance, first = 1;
diary.view.MyProfileView = (function() {

	var myProfileView = Rocketboots.view.View.extend({
		events: {
			"click .editProfile"		    										: "editProfile",		// hide overdue items
			"click .myClassesView .tabs_container .tabMenu_btn ul li.myParents"		: "myParentsView",
			"click .myClassesView .tabs_container .tabMenu_btn ul li.myTeachers"	: "myTeachersView",
			"click .myClassesView .tabs_container .tabMenu_btn ul li.myProfile"		: "myProfileView",
			"click .thcr_tbl_lt table tr"											: "displayTeacherInfo",
			"click #sendParentInvitation"		    								: "sendInvitationToParent",
			"click .rowParent"		    											: "showParentDetails",
			"click .rowTeacher"		    											: "showTeacherDetails",
			"change #countryTextBox"		    									: "getStatesForCountry",
			"click #sendMsg"														: "sendMail",
			"click .teacherEmail"													: "showPopup",
			"click #cancelPopupSendMsgBtn"											: "cancelPopup",
			"click #sendMsgBtn"														: "sendMsg",
			"click #stateTextBox"													: "selectState",
			"blur #stateTextBox"													: "deSelectState",
			"change #browseImage"				 									: "browseImg"
		},
		initialize: function(options) {
			this._appState = app.model.get(ModelName.APPLICATIONSTATE);
			//alert("init myprofile");
			var view = this;
			//getUserDetail();
			
			//grades = app.model.get(ModelName.GRADES);
			//country = app.model.get(ModelName.COUNTRY);
			//this.getStatesForCountry();
			isStudent = (app.model.get(ModelName.STUDENTPROFILE)).get('isStudent');
			//alert("alert : isStudent : "+isStudent);
			/*ckEditInstance = CKEDITOR.instances['editor1'];
			if(ckEditInstance){
				console.log("CKEDITOR.remove");
				CKEDITOR.remove(ckEditInstance);
			}
			CKEDITOR.replace( 'editor1' );*/
			this.render();
			
		},				
		render: function() {
			//alert("render");
//			if (!(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher"))) {
//				insertMyTeachers();
//				getParents();
//			}
			
			var view = this;
			calendarDates = view._appState.getCalendarDates();
			var params = {
				"schoolImage": app.model.get(ModelName.SCHOOLPROFILE).get('schoolImage')
			}
			
			var c = app.render('myProfileView',params);
			//console.log("my profile render : "+c);
			$(this.el).html(c);	
			updateProfileView = function() {
				//console.log("updateProfileView");
				view.isDirty = true
				view.render();
			};
			if(app.model.get(ModelName.STUDENTPROFILE).get("profileImage") == null){
				app.model.get(ModelName.STUDENTPROFILE).set("profileImage", "");
			}
			
			this.myProfileView();
			
		},
		
		editProfile : function(){
			var student = app.model.get(ModelName.STUDENTPROFILE);
			//console.log("student.get(dateOfBirth) : "+student.get("dateOfBirth"));
			var date = (student.get("dateOfBirth") != "" && student.get("dateOfBirth") != null && student.get("dateOfBirth") != "0000-00-00") ? student.get("dateOfBirth").split("-") : new Date();
			var d = new Date();
			if(date != ""){
				d= new Date(date[0],(date[1]-1),date[2]);
			}
			
			
		
			var view = this;
			$("#dateOfBirthTextBox").datepicker({
					'showOn' : "button",
					'buttonImage' : "assets/frontpage/calendar.png",
					'buttonImageOnly' : true,
					'dateFormat': 'dd-mm-yy',
					'defaultDate' : d,													
					'changeYear' : true,
					'changeMonth' : true,
					'duration' : 0,
					//'minDate' : new Date(1900, 0, 1),
					'maxDate' : "-5y",
					'yearRange' : "1900:-5",
					'onSelect' : function(dateText, inst) {
						//formatDate Requires a new date object to work
						var selectedDate = $(this).datepicker("getDate");
						console.log("selectedDate : "+selectedDate)
						view.populateDate(selectedDate);
						
					},
					'beforeShow' : function (textbox, instance) {   
		                /* instance.dpDiv.css({
		                    marginTop: -(textbox.offsetHeight + 200) + 'px',
		                   
		                }); */
		            }
				});
			
			
			
			if($("#saveProfileBtn").is(":visible")){
				modeEdit = true;
				$("#saveProfileBtn").hide();
				$("#editProfileBtn").show();
				var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
				var firstName = $("#nameTextBox").val();
				var lastName = $("#lastNameTextBox").val();
				var dob = $("#dateOfBirthTextBox").val();
				var imageUrl = $(".nameStrip_student #profileImg").attr('src');
				var telePhone = $("#telephoneTextBox").val();
				var countryId = $("#countryTextBox option:selected").val();
				var country = $("#countryTextBox option:selected").text();
				var stateId = $("#stateTextBox option:selected").val();
				var state = $("#stateTextBox option:selected").text();
				var postalCode = $("#postcodeTextBox").val();
				var cityTownSuburb = $("#suburbTextBox").val();
				var address_line_1 = $("#streetTextBox").val();
				var email = $("#emailId").text();
				//console.log("imageUrl : "+imageUrl);
				//console.log("browseImage : "+$("#browseImage").val())
				if(imageUrl == "assets/home/images/icon_profile.png"){
					imageUrl = "";
				}
				updateUserProfile(userId,firstName,lastName,dob,telePhone, grade, email, imageUrl);
			}
			else{
				modeEdit = false;
				$("#saveProfileBtn").show();
				$("#editProfileBtn").hide();
			}
			$(".nonEditable").each(function(){
				  $(this).toggle();
			});
			$(".editable").each(function(){
				  $(this).toggle();
			});
			
			var grade = student.get('grade');
			$("#spanGrade").show();
			/*var state = student.get('address').get("state");
			var country = student.get('address').get("country");
			console.log("state : "+state);
			console.log("country : "+country);
			$("select#gradeTextBox option").filter(function() {
				console.log('inside priority'+$(this).text());
				return $(this).text() == grade;
			}).attr('selected', true);
			
			$("select#countryTextBox option").filter(function() {
				console.log('inside priority'+$(this).text());
				return $(this).text() == country;
			}).attr('selected', true);
			
			$("select#stateTextBox option").filter(function() {
				console.log('inside priority'+$(this).text());
				return $(this).text() == state;
			}).attr('selected', true);
			*/
		},
		
		populateDate : function (selectedDate) {
			var schoolFormat = app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat');
			// default to no text
			var DOBHolderText = "",
				DOB = null;
			//console.log("selectedDate11 : "+selectedDate);
			// if no date is selected we will blank the display date
			if (selectedDate) {
				//console.log("selectedDate22 : "+selectedDate);
				//Set a new var with different format to use
				
				   if (schoolFormat == "USA")
					   	DOBHolderText = $.datepicker.formatDate("mm/dd/yy", selectedDate);
				   else
					    DOBHolderText = $.datepicker.formatDate("dd/mm/yy", selectedDate);
				DOB = new Rocketboots.date.Day(selectedDate);
				
			}
			
			//Choose the div you want to replace
			$("#dateOfBirthTextBox").val(DOBHolderText);
			//$("#spanDob").text(DOB != null ? DOB.getKey() : "");
		},
		displayTeacherInfo : function(event){
			var id = $(event.currentTarget).attr('userId');
		},
		
		myProfileView : function(event){
			
			var profileImage = null;
			var profileImageToDisplay = null;

			var student = app.model.get(ModelName.STUDENTPROFILE);
			
			profileImage = student.get('profileImage');
			
			if (profileImage != null && profileImage != '')
			{
				profileImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + profileImage;
			}
			else
			{
				profileImageToDisplay = 'assets/home/images/icon_profile.png';
			}
			
			student.set('profileImageToDisplay', profileImageToDisplay);
		
		
			$('#userProfileContainer', this.el).html(app.render('userProfileView',student.toJSON()));
			var view = this;
			
			var params = {
					"isStudent" : isStudent,
					"allowParents" : app.model.get(ModelName.SCHOOLPROFILE).get("allowParents") == 1 ? true : false
			}
			$('#subTabProfileView', this.el).html(app.render('tabContainerProfileView', params));
			//console.log("student : before render "+JSON.stringify(student));
			//console.log("student : before render "+student.get("dateOfBirth"));
			
			 var date = (student.get("dateOfBirth") != "" && student.get("dateOfBirth") != null && student.get("dateOfBirth") != "0000-00-00") ? student.get("dateOfBirth").split("-") : "";
			 var dateOfBirth = "";
			 if(date != ""){
				if (app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat') == "USA")
				   	dateOfBirth = date[1] + "/" + date[2] + "/" + date[0];
			    else
				    dateOfBirth = date[2] + "/" + date[1] + "/" + date[0];
			 }
			
			
			var userProfileDetails = {
				"user" : student.toJSON(),
				"address" : student.get("address").toJSON(),
				"isStudent" : isStudent,
				//"grades" : app.model.get(ModelName.GRADES).toJSON(),
				//"states" : states.toJSON(),
				//"countries" : app.model.get(ModelName.COUNTRY).toJSON(),
				"dateOfBirthUser" : dateOfBirth
				
			}
			$('#subTabProfileView .tabsContent', this.el).html(app.render('myProfileUserProfileView', userProfileDetails));
			
			

			if(isStudent){
				$('.parentsAndGuardian', this.el).html(app.render('myProfileParentsAndGuardianView', {}));
			    $('.teacherProfile', this.el).html(app.render('myProfileTeacherProfileView', {}));
			}
			
		},
		
		myParentsView : function(event){
			this.getParentsList();
			var parentCnt = 0;
			var parentLen = 0;
			
			var parentImage = null;
			var parentImageToDisplay = null;	
			
			var parentsObj = null;
				
			  /* if($('.tabs_container .tabMenu_btn ul li.myParents').hasClass("active")){
			  	return;
			  } */
			  $('.tabs_container .tabMenu_btn ul li.myProfile').removeClass("active");
			  $('.tabs_container .tabMenu_btn ul li.myTeachers').removeClass("active");
			  $('.tabs_container .tabMenu_btn ul li.myParents').addClass("active");
			  $('.tabContent_row.myProfile').hide();
			  $('.tabContent_row.myTeachers').hide();
			  $('.tabContent_row.myParents').show();
			 console.log("app.model.get(ModelName.PARENTS) : "+JSON.stringify(app.model.get(ModelName.PARENTS)));
			 
			 parentsObj = app.model.get(ModelName.PARENTS);
			 
			 parentLen = parentsObj.length;
			 
			 for (parentCnt = 0; parentCnt < parentLen; parentCnt++)
			 {
				 parentImage = parentsObj.at(parentCnt).get('imageUrl');
				 
				 if (parentImage != null && parentImage != '')
				 {
					 parentImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + parentImage;
				 }
				 else
				 {
					 parentImageToDisplay = 'assets/myProfile/images/missing-thumb.png';
				 }
				 
				 parentsObj.at(parentCnt).set('parentImageToDisplay', parentImageToDisplay);
			 }
			 
			  var viewparams = {
					  "parents" : app.model.get(ModelName.PARENTS).toJSON()
			  }
			  
			  $('.parentsAndGuardian', this.el).html(app.render('myProfileParentsAndGuardianView', viewparams));
			  var firstParent = app.model.get(ModelName.PARENTS).at(0).get("uniqueId");
			  var viewparams = {
					  "parents" : app.model.get(ModelName.PARENTS).at(0).toJSON()
			  }
			  $('#rowParent'+firstParent).removeClass("rowParent");
			  $('#rowParent'+firstParent).addClass("selected");
			  $('#parentDetails', this.el).html(app.render('myProfileParentsAndGuardianDetailsView', viewparams));
		},
		
		myTeachersView : function(event){
			
			var teacherCnt = 0;
			var teacherLen = 0;
			
			var teacherImage = null;
			var teacherImageToDisplay = null;
			
			
			  /* if($('.tabs_container .tabMenu_btn ul li.myTeachers').hasClass("active")){
			  	return;
			  } */
			  $('.tabs_container .tabMenu_btn ul li.myProfile').removeClass("active");
			  $('.tabs_container .tabMenu_btn ul li.myTeachers').addClass("active");
			  $('.tabs_container .tabMenu_btn ul li.myParents').removeClass("active");
			  $('.tabContent_row.myProfile').hide();
			  $('.tabContent_row.myTeachers').show();
			  $('.tabContent_row.myParents').hide();
			 
			  myTeachers = this.getTeachersList();
			  var selectedTeacher;
			  
			  teacherLen = myTeachers.length;
			  
			  if(teacherLen > 0){
			  		selectedTeacher = myTeachers[0];
			  }
			  
			  teacherImage = selectedTeacher.Image_url;
			  
			  if (teacherImage != null && teacherImage != '')
			  {
				  teacherImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + teacherImage;
			  }
			  else
			  {
				  teacherImageToDisplay = 'assets/myProfile/images/missing-thumb.png';
			  }

			  selectedTeacher.profileImageToDisplay = teacherImageToDisplay;
			  
			  
			  for (teacherCnt = 0; teacherCnt < teacherLen; teacherCnt++)
			  {
				  teacherImage = myTeachers[teacherCnt].Image_url;
				  
				  if (teacherImage != null && teacherImage != '')
				  {
					  teacherImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + teacherImage;
				  }
				  else
				  {
					  teacherImageToDisplay = 'assets/myProfile/images/icon_tchr.png';
				  }

				  myTeachers[teacherCnt].profileImageToDisplay = teacherImageToDisplay;
				  
			  }
			  
			  if(myTeachers.length > 0){
			  		selectedTeacher = myTeachers[0];
			  }
			  var viewParams = {
				'myTeachers' : myTeachers,
				'selectedTeacher' : selectedTeacher
			  };
			  
			  //alert('viewParams ' + JSON.stringify(viewParams));
			  
			  //console.log("viewparams : "+JSON.stringify(viewParams));
			  $('.teacherProfile', this.el).html(app.render('myProfileTeacherProfileView', viewParams));
			  $('#rowTeacher1').addClass("selected");
			  $('#rowTeacher1').removeClass("rowTeacher");
		},
		
		getParentsList : function(){
			var parents = new diary.collection.ParentCollection();
			var users = null;
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "SELECT Distinct parent_id, profile, email, image_url, name, phone, image FROM myParent WHERE status = 1 and isDeleted = 0" ,
	
	            // params
	            {
				
	            },
				// success
				function(statement) {
					var allusers =_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    
                    var users = [];
					$.each(allusers,function(index,record){
							// console.log("parentDetails[index].NormalUserId : "+parentDetails[index].NormalUserId)
		                	 var modelParent =  new diary.model.Parent();
		                	 modelParent.set("uniqueId", record.parent_id);
		                	 modelParent.set("email", record.email);
		                	 modelParent.set("imageUrl", record.image_url);
		                	 modelParent.set("name", record.name);
		                	 modelParent.set("phone", record.phone);
		                	 parents.push(modelParent);
		                 
		            });
					app.model.set(ModelName.PARENTS, parents);
				},
				
				// error
				function error()
				{
					
				}
			);
			
            return users;
		},
		
		getTeachersList : function () {
			
			var users = [];
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "SELECT Distinct teacher_id, profile, email, image_url, name, phone, image, subjectName FROM myTeacher WHERE status = 1 and isDeleted = 0" ,
	
	            // params
	            {
				
	            },
				// success
				function(statement) {
					var allusers =_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    
					$.each(allusers,function(index,record){
						var obj1 = new Object();
						
						obj1.UserName 	= record.name
					  	obj1.UserId 	= record.teacher_id
					  	obj1.Profile 	= record.profile
					  	obj1.Email 		= record.email
					  	obj1.Phone 		= record.phone
					  	obj1.Image_url 	= record.image_url
					  	obj1.Image 		= record.image
					  	obj1.SubjectNames = record.subjectName
					  	obj1.id			= index+1
					  	obj1.profileImageToDisplay = ''
					  	
					  	users.push(obj1);     
		            });
				},
				
				// error
				function error()
				{
					
				}
			);
			
            return users;
		},
		sendInvitationToParent : function(){
			
			var email = $("#parentEmailId").val();
			
			var allowedChars = "[-0-9A-Za-z!#$%&'*+/=?^_`{|}~.]+";
			// allow any number of allowedChars followed by only one '@' then some allowedChars, then a '.' then more allowedChars
			// note '.' is an allowedChar, so will still match "joe.blogs@rocketboots.com.au"
			if (email.length > 0) {
				if (! email.match("^" + allowedChars + "@" + allowedChars + "[.]" + allowedChars)) {
					openMessageBox("Invalid email format");
				
				}
				else{
					sendInvitationToParent(email);
				}
				//return "Invalid email address";
			} else {
				openMessageBox("Please enter parent's email");
			}
			
		},
		showParentDetails : function(event){
			
			$(".profile_tbl_ltMid .selected").addClass("rowParent");
			$(".profile_tbl_ltMid .selected").removeClass("selected");
			$(event.currentTarget).removeClass("rowParent");
			$(event.currentTarget).addClass("selected");
			
			var currentUserId = $(event.currentTarget).attr("userId");
			var selectedParent = getObjectByValue(app.model.get(ModelName.PARENTS), currentUserId, "uniqueId");
			//console.log("selectedParent : "+JSON.stringify(selectedParent));
			
			teacherImage = selectedParent.Image_url;
			  
			if (teacherImage != null && teacherImage != '')
			{
			  teacherImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + teacherImage;
			}
			else
			{
			  teacherImageToDisplay = 'assets/myProfile/images/missing-thumb.png';
			}

			selectedParent.profileImageToDisplay = teacherImageToDisplay;
			
			
			  var viewparams = {
					  "parents" : selectedParent.toJSON()
			  }
			  $('#parentDetails', this.el).html(app.render('myProfileParentsAndGuardianDetailsView', viewparams));
			  
		},
		showTeacherDetails : function(event){
			
			myTeachers = this.getTeachersList();
			var teacherImage = null;
			var teacherImageToDisplay = null;
			
			$(".thcr_tbl_lt .selected").addClass("rowTeacher");
			$(".thcr_tbl_lt .selected").removeClass("selected");
			$(event.currentTarget).removeClass("rowTeacher");
			$(event.currentTarget).addClass("selected");
			var currentUserId = parseInt($(event.currentTarget).attr("userId"));
		  
			var selectedTeacher = _.filter(myTeachers, function(teacher) {
				return teacher && teacher.UserId == currentUserId;
			});
			
			teacherImage = selectedTeacher[0].Image_url;
			  
			if (teacherImage != null && teacherImage != '')
			{
			  teacherImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + teacherImage;
			}
			else
			{
			  teacherImageToDisplay = 'assets/myProfile/images/missing-thumb.png';
			}

			selectedTeacher[0].profileImageToDisplay = teacherImageToDisplay;
			
			var viewParams = {
				'selectedTeacher' : selectedTeacher
			};
			
			$('#myProfileTeacherDetails', this.el).html(app.render('myProfileTeacherDetails', viewParams));
		},
		getStatesForCountry : function(){
			console.log("getStatesForCountry");
			var selectedCountry = $("#countryTextBox option:selected").val();
			states = app.model.get(ModelName.STATES).getStatesForCountry(selectedCountry);
			console.log("states : "+JSON.stringify(states));
			var $el = $("#stateTextBox");
			$el.empty(); // remove old options
			 $el.append($("<option></option>")
				     .attr("value", 0).text("-- No States --"));
			for(var len = 0 ; len < states.length; len++){
				console.log("get States For Country");
				 $el.append($("<option></option>")
			     .attr("value", states.at(len).get("uniqueId")).text(states.at(len).get("name")));
			}
		},
		sendMail : function(event){
			console.log("send mail to teacher"+$(".UserId").val());
			var inboxView = new diary.view.InboxView();
			
			app.view.mainView.switchToView("inbox",{
				id: "",
				type: "",
				forceRefresh: true
			});
			inboxView.createOrReplyMessage(event);
		},
		showPopup : function(event){
			console.log("showPopup send msg"+$(event.currentTarget).text());
				reminderParam = {
						"to" : $(event.currentTarget).text()
				};
				$('#popupDivSendMsg', this.el).html(app.render('popupSendMessage',reminderParam));
				
				/*console.log("ckEditInstance"+ckEditInstance);
				if(ckEditInstance){
					console.log("CKEDITOR.remove");
					CKEDITOR.remove(CKEDITOR.instances['editor1']);
					
				}
				console.log("ckEditInstance"+ckEditInstance);
				CKEDITOR.replace( 'editor1' );
				*/
				$('#popupSendMsg').show();
		        $('#myProfileFade').show();		

		},
		cancelPopup : function(){
			  $('#popupSendMsg').hide();
	          $('#myProfileFade').hide();
		},
		sendMsg : function(){
			
			
			var teacherId = $("#recvrId").val().replace(/ /g, "");
			var ccList = [];
			if($("#cc").val() != ''){
				var ccArr = $("#cc").val().split(",")
				for (var count in ccArr) {
					ccList.push(ccArr[count].replace(/ /g, ""));
				}
			}
			var bccList = [];
			if($("#bcc").val() != ''){
				var bccArr = $("#bcc").val().split(",")
				for (var count in bccArr) {
					bccList.push(bccArr[count].replace(/ /g, ""));
				}
			}
			var subject = $("#subject").val();
			var msgText = $("#msgText").val();
			var data = $("#cke_contents_msgText iframe").contents().find("body").text()
			// var data = CKEDITOR.instances.editor1.getData();
			
			if(msgText){
				msgText = msgText.replace(/(\r\n|\n|\r)/gm," ");
			}
			
			reminderParam = {
					"to" : teacherId,
					"receiver_cc" : ccList,
					"receiver_bcc" : bccList,
					"subject" : subject,
					"msgText" : msgText
			};
			console.log("reminderParam : "+JSON.stringify(reminderParam));
			if(subject == ""){
				openMessageBox("Please Enter subject");
			}
			else{
				sendMessageTeacher(reminderParam);
				$('#popupSendMsg').hide();
		        $('#myProfileFade').hide();
			}
		},
		selectState : function(event){
			console.log("selectState");
			$("#stateTextBox").attr("size","5");
		},
		deSelectState : function(event){
			console.log("deSelectState");
			$("#stateTextBox").attr("size","1");
		},
		browseImg : function(event){
			var view = this, element = $(event.currentTarget), filePath = element.val();
			if(filePath == ""){
				
				filePath = $(".nameStrip_student #profileImg").attr('src');
			}
			else{
				$(".nameStrip_student #profileImg").attr('src', filePath);
			}
	
		}
		

	});
	
	return myProfileView;

})();