diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};
var elView = null;
var clickedcolor=false;
diary.view.ClassView = (function() {

    var _MODE = {
        'UPDATE' : 1,
        'CREATE' : 2
    };

    var view = Rocketboots.view.View.extend({

        events: {
            "click .editButtonContainer .cancel"		: "closeDialog",
            "click .lockContainer .delete"				: "deleteClass",
            "click .editButtonContainer .save"			: "saveClass",
            "click .subjectColourSwatch"	            : "chooseSubjectColour"
        },

        initialize: function(options) {

            Rocketboots.view.View.prototype.initialize.call(this);
            //console.log('ClassView.initialize()');

            this.editing = false;
            this.mode = _MODE.UPDATE;
            this.dialog = null;
            this.timetable = null;
            this.currentSwatchColour = "";

            this.parseAdditionalParams(options);
            this.render();
        },

        parseAdditionalParams: function(options) {
            if (typeof(options.el) !== "undefined") {
                this.el = options.el;
				  this.classes = options.classes;
            }
            if (typeof(options.timetable) !== "undefined") {
                this.timetable = options.timetable;
            }
        },

        render: function() {
 
            var view = this,
                viewParams = {
                    'colourPalette': []
                };

            // map colour palette to array
            for (var colourKey in diary.model.Subject.Colours) {
                viewParams.colourPalette.push({ 'id' : colourKey, 'colour' : diary.model.Subject.Colours[colourKey]});
            }
			//console.log("classFormEdit params")
            var content = app.render('classFormEdit', viewParams);
            
            $(view.el).html(content);
           
			elView = view.el;
            

            view.inputs = {
                'subject'           : $("input[name=subject]", view.el),
                'room'              : $("input[name=room]", view.el),
                'subjectColour'     : $("input[name=subjectColour]", view.el),
                'colourSwatch'      : $("div.subjectColourSwatch", view.el),
                'colourName'        : $("span.colourName", view.el),
                'time'              : $("td.time", view.el),
                'deleteBtn'			: $(".delete", view.el)
            };
			
            view.dialog = $(this.el).dialog({
                modal: true,
                width: 360,
                autoOpen: false,
                close: function(event, ui) {
                    view.editing = false;
                    view.resetFormFields();
                    app.model.get(ModelName.APPLICATIONSTATE).set('modalDialogVisible', false);
                }
            });

            $("input[name=subject]", view.el).typeAhead({
                source: app.model.get(ModelName.SUBJECTS)
            }).change(function(event) {
                var target = $(event.target);
                target.val(target.val().trim());
				var oldVal = target.data('oldVal');

                // test if this is an existing subject
				var existing = false;
				app.model.get(ModelName.SUBJECTS).each(function(s) {
							if(!existing) {
								if(s.get('title') == target.val()) {
									view.setSubjectColour(s.get('colour'));
									existing = true;
								} else if(s.get('title') == oldVal) {
									view.setSubjectColour(app.model.getNextColour());
								}
							}
						});
				target.data('oldVal', target.val());
            });

            $("#classFormSubjectColourPicker", view.el).on("click", ".subjectcolour", function(event) {clickedcolor=true;
                var colourIndex = $(event.currentTarget).attr("colourIndex");
                app.popup.hide($("#classFormSubjectColourPicker", view.el));
                view.setSubjectColour(colourIndex);
            });

        },

        /*
         * FUNCTIONS FOR UPDATING THE HTML FORM
         */
        showDialog: function(classId, attrs, isAdmin) {
       
            var view = this,
                classObj = null,
                period,
                title = "";
            $("div[id*='classFormEditPopup']").parents(".ui-dialog").remove();
       	    view.dialog = $(this.el).dialog({
                modal: true,
                width: 360,
                autoOpen: false,
                close: function(event, ui) {
                    view.editing = false;
                    view.resetFormFields();
                    app.model.get(ModelName.APPLICATIONSTATE).set('modalDialogVisible', false);
                }
            });
            $(".tableFieldLayout").css("width", "100%");
           // console.log(classId, JSON.stringify(attrs));

            if (classId != null) {
                classObj = view.timetable.getClass(classId);
               // console.log(classId, JSON.stringify(classObj));
            }

            if (!view.editing) {
                view.resetFormFields();
                view.classObj = classObj;
                if (classObj == null) { // we are creating a new class
                    view.mode = _MODE.CREATE;
                    period = view.timetable.getPeriod(attrs.periodId);
                    title = "Add New Class: Day " + attrs.cycleDay + " &raquo; " + period.get('title');
                    $(".ui-dialog-title").html(title);
                    view.classObj = new diary.model.Class(attrs);
                } else {
                    view.mode = _MODE.UPDATE;
                    period = view.timetable.getPeriod(classObj.get('periodId'));
                    title = "Edit: Day " + classObj.get('cycleDay') + " &raquo; " + period.get('title');
                    $(".ui-dialog-title").html(title);
                    view.inputs.deleteBtn.show();
                }
                view.initFormFields(view.classObj, period);
                if(isAdmin){
                	$("#isAdmin").val(true);
                } else {
                	$("#isAdmin").val(false);
                }
                view.dialog.dialog('open');
                app.model.get(ModelName.APPLICATIONSTATE).set('modalDialogVisible', true);
                view.editing = true;
            }
            	var school=app.model.get(ModelName.SCHOOLPROFILE);
				var schoolFormat=school.get('RegionalFormat');
				//console.log("school Format=="+schoolFormat);
				if(schoolFormat=="USA")
                {
                     $("#colorDiv .alignTop").html("Color");  
                 } 
            $("input[name=subject]", view.el).typeAhead('update', { source: app.model.get(ModelName.SUBJECTS) });

        },


        closeDialog: function() {
            var view = this;
            view.dialog.dialog('close');
            view.editing = false;
        },

        deleteClass: function() {
            var view = this;

            if (view.classObj.get('id')) {
            	var eventParameters = getClassEventParameters(view.classObj.get('subject').get('classTitle'), view.classObj.get('room'), view.classObj.get('subject').get('colour'));
      			//alert("eventParameters : " + JSON.stringify(eventParameters));
      			FlurryAgent.logEvent("Class Deleted", eventParameters);
                app.context.trigger(EventName.DELETECLASS, view.timetable, view.classObj.get('id'));
                view.closeDialog();
            }
        },

        saveClass: function() {
			var moveon=true;
			
            var view = this;
			
            var subjectName=view.inputs.subject.val();
            var roomNo= view.inputs.room.val();
			//alert("subjectName= "+subjectName)
			if(subjectName == "")
			{ 
				openMessageBox("Please Enter Subject")
				
				moveon=false;
				return;
				
			}
			if(subjectName[0] == " ")
			{ 
				openMessageBox("Please dont enter spaces in starting of  Subject")
				
				moveon=false;
				return;
				
			}
			
			//var roomNo=$("#classFormEditPopup .roomNo").val();
			if(roomNo == "")
			{
				openMessageBox("Please Enter Room")
				moveon=false;
				return;
				
			}
			if(roomNo[0] == " ")
			{ 
				openMessageBox("Please dont enter spaces in starting of  Room")
				
				moveon=false;
				return;
				
			}
			//alert("moveon="+moveon);
			if(moveon)
			{
            var view = this;



            if (view.mode == _MODE.UPDATE) {
                view.updateClass();
            } else if (view.mode == _MODE.CREATE){
                view.createClass();
            }
			}
        },

        createClass: function() {
        //console.log("createClass");
            var view = this,
                attrs = view.getAttributesForSave();
                
			//console.log("view.timetable="+view.timetable);
			//console.log(attrs)
			//console.log(attrs.subjectColour);
				var color="";
                if(isHexCode(attrs.subjectColour))
                {
                	//console.log("IF");
                	color=getHexCode(attrs.subjectColour);
                }
                else
                {
                
                	//console.log("ELSE")
                	color=attrs.subjectColour;
                	
                }
              // console.log("CREATE CLASS"+color)
            	app.context.trigger(EventName.CREATECLASS, view.timetable,
            						view.classObj.get('periodId'), view.classObj.get('cycleDay'),
                					attrs.subjectTitle, attrs.room, color);

            	view.closeDialog();
            	// change for Defect EZTEST-407 - call addClassSubject webservice after adding class to timetable
            	updateClass();
        },

        updateClass: function() {
            var view = this;
            var isAdmin = $("#isAdmin").val();
            var oldClassObj = view.classObj;

            if (!(oldClassObj instanceof diary.model.Class)) {
               // console.log("Error: invalid class passed to update class");
                return;
            }

            var classObj = oldClassObj.clone(),
                attrs = view.getAttributesForSave(classObj.get('subject').get('title'));
               // console.log("UPDATE CALSS COLOR+"+attrs.subjectColour);
                var color="";
                if(isHexCode(attrs.subjectColour))
                {
                	color=attrs.subjectColour;
                }
                else
                {
                	//color=getHexCode(attrs.subjectColour);
                	//As color was getting replaced to MAROON if color is not present in the list
                	color=attrs.subjectColour;
                }
               // console.log("UPDATE CLASS COLOR="+color);
           	 app.context.trigger(EventName.UPDATECLASS, view.timetable, classObj.get('id'),
                attrs.subjectTitle, attrs.room, color,classObj.get('subject').get('id'),isAdmin);

            view.closeDialog();
            updateClass();
        },

        resetFormFields: function() {
            var view = this;

            // hide delete button
            view.inputs.deleteBtn.hide();
        },

        initFormFields: function(classObj, period) {
            var view = this;

            var time = diary.view.DiaryCalendarViewUtil.formatTime(period.get('startTime'), true, true)
                + " - " + diary.view.DiaryCalendarViewUtil.formatTime(period.get('endTime'), true, true);
            view.inputs.time.html(time);
            
            var className = null;
            if(classObj.get('subject') && period){
            	className = getObjectByValues(app.model.get(ModelName.ALLCLASSES), classObj.get('subject').get('id'), 'subjectId',classObj.get('cycleDay'),'cycleDay', period.get('id'), 'periodId');
            }
            view.inputs.subject.val(className != null ? className.get('name') : classObj.get('subject').get('title'));
            view.inputs.room.val(classObj.get('room') || "");

            if (view.classObj.get('subject').get('colour') != null) {
                view.setSubjectColour(view.classObj.get('subject').get('colour'));
            } else {
                var nextColour = app.model.getNextColour();
				clickedcolor=true;
                view.setSubjectColour(nextColour);
            }

            $(".editButtonContainer", view.el).show();
        },

        getAttributesForSave: function(title) {
        	var isAdmin = $("#isAdmin").val();
            var view = this;
            return {
                'subjectTitle': (isAdmin && isAdmin == 'true') ? title : view.inputs.subject.val().toUpperCase(), //'NEW SUBJECT',
                'subjectColour': view.inputs.subjectColour.val(),
                'room' : view.inputs.room.val()
            };
        },

        chooseSubjectColour: function(event) {
            var view = this;
            app.popup.show( $("#classFormSubjectColourPicker", view.el), $(event.currentTarget), "bottom", null, event );
        },

        setSubjectColour: function(colourIndex) {
			//console.log("SetSubjectColor");
			//console.log("colourIndex="+colourIndex);
            var view = this;

            view.inputs.subjectColour.val(colourIndex);
				 if(clickedcolor)
   {var c=colourIndex
   clickedcolor=false;
   }else{
   var c=getkey(colourIndex)
   }
			
			
			
			
            //view.inputs.colourName.html(c);
            if (view.currentSwatchColour != "") {
                view.inputs.colourSwatch.removeClass(view.currentSwatchColour);
            }
			//console.log("colourIndex="+colourIndex);
			//console.log("isHexCode(colourIndex)====="+checkHexCode(colourIndex));
			//colourIndex=(checkHexCode(colourIndex)?getkey(colourIndex):colourIndex);
			//console.log("after change="+colourIndex);
			
			colourIndex=(getColourValue(colourIndex) ? getColourValue(colourIndex) : colourIndex);
            view.inputs.colourSwatch.css("background-color", colourIndex);
            view.currentSwatchColour = colourIndex;
        }

    });

    return view;

})();
