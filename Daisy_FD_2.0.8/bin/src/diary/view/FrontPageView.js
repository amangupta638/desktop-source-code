diary = diary || {};
diary.view = diary.view || {};

diary.view.FrontPageView = (function() {

	// the function to populate the selected date for the date of birth
	function populateDate(selectedDate) {
		
		// default to no text
		var DOBHolderText = "",
			DOB = null;
		
		// if no date is selected we will blank the display date
		if (selectedDate) {
			//Set a new var with different format to use
			DOBHolderText = $.datepicker.formatDate("dd/mm/yy", selectedDate);
			DOB = new Rocketboots.date.Day(selectedDate);
		}
		
		//Choose the div you want to replace
		$("#DOBHolder").html(DOBHolderText);
		$("#DOB").val(DOB != null ? DOB.getKey() : "");
	}
	
	var view = Backbone.View.extend({
		
		events: {
			"click .studentProfileOverview .editButtonContainer .editStudentProfileLaunch" 	:	"editStudentProfile",
			"click .studentProfileOverview .editButtonContainer .editStudentProfileDone" 	:	"updateStudentProfile",
			"click .studentProfileOverview .editButtonContainer .editStudentProfileCancel" 	:	"cancelEditStudentProfile",
			"click .studentProfileOverview1 .editButtonContainer .editNewStudentProfileLaunch" 	:	"editNewStudentProfile",
			"click .studentProfileOverview1 .editButtonContainer .editNewStudentProfileDone" 	:	"updateNewStudentProfile",
			"click .studentProfileOverview1 .editButtonContainer .editNewStudentProfileCancel" 	:	"cancelEditNewStudentProfile"
		},
		
		initialize: function(options) {
			var view = this,
				updateModel = function () {
					view.model.set({
						school: app.model.get(ModelName.SCHOOLPROFILE),
						student: app.model.get(ModelName.STUDENTPROFILE)
					});
				};
			
			//console.log('FrontPageView.initialize()');
			
			this.parseAdditionalParams(options);
			this.editing = false;
			
			// Create a convenience view model as we are drawing information from two separate models
			this.model = new Backbone.Model();
			updateModel();
			
			// Update the views model when necessary
			app.model.bind('change:' + ModelName.SCHOOLPROFILE, updateModel, this);
			app.model.bind('change:' + ModelName.STUDENTPROFILE, updateModel, this);
			
			// When the view model changes, re-render
			this.model.bind('change', this.render, this);
			
			this.render();
		},
		
		parseAdditionalParams: function(options) {
			if (typeof(options.el) !== "undefined") {
				this.el = options.el;
			}
		},
		
		mapTemplateParams: function () {
			var myTemplateParams = app.model.get(ModelName.STUDENTPROFILE).get('password');
			//alert("myTemplateParams="+myTemplateParams);
			var studentprofile=app.model.get(ModelName.STUDENTPROFILE);
			
			var newPass= "*";
			if(myTemplateParams == null || myTemplateParams =="")
			{
	
	
			}
			else 
			{
				var x = myTemplateParams.length;
				
		
				for(i = 0;i < x-1 ;i++)
				{
				  newPass +="*";
				}
			//	alert
			
			}
			//console.log("studentprofile");
			//console.log(studentprofile);
			studentprofile.encrptpassword=newPass;
			var frontPageParams = {
				//'school' : _.extend({}, this.model.get('school')),
				//'student' : studentprofile
				'school' : _.extend({}, this.model.get('school')).toJSON(),
				'student' : studentprofile.toJSON()
			};
			
			return frontPageParams;
		},
		
		render: function() {
					var c = app.render('frontPage',this.mapTemplateParams());
					$(this.el).html(c);	
					//console.log("EMAILEXISTS="+emailExits)
					 setTimeout(function(){
		   				if(!emailExits)
						{
							//console.log("inside if");
							$(".studentProfileOverview1").show();
						}
						else
						{
							//console.log("inside else");
							$(".studentProfileOverview1").hide();
						}
		  		var ht1=$(".frontPage.heightContainer").height();
			
				var ht2=ht1*.55;
				var ht3=ht1*.3;
			
				$(".column2of2.studentProfileOverview").height(ht2)
				$(".column2of2.studentProfileOverview1").height(ht3)
		   
		  			 },10);
					
			
				},
		
		switchToEditMode: function(editing) {
			
			// default arguments
			if (arguments.length == 0) {
			
				editing = true;
				currentEditingState = true;

			}
			
			// update internal state
			this.editing = editing;
			var pageParams = {
				'student' : this.mapTemplateParams().student
			};
			
			$('.studentProfileOverview', this.el).html(
				app.render('studentProfileForm' + (editing ? 'Edit' : 'View'), this.mapTemplateParams().student)
			);
			
			$("#studentProfileEditDOB").datepicker({'showOn' : "button",
													'buttonImage' : "assets/frontpage/calendar.png",
													'buttonImageOnly' : true,
													'defaultDate' : new Date(1998, 11, 31),													
													'changeYear' : true,
													'changeMonth' : true,
													'duration' : 0,
													'minDate' : new Date(1950, 0, 1),
													'maxDate' : "-5y",
													'yearRange' : "1950:-5",
													'showOptions': {direction: 'down' },
													'onSelect' : function(dateText, inst) {
														//formatDate Requires a new date object to work
														var selectedDate = $(this).datepicker("getDate");
														populateDate(selectedDate);
														
													}
			  										});
			  										var uId=parseInt($("#errormessege").attr("rel"));
			  if(uId>0)
			  {
			  	$("#errormessege").hide();
			  	
			  }
			  else
			  {
			  }
			  
			
			if (this.model.get('student').get('dateOfBirth')) {
				$("#studentProfileEditDOB").datepicker("setDate", this.model.get('student').get('dateOfBirth').getDate());
				populateDate(this.model.get('student').get('dateOfBirth').getDate());
			}
			
		},
		
		editStudentProfile: function() {
			this.switchToEditMode();
			currentEditingState = true;

			
		},
		editNewStudentProfile: function() {
			this.switchToEditMode2();
			currentEditingState = true;

			
		},
		switchToEditMode2: function(editing) {
			
			// default arguments
			if (arguments.length == 0) {
			
				editing = true;
				currentEditingState = true;

			}
			
			// update internal state
			this.editing = editing;
			var pageParams = {
				'student' : this.mapTemplateParams().student
			};
			$('.studentProfileOverview1', this.el).html(
				app.render('newProfileForm' + (editing ? 'Edit' : 'View'), pageParams)
			);
			
		},
		
		saveEdit : function()
		{
		//alert("saveEdit");
			var studentProfile = new diary.model.StudentProfile(this.model.get('student').attributes),
				result = studentProfile.set({
					'name' : $("#studentProfileEditName").val(),
					'dateOfBirth' : dateOfBirth,
					'address' : new diary.model.Address({
								'street' : $("#studentProfileEditAddress").val(),
								'suburb' : $("#studentProfileEditSuburb").val(),
								'state' : $("#studentProfileEditState").val(),
								'postcode' : $("#studentProfileEditPostcode").val()
							}),
					'telephone' : $("#studentProfileEditTelephone").val(),
					'email' : $("#studentProfileEditEmail").val(),
					'grade' : $("#studentProfileEditYear").val(),
					'teacher' : $("#studentProfileEditTeachers").val(),
					'house' : $("#studentProfileEditHouse").val(),
					'houseCaptain' : $("#studentProfileEditCaptain").val(),
					'houseColours' : $("#studentProfileEditHouseColours").val(),
					'password' : $("#studentProfileEditPassword").val(),
					'lastName' : $("#studentProfileEditLastName").val()
				}, {
					// validation
					error: function (something, error) {
						console.log(error);
					}
				});
				// update
			if (result) {
				app.context.trigger(EventName.UPDATESTUDENTPROFILE, result);
				this.switchToEditMode(false);
				currentEditingState = false;
			}
		},

		cancelEditStudentProfile: function() {
			
			this.switchToEditMode(false);
			currentEditingState = false;

			
		},
		cancelEditNewStudentProfile: function() {
			
			this.switchToEditMode2(false);
			currentEditingState = false;

			
		},
		
		/*Performance fix: Remove view from the DOM*/		
		remove: function () {
			console.log("######### VIEW REMOVE METHOD...");
			Backbone.View.prototype.undelegateEvents.call(this);
			Backbone.View.prototype.remove.call(this);
			this.$el.remove();
			 
		},
				
		
		updateStudentProfile: function() {

			var dateOfBirth = null;

			// compute the selected date of birth if any
			// we will determine if the date of birth was selected by checking the holder if it was
			// populated, it
			if($("#DOB").val())
				dateOfBirth=Rocketboots.date.Day.parseDayKey($("#DOB").val().trim());
			else
			 	dateOfBirth="";
			 	
			//alert("dateOfBirth="+$("#DOB").val());
			var password=$("#studentProfileEditPassword").val().trim();
			var confirm= $("#studentProfileEditConfirmPassword").val().trim();
			
			$("#studentProfileEditConfirmPassword").val($("#studentProfileEditConfirmPassword").val().trim());
			$("#studentProfileEditPassword").val($("#studentProfileEditPassword").val().trim());
			$("#studentProfileEditLastName").val($("#studentProfileEditLastName").val().trim());
			$("#studentProfileEditEmail").val($("#studentProfileEditEmail").val().trim());
			//$("#studentProfileEditTelephone").val($("#studentProfileEditTelephone").val().trim());
			$("#studentProfileEditName").val($("#studentProfileEditName").val().trim());
			
			
			
			
		  if($("#studentProfileEditName").val().trim()=="")
		{
			$("#studentProfileEditPassword").val("");
			 $("#studentProfileEditConfirmPassword").val("");
			alert("Enter your Firstname");
			
			
			return;
		}
		else if($("#studentProfileEditLastName").val().trim()=="")
		{    
		     
			 $("#studentProfileEditPassword").val("");
			 $("#studentProfileEditConfirmPassword").val("");
			alert("Enter your Lastname");
			
			
			return;
			
		}
		
		else if($("#studentProfileEditEmail").val().trim()=="")
		{
			
			$("#studentProfileEditPassword").val("");
			 $("#studentProfileEditConfirmPassword").val("");
		 	alert("Please enter a mail id");
			return;
			
		}
			
			
			
			if(password==confirm&&password!=""&&confirm!="")
			{
						var userinfo={			
						"email": $("#studentProfileEditEmail").val().trim(),
						"firstname": $("#studentProfileEditName").val().trim(),
						"password": $("#studentProfileEditPassword").val().trim(),
						"add1": "",
						"add2": "",
						"state": ""
			
						}
			
					 /***********************************/
					 	var email=$("#studentProfileEditEmail").val().trim();
						var firstname=$("#studentProfileEditName").val().trim();
						var token=app.model.get(ModelName.SYSTEMCONFIGURATION).get('token');
						var pass=$("#studentProfileEditPassword").val().trim();
						//var add1=$("#studentProfileEditAddress").val().trim();
						//var address2=$("#studentProfileEditSuburb").val().trim();
						//var city="";
						//var state=$("#studentProfileEditState").val().trim();
						var add1="";
						var address2="";
						var city="";
						var state="";
						var last=$("#studentProfileEditLastName").val().trim();
	
				var dob="";
				/*var dob=dateOfBirth;
				if(dateOfBirth)
					 dob=ChangeDate($("#DOB").val());*/
	
				var url=SystemSettings.SERVERURL +"/getuserid/?email="+email+"&token="+token+"&firstname="+firstname+"&password="+pass+"&address1="+add1+"&address2="+address2+"&city="+city+"&state="+state+"&lastname="+last+"&dob="+dob;
				//console.log("SHRYTI url="+url);
				$.ajax({
			            'url' : url,
			            'type': 'POST',
			            'data': {           
			            },
			            'dataType' : "json",
			            'success': function(result) {
							var status=result.status;
						//console.log("getuserid result="+JSON.stringify(result));
							//alert("status="+status);
							if(status=="SUCCESS")
							{
					            	var userid=result.userid;
					            	var schoolid=result.schoolid;
					            	//console.log("userid="+userid);
					            	//console.log("firstname="+firstname);
					            	//console.log("last="+last);
					            	//console.log("pass="+pass);
					            	//console.log("dob="+dob);
					            	updateUserProfile(userid,firstname,last,pass,dob)
									getUserIdSuccess = true;
					            	if(userid)
					            	{
					            		emailExits=true;
										saveUserID(userid);
					            		saveEdit();
					
									}
					
									if(schoolid)
									{ app.context.trigger(EventName.DATACONTENT);
					            		saveschoolId(schoolid);
									}
				
							}
							else
							{
								//alert("inside else");
									if(result.userid)
									{
										//alert("1 userdetail");
										saveUserID(result.userid);
					            		saveschoolId(result.schoolid);
					            		emailExits=true;
					            		//alert("here");
					            		//console.log("userid="+result.userid);
					            		//console.log("firstname="+firstname);
					            		//console.log("last="+last);
					            		//console.log("pass="+pass);
					            		//console.log("dob="+dob);
					            		updateUserProfile(result.userid,firstname,last,pass,dob);
					            		saveEdit();
					            		
									}
									else
									{
										alert(result.error);
									}
										getUserIdSuccess= false;
							}
            		
            	
			            }, 
			            'error': function(response) {
			                console.log("getuserid TheCloud.error: ", JSON.stringify(response));
			            }
			        });
		 
			
				
		}
		else
		{
					if(password=="")
					{
						alert("Please enter Password");
					}
					else if(confirm == "")
					{
						alert("Please enter Confirm Password");
					}
					else
					{
						alert("Password Mismatch");
					}
			
						$("#studentProfileEditPassword").val("");
						 $("#studentProfileEditConfirmPassword").val("");
		}
			
function saveEdit()
{
		//alert("saveEdit");
			var studentProfile = new diary.model.StudentProfile(app.model.get(ModelName.STUDENTPROFILE).attributes),
				result = studentProfile.set({
					'name' : $("#studentProfileEditName").val(),
					'dateOfBirth' : "",
					'address' : new diary.model.Address({
								'street' : "",
								'suburb' : "",
								'state' : "",
								'postcode' : ""
							}),
					'telephone' : "",
					'email' : $("#studentProfileEditEmail").val(),
					'grade' : "",
					'teacher' : "",
					'house' : "",
					'houseCaptain' : "",
					'houseColours' : "",
					'password' : $("#studentProfileEditPassword").val(),
					'lastName' : $("#studentProfileEditLastName").val()
				}, {
					// validation
					error: function (something, error) {
						console.log(error);
					}
				});
				// update
			if (result) {
				app.context.trigger(EventName.UPDATESTUDENTPROFILE, result);
				this.switchToEditMode(false);
				currentEditingState = false;
			}
}			
			
},
updateNewStudentProfile: function() {


			console.log("updateNewStudentProfile");
			 	
			var password=$("#studentProfileEditPassword1").val().trim();
			
			$("#studentProfileEditPassword").val($("#studentProfileEditPassword1").val().trim());
			$("#studentProfileEditEmail").val($("#studentProfileEditEmail1").val().trim());
			
			
			
			
		 
		
	    if($("#studentProfileEditEmail1").val().trim()=="")
		{
			
			$("#studentProfileEditPassword1").val("");
			 $("#studentProfileEditConfirmPassword1").val("");
		 	alert("Please enter a mail id");
			return;
			
		}
			
			
			
			if(password)
			{
						
			
					 /***********************************/
					 	var email=$("#studentProfileEditEmail1").val().trim();
						var token=app.model.get(ModelName.SYSTEMCONFIGURATION).get('token');
						var pass=$("#studentProfileEditPassword1").val().trim();
						var add1="";
						var address2="";
						var city="";
						var state="";
						var last="";
						var dob="";
						var firstname="";
	
	
				var url=SystemSettings.SERVERURL +"/getuserid/?email="+email+"&token="+token+"&firstname="+firstname+"&password="+pass+"&address1="+add1+"&address2="+address2+"&city="+city+"&state="+state+"&lastname="+last+"&dob="+dob;
				//console.log("url="+url);
				$.ajax({
			            'url' : url,
			            'type': 'POST',
			            'data': {           
			            },
			            'dataType' : "json",
			            'success': function(result) {
							var status=result.status;
			//				console.log("getuserid result="+JSON.stringify(getuserid));
							//alert("status="+status);
							if(status=="SUCCESS")
							{
					            	var userid=result.userid;
					            	var schoolid=result.schoolid;
					            	updateUserProfile(userid,firstname,last,pass,dob)
									getUserIdSuccess = true;
					            	if(userid)
					            	{
					            		emailExits=true;
										saveUserID(userid);
					            		NewsaveEdit();
					
									}
					
									if(schoolid)
									{ app.context.trigger(EventName.DATACONTENT);
					            		saveschoolId(schoolid);
									}
				
							}
							else
							{
									if(result.userid)
									{
										saveUserID(result.userid);
					            		saveschoolId(result.schoolid);
					            		emailExits=true;
					            		NewsaveEdit();
					            		updateUserProfile(result.userid,firstname,last,pass,dob);
									}
									else
									{
										alert(result.error);
									}
										getUserIdSuccess= false;
							}
            		
            	
			            }, 
			            'error': function(response) {
			                console.log("getuserid TheCloud.error: ", JSON.stringify(response));
			            }
			        });
		 
			
				
		}
		else
		{
					if(password=="")
					{
						alert("Please enter Password");
					}
					$("#studentProfileEditPassword").val("");
		}
			
function NewsaveEdit()
{
		//alert("saveEdit");
			var studentProfile = new diary.model.StudentProfile(app.model.get(ModelName.STUDENTPROFILE).attributes),
				result = studentProfile.set({
					'name' : "",
					'dateOfBirth' : "",
					'address' : new diary.model.Address({
								'street' : "",
								'suburb' : "",
								'state' : "",
								'postcode' : ""
							}),
					'telephone' : "",
					'email' : $("#studentProfileEditEmail1").val(),
					'grade' : "",
					'teacher' : "",
					'house' : "",
					'houseCaptain' : "",
					'houseColours' : "",
					'password' : $("#studentProfileEditPassword1").val(),
					'lastName' :""
				}, {
					// validation
					error: function (something, error) {
						console.log(error);
					}
				});
				// update
			if (result) {
				app.context.trigger(EventName.UPDATESTUDENTPROFILE, result);
				this.switchToEditMode2(false);
				currentEditingState = false;
			}
}			
			
}
		
	});
		
	
	return view;

})();