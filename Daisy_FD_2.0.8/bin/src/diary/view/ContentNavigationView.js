diary = diary || {};
diary.view = diary.view || {};

diary.view.ContentNavigationView = (function() {

	// parse the sub items of an article
	function parseSubItems(subItemsKey, articles) {
		
		// holds the list of article navigations
		var navigationMenu = [];
		
		// holds sub item collection
		var subItems = null;
		
		// holds student's profile i.e. user information
		var studentProfile = null;
		
		// holds flag if user is teacher
		var isTeacher = null; 
		
		// holds flag if user is student
		var isStudent = null;
		
		// holds role to filter.
		var roleToFilter = null;
		
		var tempSubItems = null;
		
		var sIcount = 0;
		var sILen = 0;
		
		// if we have a sub items key we will try to get the list of
		// articles that are related to the given key
		if (subItemsKey) {
		
			// build the model from the content mapping model
			var schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);
			
			// get the list of sub items for the given key
			subItems = schoolProfile.get("contentMapping").get("subItems")[subItemsKey];
			
			if (subItems == undefined)
			{
				subItems = getSchoolInfoSubItem();
			}
			
			studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
			isTeacher = studentProfile.get("isTeacher");
			isStudent = studentProfile.get("isStudent");
			
			if (isTeacher != null && isTeacher == 1)
			{
				roleToFilter = ContentSetting.USERROLETEACHER;
			}
			
			if (isStudent != null && isStudent == 1)
			{
				roleToFilter = ContentSetting.USERROLESTUDENT;
			}
			
			tempSubItems = subItems;
			
			sILen = tempSubItems.length;
			
			for (sIcount = 0; sIcount < sILen;)
			{
				if(tempSubItems.at(sIcount).get('role') != null && tempSubItems.at(sIcount).get('role') != roleToFilter)
				{
					tempSubItems.remove(tempSubItems.at(sIcount));
					sILen = tempSubItems.length;
					sIcount = 0;
				}
				else
				{
					sIcount++;
				}
			}
			
			subItems = tempSubItems;
			
			// if the list of sub items for the key was found we will parse them
			// otherwise we will ignore the sub items
			if (subItems) {
				
				// NOTE - A sub item is actually an article so we will
				// treat it as an article
				_.forEach(subItems.toArray(), function(article) {
					// include the articles in the list of all articles
					articles.add(article);
					// process the article to build the navigation
					navigationMenu.push(parseArticle(article, articles));
					
				});
				
			}
			
		}
		
		// return the generated sub item navigation
		return navigationMenu;
	}
	
	// parse the article
	function parseArticle(article, articles) {
		
		// reset the given parameters
		parsedArticle = null;
		
		// if we have an article detail we will try to parse it
		if (article) {
						
			// holds the key for the sub items
			var subItemsKey = article.get('subItems');
			
			var subItems = parseSubItems(subItemsKey, articles);
			
			// we can parse the menu from the given article
			parsedArticle = { 'id' : article.get('url'),
					   		  'title' : article.get('title'),
					   		  'subItems' : subItems,
					   		  'hasSubItems' : subItems.length != 0 ? true : false };
				
		}
		
		// return the parsed article
		return parsedArticle;
		
	}
	
	// parse article
	function parseParentArticle(article) {
		
		var articleDetails = {  'tableOfContents' : null,
								'articles' : new diary.collection.ContentItemCollection(),
								'selectedArticle' : article };
		
		// if we have an article detail we will try to parse it
		if (article) {
						
			// add the article to the list of articles
			//articleDetails.articles.add(article);
			
			// holds the key for the sub items if any
			var subItemsKey = article.get('subItems');
			var subItems = parseSubItems(subItemsKey, articleDetails.articles);
			
			//console.log('subItems in parseParentArticle = ' , JSON.stringify(subItems));
			
			// we can parse the menu from the given article
			articleDetails.tableOfContents = subItems;
//			articleDetails.selectedArticle = article;
		}
		
		return articleDetails;
		
	}
	
	// gets index of the article in the list
	function getArticleIndex(article) {
		
		var articles = this.tabDetails.articles.toArray(),
			articleIndex = -1;
		
		for(var indx = 0 ; indx < articles.length ; indx++) {
			
			// check if the current article is the one we are looking for
			if (articles[indx].get('id') == article.get('id')) {
				articleIndex = indx;
				break;
			}
		}
		
		return articleIndex;
	}
	
	// This function is to fetch school information sub items from database
	function getSchoolInfoSubItem()
	{
		var arrTitleCount = 0;
		var arrTitleLen = 0;
		
		var arrTitle = new Array();
		
		var schoolInfoSubItems = null;
		var strSubItems = '';
		var user_type = "allStudents";
		if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
			user_type = "allTeachers";
		}
		
		
		// Find out holidays in given period
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
		
			// Query to find holidays in given month
			"SELECT Title FROM contentStorage WHERE ContentType = 'SchoolContent' AND " + user_type +" = 1",	
			
			// Parameters
			{
				
			},
			// Success function
			function success(statement)
        	{
				arrTitle = _.map(statement.getResult().data, function(item) {
                    return item.Title;
                }); 
				
				if (arrTitle != null)
				{
					arrTitleLen = arrTitle.length;
				}
				
				for (arrTitleCount = 0; arrTitleCount < arrTitleLen; arrTitleCount++)
				{
					strSubItems = strSubItems + '{';
					
					//strSubItems = strSubItems + '"id": null, ';
					strSubItems = strSubItems + '"id": "app:/content/SchoolInformation/' + arrTitle[arrTitleCount] + '", ';
					strSubItems = strSubItems + '"title": "' + arrTitle[arrTitleCount] + '", ';
					//strSubItems = strSubItems + '"url": null, ';
					strSubItems = strSubItems + '"url": "app:/content/SchoolInformation/' + arrTitle[arrTitleCount] + '", ';
					strSubItems = strSubItems + '"fromDate": null, ';
					strSubItems = strSubItems + '"toDate": null, ';
					strSubItems = strSubItems + '"subItems": null, ';
					strSubItems = strSubItems + '"packageId": "SchoolInformation", ';
					strSubItems = strSubItems + '"article": null, ';
					strSubItems = strSubItems + '"documentId": null, ';
					strSubItems = strSubItems + '"role": null';
					
					strSubItems = strSubItems + '},';
				}
				
				if (strSubItems != null && strSubItems != '')
				{
					strSubItems = strSubItems.substr(0, (strSubItems.length - 1));
				}
				
				strSubItems = "[" + strSubItems + "]";
				
				console.log('strSubItems = ' , strSubItems);
				
				strSubItems = JSON.parse(strSubItems);
				
				schoolInfoSubItems = new Backbone.Collection(strSubItems);
				
				
        	},
			// error
        	function error()
        	{
            
        	}
				
			
		);
		
		return schoolInfoSubItems;
	}
	
	// Cache of created view classes, to save recreating the views each time they are displayed
	var viewInstances = {};
	
	var view = Backbone.View.extend({
		
		events: {
			"click .contentNavigation .contentNavPanel .contentNavTab"		: "togglePanelState",
			"click .contentNavigation .contentNavList li"					: "navigateToArticle",
			"click .contentNavigation .contentNavigationPrevious a"			: "navigatePreviousArticle",
			"click .contentNavigation .contentNavigationNext a"				: "navigateNextArticle",
			"click #externalPDF" 											: "openExternalPDFLink",
			"click #externalYoutube"										: "openExternalYoutubeLink",
		},
		
		initialize: function(options) {
			//console.log('ContentNavigationView.initialize()');
			//alert('options in ContentNavigationView initialize = ' + options)	
			
			this.hasRendered = false;
			this.lastRenderedContentViewName = null;
			
			this.parseAdditionalParams(options);
			this.render();
		},
		
		parseAdditionalParams: function(options) {
			//console.log('ContentNavigationView.parseAdditionalParams viewName=' + options.viewName);
			
			//alert('in parseAdditionalParams')
			
			this.contentViewClass = options.contentViewClass;
			this.viewName = options.viewName;
			
			//alert('this.viewName = ' + this.viewName);
			
			this.articleToShow = options.articleToShow;
			
			//alert('this.articleToShow = ' + this.articleToShow);
			
			this.tabArticleLink = options.article;
			
			//alert('this.tabArticleLink  = ' + JSON.stringify(this.tabArticleLink));
			
			this.tabDetails = parseParentArticle(this.tabArticleLink);
			
			//alert('this.tabDetails  = ' + JSON.stringify(this.tabDetails));
			
			if (typeof(options.mustRender) !== "undefined" && options.mustRender) {
				this.hasRendered = false;
			}
		},
		
		render: function() {
			//console.log('ContentNavigationView.render()');
			
			var articleIndex,	// holds the index of the article that is being rendered
				article;		// holds the article itself
			
			if (this.articleToShow instanceof diary.model.ContentItem) {
				articleIndex = this.getArticleIndex(this.articleToShow);
			} else {
				articleIndex = this.getArticleIndex(this.tabArticleLink); // show the root article for this shortcut group
			}
			
			//To fix issue EZTEST-1728.
			if(this.tabDetails.articles.length > 0){
				articleIndex = 0;
			}
			// get the article for the tab
			article = (articleIndex != -1) ? this.tabDetails.articles.at(articleIndex) : this.tabArticleLink;
			
			//console.log('articleIndex', articleIndex, 'article', JSON.stringify(article));
			
			this.renderTableOfContent();
			this.renderArticle(article);
			
		},
				
		renderTableOfContent: function() {
			
			if (!this.hasRendered) {
				var pageParams = {
					'panelMinimised' : !app.model.get(ModelName.APPLICATIONSTATE).get('tableOfContentsVisible'),
					'tableOfContents' : this.tabDetails.tableOfContents
				};
				
				$(this.el).html(
					app.render('contentNavigation', pageParams)
				);
			
				//this.hasRendered = true;	
				this.lastRenderedContentViewName = null;				
			
			}else {

				//alert("renderTableOfContent: already rendered..");	
			}
		},
		
		renderArticle: function(article) {
			var contentNavigationView = this;
			var view;
			
			// keep track of the selected article
			this.tabDetails.selectedArticle = article;
			
			if (this.lastRenderedContentViewName != this.viewName) {
				view = viewInstances[this.viewName];
				//alert("renderArticle: this.viewName="+this.viewName);
	
				var contentViewParams = {};		
				contentViewParams.el = $('.contentNavigationPanel', this.el);
				contentViewParams.tabDetails = this.tabDetails;
				contentViewParams.name=passname;
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.VIEW_CONTENT_PAGE, article);
							
				if (typeof(view) === "undefined") {
					viewInstances[this.viewName] = new this.contentViewClass(contentViewParams);
				} else {
					if (typeof(view.parseAdditionalParams) != "undefined") {
						view.parseAdditionalParams(contentViewParams);
					}
	
					view.render();
					this.lastRenderedContentViewClass = this.viewName;
					//view.contentViewRendered = true;
				}
				
				
			}else
			{
				view = viewInstances[this.viewName];
				//alert("renderArticle: already rendered..");
				view.remove();
			}
			
			// update the table of contents
			_.defer(function() {
				contentNavigationView.updateTableOfContents(article);				
			});
			
		},
		
		updateTableOfContents: function(article) {
			// go through each one of the table of content elements
			// in the list and un select them.  If the link is pointing to the
			// article that we are currently viewing we will mark it as selected
			
			var navItems = $('.contentNavigation .contentNavList li', this.el); 
			
			navItems.each(function() {
				var $this = $(this);
				
				$this.removeClass('selected');
				
				if ($this.attr('articleId') == article.get('id')) {
					//console.log($this.attr('articleId'), '=', article.get('id'));
					$this.addClass('selected');
				}

				//console.log(this.outerHTML);
			});			
		},
		
		togglePanelState: function(event) {
		
			if(emailExits)
			{
			var contentPanel = $(event.currentTarget).parent(),
				panelMinimised;
			
			if (contentPanel.hasClass("panelMinimised")) {
				// Show
				contentPanel.removeClass("panelMinimised");
				panelMinimised = false;
			}
			else {
				// Hide
				contentPanel.addClass("panelMinimised");
				panelMinimised = true;
			}
			
			// Store the updated state back to the model
			app.model.get(ModelName.APPLICATIONSTATE).set({ 'tableOfContentsVisible' : !panelMinimised });
			}
		
		},
		
		navigateToArticle: function(event) {
   			var $link = $(event.currentTarget),
    		articleId = $link.attr('articleId'),
    		article;
    
    		passname=$(event.currentTarget).text();
    
		   // get the article from the current list of sub items
		   // for the main article
		   article = this.tabDetails.articles.get(articleId);
		   this.renderArticle(article);
		   
  },
		
		navigatePreviousArticle: function(event) {
			passname=$(event.currentTarget).text().replace("Prev: ","")
			if(emailExits)
			{
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CONTENT_PREVIOUS);
				this.moveArticle(-1);
			}
		},
		
		navigateNextArticle: function(event) {
			passname=$(event.currentTarget).text().replace("Next: ","")
			if(emailExits)
			{
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CONTENT_NEXT);
				this.moveArticle(+1);
			}
		},
		
		moveArticle: function(moveBy) {
			// get the list of articles
			var articles = this.tabDetails.articles.toArray(),
				selectedArticleIndx;
			
			// determine which article we need to move to
			if (this.tabDetails.selectedArticle) {
				selectedArticleIndx = this.getArticleIndex(this.tabDetails.selectedArticle) + moveBy;
			} else {
				selectedArticleIndx = 0;
			}
			
			// if the selected article is not the first one in the list or the last one
			// we will move to the next article
			if (selectedArticleIndx >= 0 && selectedArticleIndx <= articles.length -1) {
				
				// get the article that we need to render
				var article = articles[selectedArticleIndx];
				// render the selected article
				this.renderArticle(article);
				
			}
			
		},
		
		getArticleIndex: function(article) {
			
			var articles = this.tabDetails.articles.toArray(),
				articleIndex = -1;
			
			for(var indx = 0 ; indx < articles.length ; indx++) {
				
				// check if the current article is the one we are looking for
				if (articles[indx].get('id') == article.get('id')) {
					articleIndex = indx;
					break;
				}
			}
			
			return articleIndex;
		},
		openExternalPDFLink : function()
		{
			//Prime
//			var link = 'https://prime-planner.com/AboutPrime/StudentGuide.pdf';
//			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
//				link = 'https://prime-planner.com/AboutPrime/TeacherGuide.pdf';	
//			}
			
			//Daisy
			var link = 'https://e-planner.com.au/AboutDaisy/Daisy-Student-Manual-2015.pdf';
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				link = 'https://e-planner.com.au/AboutDaisy/Daisy-Teacher-Manual-2015.pdf';	
			}
			
			//Honeycomb
			//var link = 'http://digiplanner.co.uk/AboutHoneycomb/e-book-honeycomb-5-a.pdf';
			
			var urlReq = new air.URLRequest(link); 
			air.navigateToURL(urlReq);
		},
		openExternalYoutubeLink : function()
		{
			//Prime
			//var link = 'https://www.youtube.com/user/PRIMEWebisodes';
			//Honeycomb
			//var link = 'http://youtu.be/afJX3dV0AwY';
			
			//Daisy
			var link = 'https://www.youtube.com/watch?v=qdwdI497E3w';
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				link = 'https://www.youtube.com/watch?v=Sg2QQk_gF6I';
			}
			
			var urlReq = new air.URLRequest(link); 
			air.navigateToURL(urlReq);
		}
		
	});
	
	return view;

})();