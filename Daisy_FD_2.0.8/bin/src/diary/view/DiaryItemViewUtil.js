/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: DiaryItemViewUtil.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.view = diary.view || {};

/**
 * A collection of utility methods to be used by the diary item view
 * created by Jane Sivieng
 * @type {Object}
 */
diary.view.DiaryItemViewUtil = {

	'hideObjects': function($objects, objectsToHide, fade) {
		var hideObject = function(object) {
//			if (fade) {
//				row.fadeOut(SystemSettings.FADEANIMATIONDURATION);
//			} else {
				object.hide();
//			}
		};

		for (var i = 0; i < objectsToHide.length; i++) {
			var key = objectsToHide[i];
            $objects[key] && hideObject($objects[key]);
		}
	},

	'showObjects': function($objects, objectsToShow, fade) {
		var showObject = function(object) {
//			if (fade) {
//				row.fadeIn(SystemSettings.FADEANIMATIONDURATION);
//			} else {
                object.show();
//			}
		};

		for (var i = 0; i < objectsToShow.length; i++) {
			var key = objectsToShow[i];
            $objects[key] && showObject($objects[key]);
		}
	},

	// TODO: handle new date / time fields
	'updateVisibleRows' : function($rows, $inputs, diaryItemType, fade) {
		var rowsToHide = [],
			rowsToShow = [],
            inputsToHide = [],
            inputsToShow = [],
			setTimeInputs = function() {
				if ($inputs.allDay.is(":checked")) {
                    inputsToHide.push('startTime');
                    inputsToHide.push('endTimeContainer');
				} else {
                    inputsToShow.push('startTime');
                    // if the 'All Day' is checked and the type of diary item
                    // being manipulated is an event we will show the end time
                    // also otherwise leave it hidden as it is not required
					(diaryItemType == 'event' ? inputsToShow : inputsToHide).push('endTimeContainer');

                }
			};

		if (diaryItemType == 'task' || diaryItemType == "assignment" || diaryItemType == "homework") {
			rowsToShow = ['descriptionTable','date', 'assignedDate', 'webLink', 'endTimeContainer', 'description', 'subject', 'attachments', 'completed','priorityRow','title','rowAssignToStudent',"typeRow"];
			rowsToHide = ['noteEntry', 'messageFromText', 'responseRequired','studentsRow','commentsRow','meritsRow','assignedByRow','assignedDayRow','taskTitleRow','isNotifyRow'];
            setTimeInputs();
		} else if (diaryItemType == 'event') {
			rowsToShow = ['date', 'noteEntry','attachments','title','rowAssignToStudent',"typeRow"];
			rowsToHide = ['descriptionTable','input_tag','assignedDate', 'webLink', 'noteEntry', 'subject', 'completed', 'messageFromText', 'description', 'responseRequired','priorityRow','studentsRow','commentsRow','meritsRow','assignedByRow','assignedDayRow','taskTitleRow','isNotifyRow'];
            setTimeInputs();
		} else if (diaryItemType == 'merit') {
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				rowsToShow = ['subject','studentsRow','commentsRow','meritsRow',"typeRow"];
				rowsToHide = ['descriptionTable','input_tag','assignedDate', 'webLink', 'noteEntry', 'date', 'completed', 'messageFromText', 'description', 'responseRequired','priorityRow','title','rowAssignToStudent','assignedByRow','assignedDayRow','taskTitleRow'];
				
				if(app.model.get(ModelName.SCHOOLPROFILE).get("allowParents") == 1){
					rowsToShow.push('isNotifyRow');
				} else {
					rowsToHide.push('isNotifyRow');
				}
				
			} else {
				rowsToShow = ['subject','commentsRow','meritsRow','assignedByRow','assignedDayRow',"typeRow"];
				rowsToHide = ['descriptionTable','input_tag','assignedDate', 'webLink', 'noteEntry', 'date', 'completed', 'messageFromText', 'description', 'responseRequired','priorityRow','title','rowAssignToStudent','isNotifyRow','studentsRow'];
			}
			
            setTimeInputs();
		} else if (diaryItemType == 'messageFromTeacher' || diaryItemType == 'messageFromParent') {
			rowsToShow = ['description', 'messageFromText', 'responseRequired',"typeRow"];
			rowsToHide = ['date', 'startTime', 'assignedDate', 'endTimeContainer', 'noteEntry', 'subject', 'completed', 'attachments'];
		} else if (diaryItemType == 'note') {
			rowsToShow = ['noteEntry','title','rowAssignToStudent',"typeRow"];
			rowsToHide = ['descriptionTable','input_tag','date', 'startTime', 'assignedDate', 'webLink', 'endTimeContainer', 'description', 'subject', 'attachments', 'completed', 'messageFromText', 'responseRequired','priorityRow','studentsRow','commentsRow','meritsRow','assignedByRow','assignedDayRow','taskTitleRow','isNotifyRow'];
		}

		diary.view.DiaryItemViewUtil.hideObjects($rows, rowsToHide, fade);
		diary.view.DiaryItemViewUtil.showObjects($rows, rowsToShow, fade);
        diary.view.DiaryItemViewUtil.hideObjects($inputs, inputsToHide, fade);
        diary.view.DiaryItemViewUtil.showObjects($inputs, inputsToShow, fade);
	},

	// TODO: handle new date / time fields
	'setTime': function($inputs, dateTime, field) {
		var $time;

		if (field == 'startTime' || field == "endTime" || "assignedTime") {
            $time = $inputs[field];
		} else {
			return;
		}

		dateTime = dateTime || Rocketboots.date.Time(0, 0);
		if(dateTime)
        	$time.timepicker('setTime', dateTime.toDate());
	},

	// TODO: handle new date / time fields
	'getTime': function($inputs, field) {
		if (field == 'startTime' || field == "endTime" || "assignedTime") {
            var $time = $inputs[field];
			return Rocketboots.date.Time.parseTime($time.timepicker('getTime'));
		}
		console.log('ERROR: diary.view.DiaryItemViewUtil::getDateTime() called with an invalid field');
		return new Rocketboots.date.Time(0, 0);
	},

	'getTimeAttributes': function($inputs, oldDiaryItem) {
		// TODO: handle new date / time fields
		var startTime = diary.view.DiaryItemViewUtil.getTime($inputs, 'startTime'),
			endTime = diary.view.DiaryItemViewUtil.getTime($inputs, 'endTime'),
            assignedTime = diary.view.DiaryItemViewUtil.getTime($inputs, 'assignedTime'),
			result = function(start, end, assigned) {
				return {
					'startTime' : start,
					'endTime': end,
                    'assignedTime': assigned
				};
			};

		if ($inputs.allDay.is(":checked")) {
			return result(null, null, assignedTime);
		} else {
			return result(startTime, endTime, assignedTime);
		}
	},

	// TODO: handle new date / time fields
	'getAttributesForSave' : function($inputs, diaryItem, calanderDay, periodStartTime, assignTo) {
		var userId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
		var attrs = {};

		attrs.title = $inputs.title.val();

		var timeAttrs = diary.view.DiaryItemViewUtil.getTimeAttributes($inputs, diaryItem);

		// add extra attributes specific to tasks
		if (diaryItem instanceof diary.model.Task) {

			attrs.type = $inputs.type.val().toUpperCase();
			
			var selectedType = $(".type").find(':selected');
			attrs.dairyItemTypeId = selectedType.attr('type_id');
			attrs.localDairyItemTypeId = selectedType.attr('localtype_id');
			attrs.dairyItemTypeName = selectedType.attr('type_name').trim();
			
			attrs.priority = $inputs.priority.val();
			attrs.estimatedHours = $inputs.estimatedHours.val();
			attrs.estimatedMinutes = $inputs.estimatedMinutes.val();
			attrs.webLinkDesc = $inputs.webLinkDesc.val();
			attrs.progress = $inputs.percentage.val().replace("%","");
            attrs.description = $inputs.description.val();
            attrs.weight = $inputs.weight.val();
            //console.log("all day checked : "+$inputs.allDay.is(":checked"));
            attrs.onTheDay = $inputs.allDay.is(":checked");
            
            //alert('attrs.onTheDay : ' + attrs.onTheDay);
            if(diaryItem.get("createdBy") != null){
            	attrs.createdBy =  diaryItem.get("createdBy");
            }
            else{
            	attrs.createdBy = userId;
            }
            attrs.assignedDay = new Rocketboots.date.Day(new Date($inputs.assignedDate.val()));
            attrs.assignedTime = timeAttrs.assignedTime;

            attrs.dueDay = new Rocketboots.date.Day(new Date($inputs.date.val()));
            attrs.dueTime = timeAttrs.startTime;
           
            //edit item
            
            //for teacher
            	if($inputs.assignedToMe.length){
            		 var assignedtoMe = $inputs.assignedToMe.is(":checked");
            		 console.log("assignedtoStudents : "+assignedtoMe);
            		 attrs.assignedtoMe = assignedtoMe;
            	}
            	else{ // for student check createdBy
            		if((diaryItem.get("createdBy") != null && userId != diaryItem.get("createdBy"))){
            			 attrs.assignedtoMe = 0;
            		}
            		else{
            			attrs.assignedtoMe = 1;
            		}
            	}
            console.log(" attrs.assignedtoMe : "+ attrs.assignedtoMe);
            attrs.assignTo = assignTo;

            // set subject
            console.log("app.model.get(ModelName.SUBJECTS) : "+JSON.stringify(app.model.get(ModelName.SUBJECTS)));
            console.log("app.model.get(ModelName.CLASSES) : "+JSON.stringify(app.model.get(ModelName.CLASSES)));
            //get class object by uniqueId 
            var selectdClass = getObjectByValue(app.model.get(ModelName.CLASSES), $inputs.subject.val(), 'uniqueId');
            console.log("selectdClass.subjectId"+selectdClass.get("subjectId"));
            
			attrs.subject = app.model.get(ModelName.SUBJECTS).get(selectdClass.get("subjectId"));
			attrs.classId = $inputs.subject.val();
			
			
			// if completed, save the current time as the completed time
			var previouslyCompleted = diaryItem.isCompleted();
			var currentlyCompleted = $inputs.completed.is(":checked");

			if (previouslyCompleted && !currentlyCompleted) {
				attrs.completed = null; // marking the task as NOT completed now
			} else if (!previouslyCompleted && currentlyCompleted) {
				// mark the task as newly completed;
				attrs.completed = new Date();
			} // else they are the same so don't change anything

        } else if (diaryItem instanceof diary.model.Merit) {

			attrs.type = $inputs.type.val().toUpperCase();
			
			var selectedType = $(".type").find(':selected');
			attrs.dairyItemTypeId = selectedType.attr('type_id');
			attrs.localDairyItemTypeId = selectedType.attr('localtype_id');
			attrs.dairyItemTypeName = selectedType.attr('type_name').trim();
			attrs.estimatedHours = 0;
			attrs.estimatedMinutes = 0;
			attrs.webLinkDesc = "";
			attrs.weight = 0;
			
			var currentMerit = $(".bjqs li.active");
			
			attrs.comment = $inputs.comments.val();
			attrs.merit_type = currentMerit.attr("type");
			attrs.passtype_name = currentMerit.attr("desc");
			attrs.passtype_id = currentMerit.attr("uniqueid");
			attrs.isnotify = $(".isNotifyRow .isNotify").is(":checked") == true ? 1 : 0; //0;			
			//alert('isNotifyRow : ' + $(".isNotifyRow .isNotify").is(":checked"));
            attrs.title = currentMerit.attr("desc");
			attrs.description = $inputs.comments.val();
            
            //alert('attrs.onTheDay : ' + attrs.onTheDay);
            if(diaryItem.get("createdBy") != null){
            	attrs.createdBy =  diaryItem.get("createdBy");
            } else{
            	attrs.createdBy = userId;
            }
           
			if(diaryItem && diaryItem.get("uniqueId") == null){
				//Fetching System date and time as current always.
				var currentTime = Rocketboots.date.Time.parseTime(new Date()), currentDate = new Rocketboots.date.Day(new Date());
				
				attrs.assignedDay = currentDate;
				attrs.assignedTime = currentTime;
				attrs.dueDay = currentDate;
				attrs.dueTime = currentTime;
            } else if(diaryItem && diaryItem.get("uniqueId") != null){
				attrs.assignedDay = diaryItem.get("assignedDay");
				attrs.assignedTime = diaryItem.get("assignedTime");
				attrs.dueDay = diaryItem.get("dueDay");
				attrs.dueTime = diaryItem.get("dueTime");
			}
			
            var assignTo = [];
			
			var arrUserId = $(".toList").val().split(",");
			for(var len = 0; len < arrUserId.length; len++){
				var selectedStudent = {
		    			"id" : arrUserId[len]
				};
				assignTo.push(selectedStudent);
			}
			//alert('attrs.assignTo : ' + JSON.stringify(assignTo));
            attrs.assignTo = assignTo;

            var selectdClass = getObjectByValue(app.model.get(ModelName.CLASSES), $inputs.subject.val(), 'uniqueId');
			attrs.subject = app.model.get(ModelName.SUBJECTS).get(selectdClass.get("subjectId"));
			attrs.classId = $inputs.subject.val();
			attrs.assignedtoMe = 0;
			
			
        } else if (diaryItem instanceof diary.model.Event) {
			attrs.createdBy = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
            attrs.startDay = new Rocketboots.date.Day(new Date($inputs.date.val()));
            attrs.startTime = timeAttrs.startTime;
            attrs.endTime = timeAttrs.endTime;
            attrs.description = $inputs.noteEntry.val();
            //console.log("$inputs.allDay.is(':checked') : "+$inputs.allDay.is(":checked"));
            attrs.onTheDay = $inputs.allDay.is(":checked");
            
            var selectedType = $(".type").find(':selected');
			attrs.dairyItemTypeId = selectedType.attr('type_id');
			attrs.localDairyItemTypeId = selectedType.attr('localtype_id');
			attrs.dairyItemTypeName = selectedType.attr('type_name').trim();
			
            if($inputs.assignedToMe.length){
            		 var assignedtoMe = $inputs.assignedToMe.is(":checked");
            		 console.log("assignedtoStudents : "+assignedtoMe);
            		  if(!assignedtoMe){
            		 	attrs.classId = $(".classes_tbl_lt .selected .classId").val();
            		 }
            		// console.log("check class of event : "+$(".classes_tbl_lt .selected .classId").val());
            		 attrs.assignedtoMe = assignedtoMe;
            	}
            	else{ // for student check createdBy
            		if((diaryItem.get("createdBy") != null && userId != diaryItem.get("createdBy"))){
            			 attrs.assignedtoMe = 0;
            		}
            		else{
            			attrs.assignedtoMe = 1;
            		}
            	}

        } else if (diaryItem instanceof diary.model.Message) {
			// we are creating a new message, messages cannot be edited
            attrs.atDay = ((isSpecified(calanderDay)) ? calanderDay : new Rocketboots.date.Day(new Date()));
            attrs.atTime = isSpecified(periodStartTime) ? periodStartTime : null;
            attrs.description = $inputs.description.val();
			attrs.messageFromText = $inputs.messageFromText.val();
			attrs.responseRequired = $inputs.responseRequired.is(":checked");
        } else if (diaryItem instanceof diary.model.Note){
        
        	var selectedType = $(".type").find(':selected');
			attrs.dairyItemTypeId = selectedType.attr('type_id');
			attrs.localDairyItemTypeId = selectedType.attr('localtype_id');
			attrs.dairyItemTypeName = selectedType.attr('type_name').trim();
			
            attrs.description = $inputs.noteEntry.val()
			//alert('periodStartTime note' + periodStartTime);
			var currentTime = Rocketboots.date.Time.parseTime(new Date());
            
            //To fix issue EZTEST-1206.
            var isNoteOnDay = $inputs.allDay.is(":checked");
            if(isNoteOnDay){
            	currentTime = null;
            }
            
			// we are creating a new note, notes cannot be edited
            attrs.atDay = ((isSpecified(calanderDay)) ? calanderDay : new Rocketboots.date.Day(new Date()));
            attrs.atTime = (isSpecified(periodStartTime) && periodStartTime != "") ? periodStartTime : currentTime;
            attrs.entries = new diary.collection.NoteEntryCollection();
			attrs.entries.add(new diary.model.NoteEntry({'timestamp': new Date(), 'text': $inputs.noteEntry.val() }));
			   if($inputs.assignedToMe.length){
            		 var assignedtoMe = $inputs.assignedToMe.is(":checked");
            		 //console.log("assignedtoStudents : "+assignedtoMe);
            		  if(!assignedtoMe){
            		 	attrs.classId = $(".classes_tbl_lt .selected .classId").val();
            		 }
            		  //console.log("classId : "+$(".classes_tbl_lt .selected .classId").val());
            		 attrs.assignedtoMe = assignedtoMe;
            	}
            	else{ // for student check createdBy
            		if((diaryItem.get("createdBy") != null && userId != diaryItem.get("createdBy"))){
            			 attrs.assignedtoMe = 0;
            		}
            		else{
            			attrs.assignedtoMe = 1;
            		}
            	}
		}

		return attrs;
	}

};
