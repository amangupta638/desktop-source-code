diary = diary || {};
diary.view = diary.view || {};
var diaryItemForm="", oncecall=true, updateHomeView, newsCount;
diary.view.HomeView = (function() {
	var homeView = Rocketboots.view.View.extend({
		notifications: [],
		
		events: {
			"click .btn_hide"		    								: "hideOverDueItems", 		// hide overdue items
			"click .tabs_container .tabMenu_btn ul li.glance"			: "glanceView",
			"click .tabs_container .tabMenu_btn ul li.announcements"	: "newsAnnouncementView",
			//"click .tasksContainer .itemContainer .diaryItem"			: "showTasksInPopup",		// pop-up for diary items
			//"click .eventsContainer .itemContainer .event"			: "showEventInPopup",
			"click .calender_profile div table img"					    : "removeOverdueItem",
			"click .newsAnnouncementsContent .jspPaneHome .phwtcg_ctxt_div"	: "openInboxView",
			"click .nameStrip_student .icon_photo img.img_photo"		: "redirectToProfile",
			"click .markComplete .completed"							: "itemMarkCompleted",
			"click #editDiaryItemBtn img.assignment"					: "showDiaryItemForm",
			"click #editDiaryItemBtn img.homework"						: "showDiaryItemForm",
			"click #editDiaryItemBtn img.merit"							: "showDiaryItemForm",
			"click #editDiaryItemBtn img.note"							: "showTasksInPopup",
			"click #editEventBtn img"									: "showEventInPopup",
			"click .bool-slider .inset"									: "switchGlanceView",
			"click .phwtcg_ctxt_div .studMeritIcon"						: "showDiaryItemForm",
			
			
		},

		initialize: function(options) {
			
			//app.context.on(EventName.NEWNOTIFICATION, this.receiveNotification, this);
			//app.context.trigger(EventName.FETCHNEWSANNOUNCEMENT);
			//app.context.trigger(EventName.FETCHINBOX);
			//this.triggerFetchDiaryItems();
			//fetchDairyItems();
			//alert('before hideReports');
			this.hideReports();
			console.log('after hideReports');
        	var studentProfile = app.model.get(ModelName.STUDENTPROFILE);
			var isTeacher = app.model.get(ModelName.STUDENTPROFILE).get('isTeacher');
			if(!isTeacher){
				this.hideMyClasses();
				//getClassUsersLocal();
				//getClassForTeacher(studentProfile.get("UniqueID"));
			}
			//else {
				//getAllClasses();
			//}
			var view = this;
			this.render();
			
			$(".tempClass").load("http://54.252.237.186:8080/pentaho/content/pentaho-cdf-dd/Render?solution=PD_Report&path=&file=Students_WorkLoad_Distribution.wcdf&TeacherID=24211");	
		},	
		
		triggerFetchDiaryItems: function() {
			var year = SystemSettings.HARDCODDEDYEAR;
	        var startDate = new Rocketboots.date.Day(year, 0, 1);
	        var endDate = new Rocketboots.date.Day(year, 11, 31);
	        
			app.context.trigger(EventName.FETCHDIARYITEMS, startDate, endDate, false, false, false);
		},

		render: function() {
			
			var profileImage = null;
			var profileImageToDisplay = null;
			
			updateHomeView = function() {
					console.log("updateHomeView : render homeView");
					view.isDirty = true
					view.render();
			};
			app.model.get(ModelName.DIARYITEMS).off("change");
			app.model.get(ModelName.DIARYITEMS).off("remove");
			app.model.get(ModelName.DIARYITEMS).off("add");
			app.model.get(ModelName.DIARYITEMS).off("reset");
			app.model.get(ModelName.DIARYITEMS).on("change", updateHomeView, this);
			app.model.get(ModelName.DIARYITEMS).on("remove", updateHomeView, this);
			app.model.get(ModelName.DIARYITEMS).on("add", updateHomeView, this);
			app.model.get(ModelName.DIARYITEMS).on("reset", updateHomeView, this);
			
			var schoolImageParam = {
				"schoolImage": app.model.get(ModelName.SCHOOLPROFILE).get('schoolImage')
			}
			var c = app.render('homeView', schoolImageParam);
			$(this.el).html(c);	
		
			var student = app.model.get(ModelName.STUDENTPROFILE);
			
			profileImage = student.get('profileImage');
			
			if (profileImage != null && profileImage != '')
			{
				profileImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + profileImage;
			}
			else
			{
				profileImageToDisplay = 'assets/home/images/icon_profile.png';
			}
			
			student.set('profileImageToDisplay', profileImageToDisplay);
			
			$('#userProfileContainer', this.el).html(app.render('userProfileView',student.toJSON()));
			var view = this;
			
			//To fix issue : EZTEST-842.
			//view.renderOverDueItems();
			$('#subTabsHomeView', this.el).html(app.render('tabContainerHomeView', {}));
			var notifications = app.model.get(ModelName.INBOXITEMS).getUnreadCount();
			if(notifications == 0){
				notifications = newsCount;
			}
			
			$(".notification_num", this.el).html(notifications);
			view.glanceView(event,true);
			student = null, c = null, schoolImageParam = null, notifications = null;
			
			//$(".tempClass").load("http://54.252.237.186:8080/pentaho/content/pentaho-cdf-dd/Render?solution=PD_Report&path=&file=Students_WorkLoad_Distribution.wcdf&TeacherID=24211");	
		},
		hideMyClasses : function(){
			
			$(".myClasses_li").hide();
		},
		hideReports : function(){
			
			var reports = app.model.get(ModelName.REPORTS).isReportTabActive();
			var showReports = reports && reports.length > 0;
			if(!showReports){
				$(".report_li").hide();
			}
			
			reports = null, showReports = null;
		},
		hideOverDueItems: function(){
			$(".nameStrip_hide").hide();
			$(".calender_profile").hide();
			$(".mainShadowBottom").hide();
		},
		
		renderOverDueItems: function(){
			var overDueTasks = app.model.get(ModelName.DIARYITEMS).getRequiredOverdueTasks();
			var diaryItemOverDue = [];
			
			for(var i = 0 ; i < overDueTasks.length; i++){
				var task = overDueTasks.at(i);
				diaryItemOverDue.push(diary.view.DiaryCalendarViewUtil.mapDiaryItem(task));
			}
			var overDueTaskList = {
				"overDueTasks":diaryItemOverDue
			}
			var overDueFirstTask  = {
				"overDueTasks":overDueTasks.at(0) ? diary.view.DiaryCalendarViewUtil.mapDiaryItem(overDueTasks.at(0)) : null 
			}
			
			$('#overdueItem', this.el).html(app.render('overdueItemHomeView',overDueFirstTask));
			$('#overdueItemList', this.el).html(app.render('overdueItemListHomeView',overDueTaskList));
			
			overDueTasks = null, diaryItemOverDue = null, overDueTaskList = null, overDueFirstTask = null;
		},
		
		removeOverdueItem: function(event){

			var id = $(event.currentTarget).attr('id');
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "UPDATE diaryItem SET hideFromDue = 1  WHERE id = :diaryItemId",
	
	            // params
	            {
				'diaryItemId' :id
	            },
				// success
				function(statement) {
				
				},
				
				// error
				function error()
				{
					
				}
			);
			
		    /* var year = SystemSettings.HARDCODDEDYEAR;
	        var startDate = new Rocketboots.date.Day(year, 0, 1);
	        var endDate = new Rocketboots.date.Day(year, 11, 31);
	        
			app.context.trigger(EventName.FETCHDIARYITEMS, startDate, endDate, false, false, false); */
			fetchDairyItems();
			
            var view = this;
            
            var overDueTasks = app.model.get(ModelName.DIARYITEMS).getRequiredOverdueTasks();
			var assignedTasks = app.model.get(ModelName.DIARYITEMS).getRequiredAssignedTasks();
			$("#dueCount").html(overDueTasks.length);
			$("#assignedCount").html(assignedTasks.length);
			
			view.renderOverDueItems();
		},
		
		glanceView : function(event,isTabSelected){
			  if($('.tabs_container .tabMenu_btn ul li.glance').hasClass("active")){
			  	if(!isTabSelected){
			  		return;
			  	}
			  }
			  
			  $('.tabs_container .tabMenu_btn ul li.announcements').removeClass("active");
			  $('.tabs_container .tabMenu_btn ul li.glance').addClass("active");
			  $('.newsAnnouncementsContent').hide();
			  
			  //var calendarModel = app.model.get(ModelName.CALENDAR),
			  //diaryItemsModel = app.model.get(ModelName.DIARYITEMS),
			  day = null;
			  
			  var today = new Rocketboots.date.Day();
			  app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', today);
			  
			  var isDueSelected = app.model.get(ModelName.APPLICATIONSTATE).get('isDueSelected'); 
			  var glanceData = diary.view.DiaryCalendarViewUtil.mapGlance(true, isDueSelected);
			  //console.log("viewParams glance : "+JSON.stringify(glanceData));
			  var viewParams = { 
				'calendar': glanceData,
				'isDueSection' : isDueSelected
			  };
			  $('#tabsContent', this.el).html(app.render('glanceHomeView', viewParams));
			  this.renderPercentSlider(0);
			  if(isDueSelected){
			  		$("#toggleSwitch", this.el).addClass('true');
			  		$('.title_tabs_overdue', this.el).text('Overdue');
			  		$('.dueToggle', this.el).addClass('showGlanceActive');
			  		$('.assignedToggle', this.el).removeClass('showGlanceActive');
			  } else {
			  		$("#toggleSwitch", this.el).addClass('false');
			  		$('.title_tabs_overdue', this.el).text('Today');
			  		$('.dueToggle', this.el).removeClass('showGlanceActive');
			  		$('.assignedToggle', this.el).addClass('showGlanceActive');
			  }
			  
			  //oncecall=true;
			  if(oncecall || editor == undefined || editor2 == undefined)
			  {
				  	//alert("editorrs initialized");
					oncecall=false;
					$("div[id*='dayDiaryItemFormEditPopup']").parents(".ui-dialog").remove();
						
					viewParams={};
					viewParams.el = $('#dayDiaryItemFormEditPopup', this.el);
					viewParams.editorref="editor1";
					viewParams.classes="dayDiaryItemFormEditPopup";
					diaryItemForm = new diary.view.DiaryItemView(viewParams);
			  } 
			  
			  glanceData = null;
			  viewParams = null;
		},
		
		switchGlanceView : function(event) {
			if (!$(this).parent().parent().hasClass('disabled')) {
	            if ($("#toggleSwitch").hasClass('true')) {
	            	app.model.get(ModelName.APPLICATIONSTATE).set('isDueSelected',false);
	            	this.glanceView(event,true);
	                $("#toggleSwitch").addClass('false').removeClass('true');
	            } else {
	            	app.model.get(ModelName.APPLICATIONSTATE).set('isDueSelected',true);
	            	this.glanceView(event,true);
	                $("#toggleSwitch").addClass('true').removeClass('false');
	            }
	        }
		},
		
		renderPercentSlider : function(sliderVal) {
			var view = this;
			$(".percentSlider", this.el).each(function(){
			    var $this = $(this);
				$(this).slider({
					orientation : "horizontal",
					range : "min",
					max : 100, // max value of the slider
					value : $(this).attr('percent'),
					step : 1,
					animate : "fast",
					min : 0,
					change : function(event, ui) {
						var diaryItemId = $(this).attr('diaryItemId');
						var className = '#percent'+diaryItemId;
						var markComplete = '#markComplete'+diaryItemId;
						
						newVal = ui.value;
						$(className).val(newVal+"%");
						if(newVal == 100){
							$(markComplete).prop('checked', true);
						} else {
							$(markComplete).prop('checked', false);
						}
						view.updateProgress(diaryItemId,newVal);
					}
				});
			});
		},
		
		updateProgress: function(diaryItemId,progressVal) {
			var view = this;
			var diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId);
			diaryItem.set("progress",progressVal);
			if(progressVal == 100 ){
				diaryItem.set("completed",new Date());
			}
			else{
				diaryItem.set("completed",null);
			}
			
			//quick fix for 1053
			 var diaryItemUsers = "";
		     if(diaryItem.get("uniqueId") == null){
		       diaryItemUsers = getAssignedUsers(view.diaryItem.get("id"));
		     }
		     else{
		        diaryItemUsers = getAssignedUsersByUniqueId(diaryItem.get("uniqueId"));
		     }
		      diaryItem.set("assignTo",diaryItemUsers);
		     
		    
			
			//console.log("diaryItem : "+JSON.stringify(diaryItem)+"progressVal : "+(progressVal == 100));
			app.context.trigger(EventName.UPDATEDIARYITEM, diaryItem);
			
			if(progressVal == 100 ){
				view.render();
			}
			
			/*
			var view = this;
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "UPDATE diaryItem SET progress = :progress, completed = :completed, isSync = 0, lastupdated = :lastupdated WHERE id = :diaryItemId",
	
	            // params
	            {
					'diaryItemId' :diaryItemId,
					'progress' :progressVal,
					'completed' : progressVal == 100 ? 1 : 0,
					'lastupdated': getTimeStamp()
	            },
				// success
				function(statement) {
					//var diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId);
					//diaryItem.set("progress",progressVal);
					view.triggerFetchDiaryItems();
					if(progressVal == 100){
						view.render();
					}
				},
				
				// error
				function error()
				{
					
				}
			);
			
			*/
		},
		
		updatePercentSlider : function(sliderVal,diaryItemId) {
		    var view = this;
		    var className = '.percentSlider'+diaryItemId;
			$(className).slider({
				orientation : "horizontal",
				range : "min",
				max : 100, // max value of the slider
				value : sliderVal,
				step : 1,
				animate : "fast",
				min : 0,
				change : function(event, ui) {
					newVal = ui.value;
					var markComplete = '#markComplete'+diaryItemId;
					$("#percent"+diaryItemId).val(newVal+"%");
					if(newVal == 100){
						$(markComplete).prop('checked', true);
					} else {
						$(markComplete).prop('checked', false);
					}
					view.updateProgress(diaryItemId,newVal);
				}
			});
		},
		
		itemMarkCompleted : function(event){
			var isCompleted = $(event.target).is(":checked");
			var diaryItemId = $(event.currentTarget).attr('diaryItemId');
			if(isCompleted){
				$(event.currentTarget).prop('checked', true);
				$(event.currentTarget).closest('div.phwtcg_ctxt_div').find('.diaryItem').addClass("completed");
				this.updatePercentSlider(100, diaryItemId);
			} else {
				$(event.currentTarget).prop('checked', false);
				$(event.currentTarget).closest('div.phwtcg_ctxt_div').find('.diaryItem').removeClass("completed");
				this.updatePercentSlider(0, diaryItemId);
			}
		},
		
		newsAnnouncementView : function(event){
			  //console.log("newsAnnouncementView clicked");
			  if($('.tabs_container .tabMenu_btn ul li.announcements').hasClass("active")){
			  	return;
			  }
			  
			  $('.tabs_container .tabMenu_btn ul li.glance').removeClass("active");
			  $('.tabs_container .tabMenu_btn ul li.announcements').addClass("active");
			  $('.glanceContent').hide();
			  app.model.get(ModelName.APPLICATIONSTATE).set('isDueSelected',true);
			  var newsAndAnnouncments = app.model.get(ModelName.NEWSANNOUNCEMENTS);
			  var viewParams = this.mapDay(newsAndAnnouncments);
			  
			  //console.log("viewparams : "+JSON.stringify(viewParams));
			  $('#subTabsHomeView #tabsContent', this.el).html(app.render('newsAnnouncementsHomeView', viewParams));
			  $(".notification_num", this.el).html(newsCount);
			  
			  newsAndAnnouncments = null;
			  viewParams = null;
		},
		
		//group news and announcements daywise
		'mapDay' : function(newsAnnouncements){
			var daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
			var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			
			var view = this;
			var newsAnnouncementsModel = {
					'newsAnnouncements':[],
			};
		 
		 	var unreadItems = 0;
		 	var now=new Date();
		    var month=now.getMonth();
		 	var calendar = app.model.get(ModelName.CALENDAR);
		 	//take calendardays of 1 month to take news of 1 month span
		 	var calendarDays = calendar.getMonthDays(month+1,now.getFullYear()).toArray().reverse() ;
		 	if(month != 0){
		 		calendarDays = calendarDays.concat(calendar.getMonthDays(month,now.getFullYear()).toArray().reverse());
		 	}
		 	
		 	var dayObject;
		 	var days;
		 	var newsAnnouncementObject;
		 	newsCount = 0;
		 	
		 	_.forEach(calendarDays, function(calendarDay) {
		 		dayObject = {
						'newsAnnouncementsForDay': [],
					};
				newsAnnouncementObject={
						'newsAnnouncement':[],
				};
		 		var calDay = new Rocketboots.date.Day(calendarDay.get('day'));
				for(var i = 0; i < newsAnnouncements.length;i++){
					var newsAnnouncement =  newsAnnouncements[i];
					var dayKey = newsAnnouncement.get('Updated');
					var year=dayKey.toString().substring(0,4);
					var month=dayKey.toString().substring(5,7);
					var day=dayKey.toString().substring(8,10);
					var newDate=new Date(year,month-1,day);
					var calendarDate = new Date(calDay.getDate());
					newsAnnouncement.createdTime = (dayKey.split(" "))[1];
					
					if (newDate-calendarDate == 0) {
						if(newsAnnouncement.get('readStatus') == null || newsAnnouncement.get('readStatus') == 0){
							unreadItems = unreadItems + 1;
						}
						newsAnnouncementObject.newsAnnouncement.push(newsAnnouncement.toJSON());
					}	
				}
				if(newsAnnouncementObject.newsAnnouncement.length > 0){
					newsCount = newsCount + newsAnnouncementObject.newsAnnouncement.length;
					dayObject.newsAnnouncementsForDay.push(newsAnnouncementObject);
				}
				
				if(dayObject.newsAnnouncementsForDay.length > 0){
					var date = null; 
					var schoolFormat = app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat');
				    if (schoolFormat == "USA"){
				    	date = months[calendarDay.get('day').getMonth()].toString().substring(0,3) + " " + calendarDay.get('dayOfMonth') + ", " + now.getFullYear();
					} else {
						date = calendarDay.get('dayOfMonth')  + " " + months[calendarDay.get('day').getMonth()].toString().substring(0,3) + " " + now.getFullYear();
					}
				    
					dayObject.createdDate = date;
					dayObject.createdDay = daysOfWeek[calendarDay.get('day').getDay()];
					
					var dayOfWeekForGlance = '';
					dayOfWeekForGlance = diary.view.DiaryCalendarViewUtil.formatShortDateInString(new Rocketboots.date.Day(calendarDay.get('day')));
					if(dayOfWeekForGlance != ''){
						dayObject.createdDay = dayOfWeekForGlance;
					}
					newsAnnouncementsModel.newsAnnouncements.push(dayObject);
				}
				
				dayObject = null;
		 	});
		 	
		 	unreadItemsGlobal = unreadItems;
			calendar = null;
			calendarDays = null;
		 	return newsAnnouncementsModel;
		},
		showDiaryItemForm: function(event) {
			
			var view = this,
				params = {},
				target = $(event.currentTarget),
				diaryItemId = target.attr('diaryItemId') || null;
				
			var isMerit = $(event.currentTarget).attr('isMerit');
			

			// Do nothing for days of note
			if (target.hasClass("dayOfNote")) return;

			if (diaryItemId == null) {
				params = view.getDiaryItemAttributesFromElement(target);
			}
			//console.log("showDiaryItemForm"+diaryItemId)
			if(diaryItemId)
			{
				
			var meritParamsText = ", diu.passtype_id, diu.item_id, diu.comment, diu.student_id, di.classid";
			var meritJoin = " left outer join diaryItemuser diu on diu.item_id = di.uniqueid ";
				
			//console.log("inside if");
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT di.UserID, di.createdBy, di.type " + meritParamsText +" FROM diaryItem di "+ meritJoin +" WHERE di.id = :diaryItemId",

            // params
            {
			'diaryItemId' :diaryItemId
            },
			// success
			function(statement) {
			
				var detail=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    
                 if(detail.length)
                 {
                 		var userID=detail[0].UserID;
                 		var createdBy=detail[0].createdBy;
                 		var type=detail[0].type;
                 		if(createdBy!=null && userID==createdBy)
                 		{
                 			//console.log("createdBy diary Item Match");
                 		 diaryItemForm.showDialog(diaryItemId,
                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
                                          params.startTime,
                                          params.endTime,
                                          { 'subject' : params.subject }, createdBy);
                                   $(".editButtonContainer .save").show();
                                   $(".editButtonContainer .lockItem").hide();
                                   $(".ui-dialog-titlebar").hide();
                                   $(".editButtonContainer .delete").show();
								   
								   if(type == 'MERIT'){
										$(".editButtonContainer .save").hide();
									}
                 		}
                 		else
                 		{
                 			var isMeritReadonly = isMerit && detail[0].passtype_id && detail[0].passtype_id != 0;
							if(isMeritReadonly){
								
								//console.log("createdBy Match FALSE");
								diaryItemForm.showDialog(null,
                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
                                          params.startTime,
                                          params.endTime,
                                          { 'subject' : params.subject }, createdBy,null, true,false,true,detail[0].item_id,detail[0].student_id,detail[0].ClassId,detail[0].comment,detail[0].passtype_id);
										  
							} else {
								//console.log("createdBy Match FALSE");
								diaryItemForm.showDialog(diaryItemId,
                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
                                          params.startTime,
                                          params.endTime,
                                          { 'subject' : params.subject }, createdBy);
							}
							
							disablediaryitemformWeek();
							if(type == 'EVENT' || type == 'MERIT' || isMeritReadonly){
								$(".editButtonContainer .save").hide();
							} else {
								$(".editButtonContainer .save").show();
							}
							$(".editButtonContainer .delete").hide();
							$(".editButtonContainer .lockItem").show();
							$(".ui-dialog-titlebar").hide();
                 		}
                 }
			
			},
			
			// error
			function error()
			{
				//console.log("Error while trying to create ClassUser item");
			}
			);
			}
			else
			{
			//console.log("inside else");
				diaryItemForm.showDialog(diaryItemId,
		                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
		                                          params.startTime,
		                                          params.endTime,
		                                          { 'subject' : params.subject });
		         $(".editButtonContainer .save").show();
		         $(".editButtonContainer .lockItem").hide();
		         $(".ui-dialog-titlebar").hide();
			}
			
			event.stopPropagation();
		},
		
		showTasksInPopup: function(event) {
			//console.log("showTasksInPopup :");
			var view = this,
			// get the identifier of the diary item(s) that we need to display
			diaryItemIds = $(event.currentTarget).attr('diaryItemIds') || $(event.currentTarget).attr('diaryItemId');
			
			if (diaryItemIds) {
				view.showDiaryItemPopup(diaryItemIds, $(event.currentTarget));
				event.stopPropagation();
			}
			
		},
		
		showEventInPopup: function(event) {
			this.showDiaryItemForm(event);
			event.stopPropagation();
		},
		
		openInboxView: function(event) {
			this.markItemAsRead(event);
			
			var id = $(event.currentTarget).attr('id');
			var itemType = $(event.currentTarget).attr("type")
			app.model.get(ModelName.APPLICATIONSTATE).set('inboxItemId',id);
			app.model.get(ModelName.APPLICATIONSTATE).set('inboxItemType',itemType);
			app.view.mainView.switchToView("inbox",{
				id: id,
				type: itemType,
				forceRefresh: true
			});
		},
		
		redirectToProfile: function(event) { 
			app.view.mainView.switchToView("myprofile");
			$(".profile_li .navigationItem").addClass("selected");
		},
		
		markItemAsRead: function(event) {
			var id = $(event.currentTarget).attr('id');
			var table = $(event.currentTarget).attr('type');
			var tableName = '';
			if(table == 'News'){
				tableName = 'News';
			} else if(table == 'Announcement'){
				tableName = 'Announcements';
			}
			
			if(tableName != ''){
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
		            // query
		            "UPDATE "+ tableName + " SET readStatus = 1  WHERE id = :itemId",
		
		            // params
		            {
					'itemId' :id
		            },
					// success
					function(statement) {
					
					},
					
					// error
					function error()
					{
						
					}
				);
			}
			app.context.trigger(EventName.FETCHINBOX);
		},
		
		
		showDiaryItemPopup: function(diaryItemIds, $anchor) {
		
			app.model.get(ModelName.DIARYITEMS).off("change");
			app.model.get(ModelName.DIARYITEMS).off("remove");
			app.model.get(ModelName.DIARYITEMS).on("change", updateHomeView, this);
			app.model.get(ModelName.DIARYITEMS).on("remove", updateHomeView, this);
			
			//console.log("showDiaryItemPopup");
			var view = this, 
				// get the diary items
				diaryItemsModel = app.model.get(ModelName.DIARYITEMS),
                periodRow = $anchor.closest('li'),
                startTime,
                endTime;

            // if the start and end time of the
            if (periodRow.attr('startTime') && periodRow.attr('endTime')) {
                startTime = Rocketboots.date.Time.parseTime(periodRow.attr('startTime'));
                endTime = Rocketboots.date.Time.parseTime(periodRow.attr('endTime'));
            }

            // render the content of the popup
            //console.log("popup content : "+JSON.stringify(diary.view.DiaryCalendarViewUtil.mapTasksForPopup(diaryItemsModel,diaryItemIds,app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),startTime,endTime)));
			var popupContent = app.render('diaryItemsSummary', {'items' : diary.view.DiaryCalendarViewUtil.mapTasksForPopup(diaryItemsModel,
                                                                                                                            diaryItemIds,
                                                                                                                            app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
                                                                                                                            startTime,
                                                                                                                            endTime) });
			
			app.popup.hide($("#diaryItemsPopup"));			
			$("#diaryItemsPopup>div.popupContent").html(popupContent);
			
			app.popup.show( $("#diaryItemsPopup"), $anchor, "right", null, null, $(".glanceContainer", view.el));
			
			$("#diaryItemsPopup .diaryItemSummary .infolink", this.el).on("click", function(event) {	
				view.showDiaryItemForm(event);
			});
			
			$(".webLinkDiaryItem", this.el).on("click", function(event) {				
				var link = prepareWeblinkURL($(event.currentTarget).attr('webLinkURL'));
				var urlReq = new air.URLRequest(link); 
				air.navigateToURL(urlReq);
			});
			
			$("#diaryItemsPopup .diaryItemSummary .noteEntry .save", this.el).on("click", function(event) {
				//console.log("diaryItemsPopup : save");
				// get the note that was entered
				var noteEntryElement = $("#diaryItemsPopup .diaryItemSummary .noteEntry .noteEntry"),
					noteId = parseInt(noteEntryElement.attr("noteId"), 10),
					noteText = noteEntryElement.val().trim();
					//setnoteisSyncZero(noteId);
				// append the note
				diary.view.DiaryView.appendNote(noteId, noteText);
				app.popup.hide($("#diaryItemsPopup"));
				openMessageBox("Diary item updated successfully.");
				
			});
			$("#diaryItemsPopup .diaryItemSummary .noteEntry .delete", this.el).on("click", function(event) {
				//console.log("diaryItemsPopup : delete");
                // get the note that was entered
                var hel = confirm("Are you sure to delete this note ?");
			
					if(hel)
					{
               			var noteEntryElement = $("#diaryItemsPopup .diaryItemSummary .noteEntry .noteEntry"),
                    	noteId = parseInt(noteEntryElement.attr("noteId"), 10),
                    	noteText = noteEntryElement.val().trim();
                		DeleteNoteEntryMethod(noteId);
               			 // append the note
                		//diary.view.DiaryView.appendNote(noteId, noteText);
               			 app.popup.hide($("#diaryItemsPopup"));
                		/*var noteEntryElement = $(".noteDetail #newNoteEntry .noteEntry"),
               			 noteId = parseInt(noteEntryElement.attr("noteId"), 10);*/
                	}
                	else
                		return;
                
                
            });
            

            // re-assign the event handlers as the pop-up was rendered
            //view.dragDropHelper.assignEventHandlers();
            
		}
		

		

	});
	
	return homeView;

})();