/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: DiaryCalendarViewUtil.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.view = diary.view || {};

/**
 * A collection of utility methods to be used by the 4 calendar views (day, week, month, year).
 * created by John Pearce
 * @type {Object}
 */
diary.view.DiaryCalendarViewUtil = (function () {

	var util = {

		/**
		 * The mapping from a month index to a human-readable month. (zero-indexed)
		 * @type {array[string]}
		 */
		'months' : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		'daysOfWeek' : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

        /**
         * The mapping of Non Cycle Days Periods
         */
        'NON_CYCLE_DAY_PERIODS' : [
            {
                startTime: new Rocketboots.date.Time(0),
                endTime: new Rocketboots.date.Time(8)
            }, {
                startTime: new Rocketboots.date.Time(8),
                endTime: new Rocketboots.date.Time(10)
            }, {
                startTime: new Rocketboots.date.Time(10),
                endTime: new Rocketboots.date.Time(12)
            }, {
                startTime: new Rocketboots.date.Time(12),
                endTime: new Rocketboots.date.Time(14)
            }, {
                startTime: new Rocketboots.date.Time(14),
                endTime: new Rocketboots.date.Time(16)
            }, {
                startTime: new Rocketboots.date.Time(16),
                endTime: new Rocketboots.date.Time(18)
            }, {
                startTime: new Rocketboots.date.Time(18),
                endTime: new Rocketboots.date.Time(20)
            }, {
                startTime: new Rocketboots.date.Time(20),
                endTime: new Rocketboots.date.Time(0)
            }
        ],

		/**
		 * Map from a collection of diary items to objects to be passed into Mustache,
		 * based on a year.
		 * @param {diary.model.Calendar} calendar The calendar object representing the per-day information. Note this also defines the year.
		 * @param {diary.collection.DiaryItemCollection} diaryItems A collection of dairy items to map to objects.
		 * @return {array[Object]} Returns a list of month objects.
		 */
		'mapYear' : function (calendar, diaryItems, fromGlance) {

		var startDate = new Rocketboots.date.Day(app.model.get(ModelName.APPLICATIONSTATE).get('startDate'));
		var endDate = new Rocketboots.date.Day(app.model.get(ModelName.APPLICATIONSTATE).get('endDate'));
		var maxMonths = months_between(startDate,endDate);
		
        var year = startDate.getDate().getFullYear();
		// holds the month list
			var months = [];
            var month=monthArray[0];
            var monthIndex=monthIndexArray[0];
          // console.log("MAP YEAR monthIndex="+monthIndex);
            var uperLimit=12;
			// parse all the months from the calendar
			if(!isSingleYear)	
			{
			for (var month = monthIndex+1 ; month <= uperLimit ; month++) {
			//console.log("month====="+month);
			//console.log("claendar.year="+calendar.attributes.year);
				// TODO: filter diaryItems down to just the relevant month (for efficiency reasons)
				months.push(util.mapMonth(calendar, diaryItems, month, false, fromGlance, year));
				if(month==12)
                  {
                            month=0;
                            uperLimit=12-months.length;
                           // console.log("new uperLimit="+uperLimit);
                   }

			}
			}
			else
			{
            for (var month = 1 ; month <= maxMonths ; month++) {
            
            //console.log("month====="+month);
			//console.log("claendar.year="+calendar.attributes.year);
            	var i = month;
//            	if(month > 12){
//     			   i = month - 12;
//     		   	}
            	
            	if(month > 24){
            		i = month - 24; 
            	} else if(month > 12){
            		i = month - 12; 
            	}
            	
				// TODO: filter diaryItems down to just the relevant month (for efficiency reasons)
				months.push(util.mapMonth(calendar, diaryItems, i, false, fromGlance, year));
				if(diary.view.DiaryCalendarViewUtil.months[i-1].toLowerCase() == 'december'){
    	       		year = year+1;
    	       	}

			}
            
            }

			// return the list of months
			return months;
		},
		
		'mapGlance' : function (fromGlance, isDueSelected) {
			var allItems = [];
            
			if(isDueSelected)
				allItems.push(util.mapDueInGlance(fromGlance));
			else
				allItems.push(util.mapAssignedInGlance(fromGlance));

			return allItems;
		},
		
		'addMonth':function(monthArray)
		{
		     util.months= monthArray;
		},

		/**
		 * Map from a collection of diary items to objects to be passed into Mustache,
		 * based on the month of a year.
		 * @param {diary.model.Calendar} calendar The calendar object representing the per-day information (for a particular year).
		 * @param {diary.collection.DiaryItemCollection} diaryItems A collection of dairy items to map to objects.
		 * @param {number} month The month in which results should lie.
		 * @param {?boolean} includePadDays Flag denoting whether padding days should be included (default is false)
		 * @return {Object} Returns an object with an array of days.
		 */
		'mapMonth' : function (calendar, diaryItems, month, includePadDays, fromGlance, year) {
		//console.log("MAP MONTH  "+month);
		//console.log("calendar "+ calendar.get('year'));
		
		   
			// holds the month that is being mapped to the view model
			var days = [];
			var calendarDays = calendar.getMonthDays(month, year).toArray();
			if(fromGlance){
				calendarDays = calendarDays.reverse();
			}
			
			includePadDays = includePadDays || false;
			
            // add place-holder days before and after the month starts
            if (includePadDays) {
           // console.log("INSIDE INCLUDEPADDAYS")
             var firstDay =null;
             var x =calendar.get('year');
            // console.log("isSingleYear="+isSingleYear);
             if(!isSingleYear)	
             {
               if(month >6)
               {
                 //x = x-1;
				}
			 }
			 //console.log("*************************");
			 //console.log("month="+month);
			// console.log("year="+x);
			
				 firstDay = new Rocketboots.date.Day(x, month - 1, 1),
                 lastDay = new Rocketboots.date.Day(x, month - 1, Rocketboots.date.Day.LAST_DAY_OF_MONTH);
				
                firstDay.addDays(-1);
                lastDay.addDays(1);

                while (firstDay.getDay() != 0) { // Sunday
                    // Insert place-holder day
                    if (firstDay.getFullYear() != year) {
                        calendarDays.splice(0, 0, {
                            'date' : firstDay.getDayInMonth(), 'othermonth' : true, 'tasks': []
                        });
                    } else {
                        calendarDays.splice(0, 0, calendar.getDay(firstDay.getMonth() + 1, firstDay.getDayInMonth(), firstDay.getFullYear()));
                    }
                    // Move backwards 1 day
                    firstDay.addDays(-1);
                }

				while (lastDay.getDay() != 1 || calendarDays.length < 35) { // Monday
					// Insert place-holder day
					if (lastDay.getFullYear() != year) {
						calendarDays.push({
							'date' : lastDay.getDayInMonth(), 'othermonth' : true, 'tasks': []
						});
					} else {
						calendarDays.push(calendar.getDay(lastDay.getMonth() + 1, lastDay.getDayInMonth(), lastDay.getFullYear()));
					}
					// Move forwards 1 day
					lastDay.addDays(1);
				}

			}


			// go through each of the days and map it to the views model
			_.forEach(calendarDays, function(calendarDay) {
				// TODO: filter diary items down to just the relevant day (for efficiency reasons)
				if (calendarDay instanceof diary.model.CalendarDay) {
					var day = util.mapDay(diaryItems, calendarDay, 4, fromGlance);
					if (calendarDay.get('day').getMonth() != (month - 1)) {
						day.othermonth = true;
					}
					days.push(day);
				} else {
					days.push(calendarDay);
				}

			});
			//console.log("month="+month);
			if(!isSingleYear)
			{
			var x=0;
			//console.log("x="+ x-1);
			/*if(month>6)
			{
				x=(month)%6;
				if(x==0)
					x=6;
			}
			else if(month<=6)
				x=month+6;*/
				x=$.inArray(month-1, monthIndexArray)
			
			//console.log("x=="+x);
			//console.log("console.log(util.months)");
			//console.log(util.months);
			// return the month
			var monthObj =  {
				'monthNumber' : month,
				'month' : util.months[x],
				'year':year,
				'days' : days
			};
			}
			else
			{
				var monthObj =  {
				'monthNumber' : month,
				'month' : util.months[month - 1],
				'year':year,
				'days' : days
			};
			}
			if (days.length > 35) {
				monthObj.sixweeks = true;
			}
			//console.log("monthObj");
			//console.log(monthObj);
			return monthObj;
		},

		/**
		 * Map from a collection of diary items to objects to be passed into Mustache,
		 * based on the week of a year.
		 * @param {diary.model.Calendar} calendar The calendar object representing the per-day information (for a particular year).
		 * @param {diary.collection.DiaryItemCollection} diaryItems A collection of dairy items to map to objects.
		 * @param {number} week The week in which results should lie.
		 * @return {Object} Returns an object with an array of days.
		 */
		'mapWeek' : function (calendar, diaryItems, week, fromGlance) {
		
			//console.log("diary.view.DiaryCalendarViewUtil::mapWeek(<" + diaryItems.length + " items>, " + week + ")");
			// holds the month that is being mapped to the view model
			var days = [];
			// get all the calendar days for the month
			var calendarDays = calendar.getWeekDays(week);
			//console.log("!!!!!!!"+JSON.stringify(calendarDays));
			//console.log("calendarDays.at(0).get('day').getDate()="+calendarDays.at(0).get('day').getDate());
			//console.log("calendarDays.at(calendarDays.length - 1).get('day').getDate()="+calendarDays.at(calendarDays.length - 1).get('day').getDate());
			// go through each of the days and map it to the views model
			
			var calendarDaysarray = calendarDays.toArray();
			if(fromGlance){
				calendarDaysarray = calendarDays.toArray().reverse();
			}
			_.forEach(calendarDaysarray, function(calendarDay) {

				// TODO: filter diary items down to just the relevant day (for efficiency reasons)
				days.push(util.mapDay(diaryItems, calendarDay, null, fromGlance));

			});

			// return the week object
			return {
				'weekNumber' : week,
				'week' : util.dateRangeDescription(calendarDays.at(0).get('day').getDate(), calendarDays.at(calendarDays.length - 1).get('day').getDate()),
				'days' : days,
				// Helpers, not used in mustache, but read by the view class
				'dateStart' : calendarDays.at(0).get('day').getDate(),
				'dateEnd' : calendarDays.at(calendarDays.length - 1).get('day').getDate()
			};
		},

		/**
		 * Get the description for a date range (within the same year!), returning a textual date range.
		 * Output format: 1 January - 25 March
		 * @param {Date} dateStart First date, defining the start of the date range.
		 * @param {Date} dateEnd End date, defining the end of the date range.
		 * @return {string} A human-readable description of the date range.
		 */
		'dateRangeDescription' : function (dateStart, dateEnd) {
			//console.log("dateRangeDescription=");
			//console.log("dateStart.getDate()="+dateStart.getDate());
			//console.log("dateEnd.getMonth()="+dateEnd.getMonth());
			var dateLabel = null;
			
			if (app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat') == "USA"){
				dateLabel = monthNames[dateStart.getMonth()].substring(0,3) + " "+
						dateStart.getDate() +
						" to " + (dateStart.getMonth() == dateEnd.getMonth() ? "" : " " +monthNames[dateEnd.getMonth()].substring(0,3))  + " " + dateEnd.getDate();
			} else {
				dateLabel = dateStart.getDate() +
						(dateStart.getMonth() == dateEnd.getMonth() ? "" : " " +monthNames[dateStart.getMonth()].substring(0,3)) +
						" to " + dateEnd.getDate() + " " + monthNames[dateEnd.getMonth()].substring(0,3);
			}
			
			return dateLabel;
		},

		/**
		 * Format a date string for display.
		 * @param date {!Rocketboots.date.Day} The date to format.
		 * @param shorten {?Boolean} If set to true, the result string will not contain the day of week
		 * @return {String} The formatted date string.
		 */
		'formatDateString' : function(date, shorten) {
		//console.log("formatDateString Date= "+date);
		
		//console.log("getDay= "+util.daysOfWeek[date.getDate().getDay()]);
		//console.log("getDayInMonth()= "+date.getDayInMonth());
		//console.log("getMonth()=" + util.months[date.getMonth()]);
		//console.log("date.getMonth()=" + date.getMonth());
			// in the shortened version, exclude the day of week
			var x=0;
			var dateYear = date.getFullYear();
			if(date.getMonth()>6)
			{
				x=(date.getMonth())%6;
				if(x==0)
					x=6;
			}
			else if(date.getMonth()<=6)
				x=date.getMonth()+6;
			//var x = (date.getMonth()+ 6)%6;
			//console.log("date.getMonth()="+date.getMonth());
			var yearIndex=getindex(date.getMonth());
			var yearToShow=yearArray[yearIndex];
			var newDate=new Rocketboots.date.Day(yearToShow, date.getMonth(), date.getDayInMonth());
			date=newDate;
			
			var startDate = new Rocketboots.date.Day(app.model.get(ModelName.APPLICATIONSTATE).get('startDate'));
			var selectedDay = new Rocketboots.date.Day(app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'));
			var today = new Rocketboots.date.Day();
			
			if((today.getTime() < startDate.getTime()) && (selectedDay.getTime() < startDate.getTime())){
				yearToShow = (new Date()).getFullYear();
			}
			
			return monthNames[date.getMonth()].substring(0, 3)
				+ " " + date.getDayInMonth()
				+ ", " + dateYear;
		},

        /**
         * Converts the given date to a string relative to todays date.
         *
         * @param {!Rocketboots.date.Day} date The date that is to be formatted
         *
         * @return {string} The formatted date relative to today. The value can be either Yesterday, Today, Tomorrow or the Date in format dd MMMM
         */
		'formatShortDateRelativeToTodayString' : function(date) {
			
            var today = new Rocketboots.date.Day(),
                yesterday = new Rocketboots.date.Day(today).addDays(-1),
                tomorrow = new Rocketboots.date.Day(today).addDays(1),
                result;

            if (date.getKey() == yesterday.getKey()) {
                result = "1 day ago";
            } else if (date.getKey() == today.getKey()) {
                result = "Today";
            } 
            /* else if (date.getKey() == tomorrow.getKey()) {
                result = "Tomorrow";
            } */
            else {
            	var days = Math.floor((today.getTime() - date.getTime())/(1000*60*60*24));
            	if(days < 0){
            		var dueDay = util.formatShortDateString(date);
            		result = dueDay;//Math.abs(days) + " days after";
            	} else { 
            		result = days + " days ago";
            	}
            }

            return result;

        },
        
        'formatShortDateInString' : function(date) {

            var today = new Rocketboots.date.Day(),
                yesterday = new Rocketboots.date.Day(today).addDays(-1),
                tomorrow = new Rocketboots.date.Day(today).addDays(1),
                result='';

            if (date.getKey() == yesterday.getKey()) {
                result = "Yesterday";
            } else if (date.getKey() == today.getKey()) {
                result = "Today";
            } else if (date.getKey() == tomorrow.getKey()) {
                result = "Tomorrow";
            } 

            return result;

        },

        'formatShortDateString' : function(date) {
        	var schoolFormat = app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat');
		    if (schoolFormat == "USA")
		    	return date.getMonthName().substring(0, 3) + " " + date.getDayInMonth();
			else
				return date.getDayInMonth() + " " + date.getMonthName().substring(0, 3);
		},
		
		'formatShortDate' : function(date) {
        	var schoolFormat = app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat');
			var month = date.getMonth()+1;
		    if (schoolFormat == "USA")
		    	return month + "/" + date.getDayInMonth()+ "/" + date.getFullYear();
			else
				return date.getDayInMonth() + "/" + month + "/" + date.getFullYear();
		},

		'formatNoteDate' : function (timestamp) {
			var day = timestamp.getDate(),
				month = timestamp.getMonth() + 1;

			if (app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat') == "USA"){
				return [
						((month <= 9 ? '0' : '') + month),
						((day <= 9 ? '0' : '') + day),
						timestamp.getFullYear()
					].join("/");
			} else { 
				return [
						((day <= 9 ? '0' : '') + day),
						((month <= 9 ? '0' : '') + month),
						timestamp.getFullYear()
					].join("/");
			}
			
		},

		'formatTime' : function (date, includeAMPM, includeMinutes) {
			var hour, minute, minuteString;
			includeAMPM = true; 	//(includeAMPM == null || includeAMPM == undefined) ? true : includeAMPM;
			includeMinutes =  true; //includeMinutes || false;

			hour = (date instanceof Rocketboots.date.Time) ? date.getHour() : date.getHours();
			minute = (date instanceof Rocketboots.date.Time) ? date.getMinute() : date.getMinutes();
			minuteString = (minute == 0 ? (includeMinutes ? ":00" : "") : ":" + ((minute < 10) ? "0" : "") + minute);
			
			return (hour < 12 ?
				(hour == 0 ? 12 : hour) + minuteString + (includeAMPM ? " am" : "") :
				hour - (hour == 12 ? 0 : 12) + minuteString + (includeAMPM ? " pm" : ""));
		},

		'formatStartEndTime' : function(startTime, endTime, delimiter) {
			var str = "";
			if (startTime) {
				str += "(" + util.formatTime(startTime);
				if (endTime) {
					str += delimiter + util.formatTime(endTime);
				}
				str += ")";
			}
			return str;
		},
		
		'convertDateTime' : function(date){
			var includeMinutes = true;
			if(date != null || date != undefined || date != '')
			{
			    dateTime = date.split(" ");
			
			    var date = dateTime[0].split("-");
			    var yyyy = date[0];
			    var mm = date[1]-1;
			    var dd = date[2];
			
			    var time = dateTime[1].split(":");
			    var hour = time[0];
			    var minute = parseInt(time[1]);
			    var minuteString = (minute == 0 ? (includeMinutes ? ":00" : "") : ":" + (minute < 10 ? "0" : "") + minute);
			
			    return (hour < 12 ?
					(hour == 0 ? 12 : hour) + minuteString + " am" : hour - (hour == 12 ? 0 : 12) + minuteString + " pm" );
		    } else {
		    	return "";
		    }
		},

		/**
		 * Map from a collection of diary items to objects to be passed into Mustache,
		 * based on a day record of a calendar.
		 * @param {diary.collection.DiaryItemCollection} diaryItems A collection of dairy items to map to objects.
		 * @param {diary.model.CalendarDay} calendarDay The calendar day in which results should lie (populated with the day-specific, item-independant information).
		 * @return {Object} Returns an object containing information for a day.
		 */
		'mapDay' : function (diaryItems, calendarDay, maxEvents, fromGlance) {
		 //console.log("mapDay");
			//console.log("diary.view.DiaryCalendarViewUtil::mapDay(<" + diaryItems.length + " items>, " + calendarDay.get('day').getKey() + ")");
			// holds the day that is being generated
			var dayObject = {
				'dayOfNotes': [],
				'events' : [],
				'tasks' : [],
				'notes' : [],
				'merits' : [],
				'totcount' : [],
				'visibleitems' : [],
				// Helpers, not used in mustache, but optionally read by the view class
				'dateObject' : calendarDay.get('day').getDate()
			
			};

			var now = new Date(); // store todays date

			_.forEach(calendarDay.get('dayOfNotes').toArray(), function(dayOfNote) {
				if (dayOfNote.get('title') != "") {
					dayObject.dayOfNotes.push({
						title: dayOfNote.get('title')
					});
				}
			});

			dayObject.hasDaysOfNote = dayObject.dayOfNotes.length > 0;

			dayObject.day = calendarDay.get('dayOfMonth');
			dayObject.month = util.months[calendarDay.get('day').getMonth()];
			dayObject.dayOfWeek = util.daysOfWeek[calendarDay.get('day').getDay()];
	//		dayObject.title = calendarDay.get('title');
			dayObject.today = calendarDay.get('day').getFullYear() == now.getFullYear() && calendarDay.get('day').getMonth() == now.getMonth() && calendarDay.get('day').getDayInMonth() == now.getDate();
			dayObject.dateKey = calendarDay.get('day').getKey();
		    
			var date;
			
			var schoolFormat = app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat');
		    if (schoolFormat == "USA")
		    	date =  calendarDay.get('day').getMonthName().toString().substring(0,3) + " " + calendarDay.get('dayOfMonth') + " " + calendarDay.get('day').getFullYear();
			else
				date = calendarDay.get('dayOfMonth') + " " + calendarDay.get('day').getMonthName().toString().substring(0,3) + " " + calendarDay.get('day').getFullYear();
			
			dayObject.date = date;
			
			if(fromGlance){
				var dayOfWeekForGlance = '';
				dayOfWeekForGlance = util.formatShortDateInString(new Rocketboots.date.Day(calendarDay.get('day')));
				if(dayOfWeekForGlance != ''){
					dayObject.dayOfWeek = dayOfWeekForGlance;
				}
			}

			// if it is greyed out we will mark the cell accordingly
			if (calendarDay.get('isGreyedOut') != null && calendarDay.get('isGreyedOut')) {
				// there are two types of shading for the greyed out cell
				// one is for the weekend the other is for the special days
				// decide accordingly if it is not the weekend then it is a special day
				// nice and easy...
				if (calendarDay.get('isWeekend') != null && calendarDay.get('isWeekend')) {
					dayObject.weekend = true;
				} else {
					dayObject.holiday = true;
				}
			}

			// determine the cycle day details
			// NOTE - if we have the cycle we will assume that we have a cycle day
			if (calendarDay.get('cycle') != null) {
				dayObject.cycle = calendarDay.get('cycle');
				dayObject.cycleLabel = (calendarDay.get('cycleLabel') ? calendarDay.get('cycleLabel').toUpperCase() : null);
				dayObject.cycleday = calendarDay.get('cycleDay');
				dayObject.cycleDayLabel = calendarDay.get('cycleDayLabel') ? calendarDay.get('cycleDayLabel').toUpperCase() : null;
				dayObject.cycleDayShortLabel = calendarDay.get('cycleDayShortLabel');
			}
			
			if (calendarDay.get('tempDay') != null) {
				dayObject.tempDay = calendarDay.get('tempDay').get('cycleDayLabel');
				dayObject.tempShortDay = calendarDay.get('tempDay').get('shortCycleDayLabel');
				//console.log("shortCycleDayLabel : "+calendarDay.get('tempDay').get('shortCycleDayLabel'));
			}

			// add any diary items for the day
			var day = new Rocketboots.date.Day(calendarDay.get('day'));
			//console.log(calendarDay.get('day'));
			diaryItems.each(function(diaryItem){
				if (diaryItem.isWithinDay(day)) {
					if (diaryItem instanceof diary.model.Event) {
						var eventObject = util.mapDiaryItem(diaryItem, day);
						eventObject.event = util.getEventTitle(diaryItem);
						eventObject.id = diaryItem.get('id');
						dayObject.events.push(eventObject);
						dayObject.hasEvents = true;
					} else if (diaryItem instanceof diary.model.Task) {
						dayObject.tasks.push(util.mapDiaryItem(diaryItem, day));
						dayObject.totcount.push(util.mapDiaryItem(diaryItem, day));
						dayObject.visibleitems.push(util.mapDiaryItem(diaryItem, day));
						dayObject.hasTasks = true;
					} else if (diaryItem instanceof diary.model.Message) {
						dayObject.totcount.push(util.mapDiaryItem(diaryItem, day));
						dayObject.notes.push(util.mapDiaryItem(diaryItem, day));
						dayObject.visibleitems.push(util.mapDiaryItem(diaryItem, day));
						dayObject.hasNotes = true;
					} else if (diaryItem instanceof diary.model.Note) {
						dayObject.totcount.push(util.mapDiaryItem(diaryItem, day));
						dayObject.notes.push(util.mapDiaryItem(diaryItem, day));
						//dayObject.visibleitems.push(util.mapDiaryItem(diaryItem, day));
						dayObject.hasNotes = true;
					} else if (diaryItem instanceof diary.model.Merit) {
						//dayObject.totcount.push(util.mapDiaryItem(diaryItem, day));
						dayObject.merits.push(util.mapDiaryItem(diaryItem, day));
						dayObject.visibleitems.push(util.mapDiaryItem(diaryItem, day));
						//dayObject.hasNotes = true;
					}
				}
			});
			
			dayObject.elementsInGlance = dayObject.tasks.length + dayObject.events.length + dayObject.notes.length + dayObject.merits.length;

			// derive other properties from the diary items
			if (dayObject.tasks.length > 0 || dayObject.merits.length > 0) {
				dayObject.summary = {
					'id' : calendarDay.get('day').getTime(),
					'count' : dayObject.visibleitems.length
				};
			} 
			
			if (dayObject.notes.length > 0){
				dayObject.notesSummary = {
					'id' : calendarDay.get('day').getTime(),
					'count' : dayObject.notes.length
				};
			}
			//console.log("dayObject.events");
			//console.log(dayObject.events);
			// give mustache a comma-separated list of diary item ids
			dayObject.diaryItemIds = "";
			_.forEach(dayObject.visibleitems, function (task) {
				dayObject.diaryItemIds += task.id + ",";
			});
			
			dayObject.noteIds = "";
			_.forEach(dayObject.notes, function (note) {
				dayObject.noteIds += note.id + ",";
			});
			
			dayObject.notesCount = dayObject.notes.length;

			// sort everything
			dayObject.dayOfNotes = _.sortBy(dayObject.dayOfNotes, function(dayOfNote) {
				return dayOfNote.title;
			});
			dayObject.events = util.sortEventsByLockedUnlocked(dayObject.events, diaryItems, calendarDay);
			
			dayObject.events = _.sortBy(dayObject.events, function(event) {
				return [1 - parseInt(event.priority),event.startTime];
			});

			if(maxEvents == null) {
				maxEvents = dayObject.events.length;
			}
			
			while(dayObject.events.length > maxEvents) {
				if (dayObject.events.length > 0) {
					dayObject.events.pop();
				} /* else if (dayObject.dayOfNotes.length > 0) {
					dayObject.dayOfNotes.pop();
				} */
			}
			//console.log("dayObject.events22222");
			//console.log(dayObject.events);
			return dayObject;
		},

		/**
		 * Map from a collection of diary items to objects to be passed into Mustache,
		 * based on a day record of a calendar. Unlike the other year/month/day map methods,
		 * this method returns diary items divided at the period level as opposed to the day.
		 * @param {diary.collection.DiaryItemCollection} diaryItems A collection of dairy items to map to objects.
		 * @param {diary.model.CalendarDay} calendarDay The calendar day in which results should lie (populated with the day-specific, item-independent information).
		 * @return {Object} Returns an object containing information for a day.
		 */
		'mapPeriodsInDay' : function (diaryItems, calendarDay) {

			var dayObject = {
					'events': [],
					'items': [],
					'hasEvents': false,
					'hasItems': false,
					'periods': []
					
				},
				periods = [],
				diaryItemsOnDay = null,
				diaryItemsInPeriods = [],
				diaryItemsInAllDay,
				addDiaryItemsToObject = function (object, diaryItems) {
					if (typeof object.events === 'undefined') object.events = [];
					if (typeof object.items === 'undefined') object.items = [];

					var items = diaryItems instanceof Backbone.Collection ? diaryItems.toArray() : diaryItems;
					// sort items
					items.sort(function (a, b) {
						var getTime = function(item) {
								if (item instanceof diary.model.Note || item instanceof diary.model.Message) {
									return item.get('atTime') ? item.get('atTime').toInteger() : null;
								} else if (item instanceof diary.model.Event) {
									return item.get('startTime') != null ? item.get('startTime').toInteger() : null;
								} else if (item instanceof diary.model.Task) {
									// TODO: handle assignedTime
									return item.get('dueTime') != null ? item.get('dueTime').toInteger() : null;
								}
							},
							aTime = getTime(a),
							bTime = getTime(b);
						
						if (aTime < bTime) {
							return -1;
						} else if (aTime > bTime) {
							return 1;
						} else {
							return 0;
						}
					});

					_.each(items, function (diaryItem) {

						var startTime = isSpecified(object.startTime) ? Rocketboots.date.Time.parseTime(object.startTime) : null,
							endTime =  isSpecified(object.endTime) ? Rocketboots.date.Time.parseTime(object.endTime) : null,
							diaryItemMapped = util.mapDiaryItem(diaryItem, calendarDay.get('day'), startTime, endTime),
							isProcessed = true;

						if (diaryItem instanceof diary.model.Event) {
							// event
							object.events.push(diaryItemMapped);

						} else if (diaryItem instanceof diary.model.Task) {
							// task
							object.items.push(diaryItemMapped);
							// if the diary item is due for
							// the current date and the due time is not specified
							// i.e. Is due for all day then we need to not mark the diary item as
							// processed
							isProcessed = !(diaryItem.isDueWithinTimeFrame(calendarDay.get('day')) && diaryItem.get('dueTime') == null);

						} else if (diaryItem instanceof diary.model.Message) {
							// message
							object.items.push(diaryItemMapped);

						} else if (diaryItem instanceof diary.model.Note) {
							// note
							object.items.push(diaryItemMapped);

						} else if (diaryItem instanceof diary.model.Merit) {
							// note
							object.items.push(diaryItemMapped);

						} else {
							return; // early exit
						}

						// keep track of the diary items which are
						// associated with a period so that we can later
						// determine which once are all day diary items
						if (isProcessed) {
							diaryItemsInPeriods.push(diaryItem);
						}

					});

					object.hasEvents = object.events.length > 0;
					object.hasItems = object.items.length > 0;

					// sort events
					object.events = util.sortEventsByLockedUnlocked(object.events, diaryItemsOnDay, calendarDay);

					// return the modified object
					return object;
				},
				day = calendarDay.get('day');

			// Add label information
			dayObject.cycle = calendarDay.get('cycle');
			dayObject.cycleLabel = calendarDay.get('cycleLabel') ? calendarDay.get('cycleLabel').toUpperCase() : null;
			dayObject.cycleday = calendarDay.get('cycleDay');
			dayObject.cycleDayLabel = calendarDay.get('cycleDayLabel') ? calendarDay.get('cycleDayLabel').toUpperCase() : null;
			dayObject.timetableName = calendarDay.get('timetable') && calendarDay.get('timetable').get('name') ?
				calendarDay.get('timetable').get('name').toUpperCase() : null;

			// Get diary items for the calendar day
			diaryItemsOnDay = new diary.collection.DiaryItemCollection(_.filter(diaryItems.toArray(), function (diaryItem) {
				return diaryItem.isWithinDay(calendarDay.get('day'));
			}));

			// Get periods for cycle day
			var timetableid = 0;
			if (calendarDay.get('timetable') && calendarDay.get('tempCycleDay') != 0) {
				var number_of_Campuses = app.model.get(ModelName.SCHOOLPROFILE).get('number_of_Campuses');
				if(number_of_Campuses && number_of_Campuses > 1){
					//console.log("tempCycleDay : "+calendarDay.get('tempCycleDay'));
					if(calendarDay.get('tempCycleDay') != 0){
						timetableid = diary.view.SubjectViewUtil.getActualTimetableAcrossCampusesForDay(calendarDay.get('tempCycleDay'),calendarDay.get('timetable'),calendarDay.get('day'));
						periods = calendarDay.get('timetable').periodsForCycleDay(calendarDay.get('tempCycleDay'),timetableid).toArray();
					}
				} else {
					periods = calendarDay.get('timetable').periodsForCycleDay(calendarDay.get('tempCycleDay')).toArray();
				}
			}

			var currentDayKey = day.getKey();
			dayObject.dayKey = currentDayKey;
			
			// Cycle day - Map periods
			var isOdd = true;
			_.forEach(periods, function (period, periodIndex) {
				var className = null;
				var periodClass = (calendarDay.get('tempCycleDay') != 0 && timetableid != 0) ? app.model.get(ModelName.CALENDAR).get('timetables').get(timetableid).classAt(calendarDay.get('tempCycleDay'), period) : calendarDay.get('timetable').classAt(calendarDay.get('tempCycleDay'), period);
	            if(periodClass && period){
	            	className = getObjectByValues(app.model.get(ModelName.ALLCLASSES), periodClass.get('subject').get('id'), 'subjectId',calendarDay.get('tempCycleDay'),'cycleDay', period.get('id'), 'periodId');
	            }
				var subject = periodClass ? periodClass.get('subject') : null,
                    periodStartTime = period.get('startTime'),
                    periodEndTime = period.get('endTime'),
					periodObject = {
						period: period.get('id'),
						cycleDay: calendarDay.get('cycleDay'),
						dayKey: currentDayKey,
						periodTitle: period.get('title'),
						subjectId: (subject ? subject.get('id') : null),
						subjectName: (className ? className.get('name') : null),
						subjectColour: (subject ? subject.get('colour') : null),
						room: periodClass ? periodClass.get('room') : null,
						startTime: period.get('startTime').toString(),
						endTime: period.get('endTime').toString(),
						timeLabel: "",
						classes: null
					},
					periodDiaryItems = [];

				periodObject.timeLabel =
					util.formatTime(period.get('startTime'), true, true)
					+ ' - ' + util.formatTime(period.get('endTime'), true, true);

				// Calculate the height of the period
				if (period.get('title').search('Period') >= 0) {
					// this is not a break
					periodObject.classes = isOdd ? "light" : "dark";
					isOdd = !isOdd;
				}

				// Get diary items associated with this period
				periodDiaryItems = _.filter(diaryItemsOnDay.toArray(), function (diaryItem) {
					// check if the diary item falls within the time frame of the period
					return diaryItem.isWithinTimeFrame(day, periodStartTime, periodEndTime);
				});

				addDiaryItemsToObject(periodObject, periodDiaryItems);

				dayObject.periods.push(periodObject);
			});

			// Cycle day - add before/after school periods
			if (dayObject.periods.length > 0) {
				var lastPeriod = periods[periods.length - 1],
                    beforeSchoolStartTime,
                    afterSchoolEndTime;

                // compute the before school start time
                beforeSchoolStartTime = new Rocketboots.date.Time(0,0);

                // compute the after school end time
                afterSchoolEndTime = new Rocketboots.date.Time(23,59);

				// before school events/items
				dayObject.periods.splice(0, 0, addDiaryItemsToObject({
					periodTitle: "Before School",
					timeLabel: "12:00am - " + util.formatTime(periods[0].get('startTime'), true, true),
					dayKey: day.getKey(),
					startTime: beforeSchoolStartTime.toString(),
					endTime: periods[0].get('startTime').toString(),
					classes: null
				}, diaryItemsOnDay.filter(function (diaryItem) {
					// must start before the first period, but after midnight
					return diaryItem.isWithinTimeFrame(day,
													   beforeSchoolStartTime,
													   periods[0].get('startTime'));
				})));

				// after school events/items
				dayObject.periods.push(addDiaryItemsToObject({
					periodTitle: "After School" ,
					timeLabel: util.formatTime(lastPeriod.get('endTime'), true, true) + " - 12:00am",
					dayKey: day.getKey(),
					startTime: lastPeriod.get('endTime').toString(),
					endTime: afterSchoolEndTime.toString(),
					classes: null
				}, diaryItemsOnDay.filter(function (diaryItem) {
					// must start after the last period
					return diaryItem.isWithinTimeFrame(day,
													   lastPeriod.get('endTime'),
													   afterSchoolEndTime);
				})));

			} else {

				// Non-cycle Day - has pre-defined periods
				_.each(util.NON_CYCLE_DAY_PERIODS, function (periodObject, periodIndex) {

                    // we will consider midnight end time as being 23:59 because midnight is theoretically the next day
                    var periodEndTime = periodObject.endTime.getHour() == 0 && periodObject.endTime.getMinute() == 0 ? new Rocketboots.date.Time(23, 59) : periodObject.endTime;

                    dayObject.periods.push(addDiaryItemsToObject({
						timeLabel : util.formatTime(periodObject.startTime, true, true) + " - " + util.formatTime(periodObject.endTime, true, true),
						dayKey: day.getKey(),
						startTime : periodObject.startTime.toString(),
						endTime : periodEndTime.toString(),
						classes: (periodIndex % 2 == 0 ? "light" : "dark") + " noSubject"
					}, diaryItemsOnDay.filter(function (diaryItem) {
						// must start during the period
						return diaryItem.isWithinTimeFrame(day,
						                                   periodObject.startTime,
                                                           periodEndTime);
					})));
				});
			}

			// All day items - add any diary items which haven't been associated with a period
			// that is the difference of the array diaryItemsOnDay and diaryItems in period
			diaryItemsInAllDay = diaryItemsOnDay.filter(function (diaryItem) {
				return diaryItemsInPeriods.indexOf(diaryItem) == -1;
			});

			addDiaryItemsToObject(dayObject, diaryItemsInAllDay);

			// Add days of note - must be done after sorting (since the other events are from the event list)
			if (calendarDay.get('dayOfNotes')) {
				/* _.forEach(calendarDay.get('dayOfNotes').toArray(), function(dayOfNote, dayOfNoteIndex) {
					if (dayOfNote.get('title') != "") {
						// add, beginning at front of array
						dayObject.events.splice(dayOfNoteIndex, 0, {
							title: dayOfNote.get('title'),
							type: "dayOfNote"
						});
					}
				}); */

				// update hasEvents flag if necessary
				dayObject.hasEvents = dayObject.events.length > 0;
			}

			return dayObject;

		},
		
		'getTodaysDiaryItems' : function (diaryItems, due) {
			var today = new Rocketboots.date.Day();
			var todaysItems = [];
			
			var noteField = true;
			var eventTime = 'startTime';
			var taskField = 'assignedTime';
			var taskDay = 'assignedDay';
			if(due){
				noteField = false;
				eventTime = 'endTime';
				taskField = 'dueTime';
				taskDay = 'dueDay';
			}
			
			var items = diaryItems instanceof Backbone.Collection ? diaryItems.toArray() : diaryItems;
			items.sort(function (a, b) {
				var getTime = function(item) {
						if (noteField && item instanceof diary.model.Note) {
							return item.get('atTime') ? item.get('atTime').toInteger() : null;
						} else if (item instanceof diary.model.Event) {
							return item.get(eventTime) != null ? item.get(eventTime).toInteger() : -1;
						} else if (item instanceof diary.model.Task || item instanceof diary.model.Merit) {
							return item.get(taskField) != null ? item.get(taskField).toInteger() : -1;
						}
					},
					aTime = getTime(a),
					bTime = getTime(b);
				if (aTime < bTime) {
					return -1;
				} else if (aTime > bTime) {
					return 1;
				} else {
					return 0;
				}
			}); 
			
			_.each(items, function (diaryItem) {
				var assignedDay;
				if (diaryItem instanceof diary.model.Event) {
					assignedDay = diaryItem.get('startDay').getKey();
				} else if (diaryItem instanceof diary.model.Task || diaryItem instanceof diary.model.Merit) {
					assignedDay = diaryItem.get(taskDay).getKey();
				} else if (noteField && diaryItem instanceof diary.model.Note) {
					assignedDay = diaryItem.get('atDay').getKey();
				}
				
				if(today.getKey() == assignedDay){
					todaysItems.push(util.mapDiaryItem(diaryItem));
				} 
			});
			
			return todaysItems;
		},
		
		'getPreviousDiaryItems' : function (diaryItems,due) {
			var today = new Rocketboots.date.Day();
			var previousItems = [];
			
			var noteField = true;
			var eventTime = 'startTime';
			var taskField = 'assignedTime';
			var taskDay = 'assignedDay';
			if(due){
				noteField = false;
				eventTime = 'endTime';
				taskField = 'dueTime';
				taskDay = 'dueDay';
			}
			
			var items = diaryItems instanceof Backbone.Collection ? diaryItems.toArray() : diaryItems;
			items.sort(function (a, b) {
				var getTime = function(item) {
						if (noteField && item instanceof diary.model.Note) {
							return item.get('atDay') ? item.get('atDay').getKey() : null;
						} else if (item instanceof diary.model.Event) {
							return item.get('startDay') != null ? item.get('startDay').getKey() : null;
						} else if (item instanceof diary.model.Task || item instanceof diary.model.Merit) {
							return item.get(taskDay) != null ? item.get(taskDay).getKey() : null;
						}
					},
					aTime = getTime(a),
					bTime = getTime(b);
				if (aTime < bTime) {
					return -1;
				} else if (aTime > bTime) {
					return 1;
				} else {
					return 0;
				}
			}); 
			
			_.each(items, function (diaryItem) {
				var assignedDay;
				if (diaryItem instanceof diary.model.Event) {
					assignedDay = diaryItem.get('startDay').getKey();
				} else if (diaryItem instanceof diary.model.Task || diaryItem instanceof diary.model.Merit) {
					assignedDay = diaryItem.get(taskDay).getKey();
				} else if (noteField && diaryItem instanceof diary.model.Note) {
					assignedDay = diaryItem.get('atDay').getKey();
				}
				
				if(today.getKey() > assignedDay){
					previousItems.push(util.mapDiaryItem(diaryItem));
				} 
			});
			
			return previousItems;
		},
		
		'getUpcomingDiaryItems' : function (diaryItems) {
			var today = new Rocketboots.date.Day();
			var upcomingItems = [];
			
			var	taskDay = 'dueDay';
			
			var items = diaryItems instanceof Backbone.Collection ? diaryItems.toArray() : diaryItems;
			items.sort(function (a, b) {
				var getTime = function(item) {
						if (item instanceof diary.model.Event) {
							return item.get('startDay') != null ? item.get('startDay').getKey() : null;
						} else if (item instanceof diary.model.Task || item instanceof diary.model.Merit) {
							return item.get(taskDay) != null ? item.get(taskDay).getKey() : null;
						}
					},
					aTime = getTime(a),
					bTime = getTime(b);
				if (aTime < bTime) {
					return -1;
				} else if (aTime > bTime) {
					return 1;
				} else {
					return 0;
				}
			}); 
			
			_.each(items, function (diaryItem) {
				var assignedDay;
				if (diaryItem instanceof diary.model.Event) {
					assignedDay = diaryItem.get('startDay').getKey();
				} else if (diaryItem instanceof diary.model.Task || diaryItem instanceof diary.model.Merit) {
					assignedDay = diaryItem.get(taskDay).getKey();
				} 
				
				if(today.getKey() < assignedDay){
					upcomingItems.push(util.mapDiaryItem(diaryItem));
				} 
			});
			
			return upcomingItems;
		},
		
		'mapAssignedInGlance' : function (fromGlance) {
			var dayObject = {
				'previousItems' : [],
				'hasTodaysItems' : false,
				'todaysItems' 	: [],
				'hasPreviousItems' : false
			};

			dayObject.todaysItems = this.getTodaysDiaryItems(app.model.get(ModelName.DIARYITEMS).getDiaryItemsForToday(true));
			dayObject.previousItems = this.getPreviousDiaryItems(app.model.get(ModelName.DIARYITEMS).getPreviousDiaryItems(true));
			
			dayObject.hasTodaysItems = dayObject.todaysItems.length > 0 ? true : false;
			dayObject.hasPreviousItems = dayObject.previousItems.length > 0 ? true : false;
			
			return dayObject;
		},
		
		'mapDueInGlance' : function (fromGlance) {
			var dayObject = {
				'upcomingItems' : [],
				'hasUpcomingItems' : false,
				'todaysItems' 	: [],
				'hasTodaysItems' : false,
				'overdueItems' 	: [],
				'hasOverdueItems' : false,
			};

			var today = new Rocketboots.date.Day();
			
			dayObject.todaysItems = this.getTodaysDiaryItems(app.model.get(ModelName.DIARYITEMS).getDiaryItemsForToday(false),true);
			dayObject.overdueItems = this.getPreviousDiaryItems(app.model.get(ModelName.DIARYITEMS).getPreviousDiaryItems(false),true);
			dayObject.upcomingItems = this.getUpcomingDiaryItems(app.model.get(ModelName.DIARYITEMS).getUpcomingDiaryItems(false));
			
			dayObject.hasTodaysItems = dayObject.todaysItems.length > 0 ? true : false;
			dayObject.hasOverdueItems = dayObject.overdueItems.length > 0 ? true : false;
			dayObject.hasUpcomingItems = dayObject.upcomingItems.length > 0 ? true : false;
			
			return dayObject;
		},

		// sort by:
		// 1. Locked events - all day, alphabetically
		// 2. Locked events - by start time
		// 3. Student events - all day, alphabetically
		// 4. Student events - by start time
		'sortEventsByLockedUnlocked' : function(events, diaryItems, calendarDay) {
			var sortedEvents = [];

			var groups = _.groupBy(events, function(event) {
				return diaryItems.get(event.id).get('locked') ? 'locked' : 'unlocked';
			});

			_.forEach(['locked', 'unlocked'], function(key) {
				if (key in groups) {
					groups[key] = util.sortEventsByTime(groups[key], diaryItems, calendarDay);
					sortedEvents = sortedEvents.concat(groups[key]);
				}
			});

			return sortedEvents;
		},

		'sortEventsByTime': function(events, diaryItems, calendarDay) {
			var midnight = "0000";
			var sortedEvents = [];

			var groups = _.groupBy(events, function(event) {
				var diaryItem = diaryItems.get(event.id);
				var startTime = null;

				if (diaryItem.get('startTime')){
					startTime = diaryItem.get('startTime');
				}

				return startTime != null ? startTime.toInteger() : midnight;
			});

			var times = _.sortBy(_.keys(groups), function(time) {
				return parseInt(time, 10);
			});

			_.forEach(times, function(startTime) {
				groups[startTime] = _.sortBy(groups[startTime], function(event) {
					return event.title;
				});
				sortedEvents = sortedEvents.concat(groups[startTime]);
			});

			return sortedEvents;
		},

		'getEventTitle': function(diaryItem) {
			var title = diaryItem.get('title');
			var startEndTime = util.formatStartEndTime(diaryItem.get('startTime'), diaryItem.get('endTime'), " to ");
			if (startEndTime.length) {
				title += " " + startEndTime;
			}
			return title;
		},

		/**
		 * Map a diary item model into a basic JavaScript object.
		 * @param {!diary.model.DiaryItem} diaryItem The diary item model to be mapped.
		 * @param {?Rocketboots.date.Day} calendarDay The date that is known as current while processing the diary items
		 * @param {?Rocketboots.date.Time} startTime The time for which the diary item is being checked
		 * @param {?Rocketboots.date.Time} endTime The time to when the diary item is being checked
		 * @returns {Object} The mapped result of a diary item.
		 */
		'mapDiaryItem' : function (diaryItem, calendarDay, startTime, endTime) {
			var type = null,
				noteEntries = null,
				assignedNoteUsers = [],
				completed = false,
				attachments = [],
				timeLabel = null,
				moveable = false,
				creatorUserName = null,
				due = null;
			
			var today = new Rocketboots.date.Day();
			var isAssignedToday = false;
			var isDueToday = false;
			var hasNotes = false;
			var hasEvents = false;
			var hasTasks = false;
			var hasMerits = false;
			var isAssignedNote = false;
			var dueRelativeDay = null;
			var assignedRelativeDay = null;
			
			if (diaryItem instanceof diary.model.Task) {
				type = diaryItem.get('type').toLowerCase();
				dueRelativeDay = diaryItem.get('dueDay');
				assignedRelativeDay = diaryItem.get('assignedDay');
				moveable = true;
				hasTasks = true;
				isAssignedToday = diaryItem.get('assignedDay') ? (diaryItem.get('assignedDay').getKey() == today.getKey()) : false;
				isDueToday = diaryItem.get('dueDay') ? (diaryItem.get('dueDay').getKey() == today.getKey()) : false;
				completed = diaryItem.isCompleted();
				
				if(diaryItem.get('createdBy') != app.model.get(ModelName.STUDENTPROFILE).get('UniqueID')){
					creatorUserName = diaryItem.get('creatorUserName');
				}
				
				if (calendarDay) {
					// the start time and end time will only be taken into consideration if the
					// they are specifed otherwise the check will be performed on the day
					due = diaryItem.isDueWithinTimeFrame(calendarDay,
														 startTime,
														 endTime);

				}
			} else if (diaryItem instanceof diary.model.Merit) {
				type = diaryItem.get('type').toLowerCase();
				dueRelativeDay = diaryItem.get('dueDay');
				assignedRelativeDay = diaryItem.get('assignedDay');
				moveable = true;
				hasTasks = false;
				hasMerits = true;
				isAssignedToday = diaryItem.get('assignedDay') ? (diaryItem.get('assignedDay').getKey() == today.getKey()) : false;
				isDueToday = diaryItem.get('dueDay') ? (diaryItem.get('dueDay').getKey() == today.getKey()) : false;
				
				if(diaryItem.get('createdBy') != app.model.get(ModelName.STUDENTPROFILE).get('UniqueID')){
					creatorUserName = diaryItem.get('creatorUserName');
				}
			} else if (diaryItem instanceof diary.model.Event) {
				//type = diaryItem.get('locked') ? 'lockedEvent' : 'event';
				//To fix issue EZTEST-384
				type = 'event';
				dueRelativeDay = diaryItem.get('startDay');
				assignedRelativeDay = diaryItem.get('startDay');
				hasEvents = true;
				isAssignedToday = diaryItem.get('startDay') ? (diaryItem.get('startDay').getKey() == today.getKey()) : false;
				isDueToday = diaryItem.get('startDay') ? (diaryItem.get('startDay').getKey() == today.getKey()) : false;
				timeLabel = util.formatStartEndTime(diaryItem.get('startTime'), diaryItem.get('endTime'), " - ");
			} else if (diaryItem instanceof diary.model.Message) {
				// preserve camel case
				type = 'messageFrom' + diaryItem.get('messageFrom').toUpperCase().substr(0, 1) + diaryItem.get('messageFrom').toLowerCase().substr(1);
			} else if (diaryItem instanceof diary.model.Note) {
				type = 'note';
				dueRelativeDay = diaryItem.get('atDay');
				assignedRelativeDay = diaryItem.get('atDay');
				hasNotes = true;
				isAssignedToday = diaryItem.get('atDay') ? (diaryItem.get('atDay').getKey() == today.getKey()) : false;
				
				if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
					var noteUsers = getDiaryItemUser(diaryItem.get("uniqueId"));
					if(diaryItem.get('classId') && diaryItem.get('classId') != 0){
						//console.log("note title : "+ diaryItem.get("title"));
						noteUsers = getDiaryItemUser(diaryItem.get("uniqueId"));
					}
					
					for (var index = 0; index < noteUsers.length; index++) {
						var user = noteUsers.at(index);
						var userName =  user.get('name')+ ' ' + user.get('lastName');
						
						assignedNoteUsers.push({
							'name' : userName
						});
					}
					
					isAssignedNote = (noteUsers.length > 0) ? true : false;
					
				}
				
				noteEntries = [];
				_.forEach(diaryItem.get('entries').toArray(), function(entry) {
					var timestamp = entry.get('timestamp');
					var timestampString = util.formatNoteDate(timestamp)
						+ " " + util.formatTime(timestamp);
					noteEntries.push({
						'timestamp' : timestampString,
						'text': entry.get('text')
					});
				});
				
				if(diaryItem.get('createdBy') != app.model.get(ModelName.STUDENTPROFILE).get('UniqueID')){
					creatorUserName = diaryItem.get('creatorUserName');
				}
			}

			// attachments
			if(diaryItem && diaryItem!= "undefined" && diaryItem.length>0 ){
			
				_.forEach(diaryItem.get('attachments').toArray(), function (attachment) {
					attachments.push({
						'id': attachment.id,
						'displayName': attachment.get('displayName'),
						'iconSource': diary.view.air.ViewFilesystemUtil.loadFileIcon(attachment.get('localFilePath'), {width:16, height:16}).source
					});
				});
			}
			var subjectColor="";
			
			var isMerit = false, isStudentMerit = false;
			
			if(type == 'merit'){
				isMerit = true;
			}
			
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				isStudentMerit = false;
			} else {
				isStudentMerit = true;
			}
			
			return {
				'id' : diaryItem.get('id'),
				'uniqueId' : diaryItem.get('uniqueId'),
				'type' : type,
				'isMerit' : isMerit,
				'isStudentMerit' : isStudentMerit,
				'isClassMeritActive' : type == 'merit' ? diaryItem.get('isMeritActive') : 1,
				'isTaskMeritActive' : (diaryItem && app.model.get(ModelName.DIARYITEMTYPES).isTypeActive("Merit")) ? diaryItem.get('isMeritActive') : 0,
				'enableMerit' : app.model.get(ModelName.DIARYITEMTYPES).isTypeActive("Merit"),
				'meritComment' : diaryItem.get('meritComment'),
				'meritTitle' : diaryItem.get('passtype_name'),
				'IconSmall' : diaryItem.get('IconSmall'),
				'IconBig' : isStudentMerit && diaryItem.get('isMeritActive') ? diaryItem.get('IconBig') : null,
				'dairyItemTypeName' : diaryItem.get('dairyItemTypeName'),
				'assignedDay' : diaryItem.get('assignedDay') ? util.formatShortDateString(diaryItem.get('assignedDay')) : null,
				'assignedDate' : diaryItem.get('assignedDay') ? util.formatShortDate(diaryItem.get('assignedDay')) : null,
				'assignedTime' : diaryItem.get('assignedTime') ? util.formatTime(diaryItem.get('assignedTime')) : null,
                'assignedRelativeDay' : assignedRelativeDay ? util.formatShortDateRelativeToTodayString(assignedRelativeDay) : null,
				'dueDay' : diaryItem.get('dueDay') ? util.formatShortDateString(diaryItem.get('dueDay')) : null,
				'dueDate' : diaryItem.get('dueDay') ? util.formatShortDate(diaryItem.get('dueDay')) : null,
				'dueTime' : diaryItem.get('dueTime') ? util.formatTime(diaryItem.get('dueTime')) : null,
                'dueRelativeDay' : dueRelativeDay ? util.formatShortDateRelativeToTodayString(dueRelativeDay) : null,
				'atDay' : diaryItem.get('atDay') ? util.formatShortDateString(diaryItem.get('atDay')) : null,
				'startDay' : diaryItem.get('startDay') ? util.formatShortDateString(diaryItem.get('startDay')) : null,
				'eventDueRelativeDay' : diaryItem.get('startDay') ? util.formatShortDateRelativeToTodayString(diaryItem.get('startDay')) : null,
				'isAssignedToday' : isAssignedToday,
				'isDueToday' : isDueToday,
				'isCompleted' : diaryItem.get('progress') == 100 ? true : false,
				'endTime' : diaryItem.get('endTime') ? util.formatTime(diaryItem.get('endTime')) : null,
				'atTime' : diaryItem.get('atTime') ? util.formatTime(diaryItem.get('atTime')) : null,
				'due' : due,
				'time' : diaryItem.get('startTime') ? util.formatTime(diaryItem.get('startTime')) : null,
				'completed' : completed,
				'responseRequired' : diaryItem instanceof diary.model.Message ? diaryItem.get('responseRequired') : null,
				// TODO : remove the white CSS-class for items without a subject (currently included because popups & week/month/glance views don't support noColor)
				'subjectColour' : diaryItem.get('subject') ? diaryItem.get('subject').get('colour') : 'gray',
				'SubjectClass' : diaryItem.get('subject') ? getkey(diaryItem.get('subject').get('colour').toLowerCase()) : 'white noColor',
				'title' : diaryItem.get('title'),
				'progress' : diaryItem.get('progress'),
				'subject' : diaryItem.get('subject') ? diaryItem.get('subject').get('title') : null,
				'description' : diaryItem.get('description'),
				'messageFromText' : diaryItem instanceof diary.model.Message ? diaryItem.get('messageFromText') : null,
				'moveable' : moveable,
				'timeLabel' : timeLabel,
				'message' : (type == 'messageFromTeacher' || type == 'messageFromParent'),
				'note' : (type == 'note'),
				'noteEntries' : noteEntries,
				'assignedNoteUsers' : assignedNoteUsers,
				'attachments': attachments,
				'hasAttachments': attachments.length > 0,
				'locked' : diaryItem.get('locked'),
				'AssignedBy' : diaryItem.get('AssignedBy'),
				'showDue' : diaryItem.get('showDue'),
				'showAssigned' : diaryItem.get('showAssigned'),
				'showNotifications' : diaryItem.get('showNotifications'),
				'hasNotes' : hasNotes,
				'hasEvents': hasEvents,
				'hasTasks' : hasTasks,
				'hasMerits' : hasMerits,
				'classId' : diaryItem.get('subject') ? diaryItem.get('subject').get('classUniqueID') : null, 
				'priority' : diaryItem.get('priority'),
				'webLinkDesc' : diaryItem.get('webLinkDesc'),
				'isAssignedNote' : isAssignedNote,
				'creatorUserName' : creatorUserName,
				'isMinorEvent' : diaryItem.get('priority') == 0 ? true : false,
				'assignedYear' : diaryItem.get('assignedDay') ? Rocketboots.date.Day.parseDayKey(diaryItem.get('assignedDay').getKey()).year : null,
				'dueYear' : diaryItem.get('dueDay') ? Rocketboots.date.Day.parseDayKey(diaryItem.get('dueDay').getKey()).year : null
			};
		},

		/**
		 * Map from a collection of diary items to objects to be passed into Mustache,
		 * based on a comma-separated list of diary item IDs. This comma-separated list
		 * is typically used when a sub-set of diary items is needed to be recorded
		 * in an HTML attribute (text-only).
		 * @param {diary.collection.DiaryItemCollection} diaryItems A collection of dairy items to map to objects.
		 * @param {string} diaryItemIdList A comma-separated list of
         * @param {?Rocketboots.date.Day} calendarDay The day for which the popup is being displayed
         * @param {?Rocketboots.date.Time} startTime The start of the period, if any, for which the popup is being displayed
         * @param {?Rocketboots.date.Time} endTime The end of the period, if any, for which the popup is being displayed
		 * @return {array[Object]} Returns the list of mapped items
		 */
		'mapTasksForPopup' : function (diaryItems, diaryItemIdList, calendarDay, startTime, endTime) {

			var mappedItems = [],
				diaryItemIds = diaryItemIdList.split(',');

			_.forEach(diaryItemIds, function (diaryItemId) {

				if (diaryItemId) {
					mappedItems.push(util.mapDiaryItem(diaryItems.get(diaryItemId), calendarDay, startTime, endTime));
				}

			});

			return mappedItems;

		},

		/**
		 * Returns the start time of a diary item (if specified), otherwise returns the start time of the period
		 * the diary item is associated with
		 *
		 * @param {diary.model.DiaryItem} diaryItem The diary item
		 */
		'getDiaryItemStartTime' : function(diaryItem) {
			var startTime = diaryItem.get('startTime'),
				periodIndex = diaryItem.get('period');

			if (startTime == null && periodIndex != null) {
				// figure out the start time from the period ...
				var date = null,
					calendarModel = app.model.get(ModelName.CALENDAR),
					calendarDay = null,
					period = null;

				if (diaryItem instanceof diary.model.Note || diaryItem instanceof diary.model.Message) {

                    date = diaryItem.get('atDay');

                } else if (diaryItem instanceof diary.model.Task || diaryItem instanceof diary.model.Merit) {

                    date = diaryItem.get('assignedDay');

                } else if (diaryItem instanceof diary.model.Event) {

                    date = diaryItem.get('startDay');

                } else {

                    throw new Error ("Diary Item is not supported");

                }

				calendarDay = calendarModel.getDay(date.getMonth() + 1, date.getDayInMonth(), date.getFullYear());
				period = calendarDay.get('timetable').periodsForCycleDay(calendarDay.get('cycleDay')).get(periodIndex);

				if (period && period.get('startTime')) {
					startTime = period.get('startTime');
				}
			}
			return startTime;
		},

		'mapTasksForTasksAssignedPanel' : function(diaryItems) {
			var includeCompleted = true,
				today = new Rocketboots.date.Day(),
				yesterday = new Rocketboots.date.Day().addDays(-1),
				dayBeforeYesterday = new Rocketboots.date.Day().addDays(-2),
				lastWeekStart = new Rocketboots.date.Day().addDays(-8),
				earlierEnd = new Rocketboots.date.Day().addDays(-9),
				startOfTime = new Rocketboots.date.Day(1970, 1, 1),
				taskGroups = [{
					label: "today",
					expanded: true,
					startDate: today,
					endDate: today,
                    showDay: false
				},{
					label: "yesterday",
					expanded: false,
					startDate: yesterday,
					endDate: yesterday,
                    showDay: false
				},{
					label: "last week",
					expanded: false,
					startDate: lastWeekStart,
					endDate: dayBeforeYesterday,
                    showDay: true
				},{
					label: "earlier",
					expanded: false,
					startDate: startOfTime,
					endDate: earlierEnd,
                    showDay: true
				}],
				sortComparator = function(task) {
					return ((task.get('assignedDay').getKey()) + ((task.get('assignedTime') != null) ? task.get('assignedTime').toString() : "0000"));
				};

			_.each(taskGroups, function(taskGroup){
				var tasks = diaryItems.getTasksAssignedInDateRange(taskGroup.startDate, taskGroup.endDate, includeCompleted);
				tasks.comparator = sortComparator;
				tasks.sort();
				taskGroup.tasks = _.map(tasks.toArray(), function(task) {
					return util.mapDiaryItem(task);
				});
				taskGroup.hasTasks = taskGroup.tasks.length > 0;
				taskGroup.count = taskGroup.tasks.length;
				taskGroup.expanded = taskGroup.expanded && taskGroup.hasTasks;
			});

			return {
				'showAssigned' : true,
				'showDue' : false,
				'taskGroups' : taskGroups
			};
		},

		'mapTasksForTasksDuePanel': function(diaryItems) {
			var includeCompleted = true,
				today = new Rocketboots.date.Day(),
				tomorrow = new Rocketboots.date.Day().addDays(1),
				nextDay = new Rocketboots.date.Day().addDays(2),
				endOfNextWeek = new Rocketboots.date.Day().addDays(8),
				currentTime = new Rocketboots.date.Time(),
				midnight = new Rocketboots.date.Time(0, 0),
				taskGroups = [{
					type: "overdue",
					expanded: true,
                    showDay: true
				},{
					type: "today",
					expanded: false,
					startDate: today,
					startTime: currentTime,
					endDate: today,
                    showDay: false
				},{
					type: "tomorrow",
					expanded: false,
					startDate: tomorrow,
					startTime: midnight,
					endDate: tomorrow,
                    showDay: false
				},{
					type: "future",
					label: "next week",
					expanded: false,
					startDate: nextDay,
					startTime: midnight,
					endDate: endOfNextWeek,
                    showDay: true
				}];

			_.each(taskGroups, function(taskGroup){
				var tasks = (taskGroup.startDate && taskGroup.endDate)
							 ? diaryItems.getTasksDueInDateRange(taskGroup.startDate, taskGroup.startTime, taskGroup.endDate, includeCompleted)
							 : diaryItems.getOverdueTasks();

				// sort the tasks in order of due date and due time
				tasks.comparator = function(task) {
					return ((task.get('dueDay').getKey()) + ((task.get('dueTime') != null) ? task.get('dueTime').toString() : "0000"));
				};
				tasks.sort({silent: true});

				// map each task to the view model
				taskGroup.tasks = _.map(tasks.toArray(), function(task) {
					return util.mapDiaryItem(task);
				});

				taskGroup.hasTasks = taskGroup.tasks.length > 0;
				taskGroup.count = taskGroup.tasks.length;
				taskGroup.expanded = taskGroup.expanded && taskGroup.hasTasks;
			});

			return {
				'showAssigned' : false,
				'showDue' : true,
				'taskGroups':taskGroups
			};
		},

		'togglePanel': function (selectedPanelTab) {
			var panelTabContainer = selectedPanelTab.parent(),
				panelName = selectedPanelTab.attr("class").replace("tab", "").trim();

			// always hide all panel contents (relevant contents will be shown later)
			$(".contents > div > div", panelTabContainer).hide();

			// Expand panel
			if (panelTabContainer.hasClass("minimised")) {
				// update container
				panelTabContainer.removeClass("minimised");
				// update tab
				selectedPanelTab.addClass("selected");
				// update panel contents
				$(".contents ." + panelName, panelTabContainer).show();
			}
			// Collapse panel
			else if (selectedPanelTab.hasClass("selected")) {
				// update tab
				selectedPanelTab.removeClass("selected");
				// update container
				panelTabContainer.addClass("minimised");
			}
			// Switch tabs
			else {
				// update tabs
				$("> .tab", panelTabContainer).removeClass("selected");
				selectedPanelTab.addClass("selected");
				// update panel contents
				$(".contents ." + panelName, panelTabContainer).show();
			}

			return {
				'dayViewPanelVisible' : !panelTabContainer.hasClass("minimised"),
				'dayViewActivePanel' : panelTabContainer.hasClass("minimised") ? "" : panelName
			};
		}

	};

	return util;

})();