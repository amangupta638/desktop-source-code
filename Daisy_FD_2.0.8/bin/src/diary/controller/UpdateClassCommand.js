/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: UpdateClassCommand.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
diary = diary || {};

/** @namespace */
diary.controller = diary.controller || {};

/**
 * The UpdateClassCommand class handles updating the subject and room attributes
 * of an existing Class in a given Timetable.
 * 
 * Preconditions:
 * 	- app.locator.getService(ServiceName.TIMETABLEMANAGER)
 * 		::createSubject(subject, success, error)
 * 		::deleteSubject(subject, success, error)
 * 		::updateClassInTimetable(timetable, classModel, success, error)
 * 	- app.model.get(ModelName.SUBJECTS)
 * 
 * Postconditions:
 * 	- subject is created, if necessary
 * 		- TimetableManager::createSubject() is called
 * 		- subject is added to app model, if necessary
 * 	- class is updated
 * 		- TimetableManager::updateClassInTimetable() is called
 * 		- "classUpdated" event is fired
 * 	- subject is deleted, if necessary
 * 		- TimetableManager::deleteSubject() is called
 * 		- subject is removed from app model, if necessary
 * 
 * @constructor
 */
diary.controller.UpdateClassCommand = (function() {

	var _command = function() {
		//console.log('UpdateClassCommand()');

		this._manager = app.locator.getService(ServiceName.TIMETABLEMANAGER);
	};
var updateClassuserEntryBool=0;
	/**
	 * Updates an existing class in a given Timetable.
	 * 
	 * @param {diary.model.Timetable} timetable The timetable to which the class
	 * 		is related
	 * @param {Number} classId The ID of the class to update
	 * @param {String} subjectTitle The updated title of the subject that is
	 * 		related to the class
	 * @param {String} [room] The updated name of the room that is linked to the
	 * 		class
	 */
	_command.prototype.execute = function(timetable, classId, subjectTitle,
			room, subjectColour, subjectId, isAdmin) {
		//console.log('UpdateClassCommand::execute()');
		//console.log("UpdateClassCommand subjectColour="+subjectColour);
		var roomID="";
		var color = subjectColour;

		var command = this,		// reference to this command for sub-functions
			oldClass,			// original Class before modification
			newClass,			// Class to be modified
			oldSubject,			// original Subject associated with Class
			newSubject,			// Subject to be associated with new Class
			subjects = app.model.get(ModelName.SUBJECTS);

		// validate arguments
		if (!timetable) {
			throw new Error("Timetable must be specified");
		}

		if (!classId) {
			throw new Error("Class Identifier must be specified");
		}

		if (!subjectTitle) {
			throw new Error("Subject Title must be specified");
		}

		if (!(timetable instanceof diary.model.Timetable)) {
			throw new Error("Timetable must be of type diary.model.Timetable");
		}

		if (typeof classId !== "number") {
			throw new Error("Class Identifier must be of type number");
		}

		if (typeof subjectTitle !== "string") {
			throw new Error("Subject Title must be of type string");
		}

		if (room && (typeof room !== "string")) {
			throw new Error("Room must be of type string");
		}

		oldClass = timetable.get('classes').get(classId);
		if (!oldClass) {
			throw new Error("Class must be related to Timetable");
		}

		// create a new Subject Model to wrap the new details; this will be
		// passed to the TimetableManager::createSubject(), which will decide
		// whether the Subject already exists (by matching the title) or a new
		// Subject needs to be created
		//alert('subjectId : ' + subjectId);
		if(subjectId){
			newSubject = new diary.model.Subject({
				'id' : subjectId,
				'title' : subjectTitle,
				'colour' : subjectColour || app.model.getNextColour()
			});
		} else {
			newSubject = new diary.model.Subject({
				'title' : subjectTitle,
				'colour' : subjectColour || app.model.getNextColour()
			});
		}
		
		var addRoomSuccess = function(roomId)
		{
			roomID=roomId;

			command._manager.createSubject(newSubject, createSubjectSuccess,
				createSubjectError);
			
		};
		
		var addRoomError = function(message)
		{
			//console.log("add room error");
		};
		/**
		 * TimetableManager::createSubject() success callback
		 * 
		 * @param {diary.model.Subject} subject The "created" Subject; if the
		 * 		Subject already existed, this is a reference to the existing
		 * 		Subject
		 */
		var createSubjectSuccess = function(subject) {
			//console.log('UpdateClassCommand::createSubjectSuccess()');

			// add the Subject to the app model, if necessary
			if (!subjects.get(subject.get('id'))) {
				subjects.add(subject);
			} else {
                // update the model, in case it's out of date
                //subjects.get(subject.get('id')).set(subject.attributes);
                
                subjects.get(subject.get('id')).set('title', subject.attributes.title);
                subjects.get(subject.get('id')).set('colour', subject.attributes.colour);
            }
			//console.log("oldClass");
		    //console.log(oldClass);
			// we want to make our changes to a new instance of the Class
			newClass = oldClass.clone();

			// update the Class attributes; must get the instance of the Subject
			// from the app model to ensure the bindings are maintained

			newClass.set({
				"Code":subjects.get(subject.get('colour')),
				"subject" : subjects.get(subject.get('id')),
				"room" : roomID
			});
			
			
			// get a reference to the old Subject, before it is overwritten by the update command
			oldSubject = oldClass.get('subject').clone();
			
			/**
			 * TimetableManager::updateClassInTimetable() success callback
			 * 
			 * @param {diary.model.Timetable} timetable The Timetable to which
			 * 		the Class is associated
			 * @param {diary.model.Class} classModel The updated Class
			 */
			var updateClassSuccess = function(timetable, classModel) {
				//console.log('UpdateClassCommand::updateClassSuccess()');
				
				
				//if(classModel.id==classId)
				//{
					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.UPDATE_CLASS, classModel);
					var updatedmodel=classModel;
					updatedmodel.room=room;
				// broadcast Class was updated
				app.context.trigger(EventName.CLASSUPDATED, updatedmodel);
				//alert('UPDATE_CLASS issue 2');
				//}
				/*else
				{
					app.context.trigger(EventName.CLASSCREATED, classModel);
                	app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CLASS_CREATED, classModel);
				}*/
				

				
				/**
				 * TimetableManager::deleteClass() success callback
				 * 
				 * @param {Boolean} wasDeleted Flag denoting whether or not the
				 * 		Subject was actually deleted
				 */
				// TODO: pass deleted subject back
				var deleteClassSuccess = function(wasDeleted) {
					//console.log('UpdateClassCommand::deleteSubjectSuccess()');

					// remove the deleted Subject from the app model, if
					// necessary
					if (wasDeleted) {
						app.model.get(ModelName.SUBJECTS).remove(oldSubject);
					}
					//alert("here="+subjectColour);
					//updateclassUser(classModel,subjectColour);
				};

				/**
				 * TimetableManager::deleteClass() error callback
				 * 
				 * @param {String} message The error message
				 */
				var deleteClassError = function(message) {

					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "UpdateClassCommand", "deleteSubjectError", message);
	
				};
				
				var classuserDetail=classModel.attributes;
				classuserDetail.Code=classModel.get('subject').get('colour');
				//alert("updateClassuserEntryBool="+updateClassuserEntryBool);
				if(updateClassuserEntryBool)
				{
					updateClassuserEntry(classuserDetail,classModel.id);
					updateClassuserEntryBool=0;
				}
				else
				{
					deleteClassUser2(classModel.id);
				    addClassuserEntry(classuserDetail,classModel.id);
				}
                    	
						//deleteClassUser2(classModel.id);
						//addClassuserEntry(classuserDetail,classModel.id);
				// check if we need to delete the subject from the list of
				// subjects if it is not being used any more
				command._manager.deleteSubject(oldSubject, deleteClassSuccess,
						deleteClassError);
			};

			/**
			 * TimetableManager::updateClassInTimetable() error callback
			 * 
			 * @param {String} message The error message
			 */
			var updateClassError = function(message) {

				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "UpdateClassCommand", "updateClassError", message);
	
			};
			//console.log("newClass");
			//console.log(newClass);
			// finally, perform the Class update
				//console.log("oldClass");
				//console.log(oldClass);
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"SELECT id, UniqueID from class WHERE cycleDay=:cycleDay AND periodId=:periodId AND subjectId=:subjectId AND RoomId=:RoomId",

				// params
				{
					'cycleDay': oldClass.get('cycleDay'),
					'periodId': oldClass.get('periodId'),
					'subjectId': oldClass.get('subject').get('id'),
					'RoomId': roomID
				},
				
				// success
				function success(statement)
				{
					var values=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    //console.log("values");
                    //console.log(values);
                    if(values.length)
                    {
                    	updateClassuserEntryBool=1;
                    	//updateClassuserEntry(classuserDetail,classModel.id);
                    }
                    else
                    {
                    	//deleteClassUser2(classModel.id);
						//addClassuserEntry(classuserDetail,classModel.id);
                    }
                    
                    var classUniqueId = null;
                    if(values && values[0]){
                    	classUniqueId = values[0].UniqueID;
                    }
                    command._manager.updateClassInTimetable(timetable, newClass,room,isAdmin,classUniqueId,
					updateClassSuccess, updateClassError);
                    
                    var eventParameters = getClassEventParameters(subjectTitle,room,oldClass.get('subject').get('colour'));
	      			//alert("eventParameters : " + JSON.stringify(eventParameters));
	      			FlurryAgent.logEvent("Class Updated", eventParameters);
				},
				
				// error
				function error()
				{
					FlurryAgent.logError("Class Error","Error in updating Class", 1);
					console.log("Failed to update class");
				}
		);
			
		};

		/**
		 * TimetableManager::createSubject() error callback
		 * 
		 * @param {String} message The error message
		 */
		var createSubjectError = function(message) {
		
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "UpdateClassCommand", "createSubjectError", message);
		
		};

		command._manager.addRoomtoTimetable(room,addRoomSuccess,addRoomError);
		
	};

	return _command;

})();
