/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: CreateClassCommand.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
diary = diary || {};

/** @namespace */
diary.controller = diary.controller || {};

/**
 * The CreateClassCommand class handles creating a new Class in a given
 * Timetable.
 * 
 * Preconditions:
 * 	- app.locator.getService(ServiceName.TIMETABLEMANAGER)
 * 		::createSubject(subject, success, error)
 * 		::deleteSubject(subject, success, error)
 * 		::addClassToTimetable(timetable, classModel, success, error)
 * 	- app.model.get(ModelName.SUBJECTS)
 * 
 * Postconditions:
 * 	- subject is created, if necessary
 * 		- TimetableManager::createSubject() is called
 * 		- subject is added to app model, if necessary
 * 	- class is created
 * 		- TimetableManager::addClassToTimetable() is called
 * 		- "classCreated" event is fired
 * 
 * @constructor
 */
diary.controller.CreateClassCommand = (function() {

	var _command = function() {
		//console.log('CreateClassCommand()');

		this._manager = app.locator.getService(ServiceName.TIMETABLEMANAGER);
	};

	/**
	 * Creates a new class in a given Timetable.
	 * 
	 * @param {diary.model.Timetable} timetable The timetable to which the class
	 * 		is to be related
	 * @param {Number} periodId The ID of the period to which the class is to be
	 * 		related
	 * @param {Number} cycleDay The day in the cycle that the class occurs
	 * @param {String} subjectTitle The title of the subject that is to be
	 * 		related to the class
	 * @param {String} [room] The name of the room that is to be linked to the
	 * 		class
	 */
	_command.prototype.execute = function(timetable, periodId, cycleDay,
			subjectTitle, room, subjectColour) {
		//console.log('CreateClassCommand::execute()');
		//console.log("timetable 1=");
		//console.log(timetable);
		//alert("SubjectColor="+subjectColour);
		var roomID="";
		var color = subjectColour;
		//alert("subjectColour="+subjectColour);
		var command = this,		// reference to this command for sub-functions
			newClass,			// the new Class to create
			newSubject,			// Subject to be associated with new Class
			subjects = app.model.get(ModelName.SUBJECTS);

		// validate arguments
		if (!timetable) {
			throw new Error("Timetable must be specified");
		}

		if (!periodId) {
			throw new Error("Period Identifier must be specified");
		}

		if (!cycleDay) {
			throw new Error("Cycle Day must be specified");
		}

		if (!subjectTitle) {
			throw new Error("Subject Title must be specified");
		}
		if (!(timetable instanceof diary.model.Timetable)) {
			throw new Error("Timetable must be of type diary.model.Timetable");
		}

		if (typeof periodId !== "number") {
			throw new Error("Period Identifier must be of type number");
		}

		if (typeof cycleDay !== "number") {
			throw new Error("Cycle Day must be of type number");
		}

		if (typeof subjectTitle !== "string") {
			throw new Error("Subject Title must be of type string");
		}

		if (room && (typeof room !== "string")) {
			throw new Error("Room must be of type string");
		}

		// create a new Subject Model to wrap the new details; this will be
		// passed to the TimetableManager::createSubject(), which will decide
		// whether the Subject already exists (by matching the title) or a new
		// Subject needs to be created
		newSubject = new diary.model.Subject({
			'title' : subjectTitle,
			'colour' : subjectColour || app.model.getNextColour()
		});
		var addRoomSuccess = function(roomId)
		{
			roomID=roomId;
			command._manager.createSubject(newSubject, createSubjectSuccess,
					createSubjectError);
		};
		
		var addRoomError = function(message)
		{
			console.log("add room error");
		};
		/**
		 * TimetableManager::createSubject() error callback
		 * 
		 * @param {String} message The error message
		 */
		var createSubjectError = function(message) {
			/*console.log('CreateClassCommand::createSubjectError(' + message
					+ ')');*/
		};

		/**
		 * TimetableManager::createSubject() success callback
		 * 
		 * @param {diary.model.Subject} subject The "created" Subject; if the
		 * 		Subject already existed, this is a reference to the existing
		 * 		Subject
		 */
		var createSubjectSuccess = function(subject) {
			//console.log('CreateClassCommand::createSubjectSuccess()');
			//console.log(subject.attributes);
			// add the Subject to the app model, if necessary
			if (!subjects.get(subject.get('id'))) {
				//console.log('  adding new subject \'' + subject.get('title') + '\' to app model');
				subjects.add(subject);
			} else {
                // update the model, in case it's out of date
                subjects.get(subject.get('id')).set(subject.attributes);
            }

            // create the new Class Model; must get the instance of the Subject
			// from the app model to ensure the bindings are maintained
			newClass = new diary.model.Class({
				'periodId' : periodId,
				'cycleDay' : cycleDay,
				'subject' : subjects.get(subject.get('id')),
				'Code' : subject.attributes.colour,
				'room' : roomID
			});

			/**
			 * TimetableManager::addClassToTimetable() success callback
			 * 
			 * @param {diary.model.Timetable} timetable The Timetable to which
			 * 		the Class has been associated
			 * @param {diary.model.Class} classModel The updated Class
			 */
			
			var addClassSuccess = function(classId, timetable, classModel) {
				//console.log('UpdateClassCommand::addClassSuccess()');
				//alert("classId="+classId);
				//console.log("classModel=");
				//console.log(classModel);
				//console.log("room="+room);
				var classuserDetail=classModel.attributes;
				classModel.attributes.room=room;
				//console.log("classModel again=");
				//console.log(classModel);
				//.log("classuserDetail");
				//console.log(classuserDetail);
				
				// broadcast Class was updated
				app.context.trigger(EventName.CLASSCREATED, classModel);
                app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CLASS_CREATED,
                    classModel.attributes);
					
				addClassuserEntry(classuserDetail,classId);
			};

			/**
			 * TimetableManager::addClassToTimetable() error callback
			 * 
			 * @param {String} message The error message
			 */
			var addClassError = function(message) {
				FlurryAgent.logError("Class Error", "Error in creating Class",1);
                app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR,
                    'UpdateClassCommand::addClassError(' + message + ')');
			};
			
			// finally, perform the Class creation
			command._manager.addClassToTimetable(timetable, newClass,
					addClassSuccess, addClassError);
			
			var eventParameters = getClassEventParameters(subjectTitle, room, subjectColour);
  			//alert("eventParameters : " + JSON.stringify(eventParameters));
  			FlurryAgent.logEvent("Class Created", eventParameters);
		};
		command._manager.addRoomtoTimetable(room,addRoomSuccess,addRoomError)
		// create a new Subject or retrieve the details of the existing Subject
		
	};

	return _command;

})();
