diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.SearchCommand = (function() {

	var _command = function() {
		//console.log('SearchCommand()');

		this._manager = app.locator.getService(ServiceName.SEARCHMANAGER);
	};

	/**
	 * Starts a search
	 * 
	 * @param {!search} criteria The subject that is to be updated
	 */
	_command.prototype.execute = function(criteria) {
		//console.log('SearchCommand.execute()');
		
		this._manager.searchDiary(criteria, 
								  app.model.get(ModelName.DIARYITEMS),
								  app.model.get(ModelName.SCHOOLPROFILE).get('contentMapping').getArticles(),
								  success, 
								  error);

		function success(searchResult) {
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.SEARCH, searchResult);
			
			// we just raise the event that the search was successful
			app.context.trigger(EventName.SEARCHCOMPLETE, searchResult);
		}

		function error(message) {
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "SearchCommand", message);
	
		}
	};

	return _command;

})();
