diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.FetchNewsAnnouncementCommand = (function() {

	var _command = function() {
		this._newsAnnouncementManager = app.locator.getService(ServiceName.NEWSANNOUNCEMENTMANAGER);
	};

	//function to fetch news and announcements.
	_command.prototype.execute = function (doneCallback) {
		doneCallback = doneCallback || $.noop;
	    this._newsAnnouncementManager.getNewsAndAnnouncements(success, error);

		// the function that is to be executed on successful operation
		function success(newsAndAnnouncements) {
			app.model.set(ModelName.NEWSANNOUNCEMENTS, newsAndAnnouncements);
			doneCallback && doneCallback();
		}

		// the function that is to be executed on failure of the operation
		function error(message) {
			//alert("alert : error fetching news");
			//FlurryAgent.logError("Inbox Item Error","Error in fetching Inbox Items", 1);
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchNewsAnnouncementCommand", message);
			doneCallback && doneCallback();
		}
		
	};

	return _command;

})();
