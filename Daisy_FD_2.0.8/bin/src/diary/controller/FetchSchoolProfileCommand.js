diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.FetchSchoolProfileCommand = (function() {

	var _command = function() {
		//console.log('FetchSchoolProfileCommand()');

		this._schoolManager = app.locator.getService(ServiceName.SCHOOLMANAGER);
	};

	_command.prototype.execute = function (doneCallback) {
		
		doneCallback = doneCallback || $.noop;

		this._schoolManager.getSchoolProfile(success, error);

		// the function that is to be executed on successful operation
		function success(schoolProfile) {
			console.log("FetchSchoolProfileCommand success: "+JSON.stringify(schoolProfile));
			app.model.set(ModelName.SCHOOLPROFILE, schoolProfile);
			//console.log("app.model.set(ModelName.SCHOOLPROFILE : "+JSON.stringify(app.model.get(ModelName.SCHOOLPROFILE)));
			doneCallback();
		}

		// the function that is to be executed on failure of the operation
		function error(message) {
			//alert("alert : FetchSchoolProfileCommand ERROR: ");
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchSchoolProfileCommand", message);
			doneCallback();
			
		}
	};

	return _command;

})();
