diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.GenerateCalendarCommand = (function() {
	
	var _command = function() {
		//console.log('GenerateCalendarCommand()');

		this._calendarManager = app.locator.getService(ServiceName.CALENDARMANAGER);
	};

	_command.prototype.execute = function(year, doneCallback) {
        //console.log('GenerateCalendarCommand.execute for ('+ year+ ')');

        // get the calendar for the current year.
        // NOTE - for the time being it is going to be assumed that ONLY one calendar
        // exists for a scholastic year
        this._calendarManager.getCalendar(year, app.model.get(ModelName.SUBJECTS), success, error);

        // the function that is to be executed on success retrieval of the calendar
        function success(calendar) {
        	//console.log("GenerateCalenderCommand: success: calendar JSON object->"+JSON.stringify(calendar));
            var cal = app.model.get(ModelName.CALENDAR);
            cal.set('year', calendar.get('year'));
            cal.get('timetables').reset(calendar.get('timetables').toArray());
            cal.get('calendarDays').reset(calendar.get('calendarDays').toArray());
            doneCallback && doneCallback();
            cal = null;
        }
		
		// the function that is to be executed on failing to retrieve the calendar
		function error(message) {
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "GenerateCalendarCommand", message);
			doneCallback && doneCallback();
		}
	};

	return _command;

})();
