diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.timetableSyncData = (function () {
    // updated by aayshi


    var _command = function () {

        this._schoolManager = app.locator.getService(ServiceName.SCHOOLPROFILEDELEGATE);

    };


    _command.prototype.execute = function (doneCallback) {
		var resultsMain = "";
        doneCallback = doneCallback || $.noop;

        this._schoolManager.getschoolid(success, error);



        //var timetablids = [];


        function checkcontent(scid, lastsyncing) {


            var url = SystemSettings.SERVERURL + "/timetablesync";
            var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
            
            var ajaxType = 'POST';
            var user_type = "Student";
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				user_type = "Teacher";
			} 
			
            var dataSend = {
                "schoolId": scid,
                "lastSynch": lastsyncing,
                "user_id": currentUserId,
                "type": user_type
            }

            dataSend = JSON.stringify(dataSend);

            $.ajax({
                'url': url,
                'type': ajaxType,
                'timeout' : 0,
                'data': dataSend,
                'dataType': "json",
                'success': function (data) {
					    var icr = 0;
                    console.log("success in retrievig timetable");
					dataSend = null;
                    resultsMain = data.details; 
                    var deleted = data.deleted; 
                    
                    $.each(deleted, function (index, element) {
                    	deleteTimetable(element.Id);
                    });
                    
                    $.each(resultsMain, function (index, element) { 
                       

                        var semname = element.name;
                        var CampusCode = element.CampusCode;
                        var unID = element.id;
						var cycleLength=element.cycleLength;
						var cycleLabel=element.cycleLabel;
						
						
						var endDates=element.endDate;
						
						if(endDates!=null)
						{
						endDates=endDates.split("-");
						
						var endDate=endDates[0]+""+endDates[1]+""+endDates[2];
						}else{
							var endDate=endDates;
							
							}
						var startDates=element.startDate;
						startDates=startDates.split("-");
						var startDate=startDates[0]+""+startDates[1]+""+startDates[2];
						
						var cycleDaysArar=new Array();
						var ob=new Object();
						var cycleDaysstat=element.timeTableCycleDetails;
						$.each(cycleDaysstat,function(index,element){
							
							ob.label=element.cycleLabel;
							ob.shortLabel=element.shortLabel;
							
							cycleDaysArar.push(JSON.stringify(ob))
							
							})
						var cycleDays="["+cycleDaysArar+"]";
				


                        app.locator.getService(ServiceName.APPDATABASE).runQuery(
                            // query
                            "SELECT * FROM  timetable WHERE CampusCode = :CampusCode AND uniqueID = :uniqueID ",

                            // params
                            {
                                'CampusCode': CampusCode,
                                'uniqueID' : unID
                            },

                            // success

                            function (statement) {
								
								
								var datas = statement.getResult().data;
									 
								if(datas && datas != null)
								{
									//alert('updateTimetable');
									
								updateTimetable(cycleLength,cycleLabel,endDate,startDate,cycleDays,unID,semname,CampusCode);			
														
								}else{
									//alert('insertTimetable');
									insertTimetable(cycleLength,cycleLabel,endDate,startDate,cycleDays,unID,semname,CampusCode);	
								}
								datas = null;
							
								
							},

                            // error

                            function error() {
                               
                            }
                        );

                    });

                    app.context.trigger(EventName.PERIODSYNC);


icr++;
 if (icr > resultsMain.length - 1) {




                            var datnow = new String(new Date().getDate());
                            var yearnow = new String(new Date().getFullYear());
                            var monthnow = new String(((new Date().getMonth()) + 1));


                            var hoursnow = new String(new Date().getHours());
                            var minssnow = new String(new Date().getHours());
                            var secssnow = new String(new Date().getHours());
                            if (datnow.length == 1)
                                if (datnow.length == 1) {
                                    datnow = "0" + datnow
                                }

                            if (monthnow.length == 1) {
                                monthnow = "0" + monthnow
                            }

                            if (hoursnow.length == 1) {
                                hoursnow = "0" + hoursnow
                            }
                            if (minssnow.length == 1) {
                                minssnow = "0" + minssnow
                            }
                            if (secssnow.length == 1) {
                                secssnow = "0" + secssnow
                            }



                            timenow = yearnow + "-" + monthnow + "-" + datnow + " " + hoursnow + ":" + minssnow + ":" + secssnow;



                            app.locator.getService(ServiceName.APPDATABASE).runQuery(
                                // query


                                "UPDATE lastSynch " +
                                "SET contentsync = :contentsync " +
                                "WHERE tableName = :tableName",

                                // params
                                {
                                    'contentsync': timenow,
                                    'tableName': "timetableSyncing"

                                },

                                // success

                                function (statement) {
									timenow = null;
									datnow = null;
									yearnow = null;
									monthnow = null;
									hoursnow = null;
									minssnow = null;
									secssnow = null;
									
                                    doneCallback();

                                    //console.log("last sync time stored in db"+timenow)
                                },

                                // error

                                function error() {
                                    //console.log("last sync time not stored in db"+timenow)
                                }
                            );



                        }
                    resultsMain = null;
                    //setNoOfCampuses();
                	//console.log("call triggerEventPeriodSync");
                	//setTimeout(function(){triggerEventPeriodSync();},1000);
                    //triggerEventPeriodSync();
					
                },
               
                'error': function (response) {

                    console.log("error in fetching timetablesync : " + JSON.stringify(response))
	            	FlurryAgent.logError("Timetable Sync Error", "Error in fetching data from timetablesync web service",1); 
                    dataDownloadError();
                }
            });


        }

        function updateTimetable(cycleLength,cycleLabel,endDate,startDate,cycleDays,unID,semname,CampusCode) {
			
			

            app.locator.getService(ServiceName.APPDATABASE).runQuery(
			
			    "UPDATE timetable " +
				"SET cycleLength = :cycleLength , cycleLabel = :cycleLabel, startDate = :startDate , endDate = :endDate , cycleDays = :cycleDays , name = :name " +
				"WHERE CampusCode = :CampusCode AND uniqueID = :uniqueID",
			
                // params
                {
                    'cycleLength': cycleLength,
                    'cycleLabel': cycleLabel,
                    'endDate': endDate,
                    'startDate': startDate,
					'cycleDays':cycleDays,
                    'uniqueID': unID,
                    'CampusCode': CampusCode,
					'name':semname

                },

                // success

                function (statement) {

                    console.log("suc in updating timetable sync" + statement)

                },

                // error

                function error(statement) {
                    console.log("error in upadting timetable sync   " + statement)
                }
            );
        
			
			}


        function insertTimetable(cycleLength,cycleLabel,endDate,startDate,cycleDays,unID,semname,CampusCode) {
			
			

            app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "INSERT INTO timetable (cycleLength, cycleLabel, endDate, startDate,cycleDays, UniqueID, name, CampusCode)" +
                " VALUES (:cycleLength, :cycleLabel, :endDate, :startDate , :cycleDays,  :UniqueID,   :name, :CampusCode)",

                // params
                {
                     'cycleLength': cycleLength,
                    'cycleLabel': cycleLabel,
                    'endDate': endDate,
                    'startDate': startDate,
					'cycleDays':cycleDays,
                    'UniqueID': unID,
                    'CampusCode': CampusCode,
					'name':semname

                },

                // success

                function (statement) {

                    console.log(statement + "inserting in  timetable sync")
                },

                // error

                function error(statement) {
                    console.log(statement + "error in inserting timetable sync")
                }
            );
        
			
			}

        // the function that is to be executed on successful operation

        function success(scid) {

            if (scid != null) {
                app.locator.getService(ServiceName.APPDATABASE).runQuery(

                    // query
                    "SELECT * FROM lastSynch WHERE (tableName) = :tablename ",

                    // params
                    {
                        'tablename': "timetableSyncing"
                    },

                    // success

                    function (statement) {
                        var result = statement.getResult().data;




                        lastsyncing = result[0].contentsync


                        checkcontent(scid, lastsyncing)
						result = null;




                    },

                    // error

                    function (statement) {
                        //alert("error")
                    }




                );
            } else {}
        }

        // the function that is to be executed on failure of the operation

        function error(message) {


        }


    };




    return _command;

})();