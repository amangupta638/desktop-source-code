diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.RegisterUserCommand = (function() {

	var _command = function() {
		//console.log('RegisterTokenCommand()');

		this._manager = app.locator.getService(ServiceName.REGISTRATIONMANAGER);
	};

	/**
	 * Registers a token with the cloud service
	 * 
	 * @param {token} String The token to register
	 */
	_command.prototype.execute = function(email, password, domain) {
		

		this._manager.registerApp(email, password, domain, success, error);

		function success(result) {
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.REGISTER_USER, result);
			app.context.trigger(EventName.USERREGISTERED);
		}
		function error(message) {
		
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "RegisterUserCommand", message);
			app.context.trigger(EventName.USERREGISTRATIONFAILED, message);

		}
	};

	return _command;

})();
