diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.LoadManifestCommand = (function() {

	var _command = function() {
		//console.log('LoadManifestCommand()');

		this._appState = app.model.get(ModelName.APPLICATIONSTATE);
		this._manifestManager = app.locator.getService(ServiceName.MANIFESTMANAGER);
		this._timetableManager = app.locator.getService(ServiceName.TIMETABLEMANAGER);
		this._dayOfNoteManager = app.locator.getService(ServiceName.DAYOFNOTEMANAGER);
		this._diaryItemManager = app.locator.getService(ServiceName.DIARYITEMMANAGER);
		this._systemConfigManager = app.locator.getService(ServiceName.SYSTEMCONFIGURATIONMANAGER);
 	};


	_command.prototype.execute = function (doneCallback) {
		console.log('LoadManifestCommand.execute()');

		getReports();
		app.context.trigger(EventName.TIMETABLESYNC);
		app.context.trigger(EventName.DATACONTENT);
		
		// Ensure done callback is defined
		doneCallback = doneCallback || $.noop;

		var command = this,
			manifestFile,
			manifest,
			useLocalPaths = false,
			applicationState = app.model.get(ModelName.APPLICATIONSTATE),
			applicationMode = applicationState.get('mode');

		var updateTimetables = function(manifest, successCallback, errorCallback) {
			
			//app.context.trigger(EventName.TIMETABLESYNC);
			//app.context.trigger(EventName.PERIODSYNC);
			
			//var execSequentially = $.Deferred();
			//execSequentially.done(triggerEventTimeTableSync,triggerEventPeriodSync);
			
			//execSequentially.resolve();
			
			//app.context.trigger(EventName.GENERATECALENDAR, SystemSettings.HARDCODDEDYEAR);
			
			if (!_.isFunction(successCallback) || !_.isFunction(errorCallback)) {
				throw new Error("Success and error callbacks must be functions");
			}

			command._timetableManager.getAllTimetables(
				/* for each timetable, if it doesn't exist in the db, add it now */
				function (existingTimetables) {

					// TODO: modify to handle timetable modification and deletion (only addition is handled here, keyed on start date)

					//console.log("Found ", existingTimetables.size(), " existing timetables");

					var missingTimetables = manifest.get('timetables').filter(function (timetable) {
						var newTimetableName = timetable.get('name');
						return !_.find(existingTimetables.toArray(), function(timetableRecord) {
							// Initial Logic : if start dates are the same then we assume this is a duplicate
							//Now checking for duplicate record as per name.
							var existingTimetableName = timetableRecord.get('name');
							return existingTimetableName == newTimetableName;
						});
					});

					// TODO: If any timetables already existed, display the following warning
					/*
					if (missingTimetables.length > 0 && existingTimetables.length > 0) {
						app.view.dialogView.showWindow({
							title: "Content Update",
							message: "An important update to your timetable has been released. " +
								"After this update has been applied you will have to re-enter your subjects. " +
								"This update is not optional.",
							isError: false,
							actions: { close: true },
							closeCallback: function () {
								//alert("LoadManifestCommand updateTimetable - dialog closed");
							}
						});
					}
					*/

					// add missing timetables
					var opRunner = new Rocketboots.util.AsyncOperationRunner(
						_.map(missingTimetables, function (missingTimetable) {
							return {
								exec: command._timetableManager.addTimetable,
								context: command._timetableManager,
								args: [missingTimetable]
							};
						}),
						{
							allSuccessful: successCallback,
							anyError: errorCallback
						}
					);
					opRunner.run();
				},
				function (msg) {
					console.log("FetchMaifestCommand.updateTimetables error - " + msg);
					errorCallback(msg);
				});
		};

		var updateDaysOfNote = function(manifest, successCallback, errorCallback) {

			// TODO: check which days have had their cycleDay field updated, and disassociate diary items on those days from their subjects/classes/periods

			command._dayOfNoteManager.saveAllDaysOfNote( manifest.get('daysOfNote'), true,
				successCallback,
				function error(message) {
					console.log("FetchMaifestCommand.daysOfNote - " + message);
				
					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "LoadManifestCommand", "updateDaysOfNote", message);
					errorCallback(message);
				
				}
			);
		};
		
		var updateSchoolEvents = function(manifest, successCallback, errorCallback) {
			command._diaryItemManager.replaceAllLockedEvents( manifest.get('events'),
				successCallback,
				function error(message) {
				console.log("FetchMaifestCommand.replaceAllLockedEvents " + message);
					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "LoadManifestCommand", "replaceAllLockedEvents", message);
					errorCallback(message);
				}
			);
		};

		var updateSystemConfiguration = function(manifest, successCallback, errorCallback) {
			var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
			systemConfig.set('schoolConfigTimestamp', manifest.get('timestamp'));
			command._systemConfigManager.setSystemConfiguration(systemConfig,
				successCallback,
				function error(message) {
					console.log("FetchMaifestCommand.setSystemConfiguration " + message);
				
					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "LoadManifestCommand", "updateSystemConfiguration", message);
					errorCallback(message);
				 
				}
			);
		};

		switch(applicationMode) {

			case diary.model.ApplicationState.MODE.PROOF:
			case diary.model.ApplicationState.MODE.DEMO:
				 // Read Manifest file from build directory	
				//manifestFile = air.File.applicationStorageDirectory.resolvePath("sandbox/manifest.ddm");
				manifestFile = air.File.applicationDirectory.resolvePath("sandbox/manifest.ddm");
				useLocalPaths = true;
				break;

			case diary.model.ApplicationState.MODE.PREVIEW:

				manifestFile = new air.File(applicationState.get('manifestLocation'));
				useLocalPaths = true;
				break;

			default: //case diary.model.ApplicationState.MODE.PRODUCTION:
				// Read Manifest file from build directory
				//manifestFile = air.File.applicationStorageDirectory.resolvePath("config/manifest.ddm");
				manifestFile = air.File.applicationDirectory.resolvePath("config/manifest.ddm");
				break;
		}

		manifest = this._manifestManager.parseManifestFile(manifestFile, useLocalPaths);

		if(manifest) {
			//console.log("Successfully fetched manifest");

			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.LOADED_MANIFEST, manifest);

			app.model.get(ModelName.MANIFEST).set('contentMapping', manifest.get('contentMapping'));
			app.model.get(ModelName.MANIFEST).get('contentPackages').reset(manifest.get('contentPackages').toArray());

			var opRunner = new Rocketboots.util.AsyncOperationRunner(_.map([
				// the order of events
				updateTimetables,
				//updateDaysOfNote,
				//updateSchoolEvents,
				updateSystemConfiguration
			], function (func) {
				// every function requires the manifest as argument
				return {
					exec: func,
					context: command,
					args: [manifest]
				};
			}), {
				allSuccessful: doneCallback,
				anyError: doneCallback
			});

			if (command._appState.get("manifestUpdated")) {
				opRunner.run();
			} else {
				doneCallback();
			}
		} else {
			//console.log("Error while trying to get manifest - " + message);
			app.view.dialogView.showWindow({
				message: "Error reading school configuration manifest",
				isError: true,
				actions: { close : true }
			});

			doneCallback();
		}
	};

	return _command;

})();
