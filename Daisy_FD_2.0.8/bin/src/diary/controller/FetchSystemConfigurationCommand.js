diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.FetchSystemConfigurationCommand = (function() {

	var _command = function() {
		//console.log('FetchSystemConfigurationCommand()');

		this._systemConfigManager = app.locator.getService(ServiceName.SYSTEMCONFIGURATIONMANAGER);
	};

	_command.prototype.execute = function (doneCallback) {
		//console.log('FetchSystemConfigurationCommand.execute()');

		this._systemConfigManager.getSystemConfiguration(success, error);

		// the function that is to be executed on successful operation
		function success(systemConfiguration) {
			app.model.set(ModelName.SYSTEMCONFIGURATION, systemConfiguration);
			doneCallback && doneCallback();
		}

		// the function that is to be executed on failure of the operation
		function error(message) {
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchSystemConfigurationCommand", message);
			doneCallback && doneCallback();
		}
	};

	return _command;

})();
