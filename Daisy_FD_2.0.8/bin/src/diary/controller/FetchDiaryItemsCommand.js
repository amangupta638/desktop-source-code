diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.FetchDiaryItemsCommand = (function() {

	var _command = function() {
		//console.log('FetchDiaryItemsCommand()');

		this._diaryItemManager = app.locator.getService(ServiceName.DIARYITEMMANAGER);
	};

	_command.prototype.execute = function(startDate, endDate, isNote, isTask, isEvent, doneCallback) {
		//alert('FetchDiaryItemsCommand.execute '+ startDate.getKey()+ ' '+ endDate.getKey());

		doneCallback = doneCallback || $.noop;
		
		// get the manager to get all the diary items for the date range
		this._diaryItemManager.getDiaryItemsForDayRange(startDate.getDate(), endDate.getDate(), app.model.get(ModelName.SUBJECTS), isNote, isTask, isEvent, success, error);

		// the function that is to be executed on success retrieval of the diary items
		function success(diaryItems) {
			
			app.model.get(ModelName.DIARYITEMS).reset(diaryItems.toArray());
			
			var overDueTasks = app.model.get(ModelName.DIARYITEMS).getRequiredOverdueTasks();
			var assignedTasks = app.model.get(ModelName.DIARYITEMS).getRequiredAssignedTasks();
			var notifications = app.model.get(ModelName.DIARYITEMS).getNotifications();
			var notificationForToday = app.model.get(ModelName.INBOXITEMS).getUnreadNotifications();
			
			$("#dueCount").html(overDueTasks.length);
			$("#assignedCount").html(assignedTasks.length);
			$("#notificationCount").html(notifications.length + notificationForToday);
			
			//console.log("app.model.get(ModelName.DIARYITEMS) : "+JSON.stringify(app.model.get(ModelName.DIARYITEMS)));
			doneCallback();
		}
		
		// the function that is to be executed on failing to retrieve the diary items
		function error(message) {
			var eventParameters = getNavigationEventParameters();
			FlurryAgent.logError("Diary Item Error","Error in fetching Diary Items", 1);
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchDiaryItemsCommand", message);
			doneCallback();
		}
	};

	return _command;

})();
