diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.UpdateSubjectCommand = (function() {

	var _command = function() {
		//console.log('UpdateSubjectCommand()');

		this._manager = app.locator.getService(ServiceName.TIMETABLEMANAGER);
	};

	/**
	 * Updates a subject
	 * 
	 * @param {!diary.model.Subject} subject The subject that is to be updated
	 */
	_command.prototype.execute = function(subject) {
		//console.log('UpdateSubjectCommand.execute()');

		this._manager.updateSubject(subject, success, error);

		function success(subject) {
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.UPDATE_SUBJECT, subject);
					
			var oldSubject = app.model.get(ModelName.SUBJECTS).get(subject.get('id'));
			try {
				oldSubject.set(subject.attributes);
			}
			catch(e) {
				//console.log(e);
			}
		
		}

		function error(message) {
					
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "UpdateSubjectCommand", message);
			app.context.trigger(EventName.UPDATESUBJECTFAILED, subject.get('id'));
		}
	};

	return _command;

})();
