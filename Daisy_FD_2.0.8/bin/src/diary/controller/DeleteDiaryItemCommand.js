diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.DeleteDiaryItemCommand = (function() {

	var _command = function() {
		//console.log('DeleteDiaryItemCommand()');

		this._manager = app.locator.getService(ServiceName.DIARYITEMMANAGER);
	};

	/**
	 * Deletes a diary item from the system
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem The diary item that is to be removed
	 */
	_command.prototype.execute = function(diaryItem) {
		//console.log('DeleteDiaryItemCommand.execute()');

		this._manager.deleteDiaryItem(diaryItem, success, error);

		function success() {
			openMessageBox("Diary item deleted successfully");
			// remove diary item from app model
			var items = app.model.get(ModelName.DIARYITEMS);
			var item = items.get(diaryItem.get('id'));
			
			var eventParameters = getDiaryItemEventParameters(item);
			//alert("eventParameters : " + JSON.stringify(eventParameters));
			FlurryAgent.logEvent("Diary Item Deleted", eventParameters);
					
			items.remove(item);
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.DIARY_ITEM_DELETED, diaryItem);
            app.context.trigger(EventName.CLEARSEARCHCACHE);
		}

		function error(message) {
			openMessageBox("Error in deleting diary item");
			FlurryAgent.logEvent("Diary Item Error","Error in deleting Diary Item", 1);
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR,
                "DeleteDiaryItemCommand", message);
		}
	};

	return _command;

})();
