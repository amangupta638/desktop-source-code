diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.AppendNoteEntryToNoteCommand = (function() {

	var _command = function() {
		//console.log('AppendNoteEntryToNoteCommand()');

		this._manager = app.locator.getService(ServiceName.DIARYITEMMANAGER);
	};

	/**
	 * Creates a diary item
	 * 
	 * @param {!diary.model.NoteEntry} noteEntry The note entry that is to be appended to the note
	 * @param {!number} noteId The identifier of the note entry for which the note entry is being appended
	 */
	_command.prototype.execute = function(noteEntry, noteId) {
		//console.log('AppendNoteEntryToNoteCommand.execute()');

		// get the diary item (note) with the specified identifier to make sure
		// that we have the note first
		var note = app.model.get(ModelName.DIARYITEMS).get(noteId);
		
		if (!note) {
			throw new Error("Note Identifier is not valid, no note was found with the specified identifier");
		}
		
		// check that we are actually adding the note entry to a note
		// NOTE - I think this would be better of in the manager rather than here
		if (!(note instanceof diary.model.Note)) {
			throw new Error("Note Identifier is not valid, identified Diary Item is not of type Note");
		}
		
		// call the manager to append the given note entry
		//this._manager.appendNoteEntryToNote(noteEntry, noteId, success, error);
		this._manager.appendNoteEntryToNote(getTimestampmilliseconds(),noteId,noteEntry, success, error);
		function success(modifiedNoteEntry) {
			// include the appended note entry to the note
			note.get('entries').add(modifiedNoteEntry);
			app.context.trigger(EventName.NOTEENTRYAPPENDED, noteId);
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.NOTE_ENTRY_ADDED,
                modifiedNoteEntry);
            app.context.trigger(EventName.CLEARSEARCHCACHE);
		}

		function error(message) {
//			console.log("Error while trying to append a Note Entry - " + message);
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR,
                "AppendNoteEntryToNoteCommand:", message);
        }
	};

	return _command;

})();
