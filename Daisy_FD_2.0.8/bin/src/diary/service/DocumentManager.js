/**
 * The manager for indexing Documents files
 *
 * author - Phil Haeusler
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DocumentManager = (function() {

	/**
	 * @constructor
	 */
	var _manager = function() {

		this.documentDelegate = app.locator.getService(ServiceName.DOCUMENTDELEGATE);
		
	};

	_manager.prototype.deleteDocumentsNotInPackages = function(contentPackagesToKeep, successCallback, errorCallback) {
		this.documentDelegate.deleteDocumentsNotInPackages(_.map(contentPackagesToKeep, function(p) { return p.get('package'); }), successCallback, errorCallback);
	}

	/**
	 * Index the all content files
	 */
	_manager.prototype.indexOnStartup = function(successCallback, errorCallback) {

		var manager = this;
		
     	var manifest = app.model.get(ModelName.MANIFEST);
		var contentMapping = manifest.get('contentMapping');
		
		//console.log('contentMapping.get(subItems)');		
		
		var subItems = contentMapping.get('subItems');
		
		var callbacks = new Rocketboots.util.CallbackAggregator(successCallback, errorCallback);

		_.each(subItems, function(subItem) {

			subItem.each(function(contentItem) {

				callbacks.addCall();
				manager.indexContentItem(contentItem, callbacks.getSuccessFunction(), callbacks.getErrorFunction());
				
			});
		});

		callbacks.done();
	};

	/**
	 * Index the all content files
	 */
	_manager.prototype.indexEntirePackage = function(packageId, successCallback, errorCallback) {

		var manager = this,
				subItems = app.model.get(ModelName.MANIFEST).get('contentMapping').get('subItems');

		// Handle return of sub item indexing
		var callbacks = new Rocketboots.util.CallbackAggregator(successCallback, errorCallback);

		_.each(subItems, function(subItem) {

			subItem.each(function(contentItem) {
				
				if (contentItem.get('packageId') == packageId) {
					callbacks.addCall();
					manager.indexContentItem(contentItem, callbacks.getSuccessFunction(), callbacks.getErrorFunction());
				}
				
			});
		});

		callbacks.done();
	};
	
	_manager.prototype.indexContentItem = function(contentItem, successCallback, errorCallback) {

		var manager = this;
		
     	var url = contentItem.get('url');
		
		if (url == diary.model.ContentItem.SCHOOL_INFO_URL) {
			successCallback && successCallback("No need to index the hard-coded SCHOOL_INFO article");
			return;
		}

		var contentFile = new air.File(url);

		//console.log( 'CONTENT: ' + contentItem.get('title') +' ' + contentItem.get('documentId') + ' ' + contentFile.exists);
		
		if (!contentFile.exists) {
			errorCallback && errorCallback("File not found");
			return;
		}
		
		var theDocument = new diary.model.Document({
			type: 'SCHOOL',
			title: contentItem.get('title'),
			documentId: contentItem.get('documentId')						
		});

		var content = "";
		
		var fileStream = new air.FileStream();

		try {
			fileStream.open(contentFile, "read");
			// read the json object that is stored in the given file
			content = fileStream.readUTFBytes(fileStream.bytesAvailable);

		} catch (e) {
			//console.log("DocumentManager::indexContentItem error - ", e);
			content = "";

			errorCallback && errorCallback("Failed to read file");
			return;

		} finally {
			fileStream.close();
		}

		if (content == "") {
			successCallback && successCallback("No content to index");
			return;
		}

		// include the title in the searchable content
		var textualContent = $("<div>" + content + "</div>").text() + " " + contentItem.get('title');

		//console.log(' > length = ' + content.length + ' text = ' + textualContent.length);
	//	console.log(textualContent);

		var wordsToNotIndex = ['the', 'and'];

		var contentWords = textualContent.toLowerCase().split(/[^a-z0-9\']/),

			uniqueWords = _.uniq(contentWords),

			indexWords = _.filter(uniqueWords, function(word) {
				// Don't index 1 or 2 letter words
				if (word.length < 3) return false;

				// Don't index words on our blacklist
				if (wordsToNotIndex.indexOf(word.toLowerCase()) != -1) return false;

				return true;
			}),

			stemmedWords = _.map(indexWords, function(word) {
				return stemmer(word);
			}),

			uniqueStemmedWords = _.uniq(stemmedWords);

		/*
		//console.log('number of words: ' + contentWords.length);
		//console.log('number of unique words: ' + uniqueWords.length);
		//console.log('number of indexWords words: ' + indexWords.length);
		//console.log('number of stemmedWords: ' + stemmedWords.length);
		//console.log('number of uniqueStemmedWords: ' + uniqueStemmedWords.length);
		//console.log(uniqueStemmedWords);
		*/

		manager.documentDelegate.addDocument(theDocument, uniqueStemmedWords, function() {
			//console.log('Document added ' + theDocument.get('title'));
			successCallback && successCallback();
		},
		function(error) {
			//console.log('DocumentManager::indexContentItem error - ' + error);
			errorCallback && errorCallback(error);
		});
	
	};

	// return the generated manifest manager
	return _manager;

})();

