/**
 * The manager that will take care of the schema required for the application.  This
 * will cater to keep the schema up to date according to the version that is being run
 *
 * @author Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DiarySchemaManager = (function() {

	/**
	 * Constructs a service to manage the schema of the local database.
	 * @constructor
	 */
	var _service = function() {

		// get the database that is being used for the application
		this._database = app.locator.getService(ServiceName.APPDATABASE);

	};

	/**
	 * Any schema version before this value will be treated as unsupported -
	 * the application will refuse to update the DB schema.
	 * @type {Number}
	 * @private
	 */
	var _MINIMUM_SUPPORTED_VERSION = 3;
	var _SQL_NO_OP = "SELECT * FROM info";

	/**
	 * Append to this list if you wish to add a new schema version.
	 * If a version is specified as a function, it will be executed just prior to the upgrade
	 * being performed, and its return value is expected to be in the defined object format.
	 * 
	 * @type {Array} List of versions. May be specified as objects or functions. Each version should have a list of query objects.
	 * @private
	 */
	
	
	
	
	///**********************SHRUTI************************//
	var _schema_versions = [

		// Version 3 (earliest supported version)
		{
			
			queries: [
				{ 'sql' : "CREATE TABLE studentProfile (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NULL, " +
					" dateOfBirth VARCHAR(20) NULL, " +
					" addressStreet TEXT(200) NULL, " +
					" addressSuburb TEXT(100) NULL, " +
					" addressState TEXT(100) NULL, " +
					" addressPostcode TEXT(7) NULL, " +
					" telephone TEXT(50) NULL, " +
					" email TEXT(100) NULL, " +
					" grade TEXT(50) NULL, " +
					" teacher TEXT(500) NULL, " +
					" house TEXT(100) NULL, " +
					" houseCaptain TEXT(100) NULL, " +
					" SchoolId INTEGER, " +
					" Country TEXT, " +
					" LastName TEXT, " +
					" IsStudent INTEGER, " +
					" IsTeacher INTEGER, " +
					" IsParent INTEGER, " +
					" ProfileImage TEXT, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" Password TEXT, " +
					" image BLOB, " +
					" isSync INTEGER, " +
					" TimeZone TEXT, " +
					" houseColours TEXT(200) NULL" +
					")",
					'rollback' : "DROP TABLE studentProfile" },

				{ 'sql' : "CREATE TABLE schoolProfile (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NULL, " +
					" schoolLogo TEXT(255) NULL, " +
					" schoolImage TEXT(255) NULL, " +
					" schoolMotto TEXT NULL, " +
					" missionStatement TEXT NULL, " +
					" diaryTitle TEXT(100) NULL, " +
					" includeEmpower INTEGER(1) NOT NULL DEFAULT 0, " +
					" theme TEXT(100) NULL, " +
					" Empower_Type TEXT, " +
					" Allow_Parent INTEGER DEFAULT 0, " +
					" appInstallStatus INTEGER DEFAULT 0, " +
					" AllowAutoUpdates INTEGER, " +
					" RegionalFormat TEXT, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" Address_line_1 TEXT(200), " +
					" Address_line_2 TEXT(200), " +
					" CityTownSuburb TEXT(100), " +
					" PostalCode TEXT(10), " +
					" State TEXT(100), " +
					" Country TEXT(100), " +
					" Number_of_Campuses INTEGER, " +
					" Phone_Number TEXT(30), " +
					" Fax_Number TEXT(30), " +
					" Website_URL TEXT(200), " +
					" schoolBgImage TEXT(255), " +
					" AllowSynch INTEGER(1) " +
					")",
					'rollback' : "DROP TABLE schoolProfile" },

				{ 'sql' : "CREATE TABLE diaryItem (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" type TEXT, " +
					" title TEXT, " +
					" description TEXT, " +
					" created INTEGER, " +
					" assignedDay INTEGER, " +
					" assignedTime INTEGER, " +
					" dueDay INTEGER, " +
					" dueTime INTEGER, " +
					" completed INTEGER, " +
					" subjectId INTEGER, " +
					" startDay INTEGER, " +
					" startTime INTEGER, " +
					" endTime INTEGER, " +
					" locked INTEGER, " +
					" messageFrom TEXT, " +
					" messageFromText TEXT, " +
					" responseRequired INTEGER NOT NULL DEFAULT 0, " +
					" isDelete INTEGER, " +
					" Progress INTEGER, " +
					" endDay DATE, " +
					" SchoolId INTEGER, " +
					" ClassId INTEGER, " +
					" UniqueID INTEGER, " +
					" EstimatedHours TEXT, " +
					" EstimatedMins TEXT, " +
					" AssingedBy  INTEGER, " +
					" lastupdated INTEGER, " +
					" isSync INTEGER, " +
					" priority TEXT, " +
					" UserID INTEGER, " +
					" createdBy INTEGER, " +
					" appType TEXT, " +
					" assignedtoMe INTEGER, " +
					" weight TEXT, " +
					" hideFromDue INTEGER, " +
					" hideFromAssigned INTEGER, " +
					" hideFromNotifications INTEGER, " +
					" onTheDay INTEGER DEFAULT 0, " +
					" webLink TEXT, " +
					" allStudents INTEGER DEFAULT 1, " +
					" allTeachers INTEGER DEFAULT 1, " +
					" type_id INTEGER, " +
					" type_name TEXT, " +
					" creatorUserName TEXT " +
					")",
					'rollback' : "DROP TABLE diaryItem" },

				{ 'sql' : "CREATE TABLE attachment (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" diaryItemId INTEGER NOT NULL, " +
					" originalFilePath TEXT NOT NULL, " +
					" displayName TEXT(100) NOT NULL, " +
					" fileType TEXT(100) NULL, " +
					" userId INTEGER, " +
					" isSync INTEGER, " +
					" UniqueID INTEGER, " +
					" serverPath TEXT, " +
					" isDelete INTEGER DEFAULT 0, " +
					" fileSize INTEGER DEFAULT 0, " +
					" localFileName TEXT(100) NOT NULL UNIQUE, " +
					" dateAttached INTEGER(20)  " +
					")",
					'rollback' : "DROP TABLE attachment" },

				{ 'sql' : "CREATE TABLE subject (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NOT NULL, " +
					" classId INTEGER DEFAULT 0, " +
					" SchoolId INTEGER, " +
					" CampusId INTEGER, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" colour TEXT(100)" +
					")",
					'rollback' : "DROP TABLE subject" },

				{ 'sql' : "CREATE TABLE dayOfNote (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" date INTEGER(20) NOT NULL, " +
					" overrideCycleDay INTEGER(20), " +
					" includeInCycle INTEGER(1), " +
					" isGreyedOut INTEGER(1), " +
					" UniqueID INTEGER, " +
					" SchoolId INTEGER, " +
					" CampusId INTEGER, " +
					" lastupdated INTEGER, " +
					" title TEXT(100), " +
					" overrideCycle INTEGER(20) " +
					" )",
					'rollback' : "DROP TABLE dayOfNote" },

				{ 'sql' : "CREATE TABLE timetable (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" startDate INTEGER(20) NOT NULL, " +
					" name TEXT(100) NULL, " +
					" cycleLabel TEXT(100) NULL, " +
					" cycleDays TEXT NULL, " +
					" TimeTableCycleId INTEGER, " +
					" EndDate INTEGER, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" cycleLength INTEGER(20) NOT NULL " +
					")",
					'rollback' : "DROP TABLE timetable" },

				{ 'sql' : "CREATE TABLE period (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" timetable INTEGER NOT NULL, " +
					" startTime INTEGER(20) NOT NULL, " +
					" endTime INTEGER(20) NULL, " +
					" title TEXT(100) NULL, " +
					" cycleDay INTEGER(20) NULL, " +
					" forTeacher INTEGER DEFAULT 0, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" isBreak INTEGER(1)" +
					")",
					'rollback' : "DROP TABLE period" },

				{ 'sql' : "CREATE TABLE class ( " +
					"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					"timetable INTEGER NOT NULL, " +
					"cycleDay INTEGER(20) NOT NULL, " +
					"periodId INTEGER(20) NOT NULL, " +
					"subjectId INTEGER(20) NOT NULL, " +
					"Name TEXT, " +
					"Code TEXT, " +
					"UniqueID INTEGER, " +
					"RoomId INTEGER, " +
					"isSync INTEGER, " +
					"lastupdated INTEGER, " +
					"room TEXT(100) NULL " +
					")",
					'rollback' : "DROP TABLE class" },

				{ 'sql' : "CREATE TABLE noteEntry (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" noteId INTEGER, " +
					" timestamp INTEGER(20) NOT NULL, " +
					" text TEXT NULL, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" isDelete INTEGER, " +
					" isSynch INTEGER, " +
					" noteUniqueId INTEGER, " +
					" userId INTEGER, " +
					" noteFrom TEXT(100) NULL " +
					")",
					'rollback' : "DROP TABLE noteEntry" },


				{ 'sql' : "CREATE TABLE systemConfig (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" email TEXT(100) NULL, " +
					" lastSchoolProfileCheck INTEGER," +
					" MacId TEXT, " +
					" schoolConfigTimestamp TEXT(100) " +
					")",
					'rollback' : "DROP TABLE systemConfig" },


				{ 'sql' : "CREATE TABLE usageLog (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" time INTEGER(20) NOT NULL, " +
					" type TEXT(100) NOT NULL, " +
					" detail TEXT NULL, " +
					" token TEXT(10) NOT NULL, " +
					" macAddresses TEXT NOT NULL, " +
					" schoolConfiguration TEXT NOT NULL, " +
					" version INTEGER NOT NULL, " +
					" clientVersion TEXT NOT NULL, " +
					" createdDate TEXT, " +
					" synced INTEGER(1) NOT NULL" +
					")",
					'rollback' : "DROP TABLE usageLog" },
					
				{ 'sql' : "CREATE TABLE ClassUser (" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "classId INTEGER, " +
					"userId INTEGER, "+
					"cycleDay INTEGER, "+
					"code TEXT, "+
					"UniqueID INTEGER, "+
					"lastupdated INTEGER, "+
					"isSync INTEGER, "+
					"isDelete INTEGER, "+
					"createdBy INTEGER, " +
					"classUniqueId INTEGER " +
                    ")",
                    'rollback' : "DROP TABLE LastUpdates" },
	             
	                    
//                { 'sql' : "CREATE TABLE Country (" +
//                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
//                    "UniqueID INTEGER NOT NULL UNIQUE, " +
//                    "name TEXT " +
//                    ")",
//                    'rollback' : "DROP TABLE Country" },
                    
                    
                { 'sql' : "CREATE TABLE Room (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "SchoolId INTEGER, " +
                    "CampusId INTEGER, " +
                    "Name TEXT(100), " +
                    "Code TEXT(100), " +
                    "UniqueID INTEGER, " +
                    "lastupdated INTEGER "+
                    ")",
                    'rollback' : "DROP TABLE Room" },
                    
                    
//                { 'sql' : "CREATE TABLE State (" +
// 	               "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
// 	               "UniqueID  INTEGER NOT NULL UNIQUE, " +
// 	               "CountryId INTEGER, " +
// 	               "Name TEXT " +
// 	               ")",
// 	               'rollback' : "DROP TABLE State" },
 	               
 	               
//              { 'sql' : "CREATE TABLE TimeTableCycles (" +
//                 " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
//                 "SchoolId INTEGER, " +
//                 "CampusId INTEGER, " +
//                 "CycleDays INTEGER, " +
//                 "CycleLength INTEGER, " +
//                 "CycleLabel TEXT, " +
//                 "UniqueID INTEGER, "+
//                 "lastupdated INTEGER "+
//                 ")",
//                 'rollback' : "DROP TABLE TimeTableCycles" },
                 
                 
            { 'sql' : "CREATE TABLE contentStorage (" +
				" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" ContentType TEXT(20)  , " +
				" Title TEXT(200) NOT NULL, " +
				" Content TEXT NOT NULL, " +
				" uniqueID INTEGER NOT NULL , " +
				" respectedPanel TEXT  , " +
				" StartDate TEXT  , " +
				" allStudents INTEGER DEFAULT 1  , " +
				" allTeachers INTEGER DEFAULT 1  , " +
				" EndDate TEXT   " +					
				")",
				'rollback' : "DROP TABLE contentStorage" },
				
				
			/* { 'sql' : "CREATE TABLE Report (" +
    		 	"		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
   				"		UniqueID INTEGER, " +
   				"		isStudent INTEGER, " +
   				"		isTeacher INTEGER, " +
   				"		isReportActive INTEGER, " +
   				"		isSchoolActive INTEGER, " +
   				"		ReportDescription VARCHAR, " +
   				"		ReportName VARCHAR, " +
   				"		ReportURL VARCHAR " +
   				")",
   				'rollback' : "DROP TABLE report" }, */
   				
   				
			{ 'sql' : "CREATE TABLE lastSynch (" +
       			"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
                "tableName TEXT, " +
                "lastSynch INTEGER, " +
                "appType TEXT, "+
				"contentsync TEXT"+
                ")",
                'rollback' : "DROP TABLE lastSynch" },
                
                
             { 'sql' : "CREATE TABLE UsageLogSync (" +
				"id INTEGER, "+
                " dateLastSynced TEXT " +
                ")",
                'rollback' : "DROP TABLE UsageLogSync" },
                
                
            { 'sql' : "CREATE TABLE Announcements (" +
				" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" SchoolId INTEGER NOT NULL , " +
				" All_Students INTEGER, " +
				" All_Parents INTEGER, " +
				" All_Teachers INTEGER, " +
				" Details TEXT  NOT NULL, " +
				" CampusIds TEXT, " +
				" Isdeleted INTEGER NOT NULL DEFAULT 0,   " +		
				" UniqueID INTEGER," +			
				" Created TEXT," +
				" CreatedBy TEXT,   " +
				" Updated TEXT,   " +
				" UpdatedBy TEXT,  " +
				" readStatus INTEGER DEFAULT 0" +
				")",
				'rollback' : "DROP TABLE Announcements" },
				
				
			{ 'sql' : "CREATE TABLE News (" +
				" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" SchoolId INTEGER NOT NULL , " +
				" All_Administrators INTEGER, " +
				" All_Students INTEGER, " +
				" All_Parents INTEGER, " +
				" All_Teachers INTEGER, " +
				" Title TEXT NOT NULL, " +
				" Body TEXT, " +
				" Tags TEXT , " +
				" CampusIds TEXT, " +
				" Isdeleted INTEGER NOT NULL DEFAULT 0, " +		
				" UniqueID INTEGER," +			
				" Created TEXT," +
				" CreatedBy TEXT,   " +
				" Updated TEXT,   " +
				" UpdatedBy TEXT,  " +
				" Status INTEGER NOT NULL, " +
				" readStatus INTEGER DEFAULT 0" +
				")",
				'rollback' : "DROP TABLE News" },
			   					
			   					
			{ 'sql' : "CREATE TABLE News_Attachments (" +
				" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" NewsId INTEGER NOT NULL , " +
				" Attachments BLOB, " +
				" Created TEXT," +
				" CreatedBy INTEGER,   " +
				" Updated TEXT,   " +
				" UpdatedBy INTEGER  " +
				")",
				'rollback' : "DROP TABLE News_Attachments" },
				
				
//			{ 'sql' : "CREATE TABLE News_Images (" +
//				" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
//				" NewsId INTEGER NOT NULL , " +
//				" Image BLOB, " +
//				" Created INTEGER(20)," +
//				" CreatedBy INTEGER,   " +
//				" Updated INTEGER(20),   " +
//				" UpdatedBy INTEGER  " +
//				")",
//				'rollback' : "DROP TABLE News_Images" },
				
				
//			{ 'sql' : "CREATE TABLE emailtemplates (" +
//				" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
//				" from_name TEXT NOT NULL , " +
//				" from_email TEXT NOT NULL, " +
//				" reply_to TEXT NOT NULL," +
//				" email_title TEXT NOT NULL,   " +
//				" subject TEXT NOT NULL,   " +
//				" message TEXT NOT NULL, " +
//				" status INTEGER NOT NULL DEFAULT 1 " +
//				")",
//				'rollback' : "DROP TABLE emailtemplates" },
									   					
									   					
			{ 'sql' : "CREATE TABLE Messages (" +
				" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" ParentId INTEGER DEFAULT 0, " +
				" UserId INTEGER NOT NULL, " +
				" ReceiverId TEXT, " +
				" UserName TEXT, " +
				" ReceiverName TEXT, " +
				" Subject TEXT, " +
				" Description TEXT, " +
				" AppId INTEGER, " +
				" AppType TEXT, " +	
				" IpAddress TEXT, " +
				" IsSenderRead INTEGER , " +
				" IsReceiverRead INTEGER , " +
				" SentDate INTEGER , " +
				" DeletedStatus INTEGER NOT NULL DEFAULT 0, " +	
				" UniqueID INTEGER," +	
				" isSync INTEGER DEFAULT 1," +			
				" Created TEXT," +
				" CreatedBy TEXT,   " +
				" Updated TEXT,   " +
				" UpdatedBy TEXT," +
				" ParentEmails TEXT  " +
				")",
				'rollback' : "DROP TABLE Messages" },
				
				
			{ 'sql' : "CREATE TABLE myTeacher (" +
				" profile INTEGER, " +
				" email VARCHAR, " +
				" image_url VARCHAR, " +
				" name VARCHAR, " +
				" phone VARCHAR, " +
				" salutation VARCHAR, " +
				" subjectName VARCHAR, " +
				" teacher_id VARCHAR, " +
				" status INTEGER, " +
				" image BLOB, " +
				" isDeleted INTEGER DEFAULT 0, " +
				" PRIMARY KEY (teacher_id) "+
				")",
				'rollback' : "DROP TABLE myTeacher" },
				
				
			{ 'sql' : "CREATE TABLE myParent ( " +
   				"		parent_id INTEGER, " +
   				"		profile INTEGER, " +
   				"		email VARCHAR, " +
   				"		image_url VARCHAR, " +
   				"		name VARCHAR, " +
   				"		phone VARCHAR, " +
   				" 		status INTEGER, " +
   				"		image BLOB," +
   				"		studentId INTEGER," +
   				"		isDeleted INTEGER DEFAULT 0," +
   				"		PRIMARY KEY (parent_id,studentId) "+
   				")",
   				'rollback' : "DROP TABLE myParent" },
	   			   				
	   			   				
   			{ 'sql' : "CREATE TABLE myProfile (" +
   				"		id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, " +
   				"		isSync INTEGER, " +
   				"		school_id INTEGER, " +
   				"		birthdate TIMESTAMP, " +
   				"		address VARCHAR, " +
   				"		country VARCHAR, " +
   				"		email VARCHAR, " +
   				"		image_url VARCHAR, " +
   				"		lastName VARCHAR, " +
   				"		name VARCHAR, " +
   				"		phone VARCHAR, " +
   				"		profile_id VARCHAR, " +
   				"		state VARCHAR, " +
   				"		street VARCHAR, " +
   				"		type VARCHAR, " +
   				"		year VARCHAR, " +
   				"		zipCode VARCHAR, " +
   				"		image BLOB "+
   				")",
   				'rollback' : "DROP TABLE myProfile" },
   				
   				
   			{ 'sql' : "CREATE TABLE diaryItemUser (" +
   				"		id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, " +
   				"		item_id INTEGER, " +
   				"		unique_id INTEGER NOT NULL UNIQUE, " +
   				"		reminder VARCHAR, " +
   				"		updateDate INTEGER(20), " +
   				"		progress VARCHAR, " +
   				"		student_id VARCHAR, " +
   				"		isDeleted INTEGER DEFAULT 0, " +
   				"		user_id VARCHAR" +
   				")",
   				'rollback' : "DROP TABLE diaryItemUser" },
	   					   				
	   					   				
	   		{ 'sql' : "CREATE TABLE student (" +
   				"		class_id INTEGER NOT NULL, " +
   				"		school_id INTEGER, " +
   				"		my_class INTEGER, " +
   				"		student2 INTEGER, " +
   				"		updateTime TIMESTAMP, " +
   				"		email_id VARCHAR, " +
   				"		firstName VARCHAR, " +
   				"		image_url VARCHAR, " +
   				"		lastName VARCHAR, " +
   				"		phone_no VARCHAR, " +
   				"		student_id VARCHAR NOT NULL, " +
   				"		student_name VARCHAR, " +
   				"		teacher_id VARCHAR, " +
   				"		status INTEGER DEFAULT 1, " +
   				"		image BLOB," +
   				"		DeletedStatus INTEGER DEFAULT 0," +
   				"		PRIMARY KEY (student_id, class_id) "+
   				")",
   				'rollback' : "DROP TABLE student" },
   				
   				
			{ 'sql' : "CREATE TABLE DiaryItemTypes (" +
   				"		id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, " +
   				"		Name VARCHAR, " +
   				"		FormTemplate VARCHAR, " +
   				"		SchoolId INTEGER, " +
   				"		Active INTEGER, " +
   				" 		UniqueID INTEGER," +	
   				"		Icon VARCHAR " +
   				")",
   				'rollback' : "DROP TABLE DiaryItemTypes" },
				   				
				   				
//			{ 'sql' : "CREATE TABLE grade (" +
//	            "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
//	            "UniqueID INTEGER NOT NULL UNIQUE, " +
//	            "name TEXT " +
//	            ")",
//	            'rollback' : "DROP TABLE grade" },
	            
	            
            { 'sql' : "CREATE TABLE messageUser (" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "UniqueID INTEGER NOT NULL UNIQUE, " +
                "messageId INTEGER," +
                "receiverId INTEGER" +
                ")",
                'rollback' : "DROP TABLE messageUser" },
                
                
            { 'sql' : "CREATE TABLE eventUser (" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "uniqueId INTEGER NOT NULL UNIQUE, " +
                "eventId INTEGER," +
                "userId INTEGER, " +
                "isDeleted INTEGER" +
                ")",
                'rollback' : "DROP TABLE eventUser" }
				
			]
		},

        function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({
                'sql' : "UPDATE period SET " +
                        "startTime = strftime('%H%M', startTime/1000, 'unixepoch', 'localtime'), " +
                        "endTime = strftime('%H%M', endTime/1000, 'unixepoch', 'localtime') ",
                'rollback' : _SQL_NO_OP
            });

            // update the dayOfNote date to a format compatible with Rocketboots.date.Day
            queries.push({
                'sql' : "UPDATE dayOfNote SET " +
                        "date = strftime('%Y%m%d', date/1000, 'unixepoch', 'localtime')",
                'rollback' : _SQL_NO_OP
            });

            // update the timetable start time to a format compatible with Rocketboots.date.Day
            queries.push({
            	'sql' : "UPDATE timetable SET " +
            	"startDate = strftime('%Y%m%d', startDate/1000, 'unixepoch', 'localtime')",
            	'rollback' : _SQL_NO_OP
            });
            
            	   
            queries.push({
   				'sql' : "ALTER TABLE studentProfile RENAME TO User",
   				'rollback' : _SQL_NO_OP
   			});
			
	        queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                "tableName, lastSynch, appType) VALUES (" +
	                "'ClassUser',null, 'desktop' " +
	                ")",
	                'rollback' : "" });
				
	        queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                " tableName,lastSynch,appType ) VALUES (" +
	                "'diaryItem' ,null, 'desktop'" +
	                ")",
	                'rollback' : "" });
				
	        queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                " tableName,lastSynch,appType) VALUES (" +
	                "'noteEntry' ,null, 'desktop'" +
	                ")",
	                'rollback' : "" });
				
	        queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                   "tableName, lastSynch, appType ,contentsync) VALUES (" +
	                   "'contentsyncData',null, 'desktop', null" +
	                   ")",
	                   'rollback' : "" });
				
		   	queries.push({ 'sql' : "INSERT INTO lastSynch (" +
		                       "tableName, lastSynch, appType ,contentsync) VALUES (" +
		                       "'imagesyncing',null, 'desktop', null" +
		                       ")",
		                       'rollback' : "" });
		                       
		    queries.push({ 'sql' : "INSERT INTO lastSynch (" +
		                       "tableName, lastSynch, appType ,contentsync) VALUES (" +
		                       "'inboxItem',null, 'desktop', null" +
		                       ")",
		                       'rollback' : "" });
		                       
		    queries.push({ 'sql' : "INSERT INTO lastSynch (" +
		                       "tableName, lastSynch, appType ,contentsync) VALUES (" +
		                       "'periodSyncing',null, 'desktop', null" +
		                       ")",
		                       'rollback' : "" });	
							   
							   
			queries.push({ 'sql' : "INSERT INTO lastSynch (" +
		                       "tableName, lastSynch, appType ,contentsync) VALUES (" +
		                       "'timetableSyncing',null, 'desktop', null" +
		                       ")",
		                       'rollback' : "" });
			queries.push({ 'sql' : "INSERT INTO lastSynch (" +
		                      "tableName, lastSynch, appType ,contentsync) VALUES (" +
		                      "'getClassUserSyncing',null, 'desktop', null" +
		                      ")",
		                      'rollback' : "" });
	        queries.push({ 'sql' : "INSERT INTO lastSynch (" +
			                " tableName,lastSynch,appType) VALUES (" +
			                "'dayOfNote' ,null, 'desktop'" +
			                ")",
			                'rollback' : "" });
            
            queries.push({ 'sql' : "UPDATE Announcements SET All_Students = 1, All_Teachers = 1, Updated = Created where 1",
                    'rollback' : _SQL_NO_OP });
           
            queries.push({ 'sql' : "UPDATE News SET All_Students = 1, All_Teachers = 1, Updated = Created where 1",
                    'rollback' : _SQL_NO_OP });
            
            queries.push({ 'sql' : "UPDATE eventUser SET "+
            		" isDeleted = 0 "+
            		" where isDeleted IS NULL",
                    'rollback' : _SQL_NO_OP });
            
	        queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null WHERE tableName = 'ClassUser'",
	                'rollback' : "" });
            
            
            // Return the queries
            return {
                'queries' : queries
            };
		},
		
        function () {
            var queries = [];
            
            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });

            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		function () {
            var queries = [];

            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });
            
            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		function () {
            var queries = [];
            
            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });

            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		function () {
            var queries = [];
            
            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });

            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		function () {
            var queries = [];

            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });
            
            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		function () {
            var queries = [];
            
            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });

            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		function () {
            var queries = [];

            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });
            
            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		function () {
            var queries = [];
            
            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });

            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		function () {
            var queries = [];
            
            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });

            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		function () {
            var queries = [];

            queries.push({ 'sql' : "Select * FROM info",
                'rollback' : "" });
				
			queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "domainName TEXT",
                    'rollback' : "" });
					
            queries.push({ 'sql' : "ALTER TABLE User ADD " +
                    "EncrptedUserId TEXT",
                    'rollback' : "" });
					
			queries.push({ 'sql' : "CREATE TABLE Report (" +
    		 	"		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
   				"		UniqueID INTEGER, " +
   				"		isStudent INTEGER, " +
   				"		isTeacher INTEGER, " +
   				"		isReportActive INTEGER, " +
   				"		isSchoolActive INTEGER, " +
   				"		ReportDescription VARCHAR, " +
   				"		ReportName VARCHAR, " +
   				"		ReportURL VARCHAR " +
   				")",
   				'rollback' : "DROP TABLE report" });
            
            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		//Version 15 - 2.0.6
		function () {
            var queries = [];
			
			
			//queries.push({ 'sql' : "DROP TABLE diaryItem123", 'rollback' : "" });
			
			queries.push({ 'sql' : "DROP TABLE diaryItem", 'rollback' : "" });

			
			queries.push({ 'sql' : "CREATE TABLE diaryItem (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" type TEXT, " +
					" title TEXT, " +
					" description TEXT, " +
					" created INTEGER, " +
					" assignedDay INTEGER, " +
					" assignedTime INTEGER, " +
					" dueDay INTEGER, " +
					" dueTime INTEGER, " +
					" completed INTEGER, " +
					" subjectId INTEGER, " +
					" startDay INTEGER, " +
					" startTime INTEGER, " +
					" endTime INTEGER, " +
					" locked INTEGER, " +
					" messageFrom TEXT, " +
					" messageFromText TEXT, " +
					" responseRequired INTEGER NOT NULL DEFAULT 0, " +
					" isDelete INTEGER, " +
					" Progress INTEGER, " +
					" endDay DATE, " +
					" SchoolId INTEGER, " +
					" ClassId INTEGER, " +
					" UniqueID INTEGER, " +
					" EstimatedHours TEXT, " +
					" EstimatedMins TEXT, " +
					" AssingedBy  INTEGER, " +
					" lastupdated INTEGER, " +
					" isSync INTEGER, " +
					" priority TEXT, " +
					" UserID INTEGER, " +
					" createdBy INTEGER, " +
					" appType TEXT, " +
					" assignedtoMe INTEGER, " +
					" weight TEXT, " +
					" hideFromDue INTEGER, " +
					" hideFromAssigned INTEGER, " +
					" hideFromNotifications INTEGER, " +
					" onTheDay INTEGER DEFAULT 0, " +
					" webLink TEXT, " +
					" allStudents INTEGER DEFAULT 1, " +
					" allTeachers INTEGER DEFAULT 1, " +
					" type_id INTEGER, " +
					" type_name TEXT, " +
					" creatorUserName TEXT " +
					")",
					'rollback' : "DROP TABLE diaryItem" });
					
			queries.push({ 'sql' : "DROP TABLE attachment", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE attachment (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" diaryItemId INTEGER NOT NULL, " +
					" originalFilePath TEXT NOT NULL, " +
					" displayName TEXT(100) NOT NULL, " +
					" fileType TEXT(100) NULL, " +
					" userId INTEGER, " +
					" isSync INTEGER, " +
					" UniqueID INTEGER, " +
					" serverPath TEXT, " +
					" isDelete INTEGER DEFAULT 0, " +
					" fileSize INTEGER DEFAULT 0, " +
					" localFileName TEXT(100) NOT NULL UNIQUE, " +
					" dateAttached INTEGER(20)  " +
					")",
					'rollback' : "DROP TABLE attachment" });
					
			queries.push({ 'sql' : "DROP TABLE subject", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE subject (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NOT NULL, " +
					" classId INTEGER DEFAULT 0, " +
					" SchoolId INTEGER, " +
					" CampusId INTEGER, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" colour TEXT(100)" +
					")",
					'rollback' : "DROP TABLE subject" });
					
			queries.push({ 'sql' : "DROP TABLE dayOfNote", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE dayOfNote (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" date INTEGER(20) NOT NULL, " +
					" overrideCycleDay INTEGER(20), " +
					" includeInCycle INTEGER(1), " +
					" isGreyedOut INTEGER(1), " +
					" UniqueID INTEGER, " +
					" SchoolId INTEGER, " +
					" CampusId INTEGER, " +
					" lastupdated INTEGER, " +
					" title TEXT(100), " +
					" overrideCycle INTEGER(20) " +
					" )",
					'rollback' : "DROP TABLE dayOfNote" });
					
			queries.push({ 'sql' : "DROP TABLE timetable", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE timetable (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" startDate INTEGER(20) NOT NULL, " +
					" name TEXT(100) NULL, " +
					" cycleLabel TEXT(100) NULL, " +
					" cycleDays TEXT NULL, " +
					" TimeTableCycleId INTEGER, " +
					" EndDate INTEGER, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" cycleLength INTEGER(20) NOT NULL " +
					")",
					'rollback' : "DROP TABLE timetable" });
					
			queries.push({ 'sql' : "DROP TABLE period", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE period (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" timetable INTEGER NOT NULL, " +
					" startTime INTEGER(20) NOT NULL, " +
					" endTime INTEGER(20) NULL, " +
					" title TEXT(100) NULL, " +
					" cycleDay INTEGER(20) NULL, " +
					" forTeacher INTEGER DEFAULT 0, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" isBreak INTEGER(1)" +
					")",
					'rollback' : "DROP TABLE period" });
					
			queries.push({ 'sql' : "DROP TABLE class", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE class ( " +
					"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					"timetable INTEGER NOT NULL, " +
					"cycleDay INTEGER(20) NOT NULL, " +
					"periodId INTEGER(20) NOT NULL, " +
					"subjectId INTEGER(20) NOT NULL, " +
					"Name TEXT, " +
					"Code TEXT, " +
					"UniqueID INTEGER, " +
					"RoomId INTEGER, " +
					"isSync INTEGER, " +
					"lastupdated INTEGER, " +
					"room TEXT(100) NULL " +
					")",
					'rollback' : "DROP TABLE class" });
					
			queries.push({ 'sql' : "DROP TABLE noteEntry", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE noteEntry (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" noteId INTEGER, " +
					" timestamp INTEGER(20) NOT NULL, " +
					" text TEXT NULL, " +
					" UniqueID INTEGER, " +
					" lastupdated INTEGER, " +
					" isDelete INTEGER, " +
					" isSynch INTEGER, " +
					" noteUniqueId INTEGER, " +
					" userId INTEGER, " +
					" noteFrom TEXT(100) NULL " +
					")",
					'rollback' : "DROP TABLE noteEntry" });
					
			queries.push({ 'sql' : "DROP TABLE usageLog", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE usageLog (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" time INTEGER(20) NOT NULL, " +
					" type TEXT(100) NOT NULL, " +
					" detail TEXT NULL, " +
					" token TEXT(10) NOT NULL, " +
					" macAddresses TEXT NOT NULL, " +
					" schoolConfiguration TEXT NOT NULL, " +
					" version INTEGER NOT NULL, " +
					" clientVersion TEXT NOT NULL, " +
					" createdDate TEXT, " +
					" synced INTEGER(1) NOT NULL" +
					")",
					'rollback' : "DROP TABLE usageLog" });
					
			queries.push({ 'sql' : "DROP TABLE ClassUser", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE ClassUser (" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "classId INTEGER, " +
					"userId INTEGER, "+
					"cycleDay INTEGER, "+
					"code TEXT, "+
					"UniqueID INTEGER, "+
					"lastupdated INTEGER, "+
					"isSync INTEGER, "+
					"isDelete INTEGER, "+
					"createdBy INTEGER, " +
					"classUniqueId INTEGER " +
                    ")",
                    'rollback' : "DROP TABLE ClassUser" });
	                            
                    
            queries.push({ 'sql' : "DROP TABLE Room", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE Room (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "SchoolId INTEGER, " +
                    "CampusId INTEGER, " +
                    "Name TEXT(100), " +
                    "Code TEXT(100), " +
                    "UniqueID INTEGER, " +
                    "lastupdated INTEGER "+
                    ")",
                    'rollback' : "DROP TABLE Room" });
                 
                 
            queries.push({ 'sql' : "DROP TABLE contentStorage", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE contentStorage (" +
				" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" ContentType TEXT(20)  , " +
				" Title TEXT(200) NOT NULL, " +
				" Content TEXT NOT NULL, " +
				" uniqueID INTEGER NOT NULL , " +
				" respectedPanel TEXT  , " +
				" StartDate TEXT  , " +
				" allStudents INTEGER DEFAULT 1  , " +
				" allTeachers INTEGER DEFAULT 1  , " +
				" EndDate TEXT   " +					
				")",
				'rollback' : "DROP TABLE contentStorage" });
				
			queries.push({ 'sql' : "DROP TABLE UsageLogSync", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE UsageLogSync (" +
				"id INTEGER, "+
                " dateLastSynced TEXT " +
                ")",
                'rollback' : "DROP TABLE UsageLogSync" });
                
                
            queries.push({ 'sql' : "DROP TABLE Announcements", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE Announcements (" +
				" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" SchoolId INTEGER NOT NULL , " +
				" All_Students INTEGER, " +
				" All_Parents INTEGER, " +
				" All_Teachers INTEGER, " +
				" Details TEXT  NOT NULL, " +
				" CampusIds TEXT, " +
				" Isdeleted INTEGER NOT NULL DEFAULT 0,   " +		
				" UniqueID INTEGER," +			
				" Created TEXT," +
				" CreatedBy TEXT,   " +
				" Updated TEXT,   " +
				" UpdatedBy TEXT,  " +
				" readStatus INTEGER DEFAULT 0" +
				")",
				'rollback' : "DROP TABLE Announcements" });
				
				
			queries.push({ 'sql' : "DROP TABLE News", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE News (" +
				" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" SchoolId INTEGER NOT NULL , " +
				" All_Administrators INTEGER, " +
				" All_Students INTEGER, " +
				" All_Parents INTEGER, " +
				" All_Teachers INTEGER, " +
				" Title TEXT NOT NULL, " +
				" Body TEXT, " +
				" Tags TEXT , " +
				" CampusIds TEXT, " +
				" Isdeleted INTEGER NOT NULL DEFAULT 0, " +		
				" UniqueID INTEGER," +			
				" Created TEXT," +
				" CreatedBy TEXT,   " +
				" Updated TEXT,   " +
				" UpdatedBy TEXT,  " +
				" Status INTEGER NOT NULL, " +
				" readStatus INTEGER DEFAULT 0" +
				")",
				'rollback' : "DROP TABLE News" });
			   					
			   					
			queries.push({ 'sql' : "DROP TABLE News_Attachments", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE News_Attachments (" +
				" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" NewsId INTEGER NOT NULL , " +
				" Attachments BLOB, " +
				" Created TEXT," +
				" CreatedBy INTEGER,   " +
				" Updated TEXT,   " +
				" UpdatedBy INTEGER  " +
				")",
				'rollback' : "DROP TABLE News_Attachments" });
			
			
			queries.push({ 'sql' : "DROP TABLE Messages", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE Messages (" +
				" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				" ParentId INTEGER DEFAULT 0, " +
				" UserId INTEGER NOT NULL, " +
				" ReceiverId TEXT, " +
				" UserName TEXT, " +
				" ReceiverName TEXT, " +
				" Subject TEXT, " +
				" Description TEXT, " +
				" AppId INTEGER, " +
				" AppType TEXT, " +	
				" IpAddress TEXT, " +
				" IsSenderRead INTEGER , " +
				" IsReceiverRead INTEGER , " +
				" SentDate INTEGER , " +
				" DeletedStatus INTEGER NOT NULL DEFAULT 0, " +	
				" UniqueID INTEGER," +	
				" isSync INTEGER DEFAULT 1," +			
				" Created TEXT," +
				" CreatedBy TEXT,   " +
				" Updated TEXT,   " +
				" UpdatedBy TEXT," +
				" ParentEmails TEXT  " +
				")",
				'rollback' : "DROP TABLE Messages" });
				
			queries.push({ 'sql' : "DROP TABLE myTeacher", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE myTeacher (" +
				" profile INTEGER, " +
				" email VARCHAR, " +
				" image_url VARCHAR, " +
				" name VARCHAR, " +
				" phone VARCHAR, " +
				" salutation VARCHAR, " +
				" subjectName VARCHAR, " +
				" teacher_id VARCHAR, " +
				" status INTEGER, " +
				" image BLOB, " +
				" isDeleted INTEGER DEFAULT 0, " +
				" PRIMARY KEY (teacher_id) "+
				")",
				'rollback' : "DROP TABLE myTeacher" });
				
				
			queries.push({ 'sql' : "DROP TABLE myParent", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE myParent ( " +
   				"		parent_id INTEGER, " +
   				"		profile INTEGER, " +
   				"		email VARCHAR, " +
   				"		image_url VARCHAR, " +
   				"		name VARCHAR, " +
   				"		phone VARCHAR, " +
   				" 		status INTEGER, " +
   				"		image BLOB," +
   				"		studentId INTEGER," +
   				"		isDeleted INTEGER DEFAULT 0," +
   				"		PRIMARY KEY (parent_id,studentId) "+
   				")",
   				'rollback' : "DROP TABLE myParent" });
	   			   				
	   			   				
   			queries.push({ 'sql' : "DROP TABLE myProfile", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE myProfile (" +
   				"		id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, " +
   				"		isSync INTEGER, " +
   				"		school_id INTEGER, " +
   				"		birthdate TIMESTAMP, " +
   				"		address VARCHAR, " +
   				"		country VARCHAR, " +
   				"		email VARCHAR, " +
   				"		image_url VARCHAR, " +
   				"		lastName VARCHAR, " +
   				"		name VARCHAR, " +
   				"		phone VARCHAR, " +
   				"		profile_id VARCHAR, " +
   				"		state VARCHAR, " +
   				"		street VARCHAR, " +
   				"		type VARCHAR, " +
   				"		year VARCHAR, " +
   				"		zipCode VARCHAR, " +
   				"		image BLOB "+
   				")",
   				'rollback' : "DROP TABLE myProfile" });
   				
   				
   			queries.push({ 'sql' : "DROP TABLE diaryItemUser", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE diaryItemUser (" +
   				"		id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, " +
   				"		item_id INTEGER, " +
   				"		unique_id INTEGER NOT NULL UNIQUE, " +
   				"		reminder VARCHAR, " +
   				"		updateDate INTEGER(20), " +
   				"		progress VARCHAR, " +
   				"		student_id VARCHAR, " +
   				"		isDeleted INTEGER DEFAULT 0, " +
   				"		user_id VARCHAR" +
   				")",
   				'rollback' : "DROP TABLE diaryItemUser" });
	   					   				
	   					   				
	   		queries.push({ 'sql' : "DROP TABLE student", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE student (" +
   				"		class_id INTEGER NOT NULL, " +
   				"		school_id INTEGER, " +
   				"		my_class INTEGER, " +
   				"		student2 INTEGER, " +
   				"		updateTime TIMESTAMP, " +
   				"		email_id VARCHAR, " +
   				"		firstName VARCHAR, " +
   				"		image_url VARCHAR, " +
   				"		lastName VARCHAR, " +
   				"		phone_no VARCHAR, " +
   				"		student_id VARCHAR NOT NULL, " +
   				"		student_name VARCHAR, " +
   				"		teacher_id VARCHAR, " +
   				"		status INTEGER DEFAULT 1, " +
   				"		image BLOB," +
   				"		DeletedStatus INTEGER DEFAULT 0," +
   				"		PRIMARY KEY (student_id, class_id) "+
   				")",
   				'rollback' : "DROP TABLE student" });
   				
   				
			queries.push({ 'sql' : "DROP TABLE DiaryItemTypes", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE DiaryItemTypes (" +
   				"		id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, " +
   				"		Name VARCHAR, " +
   				"		FormTemplate VARCHAR, " +
   				"		SchoolId INTEGER, " +
   				"		Active INTEGER, " +
   				" 		UniqueID INTEGER," +	
   				"		Icon VARCHAR " +
   				")",
   				'rollback' : "DROP TABLE DiaryItemTypes" });
				
			queries.push({ 'sql' : "DROP TABLE messageUser", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE messageUser (" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "UniqueID INTEGER NOT NULL UNIQUE, " +
                "messageId INTEGER," +
                "receiverId INTEGER" +
                ")",
                'rollback' : "DROP TABLE messageUser" });
                
                
            queries.push({ 'sql' : "DROP TABLE eventUser", 'rollback' : "" });
			
			queries.push({ 'sql' : "CREATE TABLE eventUser (" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "uniqueId INTEGER NOT NULL UNIQUE, " +
                "eventId INTEGER," +
                "userId INTEGER, " +
                "isDeleted INTEGER" +
                ")",
                'rollback' : "DROP TABLE eventUser" });
            
	        queries.push({ 'sql' : "CREATE TABLE CalendarCycleDay (" +
        		 	"		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
	   				"		UniqueID INTEGER, " +
	   				"		CampusId INTEGER, " +
	   				"		CycleDay INTEGER, " +
	   				"		date INTEGER, " +
	   				"		cycleDayLabel TEXT, " +
	   				"		shortCycleDayLabel TEXT, " +
	   				" 		Created TEXT," +
   					" 		CreatedBy TEXT,   " +
   					" 		Updated TEXT,   " +
   					" 		UpdatedBy TEXT  " +
	   				")",
	   				'rollback' : "DROP TABLE CalendarCycleDay" });
        
	        queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                " tableName,lastSynch,appType) VALUES (" +
	                "'CalendarCycleDay' ,null, 'desktop'" +
	                ")",
	                'rollback' : "" });
	        
	        queries.push({ 'sql' : "ALTER TABLE class ADD " +
	                "campusCode TEXT",
	                'rollback' : "" });
	        
	        queries.push({ 'sql' : "ALTER TABLE class ADD " +
	                "campusId TEXT",
	                'rollback' : "" });
	        
	        queries.push({ 'sql' : "ALTER TABLE timetable ADD " +
                "CampusCode TEXT",
                'rollback' : "" });
            
            /* queries.push({ 'sql' : "DELETE FROM diaryItem", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM attachment", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM subject", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM dayOfNote", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM timetable", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM period", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM class", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM noteEntry", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM usageLog", 'rollback' : "" });
           	
            queries.push({ 'sql' : "DELETE FROM ClassUser", 'rollback' : "" });
           	 
            queries.push({ 'sql' : "DELETE FROM Room", 'rollback' : "" });
                   
            queries.push({ 'sql' : "DELETE FROM contentStorage", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM Report", 'rollback' : "" });
                
            queries.push({ 'sql' : "DELETE FROM CalendarCycleDay", 'rollback' : "" });
                      
            queries.push({ 'sql' : "DELETE FROM UsageLogSync", 'rollback' : "" });
                 
            queries.push({ 'sql' : "DELETE FROM Announcements", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM News", 'rollback' : "" });
              	 	
            queries.push({ 'sql' : "DELETE FROM News_Attachments", 'rollback' : "" });
           	   	
            queries.push({ 'sql' : "DELETE FROM Messages", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM myTeacher", 'rollback' : "" });

            queries.push({ 'sql' : "DELETE FROM myParent", 'rollback' : "" });
           	      
            queries.push({ 'sql' : "DELETE FROM myProfile", 'rollback' : "" });
              
            queries.push({ 'sql' : "DELETE FROM diaryItemUser", 'rollback' : "" });
           	   	   	   	   
            queries.push({ 'sql' : "DELETE FROM student", 'rollback' : "" });
                
            queries.push({ 'sql' : "DELETE FROM DiaryItemTypes", 'rollback' : "" });
               
            queries.push({ 'sql' : "DELETE FROM messageUser", 'rollback' : "" });
                 
            queries.push({ 'sql' : "DELETE FROM eventUser", 'rollback' : "" });	 */

            queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null where tableName = 'ClassUser'", 'rollback' : "" });

            queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null where tableName = 'diaryItem'", 'rollback' : "" });

            queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null where tableName = 'noteEntry'", 'rollback' : "" });

            queries.push({ 'sql' : "UPDATE lastSynch SET contentsync = null where tableName = 'contentsyncData'", 'rollback' : "" });

            queries.push({ 'sql' : "UPDATE lastSynch SET contentsync = null where tableName = 'imagesyncing'", 'rollback' : "" });

            queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null where tableName = 'inboxItem'", 'rollback' : "" });

            queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null where tableName = 'periodSyncing'", 'rollback' : "" });

            queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null where tableName = 'getClassUserSyncing'", 'rollback' : "" });

            queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null where tableName = 'dayOfNote'", 'rollback' : "" });

            queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null where tableName = 'CalendarCycleDay'", 'rollback' : "" });
            
            queries.push({ 'sql' : "UPDATE schoolProfile SET appInstallStatus = 0 where id = 1", 'rollback' : "" });
            
            queries.push({ 'sql' : "INSERT INTO lastSynch (" +
                    "tableName, lastSynch, appType ,contentsync) VALUES (" +
                    "'teacherSyncing',null, 'desktop', null" +
                    ")",
                    'rollback' : "" });
            
            queries.push({ 'sql' : "INSERT INTO lastSynch (" +
                    "tableName, lastSynch, appType ,contentsync) VALUES (" +
                    "'parentSyncing',null, 'desktop', null" +
                    ")",
                    'rollback' : "" });
            
            queries.push({ 'sql' : "CREATE TABLE list_of_values (" +
        		 	"		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
	   				"		UniqueID INTEGER, " +
	   				"		DiaryItemType TEXT, " +
	   				"		Type TEXT, " +
	   				"		Description TEXT, " +
	   				"		Value TEXT, " +
	   				" 		Icon TEXT," +
					" 		IconSmall TEXT," +
   					" 		SchoolId INTEGER,   " +
   					" 		SortOrder INTEGER,   " +
   					" 		Active INTEGER,  " +
   					" 		Created TEXT," +
   					" 		CreatedBy TEXT,   " +
   					" 		Updated TEXT,   " +
   					" 		UpdatedBy TEXT  " +
	   				")",
	   				'rollback' : "DROP TABLE list_of_values" });

            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		//Release 2.0.8 - Merits
		function () {
            var queries = [];
			
			queries.push({ 'sql' : "CREATE TABLE listOfValues (" +
        		 	"		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
	   				"		UniqueID INTEGER, " +
	   				"		DiaryItemType TEXT, " +
	   				"		Type TEXT, " +
	   				"		Description TEXT, " +
	   				"		Value TEXT, " +
	   				" 		Icon TEXT," +
					" 		IconSmall TEXT," +
   					" 		SchoolId INTEGER,   " +
   					" 		SortOrder INTEGER,   " +
   					" 		Active INTEGER,  " +
   					" 		Created TEXT," +
   					" 		CreatedBy TEXT,   " +
   					" 		Updated TEXT,   " +
   					" 		UpdatedBy TEXT  " +
	   				")",
	   				'rollback' : "DROP TABLE listOfValues" });
					
			queries.push({ 'sql' : "UPDATE lastSynch SET lastSynch = null where tableName = 'diaryItem'", 'rollback' : "" });
			
			queries.push({ 'sql' : "UPDATE schoolProfile SET appInstallStatus = 0 where id = 1", 'rollback' : "" });

            queries.push({ 'sql' : "ALTER TABLE diaryItemUser ADD " +
	                "isnotify INTEGER DEFAULT 0",
	                'rollback' : "" });
					
			queries.push({ 'sql' : "ALTER TABLE diaryItemUser ADD " +
	                "passtype_id INTEGER",
	                'rollback' : "" });
					
			queries.push({ 'sql' : "ALTER TABLE diaryItemUser ADD " +
	                "passtype_name TEXT",
	                'rollback' : "" });
					
			queries.push({ 'sql' : "ALTER TABLE diaryItemUser ADD " +
	                "comment TEXT",
	                'rollback' : "" });
					
			queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
	                "passtype_id INTEGER",
	                'rollback' : "" });
					
			queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
	                "meritComment TEXT",
	                'rollback' : "" });
					
			queries.push({ 'sql' : "ALTER TABLE diaryItemUser ADD " +
	                "isMeritDeleted INTEGER",
	                'rollback' : "" });
					
			queries.push({ 'sql' : "ALTER TABLE diaryItemUser ADD " +
	                "meritAssignedDate INTEGER",
	                'rollback' : "" });
					
            
            // Return the queries
            return {
                'queries' : queries
            };
		},
							
    ];
	//****************************************************/

	/**
	 * A helper function to modify an array of query objects by appending the query objects necessary
	 * to add a column to a table.
	 *
	 * The reason this is non-trivial is because SQLite does not support
	 * removing a column, which is required for a rollback operation.
	 *
	 * @param queryArray The array of query objects which is to be appended to.
	 * @param tableName The name of the table to have a column added, e.g. "timetable"
	 * @param newColumnDefinition The column definition for the new column, e.g. "name TEXT(100) NOT NULL DEFAULT 'hello world'"
	 * @param rollbackColumnNames The comma-separated column names for the unmodified table, e.g. "id, type, value"
	 * @param rollbackColumnDefinitions The comma-separated column definitions for the unmodified table, e.g. "id INTEGER, type TEXT, value INTEGER NULL"
	 * @return {Array} The modified query array (as passed in). Note that the original will also be modified.
	 * @private
	 */
	_service.prototype._pushQueriesToAddColumn = function (queryArray, tableName, newColumnDefinition, rollbackColumnNames, rollbackColumnDefinitions) {

		var tableCopyName = tableName + "Copy";

		queryArray.push({
			'sql' : _SQL_NO_OP,
			// Re-name the new table. Note: rollbacks should be read in reverse order.
			'rollback' : "ALTER TABLE " + tableCopyName + " RENAME TO " + tableName
		});
		queryArray.push({
			'sql' : _SQL_NO_OP,
			// Delete the table (the one with the new column)
			'rollback' : "DROP TABLE " + tableName
		});
		queryArray.push({
			'sql' : _SQL_NO_OP,
			// Copy across all the data
			'rollback' : "INSERT INTO " + tableCopyName + " (" + rollbackColumnNames + ") SELECT " + rollbackColumnNames + " FROM " + tableName
		});
		queryArray.push({
			'sql' : "ALTER TABLE " + tableName + " ADD COLUMN " + newColumnDefinition,
			// Create a table in the old format (without the new column)
			'rollback' : "CREATE TABLE " + tableCopyName + " (" + rollbackColumnDefinitions + ")"
		});

		return queryArray;
	};

	/**
	 * A helper function to modify an array of query objects by appending the query objects necessary
	 * to remove a column to a table.
	 *
	 * The reason this is non-trivial is because SQLite does not support removing a column.
	 * 
	 * Note also that rollback is destructive; while the column will be readded to the table, the data will have been deleted.
	 *
	 * @param queryArray The array of query objects which is to be appended to.
	 * @param tableName The name of the table to have a column removed, e.g. "timetable"
	 * @param newColumnDefinitions The comma-separated column definitions for the modified table, e.g. "id INTEGER, type TEXT, value INTEGER NULL"
	 * @param newColumnNames The comma-separated column names for the modified table, e.g. "id, type, value"
	 * @param oldColumnDefinition The column definition for the dropped column, e.g. "name TEXT(100) NOT NULL DEFAULT 'hello world'"
	 * @return {Array} The modified query array (as passed in). Note that the original will also be modified.
	 * @private
	 */
	_service.prototype._pushQueriesToDropColumn = function (queryArray, tableName, newColumnDefinitions, newColumnNames, oldColumnDefinition) {

		var tableCopyName = tableName + "Copy";
		
		queryArray.push({
			// Create a table with with the new column definitions.
			'sql' : "CREATE TABLE " + tableCopyName + " (" + newColumnDefinitions + ")",
			'rollback' : "ALTER TABLE " + tableName + " ADD COLUMN " + oldColumnDefinition
		});
		queryArray.push({
			// Copy across all the data from the old table to the new created one.
			'sql' : "INSERT INTO " + tableCopyName + " (" + newColumnNames + ") SELECT " + newColumnNames + " FROM " + tableName,
			'rollback' : _SQL_NO_OP
		});
		queryArray.push({
			// Delete the old table (the one with the old column).
			'sql' : "DROP TABLE " + tableName,
			'rollback' : _SQL_NO_OP
		});
		queryArray.push({
			// Re-name the new table to replace the old table.
			'sql' : "ALTER TABLE " + tableCopyName + " RENAME TO " + tableName,
			'rollback' : _SQL_NO_OP
		});

		return queryArray;
	};

	/**
	 * @private
	 * Retrieve the schema version.
	 * Has the side affect of creating the 'info' table (used to store the schema version) if
	 * the database is unversioned.
	 *
	 * @return Schema version, or zero if the database is unversioned.
	 */
	_service.prototype._getSchemaVersion = function () {

		// holds the version that is currently installed in the local store
		var version = null;
		try {

			// get the last installed version
			var result = this._database.runQuery("SELECT schemaVersion FROM info").getResult().data;

			if (result != null) {
				version = result[0].schemaVersion;
			}

		} catch (error) {

			// if an error occurs we will assume that no schema is yet installed so we will
			// try to install version 1
			//console.log("DiarySchemaManager._getSchemaVersion() - Unable to read database schema version, assuming no schema exists");
		}

		// No schema installed, so create info table to store schema version - starting at version 0
		if (version === null) {
			var queries = [];

			// Manage schema version
			queries.push({ 'sql' : "CREATE TABLE info (" +
				" schemaVersion INT NOT NULL " +
				")",
				'rollback' : "DROP TABLE info" });

			queries.push({ 'sql' : "INSERT INTO info (schemaVersion) VALUES (0)",
				'rollback' : "DELETE FROM info" });

			this._database.runScripts(queries);

			version = 0;
		}

		return version;
	};

	/**
	 * Check if the current version of the schema can be upgraded.
	 * @return {Boolean} True if the schema version is supported, false otherwise.
	 */
	_service.prototype.isSchemaVersionSupported = function () {
		var schemaVersion = this._getSchemaVersion();
		return schemaVersion == 0 || schemaVersion >= _MINIMUM_SUPPORTED_VERSION;
	};

	/**
	 * Will update the schema to the latest version of the application.  If more than one version
	 * upgrade is required, the schema updates will be executed in sequence.
	 */
	_service.prototype.installSchemaUpdates = function () {

		var initialVersion = this._getSchemaVersion();
		var currentVersion;

		if (!this.isSchemaVersionSupported()) {
			throw new Error("Your database version " + initialVersion + " is unsupported. Cannot upgrade database.");
		}

		for (currentVersion = initialVersion; currentVersion - _MINIMUM_SUPPORTED_VERSION + 1 < _schema_versions.length; ++currentVersion) {
			// Start at the version just before the minimum
			if (currentVersion < 1) {
				currentVersion = _MINIMUM_SUPPORTED_VERSION - 1;
			}
			// Guard for empty version list
			if (_schema_versions.length == 0) {
				throw new Error("No schema to install - the schema has not been defined.");
			}

			// Perform the update
			{
				var nextVersionObject = _schema_versions[currentVersion - _MINIMUM_SUPPORTED_VERSION + 1];

				// Handle version objects specified as functions
				if (_.isFunction(nextVersionObject)) {
					nextVersionObject = nextVersionObject.call(this);
				}

				// Copy the queries array, so it can be appended to
				var updateSchemaQueries = (nextVersionObject && nextVersionObject.queries ? nextVersionObject.queries.slice(0) : null);
				if (!updateSchemaQueries || !updateSchemaQueries.length) {
					throw new Error("Poorly defined schema update " + (1 + currentVersion));
				}

				// Update the schema version in the DB
				updateSchemaQueries.push({ 'sql' : "UPDATE info SET schemaVersion = " + (1 + currentVersion),
					'rollback' : "UPDATE info SET schemaVersion = " + (currentVersion == (_MINIMUM_SUPPORTED_VERSION - 1) ? 0 : currentVersion) });
					//console.log(updateSchemaQueries);
				this._database.runScripts(updateSchemaQueries);
			}
		}

		//console.log("DiarySchemaManager.installSchemaUpdates() - completed at schema version " + currentVersion + " (was version " + initialVersion + ")");

	};

	/**
	 * Drops the database
	 */
	_service.prototype.dropDatabase = function() {
		this._database.dropDatabase();
	};

	// return the created service
	return _service;

})();
