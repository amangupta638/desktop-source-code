/**
 * The local SQL delegate for Attachments. The delegate will use SQL queries to actually
 * get data from a local database.
 * 
 * author - John Pearce
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.AttachmentSQLDelegate = (function() {
	
	/**
	 * Constructs an Attachment SQL Delegate.  
	 * 
	 * @constructor
	 */
	var _delegate = function() {
		this._database = app.locator.getService(ServiceName.APPDATABASE);
		this._fileSystemDelegate = app.locator.getService(ServiceName.ATTACHMENTFILEDELEGATE);
	};

	/**
	 * @public
	 * For each diary item in the collection, set it's "attachment" property to be a newly
	 * constructed attachment collection, as loaded from the database.
	 * 
	 * @param {!diary.collect.AttachmentCollection} diaryItems The collection of diary items to have their attachment property updated.
	 * @param {!function()} successCallback The function to be called upon success.
	 * @param {?function(string)} errorCallback The function to be called upon failure.
	 */
	_delegate.prototype.getAttachmentsForDiaryItems = function(diaryItems, successCallback, errorCallback) {

		// build the list of diary item ids
		var delegate = this, ids = _.map(diaryItems.toArray(), function(item) {
			return item.id;
		}).join(",");

		// execute the query
		delegate._database.runQuery(
				// query
				"SELECT * FROM attachment WHERE attachment.isDelete = 0 AND attachment.diaryItemId IN (" + ids + ")",

				// params
				{},

				// success
				function (statement) {
					// ensure all diary items have an empty collection of attachments
					_.forEach(diaryItems.toArray(), function (diaryItem) {
						diaryItem.set("attachments", new diary.collection.AttachmentCollection());
					});

					// add the attachments to the appropriate diary items
					_.forEach(statement.getResult().data, function (dbAttachment) {
						diaryItems.get(dbAttachment.diaryItemId).get('attachments').add(new diary.model.Attachment({
							'id' : dbAttachment.id,
							'originalFilePath' : dbAttachment.originalFilePath,
							'displayName' : dbAttachment.displayName,
							'fileType' : dbAttachment.fileType,
							'fileSize' : dbAttachment.fileSize,
							'localFileName' : dbAttachment.localFileName,
							'serverPath' : dbAttachment.serverPath,
							'localFilePath' : delegate._fileSystemDelegate.resolveLocalFilePath(dbAttachment.localFileName),
							'dateAttached' : new Date(dbAttachment.dateAttached)
						}));
					});

					// call the success call back with the diary items collection (which has now been updated)
					successCallback(diaryItems);
				},

				// error
				errorCallback, "Error while trying to read attachments for diary items " + ids
		);
	};
	
	/**
	 * @public
	 * Adds an attachment to a particular diary item.
	 * 
	 * @param {!diary.model.Attachment} attachment The attachment which should be added
	 * @param {!number} diaryItemId The identifier of the diary item to which the attachment should be added
	 * @param {?function(attachmentId)} successCallback The function that is to be called on success
	 * @param {?function(message)} errorCallback The function that is to be called on failure of the query
	 */
	_delegate.prototype.addAttachmentToDiaryItem = function(attachment, diaryItemId, successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"INSERT INTO attachment (diaryItemId, originalFilePath, displayName, fileType, localFileName, dateAttached, isSync, fileSize) " +
						"VALUES (:diaryItemId, :originalFilePath, :displayName, :fileType, :localFileName, :dateAttached, :isSync, :fileSize)",

				// params
				{
					'diaryItemId': diaryItemId,
					'originalFilePath': attachment.get('originalFilePath'),
					'displayName': attachment.get('displayName'),
					'fileType': attachment.get('fileType'),
					'localFileName': attachment.get('localFileName'),
					'dateAttached': attachment.get('dateAttached').getTime(),
					'isSync': '0',
					'fileSize' : attachment.get('fileSize')
				},

				// success
				function (statement) {
					successCallback(statement.getResult().lastInsertRowID);
				},

				// error
				errorCallback, "Failed to add attachment " + attachment.get('displayName') + " to diary item " + diaryItemId
		);
	};

function uploadFile(Attachment)
{
   for(var i=0;i<Attachment.length;i++)
   {
	   if(Attachment[i].UniqueID)
	   {
			var request="";
			var schoolID = app.model.get(ModelName.SCHOOLPROFILE).get('UniqueId');
	
			buffer = new air.ByteArray();
			file = new air.File(Attachment[i].originalFilePath);
			fileStream = new air.FileStream();
			fileStream.open(file, air.FileMode.READ);
			
			var gdocURL = '';
			if((Attachment[i].fileType && (Attachment[i].fileType.toUpperCase() == '.GDOC')) ||
					(Attachment[i].displayName && (Attachment[i].displayName.toUpperCase()).indexOf(".GDOC") > 0)){
				var tempFileStream = new air.FileStream();
				tempFileStream.open(file, air.FileMode.READ);
				var sContent = tempFileStream.readUTFBytes(tempFileStream.bytesAvailable);
				var jsonObj = JSON.parse(sContent);
				gdocURL = jsonObj.url;
				tempFileStream.close();
			} 
			
			fileContents = new air.ByteArray();
			fileStream.readBytes(fileContents, 0, file.size);
			fileStream.close();
			
			url = SystemSettings.SERVERURL + "/attachmentsync?diaryItemId="+Attachment[i].UniqueID+
			"&localAttachmentId="+Attachment[i].id+"&schoolId="+schoolID+"&url="+gdocURL+"&userId="+UserId;  
			//alert("attcahmnet uplaod="+url.length);
			boundary = '--------------======-------------------AaB03x';
	
			request = new air.URLRequest(url);
			request.useCache = false;
			request.contentType = 'multipart/form-data, boundary='+boundary;
			//request.shouldCacheResponse = false;
			request.method='POST';
			
			//console.log('fileStream ' + fileStream);
			//console.log('fileContents ' + fileContents);
			
			buffer.writeUTFBytes( "--"+boundary+"\r\n" );
			buffer.writeUTFBytes( "content-disposition: form-data; name=\"Filedata\"; filename=\""+file.name+"\"\r\n" );
			buffer.writeUTFBytes( "Content-Transfer-Encoding: binary\r\n" );
			buffer.writeUTFBytes( "Content-Length: "+file.size+"\r\n" );
			buffer.writeUTFBytes( "Content-Type: application/octet-stream\r\n" );
			buffer.writeUTFBytes( "\r\n" );
	
			buffer.writeBytes(fileContents, 0, fileContents.length);
			buffer.writeUTFBytes( "\r\n--"+boundary+"--\r\n" );
	
			request.data = buffer;
			var loader = new air.URLLoader();
			loader.addEventListener(air.ProgressEvent.PROGRESS , callback_for_upload_progress);
			loader.addEventListener(air.IOErrorEvent.IO_ERROR , function (e){
				//console.log("Event Error");
				//alert("Event Error"+e.text);
				//air.trace( 'error: '+ e.text );                
			});
			loader.addEventListener(air.Event.COMPLETE, callback_for_upload_finish);
			loader.load( request );
		}
	}
		
	  
	 
	function callback_for_upload_progress(event) { 
	    var loaded = event.bytesLoaded; 
	    var total = event.bytesTotal; 
	    var pct = Math.ceil( ( loaded / total ) * 100 ); 
	    //air.trace('Uploaded ' + pct.toString() + '%');
	}
	
	
	function callback_for_upload_finish(event) {
	    var response1=event.currentTarget.data;
	    //console.log("response1 " + JSON.stringify(response1));
	    response1=$.parseJSON(response1);
	    //console.log('response2'+response1);
	    
	    var attcament = response1.localAttachmentId;
	    var UniqueID = response1.attachmentid;
	    var serverPath = response1.serverPath;
	    var fileSize = response1.fileSize;
	    
	    app.locator.getService(ServiceName.ATTACHMENTDBDELEGATE).updateAttachmnetRecords(attcament, serverPath, UniqueID, fileSize, onupdateAttachmnetSuccess, onupdateAttachmnetError);
	    
	   function onupdateAttachmnetSuccess()
	   {
	  // console.log("onupdateAttachmnetSuccess");
	   }
	   function onupdateAttachmnetError()
	   {
	   }
	}
	        
	}
	_delegate.prototype.updateAttachmnetRecords = function(attachment, serverPath, UniqueID, fileSize, successCallback, errorCallback) {
        //console.log("updateAttachmnetRecords"+attachment);
        this._database.runQuery(
                // query
                "UPDATE attachment SET isSync = 1, serverPath = :serverPath, UniqueID = :UniqueID, fileSize = :fileSize  WHERE id = :attachmentId",

                // parameters
                { 
                	'attachmentId': attachment,
                	'serverPath' : serverPath,
                	'UniqueID' : UniqueID,
                	'fileSize' : fileSize
                },

                //success
               function success()
               {
                    //alert("success");
               },

                // error
                errorCallback, "Failed to update attachment "
        );
    };
	/**
	 * @public
	 * Remove an attachment from the diary item to which it was attached.
	 * 
	 * @param {!diary.model.Attachment} attachment The attachment which should be removed
	 * @param {?function()} successCallback The function that is to be called on success
	 * @param {?function(message)} errorCallback The function that is to be called on failure
	 */
	_delegate.prototype.removeAttachment = function(attachment, successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"UPDATE attachment SET isDelete = 1 WHERE attachment.id = :attachmentId",

				// parameters
				{ 'attachmentId': attachment.id },

				//success
				successCallback || $.noop,

				// error
				errorCallback, "Failed to remove attachment " + attachment.get('displayName')
		);
	};
	
	_delegate.prototype.deleteAttachment = function(attachment, successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"UPDATE attachment SET isDelete = 1 WHERE attachment.id = :attachmentId",

				// parameters
				{ 'attachmentId': attachment.id },

				//success
				successCallback || $.noop,

				// error
				errorCallback, "Failed to remove attachment " + attachment.get('displayName')
		);
	};
	
	_delegate.prototype.GetAttachments = function(attachment, successCallback, errorCallback) {
        this._database.runQuery(
                // query
                "SELECT di.UniqueID,at.originalFilePath,"+
            "at.displayName,at.fileType,at.localFileName,at.dateAttached,at.id "+
            "FROM diaryItem as di LEFT OUTER JOIN attachment as at ON at.diaryItemId = di.id WHERE at.isSync=0",

                // parameters
                { 
                	
                },

                //success
               function(statement) {
                    console.log("success");
                 	var diaryitem=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    uploadFile(diaryitem);
                                
                },

                // error
                errorCallback, "GetAttachments Error"
        );
    };
    
    _delegate.prototype.deleteAttachments = function(attachment, successCallback, errorCallback) {
        this._database.runQuery(
                // query
                "SELECT at.id, at.UniqueID FROM diaryItem as di "+
            	" LEFT OUTER JOIN attachment as at ON at.diaryItemId = di.id WHERE at.isDelete = 1",

                // parameters
                { 
                	
                },

                //success
               function(statement) {
                   var diaryitem =_.map(statement.getResult().data, function(item) {
                        return item;
                   });
                   console.log("diaryitem : "+JSON.stringify(diaryitem));
                   successCallback(diaryitem);
                                
                },

                // error
                errorCallback, "deleteAttachments Error"
        );
    };
	
	// return the generated delegate
	return _delegate;
	
})();
