
/**
 * The manager for the Sync Diary
 *
 * author - Jane Sivieng
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SyncDiaryManager = (function() {

    /**
     * Constructs a Sync Diary Manager
     *
     * @constructor
     */
    var _manager = function() {

        this._delegate = app.locator.getService(ServiceName.SYNCDIARYITEMDELEGATE);
    };

    /**
     * Add a new log event
     *
     * @param usageLogEvent
     * @param successCallback
     * @param errorCallback
     */
    _manager.prototype.SyncDiaryItems = function(successCallback, errorCallback) {
        if (!(usageLogEvent instanceof diary.model.UsageLogEvent)) {
            throw new Error("usageLogEvent must be of type diary.model.UsageLogEvent");
        }

        if (typeof(successCallback) != "function") {
            throw new Error("successCallback must be specified");
        }

        this._delegate.SyncDiaryItem(successCallback, errorCallback);

    };

    

    // return the generated manager
    return _manager;

})();
