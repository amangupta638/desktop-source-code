/**
 * The manager for all things related to registration
 * 
 * author - Jane Sivieng
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.RegistrationManager = (function() {
	
	/**
	 * Constructs a Registration Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		this._cloudManager = app.locator.getService(ServiceName.THECLOUDSERVICEMANAGER);
		this._systemConfigManager = app.locator.getService(ServiceName.SYSTEMCONFIGURATIONMANAGER);
	};
	
	/**
	 * Checks if the current application has been registered with "the cloud"
	 * 
	 * @returns {Boolean}
	 */
	_manager.prototype.applicationIsRegistered = function() {
		var appMode = app.model.get(ModelName.APPLICATIONSTATE).get('mode');
		
		if (appMode == diary.model.ApplicationState.MODE.PRODUCTION) {
			var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION); 
			//TODO : confirm change
			//change for login
			if (systemConfig.get('token') == null) {
				return false;
			}
			/*			
			alert("registration manger"+systemConfig.get('token'))
			if (systemConfig.get('email') == null) {
				return false;
			}
*/			
			
		} // if they're in any other mode then treat the app as though it is registered
		
		return true;
	};

	/**
	 * Register the current application with a supplied token 
	 * 
	 * @param token
	 * @param successCallback
	 * @param errorCallback
	 */
	_manager.prototype.registerApp = function(email, password, domain, successCallback, errorCallback) {
		var manager = this;
		var onSuccess = function(result) {
				//alert("alert : registerApp1")
				saveSchoolid(result.schoolid);
				getSchoolInfo(result.schoolid);
			    saveMacAdress(app.model.get(ModelName.SYSTEMCONFIGURATION).get('macAddresses'));				
				if (!result.success) {
					errorCallback(result.error);
					return;
				}
				
				// save token 
				var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
				systemConfig.set('token', email);
				systemConfig.set('email', email);
				systemConfig.set('lastSchoolProfileCheck', null);	// Force a check of the school profile on next Startup
				manager._systemConfigManager.setSystemConfiguration(systemConfig, successCallback, errorCallback);
				//alert("alert : registerApp2")
				//downloadLogoUrl();
				//alert("alert : registerApp3")
			},
			onError = function(errorMessage) { 
					if (errorMessage == 'Error contacting server') {
						errorMessage = "Unable to complete token registration. Service currently unavailable. Please try again later.";
					}
					errorCallback(errorMessage);
					
			};
			manager._cloudManager.registerUser(email, password, domain, onSuccess, onError);
	};	
	
	// return the generated registration manager
	return _manager;
		
})();

