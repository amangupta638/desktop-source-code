/**
 * The local SQL delegate for CLass.  The delegate will use SQL queries to actually
 * get data from a local database. 
 * 
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.ClassSQLDelegate = (function() {
	
	/**
	 * Constructs a Class Delegate.  
	 * 
	 * @constructor
	 */
	var _delegate = function() {	
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};

	/**
	 * Searches classes with the given search criteria. The search is performed on class name
	 * 
	 * The search function will return a bunch of identifiers related to classes so that processing of classess
	 * is done in a quick way.
	 * 
	 * @param {!string} criteria The search criteria that is to be used to search the diary items
	 * @param {function(classIds)} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.searchClasses = function(criteria, successCallback, errorCallback) {
		console.log("search class");
		// check that the required inputs where given
		if (!criteria) {
			throw new Error("Criteria must be defined, criteria is not optional");
		}

		this._database.runQuery(
				// query
				"SELECT id " +
				"FROM class " +
				"WHERE Name LIKE :searchCriteria group by UniqueID",
				

				// params
				{ 'searchCriteria' : "%" + criteria + "%" },

				// success
				function (statement) {
					// we just want the ids from the classes
					successCallback(_.map(statement.getResult().data, function(item) {
						return item.id;
					}));
				},

				// error
				errorCallback, "Error while trying to search classes"
		);
	};
	// return the generated class delegate
	return _delegate;
})();
