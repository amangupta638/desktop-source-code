diary = diary || {};
diary.service = diary.service || {};

diary.service.DiaryDatabase = (function() {
	
	/**
	 * Diary Database
	 * @constructor
	 */
	var service = function() {
		//console.log('DiaryDatabase()');

		this.localDatabase = app.locator.getService("localDatabase");
		
		var folder = air.File.applicationStorageDirectory;
		this.dbFile = folder.resolvePath("diary.pd");
		
		this.cachedConnection = null;
		
	};
	
	/**
	 * Retrieve the SQLConnection for the diary database in ASYNC mode.
	 * @param {diary.service.DiaryDatabase} service The database service that is running in the background
	 * @param {function(air.SQLConnection)} successCallback Success callback once the database connection is established
	 * @param {function()} errorCallback Error callback if the database connection could not be established
	 * @private
	 */
	function connectionAsync(service, successCallback, errorCallback) {
		console.log("DiaryDatabase::connectionAsync()");
		
		if (service.cachedConnection != null) {
			successCallback(service.cachedConnection);
			return;
		}
		
		service.localDatabase.connection(service.dbFile, connectionSuccess, errorCallback);
		
		/**
		 * Connection retrieved successfully
		 * @private
		 */
		function connectionSuccess(connection) {
			service.cachedConnection = connection;
			successCallback(service.cachedConnection);
		}	

	};
	
	/**
	 * Retrieve the SQLConnection for the diary database in SYNC mode.
	 * @param {diary.service.DiaryDatabase} service The database service that is running in the background
	 * 
	 * @return {air.Connection} The connection to the database
	 * @private
	 */
	function connectionSync(service) {
		//console.log("DiaryDatabase::connectionSync()");
		
		var connection;
		
		if (service.cachedConnection != null) {
			connection = service.cachedConnection;
		} else {
			
			// create the connection
			connection = service.localDatabase.connection(service.dbFile);
			// cache the connection
			service.cachedConnection = connection;
		}
		
		// return the connection
		return connection;

	};
	
	service.prototype.setDatabase = function(dbName) {
		//console.log('DiaryDatabase.setDatabase(' + dbName + ')');

		this.localDatabase = app.locator.getService("localDatabase");
		
		var folder = air.File.applicationStorageDirectory;
		
		this.dbFile = folder.resolvePath(dbName + ".pd");
	};

	
	/**
	 * Drops the database
	 */
	service.prototype.dropDatabase = function() {
		//console.log("DiaryDatabase::dropDatabase()");
		
		if (this.cachedConnection != null) {
			this.cachedConnection.close();
		}
		
		if (this.dbFile.exists) {
			this.dbFile.deleteFile();
		}
		this.cachedConnection = null;
	};
	
	/**
	 * Returns a boolean indicating if the database exists
	 */
	service.prototype.databaseExists = function() {
		return this.dbFile.exists;
	};
	

	/**
	  * Run a series of SQL statements.  Supports separate programatic rollback where a SQL statement fails.
	  * @param {Array.<Object.<string, string, string>>} scripts in the format required by Rocketboots.data.air.LocalDatabase
	  * @param {function()} successCallback Called once all sql statements in scripts have been run [OPTIONAL]
	  * @param {function(boolean)} errorCallback Called if error while executing sql statements. Boolean indicates if all rollbacks were executed successfully. [OPTIONAL]
	  * @param {?string} default error message to pass to errorCallback [OPTIONAL]
	  *
	  * 
	  * @return {air.SQLStatement} The statement that was executed or UNDEFINED if the callback functions are specified
	  */
	service.prototype.runScripts = function(script, successCallback, errorCallback, errorMessage) {
		//console.log("DiaryDatabase::runScripts()");
		
		var service = this, errorWrapper;

		// if we have been supplied a default error message and an errorCallback, then construct a wrapper
		if(errorMessage && errorCallback) {
			errorWrapper = function() {
				errorCallback(errorMessage);
			}
			// otherwise just use the errorCallback if one exists
		} else {
			errorWrapper = errorCallback || $.noop;
		}

		if (successCallback || errorCallback) {
		
			connectionAsync(service,
				function connectionSuccess(sqlConnection) {
					service.localDatabase.runScripts(script, sqlConnection, successCallback, errorWrapper);
				}, errorWrapper);

		} else {
			
			var sqlConnection = connectionSync(service);
			service.localDatabase.runScripts(script, sqlConnection);
			
		}
	};
	
	/**
	  * Run a SQL statement against the Diary Database.
	  * @param {string} text A SQL statement to execute
	  * @param {?Object} parameters Parameters to the SQL query
	  * @param {?function()} successCallback Called once all sql statements in scripts have been run [OPTIONAL]
	  * @param {?function(string)} errorCallback Called if error while executing sql statements. Boolean indicates if all rollbacks were executed successfully. [OPTIONAL]
	  * @param {?string} default error message to pass to errorCallback [OPTIONAL]
	  *
	  * @return {air.SQLStatement} The statement that was executed or UNDEFINED if the callback functions are specified
	  */
	service.prototype.runQuery = function(text, parameters, successCallback, errorCallback, errorMessage) {
		//console.log("DiaryDatabase::runQuery()");
		
		var service = this, errorWrapper;

		// if we have been supplied a default error message and an errorCallback, then construct a wrapper
		if(errorMessage && errorCallback) {
			errorWrapper = function() {
				errorCallback(errorMessage);
			}
		// otherwise just use the errorCallback if one exists
		} else {
			errorWrapper = errorCallback || $.noop;
		}

		if (successCallback || errorCallback) {
		
			connectionAsync(service,
					function connectionSuccess(sqlConnection) {
						return service.localDatabase.runQuery(text, parameters, sqlConnection, successCallback, errorWrapper);
					}, errorWrapper);

		} else {
			
			var sqlConnection = connectionSync(service);
			return service.localDatabase.runQuery(text, parameters, sqlConnection);

		}
	};
	
	return service;
	
})();
