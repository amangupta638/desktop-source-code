/**
 * The manager that will install the demo data
 * NOTE - Eventually this will be deleted after the demo as it will no longer be required
 * 
 * @author Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DemoDataManager = (function() {

	/**
	 * Constructs a service to manage the demo data
	 * 
	 * @constructor
	 */
	var _service = function() {

		// get the database that is being used for the application
		this._database = app.locator.getService(ServiceName.APPDATABASE);

	};

	/**
	 * Will install the demo data
	 */
	_service.prototype.installDemoData = function() {

		// the queries that is to be executed to get the latest version
		var queries;

		// util function
		var getTimeFromHourMinute = function (time) { // time = HH:MM
			var ts = new Date("Jan 1, 1970 " + time + ":00").getTime();
			// convert back like so:	
			// new Date(ts).toTimeString().split(":").slice(0,2).join(":")
			return ts;
		};

		var getTwoWeeksAgoMon = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = -(13 + day.getDay());
			return day.addDays(daysToAdd);
		};

		var getOneWeekAgoMon = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = -(6 + day.getDay());
			return day.addDays(daysToAdd);
		};

		var getLastCycleDay = function () {
			var day = new Rocketboots.date.Day(),
				dow = day.getDay(),
				daysToAdd;

			// if Sun, it's two days ago
			if (dow == 0)
				daysToAdd = -2;
			// else if Mon, it's three days ago
			else if (dow == 1)
				daysToAdd = -3;
			// else it's yesterday
			else
				daysToAdd = -1;
			return day.addDays(daysToAdd);
		};

		var getCurrentCycleDay = function () {
			var day = new Rocketboots.date.Day(),
				dow = day.getDay(),
				daysToAdd;

			// if Sun, it's one day ahead
			if (dow == 0)
				daysToAdd = +1;
			// if Sat, it's two days ahead
			else if (dow == 6)
				daysToAdd = +2;
			// else it's today
			else
				daysToAdd = 0;
			return day.addDays(daysToAdd);
		};

		var getNextCycleDay = function () {
			var day = new Rocketboots.date.Day(),
				dow = day.getDay(),
				daysToAdd;

			// if Fri, it's three days ahead
			if (dow == 5)
				daysToAdd = 3;
			// else if Sat, it's two days ahead
			else if (dow == 6)
				daysToAdd = 2;
			// else it's tomorrow
			else
				daysToAdd = 1;
			return day.addDays(daysToAdd);
		};

		var getThisWeekWed = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 3 - day.getDay();
			return day.addDays(daysToAdd);
		};
		
		var getNextWeekThu = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 11 - day.getDay();
			return day.addDays(daysToAdd);
		};

		var getTwoWeeksWed = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 17 - day.getDay();
			return day.addDays(daysToAdd);
		};

		var getThreeWeeksWed = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 24 - day.getDay();
			return day.addDays(daysToAdd);
		};

		var getFourWeeksMon = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 29 - day.getDay();
			return day.addDays(daysToAdd);
		};

		var getFourWeeksTue = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 30 - day.getDay();
			return day.addDays(daysToAdd);
		};

		var getFourWeeksFri = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 33 - day.getDay();
			return day.addDays(daysToAdd);
		};

		var getFiveWeeksMon = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 36 - day.getDay();
			return day.addDays(daysToAdd);
		};

		var getFiveWeeksTue = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 37 - day.getDay();
			return day.addDays(daysToAdd);
		};

		var getFiveWeeksFri = function () {
			var day = new Rocketboots.date.Day(),
				daysToAdd = 40 - day.getDay();
			return day.addDays(daysToAdd);
		};
		
		var getThisMonthDay = function (dayOfMonth) {
			var date = new Date(),
				day = new Rocketboots.date.Day(date.getFullYear(), date.getMonth(), dayOfMonth);
			return day;
		};

		// write a log entry to indicate the start of the
		// installation of the demo data
		//console.log("Installing Demo Data");

		try {

			// create the query object
			queries = new Array();

			// ------------------------
			//   School Profile
			// ------------------------

            /* Moved to manifest */

			// ------------------------
			//   Student Profile
			// ------------------------

			queries.push({
				'sql' : "DELETE FROM studentProfile"
			});

			queries.push({
				'sql' : "INSERT INTO User (id, name, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, teacher, house, houseCaptain, houseColours) VALUES "
					+ "(1, 'James Thompson', " + new Date(1996, 1, 15).getTime() + ", '2/92, The Sample Street', 'North Sydney', 'NSW', '2060', '(02) 9568 5698', "
					+ "'james.thompson@dynamichighschool.nsw.edu.au', '11', 'Mr T.Byrnes (English 2U), Mrs J.Jacobs (Maths 2U)', 'Banksia', 'Scott Tyler', 'Red')",
				'rollback' : "DELETE FROM studentProfile"
			});

			// ------------------------
			//   Subjects
			// ------------------------

			queries.push({
				'sql' : "DELETE FROM subject"
			});

			queries.push({
				'sql' : "INSERT INTO subject (id, name, colour) VALUES "
					+ "(1, 'Maths', 'orange')",
				'rollback' : "DELETE FROM subject WHERE id = 1"
			});

			queries.push({
				'sql' : "INSERT INTO subject (id, name, colour) VALUES "
					+ "(2, 'English', 'purple')",
				'rollback' : "DELETE FROM subject WHERE id = 2"
			});

			queries.push({
				'sql' : "INSERT INTO subject (id, name, colour) VALUES "
					+ "(3, 'Physics', 'aqua')",
				'rollback' : "DELETE FROM subject WHERE id = 3"
			});

			queries.push({
				'sql' : "INSERT INTO subject (id, name, colour) VALUES "
					+ "(4, 'Chemistry', 'lightbrown')",
				'rollback' : "DELETE FROM subject WHERE id = 4"
			});

			queries.push({
				'sql' : "INSERT INTO subject (id, name, colour) VALUES "
					+ "(5, 'Industrial Arts', 'skyblue')",
				'rollback' : "DELETE FROM subject WHERE id = 5"
			});

			queries.push({
				'sql' : "INSERT INTO subject (id, name, colour) VALUES "
					+ "(6, 'French', 'red')",
				'rollback' : "DELETE FROM subject WHERE id = 6"
			});

			// ------------------------
			//   Periods
			// ------------------------

            /* Now read in from manifest */

			// ------------------------
			//   Classes
			// ------------------------

			// Cycle Day 1
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId, room) VALUES "
					+ "(1, 1, 1, 2, 1, 'Room 101')",
				'rollback' : "DELETE FROM class WHERE id = 1"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId, room) VALUES "
					+ "(2, 1, 1, 3, 2, NULL)",
				'rollback' : "DELETE FROM class WHERE id = 2"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId, room) VALUES "
					+ "(3, 1, 1, 5, 3, 'Extension Building 880, Room 3B')",
				'rollback' : "DELETE FROM class WHERE id = 3"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId, room) VALUES "
					+ "(4, 1, 1, 6, 4, 'Room 22')",
				'rollback' : "DELETE FROM class WHERE id = 4"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId, room) VALUES "
					+ "(5, 1, 1, 8, 5, 'Room 101')",
				'rollback' : "DELETE FROM class WHERE id = 5"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId, room) VALUES "
					+ "(6, 1, 1, 9, 6, 'Room 101')",
				'rollback' : "DELETE FROM class WHERE id = 6"
			});

			// Cycle Day 2
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(7, 1, 2, 2, 1)",
				'rollback' : "DELETE FROM class WHERE id = 7"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(8, 1, 2, 3, 1)",
				'rollback' : "DELETE FROM class WHERE id = 8"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(9, 1, 2, 5, 2)",
				'rollback' : "DELETE FROM class WHERE id = 9"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(10, 1, 2, 6, 2)",
				'rollback' : "DELETE FROM class WHERE id = 10"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(11, 1, 2, 8, 3)",
				'rollback' : "DELETE FROM class WHERE id = 11"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(12, 1, 2, 9, 3)",
				'rollback' : "DELETE FROM class WHERE id = 12"
			});

			// Cycle Day 3
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(13, 1, 3, 2, 1)",
				'rollback' : "DELETE FROM class WHERE id = 13"
			});

			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(14, 1, 3, 3, 1)",
				'rollback' : "DELETE FROM class WHERE id = 14"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(15, 1, 3, 5, 1)",
				'rollback' : "DELETE FROM class WHERE id = 15"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(16, 1, 3, 6, 2)",
				'rollback' : "DELETE FROM class WHERE id = 16"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(17, 1, 3, 8, 2)",
				'rollback' : "DELETE FROM class WHERE id = 17"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(18, 1, 3, 9, 2)",
				'rollback' : "DELETE FROM class WHERE id = 18"
			});

			// Cycle Day 4
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(19, 1, 4, 2, 6)",
				'rollback' : "DELETE FROM class WHERE id = 19"
			});

			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(20, 1, 4, 3, 6)",
				'rollback' : "DELETE FROM class WHERE id = 20"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(21, 1, 4, 5, 6)",
				'rollback' : "DELETE FROM class WHERE id = 21"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(22, 1, 4, 6, 6)",
				'rollback' : "DELETE FROM class WHERE id = 22"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(23, 1, 4, 8, 6)",
				'rollback' : "DELETE FROM class WHERE id = 23"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(24, 1, 4, 9, 6)",
				'rollback' : "DELETE FROM class WHERE id = 24"
			});

			// Cycle Day 5
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(25, 1, 5, 2, 5)",
				'rollback' : "DELETE FROM class WHERE id = 25"
			});

			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(26, 1, 5, 3, 5)",
				'rollback' : "DELETE FROM class WHERE id = 26"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(27, 1, 5, 5, 5)",
				'rollback' : "DELETE FROM class WHERE id = 27"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(28, 1, 5, 6, 5)",
				'rollback' : "DELETE FROM class WHERE id = 28"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(29, 1, 5, 8, 5)",
				'rollback' : "DELETE FROM class WHERE id = 29"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(30, 1, 5, 9, 5)",
				'rollback' : "DELETE FROM class WHERE id = 30"
			});

			// Cycle Day 6
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(31, 1, 6, 2, 1)",
				'rollback' : "DELETE FROM class WHERE id = 31"
			});

			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(32, 1, 6, 4, 2)",
				'rollback' : "DELETE FROM class WHERE id = 32"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(33, 1, 6, 6, 3)",
				'rollback' : "DELETE FROM class WHERE id = 33"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(34, 1, 6, 8, 4)",
				'rollback' : "DELETE FROM class WHERE id = 34"
			});

			// Cycle Day 8
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(35, 1, 8, 4, 1)",
				'rollback' : "DELETE FROM class WHERE id = 35"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(36, 1, 8, 7, 2)",
				'rollback' : "DELETE FROM class WHERE id = 36"
			});

			// Cycle Day 9
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(37, 1, 9, 1, 5)",
				'rollback' : "DELETE FROM class WHERE id = 37"
			});

			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(38, 1, 9, 2, 5)",
				'rollback' : "DELETE FROM class WHERE id = 38"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(39, 1, 9, 3, 5)",
				'rollback' : "DELETE FROM class WHERE id = 39"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(40, 1, 9, 4, 5)",
				'rollback' : "DELETE FROM class WHERE id = 40"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(41, 1, 9, 5, 5)",
				'rollback' : "DELETE FROM class WHERE id = 41"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(42, 1, 9, 6, 5)",
				'rollback' : "DELETE FROM class WHERE id = 42"
			});

			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(43, 1, 9, 7, 5)",
				'rollback' : "DELETE FROM class WHERE id = 43"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(44, 1, 9, 8, 5)",
				'rollback' : "DELETE FROM class WHERE id = 44"
			});
			
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(45, 1, 9, 9, 5)",
				'rollback' : "DELETE FROM class WHERE id = 45"
			});

			// Cycle Day 10
			queries.push({
				'sql' : "INSERT INTO class (id, timetable, cycleDay, periodId, subjectId) VALUES "
					+ "(46, 1, 10, 1, 1)",
				'rollback' : "DELETE FROM class WHERE id = 46"
			});

			// ------------------------
			//   DayOfNote
			// ------------------------

            /* Now read in from manifest */

			// ------------------------
			//   Timetable
			// ------------------------

            /* Now read in from manifest */

			// ------------------------
			//   Diary Items
			// ------------------------
			
			 /* Locked events are now read in from manifest */
			 
			queries.push({
				'sql' : "DELETE FROM diaryItem WHERE type <> 'EVENT' or (type = 'EVENT' and locked <> true)"
			});

			queries.push({
				'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
					+ "(1, 'ASSIGNMENT', "
					+ getLastCycleDay().getTime()
					+ ", null, null, 1, 'Cultural Research Assignment', false, null, 1, 'Read textbook chapter 5 before starting.')",
				'rollback' : "DELETE FROM diaryItem WHERE id = 1"
			});

			queries.push({
				'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
					+ "(2, 'NOTE', "
					+ getLastCycleDay().getTime()
					+ ", null, null, 1, 'Note', false, null, 1, null)",
				'rollback' : "DELETE FROM diaryItem WHERE id = 2"
			});

			// Note Entries
			queries.push({
				'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
					+ "(1, 2, "
					+ (getLastCycleDay().addDays(-1).getTime() + 1000*3600*8.1)
					+ ", 'J.Jacobs - You can hand this in tomorrow')",
				'rollback' : "DELETE FROM noteEntry WHERE id = 1"
			});

			queries.push({
				'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
					+ "(2, 2, "
					+ (getLastCycleDay().getTime() + 1000*3600*6.7)
					+ ", 'P.Thompson - James'' sister was taken to hospital last night')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 2"
			});

			queries.push({
				'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
					+ "(3, 2, "
					+ (getLastCycleDay().getTime() + 1000*3600*8.6)
					+ ", 'J.Jacobs - Sorry to hear that, another days extension ok')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 3"
			});

			queries.push({
				'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
					+ "(3, 'EVENT', "
					+ getLastCycleDay().getTime()
					+ ", "
					+ getTimeFromHourMinute("08:45")
					+ ", "
					+ getTimeFromHourMinute("09:15")
					+ ", null, 'Roller Derby Guest Speaker', false, null, null, null)",
				'rollback' : "DELETE FROM diaryItem WHERE id = 3"
			});

			queries.push({
				'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
					+ "(4, 'HOMEWORK', "
					+ getNextCycleDay().getTime()
					+ ", null, null, 9, 'Make a Cabin', false, null, 5, \"Don't forget to take a photograph.\")",
				'rollback' : "DELETE FROM diaryItem WHERE id = 4"
			});
			
			// Items on the 7th and 19th of the current month - to test days with maximum amount of data for display
			{
				// 4 items and 3 events on a day
				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(7, 'HOMEWORK', "
						+ getThisMonthDay(7).getTime()
						+ ", null, null, null, 'Textbook Questions', false, null, 1, \"Chapter 3, Questions 5-8.\")",
						'rollback' : "DELETE FROM diaryItem WHERE id = 7"
				});
				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(8, 'ASSIGNMENT', "
						+ getThisMonthDay(7).getTime()
						+ ", " + getTimeFromHourMinute("11:35") + ",  " + getTimeFromHourMinute("11:55") + ", null, 'Essay', false, null, 2, \"Beyond the Physical Journey.\")",
						'rollback' : "DELETE FROM diaryItem WHERE id = 8"
				});
				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(9, 'ASSIGNMENT', "
						+ getThisMonthDay(7).getTime()
						+ ", null, null, null, 'Memorise Formula Sheet', false, null, 3, \"In-class test.\")",
						'rollback' : "DELETE FROM diaryItem WHERE id = 9"
				});
				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(10, 'HOMEWORK', "
						+ getThisMonthDay(7).getTime()
						+ ", null, null, null, 'Create a Chemical', false, null, 4, \"Bring your demos to class.\")",
						'rollback' : "DELETE FROM diaryItem WHERE id = 10"
				});
								
				// 3 items on a day
				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(14, 'HOMEWORK', "
						+ getThisMonthDay(19).getTime()
						+ ", null, null, null, 'Charcoal Sketch', false, null, 5, \"We'll treat them in class to prevent blurring.\")",
						'rollback' : "DELETE FROM diaryItem WHERE id = 14"
				});
				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(15, 'ASSIGNMENT', "
						+ getThisMonthDay(19).getTime()
						+ ", null, null, null, 'Paris culture essay', false, null, 6, \"Due in by 5pm.\")",
						'rollback' : "DELETE FROM diaryItem WHERE id = 15"
				});
				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(16, 'HOMEWORK', "
						+ getThisMonthDay(19).getTime()
						+ ", null, null, null, 'Short Story', false, null, 2, \"Creative writing - topic of your choice. Maximum 1000 words.\")",
						'rollback' : "DELETE FROM diaryItem WHERE id = 16"
				});
				
				// more notes
				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(17, 'NOTE', "
						+ getTwoWeeksAgoMon().addDays(2).getTime()
						+ ", "
						+ getTimeFromHourMinute("08:00")
						+ ", null, null, 'Note', false, null, null, null)",
					'rollback' : "DELETE FROM diaryItem WHERE id = 17"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(4, 17, "
						+ (getTwoWeeksAgoMon().addDays(2).getTime() + getTimeFromHourMinute("10:15"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 4"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(18, 'NOTE', "
						+ getThisWeekWed().addDays(2).getTime()
						+ ", null, null, 1, 'Note from Teacher', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 18"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(5, 18, "
						+ (getThisWeekWed().addDays(2).getTime() + getTimeFromHourMinute("10:25"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 5"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(19, 'NOTE', "
						+ getThisWeekWed().addDays(3).getTime()
						+ ", null, null, 5, 'Note from Parent', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 19"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(6, 19, "
						+ (getTwoWeeksAgoMon().addDays(2).getTime() + getTimeFromHourMinute("09:22"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 6"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(20, 'NOTE', "
						+ getThisWeekWed().addDays(5).getTime()
						+ ", "
						+ getTimeFromHourMinute("10:15")
						+ ", null, null, 'Note to Self', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 20"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(7, 20, "
						+ (getTwoWeeksAgoMon().addDays(2).getTime() + getTimeFromHourMinute("10:22"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 7"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(21, 'NOTE', "
						+ getNextWeekThu().getTime()
						+ ", null, null, 3, 'Another Note', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 21"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(8, 21, "
						+ (getTwoWeeksAgoMon().addDays(6).getTime() + getTimeFromHourMinute("12:00"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 8"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(22, 'NOTE', "
						+ getNextWeekThu().getTime()
						+ ", null, null, 1, 'Note', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 22"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(9, 22, "
						+ (getTwoWeeksAgoMon().addDays(6).getTime() + getTimeFromHourMinute("16:00"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 9"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(23, 'NOTE', "
						+ getNextWeekThu().addDays(3).getTime()
						+ ", "
						+ getTimeFromHourMinute("14:20")
						+ ", null, null, 'Do Not Forget!', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 23"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(10, 23, "
						+ (getTwoWeeksAgoMon().addDays(6).getTime() + getTimeFromHourMinute("14:21"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 10"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(24, 'NOTE', "
						+ getNextWeekThu().addDays(2).getTime()
						+ ", "
						+ getTimeFromHourMinute("16:25")
						+ ", null, null, 'FYI', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 24"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(11, 24, "
						+ (getTwoWeeksAgoMon().addDays(6).getTime() + getTimeFromHourMinute("17:11"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 11"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(25, 'NOTE', "
						+ getTwoWeeksWed().getTime()
						+ ", null, null, 8, 'Note this Note', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 25"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(12, 25, "
						+ (getTwoWeeksAgoMon().addDays(6).getTime() + getTimeFromHourMinute("07:50"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 12"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(26, 'NOTE', "
						+ getTwoWeeksWed().addDays(2).getTime()
						+ ", null, null, 8, 'Future Stuff', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 26"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(13, 26, "
						+ (getTwoWeeksAgoMon().addDays(6).getTime() + getTimeFromHourMinute("07:04"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 13"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(27, 'NOTE', "
						+ getThreeWeeksWed().getTime()
						+ ", null, null, 2, 'Reminder', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 27"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(14, 27, "
						+ (getTwoWeeksAgoMon().addDays(6).getTime() + getTimeFromHourMinute("17:30"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 14"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(28, 'NOTE', "
						+ getThreeWeeksWed().getTime()
						+ ", "
						+ getTimeFromHourMinute("06:10")
						+ ", null, null, 'Tickler', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 28"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(15, 28, "
						+ (getTwoWeeksAgoMon().addDays(5).getTime() + getTimeFromHourMinute("07:35"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 15"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(29, 'NOTE', "
						+ getThreeWeeksWed().addDays(1).getTime()
						+ ", "
						+ getTimeFromHourMinute("11:11")
						+ ", null, null, 'Too far forward to worry about', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 29"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(16, 29, "
						+ (getTwoWeeksAgoMon().addDays(12).getTime() + getTimeFromHourMinute("11:05"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 16"
				});

				queries.push({
					'sql' : "INSERT INTO diaryItem (id, type, date, startTime, endTime, period, title, locked, completed, subjectId, description) VALUES "
						+ "(30, 'NOTE', "
						+ getFiveWeeksFri().getTime()
						+ ", null, null, 1, 'Note', false, null, 1, null)",
						'rollback' : "DELETE FROM diaryItem WHERE id = 30"
				});

				queries.push({
					'sql' : "INSERT INTO noteEntry (id, noteId, timestamp, text) VALUES "
						+ "(17, 30, "
						+ (getTwoWeeksAgoMon().addDays(10).getTime() + getTimeFromHourMinute("14:47"))
						+ ", 'J.Jacobs - This is a test entry')",
					'rollback' : "DELETE FROM noteEntry WHERE id = 17"
				});
				
				
				// ****** Include ARTICLE searchable indexing ******
				
				/* moved to manifest */
			
			}

			// execute all the queries
			this._database.runScripts(queries);
			//console.log("Successful installation of Demo Data");

		} catch (error) {
			//console.log("Error while trying to load demo data into application");
			//console.log(error);

			// throw an error so that we can stop the loading process
			throw new Error("Error while installing demo data");
		}

	};

	// return the generated service
	return _service;

})();