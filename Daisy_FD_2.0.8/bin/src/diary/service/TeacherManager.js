/**
 * The manager for Teacher Management.  The manager is used to take care of all the business
 * logic related to a techer profile.

 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.TeacherManager = (function() {
	
	/**
	 * Constructs a School Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		this._teacherDetailsDelegate = app.locator.getService(ServiceName.TEACHERDETAILSDELEGATE);
		this._cloudManager = app.locator.getService(ServiceName.THECLOUDSERVICEMANAGER);
	};
	
	/**
	 * Gets the profile of the teacher.  The profile that is passed will be updated with the details from the data source
	 * 
	 * @param {function(diary.model.Teacher)} successCallback The function that is to be called on successful fetch details [OPTIONAL]
	 * @param {function(message)} errorCallback The function that is to be called on failure of the fetch details [OPTIONAL]
	 * @param {diary.model.Teacher} The profile for the teacher to be updated if any [OPTIONAL]
	 */
	_manager.prototype.getTeacherDetails = function(successCallback, errorCallback, teacher) {
		alert("get teacher details 1");
		// check that a valid type was specified
		
		// if a teacher object was not specified
		// we will create one...
		teacher = teacher || new diary.model.Teacher();

		// get teacher details from server
		alert("get teacher details 2");
		this._teacherDetailsDelegate.getTeacherDetailsServer(
				
				function success(teacherDetails) {
					if(teacherDetails) {
						teacher.set(teacherDetails.attributes);
						successCallback(teacher);
					} else {
						errorCallback && errorCallback("Teacher details not found");
					}
				}, errorCallback || $.noop
		);
	};

    /**
     * Sets the teacher
     *
     * @param teacher
     * @param successCallback
     * @param errorCallback
     */
    _manager.prototype.setTeacherProfile = function(teacher, successCallback, errorCallback) {
        // check that a valid type was specified
        if (!(teacher instanceof diary.model.Teacher)) {
            throw new Error("teacher profile is not valid, profile must be of type diary.model.Teacher");
        }

        this._teacherDetailsDelegate.updateTeacherProfile(teacher, successCallback, errorCallback);
    };
	
	// return the generated teacher details
	return _manager;
})();

