/**
 * The manager for handling class data
 *
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.ClassManager = (function() {

	var _manager = function() {
		this._delegate = app.locator.getService(ServiceName.CLASSDELEGATE);
	};

	
	
	// return the generated manager
	return _manager;

})();

