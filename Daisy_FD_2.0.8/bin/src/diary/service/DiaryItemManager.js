/**
 * The manager for Diary Item Manager.  The idea of the manager is to abstract the application from
 * the data access storage methods.  All the business logic that is related to a Diary Item needs to
 * be in here
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DiaryItemManager = (function() {
	
	/**
	 * Constructs a Diary Item Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		this._diaryItemDelegate = app.locator.getService(ServiceName.DIARYITEMDELEGATE);
		this._attachmentFileDelegate = app.locator.getService(ServiceName.ATTACHMENTFILEDELEGATE);
		this._attachmentDBDelegate = app.locator.getService(ServiceName.ATTACHMENTDBDELEGATE);
	};
	
	/**
	 * @public
	 * Gets all the Diary Items of a student for a particular date range
	 * 
	 * @param {!date} fromDate The date from when diary items should be retrieved
	 * @param {!date} toDate The date to when diary items should be retrieved
	 * @param {!diary.collection.SubjectCollection} subjects The list of subjects that are to be related to the loaded diary items if necessary
	 * @param {!function(diary.collection.DiaryItemCollection)} successCallback The function that is to be called if all required diary items are fetched
	 * @param {function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.getDiaryItemsForDayRange = function(fromDate, toDate, subjects, isNote, isTask, isEvent, successCallback, errorCallback) {
		
		var manager = this;
		// check that the dates where given
		if (!fromDate) {
			throw new Error("From date must be defined, date is not optional");
		}
		
		if (!toDate) {
			throw new Error("To date must be defined, date is not optional");
		}
		
		if (!subjects) {
			throw new Error("Subjects must be defined, subjects is not optional");
		}
		
		// check that the from date is before the to date
		if (fromDate.getTime() > toDate.getTime()) {
			throw new Error("To date is not valid, date must be after from date");
		}

		// fetch fido fetch - all the diary items for the student
        manager._diaryItemDelegate.getDiaryItemsForDayRange(fromDate, toDate, subjects, isNote, isTask, isEvent, 
				function success(diaryItems) {
					manager._attachmentDBDelegate.getAttachmentsForDiaryItems(diaryItems, successCallback, errorCallback);
				}, errorCallback);
	};
	
	/**
	 * @public
	 * Add a file attachment to a diary item. 
	 * The file will be copied into the applications storage directory.
	 * 
	 * @param {!string} pathToAttachment Native system file path to the file to be attached.
	 * @param {!number} diaryItemId Identifier of the diary item to attach to.
	 * @param {?function(diary.model.Attachment)} successCallback Called upon success
	 * @param {?function} errorCallback Called upon failure
	 */
	_manager.prototype.addAttachmentToDiaryItem = function (pathToAttachment, diaryItemId, successCallback, errorCallback) {
		
		var manager = this;
		
		manager._attachmentFileDelegate.upload(
				pathToAttachment, 
				
				// success
				function (attachment) {
					
					// attachment has now been copied locally
					// so add it to the database
					manager._attachmentDBDelegate.addAttachmentToDiaryItem(
							attachment, diaryItemId, 
							
							// success
							function (attachmentId) {
								attachment.id = attachmentId;
								if (successCallback) {
									successCallback(attachment);
								}
							}, 
							
							// failure
							function (errorMessage) {
								
								// since we didn't update the DB, try to remove attachment from the file system
								manager._attachmentFileDelegate.destroy(attachment, 
										function () {
											//console.log("Removed attachment from file system after failed to add to data store.");
										},
										function (errorMessage) {
											//console.log("Failed to remove attachment from file system after failed to add to data store: " + errorMessage);
										});
								
								// callback
								if (errorCallback) {
									errorCallback(errorMessage);
								}
							});
					
				}, 
				
				// failure
				errorCallback
			);
		
	};
	
	/**
	 * @public
	 * Remove a file attached to a diary item, both from the data base and file system.
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem The diary item with the attachment to remove.
	 * @param {!diary.model.Attachment} attachment The attachment to be removed.
	 * @param {?function()} successCallback Called upon success, guarenteeing the file is no longer attached to the diary item.
	 * @param {?function(message)} errorCallback Called upon failure, implying the file is still attached to the diary item.
	 */
	_manager.prototype.removeAttachmentFromDiaryItem = function (diaryItem, attachment, successCallback, errorCallback) {
		
		var manager = this;
		
		// remove from data store first
		manager._attachmentDBDelegate.removeAttachment(
				attachment, 
				
				function success() {
					
					// remove from file-system second
					manager._attachmentFileDelegate.destroy(
							attachment,

							// success
							successCallback,

							// error
							function error(message) {
								//console.log("ERROR: after removing attachment '" + attachment.get('displayName') + "' from the data store, failed to remove from filesystem with error: " + message);
								
								// but still call the success callback, since it is now gone from the DB and therefore
								// will no longer appear in the model used by this managers caller
								// (and therefore will no longer exist as far as the user is concerned)
								successCallback();
							}
						);
					
				}, errorCallback
			);
		
	};
	
	/**
	 * @public
	 * Open a diary item's attachment using the systems native editor.
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem The diary item with the attachment to be opened. 
	 * @param {!diary.model.Attachment} attachment The attachment to be opened.
	 * @return {boolean} Success
	 */
	_manager.prototype.openAttachmentOnDiaryItem = function (diaryItem, attachment) {
		return this._attachmentFileDelegate.open(attachment);
	};
	
	/**
	 * Delete a diary item.  All records related to a diary item will be removed, even the attached files, if any, will be removed
	 * @param {!diary.model.DiaryItem} diaryItem The diary item that is to be deleted
	 * @param {!function} successCallback Called upon success
	 * @param {?function} errorCallback Called upon failure
	 */
	_manager.prototype.deleteDiaryItem = function (diaryItem, successCallback, errorCallback) {
		
		var manager = this;
		
		// check that both the diary item to be deleted and the success handler
		// were specified
		if (!diaryItem) {
			throw new Error("Diary Item must be defined, diary item is not optional");
		}

		if (!successCallback) {
			throw new Error("Success Callback must be defined, callback is not optional");
		}
		
		// the function to delete all the file attachments
		function deleteFileAttachments(attachments, done) {
			
			// append notes if any
			if (attachments && attachments.length > 0) {
			
				var failureMessages = [],
					i = 0;
				
				function iterate() {
					// iterate
					if (++i < attachments.length) {
						deleteFileAttachment();
					} else {
						// call the done callback handler
						done();
					}
				}
					
				function deleteFileAttachment() {
					// attachment
					var attachment = attachments.at(i);
					
					// append the note entry
					manager.removeAttachmentFromDiaryItem(diaryItem, attachment,
						
						// success function
						function () {
							iterate();
						}, 
					
						// error function
						function (errorMessage) {
							failureMessages.push(errorMessage);
							iterate();
						}
					);
				}
				
				// start deleting the attachment
				deleteFileAttachment();
				
			}  else {
				
				// call the done callback handler
				done();
			}
			
		}
		
		// we need to remove the file attachments if any first
		// NOTE - We don't really care if the diary item that is being deleted
		// actually has attachments or not (i.e. if it supports attachments).  if no
		// attachments are returned for the diary item that would mean that either it is
		// not supported by the diary item or the diary item (TASK) has no attachments
		deleteFileAttachments(diaryItem.get("attachments"), 
			// the done function for removing all the attachments
			function() {
				// delete the diary item because we deleted all attachments
				manager._diaryItemDelegate.deleteDiaryItem(diaryItem.get('id'), successCallback, errorCallback || $.noop);
			}
		);
		
	};
	
	/**
	 * Appends a note entry to the specified note entry
	 * 
	 * @param {!diary.mode.NoteEntry} noteEntry The note entry that is to be appended
	 * @param {!number} noteId The identifier of the diary item (note) to which the note entry is to be appended
	 * @param {!function(diary.model.NoteEntry} successCallback The function that is to be called if the note entry is successfully appended 
	 * @param {?function} errorCallback The function that is to be called is the append of the note entry fails
	 */
	_manager.prototype.appendNoteEntryToNote = function (timestamp, noteId, noteEntry, successCallback, errorCallback) {
	
		var manager = this;
		
		// check that both the diary item details to create and the success handler
		// were specified
		if (!noteEntry) {
			throw new Error("Note Entry must be defined, entry is not optional");
		}
		
		if (!noteId) {
			throw new Error("Note Identifier must be defined, identifier is not optional");
		}

		if (!successCallback) {
			throw new Error("Success Callback must be defined, callback is not optional");
		}
		
		// we first need to validate the given note entry to make sure that it
		// is a valid entry
		var validationError = noteEntry.validate(noteEntry.attributes);
		
		// if we have a problem with the validation we will not
		// save the new note entry
		if (validationError != null) {
			// indicate that an error was encountered
			(errorCallback ? errorCallback : $.noop)(validationError);
		} else {
			
			// we will try to save the note entry
			manager._diaryItemDelegate.appendNoteEntryToNote(timestamp, noteId, noteEntry, 
					
					// success
					function(noteEntryId) {
						noteEntry.set({'id' : noteEntryId}, {'silent' : true});
						successCallback(noteEntry);
					},
					
					// failure
					errorCallback
			);
		}
	},
	
	/**
	 * Create a diary item.
	 * @param {!diary.model.DiaryItem} diaryItem The diary item object that is to be created
	 * @param {?Array<string>} attachments The list of attachments (paths pointing to files) that are to be linked to the diary item, if any
	 * @param {!function(diary.model.DiaryItem)} successCallback Called upon success
	 * @param {?function} errorCallback Called upon failure
	 */
	_manager.prototype.createDiaryItem = function (diaryItem, attachments, successCallback, errorCallback) {
		console.log("(diaryItem) in create diaryItemManager: "+JSON.stringify(diaryItem));
		var manager = this;
		
		// check that both the diary item details to create and the success handler
		// were specified
		if (!diaryItem) {
			throw new Error("Diary Item must be defined, item is not optional");
		}

		if (!successCallback) {
			throw new Error("Success Callback must be defined, callback is not optional");
		}
		var timestamp=getTimestampmilliseconds();
		// the function to append note for a diary item function
		function appendNoteEntries(diaryItem,timestamp, done) {
			//alert("appendNoteEntries="+timestamp);
			// append notes if any
			if (diaryItem instanceof diary.model.Note &&
				diaryItem.get('entries') && diaryItem.get('entries').length > 0) {
					//alert("note entry");
				var failureMessages = [],
					noteEntries = diaryItem.get('entries'),
					i = 0;
				
				function iterate() {
					// iterate
					if (++i < noteEntries.length) {
						//alert("inside for"+timestamp);
						appendNoteEntry(timestamp);
					} else {
						// call the done callback handler
						done();
					}
					//alert("inside iterate")
				}
					
				function appendNoteEntry(timestamp) {
						//alert("appendNoteEntry="+timestamp);
					// note entry
					var noteEntry = noteEntries.at(i);
					//alert();
					// append the note entry
					manager.appendNoteEntryToNote(timestamp,diaryItem.get('id'),noteEntry,
						
						// success function
						function () {
							iterate();
						}, 
					
						// error function
						function (errorMessage) {
							failureMessages.push(errorMessage);
							iterate();
						}
					);
				}
				
				// start appending the note entries
				appendNoteEntry(timestamp);
				
			}  else {
				
				// call the done callback handler
				done();
			}

		}
		
		// the function to add all the attachments for the diary item
		function addAttachments(attachments, diaryItem, done) {
			
			// for each specified attachment we will need to upload the file and link the attachment to the diary item
			// if any
			if (attachments && attachments.length > 0) {
				
				var failureMessages = [], 
					i = 0;
				
				function iterate() {
					// iterate
					if (++i < attachments.length) {
						addAttachment();
					} else {
						// call the done callback handler
						done();
					}
				}
				
				function addAttachment() {
					
					// add the attachment to the diary item
					// NOTE - What if one of the file attachment fails? what are we to do?
					// I think we should ignore it and continue I know it might not be right but we don't
					// have a transaction here to roll back all changes
					manager.addAttachmentToDiaryItem(attachments[i], diaryItem.get('id'), 
						
						// success function
						function (attachment) {
							// add the attachment to the diary item
							diaryItem.get('attachments').add(attachment, {'silent' : true});
							iterate();
						}, 
					
						// error function
						function (errorMessage) {
							failureMessages.push(errorMessage);
							iterate();
						});
				};
				
				// start adding the attachments one by one
				addAttachment();
				
			} else {
			
				// call the done callback handler
				done();
			}
			
			
		}
		
		
		// we first need to validate the given diary item to make sure that it
		// is a valid entry
		var validationError = diaryItem.validate(diaryItem.attributes);
		
		// if we have a problem with the validation we will not
		// save the new diary item
		if (validationError != null) {
			// indicate that an error was encountered
			(errorCallback || $.noop)(validationError);
		} else {
			// we need to create the diary item first
			manager._diaryItemDelegate.createDiaryItem(diaryItem,timestamp, 
					
					// success
					function(diaryItemId) {
				        
						diaryItem.set({'id' : diaryItemId}, {'silent' : true});
						//alert("diaryItemId="+diaryItemId);
						// append the notes if any
						appendNoteEntries(diaryItem,timestamp, 
								
							// done function for including note entries
							function() {
							
								// append the attachments if any
								addAttachments(attachments, diaryItem, 
									
									// done function for adding file attachments
									function() {
									   var obj={};
									   obj.diaryItem=diaryItem;
									   obj.diaryItemId=diaryItemId
										successCallback(obj);
									}
								);
								
							}
						);
						
					},
					
					// failure
					errorCallback || $.noop
			);
		}
		
	};
	/*===============*/
	_manager.prototype.getDiaryitems = function (diaryItem, successCallback, errorCallback) {
        
        var manager = this;
        manager._diaryItemDelegate.getDiaryitems(diaryItem,function(values)
        {
       // console.log("values:DiaryItemManager");
        //console.log(values);
        successCallback(values);
        },
        function()
        {
        errorCallback || $.noop
        }); 
        
    };
    /*===============*/
    _manager.prototype.getNewRecords = function (diaryItem, successCallback, errorCallback) {
        
       // alert("DiaryItem Manager getNewRecords");
        var manager = this;
        manager._diaryItemDelegate.getNewRecords(diaryItem,function(values)
        {
       // console.log("values:DiaryItemManager");
       // console.log(values);
        successCallback(values);
        },
        function()
        {
        errorCallback || $.noop
        }); 
        
    };
    
    _manager.prototype.updateUniversalID = function (id, successCallback, errorCallback) {
        
        //alert("DiaryItem Manager updateUniversalID");
       // alert(id);
        var manager = this;
        manager._diaryItemDelegate.updateUniversalID(id,function(values)
        {
        //alert("updateUniversalID");
        //console.log(values);
        },
        function()
        {
        errorCallback || $.noop
        }); 
        
    };
    _manager.prototype.updateUniversalIDs = function (idArray, successCallback, errorCallback) {
        
        //alert("DiaryItem Manager updateUniversalID");
       // alert(id);
        var manager = this;
        manager._diaryItemDelegate.updateUniversalIDs(idArray,function(values)
        {
        //alert("updateUniversalID");
        //console.log(values);
        },
        function()
        {
        errorCallback || $.noop
        }); 
        
    };
    _manager.prototype.InsertNewRecords = function (diaryItem, successCallback, errorCallback) {
        
       // alert("DiaryItem Manager getDiaryitems");
        var manager = this;
        manager._diaryItemDelegate.InsertNewRecords("",function(values)
        {
       // console.log("values:InsertNewRecords");
       // console.log(values);
        successCallback(values);
        },
        function()
        {
        errorCallback || $.noop
        }); 
        
    };
     _manager.prototype.updateRecords = function (diaryItem, successCallback, errorCallback) {
        
       // alert("DiaryItem Manager getDiaryitems");
        var manager = this;
        manager._diaryItemDelegate.updateRecords("",function()
        {
      //  console.log("values:updateRecords");
      //  console.log();
        successCallback("");
        },
        function()
        {
        errorCallback || $.noop
        }); 
        
    };
    
    _manager.prototype.updateDiary = function (diaryItem, successCallback, errorCallback) {
        
        //alert("DiaryItem Manager getDiaryitems="+diaryItem);
        var manager = this;
        manager._diaryItemDelegate.updateDiary(diaryItem,function()
        {
        //console.log("values:InsertNewRecords");
        //console.log(diaryItem);
        successCallback(diaryItem);
        },
        function()
        {
        errorCallback || $.noop
        }); 
        
    };
     _manager.prototype.DownloadUpdatedRecords = function (diaryItem, successCallback, errorCallback) {
        
        //alert("DiaryItem Manager getDiaryitems="+diaryItem);
        var manager = this;
        manager._diaryItemDelegate.DownloadUpdatedRecords(diaryItem,function()
        {
        //console.log("values:DownloadUpdatedRecords");
       // console.log(diaryItem);
        successCallback(diaryItem);
        },
        function()
        {
        errorCallback || $.noop
        }); 
        
    };
    
     
	/**
	 * @public
	 * Update the attributes of a given diary item. The provided diary item will be used to replace all
	 * attributes of the existing diary item in the data store.
	 * 
	 * This method can optionally be used to manage diary item attachments, by providing a collection of attachments to add or remove.
	 * 
	 * NOTE: this method does not support notes.
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem The modified diary item. The identifier must match that of the existing diary item.
	 * @param {?Array<string>} attachmentsToAdd A list of file paths pointing to files to be added as attachments to the diary item.
	 * @param {?diary.collection.AttachmentCollection} attachmentsToRemove A collection of attachment objects to be removed from the diary item.
	 * @param {!function(!diary.model.DiaryItem, ?Array<string>)} successCallback Called upon success, with a list of attachment-related error messages (if any).
	 * @param {?function(string)} errorCallback Called upon failure.
	 */
	_manager.prototype.updateDiaryItem = function (diaryItem, attachmentsToAdd, attachmentsToRemove, successCallback, errorCallback) {
		//console.log("diaryItem for update : "+JSON.stringify(diaryItem));
		var manager = this,
			errorMessages = [],
			removeAttachments = function (removedAttachmentsCallback) {
				
				var i = 0,
					next = function () {
						++i;
						if (i < attachmentsToRemove.length) {
							removeAttachment();
						} else {
							removedAttachmentsCallback();
						}
					},
					removeAttachment = function () {
						manager.removeAttachmentFromDiaryItem(diaryItem, attachmentsToRemove.at(i), function () {
							// update the diary item
							diaryItem.get('attachments').remove(attachmentsToRemove.at(i));
							// iterate
							next();
						}, function (errorMessage) {
							errorMessages.push("Failed to remove attachment " + attachmentsToRemove.at(i).get('displayName') + ": " + errorMessage);
							next();
						});
					};
				
				if (attachmentsToRemove && attachmentsToRemove.length > 0) {
					removeAttachment();
				} else {
					removedAttachmentsCallback();
				}
				
			}, 
			addAttachments = function (addedAttachmentsCallback) {
				
				var i = 0,
					next = function () {
						++i;
						if (i < attachmentsToAdd.length) {
							addAttachment();
						} else {
							addedAttachmentsCallback();
						}
					},
					addAttachment = function () {
						manager.addAttachmentToDiaryItem(attachmentsToAdd[i], diaryItem.id, function (newAttachment) {
							// update diary item
							diaryItem.get('attachments').add(newAttachment);
							// loop
							next();
						}, function (errorMessage) {
							errorMessages.push("Failed to add attachment \"" + attachmentsToAdd[i] + "\": " + errorMessage);
							next();
						});
					};
				
				if (attachmentsToAdd && attachmentsToAdd.length > 0) {
					addAttachment();
				} else {
					addedAttachmentsCallback();
				}
				
			};
		
		// check parameters
		{
			// ensure diary item is of the correct type
			if (!(diaryItem instanceof diary.model.DiaryItem)) {
				throw new Error("Diary item must be of correct type");
			} else if (diaryItem instanceof diary.model.Note) {
				throw new Error("Cannot update note (notes are append-only)");
			}
			// ensure attachments to remove is of correct type
			if (isSpecified(attachmentsToRemove) && !(attachmentsToRemove instanceof diary.collection.AttachmentCollection)) {
				throw new Error("Attachments to remove is not a collection of attachments");
			}
			// locked items
			if (diaryItem.get('locked')) {
				throw new Error("Cannot modify locked items");
			}
			// validate diary item
			if (!diaryItem.isValid()) {
				throw new Error("Diary item is not valid");
			}
		}
		
		// perform update
		var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
		
		if((diaryItem.get("createdBy") == userId) || (!(app.model.get(ModelName.STUDENTPROFILE).get('isTeacher')))){
		
			manager._diaryItemDelegate.updateDiaryItem(diaryItem, function () {
				// Note that add/remove attachment methods guarentee the callback being called
				// thus success callback will always be called after reaching this point.
				// Attachments which fail will be logged.
				removeAttachments(function () {
					addAttachments(function () {
						// log the attachment error messages
						_.forEach(errorMessages, function (errorMessage) {
							console.log(errorMessage);
						});
						// return success, including the attachment related error messages
						if (successCallback) {
							successCallback(diaryItem, (errorMessages.length > 0 ? errorMessages : null));
						}
					});
				});
				
			}, errorCallback);
		}
		else{
				if( app.model.get(ModelName.STUDENTPROFILE).get('isTeacher')){
					console.log("updateDiaryItemUser updating");
					manager._diaryItemDelegate.updateDiaryItemUser(diaryItem, function () {
						
						// Note that add/remove attachment methods guarentee the callback being called
						// thus success callback will always be called after reaching this point.
						// Attachments which fail will be logged.
						removeAttachments(function () {
							addAttachments(function () {
								// log the attachment error messages
								_.forEach(errorMessages, function (errorMessage) {
									console.log(errorMessage);
								});
								// return success, including the attachment related error messages
								if (successCallback) {
									successCallback(diaryItem, (errorMessages.length > 0 ? errorMessages : null));
								}
							});
						});
						
					}, errorCallback);
				}
				
		}
	};
	
	
	
	/**
	 * Replaces all locked diary item events with  eventsCollection. 
	 *
	 * @param {diary.collection.DiaryItemCollection} eventsCollection Collection of DiaryItems to add. 
	 * @param {function()} successCallback The function that is to be called if all days of note are fetched
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.replaceAllLockedEvents = function(eventsCollection, successCallback, errorCallback) {

		var manager = this;
		
		// check that both call backs were specified
		if (!successCallback) {
			throw new Error("Success Callback must be defined, callback is not optional");
		}
		
		eventsCollection.each(function(diaryItem) {
			if (!(diaryItem instanceof diary.model.Event) || diaryItem.get('locked') != true) {
				throw new Error("eventsCollection must only contain Locked Event DiaryItems");
			}
		});
		
		//console.log('Removing all existing locked event diary items');
		manager._diaryItemDelegate.deleteAllLockedEvents(function() {

			//console.log('adding in new locked events');
			manager._diaryItemDelegate.createDiaryItems(eventsCollection, successCallback, errorCallback);
			
		}, errorCallback);
		
	};
	
	// return the generated manager
	return _manager;
	
})();
