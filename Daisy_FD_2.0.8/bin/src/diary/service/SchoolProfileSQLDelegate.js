/**
 * The local SQL delegate for School Profile.  The delegate will use SQL queries to actually
 * get data from a local database.  In future there might be another or replacement of this
 * delegate to get data from a remote service 
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SchoolProfileSQLDelegate = (function() {
	
	/**
	 * Constructs a School Delegate.  
	 * 
	 * @constructor
	 */
	var _delegate = function() {	
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};
	
	/*
	 * Maps the school profile from the db record set to the model in the application
	 */
	function schoolProfileMapper(statement) {
		var result = statement.getResult().data;
		
		if (result && result.length > 0) {
			return new diary.model.SchoolProfile({
				'id' : result[0].id,
				'name' : result[0].name,
				'schoolLogo' : result[0].schoolLogo,
				'schoolImage' : result[0].schoolImage,
				'schoolBgImage': result[0].schoolBgImage,
				'schoolMotto' : result[0].schoolMotto,
				'missionStatement' : result[0].missionStatement,
				'diaryTitle' : result[0].diaryTitle,
				'number_of_Campuses': result[0].Number_of_Campuses,
				'includeEmpower' : result[0].includeEmpower == 0 ? false : true,
				'theme' : result[0].theme,
				'AllowAutoUpdates' : result[0].AllowAutoUpdates,
				'RegionalFormat' :result[0].RegionalFormat,
				'UniqueId' :result[0].UniqueID
			});
		}
		return null;
	}
	
	function sid(statement) {
		var result = statement.getResult().data;
		
		if (result && result.length > 0) {
			return result[0].UniqueID
		}
		
	}
	
	/**
	 * Gets the school profile for the school
	 */
	 
	 
	 _delegate.prototype.getschoolid = function(successCallback, errorCallback) {
	// console.log("getschoolid");
		this._database.runQuery(
				// query
				"SELECT UniqueID FROM schoolProfile ",

				// params
				{},

				// success
				function(statement) {
				
					successCallback(sid(statement));
				},

				// error
				errorCallback, "Error while trying to read school profile"
		);
	};
	
	
	
	_delegate.prototype.getSchoolProfile = function(successCallback, errorCallback) {
		//alert("alert : getSchoolProfile : ");
		this._database.runQuery(
				// query
				"SELECT * FROM schoolProfile ",

				// params
				{},

				// success
				function(statement) {
					console.log("alert : getSchoolProfile success: ");
					successCallback(schoolProfileMapper(statement));
				},

				// error
				errorCallback, "Error while trying to read school profile"
		);
	};

    /**
     * Sets the school profile for the diary
     */
	_delegate.prototype.updateSchoolProfile = function(schoolProfile, successCallback, errorCallback) {
		if (!(schoolProfile instanceof diary.model.SchoolProfile)) {
            throw new Error("schoolProfile must be of type diary.model.SchoolProfile");
        }
		
		this._database.runQuery(
				// query
				"UPDATE schoolProfile " +
						"SET name = :name, " +
						"schoolLogo = :schoolLogo, " +
						"schoolImage = :schoolImage, " +
						"schoolMotto = :schoolMotto, " +
						"missionStatement = :missionStatement, " +
						"diaryTitle = :diaryTitle, " +
						"theme = :theme, " +
						"AllowAutoUpdates = :AllowAutoUpdates ," +
						"RegionalFormat = :RegionalFormat " +
						"WHERE id = 1", // there can be only one school profile

				// params
				{
					'name' : schoolProfile.get('name'),
					'schoolLogo' : schoolProfile.get('schoolLogo'),
					'schoolImage' : schoolProfile.get('schoolImage'),
					'schoolMotto' : schoolProfile.get('schoolMotto'),
					'missionStatement' : schoolProfile.get('missionStatement'),
					'diaryTitle' : schoolProfile.get('diaryTitle'),
					'theme' : schoolProfile.get('theme'),
					'AllowAutoUpdates' : schoolprofileServiceinfo.AllowAutoUpdate,
					'RegionalFormat' :schoolprofileServiceinfo.Regional_Format
				},

				// success
				successCallback || $.noop,

				// error
				errorCallback, "Error while trying to set school profile"
		);
    };
	
	/**
	 * Searches the school profile for fields that match a search criteria
	 */
	_delegate.prototype.searchSchoolProfile = function(criteria, successCallback, errorCallback) {
		if (!criteria) {
			throw new Error("Search criteria must be defined");
		}

		this._database.runQuery(
				// query
				"SELECT COUNT(*) AS matches FROM schoolProfile " +
						"WHERE (name LIKE :searchCriteria " +
						"OR schoolMotto LIKE :searchCriteria " +
						"OR missionStatement LIKE :searchCriteria)",

				// params
				{'searchCriteria' : "%" + criteria + "%"},

				// success
				function(statement) {
					successCallback(statement.getResult().data[0].matches);
				},

				// error
				errorCallback, "Error while trying to read school profile"
		);
	};
	
	// return the generated school profile delegate
	return _delegate;
})();
