diary = diary || {};
diary.service = diary.service || {};

diary.service.NewsAnnouncementManager = (function() {
	
	var _manager = function() {	
		this._announcementDelegate = app.locator.getService(ServiceName.NEWSANNOUNCEMENTDELEGATE);
	};
	
	_manager.prototype.getNewsAndAnnouncements = function(successCallback, errorCallback, newsAnnouncements) {
		this._announcementDelegate.getNewsAndAnnouncements(
				function success(newsAnnouncementsList) {
					if(newsAnnouncementsList) {
						successCallback(newsAnnouncementsList);
					} else {
						errorCallback && errorCallback("No News and AnnouncementsList was found.");
					}
				}, errorCallback || $.noop
		); 
		
	}; 
	
	_manager.prototype.getInboxItems = function(successCallback, errorCallback, inboxItems) {
		this._announcementDelegate.getInboxItems(
				function success(inboxItemsList) {
					//alert("success getInboxItems");
					console.log("success getInboxItems");
					if(inboxItemsList) {
						successCallback(inboxItemsList);
					} else {
						errorCallback && errorCallback("No Inbox Items was found.");
					}
				}, errorCallback || $.noop
		); 
		
	}; 
	
	_manager.prototype.sendMessage = function(inboxItem, successCallback, errorCallback) {
	
					this._announcementDelegate.sendMessage(inboxItem, 
						function success(inboxItemId) {
							inboxItem.set("itemId",inboxItemId);
							successCallback(inboxItem);
						}, errorCallback || $.noop
					); 
		
	}; 
	
	_manager.prototype.getInboxitems = function (inboxItem, successCallback, errorCallback) {
        
        var manager = this;
        manager._announcementDelegate.getInboxitems(inboxItem,function(values)
        {
	        successCallback(values);
        },
        function()
        {
        	errorCallback || $.noop
        }); 
        
    };

	return _manager;
})();

