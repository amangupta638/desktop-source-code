/**
 * The manager for DayOfNote Models.  The idea of the manager is to abstract the application from
 * the data access storage methods.  All the business logic that is related to a Task needs to
 * be in here
 * 
 * author - John Pearce
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DayOfNoteManager = (function() {
	
	/**
	 * Constructs a manager instance.
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		this._dayOfNoteDelegate = app.locator.getService(ServiceName.DAYOFNOTEDELEGATE);
	};
	
	/**
	 * Get all the days of note stored in the system.
	 * 
	 * @param {function(diary.collection.DayOfNoteCollection)} successCallback The function that is to be called if all days of note are fetched
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.getAllDaysOfNote = function(successCallback, errorCallback) {
		this._dayOfNoteDelegate.getAllDaysOfNote(successCallback, errorCallback);
	};
	
	/**
	 * Save all DayOfNote in daysOfNoteCollection.  Optionally removes all existing DayOfNotes first
	 *
	 * @param {diary.collection.DayOfNoteCollection} daysOfNoteCollection Collection of DayOfNotes to
	 * @param {Boolean} removeExisting Indicates if all exising DayOfNotes should be removed first
	 * @param {function(diary.collection.DayOfNoteCollection)} successCallback The function that is to be called if all days of note are fetched
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.saveAllDaysOfNote = function(daysOfNoteCollection, removeExisting, successCallback, errorCallback) {
		var manager = this;
		
		if (removeExisting === true) {
			// remove existing days of note first
			//console.log('Removing all existing days of note');
			manager._dayOfNoteDelegate.deleteAllDaysOfNote(saveAll, errorCallback);
		} else {
			saveAll();
		}
		
		function saveAll() {
			manager._dayOfNoteDelegate.insertDaysOfNote(daysOfNoteCollection, successCallback, errorCallback);
		}
		
	};
	
	// return the generated manager
	return _manager;
		
})();

