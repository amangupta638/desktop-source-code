/**
 * The manager for stemmed words management
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.StemmerManager = (function() {

	var ignoredWords = [
			"",
			" "
	];

	// the list of words that should not be included in the stemmed phrase
	var stopWords = [
		"a",
		"about",
		"above",
		"across",
		"after",
		"afterwards",
		"again",
		"against",
		"all",
		"almost",
		"alone",
		"along",
		"already",
		"also",
		"although",
		"always",
		"am",
		"among",
		"amongst",
		"amoungst",
		"amount",
		"an",
		"and",
		"another",
		"any",
		"anyhow",
		"anyone",
		"anything",
		"anyway",
		"anywhere",
		"are",
		"around",
		"as",
		"at",
		"back",
		"be",
		"became",
		"because",
		"become",
		"becomes",
		"becoming",
		"been",
		"before",
		"beforehand",
		"behind",
		"being",
		"below",
		"beside",
		"besides",
		"between",
		"beyond",
		"bill",
		"both",
		"bottom",
		"but",
		"by",
		"call",
		"can",
		"cannot",
		"cant",
		"co",
		"computer",
		"con",
		"could",
		"couldnt",
		"cry",
		"de",
		"describe",
		"detail",
		"do",
		"done",
		"down",
		"due",
		"during",
		"each",
		"eg",
		"eight",
		"either",
		"eleven",
		"else",
		"elsewhere",
		"empty",
		"enough",
		"etc",
		"even",
		"ever",
		"every",
		"everyone",
		"everything",
		"everywhere",
		"except",
		"few",
		"fifteen",
		"fify",
		"fill",
		"find",
		"fire",
		"first",
		"five",
		"for",
		"former",
		"formerly",
		"forty",
		"found",
		"four",
		"from",
		"front",
		"full",
		"further",
		"get",
		"give",
		"go",
		"had",
		"has",
		"hasnt",
		"have",
		"he",
		"hence",
		"her",
		"here",
		"hereafter",
		"hereby",
		"herein",
		"hereupon",
		"hers",
		"herself",
		"him",
		"himself",
		"his",
		"how",
		"however",
		"hundred",
		"i",
		"ie",
		"if",
		"in",
		"inc",
		"indeed",
		"interest",
		"into",
		"is",
		"it",
		"its",
		"itself",
		"keep",
		"last",
		"latter",
		"latterly",
		"least",
		"less",
		"ltd",
		"made",
		"many",
		"may",
		"me",
		"meanwhile",
		"might",
		"mill",
		"mine",
		"more",
		"moreover",
		"most",
		"mostly",
		"move",
		"much",
		"must",
		"my",
		"myself",
		"name",
		"namely",
		"neither",
		"never",
		"nevertheless",
		"next",
		"nine",
		"no",
		"nobody",
		"none",
		"noone",
		"nor",
		"not",
		"nothing",
		"now",
		"nowhere",
		"of",
		"off",
		"often",
		"on",
		"once",
		"one",
		"only",
		"onto",
		"or",
		"other",
		"others",
		"otherwise",
		"our",
		"ours",
		"ourselves",
		"out",
		"over",
		"own",
		"part",
		"per",
		"perhaps",
		"please",
		"put",
		"rather",
		"re",
		"same",
		"see",
		"seem",
		"seemed",
		"seeming",
		"seems",
		"serious",
		"several",
		"she",
		"should",
		"show",
		"side",
		"since",
		"sincere",
		"six",
		"sixty",
		"so",
		"some",
		"somehow",
		"someone",
		"something",
		"sometime",
		"sometimes",
		"somewhere",
		"still",
		"such",
		"system",
		"take",
		"ten",
		"than",
		"that",
		"the",
		"their",
		"them",
		"themselves",
		"then",
		"thence",
		"there",
		"thereafter",
		"thereby",
		"therefore",
		"therein",
		"thereupon",
		"these",
		"they",
		"thick",
		"thin",
		"third",
		"this",
		"those",
		"though",
		"three",
		"through",
		"throughout",
		"thru",
		"thus",
		"to",
		"together",
		"too",
		"top",
		"toward",
		"towards",
		"twelve",
		"twenty",
		"two",
		"un",
		"under",
		"until",
		"up",
		"upon",
		"us",
		"very",
		"via",
		"was",
		"we",
		"well",
		"were",
		"what",
		"whatever",
		"when",
		"whence",
		"whenever",
		"where",
		"whereafter",
		"whereas",
		"whereby",
		"wherein",
		"whereupon",
		"wherever",
		"whether",
		"which",
		"while",
		"whither",
		"who",
		"whoever",
		"whole",
		"whom",
		"whose",
		"why",
		"will",
		"with",
		"within",
		"without",
		"would",
		"yet",
		"you",
		"your",
		"yours",
		"yourself",
		"yourselves"
	];
	
	/**
	 * Constructs a Stemmer Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		
	};
	
	/**
	 * Stems the given phrase and returns back an array containing the words according to stemming algorithm
	 * 
	 * @param {!string} phrase The phrase that is going to be stemmed
	 * @return {?array} The list of stemmed words
	 */
	_manager.prototype.stemmifyPhrase = function(phrase) {
		
		var stemmedWords = null;
		
		// we will only parse the phrase if it is specified otherwise ignore
		if (phrase) {
			
			var words;
		
			// first we need to remove any stop words that we don't need to process
			if (typeof(phrase) == "array") {
				words = phrase;
			}
			else {
				words = phrase.split(" ");
			}
			
			// init the list of stemmed words
			stemmedWords = new Array();
			
			// go though the list of words and check which words are not stop words
			// so we need to stemmed them
			for (var x = words.length - 1 ; x >= 0 ; x--) {
				// holds the word in lower case
				var lowercaseWord = words[x].toLowerCase();

				if(ignoredWords.indexOf(lowercaseWord) != -1) {
					// do nothing
				} else if (stopWords.indexOf(lowercaseWord) != -1) {
					stemmedWords.push(lowercaseWord);
				} else {
					// stem the word
					stemmedWords.push(stemmer(lowercaseWord));
				}
			}
		}
		
		// return the list of stemmed words from the passed phrase
		return stemmedWords;
		
	};
	
	// return the generated manager
	return _manager;
		
})();

