/**
 * The DayOfNoteSQLDelegate class provides the logic required to retrieve
 * DayOfNote objects from a SQL Database service.
 * 
 * author - Justin Judd
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DayOfNoteSQLDelegate = (function() {

	/**
	 * Constructor.
	 * 
	 * @constructor
	 */
	var _delegate = function() {
		this._service = app.locator.getService(ServiceName.APPDATABASE);
	};

	/**
	 * Gets all DayOfNote records
	 * 
	 * @param {function()} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.getAllDaysOfNote = function(successCallback, errorCallback) {
	//alert("parseDayKey getAllDaysOfNote");
		var startDate = app.model.get(ModelName.APPLICATIONSTATE).get('startDate');
		var donStartDate = "";
		if(startDate){
			donStartDate = " where date >= " + startDate.getKey();
		}
		this._service.runQuery(
				// query
				"SELECT * FROM dayOfNote "+donStartDate,

				// params
				{},

				// success
				function (statement) {
					successCallback(new diary.collection.DayOfNoteCollection(_.map(statement.getResult().data, function(item) {
                            return new diary.model.DayOfNote({
                                'date' : (item.date != null || item.date != "") ? Rocketboots.date.Day.parseDayKey(item.date) : null,
								'overrideCycleDay' : item.overrideCycleDay,
								'includeInCycle' : item.includeInCycle,
								'isGreyedOut' : item.isGreyedOut,
								'title' : item.title,
								'overrideCycle' : item.overrideCycle
							});
						})
					));
					
				},

				// error
				errorCallback, "Error while trying to read DayOfNote records"
		);
	};

		/**
	 * Inserts DayOfNote records
	 * 
	 * @param {array} an array of days of note
	 * @param {function()} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.insertDaysOfNote = function(daysOfNote, successCallback, errorCallback) {
		this._service.runScripts(
				// queries
				daysOfNote.map(function(dayOfNote) {
					return {
						'sql'		: "INSERT INTO dayOfNote (date, overrideCycleDay, includeInCycle, isGreyedOut, title, overrideCycle) " +
								"VALUES (:date, :overrideCycleDay, :includeInCycle, :isGreyedOut, :title, :overrideCycle)",
						'params'	: {
							date: dayOfNote.get('date').getKey(),
							overrideCycleDay: dayOfNote.get('overrideCycleDay') || false,
							includeInCycle: dayOfNote.get('includeInCycle') != null ? dayOfNote.get('includeInCycle') : true,
							isGreyedOut: dayOfNote.get('isGreyedOut') || false,
							title: dayOfNote.get('title'),
							overrideCycle: dayOfNote.get('overrideCycle') || false
						},
						'rollback'	: "DELETE dayOfNote WHERE date = :date and overrideCycleDay = :overrideCycleDay " +
								"and includeInCycle = :includeInCycle and isGreyedOut = :isGreyedOut " +
								"and title := title and overrideCycle := overrideCycle"
					}

				}),

				// success
				successCallback || $.noop,

				// error
				errorCallback, "Error while trying to insert DayOfNote records");
	};

	/**
	 * Deletes all DayOfNote records
	 * 
	 * @param {function()} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.deleteAllDaysOfNote = function(successCallback, errorCallback) {
		this._service.runQuery(
				// query
				"DELETE FROM dayOfNote ",

				// params
				{},

				// success
				successCallback || $.noop,

				// error
				errorCallback, "Error while trying to delete all DayOfNote records"
		);
	};

	// return the generated student profile delegate
	return _delegate;

})();
