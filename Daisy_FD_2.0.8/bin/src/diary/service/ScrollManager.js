diary = diary || {};
diary.service = diary.service || {};

diary.service.ScrollManager = (function() {
	
	var service = function() {
		//console.log('ScrollManager()');

		var manager = this;
		manager.maintainPosition = false;
		manager.lastScrollPosition = 0;
		manager.scrollContainer = null;
	};

	/**
	 * Set the element the manager should be listening to 
	 * 
	 * @param {jQuery} $scrollContainer The DOM element we are listening to and managing scroll position for
	 */
	service.prototype.setScrollContainer = function($scrollContainer) {
		var manager = this;

		manager.scrollContainer = $scrollContainer;

		manager.scrollContainer.scroll(function(e) {
			manager.lastScrollPosition = e.originalEvent.srcElement.scrollTop;
			manager.maintainPosition = true;
        });	
	};
	
	/** 
	 * Reset the scroll top of the scroll container if it was saved  
	 */
	service.prototype.resetScrollPosition = function() {
		var manager = this;
		
		if (manager.maintainPosition) {
			manager.scrollContainer.get(0).scrollTop = manager.lastScrollPosition;
		} 
	};
	
	
	/** 
	 * Scroll to the given position
	 * 
	 * @param {number} position
	 */
	service.prototype.scrollToPosition = function(position) {
		var manager = this;
		
		manager.lastScrollPosition = position;
		manager.scrollContainer.get(0).scrollTop = manager.lastScrollPosition;
	};
	
	/** 
	 * Respond to a drag event
	 */
	service.prototype.drag = function(event) {
		var manager = this;
		// Check if the mouse is still within the container
		var container = manager.scrollContainer,
			position = container.offset();
				
		var isStillOverContainer = event.x >= position.left && event.y >= position.top && event.x <= (position.left + container.outerWidth()) && event.y <= (position.top + container.outerHeight());
		if (!isStillOverContainer) return;
		
		var isOverTopOfContainer = event.y <= position.top + 72,
			isOverBottomOfContainer = event.y >= position.top + container.outerHeight() - 72,
			scrollTop = container.scrollTop();
		
		if (isOverTopOfContainer && scrollTop > 0) {
			var speed = (72 - (event.y - position.top)) / 2;
			scrollTop = Math.max(scrollTop - speed , 0);
			
			_.defer(
				function() {
					container.scrollTop(scrollTop);
				}
			);
		}
		else if (isOverBottomOfContainer) {
			var maximumScrollTop = container.get(0).scrollHeight - container.innerHeight();
			var speed = (72 - (position.top + container.outerHeight() - event.y)) /2;
			scrollTop = Math.min(scrollTop + speed, maximumScrollTop);

			_.defer(
				function() {
					container.scrollTop(scrollTop);
				}
			);

		}
			
	};
	
	
	return service;
	
})();
