/**
 * The local SQL delegate for Subject.  The delegate will use SQL queries to actually
 * get data from a local database.
 *
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SubjectSQLDelegate = (function() {

	/**
	 * Constructs a Subject Delegate.
	 *
	 * @constructor
	 */
	var _delegate = function() {
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};

	/**
	 * Determines if the subject is in use or not.  The subject is linked to a diary item and a class, so if either one of the two has
	 * a reference to the subject the function returns true
	 */
	_delegate.prototype.isSubjectInUse = function(subjectId, successCallback, errorCallback) {
		// check that a valid subject was specified
		if (!subjectId) {
			throw new Error("Subject Identifier must be specified, identifier is not optional");
		}

		this._database.runQuery(
		// query
		"SELECT SUM(INUSE) AS INUSE FROM (" + "	SELECT COUNT(*) AS INUSE FROM diaryItem WHERE subjectId = " + subjectId + "	UNION ALL" + "	SELECT COUNT(*) FROM class WHERE subjectId = " + subjectId + ")",

		// params
		{
		},

		// success
		function(statement) {
			successCallback(statement.getResult().data[0].INUSE != 0)
		},

		// error
		errorCallback, "Error while trying to determine if subject is in use");
	};

	/**
	 * Determines if a subject exists with the given title.  The success callback will return the subject if a subject exists or NULL otherwise.
	 */
	_delegate.prototype.subjectExistsWithTitle = function(title, subjectId, successCallback, errorCallback) {
		// check that a valid title was specified
		if (!title) {
			throw new Error("Title must be specified, title is not optional");
		}

		this._database.runQuery(
		// query
		"SELECT * FROM subject WHERE LOWER(name) = :name AND id = :subjectId LIMIT 1",

		// params
		{
			'subjectId' : subjectId,
			'name' : title.toLowerCase()
		},

		// success
		function(statement) {
			var result = statement.getResult().data, subject = null;
			if (result && result.length > 0) {
				subject = new diary.model.Subject({
					'id' : result[0].id,
					'title' : result[0].name,
					'colour' : result[0].colour
				});
			}
			successCallback(subject);
		},

		// error
		errorCallback, "Error while trying to determine if subject is in use");
	};

	/**
	 * Creates a subject with the specified details
	 */
	_delegate.prototype.createSubject = function(subject, successCallback, errorCallback) {
		// check that a valid subject was specified
		if (!subject) {
			throw new Error("Subject must be specified, subject is not optional");
		}

		this._database.runQuery(
		// query
		"INSERT INTO subject (name, colour) VALUES (:name, :colour)",

		// params
		{
			'name' : subject.get('title'),
			'colour' : isHexCode((subject.get('colour'))) ? getHexCode(subject.get('colour').toString().toUpperCase()) : subject.get('colour') //getHexCode
		},

		// success
		function(statement) {
			successCallback(statement.getResult().lastInsertRowID);
		},

		// error
		errorCallback, "Error while trying to create a subject");
	};

	/**
	 * Deletes a subject from the system
	 */
	_delegate.prototype.deleteSubject = function(subjectId, successCallback, errorCallback) {
		// check that a valid subject id was specified
		if (!subjectId) {
			throw new Error("Subject Identifier must be specified, identifier is not optional");
		}

		this._database.runQuery(
		// query
		"DELETE FROM subject WHERE id = " + subjectId,

		// params
		{
		},

		// success
		successCallback || $.noop,

		// error
		errorCallback, "Error while trying to delete a subject");
	};

	/**
	 * Update a subject with the specified new details
	 */
	_delegate.prototype.updateSubject = function(subject, successCallback, errorCallback) {
		// check that a valid subject was specified
		if (!subject) {
			throw new Error("Subject must be specified, subject is not optional");
		}

		// check that the specified subject details contain an identifier
		if (!subject.get('id') || subject.get('id') == null) {
			throw new Error("Subject is not valid, subject identifier must be specified");
		}

		this._database.runQuery(
		// query
		"UPDATE subject SET name = :name, colour = :colour WHERE id = :id",

		// params
		{
			'id' : subject.get('id'),
			'name' : subject.get('title'),
			'colour' : isHexCode((subject.get('colour'))) ? getHexCode(subject.get('colour').toString().toUpperCase()) : subject.get('colour') //getHexCode
		},

		// success
		successCallback || $.noop,

		// error
		errorCallback, "Error while trying to update a subject");
	};

	/**
	 * Gets the list of subjects in the diary
	 */
	_delegate.prototype.getSubjects = function(successCallback, errorCallback) {
		var number_of_Campuses = app.model.get(ModelName.SCHOOLPROFILE).get('number_of_Campuses');
		this._database.runQuery(
		// query
		//"SELECT * FROM subject",
		"SELECT distinct subject .id, subject .name,  class  .name as className, subject .colour, " + " class  .UniqueID as classUniqueID, subject .UniqueID, class.campusCode FROM subject inner join class on class.subjectId = subject.id",
		// params
		{
		},

		// success
		function(statement) {
			successCallback(new diary.collection.SubjectCollection(_.map(statement.getResult().data, function(item) {
				 var name = item.className;
            	 if(number_of_Campuses > 1){
            		 name = item.className+ '('+item.campusCode+')';
            	 }
				return new diary.model.Subject({
					'id' : item.id,
					'title' : item.name,
					'classTitle' : name,
					'classUniqueID' : item.classUniqueID,
					'colour' : item.colour ? item.colour : '#d1d1d2',
					'UniqueID' : item.UniqueID
				});
			})));
		},

		// error
		errorCallback, "Error while trying to read subjects");
	};

	/**
	 * Searches subjects with the given search criteria. The search is performed on subject name
	 *
	 * The search function will return a bunch of identifiers related to subjects so that processing of subjects
	 * is done in a quick way.
	 *
	 * @param {!string} criteria The search criteria that is to be used to search the subject
	 * @param {function(subjectIds)} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.searchSubjects = function(criteria, successCallback, errorCallback) {
		console.log("search subject");
		// check that the required inputs where given
		if (!criteria) {
			throw new Error("Criteria must be defined, criteria is not optional");
		}

		this._database.runQuery(
		// query
		"SELECT id FROM subject WHERE name LIKE :searchCriteria group by UniqueID",

		// params
		{
			'searchCriteria' : "%" + criteria + "%"
		},

		// success
		function(statement) {
			// we just want the ids from the subject
			successCallback(_.map(statement.getResult().data, function(item) {
				return item.id;
			}));
		},

		// error
		errorCallback, "Error while trying to search subjects");
	};

	// return the generated subject delegate
	return _delegate;
})();
