/**
 * The local SQL delegate for Diary Items. The delegate will use SQL queries to
 * actually get data from a local database. In future there might be another or
 * replacement of this delegate to get data from a remote service
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DiaryItemSQLDelegate = (function() {

	/**
	 * Constructs a Diary Item SQL Delegate.
	 * 
	 * @constructor
	 */
	var _delegate = function() {
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};

	/**
	 * Searches the diary items with the given search criteria. The search is performed on the following fields
	 * Title, Description, Subject, Note Entry
	 * 
	 * The search function will return a bunch of identifiers related to diary items so that processing of diary items
	 * is done in a quick way.
	 * 
	 * @param {!string} criteria The search criteria that is to be used to search the diary items
	 * @param {function(diaryItemIds)} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.searchDiaryItems = function(criteria, successCallback, errorCallback) {
		// check that the required inputs where given
		if (!criteria) {
			throw new Error("Criteria must be defined, criteria is not optional");
		}

		this._database.runQuery(
				// query
				"SELECT DISTINCT(di.id) " +
				"FROM diaryItem AS di LEFT OUTER JOIN noteEntry ne ON di.id = ne.noteId " +
				"LEFT OUTER JOIN subject s ON di.subjectId = s.id " +
				"WHERE (di.title LIKE :searchCriteria " +
				"OR di.description LIKE :searchCriteria " +
				"OR di.messageFromText LIKE :searchCriteria " +
				"OR s.name LIKE :searchCriteria " +
				"OR ne.text LIKE :searchCriteria)",

				// params
				{ 'searchCriteria' : "%" + criteria + "%" },

				// success
				function (statement) {
					// we just want the ids from the diary items
					successCallback(_.map(statement.getResult().data, function(item) {
						return item.id;
					}));
				},

				// error
				errorCallback, "Error while trying to search diary items for student"
		);
	};

	/**
	 * Gets all the Diary Items of a student for a date range
	 * 
	 * @param {date} fromDate The date from when diary items should be retrieved
	 * @param {date} toDate The date to when diary items should be retrieved
	 * @param {!diary.collection.SubjectCollection} subjects The list of subjects that are to be related to the loaded diary items if necessary
	 * @param {function(diary.collection.DiaryItemCollection)} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.getDiaryItemsForDayRange = function(fromDate, toDate, subjects, isNote, isTask, isEvent, successCallback, errorCallback) {
		
		// check that the dates where given
		if (!fromDate) {
			throw new Error("From date must be defined, date is not optional");
		}

		if (!toDate) {
			throw new Error("To date must be defined, date is not optional");
		}

		if (!subjects) {
			throw new Error("Subjects must be defined, subjects is not optional");
		}
		
		// check that the from date is before the to date
		if (fromDate.getTime() > toDate.getTime()) {
			throw new Error("To date is not valid, date must be after from date");
		}
		var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
		var fromDay = new Rocketboots.date.Day(fromDate);
		var toDay = new Rocketboots.date.Day(toDate);
		
		var user_type = " AND di.allStudents = 1";
		if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
			user_type = " AND di.allTeachers = 1";
		}

		this._database.runQuery(
				// query
				"SELECT itemTypes.id AS itemType_id, itemTypes.Name AS itemType_name, teac.name as creatorUser, sub.id AS subject_id, di.*, ne.id AS noteEntryId, ne.timestamp, ne.text, lov.UniqueID as passtype_id, lov.Value as passtype_name, lov.IconSmall as IconSmall, lov.Active as isMeritActive, lov.Icon as IconBig, "
						+ " lov.Type as meritType FROM diaryItem AS di LEFT OUTER JOIN subject sub ON sub.UniqueID = di.subjectId LEFT OUTER JOIN noteEntry ne ON di.id = ne.noteId "
						+ " LEFT OUTER JOIN DiaryItemTypes itemTypes ON di.type_id = itemTypes.UniqueID LEFT OUTER JOIN listOfValues lov ON lov.UniqueID = di.passtype_id"
						+ " LEFT OUTER JOIN myTeacher teac on teac.teacher_id = di.createdBy WHERE ((di.assignedDay >= " + fromDay.getKey()
						+ " AND di.assignedDay <= " + toDay.getKey() + ") "
						+ "OR (di.dueDay >= " + fromDay.getKey()
						+ " AND di.dueDay <= " + toDay.getKey() + ") "
						+ "OR (di.startDay >= " + fromDay.getKey()
						+ " AND di.startDay <= " + toDay.getKey() + ")"
						+") AND di.isDelete = 0 AND itemTypes.Active = 1 "+ user_type,

				// params
				{},

				// success
				function(statement) {
					var diaryItem, lastDiaryItemId, type, values,
							diaryItemCollection = new diary.collection.DiaryItemCollection();

					// go through each of the found results and convert it to a model
					_.forEach(statement.getResult().data, function(dbDiaryItem) {
						
						//set locked flag for note
						var locked = false;
						if(dbDiaryItem.createdBy != userId){
							locked = true;
						}
						
						if (dbDiaryItem.id != lastDiaryItemId) {
							type = dbDiaryItem.type.toUpperCase();
							var objSubject = null;
							if(dbDiaryItem.ClassId){
								var subjects = app.model.get(ModelName.SUBJECTS);
								objSubject = getObjectByValue(subjects, dbDiaryItem.ClassId,"classUniqueID");
							}
							values = {
								'id' : dbDiaryItem.id,
								'type' : type,
								'title' : dbDiaryItem.title,
								'description' : dbDiaryItem.description,
								'created' : new Date(dbDiaryItem.created),
								'createdBy' : dbDiaryItem.createdBy,
								'creatorUserName' : dbDiaryItem.creatorUser ? dbDiaryItem.creatorUser : dbDiaryItem.creatorUserName,
								'meritComment' : dbDiaryItem.meritComment,
								'onTheDay' : dbDiaryItem.onTheDay,
								'uniqueId' : dbDiaryItem.UniqueID,
								'subject' : (objSubject != null) ? objSubject : null,
								'localDairyItemTypeId' : dbDiaryItem.itemType_id,
								'dairyItemTypeName' : dbDiaryItem.itemType_name,
								'passtype_id' : dbDiaryItem.passtype_id,
								'passtype_name' : dbDiaryItem.passtype_name,
								
								
							};

							if(type == "HOMEWORK" || type == "ASSIGNMENT") {
							
								diaryItem = new diary.model.Task(_.extend(values, {
									'assignedDay' : Rocketboots.date.Day.parseDayKey(dbDiaryItem.assignedDay),
									'assignedTime' : (dbDiaryItem.assignedTime != null ) ? Rocketboots.date.Time.parseTime(dbDiaryItem.assignedTime) : null,
									'dueDay' : Rocketboots.date.Day.parseDayKey(dbDiaryItem.dueDay),
									'dueTime' : (dbDiaryItem.dueTime != null) ? Rocketboots.date.Time.parseTime(dbDiaryItem.dueTime) : null,
									'completed' : (dbDiaryItem.completed == null || dbDiaryItem.completed=="0") ? null:new Date(dbDiaryItem.completed),
									
									'AssignedBy' : dbDiaryItem.AssingedBy,
									'showDue' : dbDiaryItem.hideFromDue ? false : true,
									'showAssigned' : dbDiaryItem.hideFromAssigned ? false : true,
									'showNotifications' : dbDiaryItem.hideFromNotifications ? false : true,
									'progress' : dbDiaryItem.Progress ? dbDiaryItem.Progress : 0,
									'priority' : dbDiaryItem.priority ? dbDiaryItem.priority : "Low",
									'estimatedHours' : dbDiaryItem.EstimatedHours ? dbDiaryItem.EstimatedHours : 0,
									'estimatedMinutes' : dbDiaryItem.EstimatedMins ? dbDiaryItem.EstimatedMins : 0,
									'weight' : dbDiaryItem.weight,
									'IconSmall' : dbDiaryItem.IconSmall,
									'IconBig' : dbDiaryItem.IconBig,
									'isMeritActive' : dbDiaryItem.isMeritActive,
									'webLinkDesc' : dbDiaryItem.webLink,
									'assignedtoMe' : dbDiaryItem.assignedtoMe,
                                	'classId' : dbDiaryItem.ClassId
									
								}));
							} else if (type == "EVENT") {
                                diaryItem = new diary.model.Event(_.extend(values, {
                                	'startDay' : Rocketboots.date.Day.parseDayKey(dbDiaryItem.startDay),
                                    'startTime' : (dbDiaryItem.startTime != null) ? Rocketboots.date.Time.parseTime(dbDiaryItem.startTime) : null,
                                    'endTime' : (dbDiaryItem.endTime != null) ? Rocketboots.date.Time.parseTime(dbDiaryItem.endTime) : null,
                                    'atTime' : (dbDiaryItem.startTime != null) ? Rocketboots.date.Time.parseTime(dbDiaryItem.startTime) : null,
                                    'locked' : dbDiaryItem.locked || false,
                                    'assignedtoMe' : dbDiaryItem.assignedtoMe,
        							'showNotifications' : dbDiaryItem.hideFromNotifications ? false : true,
                                	'classId' : dbDiaryItem.ClassId,
									'priority' : dbDiaryItem.priority
                                }));
							} else if (type == "NOTE") {
								diaryItem = new diary.model.Note(_.extend(values, {
                                	'atDay' : Rocketboots.date.Day.parseDayKey(dbDiaryItem.startDay),
                                    'atTime' : (dbDiaryItem.startTime != null) ? Rocketboots.date.Time.parseTime(dbDiaryItem.startTime) : null,
                                    'assignedtoMe' : dbDiaryItem.assignedtoMe,
                                	'classId' : dbDiaryItem.ClassId,
                                	'showAssigned' : dbDiaryItem.hideFromAssigned ? false : true,
        							'showNotifications' : dbDiaryItem.hideFromNotifications ? false : true,
									'classId' : dbDiaryItem.ClassId,
                                	'locked' : locked
								}));
							} else if (type == "MERIT") {
								diaryItem = new diary.model.Merit(_.extend(values, {
                                	'classId' : dbDiaryItem.ClassId,
									'passtype_id' : dbDiaryItem.passtype_id,
									'IconSmall' : dbDiaryItem.IconSmall,
									'isMeritActive' : dbDiaryItem.isMeritActive,
									'merit_type' : dbDiaryItem.meritType,
									'assignedDay' : Rocketboots.date.Day.parseDayKey(dbDiaryItem.assignedDay),
									'assignedTime' : (dbDiaryItem.assignedTime != null ) ? Rocketboots.date.Time.parseTime(dbDiaryItem.assignedTime) : null,
									'dueDay' : Rocketboots.date.Day.parseDayKey(dbDiaryItem.dueDay),
									'dueTime' : (dbDiaryItem.dueTime != null) ? Rocketboots.date.Time.parseTime(dbDiaryItem.dueTime) : null
									
								}));
							} else if (type == "MESSAGE") {
								diaryItem = new diary.model.Message(_.extend(values, {
                                	'atDay' : Rocketboots.date.Day.parseDayKey(dbDiaryItem.startDay),
                                    'atTime' : (dbDiaryItem.startTime != null) ? Rocketboots.date.Time.parseTime(dbDiaryItem.startTime) : null,
									'messageFrom' : dbDiaryItem.messageFrom,
									'messageFromText' : dbDiaryItem.messageFromText,
									'responseRequired' : dbDiaryItem.responseRequired ? true : false
								}));
							} else {
								throw new Error("Diary item [type:" + type + "] is not supported");
							}
							diaryItemCollection.add(diaryItem);
						}

						// append note entries
						if (type == 'NOTE') {
							diaryItem.get('entries').add(new diary.model.NoteEntry({
								'id' : dbDiaryItem.noteEntryId,
								'timestamp' : new Date(dbDiaryItem.timestamp),
								'text' : dbDiaryItem.text
							}));
						}
						lastDiaryItemId = dbDiaryItem.id;
					});

					successCallback(diaryItemCollection);
				},

				// error
				errorCallback, "Error while trying to read all diary items in date range for student"
		);
	};
	
	
Date.prototype.toUTCArray= function()
{
    var D= this;
    return (D.getUTCFullYear()+"-"+(D.getUTCMonth()+1)+"-"+D.getUTCDate()+" "+D.getUTCHours()+":"+ D.getUTCMinutes()+":"+D.getUTCSeconds());  
   
}

function getTimeStamp() {
	var time=new Date().getTime();
    return time;
}
function getUTCTime()
{
    var d=new Date();
    return(d.toUTCArray());
}

	/**
	 * Appends a note entry to a note
	 * 
	 * @param {!number} noteId The identifier of the note to which the note entry is to be appended
	 * @param {!diary.model.NoteEntry} noteEntry The note entry that is to be appended
	 * @param {function(int)} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.appendNoteEntryToNote = function(timestamp, noteId, noteEntry, successCallback, errorCallback) {
		// check that the the note identifier is given
		//alert("appendNoteEntryToNote="+timestamp);
		if (!noteId) {
			throw new Error("Note Identifier must be defined, identifier is not optional");
		}
		
		// check that the identifier of the diary item is given
		if (!noteEntry) {
			throw new Error("Note Entry must be defined, note entry is not optional");
		}
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"SELECT UniqueID from diaryItem WHERE id=:noteId",

				// params
				{
					'noteId' : noteId,
					
				},

				// success
				function(statement) {
				var UniqueID=_.map(statement.getResult().data, function(item) {
                        return item.UniqueID;
                    });
                  // console.log("UniqueID");
                   //console.log(UniqueID.length);
                    if(UniqueID[0]!=null)
                    {
                    	var uniqueid=UniqueID[0];
                    	// execute the query to append a note entry
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"INSERT INTO noteEntry (noteId, timestamp, text, lastupdated, isDelete, isSynch, noteUniqueId) VALUES (:noteId, :timestamp, :text, :lastupdated, :isDelete, :isSync, :noteUniqueId)",

				// params
				{
					'noteId' : noteId,
					'timestamp' : noteEntry.get('timestamp').getTime(),
					'text' : noteEntry.get('text'),
					'lastupdated' :timestamp,
					'isDelete' : 0,
					'isSync':0,
					'noteUniqueId':UniqueID[0]
				},

				// success
				function(statement) {
					successCallback(statement.getResult().lastInsertRowID);
				},
				
				// error
				errorCallback, "Error while trying to append a note entry to the note"
		);
                    }
                    else
                    {
                    
                    //console.log("here="+noteId);
                    		app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"INSERT INTO noteEntry (noteId, timestamp, text, lastupdated, isDelete, isSynch, noteUniqueId) VALUES (:noteId, :timestamp, :text, :lastupdated, :isDelete, :isSync, :noteUniqueId)",

				// params
				{
					'noteId' : noteId,
					'timestamp' : noteEntry.get('timestamp').getTime(),
					'text' : noteEntry.get('text'),
					'lastupdated' :timestamp,
					'isDelete' : 0,
					'isSync':0,
					'noteUniqueId':null
				},

				// success
				function(statement) {
					successCallback(statement.getResult().lastInsertRowID);
				},
				
				// error
				errorCallback, "Error while trying to append a note entry to the note"
		);
                    }
				},
				
				// error
				errorCallback, "Error while trying to append a note entry to the note"
		);
		
	};

	/**
	 * Creates a diary item with the details as specified in the diary item
	 * 
	 * @param {!diary.model.DiaryItemCollection} diaryItemCollection The collection of diary item to be created
	 * @param {function()} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.createDiaryItems = function(diaryItemCollection, successCallback, errorCallback) {
		var delegate = this,
			callbacks = new Rocketboots.util.CallbackAggregator(successCallback, errorCallback);
		_.each(diaryItemCollection.toArray(), function(item) {
			callbacks.addCall();
			delegate.createDiaryItem(item, callbacks.getSuccessFunction(), callbacks.getErrorFunction());
		});
		callbacks.done();
	};

	/**
	 * Creates a diary item with the details as specified in the diary item
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem The diary item that is being created
	 * @param {!function(number)} successCallback
	 * @param {?function(error)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.createDiaryItem = function(diaryItem,timestamp, successCallback, errorCallback) {
		//alert("alert : create diaryitem");
		console.log("alert : create diaryitem"+JSON.stringify(diaryItem)+"classId : "+diaryItem.get('classId'));
        var startDay = null,
            startTime = null,
            endTime = null;

		// check that the the diary item is given
		if (!diaryItem) {
			throw new Error("Diary Item must be defined, item is not optional");
		}

		if (!(diaryItem instanceof diary.model.Task) &&
				!(diaryItem instanceof diary.model.Event) &&
				!(diaryItem instanceof diary.model.Message) &&
				!(diaryItem instanceof diary.model.Merit) &&
				!(diaryItem instanceof diary.model.Note)) {
			throw new Error("Unsupported Diary item type");
		}

        // compute the start day and times for the diary item
        if (diaryItem instanceof diary.model.Event) {

            startDay = diaryItem.get('startDay').getKey();
            startTime = diaryItem.get('startTime') != null ? diaryItem.get('startTime').toInteger() : null;
            endTime = diaryItem.get('endTime') != null ? diaryItem.get('endTime').toInteger() : null;

        } else if (diaryItem instanceof diary.model.Message || diaryItem instanceof diary.model.Note) {

            startDay = diaryItem.get('atDay').getKey();
            //console.log("Diary Item (atTime) - " + diaryItem.get('atTime'));
            startTime = diaryItem.get('atTime') != null ? diaryItem.get('atTime').toInteger() : null;

        }
		var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
		var schoolID=app.model.get(ModelName.SCHOOLPROFILE).get('UniqueID');
		// execute the query to create the diary item
		
		this._database.runQuery(
                // query
                "INSERT INTO diaryItem (type, title, description, priority, Progress," +
                    "created, assignedDay, assignedTime, " +
                    "dueDay, dueTime, completed, EstimatedHours, EstimatedMins, webLink, " +
                    "subjectId, startTime, startDay, " +
                    "endTime, locked, messageFrom, lastupdated, onTheDay, " +
                    "messageFromText, responseRequired, isSync, isDelete, createdBy, UserID, SchoolId, ClassId, appType, assignedtoMe, weight, type_id, type_name)" +
                "VALUES (:type, :title, :description, :priority, :progress, " +
                    ":created, :assignedDay, :assignedTime, " +
                    ":dueDay, :dueTime, :completed, :estimatedHours, :estimatedMinutes, :webLinkDesc, " +
                    ":subjectId, :startTime, :startDay, " +
                    ":endTime, :locked, :messageFrom, :lastupdated, :onTheDay, " +
                    ":messageFromText, :responseRequired, :isSync, :isDelete, :createdBy, :UserID, :SchoolId, :classId, :appType, :assignedtoMe, :weight, :type_id, :type_name)",

                // params
                {
                    'type' : diaryItem.get('type'),
                    'title' : diaryItem.get('title'),
                    'description' : diaryItem.get('description'),
                    'priority':diaryItem.get('priority'),
                	'progress':diaryItem.get('progress'),
                    'created' : diaryItem.get('created').getTime(),
                    'assignedDay' : diaryItem.get('assignedDay') != null ? diaryItem.get('assignedDay').getKey() : null,
                    'assignedTime' : diaryItem.get('assignedTime') != null ? diaryItem.get('assignedTime').toInteger() : null,
                    'dueDay' : diaryItem.get('dueDay') != null ? diaryItem.get('dueDay').getKey() : null,
                    'dueTime' : diaryItem.get('dueTime') != null ? diaryItem.get('dueTime').toInteger() : null,
                    'completed' : diaryItem.get('completed') != null ? diaryItem.get('completed').getTime() : null,
                    'estimatedHours':diaryItem.get('estimatedHours'),
                	'estimatedMinutes':diaryItem.get('estimatedMinutes'),
                	'webLinkDesc' : diaryItem.get('webLinkDesc'),
                    'subjectId' : diaryItem.get('subject') != null ? diaryItem.get('subject').get('UniqueID') : null,
                    'startTime' : startTime,
                    'endTime' : endTime,
                    'locked' : diaryItem.get('locked') || false,
                    'messageFrom' : diaryItem.get('messageFrom') != null ? diaryItem.get('messageFrom') : null,
                    'lastupdated' : getTimeStamp(),
                    'messageFromText' : diaryItem.get('messageFrom') != null ? diaryItem.get('messageFromText') : null,
                    'responseRequired' : diaryItem.get('responseRequired') || false,
                    'isSync':0,
					'isDelete':0,
					'createdBy':userId,
					'UserID':userId,
					'SchoolId':schoolID,
					'appType' : "desktop",
					'assignedtoMe' : diaryItem.get('assignedtoMe'),
					'weight' : diaryItem.get('weight'),
					'startDay' : startDay,
					'onTheDay' : diaryItem.get('onTheDay'),
					'classId' : diaryItem.get('classId'),
					'type_id' : diaryItem.get('dairyItemTypeId'),
					'type_name' : diaryItem.get('dairyItemTypeName')
                },

				// success
				function(statement) {
                	
	      			var eventParameters = getDiaryItemEventParameters(diaryItem);
	      			//alert("eventParameters : " + JSON.stringify(eventParameters));
	      			FlurryAgent.logEvent("Diary Item Created", eventParameters);
                	
	      			openMessageBox("Diary item created successfully");
                	successCallback(statement.getResult().lastInsertRowID);
                	
				},

				// error
				errorCallback, "Error while trying to create diary item"
		);
	};
	_delegate.prototype.getDiaryitems = function(id, successCallback, errorCallback) {
		GetLastSync("diaryItem");
        params = {};
        var delegate = this,
        this._database.runQuery(
            // query
            "SELECT distinct di.onTheDay,di.id as diaryid,di.type, di.appType, di.title,di.description,di.created,di.assignedday,di.EstimatedHours,di.EstimatedMins,"+
            "di.assignedtime,di.dueday,di.duetime,di.completed,di.subjectid,di.startday,di.starttime,di.priority,di.Progress,di.webLink,"+
            "di.endtime,di.messagefrom,di.messagefromtext,di.lastupdated,di.UniqueID,di.weight, subject.name as subjName, di.ClassId, di.assignedtoMe, di.type_id, di.type_name "+
            "FROM diaryItem as di LEFT OUTER  JOIN subject ON di.subjectId=subject.UniqueID WHERE di.isSync = 0 AND di.type!='NOTE' AND ((di.UniqueID IS NULL and di.isDelete = 0) or (di.UniqueID IS NOT NULL))"+
            " AND di.type!='message'",
                // params
                {},

                // success
                function(statement) {
                    //console.log("success");
                     var diaryitem=_.map(statement.getResult().data, function(item) {
                        return item;
                     });
                   //console.log("diaryitem : "+JSON.stringify(diaryitem));
                   successCallback(diaryitem);
                                
                },

                // error
                errorCallback, "Error while trying to create diary item"
        );
    };
    
    
	/**
	 * Deletes the diary item for the specified identifier. If the type of diary item that is being deleted is a Note all the
	 * related notes for the diary items will also be deleted.
	 * 
	 * @param {!number} diaryItemId The identifier of the diary item to delete
	 * @param {function()} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.deleteDiaryItem = function(diaryItemId, successCallback, errorCallback) {
			// check that the identifier of the diary item is given
			if (!diaryItemId) {
				throw new Error("Diary Item identifier must be defined, identifier is not optional");
			}
			var delegate = this,
				params = { 'diaryItemId' : diaryItemId };

			this._database.runQuery(
				// query
				"UPDATE  diaryItem SET isDelete = 1 WHERE id = :diaryItemId",

				// params
				params,

				// success
				function () {
				//alert("isdelete set to 1");
					delegate._database.runQuery(
							// query
							"UPDATE   noteEntry SET isDelete = 1  WHERE noteId = :diaryItemId",

							// params
							params,

							// call the successCallback on success and error because the
							// diaryItem has been deleted anyway
							successCallback || $.noop, successCallback
					);
				},

				// error
				errorCallback, "Error while trying to delete diary item [id:" + diaryItemId + "]"
			);
		};

	/**
	 * Update a diary item. The diary item must have it's ID specified (unchanged from the original), 
	 * and the corresponding diary item will have its attributes replaced with the attributes of the provided item.
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem Diary item to replace existing diary item of the same ID.
	 * @param {function()} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.updateDiaryItem = function (diaryItem, successCallback, errorCallback) {

        var startDay = null,
            startTime = null,
            endTime = null;

		// check diary item type
		if (!(diaryItem instanceof diary.model.DiaryItem)) {
			throw new Error("Diary item is not of type DiaryItem");
		}

		if (!diaryItem.id) {
			throw new Error("Diary item does not have it's ID set");
		}

		if (!diaryItem.isValid()) {
			throw new Error("Diary item does not validate");
		}

        // compute the start day and times for the diary item
        if (diaryItem instanceof diary.model.Event) {

            startDay = diaryItem.get('startDay').getKey();
            startTime = diaryItem.get('startTime') != null ? diaryItem.get('startTime').toInteger() : null;
            endTime = diaryItem.get('endTime') != null ? diaryItem.get('endTime').toInteger() : null;

        } else if (diaryItem instanceof diary.model.Message || diaryItem instanceof diary.model.Note) {

            startDay = diaryItem.get('atDay').getKey();
            startTime = diaryItem.get('atTime').toInteger();

        }
        


		this._database.runQuery(
            // query
            "UPDATE diaryItem " +
            "SET hideFromDue = 0, hideFromAssigned = 0, hideFromNotifications = 0, type = :type, title = :title, description = :description, priority = :priority, Progress = :progress," +
                "created = :created, assignedDay = :assignedDay, assignedTime = :assignedTime, " +
                "dueDay = :dueDay, dueTime = :dueTime, completed = :completed, EstimatedHours = :estimatedHours, EstimatedMins = :estimatedMinutes," +
                "webLink = :webLinkDesc, subjectId = :subjectId, startDay = :startDay, startTime = :startTime, " +
                "endTime = :endTime, locked = :locked, messageFrom = :messageFrom, " +
                "messageFromText = :messageFromText, responseRequired = :responseRequired, lastupdated = :lastupdated, isSync= :isSync, weight = :weight, onTheDay = :onTheDay, ClassId = :ClassId " +
                 "WHERE id = :diaryItemId",

            // params
            {
                'diaryItemId' : diaryItem.id,
                'type' : diaryItem.get('type'),
                'title' : diaryItem.get('title'),
                'description' : diaryItem.get('description'),
                'priority':diaryItem.get('priority'),
                'progress':diaryItem.get('progress'),
                'created' : diaryItem.get('created').getTime(),
                'assignedDay' : diaryItem.get('assignedDay') != null ? diaryItem.get('assignedDay').getKey() : null,
                'assignedTime' : diaryItem.get('assignedTime') != null ? diaryItem.get('assignedTime').toInteger() : null,
                'dueDay' : diaryItem.get('dueDay') != null ? diaryItem.get('dueDay').getKey() : null,
                'dueTime' : diaryItem.get('dueTime') != null ? diaryItem.get('dueTime').toInteger() : null,
                'completed' : diaryItem.get('completed') != null ? diaryItem.get('completed').getTime() : null,
                'estimatedHours':diaryItem.get('estimatedHours'),
                'estimatedMinutes':diaryItem.get('estimatedMinutes'),
                'webLinkDesc':diaryItem.get('webLinkDesc'),
                'subjectId' : diaryItem.get('subject') != null ? diaryItem.get('subject').get('UniqueID') : null,
                'startDay' : startDay,
                'startTime' : startTime,
                'endTime' : endTime,
                'locked' : diaryItem.get('locked') || false,
                'messageFrom' : diaryItem.get('messageFrom') != null ? diaryItem.get('messageFrom') : null,
                'messageFromText' : diaryItem.get('messageFrom') != null ? diaryItem.get('messageFromText') : null,
                'responseRequired' : diaryItem.get('responseRequired') || false,
                'lastupdated': getTimeStamp(),
                'isSync': 0,
                'weight' : diaryItem.get('weight'),
                'onTheDay' : diaryItem.get('onTheDay'),
				'ClassId' : diaryItem.get('classId')
            },

			// success
			successCallback || $.noop,

			// error
			errorCallback
		);
		
		var eventParameters = getDiaryItemEventParameters(diaryItem);
		//alert("eventParameters : " + JSON.stringify(eventParameters));
		FlurryAgent.logEvent("Diary Item Updated", eventParameters);
	};
	
	/**
	 * Update a diary item user. The diary item must have it's ID specified (unchanged from the original), 
	 * and the corresponding diary item will have its attributes replaced with the attributes of the provided item.
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem Diary item to replace existing diary item of the same ID.
	 * @param {function()} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.updateDiaryItemUser = function (diaryItem, successCallback, errorCallback) {

		console.log("update diaryItemUser");

		// check diary item type
		if (!(diaryItem instanceof diary.model.DiaryItem)) {
			throw new Error("Diary item is not of type DiaryItem");
		}

		if (!diaryItem.id) {
			throw new Error("Diary item does not have it's ID set");
		}

		if (!diaryItem.isValid()) {
			throw new Error("Diary item does not validate");
		}

  

		this._database.runQuery(
            // query
            "UPDATE diaryItemUser " +
            "SET progress = :progress " +
              	"WHERE item_id = :diaryItemId",

            // params
            {
                'progress':diaryItem.get('progress'),
                'diaryItemId' : diaryItem.get('uniqueId')
               
            },

			// success
			successCallback || $.noop,

			// error
			errorCallback
		);
	};

_delegate.prototype.updateUniversalID = function(id, successCallback, errorCallback) {
       // alert("Universal_ID="+id.Universal_ID);
        params = {};
        if(id.Universal_ID)
        {
        var delegate = this,
        this._database.runQuery(
            // query
            "UPDATE diaryItem SET isSync = 1,Universal_ID='"+id.Universal_ID+"' WHERE id="+id.id,
                // params
                {},

                // success
                function(statement) {
                   // alert("updateUniversalID success");
                   successCallback("");
                                
                },

                // error
                errorCallback, "Error while trying to create diary item"
        );
        }
    };
    _delegate.prototype.updateUniversalIDs = function(idArray, successCallback, errorCallback) {
       //console.log("idArray");
      // console.log(idArray);
        params = {};
        for(var i=0;i<idArray.length;i++)
        {
        if(idArray[i].Universal_ID)
        {
        var delegate = this,
        this._database.runQuery(
            // query
            "UPDATE diaryItem SET isSync = 1,Universal_ID='"+idArray[i].Universal_ID+"' WHERE id="+idArray[i].id,
                // params
                {},

                // success
                function(statement) {
                   // alert("updateUniversalID success");
                   successCallback("");
                                
                },

                // error
                errorCallback, "Error while trying to create diary item"
        );
        }
        }
    };
    _delegate.prototype.updateDiary = function(id, successCallback, errorCallback) {
      
        params = {};
        var delegate = this,
        this._database.runQuery(
            // query
            "SELECT di.id as diaryid,di.type,di.title,di.description,di.created,di.assignedday,"+
            "di.assignedtime,di.dueday,di.duetime,di.completed,di.subjectid,di.startday,di.starttime,"+
            "di.endtime,di.messagefrom,di.messagefromtext,di.lastupdated,di.UniqueID,at.originalFilePath,"+
            "at.displayName,at.fileType,at.localFileName,at.dateAttached,at.id "+
            "FROM diaryItem as di LEFT OUTER JOIN attachment as at ON at.diaryItemId = di.id WHERE di.id="+id,
                // params
                {},

                // success
                function(statement) {
                    var values=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    var dataArray=[];
                 var obj={};
                 var i;
                 var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
               var token=systemConfig.get('token');
                for (i=0;i<values.length;i++)
                {
                    var obj1=new Object();
                    obj1.diaryid=values[i].diaryid
                     obj1.student_id=token
                    obj1.type=values[i].type
                     obj1.title=values[i].title
                     obj1.description=values[i].description
                     obj1.created=values[i].created
                     obj1.assignedday=values[i].assignedDay
                     obj1.assignedtime=values[i].assignedTime
                     obj1.dueday=values[i].dueDay
                     obj1.duetime=values[i].dueTime
                     obj1.completed=values[i].completed
                     obj.subjectid=values[i].subjectId
                     obj1.startday=values[i].startDay
                     obj1.starttime=values[i].startTime
                     obj1.endtime=values[i].endTime
                     obj1.messagefrom=values[i].messageFrom
                    obj1.messagefromtext=values[i].messageFromText
                     obj1.lastupdated=values[i].lastupdated
                     obj1.originalFilePath=values[i].originalFilePath
                     obj1.displayName=values[i].displayName
                    obj1.fileType=values[i].fileType
                     obj1.localFileName=values[i].localFileName
                     obj1.dateAttached=values[i].dateAttached
                    obj1.attachmentid=values[i].id
                   // alert(JSON.stringify(obj1));
                    dataArray.push(obj1);
                 }
            var url = SystemSettings.SERVERURL+"/web/dbsync/update";
            data =  {
                "param":dataArray
             };
        ajaxType = 'POST'; 
            //alert(values[0]);
           //console.log(" update TheCloud.callService: url=" + url + ", type=" + ajaxType + " data=" + JSON.stringify(data));
                
       /* $.ajax({
            'url' : url,
            'type': ajaxType,
            'data': data,
            'dataType' : "json",
            'success': function(result) {
                 //console.log("update sucess"+JSON.stringify(result));
                 $.each(result,function(i,j)
            {
                var objId={};
                $.each(j,function(key,value)
                {
                   app.locator.getService(ServiceName.DIARYITEMDELEGATE).updateSyncRecords(key, onupdateSuccess, onupdateError);
                   function onupdateSuccess()
                   {
                    //alert("success");
                   }
                   function onupdateError()
                   {
                    
                   }
                })
                
            });
            }, 
            'error': function(response) {
              // console.log("update TheCloud.error: " +JSON.stringify(response));
            }
        });*/
                  // successCallback(values);
                                
                },

                // error
                errorCallback, "Error while trying to create diary item"
        );
    };
    
    /*insert sync records*/
    _delegate.prototype.InsertSyncRecords = function(record, successCallback, errorCallback) {
    //alert(JSON.stringify(record));
        params = {};
        var delegate = this,
        this._database.runQuery(
            // query
            "INSERT INTO diaryItem (type, title, description, " +
                    "created, assignedDay, assignedTime, " +
                    "dueDay, dueTime, completed, " +
                    "subjectId, startDay, startTime, " +
                    "endTime, locked, messageFrom, " +
                    "messageFromText, responseRequired, lastupdated, isSync, createdTimesatmp)" +
                "VALUES (:type, :title, :description, " +
                    ":created, :assignedDay, :assignedTime, " +
                    ":dueDay, :dueTime, :completed, " +
                    ":subjectId, :startDay, :startTime, " +
                    ":endTime, :locked, :messageFrom, " +
                    ":messageFromText, :responseRequired, :lastupdated, :isSync, :createdDate)",

                // params
                {
                    'type' :record.type,
                    'title' : record.title,
                    'description' : record.description,
                    'created' : "1366203795521",
                    'assignedDay' : "20130417",
                    'assignedTime' : "1000",
                    'dueDay' : "20130417",
                    'dueTime' : "1000",
                    'completed' : record.completed,
                    'subjectId' :record.subjectid,
                    'startDay' : record.startDay,
                    'startTime' : record.startTime,
                    'endTime' : record.endTime,
                    'locked' :"0",
                    'messageFrom' :record.messageFrom,
                    'messageFromText' : record.messageFrom,
                    'responseRequired' :"0",
                    'lastupdated': "1366203796397",
                    'isSync':"1",
                    'createdDate':record.created_time
                    
                },

                // success
                function(statement) {
                    //alert("InsertSyncRecords success");
                                
                },
                function() 
                {
                   // alert("error");
                                
                }
        );
    };
    
	/**
	 * Deletes all Locked Event records
	 * 
	 * @param {function()} successCallback
	 * @param {function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.deleteAllLockedEvents = function(successCallback, errorCallback) {
		this._database.runQuery(
			// query
			"DELETE FROM diaryItem WHERE type = 'EVENT' and locked = true",

			// params
			{},

			// success
			successCallback || $.noop,

			// error
			errorCallback, "Error while trying to delete all Locked Event DiaryItem records"
		);
	};
	_delegate.prototype.updateSyncRecords = function(id,successCallback, errorCallback) {
        this._database.runQuery(
            // query
            "UPDATE diaryItem SET isSync=1 WHERE id = "+id,

            // params
            {},

            // success
            successCallback || $.noop,

            // error
            errorCallback, "Error while trying to delete all Locked Event DiaryItem records"
        );
    };
    _delegate.prototype.updateNewSyncRecords = function(diaryItem,successCallback, errorCallback) {
    //console.log(JSON.stringify(diaryItem));
    //console.log("diaryItem.universal_id="+diaryItem.universal_id);
       /* this._database.runQuery(
            // query
            "UPDATE diaryItem " +
            "SET type = :type, title = :title, description = :description, " +
                "created = :created, assignedDay = :assignedDay, assignedTime = :assignedTime, " +
                "dueDay = :dueDay, dueTime = :dueTime, completed = :completed, " +
                "subjectId = :subjectId, startDay = :startDay, startTime = :startTime, " +
                "endTime = :endTime, locked = :locked, messageFrom = :messageFrom, " +
                "messageFromText = :messageFromText, responseRequired = :responseRequired, lastupdated = :lastupdated, isSync= :isSync " +
                 "WHERE Universal_ID='ef68857a03'",

            // params
            {
                    'type' :diaryItem.type,
                    'title' : diaryItem.title,
                    'description' : diaryItem.description,
                    'created' : "1366203795521",
                    'assignedDay' : "20130417",
                    'assignedTime' : "1000",
                    'dueDay' : "20130417",
                    'dueTime' : "1000",
                    'completed' : diaryItem.completed,
                    'subjectId' :diaryItem.subjectid,
                    'startDay' : diaryItem.startDay,
                    'startTime' : diaryItem.startTime,
                    'endTime' : diaryItem.endTime,
                    'locked' :"0",
                    'messageFrom' :diaryItem.messageFrom,
                    'messageFromText' : diaryItem.messageFrom,
                    'responseRequired' :"0",
                    'lastupdated': "1366203796397",
                    'isSync':"1"
            },
            // success
           function success(statement)
           {
                //console.log("success");
           },
            // error
            function error()
            {
               // console.log("error");
            }
        );*/
    };
    function updateServer(test)
    {
    
    var url = SystemSettings.SERVERURL+"/web/dbsync/syncupdate";
    var data={
    "param":test,
    "is_desktop_sync":1
    
    }
    //console.log("url="+url+", Data="+ JSON.stringify(data));
       /* $.ajax({
            'url' : url,
            'type': 'POST',
            'data': data,
            'dataType' : "json",
            'success': function(result) {
            //console.log("syncupdate");
           // console.log(JSON.stringify(result));
            }, 
            'error': function(response) {
               // alert("TheCloud.error: "+ JSON.stringify(response));
            }
        });*/
    }
    
    _delegate.prototype.getNewRecords = function(id,successCallback, errorCallback) {
    app.view.splashView.showprogress("synchronizing with cloud server");
    //console.log("Adding new records");
        this._database.runQuery(
            // query
            "SELECT createdTimesatmp FROM diaryItem where UniqueID  not null ORDER BY createdTimesatmp DESC LIMIT 1",

            // params
            {},

            // success
             function(statement) {
                    var values=_.map(statement.getResult().data, function(item) {
                    
                    });
                    successCallback("true");
                    var url = SystemSettings.SERVERURL+"/web/dbsync/syncdownload";
            data =  {
               created_tim:"2013-04-16 11:50:27",
               updated_tim:"2013-04-16 11:50:27",
               is_desktop_sync:1
             };
        ajaxType = 'POST'; 
        
       /* $.ajax({
            'url' : url,
            'type': 'POST',
            'data': data,
            'dataType' : "json",
            'success': function(result) {
            //console.log("syncdownload");
          //console.log(JSON.stringify(result));
         app.view.dialogView.hideWindow();
                   successCallback("true");
                 response=result.newrecords;
                 var updated=result.updatedrecords;
                // console.log(JSON.stringify(result.updatedrecords))
                 var test=[];
                for(var i=0;i<response.length;i++)
                {
                   test.push(response[i].universal_id);
                   app.locator.getService(ServiceName.DIARYITEMDELEGATE).InsertSyncRecords(response[i], onInsertSyncRecordsSuccess, onInsertSyncRecordsError);
                    
                }
                
                for(var j=0;j<updated.length;j++)
                {
                    test.push(updated[j].universal_id);
                    app.locator.getService(ServiceName.DIARYITEMDELEGATE).updateNewSyncRecords(updated[j], onInsertSyncRecordsSuccess, onInsertSyncRecordsError);
                }
               // console.log(test);
                if(test.length)
                    updateServer(test);
                function onInsertSyncRecordsSuccess()
                {
                   
                   // alert("onInsertSyncRecordsSuccess");
                }
                function onInsertSyncRecordsError()
                {
                   // alert("onInsertSyncRecordsError");
                }
                
                 
            }, 
            'error': function(response) {
               // alert("TheCloud.error: "+ JSON.stringify(response));
            }
        });*/
                    },
            

            // error
            errorCallback, "Error while trying to delete all Locked Event DiaryItem records"
        );
    };
	
	_delegate.prototype.updateRecords = function() {
    app.view.splashView.showprogress("Updating From Server....");
    // console.log("Updating From Server");
        this._database.runQuery(
            // query
            "SELECT di.id as diaryid,di.type,di.title,di.description,di.created,di.assignedday,"+
            "di.assignedtime,di.dueday,di.duetime,di.completed,di.subjectid,di.startday,di.starttime,"+
            "di.endtime,di.messagefrom,di.messagefromtext,di.lastupdated,di.UniqueID,at.originalFilePath,"+
            "at.displayName,at.fileType,at.localFileName,at.dateAttached,at.id "+
            "FROM diaryItem as di LEFT OUTER JOIN attachment as at ON at.diaryItemId = di.id WHERE UniqueID is not null and di.isSync=0",

            // params universal_id is null
            {},

                // success
                function(statement) {
               /* app.view.dialogView.showWindow({
                            title : "test View",
                            message : "updating data to cloud...",
                            isError : false,
                            disableClose: true
                        });*/
                     var values=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    
                    var dataArray=[];
                 var obj={};
                 var i;
                 var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
               var token=systemConfig.get('token');
                for (i=0;i<values.length;i++)
                {
                    var obj1=new Object();
                    obj1.diaryid=values[i].diaryid
                     obj1.student_id=token
                    obj1.type=values[i].type
                     obj1.title=values[i].title
                     obj1.description=values[i].description
                     obj1.created=values[i].created
                     obj1.assignedday=values[i].assignedDay
                     obj1.assignedtime=values[i].assignedTime
                     obj1.dueday=values[i].dueDay
                     obj1.duetime=values[i].dueTime
                     obj1.completed=values[i].completed
                     obj.subjectid=values[i].subjectId
                     obj1.startday=values[i].startDay
                     obj1.starttime=values[i].startTime
                     obj1.endtime=values[i].endTime
                     obj1.messagefrom=values[i].messageFrom
                    obj1.messagefromtext=values[i].messageFromText
                     obj1.lastupdated=values[i].lastupdated
                    obj1.universal_id=values[i].Universal_ID
                     obj1.originalFilePath=values[i].originalFilePath
                     obj1.displayName=values[i].displayName
                    obj1.fileType=values[i].fileType
                     obj1.localFileName=values[i].localFileName
                     obj1.dateAttached=values[i].dateAttached
                    obj1.attachmentid=values[i].id
                    dataArray.push(obj1);
                 }
            var url = SystemSettings.SERVERURL+"/web/dbsync/update";
            data =  {
                "param":dataArray
             };
        ajaxType = 'POST'; 
            //alert(values[0]);
           // alert("TheCloud.callService: url=" + url + ", type=" + ajaxType + " data=" + JSON.stringify(data));
                
       /* $.ajax({
            'url' : url,
            'type': ajaxType,
            'data': data,
            'dataType' : "json",
            'success': function(result) {
               //  alert(JSON.stringify(result));
                 $.each(result,function(i,j)
            {
                var objId={};
                $.each(j,function(key,value)
                {
                   app.locator.getService(ServiceName.DIARYITEMDELEGATE).updateSyncRecords(key, onupdateSuccess, onupdateError);
                   function onupdateSuccess()
                   {
                    //alert("success");
                   }
                   function onupdateError()
                   {
                    
                   }
                })
                app.locator.getService(ServiceName.DIARYITEMMANAGER).updateUniversalID(objId, onupdateSuccess, onupdateError);
                function onupdateSuccess()
                   {
                    //alert("success");
                   }
                   function onupdateError()
                   {
                    
                   }
            });
            }, 
            'error': function(response) {
               // alert("TheCloud.error: "+ JSON.stringify(response));
            }
        });*/
                   
                },

                // error
                function (error)
                {
                    //alert("error");
                }
        );
    };
   
    
	/*==================*/
	_delegate.prototype.InsertNewRecords = function() {
	
	app.view.splashView.showprogress("Inserting new records"); 
	//console.log("Show  Progress");
	
        this._database.runQuery(
            // query
            "SELECT di.id as diaryid,di.type,di.title,di.description,di.created,di.assignedday,"+
            "di.assignedtime,di.dueday,di.duetime,di.completed,di.subjectid,di.startday,di.starttime,"+
            "di.endtime,di.messagefrom,di.messagefromtext,di.lastupdated,di.UniqueID,at.originalFilePath,"+
            "at.displayName,at.fileType,at.localFileName,at.dateAttached,at.id "+
            "FROM diaryItem as di LEFT OUTER JOIN attachment as at ON at.diaryItemId = di.id WHERE Universal_ID is null",

            // params universal_id is null
            {},

                // success
                function(statement) {
                //alert("InsertNewRecords");
                
               
                     var values=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    var dataArray=[];
        var obj={};
        var i;
        var fileArray=[];
        var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
               var token=systemConfig.get('token');
        for (i=0;i<values.length;i++)
        {
        var obj1=new Object();
        obj1.diaryid=values[i].diaryid
        obj1.student_id=token
        obj1.type=values[i].type
        obj1.title=values[i].title
        obj1.description=values[i].description
        obj1.created=values[i].created
        obj1.assignedday=values[i].assignedDay
        obj1.assignedtime=values[i].assignedTime
        obj1.dueday=values[i].dueDay
        obj1.duetime=values[i].dueTime
        obj1.completed=values[i].completed
        obj.subjectid=values[i].subjectId
        obj1.startday=values[i].startDay
        obj1.starttime=values[i].startTime
        obj1.endtime=values[i].endTime
        obj1.messagefrom=values[i].messageFrom
        obj1.messagefromtext=values[i].messageFromText
        obj1.lastupdated=values[i].lastupdated
       obj1.universal_id=values[i].Universal_ID
       obj1.originalFilePath=values[i].originalFilePath
       obj1.displayName=values[i].displayName
       obj1.fileType=values[i].fileType
       obj1.localFileName=values[i].localFileName
       obj1.dateAttached=values[i].dateAttached
       obj1.attachmentid=values[i].id
       obj1.is_sync_desktop=1
       fileArray.push(values[i].originalFilePath);
       ///
       dataArray.push(obj1);
        }
        /*===================*/
        
        /*====================*/
        var url = SystemSettings.SERVERURL+"/insertupdate";
        //alert(url); 
        data =  {
        "param":dataArray
        };
        ajaxType = 'POST'; 
            //alert("TheCloud.callService: url=" + url + ", type=" + ajaxType + " data=" + data);
                
        $.ajax({
            'url' : url,
            'type': ajaxType,
            'data': data,
            'dataType' : "json",
            'success': function(result) {
         	 //console.log("insert resposne");
            //console.log(JSON.stringify(result));
            var resposne=result.response;
            $.each(resposne,function(i,j)
            {
                var objId={};
                $.each(j,function(key,value)
                {
                   
                   if(key=="itemid")
                   {
                   objId.id=value;
                   }
                   else if(key=="universalid")
                   {
                   objId.Universal_ID=value;
                   }
                })
                app.locator.getService(ServiceName.DIARYITEMMANAGER).updateUniversalID(objId, onupdateSuccess, onupdateError);
        function onupdateSuccess(values) 
        {
        
       // console.log("onupdateSuccess");
        }
        function onupdateError(values) 
        {
      //  console.log("onupdateError");
        }
            });
            
                
            }, 
            'error': function(response) {
               // alert("TheCloud.error: "+ JSON.stringify(response));
            }
        });
        if(fileArray.length)
  {
  var request="";
    url = "http://product-dynamics-dev.expensetrackingapplication.com/web/school/checkupload";                   
 boundary = '--------------======-------------------AaB03x';

  request = new air.URLRequest(url);
  request.useCache = false;
  request.contentType = 'multipart/form-data, boundary='+boundary;
  //request.shouldCacheResponse = false;
  request.method='POST';
    for(var j=0;j<fileArray.length;j++)
    {
  buffer = new air.ByteArray();
  file = new air.File(fileArray.nativePath);

  fileStream = new air.FileStream();
  fileStream.open(file, air.FileMode.READ);
  fileContents = new air.ByteArray();
  fileStream.readBytes(fileContents, 0, file.size);
  fileStream.close();

  buffer.writeUTFBytes( "--"+boundary+"\r\n" );
  buffer.writeUTFBytes( "content-disposition: form-data; name=\"Filedata\"; filename=\""+file.name+"\"\r\n" );
  buffer.writeUTFBytes( "Content-Transfer-Encoding: binary\r\n" );
  buffer.writeUTFBytes( "Content-Length: "+file.size+"\r\n" );
  buffer.writeUTFBytes( "Content-Type: application/octet-stream\r\n" );
  buffer.writeUTFBytes( "\r\n" );

  buffer.writeBytes(fileContents, 0, fileContents.length);

  buffer.writeUTFBytes( "\r\n--"+boundary+"--\r\n" );
}
  request.data = buffer;

  var loader = new air.URLLoader();
  loader.addEventListener(air.ProgressEvent.PROGRESS , callback_for_upload_progress);
  loader.addEventListener(air.IOErrorEvent.IO_ERROR , function (e){
  //console.log("Event Error");
  //alert("Event Error"+e.text);
    //air.trace( 'error: '+ e.text );                
  });
  loader.addEventListener(air.Event.COMPLETE, callback_for_upload_finish);
  loader.load( request );
  }
  function callback_for_upload_progress(event) { 
    var loaded = event.bytesLoaded; 
    var total = event.bytesTotal; 
    var pct = Math.ceil( ( loaded / total ) * 100 ); 
    //air.trace('Uploaded ' + pct.toString() + '%');
}


function callback_for_upload_finish(event) {
    //alert('File upload complete');
    //console.log("event");
   // console.log(event);
   // console.log("event.data");
   // console.log(event.currentTarget.data);
   // air.trace(event.data); // output of server response to AIR dev console
}
                    
                    
                },

                // error
                function (error)
                {
                   // alert("error");
                }
        );
    };

	// return the generated delegate
	return _delegate;

})();
