/**
 * The manager for handling content page form data.
 *
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.ContentItemFormManager = (function() {

	var _manager = function() {
		this._delegate = app.locator.getService(ServiceName.CONTENTITEMFORMDELEGATE);
	};

	_manager.prototype.getDataForArticle = function(article, successCallback) {
		this._delegate.getContentItemFormData(article, successCallback, function() {
			successCallback({}); // call success with an empty object even on error
		});
	};

	_manager.prototype.setDataForArticle = function(data, article) {
		if (data.id) {
			this._delegate.updateContentItemFormData(data);
		} else {
			this._delegate.addContentItemFormData(data, article, function success(id) {
				data.id = id;
			});
		}
	};

	// return the generated manager
	return _manager;

})();

