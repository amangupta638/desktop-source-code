/**
 * The model that represents a usage log event
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.UsageLogEvent = (function() {

    /**
     * The constructor for the Usage Log Event
     *
     * @constructor
     */
    var usageLogEvent = Backbone.Model.extend({

        // return the default attributes
        defaults : function() {

            // return all of the properties for the model
            return {
                'id' : null, // a client-only unique identifier for this usageLogEvent
                'time' : new Date(), // time of the event
                'type' : null, //
                'detail' : null,
                'token' : null,
                'macAddresses' : null,
                'schoolConfiguration' : null,
                'version' : null
            };
        }

    });

    usageLogEvent.TYPE = {
        CALENDAR_JUMP				: 'CALENDAR_JUMP',
        CALENDAR_NEXT				: 'CALENDAR_NEXT',
        CALENDAR_PREVIOUS			: 'CALENDAR_PREVIOUS',		
		CLASS_CREATED               : 'CLASS_CREATED',
        CLASS_DELETED               : 'CLASS_DELETED',
		CONTENT_NEXT				: 'CONTENT_NEXT',
		CONTENT_PREVIOUS			: 'CONTENT_PREVIOUS',
        DIARY_ITEM_CREATED          : 'DIARY_ITEM_CREATED',
        DIARY_ITEM_DELETED          : 'DIARY_ITEM_DELETED',
		DISMISS_INDIVIDUAL_NOTIFICATION		: 'DISMISS_INDIVIDUAL_NOTIFICATION',
		DISMISS_NOTIFICATION		: 'DISMISS_NOTIFICATION',
        DOWNLOAD_CONTENT_PACKAGE    : 'DOWNLOAD_CONTENT_PACKAGE',
        DOWNLOAD_SCHOOL_CONFIG      : 'DOWNLOAD_SCHOOL_CONFIG',
        ERROR                       : 'ERROR',
		FEEDBACK					: 'FEEDBACK',
		LOADED_MANIFEST				: 'LOADED_MANIFEST',
		FETCHED_REMOTE_CONTENT		: 'FETCHED_REMOTE_CONTENT',
		FETCHED_REMOTE_MANIFEST		: 'FETCHED_REMOTE_MANIFEST',
		FETCHED_STORED_PACKAGES		: 'FETCHED_STORED_PACKAGES',
        NAVIGATE_CONTENT_PAGE       : 'NAVIGATE_CONTENT_PAGE',
        NOTE_ENTRY_ADDED            : 'NOTE_ENTRY_ADDED',
        REGISTER_TOKEN              : 'REGISTER_TOKEN',
        REGISTER_USER  				: 'REGISTER_USER',
        RESIZE_WINDOW               : 'RESIZE_WINDOW',
		SEARCH						: 'SEARCH',
        SELECT_DAY_VIEW             : 'SELECT_DAY_VIEW',
        SELECT_GLANCE_VIEW          : 'SELECT_GLANCE_VIEW',
        SELECT_MONTH_VIEW           : 'SELECT_MONTH_VIEW',
        SELECT_SUBJECTS_VIEW        : 'SELECT_SUBJECTS_VIEW',
        SELECT_SHORTCUT             : 'SELECT_SHORTCUT',
        SELECT_WEEK_VIEW            : 'SELECT_WEEK_VIEW',
        SHOW_REMINDER               : 'SHOW_REMINDER',
        START_APPLICATION           : 'START_APPLICATION',
		TASK_COMPLETED				: 'TASK_COMPLETED',
		UPDATE_CLASS				: 'UPDATE_CLASS',
		UPDATE_DIARY_ITEM			: 'UPDATE_DIARY_ITEM',
		UPDATE_DIARY_ITEM_ON_DROP	: 'UPDATE_DIARY_ITEM_ON_DROP',
        UPDATE_STUDENT_PROFILE      : 'UPDATE_STUDENT_PROFILE',
        UPDATE_SUBJECT				: 'UPDATE_SUBJECT',
		UPDATE_TOKEN                : 'UPDATE_TOKEN',
        VIEW_CONTENT_PAGE           : 'VIEW_CONTENT_PAGE',
		VIEW_DAY					: 'VIEW_DAY',
		VIEW_WEEK					: 'VIEW_WEEK',
		VIEW_MONTH					: 'VIEW_MONTH',
		VIEW_NOTES					: 'VIEW_NOTES',
		VIEW_GLANCE					: 'VIEW_GLANCE',
		VIEW_SHORTCUT				: 'VIEW_SHORTCUT',
		VIEW_SUBJECT				: 'VIEW_SUBJECT',
		VIEW_TASK					: 'VIEW_TASK',
		VIEW_TASKS_ASSIGNED			: 'VIEW_TASKS_ASSIGNED',
		VIEW_TASKS_DUE				: 'VIEW_TASKS_DUE',
        WARNING                     : 'WARNING',
        VIEW_MYPROFILE				: 'VIEW_MYPROFILE'
    };

    usageLogEvent.VERSION = {
        CALENDAR_JUMP				: 1,
        CALENDAR_NEXT				: 1,
        CALENDAR_PREVIOUS			: 1,		
        CLASS_CREATED               : 1,
        CLASS_DELETED               : 1,
		CONTENT_NEXT				: 1,
		CONTENT_PREVIOUS			: 1,
        DIARY_ITEM_CREATED          : 1,
        DIARY_ITEM_DELETED          : 1,
		DISMISS_INDIVIDUAL_NOTIFICATION : 1,
		DISMISS_NOTIFICATION		: 1,
        DOWNLOAD_CONTENT_PACKAGE    : 1,
        DOWNLOAD_SCHOOL_CONFIG      : 1,
        ERROR                       : 1,
		FEEDBACK					: 1,
		LOADED_MANIFEST				: 1,
		FETCHED_REMOTE_CONTENT		: 1,
		FETCHED_REMOTE_MANIFEST		: 1,
        FETCHED_STORED_PACKAGES		: 1,
		NAVIGATE_CONTENT_PAGE       : 1,
        NOTE_ENTRY_ADDED            : 1,
        REGISTER_TOKEN              : 1,
        REGISTER_USER				: 1,
        RESIZE_WINDOW               : 1,
		SEARCH						: 1,
        SELECT_DAY_VIEW             : 1,
        SELECT_GLANCE_VIEW          : 1,
        SELECT_MONTH_VIEW           : 1,
        SELECT_SUBJECTS_VIEW        : 1,
        SELECT_SHORTCUT             : 1,
        SELECT_WEEK_VIEW            : 1,
        SHOW_REMINDER               : 1,
        START_APPLICATION           : 1,
		TASK_COMPLETED				: 1,
		UPDATE_CLASS				: 1,
		UPDATE_DIARY_ITEM			: 1,
		UPDATE_DIARY_ITEM_ON_DROP	: 1,
        UPDATE_STUDENT_PROFILE      : 1,
        UPDATE_SUBJECT              : 1,
        UPDATE_TOKEN                : 1,
        VIEW_CONTENT_PAGE           : 1,
		VIEW_DAY					: 1,
		VIEW_WEEK					: 1,
		VIEW_MONTH					: 1,
		VIEW_NOTES					: 1,
		VIEW_GLANCE					: 1,
		VIEW_SHORTCUT				: 1,
		VIEW_SUBJECT				: 1,
		VIEW_TASK					: 1,
		VIEW_TASKS_ASSIGNED			: 1,
		VIEW_TASKS_DUE				: 1,
        WARNING                     : 1,
        VIEW_MYPROFILE				: 1
    };    
    
    // return the created usage log event
    return usageLogEvent;

})();

