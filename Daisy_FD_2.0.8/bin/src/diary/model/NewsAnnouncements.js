diary = diary || {};
diary.model = diary.model || {};

diary.model.NewsAnnouncements = (function() {
	
	var newsAnnouncements = Backbone.Model.extend({

		defaults : function() {
			
			// define the attributes of the school profile
			var attrs = { 'id' : null,					
						  'title' : null,
						  'notificationTitle' : null,
						  'schoolId' : null,				
						  'body' : null,				
						  'tags' : null,		
						  'All_Administrators' : null,				
						  'All_Teachers' : null,			
						  'All_Students' : null,                   
						  'All_Parents': null,
						  'CampusIDs': null,
						  'Isdeleted': null,
						  'Created':new Date(),	
						  'CreatedTime': null,	
						  'CreatedBy' : null,
						  'Updated': null,
						  'UpdatedBy': null,
						  'Status':null,			
						  'type' : null,
						  'readStatus' : 0
						   };		
						  
			// return the attributes
			return attrs;
			
		},
		isWithinDay : function(day) {
			throw new Error("Function isWithinDay() must be implemented by extended class");
		},
		
	});
	
	return newsAnnouncements;
	
})();

