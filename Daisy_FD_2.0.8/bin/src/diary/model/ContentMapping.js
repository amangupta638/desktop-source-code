/**
 * The model that represents all the mapping of the content in the application.  This is the mapping of all various types of content to
 * a school profile.  The school profile will be linked to this mapping details highlighting what is available for the school
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.ContentMapping = (function() {
	
	/**
	 * The constructor for the Content Menu.
	 * 
	 * @constructor
	 */
	var item = Backbone.Model.extend({
		
		// init the default properties for the content menu
		defaults : function() {
			
			// define the task attributes
			var attrs = { 'subItems' : null,		// all mapped sub-items for different content (Dictionary<string, diary.collection.ContentItemCollection>)
						  'calendarBanners' : null,	// the content items related to banners that can be shown next to different calendars (diary.collection.ContentItemCollection)
						  'shortcuts' : null };		// the content items related to short cut menus (diary.collection.ContentItemCollection)
			
			// return all of the properties for the model
			return attrs; 
		},
		
		/**
		 * Gets the list of all the articles that are related to the diary
		 */
		getArticles : function() {
			
			// holds the collection that is to be returned back
			var articles = new diary.collection.ContentItemCollection();
			
			// we need to merge both the subitems and the calendar Banners
			
			// go through the dictionary of subitems 
			_.forEach(this.get('subItems'), function(contentItemCollection) {				
				_.forEach(contentItemCollection.toArray(), function(article) {
					// only include it if we don't have it
					if (articles.get(article.get('id')) == null) {
						articles.add(article);
					}
				});
			});
			
			// now we merge the banners
			_.forEach(this.get('calendarBanners').toArray(), function(article) {
				// only include it if we don't have it
				if (articles.get(article.get('id')) == null) {
					articles.add(article);
				}
				
			});
			
			// return the build list of articles
			return articles;
		}, 
		
		/**
		 * Gets the first shortcut that this article is linked to
		 */
		getShortcut: function(article) {
			
			var shortcutKey = null, 
                contentMapping = this;
			
			// go through the dictionary of subitems 
            _.each(contentMapping.get('subItems'), function(subItemCollection, subItemKey) {

                // cannot use 'contains' as it expects a value rather than an iterator function
                if (subItemCollection.find(function(contentItem) { return contentItem.get('id') == article.get('id'); })) {
                    shortcutKey = subItemKey;
                }
            });

            // retrieve the corresponding shortcut
            return contentMapping.get('shortcuts').find(function (shortcut) {
                return shortcut.get('subItems') == shortcutKey;
            });
		},

        /**
         * Used in cases where an article is not linked to a shortcut but a shortcut is required for display
         *
         * @return The default shortcut or NULL if no shortcuts are defined
         */
        getDefaultShortcut : function() {
            // NOTE - It would be a better idea to actually have the default shortcut specified in the
            // manifest file so that we don't automatically default to the first item in the list, like that
            // the solution would be more flexible
            return this.get('shortcuts').length > 0 ? this.get('shortcuts').at(0) : null;
        }
	
	});
	
	// return the created content menu
	return item;
	
})();

