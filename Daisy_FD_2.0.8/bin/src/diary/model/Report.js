diary = diary || {};
diary.model = diary.model || {};

diary.model.Report = (function() {
	
	/**
	 * Creates a Report by extending the Backbone model class
	 * 
	 * @constructor
	 */
	var report = Backbone.Model.extend({

		// init the report with the default values
		defaults : function() {
			
			// define the attributes of the school profile
			var attrs = { 'id' : null,					
						  'UniqueID': null,
						  'isStudent': null,
						  'isTeacher': null,
						  'isReportActive': null,
						  'isSchoolActive':null,	
						  'ReportDescription' : null,
						  'ReportURL' : null,
						  'ReportName' : null };		
						  
			// return the attributes
			return attrs;
			
		}
		
	});
	
	return report;
	
})();

