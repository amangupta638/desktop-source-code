/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: CalendarDay.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.model = diary.model || {};

(function() {
	"use strict";

	/**
	 * @class The model that represents a Day in the Calendar. This model will highlight
	 * the cycle and cycle day of the day and other useful information that might
	 * be relevant for the day. This will eliminate the processing that would be
	 * required each time details are required for a particular day in a calendar.
	 */
	diary.model.CalendarDay = Backbone.Model.extend(
		/** @lends diary.model.CalendarDay.prototype */
		{
			/** @constructs */
			initialize : function() {
				var day = this.get('day');

				// check that the date was specified
				if (!day) {
					throw new Error("Day must be specified, date is not optional");
				}

				// set the identifier of the model to be the timestamp so that
				// we can use it to quickly identify the calendar day
				this.set('id', day.getTime());
			},

			/**
			 * Sets the model attribute defaults.
			 *
			 * @returns {Object} The model attribute defaults
			 */
			// TODO: default to valid values
			defaults : function() {
				var attrs = {
					"day" : null,					// {!Rocketboots.date.Day} the date related to the calendar
					"cycle" : null,					// {?number} the cycle number within the school calendar (INT)
					"cycleLabel" : null,			// {?String} The label to display describing the cycle number
					"cycleDay" : null,				// {?number} the day in the cycle within the school calendar (INT)
					"cycleDayLabel" : null,			// {?String} The label to display describing the cycle day
					"cycleDayShortLabel" : null,	// {?String} The label to display describing the cycle day
					"title" : null,					// {?string} the title for the day if any (STRING)
					"dayOfNotes" : null,
					"isGreyedOut" : false,			// {?boolean} determines if the day should be greyed out or not (BOOLEAN)
													// NOTE - The manager will determine if the date is greyed out or not
					"timetable" : null				// {?diary.model.Timetable} the time table that is to be used for the day
				};

				return attrs;
			},

			/**
			 * Gets the attribute of a calendar profile.
			 *
			 * @param {string} attribute The attribute name that is required for the Calendar Day
			 *
			 * @return {object} The value of the requested attribute
			 */
			get : function (attribute) {
				// holds the return value
				var value;

				if (attribute === "isWeekend") {
					// determine if the date is a weekend
					// if day is Sat (i.e. 6) or Sun (i.e. 0) dahh....
					var date = Backbone.Model.prototype.get.call(this, "day").getDate();
					value = (date.getDay() == 6) | (date.getDay() == 0);
				} else if (attribute === "weekday") {
					// determine the weekday of the date
					var date = Backbone.Model.prototype.get.call(this, "day").getDate();
					value = diary.model.CalendarDay.WEEKDAY[date.getDay()];
				} else if (attribute === "dayOfMonth") {
					// determine the date of the month
					var date = Backbone.Model.prototype.get.call(this, "day").getDate();
					value = date.getDate();
				} else if (attribute === "month") {
					// determine the month
					var date = Backbone.Model.prototype.get.call(this, "day").getDate();
					value = date.getMonth() + 1;
				} else {
					// get the attribute from the base class
					value = Backbone.Model.prototype.get.call(this, attribute);
				}

				// return the attribute of the value
				return value;
			}
		}, {
			/**
			 * The days of the week
			 * @const
			 */
			WEEKDAY: [
				"Sunday",
				"Monday",
				"Tuesday",
				"Wednesday",
				"Thursday",
				"Friday",
				"Saturday"
			]
		}
	);

})();
