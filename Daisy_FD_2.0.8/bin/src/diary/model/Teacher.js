    
    
    
 /**
 * The model that represents a Teacher details in a diary.
 * A Teacher is a Diary Item that requires action on the part of the teacher
 * @constructor
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.Teacher = (function() {
	

	
	// we will extend the Model for the StudentProfile and keep a reference to it so that
	// we can late on add some static functions to the model.
	var teacher = Backbone.Model.extend({

		// init the studentProfile with the default values
		defaults : function() {
			
			// define the attributes of the student profile
			var attrs = { 
						  	'profileDetails' : new diary.model.StudentProfile(),	
            				'subjects' : new diary.model.SujectCollection()
						};					
			
			// return the attributes of the class
			return attrs;
		},
	
	
		
		/*
		 * gets the attribute of a student profile
		 * 
		 * @param {string} attribute The attribute name that is required for the Student Profile
		 *
		 * @return {object} The value of the requested attribute
		 */ 
		get : function (attribute) {
			
			// holds the return value
			var value;
			
			
			value = Backbone.Model.prototype.get.call(this, attribute);
			
			
			// return the attribute of the value
			return value;
		}
		
	});
	
	return teacher;
	
})();

    
   