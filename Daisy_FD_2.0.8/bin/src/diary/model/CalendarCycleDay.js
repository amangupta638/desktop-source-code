diary = diary || {};
diary.model = diary.model || {};

diary.model.CalendarCycleDay = (function() {
	
	/**
	 * Creates a CalendarCycleDay by extending the Backbone model class
	 * 
	 * @constructor
	 */
	var calendarCycleDay = Backbone.Model.extend({

		// init the calendarCycleDay with the default values
		defaults : function() {
			
			// define the attributes of the calendar Cycle Day
			var attrs = { 'id' : null,					
						  'UniqueID': null,
						  'date': null,
						  'cycleDayLabel': null,
						  'shortCycleDayLabel': null,
						  'Created': null,
						  'CreatedBy':null,	
						  'Updated' : null,
						  'UpdatedBy' : null,
						  'CampusId' : null,
						  'CycleDay' : null };		
						  
			// return the attributes
			return attrs;
			
		}
		
	});
	
	return calendarCycleDay;
	
})();

