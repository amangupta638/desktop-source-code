/**
 * The model that represents a content item.  Content Items can be of various types such as school
 * content, educational information, catholic pullbar.
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.ContentItem = (function() {
	
	/**
	 * The constructor for the Content Item.
	 * 
	 * @constructor
	 */
	var item = Backbone.Model.extend({
		
		// init the default properties for the content item
		defaults : function() {
			
			// define the task attributes
			var attrs = { 'title' : null,			// the title for the content item
						  'url' : null,				// the URL pointing to the article that is being published
						  'fromDate' : null, 		// (Rocketboots.date.Day) the date from when the content is to be displayed (if no time specified the content will always be displayed)
						  'toDate' : null,			// (Rocketboots.date.Day) the date to when the content is to be displayed
						  'subItems' : null,		// the identifier that points to sub item that are to be linked to the content item
						  'packageId' : null,		// the package that this content item belongs to
						  'article' : null,			// the source article
						  'documentId' : null, 
						  'role' : null				// Added role in manifest file to display content based on role
							};	// the documentId for this content item		
			
			// return all of the properties for the model
			return attrs; 
		},
		
		/*
		 * the initialization function of the model
		 * this is executed on the construction of the model class
		 */
		initialize : function() {
			
			// check that both the title and the URL were specified
			if (!this.get('title')) {
				throw new Error("Title must be specified, title cannot be NULL");
			}
			
			if (!this.get('url')) {
				throw new Error("URL must be specified, url cannot be NULL");
			}
			
		}
		
	});
	
	// holds the identifier for the SCHOOL INFO tag to identify the school info article
	item.SCHOOL_INFO_URL = "SCHOOL_INFO";
	item.baseTitle="Student Profile";
	/**
	 * Gets the folder where the downloadable content is stored locally 
	 */
	item.getContentFolder = function(){
		
		return "app:/content/";
		//return "app-storage:/content/";
	};
	
	// return the created content menu item
	return item;
	
})();

