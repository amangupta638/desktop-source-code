/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: DiaryItem.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.model = diary.model || {};

(function() {
	"use strict";

	/** @class */
	diary.model.DiaryItem = Backbone.Model.extend(
		/** @lends diary.model.DiaryItem.prototype */
		{
			/**
			 * Sets the model attribute defaults.
			 * 
			 * @returns {Object} The model attribute defaults
			 */
			defaults : function() {
				var attrs = {
					"title" : "(unknown)",
					"description" : null,
					"attachments" : new diary.collection.AttachmentCollection(),
					"created" : new Date(),
					"showDue" : true,
					"showAssigned" : true,
					"showNotifications" : true,
					"locked" : false,
					"createdBy" : null,
					"IconSmall" : null,
					"IconBig" : null,
					'isMeritActive' : null,
					"onTheDay" : 0,
					"dairyItemTypeId" : 0,
					"localDairyItemTypeId" : 0,
					"dairyItemTypeName" : null,
					"creatorUserName" : null,
					"meritComment" : null
					
				};

				return attrs;
			},

			/**
			 * Validates the model attributes.
			 *
			 * @param {!Object} attrs The model attributes to be set
			 *
			 * @returns {String} Validation error, if any
			 */
			validate : function(attrs) {
				// 'title' is required
				if (!isSpecified(attrs.title)) {
					return "Title must be specified and cannot be NULL";
				}

				// 'title' must be a string
				if (typeof(attrs.title) != 'string') {
					return "Title must be a valid string";
				}

				// 'title' cannot be empty
				if (attrs.title == "") {
					return "Title must be specified and cannot be empty";
				}

				// 'description' must be a string, if specified
				if (isSpecified(attrs.description) && typeof attrs.description != 'string') {
					return "Description must be a valid string";
				}

				// 'attachments' must be an AttachmentCollection, if specified
				if (isSpecified(attrs.attachments) && !(attrs.attachments instanceof diary.collection.AttachmentCollection)) {
					return "Attachments must be a valid collection of Attachments";
				}
			},

			/**
			 * Determines if the Diary Item falls on the specified day.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day
			 */
			isWithinDay : function(day) {
				throw new Error("Function isWithinDay() must be implemented by extended class");
			},

			/**
			 * Determines if the Diary Item falls within the specified time frame.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 * @param {!Rocketboots.date.Time} startTime The start time from which to check
			 * @param {!Rocketboots.date.Time} endTime The end time from which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day and within the specified time frame
			 */
			isWithinTimeFrame : function(day, startTime, endTime) {
				throw new Error("Function isWithinTimeFrame() must be implemented by extended class");
			}

		}, {
			/**
			 * Defines the different types of Diary Items that can exist in the system.
			 * @enum {string}
			 */
			TYPE : {

				EVENT : "EVENT",
				MESSAGE : "MESSAGE",
				NOTE : "NOTE",
				HOMEWORK : "HOMEWORK",
				ASSIGNMENT : "ASSIGNMENT",
				MERIT : "MERIT"

			}
		}

	);


})();
