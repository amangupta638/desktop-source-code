/**
 * The model that represents a parent details
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.Parent = (function() {

	var parentProfile = Backbone.Model.extend({

		// init the parent with the default values
		defaults : function() {
			// define the attributes of the parent profile
			var attrs = { 
						  'uniqueId' : null,
						  'profile' : null,
						  'email' : null,
						  'imageUrl' : null,
						  'name' : null,
						  'phone' : null,
						  'image' : null,
						  'parentImageToDisplay' : null
						 };	
			return attrs;
		},
	
		/*
		 * gets the attribute of a parent profile
		 * 
		 * @param {string} attribute The attribute name that is required for the Parent Profile
		 *
		 * @return {object} The value of the requested attribute
		 */ 
		get : function (attribute) {
			
			value = Backbone.Model.prototype.get.call(this, attribute);
			
			// return the attribute of the value
			return value;
		}
		
	});
	
	return parentProfile;
	
})();

