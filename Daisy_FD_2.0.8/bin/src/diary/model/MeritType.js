diary = diary || {};
diary.model = diary.model || {};

diary.model.MeritType = (function() {
	
	var meritType = Backbone.Model.extend({
		
		defaults : function() {
			
			var attrs = { 
				'id' : null, 
				'DiaryItemType' : null,
				'Type' : null,
				'isActive' : 1,
				'iconName' : null,
				'IconSmall' : null,
				'UniqueID' : null, 
				'Description' : null,
				'Value' : null
			};
			
			
			return attrs;
		}
		
	});
	
	return meritType;
	
})();
