var diary = diary || {};
diary.model = diary.model || {};

(function() {
	"use strict";

	diary.model.Merit = diary.model.DiaryItem.extend(
		/** @lends diary.model.Merit.prototype */
		{
			/**
			 * Sets the model attribute defaults.
			 *
			 * @returns {Object} The model attribute defaults
			 */
			defaults : function() {
				var attrs = {
					"type" : diary.model.DiaryItem.TYPE.MERIT,
					"students" : null,
					"assignedDay" : new Rocketboots.date.Day(),
					"assignedTime" : new Rocketboots.date.Time(),
					"dueDay" : new Rocketboots.date.Day().addDays(1),
					"dueTime" : null,
					"comment" : null,
					"merit_type" : null,
					"passtype_name" : null,
					"passtype_id" : 0,
					"IconSmall" : null,
					'isMeritActive' : null,
					"isnotify" : 0,					
					"classId" : null
				};

				return _.extend(diary.model.DiaryItem.prototype.defaults(), attrs);
			},

			/**
			 * Validates the model attributes.
			 *
			 * @param {!Object} attrs The model attributes to be set
			 *
			 * @returns {String} Validation error, if any
			 */
			validate : function(attrs) {
				//var result = null;

				// validate base class
				/* result = diary.model.DiaryItem.prototype.validate.call(this, attrs);
				if (result != null) {
					return result;
				} */
				//return false;
				
			},
			
						isWithinDay : function(day) {
				return this.get('assignedDay').getKey() == day.getKey()
					|| this.get('dueDay').getKey() == day.getKey();
			},
			
			isAssignedToday : function() {
				var day = new Rocketboots.date.Day();
				return this.get('assignedDay').getKey() == day.getKey();
			},

			/**
			 * Determines if the Diary Item falls within the specified time frame. A Task is
			 * considered to fall within a time frame if either the Assigned Day and Time or Due Day
			 * and Time matches the specified day and falls within the specified time frame. If no
			 * Due Time is specified for the Task, only the Assigned Date and Time will be
			 * considered.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 * @param {!Rocketboots.date.Time} startTime The start time from which to check
			 * @param {!Rocketboots.date.Time} endTime The end time from which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day and within the specified time frame
			 */
			isWithinTimeFrame : function(day, startTime, endTime) {

               return (this.get('assignedDay').getKey() == day.getKey()
               		&& isSpecified(this.get('assignedTime'))
					&& this.get('assignedTime').toInteger() >= startTime.toInteger()
					&& this.get('assignedTime').toInteger() < endTime.toInteger())
				|| (this.get('dueDay').getKey() == day.getKey()
					&& isSpecified(this.get('dueTime'))
					&& this.get('dueTime').toInteger() >= startTime.toInteger()
					&& this.get('dueTime').toInteger() < endTime.toInteger()
				);
			},

			/**
			 * Determines if the Task is due within the specified time frame. If no time frame
			 * is specified, this will only check the specified day. If a Start and End Time has
			 * been specified and the Task has no Due Time, it will be considered NOT due within the
			 * specified time frame.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 * @param {?Rocketboots.date.Time} startTime The start time from which to check
			 * @param {?Rocketboots.date.Time} endTime The end time from which to check
			 *
			 * @returns {Boolean} True if the Task is due on the specified date and within the specified time frame
			 */
			isDueWithinTimeFrame : function(day, startTime, endTime) {
				var isDue = day.getKey() == this.get('dueDay').getKey();

				if (isSpecified(startTime) && isSpecified(endTime)) {
					isDue = isDue && isSpecified(this.get('dueTime'))
						&& this.get('dueTime').toInteger() >= startTime.toInteger()
						&& this.get('dueTime').toInteger() < endTime.toInteger();
				}

				return isDue;
			},

			/**
			 * Determines if the Task has been marked 'completed'.
			 *
			 * @returns {Boolean} True if the Task has been marked 'completed'
			 */
			isCompleted : function () {
				return this.get('completed') != null;
			},
			
			isAssigned : function () {
				return this.get('AssignedBy') == 1;
			},

			/**
			 * Determines if the Task is overdue for the specified day and time. If day and time are not
			 * given it will determine if the task is overdue based on the current local time.
			 * Completed tasks are never considered overdue. A Task with no Due Time will be
			 * considered due at the beginning of the Due Day (i.e. at 00:00 AM).
			 *
			 * @param {?Rocketboots.date.Day} onDay The day from when to check
			 * @param {?Rocketboots.date.Time} onTime the time from when to check
			 *
			 * @returns {Boolean} True if the Task is overdue
			 */
			isOverdue : function (onDay, onTime) {
				var fromDay,
					fromTime,
					dueTimeString;

				if ((onDay && !onTime) || (!onDay && onTime)) {
					throw new Error ("Both Day and Time must be specified");
				}

				if (onDay && !(onDay instanceof Rocketboots.date.Day)) {
					throw new Error ("Day is not valid, day must be of type Rocketboots.date.Day");
				}

				if (onTime && !(onTime instanceof Rocketboots.date.Time)) {
					throw new Error ("Time is not valid, time must be of type Rocketboots.date.Time");
				}

				fromDay = onDay || new Rocketboots.date.Day();
				fromTime = onTime || new Rocketboots.date.Time();
				dueTimeString = (isSpecified(this.get('dueTime'))) ? this.get('dueTime').toString() : "0000";

				return !this.isCompleted()
					&& (this.get('dueDay').getKey() + dueTimeString) < (fromDay.getKey() + fromTime.toString());
			}

			
		}
	);

})();
