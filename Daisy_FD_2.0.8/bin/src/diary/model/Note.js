/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: Note.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.model = diary.model || {};

(function() {

	diary.model.Note = diary.model.DiaryItem.extend(
		/** @lends diary.model.Note.prototype */
		{
			/**
			 * Sets the model attribute defaults.
			 *
			 * @returns {Object} The model attribute defaults
			 */
			defaults : function() {
				var attrs = {
					"type" : diary.model.DiaryItem.TYPE.NOTE,
					"atDay" : new Rocketboots.date.Day(),
					"atTime" : null,
					"entries" : new diary.collection.NoteEntryCollection(),
					"classId" : null,
					"assignedtoMe":1
					
				};

				return _.extend(diary.model.DiaryItem.prototype.defaults(), attrs);
			},

			/**
			 * Validates the model attributes.
			 *
			 * @param {!Object} attrs The model attributes to be set
			 *
			 * @returns {String} Validation error, if any
			 */
			validate : function(attrs) {
				var result = null;

				// validate base class
				result = diary.model.DiaryItem.prototype.validate.call(this, attrs);
				if (result != null) {
					return result;
				}

				// 'atDay' is required
				if (!isSpecified(attrs.atDay)) {
					return "Day must be specified and cannot be NULL";
				}

				// 'atDay' must be Rocketboots.date.Day
				if (!(attrs.atDay instanceof Rocketboots.date.Day)) {
					return "Day must be a valid Day";
				}

				// 'atTime' must be Rocketboots.date.Time, if specified
				if (isSpecified(attrs.atTime) && !(attrs.atTime instanceof Rocketboots.date.Time)) {
					return "Time must be a valid Time";
				}

				// 'entries' is required
				if (!isSpecified(attrs.entries)) {
					return "Note Entries must be specified and cannot be NULL";
				}

				// 'entries' must be a NoteEntryCollection
				if (!(attrs.entries instanceof diary.collection.NoteEntryCollection)) {
					return "Note Entries must be a valid collection of Note Entries";
				}
			},

			/**
			 * Determines if the Diary Item falls on the specified day.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day
			 */
			isWithinDay : function(day) {
				//console.log("at day for : "+this.get('uniqueId')+" atDay : "+this.get('atDay'))
				return this.get('atDay').getKey() == day.getKey();
			},
			
			isAssignedToday : function() {
				var day = new Rocketboots.date.Day();
				return this.get('atDay').getKey() == day.getKey();
			},

			/**
			 * Determines if the Diary Item falls within the specified time frame. A Note is
			 * considered to fall within a time frame if the At Day matches the specified day and
			 * the At Time falls within the specified time frame. If no At Time is specified for a
			 * Note, it will be considered NOT falling within the specified time frame.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 * @param {!Rocketboots.date.Time} startTime The start time from which to check
			 * @param {!Rocketboots.date.Time} endTime The end time from which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day and within the specified time frame
			 */
			isWithinTimeFrame : function(day, startTime, endTime) {
				return this.get('atDay').getKey() == day.getKey()
					&& isSpecified(this.get('atTime'))
					&& this.get('atTime').toInteger() >= startTime.toInteger()
					&& this.get('atTime').toInteger() < endTime.toInteger();
			}
		}
	);

})();
