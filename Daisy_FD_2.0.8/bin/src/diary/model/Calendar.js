/**
 * The model that represents the Calendar for the diary.  The calendar will hold one
 * scholastic year and each day will be represented by a Calendar Day model.
 * 
 * NOTE - For the time being the calendar will assume that all the days that are being added
 * are of the same year
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.model = diary.model || {};
 
diary.model.Calendar = (function() {
	
	/**
	 * Creates a calendar
	 * 
	 * @param {integer} year The year of the calendar that is being created
	 * 
	 * @constructor
	 */
	return Backbone.Model.extend({
			
			
		// init the calendar with the default values
		defaults : function() {
			
			var attrs = { 'year' : null,													// the year of the calendar (INTEGER)
						  'timetables' : new diary.collection.TimetableCollection(),		// the timetables that are to be used for the calendar year
						  'calendarDays' : new diary.collection.CalendarDayCollection() 	// the the calendar days (diary.collection.CalendarDayCollection)
						};
			return attrs;
		},
		
		/**
		 * the initialization function of the model
		 */
		initialize : function() {
			
			// holds the set year of the calendar
			//alert("this.get('year')="+SystemSettings.HARDCODDEDYEAR);
			var year = SystemSettings.HARDCODDEDYEAR;
			//alert("claendar year="+year);
			
			// check that it was specified
			if (!year) {
				throw new Error("Year must be specified, year is not optional");
			}
			
			// check that a valid year was given
			//console.log("year initialize : "+year+"typeof(year) : "+typeof(year));
			if (typeof(year) !== 'number') {
				throw new Error("Year is not valid, year must be an integer");
			}
			
			// check that it is a positive number
			if (year < 1) {
				throw new Error("Year is not valid, year must be equal or greater than one");
			}
			
		},
		
		/**
		 * Gets all the Calendar Days for the specified month
		 * @public
		 * 
		 * @param {integer} month The month for which Calendar Days are required. A valid month is from 1 - 12 
		 * 
		 * @return {diary.collection.CalendarDayCollection} The calendar days for the specified month.  The month should be in the natural order
		 */
		getMonthDays : function(month, year) {
			
			// holds the filtered months
			var filteredDays;
			
			// holds the calendar days that are to be returned
			var monthDays;
			
			// check that a valid month was specified
			if (month < 1 || month > 12) {
				throw new Error("Invalid month, month must be between 1 and 12");
			}
			
			// init the calendar day collection
			monthDays = new diary.collection.CalendarDayCollection();
			
			// the function that is to be used to filter the required days from
			// the calendar
			function filterFunction(calendarDay) {
				// holds the date of the calendar day
				var day = calendarDay.get('day');
				// return true ONLY if the month is the same as the specified month
				// otherwise ignore the record or rather return FALSE
				return (day.getMonth() + 1) == month && day.getFullYear() == year;
			}
			
			// get the days for the required month
			filteredDays = _.filter(this.get('calendarDays').toArray(), filterFunction);
			
			// include the found days in the month that is
			// to be returned
			monthDays.add(filteredDays);
			
			// return the details for the month
			return monthDays;
			
		},
		
		/**
		 * Gets all the Calendar Days for the specified week of the year
		 * @public
		 * 
		 * @param {integer} week The week of the year for which Calendar Days are required. A valid week is from 0 - 53,
		 * but in most leap years week 53 will be empty
		 * 
		 * @return {diary.collection.CalendarDayCollection} The calendar days for the specified week. The days should be in the natural order
		 */
		getWeekDays : function(week) {
			
			var startDate = app.model.get(ModelName.APPLICATIONSTATE).get('startDate'); 
            var endDate = app.model.get(ModelName.APPLICATIONSTATE).get('endDate');
            
			var maxWeeks = weeks_between(startDate,endDate);
			// holds the filtered days
			var filteredDays;
			
			// holds the calendar days that are to be returned
			var weekDays;
			
			// check that a valid week was specified
			if (week < 0 || week > maxWeeks) {
				throw new Error("Invalid week, week must be between 0 and "+maxWeeks);
			}
			
			// init the calendar day collection
			weekDays = new diary.collection.CalendarDayCollection();
			//console.log("weekDays=");
			//console.log(weekDays);
			//console.log(calendarDay);
			// the function that is to be used to filter the required days from
			// the calendar
			var weekfirstday = "";
			function filterFunction(calendarDay) {
				if(weekfirstday == "" && calendarDay.get('day').getWeek() == week) {
				 weekfirstday = calendarDay.get('day').getTime();
				}
				
				var datediff = Math.abs(weekfirstday - calendarDay.get('day').getTime()); // difference 
				var diffd = parseInt(datediff / (24 * 60 * 60 * 1000), 10);
				//console.log("datediff="+diffd);
				
    			if(diffd <= 7) {
					
				//console.log("calendarDay.get('day').getWeek()="+calendarDay.get('day').getWeek());
				//console.log("calendarDay.get('day').getYear()="+calendarDay.get('day').getYear());
				//console.log("week="+week);
					//return ((calendarDay.get('day').getWeek() == week)&&(calendarDay.get('day').getYear()=="2013"));
					return (calendarDay.get('day').getWeek() == week);
				}	
			}
			//console.log("this.get('calendarDays').toArray()=")
			//console.log(this.get('calendarDays').toArray())
			// get the days for the required week
			filteredDays = _.filter(this.get('calendarDays').toArray(), filterFunction);
			
			// include the found days in the week that is
			// to be returned
			weekDays.add(filteredDays);
			
			// return the details for the week
			return weekDays;
			
		},
		
		/**
		 * Gets the Calendar Day for the specified day of the year, by month and day
		 * @public
		 * 
		 * @param {integer} month The month of the year for which Calendar Days are required. A valid month is from 1 - 12
		 * @param {integer} day The day of the month for which Calendar Days are required. A valid day is from 1 - 31
		 * 
		 * @return {diary.collection.CalendarDay} The calendar day for the specified month and day.
		 */
		getDay : function (month, day, year) {
			
			var filteredDays;
			
			// check that a valid month was specified
			if (month < 1 || month > 12) {
				throw new Error("Invalid month, must be between 1 and 12");
			}
			// check the day
			if (day < 1 || day > 31) {
				throw new Error("Invalid day, must be between 1 and 31");
			}
			
			filteredDays = _.filter(this.get('calendarDays').toArray(), function (calendarDay) {
				var date = calendarDay.get('day');
				return (date.getMonth() + 1) == month && date.getDayInMonth() == day && date.getFullYear() == year;
				
			});
			
			if (filteredDays.length > 0) {
				return filteredDays[0];
			}
			return null;
			
		},
		
		/**
		 * Adds a calendar day to the calendar
		 * @public
		 * 
		 * @param {diary.model.CalendarDay} calendarDay The day that is to be included in the calendar
		 */
		add : function(calendarDay) {
			
			// holds the old calendar day if any
			var currentCalendarDay;
			
			// check that a valid day was given
			if (!calendarDay) {
				throw new Error("Calendar Day must be specified, day is not optional");
			}
			
			// check that the given calendar days are of the right type
			if (!(calendarDay instanceof diary.model.CalendarDay)) {
				throw new Error("Invalid calendar day, day should be of type diary.model.CalendarDay");
			}
			
			// Check that the day that is being added
			// is related to the year of the calendar
			/*if (calendarDay.get('day').getFullYear() != this.get('year')) {
				throw new Error("Invalid calendar day, day is not in year " + this.get('year'));
			}*/
			
			// check if the day that is being added is already in the
			// calendar
			currentCalendarDay = this.get('calendarDays').get(calendarDay.get('id'));
			
			// if we have a day already specified for the date that is being added
			// we will replace it
			if (currentCalendarDay) {
				// we will first remove the old date and then we will add
				// the new one
				this.get('calendarDays').remove(currentCalendarDay);
			}
			
			// add the day to the calendar
			this.get('calendarDays').add(calendarDay);
		}
		
	});
	
})();