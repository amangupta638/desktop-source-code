diary = diary || {};
diary.model = diary.model || {};

diary.model.DiaryItemType = (function() {
	
	var diaryItemType = Backbone.Model.extend({
		
		defaults : function() {
			
			var attrs = { 
				'id' : null, 
				'name' : null,
				'formTemplate' : null,
				'isActive' : 1,
				'iconName' : null
			};
			
			return attrs;
		}
		
	});
	
	return diaryItemType;
	
})();
