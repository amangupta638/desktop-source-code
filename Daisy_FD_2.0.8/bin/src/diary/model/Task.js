/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: Task.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.model = diary.model || {};

(function() {
	"use strict";

	diary.model.Task = diary.model.DiaryItem.extend(
		/** @lends diary.model.Task.prototype */
		{
			/** @constructs */
			initialize : function() {
				var self = this,

					// the function that is to be triggered upon changing of the subject details
					subjectDetailsChanged = function() {
						self.trigger("change", self);
					},

					// the function for binding the subject change details to the class change event
					bindChangeEvent = function() {
						// get the previous subject attribute if set
						if (self.previous("subject")) {
							self.previous("subject").unbind("change", subjectDetailsChanged);
						}

						// bind the subject to the class
						self.get('subject') && self.get("subject").bind("change", subjectDetailsChanged);
					};

				this.on("change:subject", bindChangeEvent, this);

				bindChangeEvent();
			},

			/**
			 * Sets the model attribute defaults.
			 *
			 * @returns {Object} The model attribute defaults
			 */
			defaults : function() {
				var attrs = {
					"type" : diary.model.DiaryItem.TYPE.ASSIGNMENT,
					"assignedDay" : new Rocketboots.date.Day(),
					"assignedTime" : new Rocketboots.date.Time(),
					"dueDay" : new Rocketboots.date.Day().addDays(1),
					"dueTime" : null,
					"completed" : null,
					"subject" : new diary.model.Subject(),
					"AssignedBy" : 0,
					"responseRequired" : false,
					"progress":0,
					"priority":"Low",
					"estimatedHours":0,
					"estimatedMinutes":0,
					"webLinkDesc":null,
					"uniqueId":null,
					"assignedtoMe":1,
					"assignTo":[],
					"weight":null,
					"classId" : null
				};

				return _.extend(diary.model.DiaryItem.prototype.defaults(), attrs);
			},

			/**
			 * Validates the model attributes.
			 *
			 * @param {!Object} attrs The model attributes to be set
			 *
			 * @returns {String} Validation error, if any
			 */
			validate : function(attrs) {
				//console.log("in validate");
				var result = null,
					validTypes = [],
					dueTimeString;

				// validate base class
				result = diary.model.DiaryItem.prototype.validate.call(this, attrs);
				if (result != null) {
					return result;
				}

				// 'type' is required
				if (!isSpecified(attrs.type)) {
					return "Type must be specified and cannot be NULL";
				}
				console.log("in validate1");
				// 'type' must be either TASK or ASSIGNMENT
				validTypes.push(diary.model.DiaryItem.TYPE.ASSIGNMENT);
				validTypes.push(diary.model.DiaryItem.TYPE.HOMEWORK);

				if (_.indexOf(validTypes, attrs.type) == -1) {
					return "Type must be one of: " + validTypes;
				}

				// 'assignedDay' is required
				if (!isSpecified(attrs.assignedDay)) {
					return "Assigned Day must be specified and cannot be NULL";
				}
				console.log("in validate12");
				// 'assignedDay' must be a valid Rocketboots.date.Day
				if (!(attrs.assignedDay instanceof Rocketboots.date.Day)) {
					return "Assigned Day must be a valid Day";
				}

				// 'assignedTime' is required
				if (!isSpecified(attrs.assignedTime)) {
					return "Assigned Time must be specified and cannot be NULL";
				}

				// 'assignedTime' must be a valid Rocketboots.date.Time
				if (!(attrs.assignedTime instanceof Rocketboots.date.Time)) {
					return "Assigned Time must be a valid Time";
				}
				console.log("in validate13");
				// 'dueDay' is required
				if (!isSpecified(attrs.dueDay)) {
					return "Due Day must be specified and cannot be NULL";
				}
				console.log("in validate131");
				// 'dueDay' must be a valid Rocketboots.date.Day
				if (!(attrs.dueDay instanceof Rocketboots.date.Day)) {
					return "Due Day must be a valid Day";
				}
console.log("in validate132");
				// 'dueTime' must be a valid Rocketboots.date.Time, if specified
				if (isSpecified(attrs.dueTime) && !(attrs.dueTime instanceof Rocketboots.date.Time)) {
					return "Due Time must be a valid Time";
				}
console.log("in validate133");


				// 'dueDay' + 'dueTime' must be > 'assignedDay' + 'assignedTime'
				dueTimeString = (attrs.dueTime != null) ? attrs.dueTime.toString() : "2359";
				
				console.log("attrs.dueDay.getKey()"+attrs.dueDay.getKey());
				console.log("dueTimeString"+dueTimeString);
				console.log("attrs.assignedDay.getKey()"+attrs.assignedDay.getKey());
				console.log("attrs.assignedTime.toString()"+attrs.assignedTime.toString());
				
				console.log("attrs.dueDay.getKey() + dueTimeString < attrs.assignedDay.getKey() + attrs.assignedTime.toString()"+attrs.dueDay.getKey() + dueTimeString < attrs.assignedDay.getKey() + attrs.assignedTime.toString());
				
				if (attrs.dueDay.getKey() + dueTimeString < attrs.assignedDay.getKey() + attrs.assignedTime.toString()) {
					
					return "Due Day and Time must be after Assigned Day and Time";
				}
				console.log("in validate14");
				// 'completed' must be a timestamp, if specified
				if (isSpecified(attrs.completed) && !(attrs.completed instanceof Date)) {
					return "Date Completed must be a valid timestamp";
				}

				// 'subject' must be a Subject, if specified
				if (isSpecified(attrs.subject) && !(attrs.subject instanceof diary.model.Subject)) {
					return "Subject must be a valid Subject";
				}
				console.log("in validate15");
			},

			/**
			 * Determines if the Diary Item falls on the specified day. A Task is considered to fall
			 * on a day if it is either Assigned Day or Due Day matches the specified day.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day
			 */
			isWithinDay : function(day) {
				return this.get('assignedDay').getKey() == day.getKey()
					|| this.get('dueDay').getKey() == day.getKey();
			},
			
			isAssignedToday : function() {
				var day = new Rocketboots.date.Day();
				return this.get('assignedDay').getKey() == day.getKey();
			},

			/**
			 * Determines if the Diary Item falls within the specified time frame. A Task is
			 * considered to fall within a time frame if either the Assigned Day and Time or Due Day
			 * and Time matches the specified day and falls within the specified time frame. If no
			 * Due Time is specified for the Task, only the Assigned Date and Time will be
			 * considered.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 * @param {!Rocketboots.date.Time} startTime The start time from which to check
			 * @param {!Rocketboots.date.Time} endTime The end time from which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day and within the specified time frame
			 */
			isWithinTimeFrame : function(day, startTime, endTime) {

               return (this.get('assignedDay').getKey() == day.getKey()
               		&& isSpecified(this.get('assignedTime'))
					&& this.get('assignedTime').toInteger() >= startTime.toInteger()
					&& this.get('assignedTime').toInteger() < endTime.toInteger())
				|| (this.get('dueDay').getKey() == day.getKey()
					&& isSpecified(this.get('dueTime'))
					&& this.get('dueTime').toInteger() >= startTime.toInteger()
					&& this.get('dueTime').toInteger() < endTime.toInteger()
				);
			},

			/**
			 * Determines if the Task is due within the specified time frame. If no time frame
			 * is specified, this will only check the specified day. If a Start and End Time has
			 * been specified and the Task has no Due Time, it will be considered NOT due within the
			 * specified time frame.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 * @param {?Rocketboots.date.Time} startTime The start time from which to check
			 * @param {?Rocketboots.date.Time} endTime The end time from which to check
			 *
			 * @returns {Boolean} True if the Task is due on the specified date and within the specified time frame
			 */
			isDueWithinTimeFrame : function(day, startTime, endTime) {
				var isDue = day.getKey() == this.get('dueDay').getKey();

				if (isSpecified(startTime) && isSpecified(endTime)) {
					isDue = isDue && isSpecified(this.get('dueTime'))
						&& this.get('dueTime').toInteger() >= startTime.toInteger()
						&& this.get('dueTime').toInteger() < endTime.toInteger();
				}

				return isDue;
			},

			/**
			 * Determines if the Task has been marked 'completed'.
			 *
			 * @returns {Boolean} True if the Task has been marked 'completed'
			 */
			isCompleted : function () {
				return this.get('completed') != null;
			},
			
			isAssigned : function () {
				return this.get('AssignedBy') == 1;
			},

			/**
			 * Determines if the Task is overdue for the specified day and time. If day and time are not
			 * given it will determine if the task is overdue based on the current local time.
			 * Completed tasks are never considered overdue. A Task with no Due Time will be
			 * considered due at the beginning of the Due Day (i.e. at 00:00 AM).
			 *
			 * @param {?Rocketboots.date.Day} onDay The day from when to check
			 * @param {?Rocketboots.date.Time} onTime the time from when to check
			 *
			 * @returns {Boolean} True if the Task is overdue
			 */
			isOverdue : function (onDay, onTime) {
				var fromDay,
					fromTime,
					dueTimeString;

				if ((onDay && !onTime) || (!onDay && onTime)) {
					throw new Error ("Both Day and Time must be specified");
				}

				if (onDay && !(onDay instanceof Rocketboots.date.Day)) {
					throw new Error ("Day is not valid, day must be of type Rocketboots.date.Day");
				}

				if (onTime && !(onTime instanceof Rocketboots.date.Time)) {
					throw new Error ("Time is not valid, time must be of type Rocketboots.date.Time");
				}

				fromDay = onDay || new Rocketboots.date.Day();
				fromTime = onTime || new Rocketboots.date.Time();
				dueTimeString = (isSpecified(this.get('dueTime'))) ? this.get('dueTime').toString() : "0000";

				return !this.isCompleted()
					&& (this.get('dueDay').getKey() + dueTimeString) < (fromDay.getKey() + fromTime.toString());
			}
		}
	);

})();
