
function hexToR(h) {return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h) {return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h) {return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h}

function getContrastYIQ(hexcolor){
    var r = parseInt(hexcolor.substr(1,2),16);
    var g = parseInt(hexcolor.substr(3,2),16);
    var b = parseInt(hexcolor.substr(5,2),16);
    var yiq = ((r*299)+(g*587)+(b*114))/1000;
    return (yiq >= 128) ? 'black' : 'white';
}

function ColorCodes(ColorCode) 
{
// ColorCode = "#54A601";
var R = hexToR(ColorCode);
var G = hexToG(ColorCode);
var B = hexToB(ColorCode);
var lighter = lighterColor('rgba('+R+', '+G+', '+B+', .9)', .1);
 return lighter;
}


var pad = function(num, totalChars) {
    var pad = '0';
    num = num + '';
    while (num.length < totalChars) {
        num = pad + num;
    }
    return num;
};

// Ratio is between 0 and 1
var changeColor = function(color, ratio, darker) {
    // Trim trailing/leading whitespace
    color = color.replace(/^\s*|\s*$/, '');

    // Expand three-digit hex
    color = color.replace(
        /^#?([a-f0-9])([a-f0-9])([a-f0-9])$/i,
        '#$1$1$2$2$3$3'
    );

    // Calculate ratio
    var difference = Math.round(ratio * 256) * (darker ? -1 : 1),
        // Determine if input is RGB(A)
        rgb = color.match(new RegExp('^rgba?\\(\\s*' +
            '(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])' +
            '\\s*,\\s*' +
            '(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])' +
            '\\s*,\\s*' +
            '(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])' +
            '(?:\\s*,\\s*' +
            '(0|1|0?\\.\\d+))?' +
            '\\s*\\)$'
        , 'i')),
        alpha = !!rgb && rgb[4] != null ? rgb[4] : null,

        // Convert hex to decimal
        decimal = !!rgb? [rgb[1], rgb[2], rgb[3]] : color.replace(
            /^#?([a-f0-9][a-f0-9])([a-f0-9][a-f0-9])([a-f0-9][a-f0-9])/i,
            function() {
                return parseInt(arguments[1], 16) + ',' +
                    parseInt(arguments[2], 16) + ',' +
                    parseInt(arguments[3], 16);
            }
        ).split(/,/),
        returnValue;

    // Return RGB(A)
    return !!rgb ?
        'rgb' + (alpha !== null ? 'a' : '') + '(' +
            Math[darker ? 'max' : 'min'](
                parseInt(decimal[0], 10) + difference, darker ? 0 : 255
            ) + ', ' +
            Math[darker ? 'max' : 'min'](
                parseInt(decimal[1], 10) + difference, darker ? 0 : 255
            ) + ', ' +
            Math[darker ? 'max' : 'min'](
                parseInt(decimal[2], 10) + difference, darker ? 0 : 255
            ) +
            (alpha !== null ? ', ' + alpha : '') +
            ')' :
        // Return hex
        [
            '#',
            pad(Math[darker ? 'max' : 'min'](
                parseInt(decimal[0], 10) + difference, darker ? 0 : 255
            ).toString(16), 2),
            pad(Math[darker ? 'max' : 'min'](
                parseInt(decimal[1], 10) + difference, darker ? 0 : 255
            ).toString(16), 2),
            pad(Math[darker ? 'max' : 'min'](
                parseInt(decimal[2], 10) + difference, darker ? 0 : 255
            ).toString(16), 2)
        ].join('');
};
var lighterColor = function(color, ratio) {
    return changeColor(color, ratio, false);
};
var darkerColor = function(color, ratio) {
    return changeColor(color, ratio, true);
};

// Use
//var darker = darkerColor('rgba('+R+', '+G+', '+B+', .5)', .2);

//document.write('<div style="background: '+lighter+'; background: -moz-linear-gradient(top, '+lighter+' 0%, '+ColorCode+' 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,'+lighter+'), color-stop(100%,'+ColorCode+')); background: -webkit-linear-gradient(top, '+lighter+' 0%,'+ColorCode+' 100%); background: -o-linear-gradient(top, '+lighter+' 0%,'+ColorCode+' 100%); background: -ms-linear-gradient(top, '+lighter+' 0%,'+ColorCode+' 100%); background: linear-gradient(to bottom, '+lighter+' 0%,'+ColorCode+' 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\''+lighter+'\', endColorstr=\''+ColorCode+'\',GradientType=0 ); width:200px; height:200px; float:left; border-radius:4px;"></div>');

//document.write('<div style="background:linear-gradient(to bottom, '+lighter+' 10% , '+ColorCode+' 35% ); background:0,1 0 0 0, 2 0 0 0,3 0 1 0; background-radius:3; width:200px; height:200px; float:left; border-radius:4px;"></div>');
//document.write('<div style="background:linear-gradient(to bottom, #D73333 20% , #B80000 45% ); background:0,1 0 0 0, 2 0 0 0,3 0 1 0; background-radius:3; width:200px; height:200px; float:left; border-radius:4px;"></div>');
//document.write('<div style="background:'+darker+'; width:20px; height:30px; float:left;"></div>');
//document.write('<div style="background:'+ColorCode+'; width:20px; height:30px; float:left;"></div>');

