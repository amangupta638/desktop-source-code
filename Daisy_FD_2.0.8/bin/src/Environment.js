/* Environment = {
    "name": "Production",
	"debug": true,
	"debugConsole": true,
	"debugConsoleLogAll": true,
    "remoting": {
		"serverUrl": "http://marcellin.e-plannertest.com.au/desktopv8",
		"serverImageUrl": "http://marcellin.e-plannertest.com.au",
		"calendarAccessURL" : ".e-plannertest.com.au/calendar/?pop=1&popOpt=link&userID=",
		"updateUrl": "http://marcellin.e-plannertest.com.au/client/update.xml"
    },
    "splashDelay": 1000
}; */

Environment = {
    "name": "Production",
    "debug": false,
	"debugConsole": false,
	"debugConsoleLogAll": false,
    "remoting": {
		"serverUrl": "https://e-planner.com.au/desktopv7",
		"serverImageUrl": "https://e-planner.com.au",
		"calendarAccessURL" : ".e-planner.com.au/calendar/?pop=1&popOpt=link&userID=",
		"updateUrl": "https://e-planner.com.au/client/update.xml"
    },
    "splashDelay": 1000
};