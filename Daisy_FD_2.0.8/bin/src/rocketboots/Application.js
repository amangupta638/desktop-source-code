/**
 * $Id: Application.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

Rocketboots.Application = (function() {

	var App = function(defaults) {
		// Mix in event support

		_.extend(this, Backbone.Events);
		_.extend(this, defaults);
		
		this.templateRenderer = null;
	};

	/*
	 * Namespaces
	 */
	App.prototype = {
		collection: {},
		controller: {},
		service: {},
		view: {}
	};

	/*
	 * initialize() 
	 * 
	 */
	App.prototype.initialize = function() {

		var app = this;
        if (typeof(this.preAppInitialisation) !== "undefined") {
            this.preAppInitialisation.apply(this, [this]);
        }

		// Create application model
		if (typeof(this.modelClass) !== "undefined") {
			this.model = new this.modelClass;
		}

		// Create popup manager
		if (typeof(this.popupManager) !== "undefined") {
			this.popup = new this.popupManager;
		}
				
		// Create context (event -> command mappings)
		this.context = new Rocketboots.Context();
	
		if (typeof(this.mapEvents) !== "undefined") {
			this.mapEvents.apply(this);
		}
	
		if (typeof(this.contextCreated) !== "undefined") {
			this.contextCreated.apply(this, [this.context]);
		}
		
		this.trigger("contextCreated", this.context);

		// Create locator (name -> service mapping)
	
		this.locator = new Rocketboots.Locator();
	
		if (typeof(this.mapServices) !== "undefined") {
			this.mapServices.apply(this);
		}

		if (typeof(this.locatorCreated) !== "undefined") {
			this.locatorCreated.apply(this, [this.locator]);
		}

		this.trigger("locatorCreated", this.locator);
	
		// If registerTemplates 
		if (typeof(this.registerTemplates) === "undefined") {
			invokeAppInitialized.apply(this);
			
			return this.context;
		}
		//alert("App.prototype.initialize");
		this.templateRenderer = new Rocketboots.template.TemplateRenderer({
			templates: this.registerTemplates,
			initialized: function() {
				invokeAppInitialized.apply(app);
			}
		}).initialize();
		
		return this.context;
	};
	
	function invokeAppInitialized() {
		if (typeof(this.appInitialized) !== "undefined") {
			this.appInitialized.apply(this, [this]);
		}

		this.trigger("appInitialized", this);
	}
	
	App.prototype.mapEvent = function(eventName, command) {
		this.context.mapEvent(eventName, command);
	};
	
	App.prototype.mapService = function(serviceName, service) {
		this.locator.mapService(serviceName, service);
	};
	
	App.prototype.render = function(name, params) {
	console.log("App.prototype.render name="+name);
		if (this.templateRenderer == null) {
			throw new Error("No templates registered");
		}
		
		return this.templateRenderer.render(name, params);
	};
	
	return App;
	
}).call(this);
