/**
 * $Id: Debug.as 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
package com.rocketboots.debug {

	// For now, just proxy out to Arthropod
	import com.carlcalderon.arthropod.Debug;
	
	import flash.display.Stage;
	
	public class Debug {
		
		/**
		 * Traces message
		 * 
		 * @param	message		Message to be traced
		 * @param	color		opt. Color of the message
		 * @return				True if successful
		 */
		public static function log ( message:*, color:uint = 0xFEFEFE ) :Boolean {
			return com.carlcalderon.arthropod.Debug.log(message, color);	
		}
		
		/**
		 * Trace an error
		 * 
		 * @param	message		Message to be traced
		 * @return				True if successful
		 */
		public static function error ( message:* ) :Boolean {
			return com.carlcalderon.arthropod.Debug.error(message);	
		}
		
		/**
		 * Trace a warning
		 * 
		 * @param	message		Message to be traced
		 * @return				True if successful
		 */
		public static function warning ( message:* ) :Boolean {
			return com.carlcalderon.arthropod.Debug.error(message);	
		}
		
		/**
		 * Clears all the traces
		 * 
		 * @return				True if successful
		 */
		public static function clear ( ) :Boolean {
			return com.carlcalderon.arthropod.Debug.clear();
		}
		
		/**
		 * Traces an array
		 * 
		 * @param	arr			Array to be traced
		 * @return				True if successful
		 */
		public static function array ( arr:Array ) :Boolean {
			return com.carlcalderon.arthropod.Debug.array(arr);
		}
		
		/**
		 * Traces a thumbnail of the specified BitmapData.
		 *
		 * @param	bmd			Any IBitmapDrawable
		 * @param	label		Label
		 * @return				True if successful
		 */
		public static function bitmap ( bmd:*, label:String = null ) :Boolean {
			return com.carlcalderon.arthropod.Debug.bitmap(bmd, label);
		}
		
		/**
		 * Traces a snapshot of the current stage state.
		 * 
		 * @param	stage		Stage
		 * @param	label		Label
		 * @return				True if successful
		 */
		public static function snapshot ( stage:Stage, label:String=null ) :Boolean {
			return com.carlcalderon.arthropod.Debug.snapshot(stage, label);
		}
		
		/**
		 * Traces an <code>object</code>.
		 * The first level of arguments are traced as follows:
		 * 
		 * trace:
		 * Debug.object( { name: Carl, surname: Calderon } );
		 * 
		 * output:
		 * object
		 * name: Carl
		 * surname: Calderon
		 * 
		 * @param	obj			Object to be traced
		 * @return				True if successful
		 */
		public static function object ( obj:* ) :Boolean {
			return com.rocketboots.debug.Debug.object(obj);
		}
		
		/**
		 * Traces the current memory size used by Adobe Flash Player.
		 * Observe that this also includes AIR applications. Use with care.
		 * 
		 * @return				True if successful
		 */
		public static function memory ( ) :Boolean {
			return com.carlcalderon.arthropod.Debug.memory();
		}
	}
	
}
