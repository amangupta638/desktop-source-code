package
{
	import com.hyakugei.Mustache;
	import com.rocketboots.debug.Debug;
	
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.desktop.NativeApplication;
	import flash.display.NativeWindowInitOptions;
	import flash.display.NativeWindowSystemChrome;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.InvokeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.StatusEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.html.HTMLLoader;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	import flash.net.URLRequest;
	import flash.system.Capabilities;
	
	import mx.collections.ArrayCollection;
	import mx.graphics.codec.PNGEncoder;
	import mx.utils.Base64Encoder;
	import mx.utils.StringUtil;
	
	import air.net.URLMonitor;
	import air.update.ApplicationUpdaterUI;
	import air.update.events.StatusUpdateEvent;
	import air.update.events.UpdateEvent;
	
	import au.com.productdynamics.view.ClippedRoundedWindow;
	
	import deng.fzip.FZip;
	import deng.fzip.FZipErrorEvent;
	
	import nochump.util.zip.ZipFile;
	import flash.utils.*;
	import flash.net.URLRequestDefaults;
	
	
	
	public class DigitalDiary extends Sprite
	{		
		/**
		 * File extensions 
		 */
		private var MANIFEST_EXTENSION 	: String = ".ddm";
		private var ARCHIVE_EXTENSION  	: String = ".dda";
		private var DEMO_EXTENSION		: String = ".ddd";
		
		/**
		 * Network test URL
		 */
		private var NETWORK_TEST_URL : String = "http://www.google.com";
		
		/**
		 * Application states
		 */
		private var APP_STATE_INITIAL : int = 0;
		private var APP_STATE_LOADING : int = 1;
		private var APP_STATE_RUNNING : int = 2;
		
		private var isAppActive:Boolean = true;
		
		private static const WINDOWS_TASK_BAR : int = 40;
		private static const MAC_MENU_BAR : int = 22;
		private static const MAC_DOCK : int = 60;
		
		private var isMacOS:Boolean = Capabilities.os.indexOf("Mac OS") >= 0;
		
		private var ROOT_WINDOW_MIN_WIDTH : int = Capabilities.screenResolutionX;
		private var ROOT_WINDOW_MAX_WIDTH : int = Capabilities.screenResolutionX;
		private var ROOT_WINDOW_MAX_HEIGHT : int = Capabilities.screenResolutionY - (isMacOS ? MAC_MENU_BAR : WINDOWS_TASK_BAR);
		private var ROOT_WINDOW_MIN_HEIGHT : int = Capabilities.screenResolutionY - (isMacOS ? MAC_MENU_BAR : WINDOWS_TASK_BAR);
		
		private var currentDir:File;
		private var manifestLocation:String;
		private var archiveLocation:String;
		private var demoArchiveLocation:String;
		private var htmlLoader : HTMLLoader;
		private var clippedMask : ClippedRoundedWindow;
		private var macAddresses : Array;
		private var networkMonitor : URLMonitor;
		private var check :String;
		
		private var appUpdater:ApplicationUpdaterUI;
		private var allowautoupdate:int;
		// Flags to keep track of the state of the app
		public var applicationState : int;		
		
		public function onInvokeEvent(invocation:InvokeEvent):void {
			currentDir = invocation.currentDirectory;
			
			var now:String = new Date().toString();
			Debug.log("Invoke event received: " + now);
			
			var launchWithFile:Boolean = false;
			
			{ // TODO: remove logging 
				if (invocation.currentDirectory != null){
					Debug.log("Current directory=" + invocation.currentDirectory.nativePath);
				} else {
					Debug.log("--no directory information available--");
				}
				
				if (invocation.arguments.length > 0){
					Debug.log("Arguments: " + invocation.arguments.toString());
				} else {
					Debug.log("--no arguments--");
				}		
			}
			
			if (invocation.arguments.length == 1) {
				var arg:String = invocation.arguments[0].toString();
				var ext:String = arg.substr(arg.length - 4, 4);
				
				// check if this argument has the manifest file extension
				if (ext == MANIFEST_EXTENSION) {
					manifestLocation = arg;
					Debug.log("manifest location = " + manifestLocation);
					launchWithFile = true;
				} else if (ext == ARCHIVE_EXTENSION) {
					archiveLocation = arg;
					Debug.log("archive location = " + archiveLocation);
					launchWithFile = true;
				} else if (ext == DEMO_EXTENSION) {
					demoArchiveLocation = arg;
					Debug.log("demo archive location = " + demoArchiveLocation);
					launchWithFile = true;
				}
				
			}
			
			
			macAddresses = new Array();
			check="";
			var networkInterfaces : Vector.<NetworkInterface> =  NetworkInfo.networkInfo.findInterfaces();				
			for each (var networkInfo:NetworkInterface in networkInterfaces) {
				trace(" displayName ="+ networkInfo.name);
				trace(" active="+ networkInfo.active);
				check +=StringUtil.trim(networkInfo.hardwareAddress.toString())+",state="+networkInfo.active+"---";
				var macAddress:String = StringUtil.trim(networkInfo.hardwareAddress.toString());
				if (macAddress != "") {
					macAddresses.push(macAddress);
				}
			}			
			macAddresses.sort();
			trace("check");
			trace(check);
			
			/**
			 * On each invoke event, check if the application is already running. 
			 * If so, alert the user instead of relaunching the htmlLoader.
			 */ 			
			switch(applicationState) {
				case APP_STATE_INITIAL : 
					htmlLoader_load();
					break;
				case APP_STATE_LOADING : 
					// wait ?
					Debug.log("Application is currently loading html container");
					break;
				case APP_STATE_RUNNING : 
					// ?? 					
					Debug.log("Application is currently running");
					if (htmlLoader.window.nativeWindow) {
						if (launchWithFile) {
							htmlLoader.window.alert("Digital Diary is currently open. Please close the application and retry.");
						} 
					}
					break;
				default : 
					Debug.log("Invalid application state");
			}
		}
		
		public function DigitalDiary() : void {
			trace("DigitalDiary()");
			applicationState = APP_STATE_INITIAL;
			
			// register invoke event handler
			NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, onInvokeEvent);
			
			try {
				// read in the environment variables
				//	Alert("ashish");
				var f:File = File.applicationDirectory.resolvePath("src/Environment.js");
				var fs:FileStream = new FileStream();
				fs.open(f, FileMode.READ);
				var jsonStr:String = fs.readUTFBytes(fs.bytesAvailable);
				fs.close();
				
				// just get the remote services property as a string
				jsonStr = jsonStr.match(/(?<="remoting":)\s?{[\s\S]*?}/)[0];
				
				var updateUrl:String = JSON.parse(jsonStr).updateUrl;
				allowautoupdate= -1;
				DatabaseManager();
				
				/*while(true)
				{
				if(allowautoupdate == 1)
				{
				throw new Error("");
				}
				}*/
				if(allowautoupdate == 0)
				{
					return;
				}
				
				if(updateUrl == null || updateUrl == "") 
				{
					throw new Error("");
				}
				
				Debug.log("Starting application");
				
				appUpdater = new ApplicationUpdaterUI();
				appUpdater.updateURL = updateUrl;
				appUpdater.isCheckForUpdateVisible = false;
				appUpdater.addEventListener(UpdateEvent.INITIALIZED, function(event:UpdateEvent):void {
					appUpdater.checkNow();
				});
				appUpdater.addEventListener(ErrorEvent.ERROR, function(event:ErrorEvent):void {
					Debug.log("error updating application" + event.toString());
				});
				appUpdater.addEventListener(StatusUpdateEvent.UPDATE_STATUS,function( event :StatusUpdateEvent) :void{
					
					trace(event.versionLabel);
					
				});
				appUpdater.initialize();
			} catch (e:Error) {
				Debug.log("Allow to update: " + allowautoupdate);
			}
		}
		
		private function htmlLoader_load() : void {
			var windowInitOptions : NativeWindowInitOptions = new NativeWindowInitOptions();			
			windowInitOptions.systemChrome = NativeWindowSystemChrome.NONE;
			windowInitOptions.transparent = true;
			
			var appWidth : int = ROOT_WINDOW_MIN_WIDTH;
			var appHeight : int = ROOT_WINDOW_MIN_HEIGHT;
			var appX : int = (Capabilities.screenResolutionX - appWidth)/2;
			var appY : int = (Capabilities.screenResolutionY - (appHeight+50))/2;
			var bounds : Rectangle = new Rectangle(appX, appY, appWidth, appHeight);
			
			var initialUrl : URLRequest = new URLRequest("src/index.html");
			
			htmlLoader = HTMLLoader.createRootWindow(
				false,
				windowInitOptions,
				false,
				bounds
			);
			
			clippedMask = new ClippedRoundedWindow(htmlLoader);
			htmlLoader.parent.stage.nativeWindow.minSize = new Point(ROOT_WINDOW_MIN_WIDTH, ROOT_WINDOW_MIN_HEIGHT);
			htmlLoader.parent.stage.nativeWindow.maxSize = new Point(ROOT_WINDOW_MAX_WIDTH, ROOT_WINDOW_MAX_HEIGHT);
			
			htmlLoader.addEventListener(Event.COMPLETE, htmlLoader_complete);
			htmlLoader.parent.stage.nativeWindow.addEventListener(Event.CLOSE, htmlLoader_close);
			
			// extract window title from application descriptor
			var appDescriptor:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appDescriptor.namespace();
			var appName:String = appDescriptor.ns::name;
			htmlLoader.stage.nativeWindow.title = appName;
			
			applicationState = APP_STATE_LOADING;
			
			URLRequestDefaults.idleTimeout = 120000;
			
			htmlLoader.load(initialUrl);
		}
		
		private function announceNetworkStatus(e:StatusEvent) : void {			
			Debug.log("Network Status change. Current status: " + networkMonitor.available);
			
			if (htmlLoader.window.nativeWindow) {
				htmlLoader.window.announceNetworkStatus(networkMonitor.available);
			}
		}
		
		private function onUserIdle(e:Event):void
		{ 
			
			// No keyboard or mouse input for 2 minutes. 
			isAppActive = false;
			trace("isAppActive: " + isAppActive);
		}
		private function onUserPresent(e:Event):void
		{
			// The user is back! 
			isAppActive = true;
			trace("isAppActive: " + isAppActive);
		}
		
		private function htmlLoader_complete(event:Event) : void {
			// Explicitly inject AS classes into the HTML because this isn't working
			// http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7ed5.html#WS5b3ccc516d4fbf351e63e3d118666ade46-7ec5
			
			htmlLoader.window.nochump = {
				util: {
					zip: {
						ZipFile: ZipFile
					}
				}
			};
			
			htmlLoader.window.deng = {
				fzip: {
					FZip: FZip,
					FZipErrorEvent: FZipErrorEvent
				}
			};
			
			htmlLoader.window.com = {
				hyakugei: {
					Mustache: com.hyakugei.Mustache
				},
				rocketboots: {
					debug: {
						Debug: Debug
					}
				}
			};
			
			htmlLoader.window.au = {
				com: {
					productdynamics: {
						view: {
							ClippedRoundedWindow: ClippedRoundedWindow
						}
					}
				}
			};
			
			htmlLoader.window.mx = {
				graphics: {
					codec: {
						PNGEncoder: PNGEncoder
					}
				},
				utils: {
					Base64Encoder : Base64Encoder
				}
			};
			
			// pass in command line arguments
			htmlLoader.window.args = {
				ManifestLocation : manifestLocation, 
				ArchiveLocation : archiveLocation, 
				DemoArchiveLocation : demoArchiveLocation, 
				Check :check,
				MacAddresses : macAddresses.toString()
			};
			
			networkMonitor = new URLMonitor(new URLRequest(NETWORK_TEST_URL));
			networkMonitor.addEventListener(StatusEvent.STATUS, announceNetworkStatus);
			networkMonitor.start();			
			
			NativeApplication.nativeApplication.idleThreshold = 6 * 60; // In seconds
			NativeApplication.nativeApplication.addEventListener(Event.USER_IDLE, onUserIdle);
			NativeApplication.nativeApplication.addEventListener(Event.USER_PRESENT, onUserPresent);
			
			setInterval(detectIdleApp,1000);

			htmlLoader.window.monitors = 
				{
					network : networkMonitor
				};
			
			if (htmlLoader.window.nativeWindow) 
			{
				// Signal to app that all the container is now properly set up
				htmlLoader.window.containerIsReady();
				applicationState = APP_STATE_RUNNING;
			}
			
		}
		
		private function htmlLoader_close(event:Event) : void {
			Debug.log("htmlLoader closed. Exiting.");
			NativeApplication.nativeApplication.exit();
		}
		
		private function detectIdleApp() : void {
			//trace("detectIdleApp : " + isAppActive);
			htmlLoader.window.monitors1 = 
				{
					isIdle : isAppActive
				};
		}
		
		private var dbconn:SQLConnection;
		private var sqlQuery:SQLStatement= new SQLStatement();
		private var sqlCreateTable:SQLStatement;
		private var sqlInsert:SQLStatement;
		private var sqlImport:SQLStatement;
		
		[Bindable] private var dp:ArrayCollection = new ArrayCollection();
		
		//--------------listner functions --------------
		public function onSQLError(e:SQLErrorEvent):void
		{
			//Alert.show(e.toString());
		}
		
		private function result(e:SQLEvent):void
		{
			// with sqls.getResault().data we get the array of objects for each row out of our database
			var result:SQLResult  = sqlQuery.getResult();
			// we pass the array of objects to our data provider to fill the datagrid
			//allowautoupdate =	data[0];
			
			if (result != null)
			{
				allowautoupdate = result.data[0]["AllowAutoUpdates"];
			}
			
			
			
			//dp = new ArrayCollection(data);
			//dp.getItemAt(0,0);
			
		}
		
		public function onSQLOpen(e:SQLEvent) :void 
		{
			try 
			{
				sqlQuery.sqlConnection = dbconn;
				// in property text of our SQLStatment we write our sql command. We can also combine sql statments in our text property so that more than one statment can be executed at a time.
				// in this sql statment we create table in our database with name "test_table" with three columns (id, first_name and last_name). Id is an integer that is auto incremented when each item is added. First_name and last_name are columns in which we can store text
				// If you want to know more about sql statments search the web.
				sqlQuery.text = "SELECT AllowAutoUpdates FROM schoolProfile";
				//sqlQuery.itemClass = AllowUpdateClass;
				// after we have connected sql statment to our sql connection and writen our sql commands we also need to execute our sql statment.
				// nothing will change in database until we execute sql statment.
				sqlQuery.execute();
				
				// after we load the database and create the table if it doesn't already exists, we call refresh method which i have created to populate our datagrid
				//refresh();
			}
			catch(e:Error)
			{
				Debug.log("Error occured while fetching data from schoolProfile.");
			}
		}
		
		public function onSQLCreate(e:SQLEvent):void
		{
			
		}
		//----------------
		public function DatabaseManager(): void 
		{
			//Connect to the db
			dbconn = new SQLConnection();
			//Add event listener
			dbconn.addEventListener( SQLErrorEvent.ERROR, onSQLError );
			//Set the location of the db file              
			var dbFile:File = File.applicationStorageDirectory.resolvePath( "diary.pd");
			//Check if the db file exsists
			//if ( dbFile.exists )
			//{
			dbconn.addEventListener( SQLEvent.OPEN, onSQLOpen );
			sqlQuery.addEventListener(SQLEvent.RESULT, result);
			//	} 
			//	else 
			//{
			
			//	}
			
			//Execute
			
			dbconn.open(dbFile);        
			
		} 
		
		
	}
}