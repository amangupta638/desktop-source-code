package au.com.productdynamics.view
{
	import flash.display.NativeWindowDisplayState;
	import flash.display.Sprite;
	import flash.events.NativeWindowBoundsEvent;
	import flash.geom.Rectangle;
	import flash.html.HTMLLoader;

	public class ClippedRoundedWindow
	{
		private var htmlLoader : HTMLLoader;
		private var elipticalMask:Sprite;
		private var radius : Number;
		
		public function ClippedRoundedWindow(htmlLoader:HTMLLoader, radius:Number=16) {
			this.htmlLoader = htmlLoader;
			this.elipticalMask = new Sprite();
			this.radius = radius;
			
			htmlLoader.stage.nativeWindow.addEventListener(NativeWindowBoundsEvent.RESIZE, onWindowResize);

			setClipMask(htmlLoader.stage.nativeWindow.bounds);

			htmlLoader.parent.addChild(elipticalMask);
			htmlLoader.mask = elipticalMask;
		}

		private function setClipMask(bounds:Rectangle):void{
			var isMaximised : Boolean = htmlLoader.stage.nativeWindow.displayState == NativeWindowDisplayState.MAXIMIZED
			
			elipticalMask.graphics.clear();
			elipticalMask.graphics.beginFill(0xffffff,1);
			
			if (isMaximised) {
				elipticalMask.graphics.drawRect(0, 0, bounds.width, bounds.height);			
			}
			else {
				elipticalMask.graphics.drawRoundRect(0, 0, bounds.width, bounds.height, radius, radius);
			}
			
			elipticalMask.graphics.endFill();         
		}
		
		private function onWindowResize(event:NativeWindowBoundsEvent):void{
			setClipMask(event.afterBounds);
		}
	}
}