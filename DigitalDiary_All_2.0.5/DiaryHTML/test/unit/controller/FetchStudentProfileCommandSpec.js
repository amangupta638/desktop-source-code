/**
 * Unit Tests for the FetchStudentProfileCommand
 * 
 * author - Justin Judd
 */
describe("FetchStudentProfileCommand - Test Suite", function() {

	/**
	 * Suite of tests for proper operation of the FetchStudentProfileCommand
	 */
	describe("Successful cases for FetchStudentProfileCommand Suite" , function() {

		// the dummy student manager
		var dummyStudentManager = (function() {

			var service = function() {};

			// the dummy service of the student profile
			service.prototype.getStudentProfile = function(successCallback , errorCallback) {

				// the student profile
				var studentProfile = new diary.model.StudentProfile({
					'name' : "Test User",
					'dateOfBirth' : new Date(1975, 2, 7),
					'address' : new diary.model.Address({
						'street' : "Test Street",
						'suburb' : "Test Town",
						'state' : "NSW",
						'postcode' : "2000"
					}),
					'telephone' : "(02) 4325 6789",
					'email' : "test@gmail.com",
					'grade' : "U1",
					'teacher' : null,
					'house' : null,
					'houseCaptain' : null,
					'houseColours' : null
				});
				
				successCallback(studentProfile);

			};

			return service;

		})();
		
		// setup the env for the the unit test
		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

				modelClass:	diary.model.DiaryModel,
				
				mapEvents: function() {
					// map events needed for the test
				},

				mapServices: function() {
					// map the services needed for the test
					this.mapService(ServiceName.STUDENTMANAGER, dummyStudentManager);
				}

			});

			
			// init the application context
			app.initialize();

		});

		/*
		 * invoke the command
		 */
		it("should be able to invoke the command", function() {

			// holds the command
			var command = new diary.controller.FetchStudentProfileCommand();
			
			// execute the command
			command.execute();
			
			// get the student profile from the model
			var studentProfile = app.model.get(ModelName.STUDENTPROFILE);

			// the delegate should have returned a student profile
			expect(studentProfile).not.toBeNull();
			expect(studentProfile instanceof diary.model.StudentProfile).toBeTruthy();

			// the profile that was returned should be the same as the dummy data
			expect(studentProfile.get("name")).toEqual("Test User");
			expect(studentProfile.get("dateOfBirth")).toEqual(new Date(1975, 2, 7));
			expect(studentProfile.get("address")).not.toBeNull();
			expect(studentProfile.get("address").get("street")).toEqual("Test Street");
			expect(studentProfile.get("address").get("suburb")).toEqual("Test Town");
			expect(studentProfile.get("address").get("state")).toEqual("NSW");
			expect(studentProfile.get("address").get("postcode")).toEqual("2000");
			expect(studentProfile.get("telephone")).toEqual("(02) 4325 6789");
			expect(studentProfile.get("email")).toEqual("test@gmail.com");
			expect(studentProfile.get("grade")).toEqual("U1");
			expect(studentProfile.get("teacher")).toBeNull();
			expect(studentProfile.get("house")).toBeNull();
			expect(studentProfile.get("houseCaptain")).toBeNull();
			expect(studentProfile.get("houseColours")).toBeNull();

		});

	});

});