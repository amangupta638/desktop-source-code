/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: UpdateClassCommandSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

// TODO: Check Subject instances for identity, not just attributes
describe("UpdateClassCommand Test Suite", function() {

	//--------------------------------------------------------------------------
	//
	//  Test Data
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  subjects
	//----------------------------------

	var subjects = [ new diary.model.Subject({
		"id" : 1,
		"title" : "Test Subject 1",
		"colour" : "aqua"
	}), new diary.model.Subject({
		"id" : 2,
		"title" : "Test Subject 2",
		"colour" : "red"
	}) ];

	//--------------------------------------------------------------------------
	//
	//  Mock Service Callbacks
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  createSubject
	//----------------------------------

	var createSubjectCallbackNew = function(subject, success, error) {

		// return a new instance of a new subject
		success(new diary.model.Subject({
			"id" : 3,
			"title" : subject.get("title"),
			"colour" : subject.get("colour")
		}));

	};

	var createSubjectCallbackExisting = function(subject, success, error) {

		// return a new instance of an existing subject (find by title)
		var a = 0,
			origSubject;
		for (a in subjects) {
			origSubject = subjects[a];
			if (origSubject.get("title") == subject.get("title")) {
				success(new diary.model.Subject({
					"id" : origSubject.get("id"),
					"title" : origSubject.get("title"),
					"colour" : origSubject.get("colour")
				}));
				break;
			} else {
				error("Can't find subject");
			}
		}

	};

	var createSubjectCallbackError = function(subject, success, error) {

		// return an error
		error("Test error message");

	};

	// ----------------------------------
	//  deleteSubject
	//----------------------------------

	var deleteSubjectCallbackTrue = function(subject, success, error) {

		success(true);

	};

	var deleteSubjectCallbackFalse = function(subject, success, error) {

		success(false);

	};

	var deleteSubjectCallbackError = function(subject, success, error) {

		// return an error
		error("Test error message");

	};

	// ----------------------------------
	//  updateClassInTimetable
	//----------------------------------

	var updateClassInTimetableCallback = function(timetable, classModel,
			success, error) {

		// replace class in timetable
		var classes = timetable.get("classes");
		classes.remove(classModel.get("id"));
		classes.add(classModel);

		success(timetable, classModel);

	};

	var updateClassInTimetableCallbackError = function(timetable, classModel,
			success, error) {

		// return an error
		error("Test error message");

	};

	//--------------------------------------------------------------------------
	//
	//  Mock Services
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  TimetableManager
	//----------------------------------

	var MockTimetableManager = (function() {

		var _me = function() {
		};

		_me.prototype.createSubject = createSubjectCallbackExisting;

		_me.prototype.deleteSubject = deleteSubjectCallbackFalse;

		_me.prototype.updateClassInTimetable = updateClassInTimetableCallback;

		return _me;

	})();

	//--------------------------------------------------------------------------
	//
	//  Mock Application
	//
	//--------------------------------------------------------------------------

	var mockApp = new Rocketboots.Application(
			{
				modelClass : diary.model.DiaryModel,

				mapEvents : function() {
				},

				mapServices : function() {
					this.mapService(ServiceName.TIMETABLEMANAGER,
							MockTimetableManager);
				}
			});

	// init the app
	mockApp.initialize();

	//--------------------------------------------------------------------------
	//
	//  Spies
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  Application Events
	//----------------------------------

	var eventSpy = jasmine.createSpy("EventSpy");
	mockApp.context.on(EventName.CLASSUPDATED, eventSpy);

	//----------------------------------
	//  Services
	//----------------------------------

	/**
	 * Setup spies on service methods. Pass in custom callbacks to override the
	 * default callbacks on the mock service.
	 */
	var setupServiceSpies = function(createSubjectCallback,
			deleteSubjectCallback, updateClassInTimetableCallback) {

		// get service instance
		var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

		// configure spies
		spies = {
			"updateClassInTimetable" : spyOn(service, "updateClassInTimetable"),
			"createSubject" : spyOn(service, "createSubject"),
			"deleteSubject" : spyOn(service, "deleteSubject")
		};

		if (createSubjectCallback == null)
			spies.createSubject.andCallThrough();
		else
			spies.createSubject.andCallFake(createSubjectCallback);

		if (deleteSubjectCallback == null)
			spies.deleteSubject.andCallThrough();
		else
			spies.deleteSubject.andCallFake(deleteSubjectCallback);

		if (updateClassInTimetableCallback == null)
			spies.updateClassInTimetable.andCallThrough();
		else
			spies.updateClassInTimetable
					.andCallFake(updateClassInTimetableCallback);

	};

	//--------------------------------------------------------------------------
	//
	//  Standard Assertions
	//
	//--------------------------------------------------------------------------

	/**
	 * Assert postconditions succeeded.
	 */
	var assertPostConditionsSuccess = function(expectSubjectCreated,
			expectSubjectDeleted) {

		var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

		// postcondition: TimetableManager::createSubject() is called
		expect(service.createSubject.callCount).toEqual(1);
		
		// postcondition: TimetableManager::updateClassInTimetable() is called
		expect(service.updateClassInTimetable.callCount).toEqual(1);

		// postcondition: "classUpdated" event is fired
		expect(eventSpy.callCount).toEqual(1);

		// postcondition: TimetableManager::deleteSubject() is called
		expect(service.deleteSubject.callCount).toEqual(1);

		var subjects = app.model.get(ModelName.SUBJECTS);
		var subjectCount = 2;	// original number of subjects
		if (expectSubjectCreated) {
			// postcondition: subject added to app model
			expect(subjects.get(3) instanceof diary.model.Subject).toBeTruthy();
			subjectCount++;
		} else {
			// postcondition: subject not added to app model
			expect(subjects.get(3)).toBeUndefined();
		}

		if (expectSubjectDeleted) {
			// postcondition: subject removed from app model
			expect(subjects.get(1)).toBeUndefined();
			subjectCount--;
		} else {
			// postcondition: subject not removed from app model
			expect(subjects.get(1) instanceof diary.model.Subject).toBeTruthy();
		}
		expect(subjects.length).toEqual(subjectCount);

	};

	/**
	 * Assert postconditions failed.
	 */
	var assertPostConditionsFailure = function(expectSubjectCreated,
			expectSubjectDeleted) {

		var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

		// postcondition: TimetableManager::createSubject() is not called
		expect(service.createSubject.callCount).toEqual(0);

		// postcondition: TimetableManager::updateClassInTimetable() is not
		// called
		expect(service.updateClassInTimetable.callCount).toEqual(0);

		// postcondition: "classUpdated" event is not fired
		expect(eventSpy.callCount).toEqual(0);

		// postcondition: TimetableManager::deleteSubject() is not called
		expect(service.deleteSubject.callCount).toEqual(0);

		// postcondition: no subjects added to or removed from app model
		var subjects = app.model.get(ModelName.SUBJECTS);
		expect(subjects.length).toEqual(2);
		expect(subjects.get(1) instanceof diary.model.Subject).toBeTruthy();
		expect(subjects.get(2) instanceof diary.model.Subject).toBeTruthy();
		expect(subjects.get(3)).toBeUndefined();

	};

	//--------------------------------------------------------------------------
	//
	//  Test Cases / Specs
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  setup / teardown
	//----------------------------------

	beforeEach(function() {
		// populate the test data
		mockApp.model.set(ModelName.SUBJECTS,
				new diary.collection.SubjectCollection(subjects));

		// reset the spy counters
		eventSpy.reset();
		
		// replace global app with our mock app
		app = mockApp;
	});

	//----------------------------------
	//  execute() - success
	//----------------------------------

	describe("UpdateClassCommand::execute() - Success cases", function() {

		// subject exists; room provided
		it("should succeed with an existent subject", function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});
			var room = "Test Room";

			// execute the command
			var command = new diary.controller.UpdateClassCommand();
			command.execute(timetable, classId, subject.get("title"), room);

			// no exceptions / errors

			// standard postconditions
			assertPostConditionsSuccess(false, false);

		});

		// subject does not exist; room provided
		it("should succeed with a nonexistent subject", function() {

			// setup service spies
			setupServiceSpies(createSubjectCallbackNew, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});
			var room = "Test Room";

			// execute the command
			var command = new diary.controller.UpdateClassCommand();
			command.execute(timetable, classId, "Test Subject 3", room);

			// no exceptions / errors

			// standard postconditions
			assertPostConditionsSuccess(true, false);

		});

		// subject exists; room not provided
		it("should succeed without a 'room' argument", function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();
			command.execute(timetable, classId, subject.get("title"));

			// no exceptions / errors

			// standard postconditions
			assertPostConditionsSuccess(false, false);

		});

		// subject orphaned
		it("should delete an orphaned subject", function() {

			// setup service spies
			setupServiceSpies(null, deleteSubjectCallbackTrue, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();
			command.execute(timetable, classId, subject.get("title"));

			// no exceptions / errors

			// standard postconditions
			assertPostConditionsSuccess(false, true);

		});

	});

	//----------------------------------
	//  execute() - failure
	//----------------------------------

	describe("UpdateClassCommand::execute() - Failure cases", function() {

		// timetable argument is not provided
		it("should fail without the 'timetable' argument", function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();

			expect(function() {
				command.execute(null, classId, subject.get("title"));
			}).toThrow("Timetable must be specified");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// classId argument is not provided
		it("should fail without a 'classId' argument", function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();

			expect(function() {
				command.execute(timetable, null, subject.get("title"));
			}).toThrow("Class Identifier must be specified");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// subjectTitle argument is not provided
		it("should fail without a 'subjectTitle' argument", function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();

			expect(function() {
				command.execute(timetable, classId, null);
			}).toThrow("Subject Title must be specified");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// timetable argument is not Timetable object
		it("should fail when the 'timetable' argument is not of type "
				+ "'diary.model.Timetable'", function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = {
				"classes" : new diary.collection.ClassCollection([classModel])
			};

			// execute the command
			var command = new diary.controller.UpdateClassCommand();

			expect(function() {
				command.execute(timetable, classId, subject.get("title"));
			}).toThrow("Timetable must be of type diary.model.Timetable");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// classId argument is not numeric
		it("should fail when the 'classId' argument is not of type 'Number'",
				function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();

			expect(function() {
				command.execute(timetable, "string", subject.get("title"));
			}).toThrow("Class Identifier must be of type number");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// subjectTitle argument is not string
		it("should fail when the 'subjectTitle' argument is not of type "
				+ "'String'", function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();

			expect(function() {
				command.execute(timetable, classId, 3);
			}).toThrow("Subject Title must be of type string");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// room argument is provided and not string
		it("should fail when the 'room' argument is provided, but not of type "
				+ "'String'", function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();

			expect(function() {
				command.execute(timetable, classId, subject.get("title"), 3);
			}).toThrow("Room must be of type string");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// class is not in timetable
		it("should fail when the provided class is not in the provided "
				+ "timetable", function() {

			// setup service spies
			setupServiceSpies(null, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();

			expect(function() {
				command.execute(timetable, classId, subject.get("title"));
			}).toThrow("Class must be related to Timetable");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// TimetableManager::createSubject() error
		it("should fail when the 'TimetableManager::createSubject()' method "
				+ "fails", function() {

			// setup service spies
			setupServiceSpies(createSubjectCallbackError, null, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();
			command.execute(timetable, classId, subject.get("title"));

			// assert partial postconditions
			var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

			// postcondition: TimetableManager::createSubject() is called
			expect(service.createSubject.callCount).toEqual(1);

			// postcondition: TimetableManager::updateClassInTimetable() is not
			// called
			expect(service.updateClassInTimetable.callCount).toEqual(0);

			// postcondition: "classUpdated" event is not fired
			expect(eventSpy.callCount).toEqual(0);

			// postcondition: TimetableManager::deleteSubject() is not called
			expect(service.deleteSubject.callCount).toEqual(0);

			// postcondition: no subjects added to or removed from app model
			var appSubjects = app.model.get(ModelName.SUBJECTS);
			expect(appSubjects.length).toEqual(2);
			expect(appSubjects.get(1) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(2) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(3)).toBeUndefined();

		});

		// TimetableManager::deleteSubject() error
		it("should fail when the 'TimetableManager::deleteSubject()' method "
				+ "fails", function() {

			// setup service spies
			setupServiceSpies(null, deleteSubjectCallbackError, null);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();
			command.execute(timetable, classId, subject.get("title"));

			// assert partial postconditions
			var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

			// postcondition: TimetableManager::createSubject() is called
			expect(service.createSubject.callCount).toEqual(1);

			// postcondition: TimetableManager::updateClassInTimetable() is
			// called
			expect(service.updateClassInTimetable.callCount).toEqual(1);

			// postcondition: "classUpdated" event is fired
			expect(eventSpy.callCount).toEqual(1);

			// postcondition: TimetableManager::deleteSubject() is called
			expect(service.deleteSubject.callCount).toEqual(1);

			// postcondition: no subjects added to or removed from app model
			var appSubjects = app.model.get(ModelName.SUBJECTS);
			expect(appSubjects.length).toEqual(2);
			expect(appSubjects.get(1) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(2) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(3)).toBeUndefined();

		});

		// TimetableManager::updateClassInTimetable() error
		it("should fail when the 'TimetableManager::updateClassInTimetable()' "
				+ "method fails", function() {

			// setup service spies
			setupServiceSpies(null, null, updateClassInTimetableCallbackError);

			// setup execute arguments
			var classId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : classId,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.UpdateClassCommand();
			command.execute(timetable, classId, subject.get("title"));

			// assert partial postconditions
			var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

			// postcondition: TimetableManager::createSubject() is called
			expect(service.createSubject.callCount).toEqual(1);

			// postcondition: TimetableManager::updateClassInTimetable() is
			// called
			expect(service.updateClassInTimetable.callCount).toEqual(1);

			// postcondition: "classUpdated" event is not fired
			expect(eventSpy.callCount).toEqual(0);

			// postcondition: TimetableManager::deleteSubject() is not called
			expect(service.deleteSubject.callCount).toEqual(0);

			// postcondition: no subjects added to or removed from app model
			var appSubjects = app.model.get(ModelName.SUBJECTS);
			expect(appSubjects.length).toEqual(2);
			expect(appSubjects.get(1) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(2) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(3)).toBeUndefined();

		});

	});

});