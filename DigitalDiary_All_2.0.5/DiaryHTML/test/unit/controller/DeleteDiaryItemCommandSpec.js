/**
 * Unit Tests for the DeleteDiaryItemCommand
 * 
 * author - Justin Judd
 */
describe("DeleteDiaryItemCommand - Test Suite", function() {

	describe("Successful cases for DeleteDiaryItemCommand Suite", function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// mock manager
			var dummyManager = (function() {

				// counts calls to methods
				var calls = {
					deleteDiaryItem : 0
				};

				var service = function() {
				};

				service.prototype.deleteDiaryItem = function(diaryItem,
						successCallback, errorCallback) {

					calls.deleteDiaryItem++;
					successCallback(diaryItem);

				};

				service.prototype.getCalls = function(functionName) {
					return calls[functionName];
				};

				return service;

			})();

			// configure the application context for the unit
			// test
			app = new Rocketboots.Application({

				modelClass : diary.model.DiaryModel,

				mapEvents : function() {
					// map events needed for the test
				},

				mapServices : function() {
					// map the services needed for the test
					this.mapService(ServiceName.DIARYITEMMANAGER, dummyManager);
				}

			});

			// init the application context
			app.initialize();

		});

		it("should delete a single Diary Item", function() {

			var day = new Rocketboots.date.Day();

			var attrs = {
				id: 1,
				date: day.getDate(),
				title: "DeleteDiaryItemCommand Test Item"
			};
			var item = new diary.model.DiaryItem(attrs);
			app.model.get(ModelName.DIARYITEMS).add(item);

			// execute the command
			var command = new diary.controller.DeleteDiaryItemCommand();
			command.execute(item);
			
			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("deleteDiaryItem")).toEqual(1);
			
			// check item was removed from the app model
			var items = app.model.get(ModelName.DIARYITEMS);
			expect(items.length).toEqual(0);

		});

		it("should delete a Diary Item without affecting other Diary Items", function() {

			var day = new Rocketboots.date.Day();

			var attrs1 = {
				id: 1,
				title: "DeleteDiaryItemCommand Test Item"
			};
			var item1 = new diary.model.DiaryItem(attrs1);
			app.model.get(ModelName.DIARYITEMS).add(item1);

			day.addDays(2);
			var attrs2 = {
				id: 2,
				title: "DeleteDiaryItemCommand Test Item 2"
			};
			var item2 = new diary.model.DiaryItem(attrs2);
			app.model.get(ModelName.DIARYITEMS).add(item2);

			// execute the command
			var command = new diary.controller.DeleteDiaryItemCommand();
			command.execute(item1);
			
			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("deleteDiaryItem")).toEqual(1);
			
			// check the correct item was removed from the app model
			var items = app.model.get(ModelName.DIARYITEMS);
			expect(items.length).toEqual(1);
			expect(items.at(0).get("title")).toEqual(attrs2.title);

		});

	});

});