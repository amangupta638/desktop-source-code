/**
 * Unit Tests for the Command StartUp Command
 * 
 * author - Brian Bason
 */
describe("Start Up Command - Test Suite", function() {
	
	/**
	 * Suite of tests for proper operation of the StartUp Command
	 */
	describe("Successful cases for StartUp Command Suite" , function() {
		
		// the dummy service for the notification manager
		var dummyNotificationManager = (function() {
			
			return function() {
				
				this.generatePendingNotifications = function() { };
				
			};
				
		})();

		var dummySystemConfigurationManager = (function() {
			
			return function() {
				
				this.generatePendingNotifications = function() { };
				
			};
				
		})();
				
		var dummyRegistrationManager = (function() {
			return function() {
				this.applicationIsRegistered = function() { return true; };
			};
			
		})();

		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.NOTIFICATIONMANAGER, dummyNotificationManager);
	        	   this.mapService(ServiceName.REGISTRATIONMANAGER, dummyRegistrationManager);
	        	   this.mapService(ServiceName.SYSTEMCONFIGURATIONMANAGER, dummySystemConfigurationManager);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			app.model = {};
			app.model.get = function(modelname) {
				if (modelname == ModelName.APPLICATIONSTATE)
					return new diary.model.DiaryModel();
				else
					return {};
			};
			
			// init the application context
			app.initialize();
			
		});
		
		/*
		 * invoke the command
		 */
		it("should be able to invoke the command", function() {
		   
			// holds the command
			var command = new diary.controller.StartApplicationCommand();
			
			// execute the command
			var testFunction = function() {
				command.execute();
			};
			
			// no error should be generated
			expect(testFunction).not.toThrow();
			
		});
		
	});
		
});