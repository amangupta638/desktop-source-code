/**
 * Unit Tests for the CreateDiaryItemCommand
 * 
 * author - Justin Judd
 */
describe("CreateDiaryItemCommand - Test Suite", function() {

	describe("Successful cases for CreateDiaryItemCommand Suite", function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// mock manager
			var dummyManager = (function() {

				// counts calls to methods
				var calls = {
					createDiaryItem : 0
				};

				var service = function() {
				};

				service.prototype.createDiaryItem = function(diaryItem, attachments, successCallback, errorCallback) {

					calls.createDiaryItem++;
					successCallback(diaryItem);

				};

				service.prototype.getCalls = function(functionName) {
					return calls[functionName];
				};

				return service;

			})();

			// configure the application context for the unit
			// test
			app = new Rocketboots.Application({

				modelClass : diary.model.DiaryModel,

				mapEvents : function() {
					// map events needed for the test
				},

				mapServices : function() {
					// map the services needed for the test
					this.mapService(ServiceName.DIARYITEMMANAGER, dummyManager);
				}

			});

			// init the application context
			app.initialize();

		});

		it("should be able to invoke the command (task)", function() {

			var assignedDay = new Rocketboots.date.Day().addDays(-1),
				assignedTime = new Rocketboots.date.Time(12, 30),
				dueDay = new Rocketboots.date.Day(),
				dueTime = new Rocketboots.date.Time(13, 0),
				attrs = {
					id: 1,
					type: diary.model.DiaryItem.TYPE.HOMEWORK,
					title: "CreateDiaryItemCommand Test (Homework)",
					assignedDay: assignedDay,
					assignedTime: assignedTime,
					dueDay: dueDay,
					dueTime: dueTime,
					subjectName: "English"
				},
				item = new diary.model.Task(attrs);

			// execute the command
			var command = new diary.controller.CreateDiaryItemCommand();
			command.execute(item);

			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("createDiaryItem")).toEqual(1);

			// check item was added to app model
			var items = app.model.get(ModelName.DIARYITEMS);
			expect(items.length).toEqual(1);
			expect(items.get(1) instanceof diary.model.Task).toBeTruthy();
			expect(items.get(1).get("title")).toEqual(attrs.title);
			expect(items.get(1).get("assignedDay").getKey()).toEqual(assignedDay.getKey());
			expect(items.get(1).get("assignedTime").toInteger()).toEqual(assignedTime.toInteger());
			expect(items.get(1).get("dueDay").getKey()).toEqual(dueDay.getKey());
			expect(items.get(1).get("dueTime").toInteger()).toEqual(dueTime.toInteger());

		});

		it("should be able to invoke the command (event)", function() {

			var atDay = new Rocketboots.date.Day().addDays(-1),
				atTime = new Rocketboots.date.Time(12, 30),
				attrs = {
					id: 2,
					atDay: atDay,
					atTime: atTime,
					title: "CreateDiaryItemCommand Test (Event)"
				},
				item = new diary.model.Event(attrs);

			// execute the command
			var command = new diary.controller.CreateDiaryItemCommand();
			command.execute(item);

			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("createDiaryItem")).toEqual(1);

			// check item was added to app model
			var items = app.model.get(ModelName.DIARYITEMS);
			expect(items.length).toEqual(1);
			expect(items.get(2) instanceof diary.model.Event).toBeTruthy();
			expect(items.get(2).get("title")).toEqual(attrs.title);
			expect(items.get(2).get("atDay").getKey()).toEqual(atDay.getKey());
			expect(items.get(2).get("atTime").toInteger()).toEqual(atTime.toInteger());

		});

		it("should be able to invoke the command (note)", function() {

			var day = new Rocketboots.date.Day();

			day.addDays(-1);
			var entryAttrs = {
				id: 3,
				timestamp: day.getDate(),
				text: "CreateDiaryItemCommand Test Entry"
			};
			var entry = new diary.model.NoteEntry(entryAttrs);
			
			var atDay = new Rocketboots.date.Day().addDays(-1),
				noteAttrs = {
					id: 4,
					atDay: atDay,
					entries: new diary.collection.NoteEntryCollection([entry])
				},
				note = new diary.model.Note(noteAttrs);

			// execute the command
			var command = new diary.controller.CreateDiaryItemCommand();
			command.execute(note);

			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("createDiaryItem")).toEqual(1);

			// check item was added to app model
			var items = app.model.get(ModelName.DIARYITEMS);
			expect(items.length).toEqual(1);
			expect(items.get(4) instanceof diary.model.Note).toBeTruthy();
			expect(items.get(4).get('atDay').getKey()).toEqual(atDay.getKey());

			var entries = items.get(4).get("entries");
			expect(entries.length).toEqual(1);

			var entry = entries.get(3);
			expect(entry.get("timestamp")).toEqual(entryAttrs.timestamp);
			expect(entry.get("text")).toEqual(entryAttrs.text);

		});

		it("should be able to invoke the command twice", function() {

			var hwAttrs = {
				id: 5,
				type: diary.model.DiaryItem.TYPE.HOMEWORK,
				title: "CreateDiaryItemCommand Test Item",
				assignedDay: new Rocketboots.date.Day().addDays(-1),
				assignedTime: new Rocketboots.date.Time(10,5),
				dueDay: new Rocketboots.date.Day().addDays(1),
				dueTime: new Rocketboots.date.Time(12,30),
				subjectName: "English"
			};
			var hw = new diary.model.Task(hwAttrs);

			var entryAttrs = {
				id: 6,
				timestamp: new Rocketboots.date.Day().addDays(-1).getDate(),
				text: "CreateDiaryItemCommand Test Entry"
			};
			var entry = new diary.model.NoteEntry(entryAttrs);

			var noteAttrs = {
				id: 7,
				atDay: new Rocketboots.date.Day().addDays(-1),
				entries: new diary.collection.NoteEntryCollection([entry])
			};
			var note = new diary.model.Note(noteAttrs);

			// execute the command twice
			var command = new diary.controller.CreateDiaryItemCommand();
			command.execute(hw);
			command.execute(note);

			// check mock service calls
			var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
			expect(service.getCalls("createDiaryItem")).toEqual(2);

			// check both items were added to app model
			var items = app.model.get(ModelName.DIARYITEMS);
			expect(items.length).toEqual(2);

			expect(items.get(5) instanceof diary.model.Task).toBeTruthy();
			expect(items.get(5).get("type")).toEqual(hwAttrs.type);
			expect(items.get(5).get("title")).toEqual(hwAttrs.title);
			expect(items.get(5).get("assignedDay")).toEqual(hwAttrs.assignedDay);
			expect(items.get(5).get("assignedTime")).toEqual(hwAttrs.assignedTime);
			expect(items.get(5).get("dueDay")).toEqual(hwAttrs.dueDay);
			expect(items.get(5).get("dueTime")).toEqual(hwAttrs.dueTime);

			expect(items.get(7) instanceof diary.model.Note).toBeTruthy();
			expect(items.get(7).get("atDay").getKey()).toEqual(noteAttrs.atDay.getKey());
			expect(items.get(7).get("entries").length).toEqual(1);

		});

	});

});