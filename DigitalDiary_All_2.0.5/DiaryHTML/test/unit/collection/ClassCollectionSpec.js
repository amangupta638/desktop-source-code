/**
 * Unit Tests for the Class Collection
 * 
 * author - Jane Sivieng
 */
describe("Class Collection - Test Suite", function() {

	beforeEach(function() {
		this.addMatchers({
			toBeGreaterThanOrEqualTo: function(expected) {
				this.message = function () {
					return "Expected " + actual + " to be greater than or equal to " + expected;
				};
				return this.actual >= expected;
			}
		});
	});
	
	
	/*
	 * class entries should be in cycleday then period order
	 */
	it("class entries should be in cycleday then period order", function() {
		
		// create the class collection 
		var entries = new diary.collection.ClassCollection();
	
		entries.add(new diary.model.Class({'cycleDay' : 3 , 'periodId' : 1 }));
		entries.add(new diary.model.Class({'cycleDay' : 3 , 'periodId' : 3 }));
		entries.add(new diary.model.Class({'cycleDay' : 3 , 'periodId' : 2 }));
		entries.add(new diary.model.Class({'cycleDay' : 2 , 'periodId' : 3 }));
		entries.add(new diary.model.Class({'cycleDay' : 2 , 'periodId' : 1 }));
		entries.add(new diary.model.Class({'cycleDay' : 2 , 'periodId' : 2 }));
		entries.add(new diary.model.Class({'cycleDay' : 1 , 'periodId' : 1 }));
		entries.add(new diary.model.Class({'cycleDay' : 1 , 'periodId' : 2 }));
		entries.add(new diary.model.Class({'cycleDay' : 1 , 'periodId' : -1 }));

		
		// Verify ordered correctly
		var previousCycleDay = null;
		var previousPeriodId = null;
		_.forEach(entries.toArray(), function (classObj) {
			// Ensure correct ordering
			if (previousCycleDay != null && previousPeriodId != null) {
				expect(classObj.get('cycleDay')).toBeGreaterThanOrEqualTo(previousCycleDay); 
				if (classObj.get('cycleDay') == previousCycleDay) {					
					expect(classObj.get('periodId')).toBeGreaterThanOrEqualTo(previousPeriodId); 
				}
			}
			
			// Loop
			previousCycleDay = classObj.get('cycleDay');
			previousPeriodId = classObj.get('periodId');
		});
		
		// Post-checks
		expect(previousCycleDay).not.toBeNull();
		expect(previousPeriodId).not.toBeNull();
		
	});
});