/**
 * @author John Pearce
 */
describe("Timetable Collection", function() {

	var timetableCollection = null;

	beforeEach(function () {
		timetableCollection = new diary.collection.TimetableCollection();

		timetableCollection.add(new diary.model.Timetable({
			'name' : "Summer School A",
			'startDay' : new Rocketboots.date.Day(2013, 0, 7),
			'cycleLength' : 5
		}));

		timetableCollection.add(new diary.model.Timetable({
			'name' : "Semester 1",
			'startDay' : new Rocketboots.date.Day(2013, 1, 21),
			'cycleLength' : 10
		}));

		timetableCollection.add(new diary.model.Timetable({
			'name' : "Semester 2",
			'startDay' : new Rocketboots.date.Day(2013, 6, 1),
			'cycleLength' : 10
		}));

		timetableCollection.add(new diary.model.Timetable({
			'name' : "Summer School B",
			'startDay' : new Rocketboots.date.Day(2013, 10, 19),
			'cycleLength' : 5
		}));
	});

	it("should be ordered by start date", function() {

		var unordered = new diary.collection.TimetableCollection();
		unordered.add(timetableCollection.at(2));
		unordered.add(timetableCollection.at(1));
		unordered.add(timetableCollection.at(3));
		unordered.add(timetableCollection.at(0));

		expect(unordered.at(0)).toEqual(timetableCollection.at(0));
		expect(unordered.at(1)).toEqual(timetableCollection.at(1));
		expect(unordered.at(2)).toEqual(timetableCollection.at(2));
		expect(unordered.at(3)).toEqual(timetableCollection.at(3));

		expect(unordered.at(0).get('name')).toEqual("Summer School A");
		expect(unordered.at(1).get('name')).toEqual("Semester 1");
		expect(unordered.at(2).get('name')).toEqual("Semester 2");
		expect(unordered.at(3).get('name')).toEqual("Summer School B");

	});

	it("should be able to determine current timetable", function () {

		// Before any timetables have started for the year
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 0, 1))).toBeNull("Start of year");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 0, 2))).toBeNull("Year started");

		// Summer A (including boundary cases)
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 0, 7))).toEqual(timetableCollection.at(0), "Start of Summer A");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 1, 7))).toEqual(timetableCollection.at(0), "Mid- Summer A");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 1, 20))).toEqual(timetableCollection.at(0), "End of Summer A");

		// Semester 1 (including boundary cases)
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 1, 21))).toEqual(timetableCollection.at(1), "Start of Semester 1");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 5, 1))).toEqual(timetableCollection.at(1), "Mid- Semester 1");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 5, 30))).toEqual(timetableCollection.at(1), "End of Semester 1");

		// Semester 2 (including boundary cases)
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 6, 1))).toEqual(timetableCollection.at(2), "Start of Semester 2");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 7, 30))).toEqual(timetableCollection.at(2), "Mid- Semester 2");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 10, 18))).toEqual(timetableCollection.at(2), "End of Semester 2");

		// Summer B (including boundary case - end of year)
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 10, 19))).toEqual(timetableCollection.at(3), "Start of Summer B");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 11, 19))).toEqual(timetableCollection.at(3), "Mid- Summer B");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2013, 11, 31))).toEqual(timetableCollection.at(3), "End of year");

		// Dates outside of the timetables year should return null
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2012, 11, 31))).toBeNull("Dates outside of the timetables year should return null");
		expect(timetableCollection.getCurrentAt(new Rocketboots.date.Day(2014, 0, 1))).toBeNull("Dates outside of the timetables year should return null");

	});

});