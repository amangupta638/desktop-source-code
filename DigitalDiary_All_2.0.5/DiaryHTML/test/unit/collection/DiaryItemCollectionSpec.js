/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: DiaryItemCollectionSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Diary Item Collection", function() {

	var collection = null,					// test collection
		models = {							// test models
			// Events
			pastAllDayEvent: new diary.model.Event({
				id: 11,
				title: "Past All-Day Event",
				startDay: new Rocketboots.date.Day().addDays(-5),
				locked: false
			}),
			todayAllDayEvent: new diary.model.Event({
				id: 12,
				title: "Today All-Day Event",
				startDay: new Rocketboots.date.Day(),
				locked: true
			}),
			futureAllDayEvent: new diary.model.Event({
				id: 13,
				title: "Future All-Day Event",
				startDay: new Rocketboots.date.Day().addDays(3),
				locked: false
			}),
			pastPeriodEvent: new diary.model.Event({
				id: 14,
				title: "Past Period Event",
				startDay: new Rocketboots.date.Day().addDays(-1),
				startTime: new Rocketboots.date.Time(10, 20),
				endTime: new Rocketboots.date.Time(11, 20),
				locked: true
			}),
			todayPeriodEvent: new diary.model.Event({
				id: 15,
				title: "Today Period Event",
				startDay: new Rocketboots.date.Day(),
				startTime: new Rocketboots.date.Time(9, 30),
				endTime: new Rocketboots.date.Time(15, 30),
				locked: false
			}),
			futurePeriodEvent: new diary.model.Event({
				id: 16,
				title: "Future Period Event",
				startDay: new Rocketboots.date.Day().addDays(10),
				startTime: new Rocketboots.date.Time(8, 0),
				endTime: new Rocketboots.date.Time(8, 5),
				locked: true
			}),

			// Messages
			pastAllDayMessage: new diary.model.Message({
				id: 21,
				title: "Past All-Day Message",
				atDay: new Rocketboots.date.Day().addDays(-1),
				responseRequired: false
			}),
			todayAllDayMessage: new diary.model.Message({
				id: 22,
				title: "Today All-Day Message",
				atDay: new Rocketboots.date.Day(),
				responseRequired: true
			}),
			futureAllDayMessage: new diary.model.Message({
				id: 23,
				title: "Future All-Day Message",
				atDay: new Rocketboots.date.Day().addDays(8),
				responseRequired: false
			}),
			pastPeriodMessage: new diary.model.Message({
				id: 24,
				title: "Past Period Message",
				atDay: new Rocketboots.date.Day().addDays(-3),
				atTime: new Rocketboots.date.Time(11, 22),
				responseRequired: true
			}),
			todayPeriodMessage: new diary.model.Message({
				id: 25,
				title: "Today Period Message",
				atDay: new Rocketboots.date.Day(),
				atTime: new Rocketboots.date.Time(20, 0),
				responseRequired: false
			}),
			futurePeriodMessage: new diary.model.Message({
				id: 26,
				title: "Future Period Message",
				atDay: new Rocketboots.date.Day().addDays(1),
				atTime: new Rocketboots.date.Time(14, 2),
				responseRequired: true
			}),

			// Notes
			pastAllDayNote: new diary.model.Note({
				id: 31,
				title: "Past All-Day Note",
				atDay: new Rocketboots.date.Day().addDays(-4)
			}),
			todayAllDayNote: new diary.model.Note({
				id: 32,
				title: "Today All-Day Note",
				atDay: new Rocketboots.date.Day()
			}),
			futureAllDayNote: new diary.model.Note({
				id: 33,
				title: "Future All-Day Note",
				atDay: new Rocketboots.date.Day().addDays(3)
			}),
			pastPeriodNote: new diary.model.Note({
				id: 34,
				title: "Past Period Note",
				atDay: new Rocketboots.date.Day().addDays(-3),
				atTime: new Rocketboots.date.Time(11, 5)
			}),
			todayPeriodNote: new diary.model.Note({
				id: 35,
				title: "Today Period Note",
				atDay: new Rocketboots.date.Day(),
				atTime: new Rocketboots.date.Time(9, 30)
			}),
			futurePeriodNote: new diary.model.Note({
				id: 36,
				title: "Future Period Note",
				atDay: new Rocketboots.date.Day().addDays(4),
				atTime: new Rocketboots.date.Time(12, 10)
			}),

			// Tasks
			pastAllDayTask: new diary.model.Task({
				id: 41,
				type: diary.model.DiaryItem.TYPE.HOMEWORK,
				title: "Past All-Day Homework",
				assignedDay: new Rocketboots.date.Day().addDays(-6),
				assignedTime: new Rocketboots.date.Time(13, 15),
				dueDay: new Rocketboots.date.Day().addDays(-4),
				completed: null
			}),
			todayAllDayTask: new diary.model.Task({
				id: 42,
				type: diary.model.DiaryItem.TYPE.HOMEWORK,
				title: "Today All-Day Homework",
				assignedDay: new Rocketboots.date.Day().addDays(-1),
				assignedTime: new Rocketboots.date.Time(13, 15),
				dueDay: new Rocketboots.date.Day(),
				completed: null
			}),
			futureAllDayTask: new diary.model.Task({
				id: 43,
				type: diary.model.DiaryItem.TYPE.ASSIGNMENT,
				title: "Future All-Day Assignment",
				assignedDay: new Rocketboots.date.Day(),
				assignedTime: new Rocketboots.date.Time(15, 25),
				dueDay: new Rocketboots.date.Day().addDays(3),
				completed: new Date()
			}),
			pastPeriodTask: new diary.model.Task({
				id: 44,
				type: diary.model.DiaryItem.TYPE.HOMEWORK,
				title: "Past Period Homework",
				assignedDay: new Rocketboots.date.Day().addDays(-5),
				assignedTime: new Rocketboots.date.Time(0, 0),
				dueDay: new Rocketboots.date.Day().addDays(-3),
				dueTime: new Rocketboots.date.Time(11, 5),
				completed: null
			}),
			todayPeriodTask: new diary.model.Task({
				id: 45,
				type: diary.model.DiaryItem.TYPE.ASSIGNMENT,
				title: "Today Period Assignment",
				assignedDay: new Rocketboots.date.Day().addDays(-2),
				assignedTime: new Rocketboots.date.Time(1, 55),
				dueDay: new Rocketboots.date.Day(),
				dueTime: new Rocketboots.date.Time(9, 30),
				completed: new Date()
			}),
			futurePeriodTask: new diary.model.Task({
				id: 46,
				type: diary.model.DiaryItem.TYPE.ASSIGNMENT,
				title: "Future Period Assignment",
				assignedDay: new Rocketboots.date.Day(),
				assignedTime: new Rocketboots.date.Time(8, 44),
				dueDay: new Rocketboots.date.Day().addDays(4),
				dueTime: new Rocketboots.date.Time(12, 10),
				completed: null
			})
		},
		earliestItem = new Rocketboots.date.Day().addDays(-10),
		latestItem = new Rocketboots.date.Day().addDays(10),
		earliestTaskAssigned = new Rocketboots.date.Day().addDays(-6),
		latestTaskAssigned = new Rocketboots.date.Day(),
		earliestTaskDue = new Rocketboots.date.Day().addDays(-4),
		latestTaskDue = new Rocketboots.date.Day().addDays(4);

	describe("when created with no items", function() {

		beforeEach(function() {
			collection = new diary.collection.DiaryItemCollection();
		});

		describe("when getTasksAssignedInDateRange() is called with any date range", function() {
			var results = null;

			beforeEach(function() {
				results = collection.getTasksAssignedInDateRange(earliestItem, latestItem);
			});

			it("should return an empty collection", function() {
				expect(results.length).toEqual(0);
			});
		});

		describe("when getTasksDueInDateRange() is called with any date range", function() {
			var results = null,
				startTime = new Rocketboots.date.Time(12, 0);

			beforeEach(function() {
				results = collection.getTasksDueInDateRange(earliestItem, startTime, latestItem);
			});

			it("should return an empty collection", function() {
				expect(results.length).toEqual(0);
			});
		});

		describe("when getNonOverdueTasks() is called", function() {
			var results = null;

			beforeEach(function() {
				results = collection.getNonOverdueTasks();
			});

			it("should return an empty collection", function() {
				expect(results.length).toEqual(0);
			});
		});

		describe("when getOverdueTasks() is called", function() {
			var results = null;

			beforeEach(function() {
				results = collection.getOverdueTasks();
			});

			it("should return an empty collection", function() {
				expect(results.length).toEqual(0);
			});
		});

	});

	describe("when created with non-Tasks", function() {

		beforeEach(function() {
			collection = new diary.collection.DiaryItemCollection([
				models.pastAllDayEvent,
				models.todayAllDayEvent,
				models.futureAllDayEvent,
				models.pastPeriodEvent,
				models.todayPeriodEvent,
				models.futurePeriodEvent,
				models.pastAllDayMessage,
				models.todayAllDayMessage,
				models.futureAllDayMessage,
				models.pastPeriodMessage,
				models.todayPeriodMessage,
				models.futurePeriodMessage,
				models.pastAllDayNote,
				models.todayAllDayNote,
				models.futureAllDayNote,
				models.pastPeriodNote,
				models.todayPeriodNote,
				models.futurePeriodNote
			]);
		});

		describe("when getTasksAssignedInDateRange() is called with a date range containing all items", function() {
			var results = null;

			beforeEach(function() {
				results = collection.getTasksAssignedInDateRange(earliestItem, latestItem);
			});

			it("should return an empty collection", function() {
				expect(results.length).toEqual(0);
			});
		});

		describe("when getTasksDueInDateRange() is called with a date range containing all items", function() {
			var results = null,
				startTime = new Rocketboots.date.Time(12, 0);

			beforeEach(function() {
				results = collection.getTasksDueInDateRange(earliestItem, startTime, latestItem);
			});

			it("should return an empty collection", function() {
				expect(results.length).toEqual(0);
			});
		});

		describe("when getNonOverdueTasks() is called", function() {
			var results = null;

			beforeEach(function() {
				results = collection.getNonOverdueTasks();
			});

			it("should return an empty collection", function() {
				expect(results.length).toEqual(0);
			});
		});

		describe("when getOverdueTasks() is called", function() {
			var results = null;

			beforeEach(function() {
				results = collection.getOverdueTasks();
			});

			it("should return an empty collection", function() {
				expect(results.length).toEqual(0);
			});
		});

	});

	describe("when created with a mix of Tasks and non-Tasks", function() {

		beforeEach(function() {
			collection = new diary.collection.DiaryItemCollection([
				models.pastAllDayEvent,
				models.todayAllDayEvent,
				models.futureAllDayEvent,
				models.pastPeriodEvent,
				models.todayPeriodEvent,
				models.futurePeriodEvent,
				models.pastAllDayMessage,
				models.todayAllDayMessage,
				models.futureAllDayMessage,
				models.pastPeriodMessage,
				models.todayPeriodMessage,
				models.futurePeriodMessage,
				models.pastAllDayNote,
				models.todayAllDayNote,
				models.futureAllDayNote,
				models.pastPeriodNote,
				models.todayPeriodNote,
				models.futurePeriodNote,
				models.pastAllDayTask,
				models.todayAllDayTask,
				models.futureAllDayTask,
				models.pastPeriodTask,
				models.todayPeriodTask,
				models.futurePeriodTask
			]);
		});

		describe("when getTasksAssignedInDateRange() is called with a date range containing all items", function() {
			var results = null;

			beforeEach(function() {
				results = collection.getTasksAssignedInDateRange(earliestItem, latestItem);
			});

			it("should return all incomplete Tasks", function() {
				expect(results.length).toEqual(4);
				expect(results.get(models.pastAllDayTask)).toBeDefined();
				expect(results.get(models.todayAllDayTask)).toBeDefined();
				expect(results.get(models.futureAllDayTask)).toBeUndefined();
				expect(results.get(models.pastPeriodTask)).toBeDefined();
				expect(results.get(models.todayPeriodTask)).toBeUndefined();
				expect(results.get(models.futurePeriodTask)).toBeDefined();
			});
		});

		describe("when getTasksDueInDateRange() is called with a date range containing all items", function() {
			var results = null,
				startTime = new Rocketboots.date.Time(12, 0);

			beforeEach(function() {
				results = collection.getTasksDueInDateRange(earliestItem, startTime, latestItem);
			});

			it("should return all incomplete Tasks", function() {
				expect(results.length).toEqual(4);
				expect(results.get(models.pastAllDayTask)).toBeDefined();
				expect(results.get(models.todayAllDayTask)).toBeDefined();
				expect(results.get(models.futureAllDayTask)).toBeUndefined();
				expect(results.get(models.pastPeriodTask)).toBeDefined();
				expect(results.get(models.todayPeriodTask)).toBeUndefined();
				expect(results.get(models.futurePeriodTask)).toBeDefined();
			});
		});

		describe("when getTasksAssignedInDateRange() is called with a date range containing no Tasks", function() {
			var results = null;

			beforeEach(function() {
				var fromDay = new Rocketboots.date.Day(earliestTaskAssigned).addDays(-5);
				var toDay = new Rocketboots.date.Day(earliestTaskAssigned).addDays(-2);
				results = collection.getTasksAssignedInDateRange(fromDay, toDay);
			});

			it("should return no items", function() {
				expect(results.length).toEqual(0);
			});
		});

		describe("when getTasksDueInDateRange() is called with a date range containing no Tasks", function() {
			var results = null,
				startTime = new Rocketboots.date.Time(12, 0);

			beforeEach(function() {
				var fromDay = new Rocketboots.date.Day(earliestTaskDue).addDays(-5);
				var toDay = new Rocketboots.date.Day(earliestTaskDue).addDays(-2);
				results = collection.getTasksDueInDateRange(fromDay, startTime, toDay);
			});

			it("should return no items", function() {
				expect(results.length).toEqual(0);
			});
		});

		describe("when getTasksAssignedInDateRange() is called with a date range containing some Tasks", function() {
			var results = null;

			beforeEach(function() {
				var fromDay = new Rocketboots.date.Day(earliestTaskAssigned).addDays(-2);
				var toDay = new Rocketboots.date.Day(latestTaskAssigned).addDays(-2);
				results = collection.getTasksAssignedInDateRange(fromDay, toDay);
			});

			it("should return some items", function() {
				expect(results.length).toEqual(2);
				expect(results.get(models.pastAllDayTask)).toBeDefined();
				expect(results.get(models.todayAllDayTask)).toBeUndefined();
				expect(results.get(models.futureAllDayTask)).toBeUndefined();	// completed
				expect(results.get(models.pastPeriodTask)).toBeDefined();
				expect(results.get(models.todayPeriodTask)).toBeUndefined();	// completed
				expect(results.get(models.futurePeriodTask)).toBeUndefined();	// assigned date out of range
			});
		});

		describe("when getTasksDueInDateRange() is called with a date range containing some Tasks", function() {
			var results = null,
				startTime = new Rocketboots.date.Time(12, 0);

			beforeEach(function() {
				var fromDay = new Rocketboots.date.Day(earliestTaskDue).addDays(-2);
				var toDay = new Rocketboots.date.Day(latestTaskDue).addDays(-2);
				results = collection.getTasksDueInDateRange(fromDay, startTime, toDay);
			});

			it("should return some items", function() {
				expect(results.length).toEqual(3);
				expect(results.get(models.pastAllDayTask)).toBeDefined();
				expect(results.get(models.todayAllDayTask)).toBeDefined();
				expect(results.get(models.futureAllDayTask)).toBeUndefined();	// completed
				expect(results.get(models.pastPeriodTask)).toBeDefined();
				expect(results.get(models.todayPeriodTask)).toBeUndefined();	// completed
				expect(results.get(models.futurePeriodTask)).toBeUndefined();	// due date out of range
			});
		});

		describe("when getNonOverdueTasks() is called", function() {
			var results = null;

			beforeEach(function() {
				results = collection.getNonOverdueTasks();
			});

			it("should return some items", function() {
				expect(results.length).toEqual(1);
				expect(results.get(models.pastAllDayTask)).toBeUndefined();		// overdue
				expect(results.get(models.todayAllDayTask)).toBeUndefined();	// overdue
				expect(results.get(models.futureAllDayTask)).toBeUndefined();	// completed
				expect(results.get(models.pastPeriodTask)).toBeUndefined();		// overdue
				expect(results.get(models.todayPeriodTask)).toBeUndefined();	// completed
				expect(results.get(models.futurePeriodTask)).toBeDefined();
			});
		});

		describe("when getOverdueTasks() is called", function() {
			var results = null;

			beforeEach(function() {
				results = collection.getOverdueTasks();
			});

			it("should return some items", function() {
				expect(results.length).toEqual(3);
				expect(results.get(models.pastAllDayTask)).toBeDefined();
				expect(results.get(models.todayAllDayTask)).toBeDefined();
				expect(results.get(models.futureAllDayTask)).toBeUndefined();	// completed
				expect(results.get(models.pastPeriodTask)).toBeDefined();
				expect(results.get(models.todayPeriodTask)).toBeUndefined();	// completed
				expect(results.get(models.futurePeriodTask)).toBeUndefined();	// not overdue
			});
		});

	});

});
