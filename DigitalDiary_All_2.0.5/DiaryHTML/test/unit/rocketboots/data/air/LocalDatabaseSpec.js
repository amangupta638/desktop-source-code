/**
 * $Id: LocalDatabaseSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
describe("Rocketboots.data.air.LocalDatabase - Test Suite", function() {

	beforeEach(function() {
		// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
		air = {
			SQLEvent: {
				OPEN: 'open',
				RESULT: 'result'
			},
			SQLErrorEvent: {
				ERROR: 'error'
			}
		};

		air.SQLConnection = function() {
			this.listeners = {};
		};
	
		air.SQLConnection.prototype.addEventListener = function(type, callback) {
			this.listeners[type] = callback;
		};
		
		air.SQLConnection.prototype.removeEventListener = function(type, callback) {
			delete this.listeners[type];
		};

		air.SQLStatement = function() {
			this.listeners = {};
		};	
	
		air.SQLStatement.prototype.addEventListener = function(type, callback) {
			this.listeners[type] = callback;
		};
		
		air.SQLStatement.prototype.removeEventListener = function(type, callback) {
			delete this.listeners[type];
		};
	});
	
	/**
	 * Suite of tests which should create a notification
	 */
	describe("Successful creation of LocalDatabase" , function() {
		
		beforeEach(function() {
			
		});
		
		
		/*
		 * creation of a notification with default values
		 */
		it("should be able to create a LocalDatabase", function() {
		   
			// create the test notification that we will use for the test
			var localDatabase = new Rocketboots.data.air.LocalDatabase();
			
			expect(localDatabase).toNotBe(null);			
		});
	
	});
	
	describe("Retrieve a database connection", function() {
	
		beforeEach(function() {
			
			air.SQLConnection.prototype.openAsync = function(dbFile) {
				this.listeners[air.SQLEvent.OPEN].apply(this, [this]);
			};
				
		});
		
		it("should retrieve a Connection for a valid file", function() {

			var dbFile = {
					nativePath: 'aValidFile'
				},
				localDatabase = new Rocketboots.data.air.LocalDatabase(),
				successCalled = false;
			
			function success(connection) {
				expect(connection).toNotBe(null);
				successCalled = true;
			}
			
			function error() {
				throw 'should not be an error';
			}

			localDatabase.connection(dbFile, success, error);
			
			waitsFor(function() {
				return successCalled;
			});
				
		});
				
	});

	describe("Don't retrieve an invalid database connection", function() {

		beforeEach(function() {
			
			air.SQLConnection.prototype.openAsync = function(dbFile) {
				this.listeners[air.SQLErrorEvent.ERROR].apply(this);
			};
				
		});

		it("should not retrieve a Connection for an invalid file", function() {

			var invalidDbFile = {
					nativePath: 'notAValidFile'
				},
				localDatabase = new Rocketboots.data.air.LocalDatabase(),
				errorCalled = false;
			
			function success(connection) {
				throw 'should not be successful';
			}
			
			function error() {
				expect(true).toBe(true);
				errorCalled = true;
			}

			localDatabase.connection(invalidDbFile, success, error);
			
			waitsFor(function() {
				return errorCalled;
			});
				
		});
				
	});

	describe("Execute a SQL statement async", function() {
	
		beforeEach(function() {
			
			air.SQLStatement.prototype.execute = function(dbFile) {
				this.listeners[air.SQLEvent.RESULT].apply(this, [this]);
			};
			
		});
		
		it("should execute a valid SQL statement async", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
				connection = {},
				successCalled = false;
			
			function success(connection) {
				expect(connection).toNotBe(null);
				successCalled = true;
			}
			
			function error() {
				throw 'should not be an error';
			}

			localDatabase.runQuery("SELECT * FROM TEST", {}, connection, success, error);
			
			waitsFor(function() {
				return successCalled;
			});
				
		});
				
	});
	
	describe("Execute a SQL statement sync", function() {
		
		beforeEach(function() {
			
			air.SQLStatement.prototype.execute = function() {
			};
			
			air.SQLStatement.prototype.getResult = function() {
				// return a collection of test data
				return {'data': [{ 'uid' : "123456", 'name' : "Test" }]};
			};
			
		});
		
		it("should execute a valid SQL statement sync", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
				connection = {};
			
			var data = localDatabase.runQuery("SELECT * FROM TEST", {}, connection).getResult().data;
			
			expect(data.length).toBeGreaterThan(0);
		});
				
	});

	describe("Don't execute an invalid SQL statement async", function() {

		beforeEach(function() {
			
			air.SQLStatement.prototype.execute = function(dbFile) {
				this.listeners[air.SQLErrorEvent.ERROR].apply(this);
			};
				
		});

		it("should fail when attempting to execute an invalid SQL statement async", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
				connection = {},
				errorCalled = false;
			
			function success(connection) {
				throw 'should not be successful';
			}
			
			function error() {
				expect(true).toBe(true);
				errorCalled = true;
			}

			localDatabase.runQuery("INVALID SQL STATEMENT", {}, connection, success, error);
			
			waitsFor(function() {
				return errorCalled;
			});
				
		});
				
	});
	
	describe("Don't execute an invalid SQL statement sync", function() {

		beforeEach(function() {
			
			air.SQLStatement.prototype.execute = function() {
				throw new Error("Invalid SQL statement");
			};
			
			air.SQLStatement.prototype.getResult = function() {
			};
				
		});

		it("should fail when attempting to execute an invalid SQL statement sync", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
			    connection = {};
			
			var errorFunction = function() {
				localDatabase.runQuery("INVALID SQL STATEMENT", {}, connection);
			};
			
			expect(errorFunction).toThrow("Invalid SQL statement");
		});
				
	});
	
	describe("Run a series of SQL statements async", function() {

		beforeEach(function() {
			
			air.SQLStatement.prototype.execute = function(dbFile) {
				if (this.text == "VALID SQL") {
					this.listeners[air.SQLEvent.RESULT].apply(this, [this]);
				}
				else if (this.text == "INVALID SQL") {
					this.listeners[air.SQLErrorEvent.ERROR].apply(this);
				}
				else {
					throw "unsupported test sql";
				}
			};
				
		});
			
		it("should execute all statements and complete successfully", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
				connection = {},
				successCalled = false;
			
			function success(connection) {
				expect(true).toBe(true);
				successCalled = true;
			}
			
			function error() {
				throw 'should not be an error';
			}

			localDatabase.runScripts([
				{
					sql: "VALID SQL"
				},
				{
					sql: "VALID SQL"
				},
				{
					sql: "VALID SQL"
				}				
			], connection, success, error);
			
			waitsFor(function() {
				return successCalled;
			});
					
		});
		
		it("should execute some statements, error, rollback and indicate error with successful rollback", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
				connection = {},
				errorCalled = false;
			
			function success(connection) {
				throw 'should not be an error';
			}
			
			function error(rollbackSuccessful) {
				expect(rollbackSuccessful).toBe(true);
				errorCalled = true;
			}

			localDatabase.runScripts([
				{
					sql: "VALID SQL",
					rollback: "VALID SQL"
				},
				{
					sql: "VALID SQL",
					rollback: "VALID SQL"
				},
				{
					sql: "INVALID SQL"
				}				
			], connection, success, error);
			
			waitsFor(function() {
				return errorCalled;
			});
						
		});
		
		it("should execute some statements, error, partially rollback and indicate error with unsuccessful rollback", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
				connection = {},
				errorCalled = false;
			
			function success(connection) {
				throw 'should not be an error';
			}
			
			function error(rollbackSuccessful) {
				expect(rollbackSuccessful).toBe(false);
				errorCalled = true;
			}

			localDatabase.runScripts([
				{
					sql: "VALID SQL",
					rollback: "INVALID SQL"
				},
				{
					sql: "VALID SQL",
					rollback: "VALID SQL"
				},
				{
					sql: "INVALID SQL"
				}				
			], connection, success, error);
			
			waitsFor(function() {
				return errorCalled;
			});
						
		
		});
		
	});
	
	describe("Run a series of SQL statements sync", function() {

		beforeEach(function() {
			
			air.SQLStatement.prototype.execute = function(dbFile) {
				if (this.text == "VALID SQL") {
					
				}
				else if (this.text == "INVALID SQL") {
					throw new Error("Invalid SQL Statement");
				}
				else if (this.text == "VALID ROLLBACK SQL") {
					
				}
				else if (this.text == "INVALID ROLLBACK SQL") {
					throw new Error("Invalid ROLLBACK SQL Statement");
				}
				else {
					throw "unsupported test sql";
				}
			};
				
		});
			
		it("should execute all statements and complete successfully", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
				connection = {};
			
			function success(connection) {
				expect(true).toBe(true);
				successCalled = true;
			}
			
			function error() {
				throw 'should not be an error';
			}

			var testFunction = function() {
				localDatabase.runScripts([
				          				{
				          					sql: "VALID SQL"
				          				},
				          				{
				          					sql: "VALID SQL"
				          				},
				          				{
				          					sql: "VALID SQL"
				          				}				
				          			], connection);
			};
			
			expect(testFunction).not.toThrow("Invalid SQL Statement");
					
		});
		
		it("should execute some statements, error, rollback and indicate error with successful rollback", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
				connection = {};
			
			var testFunction = function() {
				localDatabase.runScripts([
					{
						sql: "VALID SQL",
						rollback: "VALID ROLLBACK SQL"
					},
					{
						sql: "VALID SQL",
						rollback: "VALID ROLLBACK SQL"
					},
					{
						sql: "INVALID SQL"
					}				
				], connection);
			};
			
			expect(testFunction).toThrow("Invalid SQL Statement");
			
		});
		
		it("should execute some statements, error, partially rollback and indicate error with unsuccessful rollback", function() {

			var localDatabase = new Rocketboots.data.air.LocalDatabase(),
				connection = {};
			
			var testFunction = function() {
				localDatabase.runScripts([
					{
						sql: "VALID SQL",
						rollback: "INVALID ROLLBACK SQL"
					},
					{
						sql: "VALID SQL",
						rollback: "VALID SQL"
					},
					{
						sql: "INVALID SQL"
					}				
				], connection);
			};
			
			expect(testFunction).toThrow("Invalid SQL Statement");
			
		});
		
	});
	
});