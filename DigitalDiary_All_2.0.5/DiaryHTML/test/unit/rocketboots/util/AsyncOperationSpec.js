/**
 * $Id: AsyncOperationSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
describe("RocketBoots AsyncOperation Utility", function() {

	var defaultContext = window;

	beforeEach(function() {
	});

	it("should be able to construct with function", function() {

		var opExec = function () {
			// default scope
			expect(this).toEqual(defaultContext);
			// no args
			expect(arguments.length).toEqual(2);

			// call success
			arguments[0]();
		};

		var opSuccess = jasmine.createSpy();

		var opError = function () {
			throw new Error("Should not have got error");
		};

		var op = new Rocketboots.util.AsyncOperation(opExec, opSuccess, opError);

		op.run();
		expect(op.isRunning()).toBeFalsy();
		expect(op.success()).toBeTruthy();
		expect(op.getSettings().exec).toEqual(opExec);
		expect(op.getSettings().context).toEqual(defaultContext);
		expect(op.getSettings().args).toEqual([]);
		expect(op.getResults()).toEqual([]);

		expect(opSuccess).toHaveBeenCalledWith(op, []);
	});

	it("should be able to construct with object", function () {

		var opObj = {
			exec : function (arg1, arg2, success, error) {
				// default scope
				expect(this).toEqual(opObj.context);
				// no args
				expect(arguments.length).toEqual(4);

				expect(arg1).toEqual("arg1");
				expect(arg2).toEqual("arg2");
				expect(_.isFunction(success)).toBeTruthy();
				expect(_.isFunction(error)).toBeTruthy();

				// call success
				success();
			},
			context : { "random" : "object" },
			args : ["arg1", "arg2"]
		};

		var opSuccess = jasmine.createSpy();

		var opError = function () {
			throw new Error("Should not have got error");
		};

		var op = new Rocketboots.util.AsyncOperation(opObj, opSuccess, opError);

		op.run();
		expect(op.isRunning()).toBeFalsy();
		expect(op.success()).toBeTruthy();
		expect(op.getSettings().exec).toEqual(opObj.exec);
		expect(op.getSettings().context).toEqual(opObj.context);
		expect(op.getSettings().args).toEqual(opObj.args);
		expect(op.getResults()).toEqual([]);

		expect(opSuccess).toHaveBeenCalledWith(op, []);
	});

	it("should be able to construct with custom options", function () {
		var opObj = {
			exec : function (arg1, arg2, success, error) {
				// default scope
				expect(this).toEqual(opObj.context);
				// no args
				expect(arguments.length).toEqual(4);

				expect(arg1).toEqual("arg1");
				expect(arg2).toEqual("arg2");
				expect(_.isFunction(success)).toBeTruthy();
				expect(_.isFunction(error)).toBeTruthy();

				// call success
				success();
			},
			context : { "random" : "object" },
			args : ["arg1", "arg2"],
			custom1 : "abc",
			custom2 : 123
		};

		var opSuccess = jasmine.createSpy();

		var opError = function () {
			throw new Error("Should not have got error");
		};

		var op = new Rocketboots.util.AsyncOperation(opObj, opSuccess, opError);

		op.run();
		expect(op.isRunning()).toBeFalsy();
		expect(op.success()).toBeTruthy();
		expect(op.getSettings().exec).toEqual(opObj.exec);
		expect(op.getSettings().context).toEqual(opObj.context);
		expect(op.getSettings().args).toEqual(opObj.args);
		expect(op.getSettings().custom1).toEqual("abc");
		expect(op.getSettings().custom2).toEqual(123);
		expect(op.getResults()).toEqual([]);

		expect(opSuccess).toHaveBeenCalledWith(op, []);
	});

	it("should be able to construct with arguments as a function", function () {
		var opObj = {
			exec : function (arg1, arg2, success, error) {
				// default scope
				expect(this).toEqual(opObj.context);
				// no args
				expect(arguments.length).toEqual(4);

				expect(arg1).toEqual("arg1");
				expect(arg2).toEqual("arg2");
				expect(_.isFunction(success)).toBeTruthy();
				expect(_.isFunction(error)).toBeTruthy();

				// call success
				success();
			},
			context : { "random" : "object" },
			args : function () {
				expect(this).toEqual(opObj.context);
				return ["arg1", "arg2"];
			}
		};

		spyOn(opObj, "args").andCallThrough();

		var opSuccess = jasmine.createSpy();

		var opError = function () {
			throw new Error("Should not have got error");
		};

		var op = new Rocketboots.util.AsyncOperation(opObj, opSuccess, opError);

		// The arguments definition function should not be called until the operation is run
		expect(opObj.args).not.toHaveBeenCalled();

		op.run();
		expect(op.isRunning()).toBeFalsy();
		expect(op.success()).toBeTruthy();
		expect(op.getSettings().exec).toEqual(opObj.exec);
		expect(op.getSettings().context).toEqual(opObj.context);
		expect(op.getSettings().args).toEqual(opObj.args);
		expect(op.getResults()).toEqual([]);
		expect(opObj.args).toHaveBeenCalledWith();

		expect(opSuccess).toHaveBeenCalledWith(op, []);
	});

	it("should throw if argument function does not return an array", function () {
		var opObj = {
			exec : function (success, error) {
				throw new Error("Should not have reached here");
			},
			context : { "random" : "object" },
			args : function () {
				expect(this).toEqual(opObj.context);
				// deliberately don't return
			}
		};

		spyOn(opObj, "args").andCallThrough();

		var opSuccess = jasmine.createSpy();

		var opError = function () {
			throw new Error("Should not have got error");
		};

		var op = new Rocketboots.util.AsyncOperation(opObj, opSuccess, opError);

		// The arguments definition function should not be called until the operation is run
		expect(opObj.args).not.toHaveBeenCalled();

		expect(function () { op.run(); }).toThrow("Operation arguments (specified as a function) were not valid - the argument definition function must return an array.");

		expect(opObj.args).toHaveBeenCalledWith();

	});

	it("should be able to handle error", function () {

		var opObj = {
			exec : function (success, error) {
				// default scope
				expect(this).toEqual(defaultContext);
				// no args
				expect(arguments.length).toEqual(2);

				expect(_.isFunction(success)).toBeTruthy();
				expect(_.isFunction(error)).toBeTruthy();

				// call error
				error();
			}
		};

		var opSuccess = function () {
			throw new Error("Should not have got success");
		};

		var opError = jasmine.createSpy();

		var op = new Rocketboots.util.AsyncOperation(opObj, opSuccess, opError);

		expect(op.isRunning()).toBeFalsy();
		op.run();
		expect(op.isRunning()).toBeFalsy();
		expect(op.success()).toBeFalsy();
		expect(op.getSettings().exec).toEqual(opObj.exec);
		expect(op.getSettings().context).toEqual(defaultContext);
		expect(op.getSettings().args).toEqual([]);
		expect(op.getResults()).toEqual([]);

		expect(opError).toHaveBeenCalledWith(op, []);
	});

	it("should call handlers with given context", function () {
		var opSuccess = function (success) { success(); };
		var opError = function (success, error) { error(); };
		var context = {"random": "object"};

		var successHandler = function () {
			expect(this).toEqual(context);
		};
		var errorHandler = function () {
			expect(this).toEqual(context);
		};

		new Rocketboots.util.AsyncOperation(opSuccess, successHandler, errorHandler, context).run();
		new Rocketboots.util.AsyncOperation(opError, successHandler, errorHandler, context).run();
		context = defaultContext;
		new Rocketboots.util.AsyncOperation(opSuccess, successHandler, errorHandler).run();
		new Rocketboots.util.AsyncOperation(opError, successHandler, errorHandler).run();
	});

	it("should be able to retrieve operation result on success", function () {
		var opObj = {
			exec : function (arg1, arg2, success, error) {
				// default scope
				expect(this).toEqual(opObj.context);
				// no args
				expect(arguments.length).toEqual(4);

				expect(arg1).toEqual("arg1");
				expect(arg2).toEqual("arg2");
				expect(_.isFunction(success)).toBeTruthy();
				expect(_.isFunction(error)).toBeTruthy();

				// call success
				success("first result", opObj, 123);
			},
			context : { "random" : "object" },
			args : ["arg1", "arg2"]
		};

		var opSuccess = jasmine.createSpy();

		var opError = function () {
			throw new Error("Should not have got error");
		};

		var op = new Rocketboots.util.AsyncOperation(opObj, opSuccess, opError);

		op.run();
		expect(op.isRunning()).toBeFalsy();
		expect(op.success()).toBeTruthy();
		expect(op.getSettings().exec).toEqual(opObj.exec);
		expect(op.getSettings().context).toEqual(opObj.context);
		expect(op.getSettings().args).toEqual(opObj.args);
		expect(op.getResults()).toEqual(["first result", opObj, 123]);

		expect(opSuccess).toHaveBeenCalledWith(op, ["first result", opObj, 123]);
	});

	it("should be able to retrieve operation result on error", function () {
		var opObj = {
			exec : function (success, error) {
				// default scope
				expect(this).toEqual(defaultContext);
				// no args
				expect(arguments.length).toEqual(2);

				expect(_.isFunction(success)).toBeTruthy();
				expect(_.isFunction(error)).toBeTruthy();

				// call error
				error(123, "anotherResult");
			}
		};

		var opSuccess = function () {
			throw new Error("Should not have got success");
		};

		var opError = jasmine.createSpy();

		var op = new Rocketboots.util.AsyncOperation(opObj, opSuccess, opError);

		expect(op.isRunning()).toBeFalsy();
		op.run();
		expect(op.isRunning()).toBeFalsy();
		expect(op.success()).toBeFalsy();
		expect(op.getSettings().exec).toEqual(opObj.exec);
		expect(op.getSettings().context).toEqual(defaultContext);
		expect(op.getSettings().args).toEqual([]);
		expect(op.getResults()).toEqual([123, "anotherResult"]);

		expect(opError).toHaveBeenCalledWith(op, [123, "anotherResult"]);
	});

});
