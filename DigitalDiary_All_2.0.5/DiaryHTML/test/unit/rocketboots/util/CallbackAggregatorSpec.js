/**
 * $Id: CallbackAggregatorSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
describe("RocketBoots CallbackAggregator Utility", function() {

	var defaultContext, handlers, callbackAggregator;

	beforeEach(function() {
		defaultContext = window;
		handlers = {
			success: function () {
				expect(this).toEqual(handlers);
			},
			error: function () {
				expect(this).toEqual(handlers);
			}
		};
		spyOn(handlers, "success").andCallThrough();
		spyOn(handlers, "error").andCallThrough();

		callbackAggregator = new Rocketboots.util.CallbackAggregator(handlers.success, handlers.error, handlers);
	});

	it("should be able to construct with no args and mark done", function() {

		var callbacks = new Rocketboots.util.CallbackAggregator();
		expect(callbacks.isComplete()).toBeFalsy();
		callbacks.done();
		expect(callbacks.isComplete()).toBeTruthy();
	});

	it("should be able to construct with all args and mark done", function() {
		expect(callbackAggregator.isComplete()).toBeFalsy();
		callbackAggregator.done();
		expect(callbackAggregator.isComplete()).toBeTruthy();
		expect(handlers.success).toHaveBeenCalledWith();
		expect(handlers.error).not.toHaveBeenCalled();
	});

	it("should be able to add call", function() {
		expect(callbackAggregator.isComplete()).toBeFalsy();
		callbackAggregator.addCall();
		callbackAggregator.done();
		expect(callbackAggregator.isComplete()).toBeFalsy();
		expect(handlers.success).not.toHaveBeenCalled();
		expect(handlers.error).not.toHaveBeenCalled();
	});

	it("should be able to add calls", function() {
		expect(callbackAggregator.isComplete()).toBeFalsy();
		callbackAggregator.addCalls(5);
		callbackAggregator.done();
		expect(callbackAggregator.isComplete()).toBeFalsy();
		expect(handlers.success).not.toHaveBeenCalled();
		expect(handlers.error).not.toHaveBeenCalled();
	});

	it("should be able to explicitly add success and error calls after done adding calls", function() {
		var checkNotComplete = function () {
			expect(callbackAggregator.isComplete()).toBeFalsy();
			expect(handlers.success).not.toHaveBeenCalled();
			expect(handlers.error).not.toHaveBeenCalled();
		};

		expect(callbackAggregator.isComplete()).toBeFalsy();
		callbackAggregator.addCalls(5);
		callbackAggregator.done();

		checkNotComplete();
		callbackAggregator.returnSuccess();
		checkNotComplete();
		callbackAggregator.returnError("First error message");
		checkNotComplete();
		callbackAggregator.returnSuccess();
		checkNotComplete();
		callbackAggregator.returnError("Second error message");
		checkNotComplete();
		callbackAggregator.returnSuccess();

		expect(callbackAggregator.isComplete()).toBeTruthy();
		expect(handlers.success).not.toHaveBeenCalled();
		expect(handlers.error).toHaveBeenCalledWith("First error message");
	});

	it("should be able to get success and error callback functions", function() {
		var checkNotComplete = function () {
			expect(callbackAggregator.isComplete()).toBeFalsy();
			expect(handlers.success).not.toHaveBeenCalled();
			expect(handlers.error).not.toHaveBeenCalled();
		};
		var success = callbackAggregator.getSuccessFunction();
		var error = callbackAggregator.getErrorFunction();

		checkNotComplete();
		callbackAggregator.addCalls(5);
		callbackAggregator.done();

		checkNotComplete();
		success();
		checkNotComplete();
		error("First error message", 123);
		checkNotComplete();
		success();
		checkNotComplete();
		error("Second error message");
		checkNotComplete();
		success();

		expect(callbackAggregator.isComplete()).toBeTruthy();
		expect(handlers.success).not.toHaveBeenCalled();
		expect(handlers.error).toHaveBeenCalledWith("First error message", 123);
	});

	it("should not be able to add call after done", function() {
		callbackAggregator.done();
		expect(function(){ callbackAggregator.addCall(); }).toThrow();
	});

	it("should not be able to add calls after done", function() {
		callbackAggregator.done();
		expect(function(){ callbackAggregator.addCalls(); }).toThrow();
	});

	it("should not be able to get success handler after done", function() {
		callbackAggregator.done();
		expect(function(){ callbackAggregator.getSuccessFunction(); }).toThrow();
	});

	it("should be able to get error handler after done", function() {
		callbackAggregator.done();
		expect(function(){ callbackAggregator.getErrorFunction(); }).toThrow();
	});

	it("should not be able to return before adding a call", function () {
		expect(function () { callbackAggregator.returnSuccess(); }).toThrow("More returns than calls made");
	});

	it("should not be able to add a negative number of calls", function () {
		expect(function () { callbackAggregator.addCalls(-1); }).toThrow("Cannot add a negative number of calls");
	});

	it("should not be able to get all error arguments until done is called", function () {
		expect(function () { callbackAggregator.getAllErrorArgs(); }).toThrow("Cannot retrieve the error arguments until all calls have been added. Did you forget to call 'done', punk?");
	});

	it("should be able to get all error args", function() {
		var checkNotComplete = function () {
			expect(callbackAggregator.isComplete()).toBeFalsy();
			expect(handlers.success).not.toHaveBeenCalled();
			expect(handlers.error).not.toHaveBeenCalled();
		};
		var success = callbackAggregator.getSuccessFunction();
		var error = callbackAggregator.getErrorFunction();

		checkNotComplete();
		callbackAggregator.addCalls(5);
		callbackAggregator.done();

		checkNotComplete();
		success();
		checkNotComplete();
		error("First error message", 123);
		checkNotComplete();
		success();
		checkNotComplete();
		error("Second error message");
		checkNotComplete();
		success();

		expect(callbackAggregator.isComplete()).toBeTruthy();
		expect(handlers.success).not.toHaveBeenCalled();
		expect(handlers.error).toHaveBeenCalledWith("First error message", 123);
		expect(callbackAggregator.getAllErrorArgs().length).toEqual(2);
		expect(callbackAggregator.getAllErrorArgs()[0]).toEqual(["First error message", 123]);
		expect(callbackAggregator.getAllErrorArgs()[1]).toEqual(["Second error message"]);
	});

	it("should call success handler after one call", function () {
		var checkNotComplete = function () {
			expect(callbackAggregator.isComplete()).toBeFalsy();
			expect(handlers.success).not.toHaveBeenCalled();
			expect(handlers.error).not.toHaveBeenCalled();
		};
		var success = callbackAggregator.getSuccessFunction();
		var error = callbackAggregator.getErrorFunction();

		checkNotComplete();
		callbackAggregator.addCall();
		callbackAggregator.done();

		expect(callbackAggregator.isComplete()).toBeFalsy();
		expect(handlers.success).not.toHaveBeenCalled();
		expect(handlers.error).not.toHaveBeenCalled();

		success();

		expect(callbackAggregator.isComplete()).toBeTruthy();
		expect(handlers.success).toHaveBeenCalledWith();
		expect(handlers.error).not.toHaveBeenCalled();
	});

	it("should call error handler after one call", function () {
		var checkNotComplete = function () {
			expect(callbackAggregator.isComplete()).toBeFalsy();
			expect(handlers.success).not.toHaveBeenCalled();
			expect(handlers.error).not.toHaveBeenCalled();
		};
		var success = callbackAggregator.getSuccessFunction();
		var error = callbackAggregator.getErrorFunction();

		checkNotComplete();
		callbackAggregator.addCall();
		callbackAggregator.done();

		expect(callbackAggregator.isComplete()).toBeFalsy();
		expect(handlers.success).not.toHaveBeenCalled();
		expect(handlers.error).not.toHaveBeenCalled();

		error();

		expect(callbackAggregator.isComplete()).toBeTruthy();
		expect(handlers.success).not.toHaveBeenCalled();
		expect(handlers.error).toHaveBeenCalledWith();
	});

	it("should handle functions which behave synchronously", function () {
		var checkNotComplete = function () {
			expect(callbackAggregator.isComplete()).toBeFalsy();
			expect(handlers.success).not.toHaveBeenCalled();
			expect(handlers.error).not.toHaveBeenCalled();
		};
		var success = callbackAggregator.getSuccessFunction();
		var error = callbackAggregator.getErrorFunction();

		checkNotComplete();
		callbackAggregator.addCall();
		callbackAggregator.returnSuccess(); // simulate instant (synchronous) return

		checkNotComplete();
		callbackAggregator.addCall();
		callbackAggregator.returnError("something");

		checkNotComplete();
		callbackAggregator.addCall();
		callbackAggregator.returnError("something else", 123);

		checkNotComplete();
		callbackAggregator.addCall();
		callbackAggregator.returnSuccess();

		checkNotComplete();
		callbackAggregator.addCall();
		callbackAggregator.returnSuccess();

		checkNotComplete();
		callbackAggregator.done();

		// check complete
		expect(callbackAggregator.isComplete()).toBeTruthy();
		expect(handlers.success).not.toHaveBeenCalled();
		expect(handlers.error).toHaveBeenCalledWith("something");
		expect(handlers.error.callCount).toEqual(1, "error should be called exactly once");
	});

});
