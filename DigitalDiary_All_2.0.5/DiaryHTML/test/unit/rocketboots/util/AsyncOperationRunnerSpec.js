/**
 * $Id: AsyncOperationRunnerSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
describe("RocketBoots AsyncOperationRunner Utility", function() {

	var defaultContext = null,
		operations = [],
		_defer = null;

	beforeEach(function() {
		defaultContext = window;
		_defer = _.defer;

		// Mock out defer
		_.defer = function (func) { func(); };

		var execSuccess = function (success, error) {
			expect(this).toEqual(defaultContext);
			expect(arguments.length).toEqual(2);
			expect(_.isFunction(success)).toBeTruthy();
			expect(_.isFunction(error)).toBeTruthy();
			success();
		};

		operations = [
			{
				exec : execSuccess,
				context : defaultContext,
				args: []
			},
			{
				exec : function (arg1, arg2, arg3, arg4) {
					expect(this).toEqual("another context");
					expect(arg1).toEqual(123);
					expect(arg2).toEqual("ABC");
					expect(_.isFunction(arg3)).toBeTruthy();
					expect(_.isFunction(arg4)).toBeTruthy();
					arg3();
				},
				context : "another context",
				args: [123, "ABC"]
			},
			{
				exec : function (success, error) {
					expect(this).toEqual({});
					expect(arguments.length).toEqual(2);
					success();
				},
				context : {}
			},
			{
				exec : execSuccess,
				args : []
			},
			function (success, error) {
				expect(_.isFunction(success)).toBeTruthy();
				expect(_.isFunction(error)).toBeTruthy();
				success();
			}
		];
	});

	afterEach(function () {
		// Restore defer
		_.defer = _defer;
	});

	it("should be able to construct", function() {

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations);
		expect(opRunner.hasCompleted()).toBeFalsy("should not be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");

	});

	it("should be able to run successfully", function () {

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations);
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeTruthy("should run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[4]).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
	});

	it("should not be able to run multiple times", function () {
		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations);
		opRunner.run();
		expect(opRunner.run).toThrow();
	});

	it("should be able to run with errors", function () {

		// error at #3
		operations[2].exec = function (success, error) { error(); };

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations);
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeFalsy("should not run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).not.toHaveBeenCalled();
		expect(operations[4]).not.toHaveBeenCalled();
	});

	it("should be able to run with default args", function () {

		var defaultArgsHandler = function (arg1, arg2, arg3, success, error) {
			expect(arg1).toEqual(1);
			expect(arg2).toEqual(2);
			expect(arg3).toEqual(3);
			success();
		};

		// replace the functions which are expected to use the default args
		operations[2].exec = defaultArgsHandler;
		operations[4] = defaultArgsHandler;

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations, {
			defaultArgs : [1, 2, 3]
		});
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeTruthy("should run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(1, 2, 3, jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[4]).toHaveBeenCalledWith(1, 2, 3, jasmine.any(Function), jasmine.any(Function));
	});

	it("should be able to run with default context", function () {

		operations[3].exec = function (success, error) {
			expect(this).toEqual("different context");
			expect(arguments.length).toEqual(2);
			success();
		};

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations, {
			defaultContext: "different context"
		});
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeTruthy("should run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[4]).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
	});

	it("should be able to run with errors and force continue", function () {
		// error at #3
		operations[2].exec = function (success, error) { error(); };
		// force continue
		operations[2].error = function () { return false; };

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations);
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeTruthy("should run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[4]).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
	});

	it("should be able to run successfully and early exit", function () {
		// force continue
		operations[2].success = function () { return false; };

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations);
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeFalsy("should not run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).not.toHaveBeenCalled();
		expect(operations[4]).not.toHaveBeenCalled();
	});

	it("should call 'allSuccessful' upon completion", function () {

		var allSuccessfulSpy = jasmine.createSpy();

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations, {
			allSuccessful: allSuccessfulSpy,
			anyError: function () {
				throw new Error("Should not have called error");
			}
		});
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeTruthy("should run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[4]).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));

		expect(allSuccessfulSpy).toHaveBeenCalledWith(opRunner);
	});

	it("should 'anyError' upon any error", function () {

		var anyErrorSpy = jasmine.createSpy();

		// error at #3
		operations[2].exec = function (success, error) { error(); };

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations, {
			allSuccessful: function () {
				throw new Error("Should not complete successfully");
			},
			anyError: anyErrorSpy
		});
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeFalsy("should not run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).not.toHaveBeenCalled();
		expect(operations[4]).not.toHaveBeenCalled();

		expect(anyErrorSpy).toHaveBeenCalledWith(opRunner);
	});

	it("should be able to apply any error arguments to a error handler function", function () {
		var anyErrorSpy = jasmine.createSpy();

		// error at #3
		operations[2].exec = function (success, error) { error("error argument 1", "error argument 2"); };

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations, {
			allSuccessful: function () {
				throw new Error("Should not complete successfully");
			},
			anyError: anyErrorSpy
		});
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeFalsy("should not run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).not.toHaveBeenCalled();
		expect(operations[4]).not.toHaveBeenCalled();

		expect(anyErrorSpy).toHaveBeenCalledWith(opRunner);

		// Apply error arguments
		var handlers = {
			"errorHandler" : function (arg1, arg2) {
				expect(this).toEqual(handlers);
				expect(arg1).toEqual("error argument 1");
				expect(arg2).toEqual("error argument 2");
				expect(arguments.length).toEqual(2);
			}
		};
		spyOn(handlers, "errorHandler").andCallThrough();
		opRunner.applyAnyErrorArgs(handlers.errorHandler, handlers);
		expect(handlers.errorHandler).toHaveBeenCalledWith("error argument 1", "error argument 2");
	});

	it("should be able to apply any zero error arguments to a error handler function", function () {
		var anyErrorSpy = jasmine.createSpy();

		// error at #3
		operations[2].exec = function (success, error) { error(); };

		spyOn(operations[0], "exec").andCallThrough();
		spyOn(operations[1], "exec").andCallThrough();
		spyOn(operations[2], "exec").andCallThrough();
		spyOn(operations[3], "exec").andCallThrough();
		spyOn(operations, 4).andCallThrough();

		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations, {
			allSuccessful: function () {
				throw new Error("Should not complete successfully");
			},
			anyError: anyErrorSpy
		});
		opRunner.run();

		expect(opRunner.hasCompleted()).toBeTruthy("should be complete");
		expect(opRunner.isRunning()).toBeFalsy("should not be running");
		expect(opRunner.didRunAll()).toBeFalsy("should not run all");

		expect(operations[0].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[1].exec).toHaveBeenCalledWith(123, "ABC", jasmine.any(Function), jasmine.any(Function));
		expect(operations[2].exec).toHaveBeenCalledWith(jasmine.any(Function), jasmine.any(Function));
		expect(operations[3].exec).not.toHaveBeenCalled();
		expect(operations[4]).not.toHaveBeenCalled();

		expect(anyErrorSpy).toHaveBeenCalledWith(opRunner);

		// Apply error arguments
		var handlers = {
			"errorHandler" : function () {
				expect(this).toEqual(handlers);
				expect(arguments.length).toEqual(0);
			}
		};
		spyOn(handlers, "errorHandler").andCallThrough();
		opRunner.applyAnyErrorArgs(handlers.errorHandler, handlers);
		expect(handlers.errorHandler).toHaveBeenCalledWith();
	});

	it("should not be able to call applyAnyErrorArgs before operation completion", function () {
		var opRunner = new Rocketboots.util.AsyncOperationRunner(operations);
		expect(opRunner.applyAnyErrorArgs).toThrow("Cannot apply error arguments until all operations completed");
	});

});
