/**
 * $Id: StringUtilSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
describe("RocketBoots String Utilities", function() {

	var defaultEllipsis = "&hellip;";

	beforeEach(function() {

	});

	describe("Condense a string", function () {

		it("should be able to condense a string with 1 word", function() {
			expect(Rocketboots.util.StringUtil.condense("abc", 5)).toEqual("abc");
			expect(Rocketboots.util.StringUtil.condense("abcdef", 5, null, "...")).toEqual("abcde...");
		});

		it("should be able to condense a string with whole words only", function () {
			var input = "Hi my name is Bob";

			// First word case
			expect(Rocketboots.util.StringUtil.condense(input, 1, null, "...")).toEqual("H...");
			expect(Rocketboots.util.StringUtil.condense(input, 2, null, "...")).toEqual("Hi...");
			expect(Rocketboots.util.StringUtil.condense(input, 3, null, "...")).toEqual("Hi...");

			// Middle case
			for (var i = 5; i <= 9; ++i) {
				expect(Rocketboots.util.StringUtil.condense(input, i, null, "...")).toEqual("Hi my...");
			}
			expect(Rocketboots.util.StringUtil.condense(input, 10, null, "...")).toEqual("Hi my name...");

			// End case
			expect(Rocketboots.util.StringUtil.condense(input, input.length - 1, null, "...")).toEqual("Hi my name is...");
			expect(Rocketboots.util.StringUtil.condense(input, input.length, null, "...")).toEqual(input);
			expect(Rocketboots.util.StringUtil.condense(input, input.length + 1, null, "...")).toEqual(input);
		});

		it("should be able to condense a string ignoring words", function () {
			expect(Rocketboots.util.StringUtil.condense("Hi my name is Bob", 8, false, "...")).toEqual("Hi my na...");
		});

		it("should be able to condense a string with the default ellipsis", function () {
			expect(Rocketboots.util.StringUtil.condense("Hi my name is Bob", 8, false)).toEqual("Hi my na" + defaultEllipsis);
		});

	});

});