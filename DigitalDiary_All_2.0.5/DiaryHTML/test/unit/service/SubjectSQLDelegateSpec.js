/**
 * Unit Tests for the Subject Delegate
 * 
 * author - Brian Bason
 */
describe("Subject SQL Delegate - Test Suite", function() {
	
	// holds the dummy app database layer that is required for testing
	var dummyApplicationDatabase = (function() {
		
		var service = function() {};
		
		service.prototype.runQuery = function(text, parameters, successCallback, errorCallback) {
			
			// return the dummy SQL Statement
			successCallback(new air.SQLStatement());
			
		};
		
		return service;
		
	})();
	
	beforeEach(function() {
		// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
		air = new Object();

		air.SQLStatement = function() {
		};
		
		air.SQLStatement.prototype.execute = function() {
		};
		
		// configure the application context for the unit test
		app = new Rocketboots.Application({
	           
           mapEvents: function() {
               // map events needed for the test
           },
           
           mapServices: function() {
        	   // map the services needed for the test
        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
           },
           
           registerTemplates: {
               // register templates, if needed
           }
	           
	    });
		
		// init the application context
		app.initialize();
		
	});
	
	describe("Subject is in use returns true" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : [{ 'INUSE' : 1 }] };
									
			};
			
		});
	
		it("should be able to determine that subject is in use", function() {
		   
			// holds the subject delegate that is being tested
			var delegate = new diary.service.SubjectSQLDelegate(),
				successCalled = false;
			
			// the success function
			function success(isInUse) {
				// the delegate should have returned that subject is in use
				expect(isInUse).toBeTruthy();
				successCalled = true;
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// determine if subject is in use
			delegate.isSubjectInUse(1, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
	});
	
	describe("Subject is in use returns false" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : [{ 'INUSE' : 0 }] };
									
			};
			
		});
	
		it("should be able to determine that subject is not use", function() {
		   
			// holds the subject delegate that is being tested
			var delegate = new diary.service.SubjectSQLDelegate(),
				successCalled = false;
			
			// the success function
			function success(isInUse) {
				// the delegate should have returned that subject exists with title
				expect(isInUse).toBeFalsy();
				successCalled = true;
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// determine if subject is in use
			delegate.isSubjectInUse(1, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
	});
	
	describe("Subject exists with title returns subject" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : [{ 'id' : 1,
									'name' : "Math",
									'colour' : "Blue" }] };
									
			};
			
		});
	
		it("should be able to determine that subject exists with title", function() {
		   
			// holds the subject delegate that is being tested
			var delegate = new diary.service.SubjectSQLDelegate(),
				successCalled = false;
			
			// the success function
			function success(subject) {
				// the delegate should have returned that subject is in use
				expect(subject instanceof diary.model.Subject).toBeTruthy();
				expect(subject.get('id')).toEqual(1);
				expect(subject.get('title')).toEqual("Math");
				expect(subject.get('colour')).toEqual("Blue");
				successCalled = true;
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// determine if subject is in use
			delegate.subjectExistsWithTitle("Math", success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
	});
	
	describe("Subject exists with title returns null" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : null };
									
			};
			
		});
	
		it("should be able to determine that not subject exists with title", function() {
		   
			// holds the subject delegate that is being tested
			var delegate = new diary.service.SubjectSQLDelegate(),
				successCalled = false;
			
			// the success function
			function success(subject) {
				// the delegate should have returned that no subject exists with title
				expect(subject).toBeNull();
				successCalled = true;
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// determine if subject is in use
			delegate.subjectExistsWithTitle('test title', success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
	});
	
	describe("Creating a subject" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy identifier
				return {'lastInsertRowID' : 1 };
									
			};
						
		});
	
		it("should be able to create a subject", function() {
		   
			// holds the subject delegate that is being tested
			var delegate = new diary.service.SubjectSQLDelegate(),
				successCalled = false;
			
			// the success function
			function success(subjectId) {
				// the delegate should have returned the subject identifier
				expect(subjectId).toEqual(1);
				successCalled = true;
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			var subject = new diary.model.Subject({'title' : "test subject", 
												   'colour' : 'red'});
			
			// create a subject
			delegate.createSubject(subject, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
	});
	
	describe("Deleting a subject" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
						
		});
	
		it("should be able to delete a subject", function() {
		   
			// holds the subject delegate that is being tested
			var delegate = new diary.service.SubjectSQLDelegate(),
				successCalled = false;
			
			// the success function
			function success() {
				successCalled = true;
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// delete a subject
			delegate.deleteSubject(1, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
	});
	
	describe("Update a subject" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy count data for the updated subject
				return {'data' : [{ 'count' : 1 }] };
									
			};
			
		});
	
		it("should be able to update the subject", function() {
		   
			// holds the student profile delegate that is being tested
			var delegate = new diary.service.SubjectSQLDelegate(),
				// create a dummy subject
				subject = new diary.model.Subject({ 'id' : 1,
													'title' : "Test Subject",
													'colour' : "red" }),
				successCalled = false;
			
			// the success function
			function success() {
				successCalled = true;
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// get the student profile from the delegate
			delegate.updateSubject(subject, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
		it("should fail if subject to be updated has no identifier specified", function() {
			   
			// holds the subject delegate that is being tested
			var delegate = new diary.service.SubjectSQLDelegate(),
				// create a dummy subject
				subject = new diary.model.Subject({ 'title' : "Test Subject",
													'colour' : "red" });
			
			// the success function
			function success() {
				throw "Test failed - an error should have been produced";
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// try to update the student profile
			var testFunction = function() {
				delegate.updateSubject(subject, success, error);
			};
			
			expect(testFunction).toThrow("Subject is not valid, subject identifier must be specified");
			
		});
		
	});
		
});