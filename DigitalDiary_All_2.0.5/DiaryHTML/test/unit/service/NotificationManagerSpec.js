/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: NotificationManagerSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Notification Manager", function() {

	describe("Successful cases for Notification Manager Suite" , function() {

		// holds the triggered notifications by the notification manager
		var generatedNotifications;

		// the function that generates the overdue tasks
		var generateOverdueTasks = function() {
			var homeWorkTaskDueDay = new Rocketboots.date.Day();
			var assignmentDueDay = new Rocketboots.date.Day().addDays(-1);
			var overdueTasks = new diary.collection.DiaryItemCollection();

			overdueTasks.add(new diary.model.Task({
				"id" : 1,
				"title" : "Overdue Task Homework",
				"type" : diary.model.DiaryItem.TYPE.HOMEWORK,
				"assignedDay" : new Rocketboots.date.Day(homeWorkTaskDueDay).addDays(-1),
				"assignedTime" : new Rocketboots.date.Time(12, 0),
				"dueDay" : homeWorkTaskDueDay,
				"dueTime" : new Rocketboots.date.Time(5, 55),
				"subject" : new diary.model.Subject({'title': "U1 Math", 'colour': "Red"})
			}));

			overdueTasks.add(new diary.model.Task({
				"id" : 2,
				"title" : "Overdue Task Assignment",
				"type" : diary.model.DiaryItem.TYPE.ASSIGNMENT,
				"assignedDay" : new Rocketboots.date.Day(assignmentDueDay).addDays(-3),
				"assignedTime" : new Rocketboots.date.Time(12, 0),
				"dueDay" : assignmentDueDay,
				"subject" : new diary.model.Subject({'title': "U1 English", 'colour': "Blue"})
			}));

			return overdueTasks;
		};

		// the dummy function for the listening of events
		var dummyEventListener = (function() {

			var _command = function() {};

			_command.prototype.execute = function(notification) {
				generatedNotifications.push(notification);
			};

			return _command;

		})();

		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

			   modelClass:	diary.model.DiaryModel,

	           mapEvents: function() {
	               // map events needed for the test
	        	   this.mapEvent(EventName.NEWNOTIFICATION, dummyEventListener);
	           },

	           mapServices: function() {
	        	   // map the services needed for the test
	           },

	           registerTemplates: {
	               // register templates, if needed
	           }

		    });

			// init the application context
			app.initialize();

			// create a dummy diary item collection
			app.model.set(ModelName.DIARYITEMS, generateOverdueTasks());

			// reset the notification array for each test
			generatedNotifications = new Array();

		});

		it("should be able to generate notification for overdue tasks", function() {

			// holds the notification manager
			var manager = new diary.service.NotificationManager();

			// create the test task that we will use for the test
			manager.generatePendingNotifications();

			// by now the events should have been fired so we can
			// check if we have any notification in the notification
			// queue of the test
			expect(generatedNotifications.length).toBe(2);

			// go through the notifications and make sure that the correct ones were generated
			_.forEach(generatedNotifications, function(notification) {

				// check that the title contains the Overdue message
				expect(notification.get("title")).toContain("Overdue ");

				if (notification.get("title") === "Overdue homework") {

					// validate the notification details
					expect(notification.get("message")).toEqual("'Overdue Task&hellip;' is overdue by 1 day");
					expect(notification.get("type")).toEqual(diary.model.DiaryItem.TYPE.HOMEWORK.toLowerCase());

				} else if (notification.get("title") === "Overdue assignment") {

					// validate the notification details
					expect(notification.get("message")).toEqual("'Overdue Task&hellip;' is overdue by 2 days");
					expect(notification.get("type")).toEqual(diary.model.DiaryItem.TYPE.ASSIGNMENT.toLowerCase());

				}

			});

		});

	});

});
