/**
 * Unit Tests for the School Manager
 * 
 * author - Brian Bason
 */
describe("School Manager - Test Suite", function() {
	
	describe("Successful cases for School Manager Suite" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummySchoolDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the school profile
			service.prototype.getSchoolProfile = function(successCallback, errorCallback) {
				
				var schoolProfile = new diary.model.SchoolProfile({
													 'id' : 1,
													 'name' : "Test School",
													 'schoolLogo' : "file://var/lib/images/logo.gif",
													 'schoolImage' : "file://var/lib/images/image.gif",
													 'schoolMotto' : "The school motto",
													 'missionStatement' : "The school mission statement",
													 'diaryTitle' : "The Diary 2012",
													 'includeEmpower' : true });
				
				successCallback(schoolProfile);
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.SCHOOLPROFILEDELEGATE, dummySchoolDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should be able to return the school profile", function() {
		   
			// holds the school manager
			var manager = new diary.service.SchoolManager();
			
			// get the school profile from the manager
			manager.getSchoolProfile(success, error);
			
			// success function
			function success(schoolProfile) {
				// the manager should have returned a school profile
				expect(schoolProfile instanceof diary.model.SchoolProfile).toBeTruthy();
				
				// the profile that was returned should be the same as the dummy data			
				expect(schoolProfile.id).toEqual(1);
				expect(schoolProfile.get("name")).toEqual("Test School");					
				expect(schoolProfile.get("schoolLogo")).toEqual("file://var/lib/images/logo.gif");					
				expect(schoolProfile.get("schoolImage")).toEqual("file://var/lib/images/image.gif");
				expect(schoolProfile.get("schoolMotto")).toEqual("The school motto");					
				expect(schoolProfile.get("missionStatement")).toEqual("The school mission statement");					
				expect(schoolProfile.get("diaryTitle")).toEqual("The Diary 2012");
				expect(schoolProfile.get("includeEmpower")).toBeTruthy();
				expect(schoolProfile.get("contentMapping") instanceof diary.model.ContentMapping).toBeTruthy();
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
		});
		
		it("should be able to populate the school profile", function() {
		   
			// holds the school manager
			var manager = new diary.service.SchoolManager();
			
			// holds the model that is to be updated
			var schoolProfile = new diary.model.SchoolProfile();
			
			// get the school profile from the manager
			manager.getSchoolProfile(success, error, schoolProfile);
			
			// success function
			function success() {
				// the profile that was returned should be the same as the dummy data			
				expect(schoolProfile.id).toEqual(1);
				expect(schoolProfile.get("name")).toEqual("Test School");					
				expect(schoolProfile.get("schoolLogo")).toEqual("file://var/lib/images/logo.gif");					
				expect(schoolProfile.get("schoolImage")).toEqual("file://var/lib/images/image.gif");
				expect(schoolProfile.get("schoolMotto")).toEqual("The school motto");					
				expect(schoolProfile.get("missionStatement")).toEqual("The school mission statement");					
				expect(schoolProfile.get("diaryTitle")).toEqual("The Diary 2012");
				expect(schoolProfile.get("includeEmpower")).toBeTruthy();
				expect(schoolProfile.get("contentMapping") instanceof diary.model.ContentMapping).toBeTruthy();
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
		});
		
	});

	describe("No shcool profile is found in data source" , function() {
			
		// holds the dummy app database layer that is required for testing
		var dummySchoolDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the school profile
			service.prototype.getSchoolProfile = function(success, error) {
				
				success(null);
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.SCHOOLPROFILEDELEGATE, dummySchoolDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should generate error if no school profile is found", function() {
		   
			// holds the school manager
			var manager = new diary.service.SchoolManager();
			
			// the success function
			function success() {
				throw "Test failed - Should have produced an error";
			}
			
			// the error function
			function error(message) {
				expect(message).toEqual("No school profile was found, profile should be defined");
			}
			
			// try to get the school profile
			manager.getSchoolProfile(success, error);
			
		});
		
	});
	
	describe("Error while trying to read school profile from data source" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummySchoolDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the school profile
			service.prototype.getSchoolProfile = function(success, error) {
				
				error("Error while trying to get school profile");
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.SCHOOLPROFILEDELEGATE, dummySchoolDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should generate error if an error occurs while trying to read school profile", function() {
		   
			// holds the school manager
			var manager = new diary.service.SchoolManager();
			
			// the success function
			function success() {
				throw "Test failed - Should have produced an error";
			}
			
			// the error function
			function error(message) {
				expect(message).toEqual("Error while trying to get school profile");
			}
			
			// try to get the school profile
			manager.getSchoolProfile(success, error);
			
		});
		
	});
		
});