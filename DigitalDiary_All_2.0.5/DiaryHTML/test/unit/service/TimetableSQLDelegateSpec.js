/**
 * Unit Tests for the Timetable Delegate
 * 
 * author - John Pearce
 */
describe("Timetable SQL Delegate - Test Suite", function() {
	
	describe("Get a current timetable" , function() {
		
		var timetableDay = new Rocketboots.date.Day();

		// setup the env for the the unit test
		beforeEach(function() {

			// the dummy application for the unit test
			app = mock.appInitialize({
				'sqlStatementResults' : [
					// return the dummy data for the timetable
					[
						{
							'timetableId' : 1,
							'startDate' : timetableDay.getKey(),
							'cycleLength' : 10,
							'periodId' : 1, 
							'startTime' : 1100 // 11am
						},
						{
							'timetableId' : 1,
							'startDate' : timetableDay.getKey(),
							'cycleLength' : 10,
							'periodId' : 2,
							'startTime' : 1200, // 12pm
							'endTime' : 1300 // 1pm
						}
					],
					
					// return the dummy data for the classes
					[
					 	{
					 		'id': 1,
					 		'cycleDay': 1,
					 		'periodId': 1,
					 		'subjectId': 1,
					 		'room': "Test Room"
					 	}
					]
				]
			});
			
		});
	
		it("should be able to return a timetable", function() {
		   
			// holds the task delegate that is being tested
			var delegate = new diary.service.TimetableSQLDelegate();
			
			// the success function
			function success(timetable) {
				
				// the delegate should have returned a collection of tasks
				expect(timetable instanceof diary.model.Timetable).toBeTruthy();
				
				expect(timetable.id).toEqual(1);
				expect(timetable.get("startDay").getKey()).toEqual(timetableDay.getKey());
				expect(timetable.get("periods") instanceof diary.collection.PeriodCollection).toBeTruthy();
				expect(timetable.get("periods").size()).toEqual(2);
				expect(timetable.get("classes") instanceof diary.collection.ClassCollection).toBeTruthy();
				expect(timetable.get("classes").size()).toEqual(1);
				expect(timetable.get("cycleLength")).toEqual(10);
				
			}
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// create a dummy collection of subjects
			var subjects = new diary.collection.SubjectCollection();
			subjects.add(new diary.model.Subject({ 'id' : 1,
												   'title' : "Subject Test", 
												   'colour' : "Subject Colour" }));
			
			delegate.getCurrentTimetable(timetableDay, subjects, success, error);
			
		});
		
	});
	
	describe("Get a timetable in period" , function() {
		
		var timetableDay1 = new Rocketboots.date.Day(2012, 1, 1),
			timetableDay2 = new Rocketboots.date.Day(2012, 5, 1);
			

		// setup the env for the the unit test
		beforeEach(function() {
				
			// reset app before each test
			app = mock.appInitialize({
				// return the dummy data for the timetable
				'sqlStatementResults' : [
                     // timetables
                     [
     					{
     						'timetableId' : 1,
     						'startDate' : timetableDay1.getKey(),
     						'cycleLength' : 10,
     						'periodId' : 1, 
     						'startTime' : Rocketboots.date.Time.parseTime(new Date()).toInteger()
     					},
     					{
     						'timetableId' : 1,
     						'startDate' : timetableDay1.getKey(),
     						'cycleLength' : 10,
     						'periodId' : 2,
     						'startTime' : Rocketboots.date.Time.parseTime(new Date()).toInteger()
     					},
     					{
     						'timetableId' : 2,
     						'startDate' : timetableDay2.getKey(),
     						'cycleLength' : 20,
     						'periodId' : 3, 
     						'startTime' : Rocketboots.date.Time.parseTime(new Date()).toInteger()
     					},
     					{
     						'timetableId' : 2,
     						'startDay' : timetableDay2.getKey(),
     						'cycleLength' : 20,
     						'periodId' : 4,
     						'startTime' : Rocketboots.date.Time.parseTime(new Date()).toInteger()
     					}
     				],
     				
     				// classes
     				[
     					{
     						'timetable': 1,
     						'id': 1,
     						'cycleDay': 1,
     						'periodId': 1,
     						'subjectId': 1,
     						'room': "Test Room 1"
     					},
     					{
     						'timetable': 1,
     						'id': 2,
     						'cycleDay': 1,
     						'periodId': 2,
     						'subjectId': 2,
     						'room': "Test Room 2"
     					}
     				 ]
                 ]
			});
			
		});
		
		it("should be able to return timetables in a range", function() {
		   
			// holds the task delegate that is being tested
			var delegate = new diary.service.TimetableSQLDelegate();
			
			// the success function
			function success(timetables) {
				
				// the delegate should have returned a collection of timetables
				expect(timetables instanceof diary.collection.TimetableCollection).toBeTruthy();
				expect(timetables.length).toEqual(2);
						
				var timetable1 = timetables.get(1);
				
				expect(timetable1.get("startDay").getKey()).toEqual(timetableDay1.getKey());
				expect(timetable1.get("periods") instanceof diary.collection.PeriodCollection).toBeTruthy();
				expect(timetable1.get("periods").size()).toEqual(2);
				expect(timetable1.get("classes") instanceof diary.collection.ClassCollection).toBeTruthy();
				expect(timetable1.get("classes").size()).toEqual(2, "Number of classes");
				expect(timetable1.get("cycleLength")).toEqual(10);
				
				var timetable2 = timetables.get(2);
				
				expect(timetable2.get("startDay").getKey()).toEqual(timetableDay2.getKey());
				expect(timetable2.get("periods") instanceof diary.collection.PeriodCollection).toBeTruthy();
				expect(timetable2.get("periods").size()).toEqual(2);
				expect(timetable2.get("classes") instanceof diary.collection.ClassCollection).toBeTruthy();
				expect(timetable2.get("classes").size()).toEqual(0);
				expect(timetable2.get("cycleLength")).toEqual(20);
				
			}
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// create a dummy collection of subjects
			var subjects = new diary.collection.SubjectCollection();
			
			subjects.add(new diary.model.Subject({ 'id' : 1,
												   'title' : "Subject Test", 
												   'colour' : "Grey" }));
			
			subjects.add(new diary.model.Subject({ 'id' : 2,
				   								   'title' : "Subject Test 2", 
				   								   'colour' : "Blue" }));
			
			// get the overdue tasks from the delegate
			delegate.getTimetables(new Rocketboots.date.Day(2012, 0, 1), 
								   new Rocketboots.date.Day(2012, 11, 31),
								   subjects,
								   success, 
								   error);
			
		});
		
	});
		
});