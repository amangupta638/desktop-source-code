/**
 * Unit Tests for the DayOfNote Model
 * 
 * author - Justin Judd
 */
describe("DayOfNote - Test Suite", function() {

	/**
	 * Suite of tests which should create a DayOfNote
	 */
	describe("Successful creation of DayOfNote Suite", function() {

		it("should be able to create a DayOfNote with default values",
				function() {
					var testDay = new diary.model.DayOfNote();

					expect(testDay.get("date")).toBeNull();
					expect(testDay.get("overrideCycleDay")).toBeNull();
					expect(testDay.get("includeInCycle")).toBeNull();
					expect(testDay.get("isGreyedOut")).toBeNull();
					expect(testDay.get("title")).toBeNull();
				});

		it("should be able to create a DayOfNote with given values",
				function() {
					var testDate = new Rocketboots.date.Day();
					var testTitle = "Test Holiday";
					var testOverrideCycleDay = 3;
					var testDay = new diary.model.DayOfNote({
						"date" : testDate,
						"overrideCycleDay" : testOverrideCycleDay,
						"includeInCycle" : false,
						"isGreyedOut" : true,
						"title" : testTitle
					});

					expect(testDay.get("date").getKey()).toEqual(testDate.getKey());
					expect(testDay.get("overrideCycleDay")).toEqual(
							testOverrideCycleDay);
					expect(testDay.get("includeInCycle")).toBeFalsy();
					expect(testDay.get("isGreyedOut")).toBeTruthy();
					expect(testDay.get("title")).toEqual(testTitle);
				});

		it("should be able to create a DayOfNote with partial values",
				function() {
					var testDate = new Rocketboots.date.Day();
					var testDay = new diary.model.DayOfNote({
						"date" : testDate,
						"includeInCycle" : true
					});

					expect(testDay.get("date").getKey()).toEqual(testDate.getKey());
					expect(testDay.get("overrideCycleDay")).toBeNull();
					expect(testDay.get("includeInCycle")).toBeTruthy();
					expect(testDay.get("isGreyedOut")).toBeNull();
					expect(testDay.get("title")).toBeNull();
				});

	});

	/**
	 * Suite of tests which should stop the DayOfNote from being created
	 */
	describe("Unsuccessful creation of DayOfNote Suite", function() {

		it("should fail when date is not specified", function() {

			var testTitle = "Test Holiday";
			var testDay = new diary.model.DayOfNote({});
			var attrs = {
				"overrideCycleDay" : true,
				"includeInCycle" : false,
				"isGreyedOut" : true,
				"title" : testTitle
			};

			expect(testDay.save(attrs, {
				error : function(model, response) {
					expect(response).toEqual(
							"'date' must be specified, 'date' cannot be NULL");
				}
			})).toBeFalsy("Should have failed validation");

		});

	});

});