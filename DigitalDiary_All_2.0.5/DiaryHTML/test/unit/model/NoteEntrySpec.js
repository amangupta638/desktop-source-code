/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: NoteEntrySpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Note Entry model", function() {

	var model = null,		// test model
		eventSpy = null;	// spy for events

	describe("when created with default values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.NoteEntry();

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

//		it("should set 'timestamp' to default value", function() {
//			var today = new Date();
//			expect(model.get("timestamp")).toEqual(today);
//		});

		it("should set 'text' to default value", function() {
			expect(model.get("text")).toEqual("(no entry)");
		});

		describe("when updated with valid values", function() {
			var tomorrow = new Rocketboots.date.Day().addDays(1).getDate();

			it("should set 'timestamp' to tomorrow", function() {
				model.set("timestamp", tomorrow);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("timestamp")).toEqual(tomorrow);
			});

			it("should set 'text' to 'test value'", function() {
				model.set("text", "test value");

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("text")).toEqual("test value");
			});

		});

	});

	describe("when created with valid values", function() {

		var today = new Date();

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.NoteEntry({
				"timestamp" : today,
				"text" : "This is the text."
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'timestamp' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("timestamp")).toEqual(today);
		});

		it("should set 'text' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("text")).toEqual("This is the text.");
		});

	});

	describe("when updated with invalid values", function() {

		var today = new Date();

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.NoteEntry({
				"timestamp" : today,
				"text" : "This is the text."
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should require a non-null 'timestamp'", function() {
			model.set("timestamp", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Timestamp must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("timestamp")).toEqual(today);
		});

		it("should require a valid 'timestamp' value", function() {
			model.set("timestamp", "Student");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Timestamp must be a valid timestamp");

			// confirm model attr wasn't updated
			expect(model.get("timestamp")).toEqual(today);
		});

		it("should require a non-null 'text'", function() {
			model.set("text", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Text must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("text")).toEqual("This is the text.");
		});

		it("should require a string 'text'", function() {
			model.set("text", []);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Text must be a valid string");

			// confirm model attr wasn't updated
			expect(model.get("text")).toEqual("This is the text.");
		});

		it("should require a non-empty 'text'", function() {
			model.set("text", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Text must be specified and cannot be empty");

			// confirm model attr wasn't updated
			expect(model.get("text")).toEqual("This is the text.");
		});

	});

});
