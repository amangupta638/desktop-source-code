/**
 * Unit Tests for the Calendar Model
 * 
 * author - Brian Bason
 */
describe("Calendar - Test Suite", function() {
	
	describe("Successful creation of Calendar Suite" , function() {
		
		it("should be able to create a calendar", function() {
		   
			// holds the year of the calendar
			var year = new Date().getFullYear();
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			expect(calendar.get('year')).toEqual(year);
		});
			
	});
	
	describe("Unsuccessful creation of Calendar Suite" , function() {
		
		it("should not be able to create a calendar with no year", function() {
		   
			// the test function
			var testFunction = function() {
				new diary.model.Calendar();
			};
			
			expect(testFunction).toThrow("Year must be specified, year is not optional");
		});
		
		it("should not be able to create a calendar with year string", function() {
			   
			// the test function
			var testFunction = function() {
				new diary.model.Calendar({ 'year' : "1997" });
			};
			
			expect(testFunction).toThrow("Year is not valid, year must be an integer");
		});
		
		it("should not be able to create a calendar with year negative", function() {
			   
			// the test function
			var testFunction = function() {
				new diary.model.Calendar({ 'year' : -1 });
			};
			
			expect(testFunction).toThrow("Year is not valid, year must be equal or greater than one");
		});
			
	});
	
	describe("Adding of Calendar Day to Calendar" , function() {
		
		it("should be able to add a calendar day to calendar", function() {
		   
			// holds the year of the calendar
			var year = new Date().getFullYear();
			// holds the day that is to be added
			var day = new diary.model.CalendarDay({ 'day' : new Rocketboots.date.Day() });
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			// add the day to the calendar
			calendar.add(day);
			
			// check that one day exists in the calendar
			expect(calendar.get('calendarDays').length).toEqual(1);
			
		});
		
		it("should be able to add a number of calendar days to calendar and be sorted", function() {
			   
			// holds the year of the calendar
			var year = new Date().getFullYear();
			
			// holds the day1 date
			var day1Date = new Rocketboots.date.Day();
			// prevent errors when on year boundary
			if (day1Date.getMonth() == 11 && day1Date.getDate() == 31)
				day1Date.setDate(1);
			// holds the day2 date which is 24 hours after day1
			var day2Date = new Rocketboots.date.Day(day1Date).addDays(1);
			
			// holds the days that are to be added
			var day1 = new diary.model.CalendarDay({ 'day' : day1Date });
			var day2 = new diary.model.CalendarDay({ 'day' : day2Date });
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			// add the day to the calendar
			calendar.add(day2);
			calendar.add(day1);
			
			// check that one day exists in the calendar
			expect(calendar.get('calendarDays').length).toEqual(2);
			
			// the calendar days should be properly sorted
			expect(calendar.get('calendarDays').at(0).get('day').getKey()).toEqual(day1Date.getKey());
			expect(calendar.get('calendarDays').at(1).get('day').getKey()).toEqual(day2Date.getKey());
			
		});
		
		it("should be able to replace a calendar day in the calendar", function() {
			   
			// holds the year of the calendar
			var year = new Date().getFullYear();
			
			// holds the day date
			var dayDate = new Rocketboots.date.Day();
			
			// holds the day that are to be added
			var day = new diary.model.CalendarDay({ 'day' : dayDate });
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			// add the day to the calendar
			calendar.add(day);
			
			// check that one day exists in the calendar
			expect(calendar.get('calendarDays').length).toEqual(1);
			
			// the calendar days should be properly sorted
			expect(calendar.get('calendarDays').at(0).get('day').getKey()).toEqual(dayDate.getKey());
			expect(calendar.get('calendarDays').at(0).get('cycle')).toBeNull();
			expect(calendar.get('calendarDays').at(0).get('cycleDay')).toBeNull();
			
			// modify the day
			day = new diary.model.CalendarDay({ 'day' : new Rocketboots.date.Day(dayDate),
												'cycle' : 1,
												'cycleDay' : 2 });
			
			// add the day to the calendar
			calendar.add(day);
			
			// check that one day exists in the calendar
			expect(calendar.get('calendarDays').length).toEqual(1);
			
			// the calendar days should be properly sorted
			expect(calendar.get('calendarDays').at(0).get('day').getKey()).toEqual(dayDate.getKey());
			expect(calendar.get('calendarDays').at(0).get('cycle')).toEqual(1);
			expect(calendar.get('calendarDays').at(0).get('cycleDay')).toEqual(2);
			
		});
		
		it("should not be able to add to calendar with no Calendar Day defined", function() {
			   
			// holds the year of the calendar
			var year = new Date().getFullYear();
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			// the test function
			var testFunction = function() {
				// add an object to the calendar
				calendar.add();
			};
			
			// check that the function throws an error
			expect(testFunction).toThrow("Calendar Day must be specified, day is not optional");
			
		});
	
		it("should not be able to add to calendar if not a calendar day", function() {
			   
			// holds the year of the calendar
			var year = new Date().getFullYear();
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			// the test function
			var testFunction = function() {
				// add an object to the calendar
				calendar.add({});
			};
			
			// check that the function throws an error
			expect(testFunction).toThrow("Invalid calendar day, day should be of type diary.model.CalendarDay");
			
		});
		
		it("should not be able to add to calendar if calendar day is not of same year", function() {
			   
			// holds the year of the calendar
			var year = new Date().getFullYear();
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			// the test function
			var testFunction = function() {
				// the date of the day
				var date = new Date();
				date.setFullYear(year - 1);
				
				// add an object to the calendar
				calendar.add(new diary.model.CalendarDay({ 'day' : new Rocketboots.date.Day(date) }));
			};
			
			// check that the function throws an error
			expect(testFunction).toThrow("Invalid calendar day, day is not in year " + year);
			
		});
		
	});
	
	describe("Get Calendar Days for Month from Calendar" , function() {
		
		it("should be able to get all calendar days of month from calendar", function() {
		   
			// holds the year of the calendar
			var year = 2012;
			// holds the start date of the days that are to be added
			// 1st Jan 2012
			var startDate = new Date(1325397600000);
			// holds the end date of the days that are to be added
			// 31st Mar 2012
			var endDate = new Date(1333170000000);
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			// add three months worth of days in the calendar
			for (var dayDate = startDate.getTime() ; dayDate <= endDate.getTime() ; dayDate = dayDate + 86400000) {
				// holds the day that is to be added
				var day = new diary.model.CalendarDay({ 'day' : new Rocketboots.date.Day(new Date(dayDate)) });
				// add the day to the calendar
				calendar.add(day);
			}
			
			// now we will test the calendar by trying to get Feb
			var daysOfMonth = calendar.getMonthDays(2);
			
			// check that the returned data is a Calendar Day Collection
			expect(daysOfMonth instanceof diary.collection.CalendarDayCollection).toBeTruthy();
			
			expect(daysOfMonth.length).toEqual(29);
			
			// now go through all the days and check that they truly are of feb
			_.forEach(daysOfMonth.toArray(), function(calendarDay) {
				expect(calendarDay.get('day').getMonth() + 1).toEqual(2);
			});
			
		});
		
		it("should not be able to get calendar days for month equal to 0", function() {
			   
			// holds the year of the calendar
			var year = new Date().getFullYear();
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			// the test function
			var testFunction = function() {
				// add an object to the calendar
				calendar.getMonthDays(0);
			};
			
			// check that the function throws an error
			expect(testFunction).toThrow("Invalid month, month must be between 1 and 12");
			
		});
		
		it("should not be able to get calendar days for month equal to 13", function() {
			   
			// holds the year of the calendar
			var year = new Date().getFullYear();
			
			// create the test calendar that we will use for the test
			var calendar = new diary.model.Calendar({ 'year' : year });
			
			// the test function
			var testFunction = function() {
				// add an object to the calendar
				calendar.getMonthDays(13);
			};
			
			// check that the function throws an error
			expect(testFunction).toThrow("Invalid month, month must be between 1 and 12");
			
		});
		
	});

	describe("Get calendar days for weeks in 2012", function () {
		
		var calendar = null;
		
		beforeEach(function () {
			
			// Create calendar
			calendar = new diary.model.Calendar({ 'year' : 2012 });
			
			// Fill calendar
			for (var day = new Rocketboots.date.Day(2012, 0, 1) ; day.getFullYear() == 2012 ; day.addDays(1)) {
				calendar.add(new diary.model.CalendarDay({ 'day' : new Rocketboots.date.Day(day) }));
			}
			
		});
		
		it("Should throw an error for weeks outside range", function () {
			
			expect(function () {
				calendar.getWeekDays(-1);
			}).toThrow("Invalid week, week must be between 0 and 53");
			
			expect(function () {
				calendar.getWeekDays(54);
			}).toThrow("Invalid week, week must be between 0 and 53");
			
		});
		
		it("Should return 1 day for week 0 of 2012", function () {
			expect(calendar.getWeekDays(0).length).toEqual(1);
		});
		
		it("Should return 7 days for week 1 of 2012", function () {
			expect(calendar.getWeekDays(1).length).toEqual(7);
		});
		
		it("Should return 7 days for week 52 of 2012", function () {
			expect(calendar.getWeekDays(52).length).toEqual(7);
		});
		
		it("Should return 1 day for week 53 of 2012", function () {
			expect(calendar.getWeekDays(53).length).toEqual(1);
		});
		
	});
	
	describe("Get calendar days for days in 2012", function () {
		
		var calendar = null;
		
		beforeEach(function () {
			
			// Create calendar
			calendar = new diary.model.Calendar({ 'year' : 2012 });
			
			// Fill calendar
			for (var day = new Rocketboots.date.Day(2012, 0, 1) ; day.getFullYear() == 2012 ; day.addDays(1)) {
				calendar.add(new diary.model.CalendarDay({ 'day' : new Rocketboots.date.Day(day) }));
			}
			
		});
		
		it("Should throw an error for days outside range", function () {
			
			// invalid month of year
			
			expect(function () {
				calendar.getDay(0, 1);
			}).toThrow("Invalid month, must be between 1 and 12");
			
			expect(function () {
				calendar.getDay(13, 1);
			}).toThrow("Invalid month, must be between 1 and 12");
			
			expect(function () {
				calendar.getDay(1, 1);
			}).not.toThrow("Invalid month, must be between 1 and 12");
			
			expect(function () {
				calendar.getDay(12, 1);
			}).not.toThrow("Invalid month, must be between 1 and 12");
			
			// invalid day of month
			
			expect(function () {
				calendar.getDay(1, 0);
			}).toThrow("Invalid day, must be between 1 and 31");
			
			expect(function () {
				calendar.getDay(1, 32);
			}).toThrow("Invalid day, must be between 1 and 31");
			
			expect(function () {
				calendar.getDay(1, 1);
			}).not.toThrow("Invalid day, must be between 1 and 31");
			
			expect(function () {
				calendar.getDay(1, 31);
			}).not.toThrow("Invalid day, must be between 1 and 31");
			
		});
		
		it("Should return correct day for Jan 1st of 2012", function () {
			expect(calendar.getDay(1, 1).get('day').getKey()).toEqual("20120101");
		});
		
		it("Should return correct day for Jan 31st of 2012", function () {
			expect(calendar.getDay(1, 31).get('day').getKey()).toEqual("20120131");
		});
		
		it("Should return correct day for June 16th of 2012", function () {
			expect(calendar.getDay(6, 16).get('day').getKey()).toEqual("20120616");
		});
		
		it("Should return correct day for Dec 31st of 2012", function () {
			expect(calendar.getDay(12, 31).get('day').getKey()).toEqual("20121231");
		});
		
	});

	describe("Get calendar days for weeks in 2013", function () {

		var calendar = null;

		beforeEach(function () {

			// Create calendar
			calendar = new diary.model.Calendar({ 'year' : 2013 });

			// Fill calendar
			for (var day = new Rocketboots.date.Day(2013, 0, 1) ; day.getFullYear() == 2013 ; day.addDays(1)) {
				calendar.add(new diary.model.CalendarDay({ 'day' : new Rocketboots.date.Day(day) }));
			}
		});

		it("Should throw an error for weeks outside range", function () {

			expect(function () {
				calendar.getWeekDays(-1);
			}).toThrow("Invalid week, week must be between 0 and 53");

			expect(function () {
				calendar.getWeekDays(54);
			}).toThrow("Invalid week, week must be between 0 and 53");

		});

		it("Should return 6 days for week 0 of 2013", function () {
			expect(calendar.getWeekDays(0).length).toEqual(6);
		});

		it("Should return 7 days for week 1 of 2013", function () {
			expect(calendar.getWeekDays(1).length).toEqual(7);
		});

		it("Should return 7 days for week 51 of 2013", function () {
			expect(calendar.getWeekDays(51).length).toEqual(7);
		});

		it("Should return 2 days for week 52 of 2013", function () {
			expect(calendar.getWeekDays(52).length).toEqual(2);
		});

		it("Should return 0 days for week 53 of 2013", function () {
			expect(calendar.getWeekDays(53).length).toEqual(0);
		});

	});

	describe("Get calendar days for days in 2013", function () {

		var calendar = null;

		beforeEach(function () {

			// Create calendar
			calendar = new diary.model.Calendar({ 'year' : 2013 });

			// Fill calendar
			for (var day = new Rocketboots.date.Day(2013, 0, 1) ; day.getFullYear() == 2013 ; day.addDays(1)) {
				calendar.add(new diary.model.CalendarDay({ 'day' : new Rocketboots.date.Day(day) }));
			}

		});

		it("Should throw an error for days outside range", function () {

			// invalid month of year

			expect(function () {
				calendar.getDay(0, 1);
			}).toThrow("Invalid month, must be between 1 and 12");

			expect(function () {
				calendar.getDay(13, 1);
			}).toThrow("Invalid month, must be between 1 and 12");

			expect(function () {
				calendar.getDay(1, 1);
			}).not.toThrow("Invalid month, must be between 1 and 12");

			expect(function () {
				calendar.getDay(12, 1);
			}).not.toThrow("Invalid month, must be between 1 and 12");

			// invalid day of month

			expect(function () {
				calendar.getDay(1, 0);
			}).toThrow("Invalid day, must be between 1 and 31");

			expect(function () {
				calendar.getDay(1, 32);
			}).toThrow("Invalid day, must be between 1 and 31");

			expect(function () {
				calendar.getDay(1, 1);
			}).not.toThrow("Invalid day, must be between 1 and 31");

			expect(function () {
				calendar.getDay(1, 31);
			}).not.toThrow("Invalid day, must be between 1 and 31");

		});

		it("Should return correct day for Jan 1st of 2013", function () {
			expect(calendar.getDay(1, 1).get('day').getKey()).toEqual("20130101");
		});

		it("Should return correct day for Jan 31st of 2013", function () {
			expect(calendar.getDay(1, 31).get('day').getKey()).toEqual("20130131");
		});

		it("Should return correct day for June 16th of 2013", function () {
			expect(calendar.getDay(6, 16).get('day').getKey()).toEqual("20130616");
		});

		it("Should return correct day for Dec 31st of 2013", function () {
			expect(calendar.getDay(12, 31).get('day').getKey()).toEqual("20131231");
		});

	});


});