/**
 * Unit Tests for the Class Model
 * 
 * author - John Pearce
 */
describe("Class - Test Suite", function() {
	
	/**
	 * Suite of tests which should create a class
	 */
	describe("Successful creation of Class Suite", function() {
		
		beforeEach(function() {
			
		});
		
		/*
		 * creation of a class with default values
		 */
		it("should be able to create a class with default values", function() {
			
			// create the model that we will use for the test
			var classObj = new diary.model.Class();
			
			expect(classObj.get("periodId")).toEqual(-1);
			expect(classObj.get("subject") instanceof diary.model.Subject).toBeTruthy();
			expect(classObj.get("room")).toBeNull();
			expect(classObj.get("cycleDay")).toEqual(0);
			
		});
		
		/*
		 * creation of a class with given values
		 */
		it("should be able to create a class with given values", function() {
		   
			var attrs = {
				'periodId': 1,
				'subject': new diary.model.Subject({
					'title' : "Hello world",
					'colour' : "orange"
				}),
				'room' : "Test Room",
				'cycleDay': 9
			};
			
			// create the test class that we will use for the test
			var classObj = new diary.model.Class(attrs);
			
			expect(classObj.get("periodId")).toEqual(attrs.periodId);
			expect(classObj.get("subject")).toEqual(attrs.subject);
			expect(classObj.get("room")).toEqual(attrs.room);
			expect(classObj.get("subject").get("title")).toEqual("Hello world");
			expect(classObj.get("subject").get("colour")).toEqual("orange");
			expect(classObj.get("cycleDay")).toEqual(attrs.cycleDay);
			
		});
			
	});
	
	/**
	 * Suite of tests which should stop the class from being created
	 */
	describe("Unsuccessful creation of Class Suite" , function() {
		
		// holds the attributes that are to be used for each test
		var attrs;
		// holds the subject that is to be used for testing
		var classObj = null;
		
		beforeEach(function() {
			// holds the attributes that are to be assigned during save
			attrs = {
					'periodId': 5,
					'subject': new diary.model.Subject({'title': "Test", 'colour': "yellow"}),
					'room': 'Test Room',
					'cycleDay': 3
			};
			
			// create the test diary item that we will use for the test
			classObj = new diary.model.Class();
		});
		
		/*
		 * period Id not specified
		 */
		it("should fail when class period Id is not specified", function() {

			// update the attributes for the test
			attrs.periodId = null;
			
			expect(classObj.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("No period Id specified");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
		/*
		 * subject not specified
		 */
		it("should fail when class subject is not specified", function() {

			// update the attributes for the test
			attrs.subject = null;
			
			expect(classObj.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("No subject specified");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
		/*
		 * cycle day not specified
		 */
		it("should fail when class cycle day is not specified", function() {

			// update the attributes for the test
			attrs.cycleDay = null;
			
			expect(classObj.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("No cycle day specified");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
		/*
		 * period id type
		 */
		it("should fail when class period id is not of type number", function() {
			
			// update the attributes for the test
			attrs.periodId = "string";
			
			expect(classObj.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("Period Id must be of type number");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
		/*
		 * subject type
		 */
		it("should fail when class subject is not of type Subject", function() {
			
			// update the attributes for the test
			attrs.subject = {};
			
			expect(classObj.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("Subject must be of type subject");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
		/*
		 * cycle day type
		 */
		it("should fail when class cycle day is not of type number", function() {
			
			// update the attributes for the test
			attrs.cycleDay = function () {};
			
			expect(classObj.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("Cycle day must be of type number");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
		/*
		 * cycle day positive
		 */
		it("should fail when class cycle day is not positive", function() {
			
			// update the attributes for the test
			attrs.cycleDay = -1;
			
			expect(classObj.set(attrs, {
				error : function(model, response) {
					 expect(response).toEqual("Cycle day must not be negative");
				}
			})).toBeFalsy("Should have failed to save");
			
		});
		
	});
	
});