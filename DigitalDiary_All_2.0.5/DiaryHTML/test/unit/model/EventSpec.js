/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: EventSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Event model", function() {

	var model = null,		// test model
		eventSpy = null;	// spy for events

	describe("when created with default values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Event();

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'startDay' to today", function() {
			var today = new Rocketboots.date.Day();
			expect(model.get("startDay").getKey()).toEqual(today.getKey());
		});

		it("should set 'startTime' to NULL", function() {
			expect(model.get("startTime")).toEqual(null);
		});

		it("should set 'endTime' to NULL", function() {
			expect(model.get("endTime")).toEqual(null);
		});

		it("should set 'locked' to false", function() {
			expect(model.get("locked")).toBeFalsy();
		});

		describe("when updated with valid values", function() {

			it("should set 'startDay' to '" + new Rocketboots.date.Day() + "'", function() {
				model.set("startDay", new Rocketboots.date.Day());

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("startDay").getKey()).toEqual(new Rocketboots.date.Day().getKey());
			});

			it("should set 'startDay' to '" + new Rocketboots.date.Day().addDays(1) + "'", function() {
				model.set("startDay", new Rocketboots.date.Day().addDays(1));

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("startDay").getKey()).toEqual(new Rocketboots.date.Day().addDays(1).getKey());
			});

			it("should set 'startDay' to '" + new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7) + "'", function() {
				model.set("startDay", new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7));

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("startDay").getKey()).toEqual(new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7).getKey());
			});

			it("should set 'startTime' to '" + new Rocketboots.date.Time(10, 24) + "'", function() {
				var time = new Rocketboots.date.Time(10, 24);
				model.set("startTime", time);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("startTime").toString()).toEqual(time.toString());
			});

			it("should set 'endTime' to '" + new Rocketboots.date.Time(14, 10) + "'", function() {
				var time = new Rocketboots.date.Time(14, 10);
				model.set("endTime", time);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("endTime").toString()).toEqual(time.toString());
			});

			it("should set 'locked' to 'true'", function() {
				model.set("locked", true);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("locked")).toBeTruthy();
			});

			it("should set 'locked' to 'false'", function() {
				model.set("locked", false);

				// confirm error event was not dispatched
				expect(eventSpy.callCount).toEqual(0);

				// confirm model attr was updated
				expect(model.get("locked")).toBeFalsy();
			});

		});

	});

	describe("when created with valid values", function() {

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Event({
				"startDay" : new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7),
				"startTime" : new Rocketboots.date.Time(4, 20),
				"endTime" : new Rocketboots.date.Time(20, 4),
				"locked" : true
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should set 'startDay' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			var day = new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7);
			expect(model.get("startDay").getKey()).toEqual(day.getKey());
		});

		it("should set 'startTime' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			var time = new Rocketboots.date.Time(4, 20);
			expect(model.get("startTime").toString()).toEqual(time.toString());
		});

		it("should set 'endTime' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			var time = new Rocketboots.date.Time(20, 4);
			expect(model.get("endTime").toString()).toEqual(time.toString());
		});

		it("should set 'locked' to provided value", function() {
			// confirm error event was not dispatched
			expect(eventSpy.callCount).toEqual(0);

			expect(model.get("locked")).toBeTruthy();
		});

	});

	describe("when updated with invalid values", function() {

		var startDay =  new Rocketboots.date.Day(1975, Rocketboots.date.Day.MARCH, 7),
			startTime = new Rocketboots.date.Time(4, 20),
			endTime = new Rocketboots.date.Time(20, 4);

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Event({
				"startDay" : startDay,
				"startTime" : startTime,
				"endTime" : endTime,
				"locked" : true
			});

			// create a spy for events
			eventSpy = jasmine.createSpy("EventSpy");
			model.on("error", eventSpy);
		});

		it("should require non-null 'startDay'", function() {
			model.set("startDay", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Start Day must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("startDay").getKey()).toEqual(startDay.getKey());
		});

		it("should require valid 'startDay'", function() {
			model.set("startDay", new Date());

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Start Day must be a valid Day");

			// confirm model attr wasn't updated
			expect(model.get("startDay").getKey()).toEqual(startDay.getKey());
		});

		it("should require valid 'startTime'", function() {
			model.set("startTime", new Date());

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Start Time must be a valid Time");

			// confirm model attr wasn't updated
			expect(model.get("startTime").toInteger()).toEqual(startTime.toInteger());
		});

		it("should require valid 'endTime'", function() {
			model.set("endTime", new Date());

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("End Time must be a valid Time");

			// confirm model attr wasn't updated
			expect(model.get("endTime").toInteger()).toEqual(endTime.toInteger());
		});

		it("should require non-null 'locked'", function() {
			model.set("locked", null);

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Locked must be specified and cannot be NULL");

			// confirm model attr wasn't updated
			expect(model.get("locked")).toBeTruthy();
		});

		it("should require non-empty 'locked'", function() {
			model.set("locked", "");

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Locked must be either 'true' or 'false'");

			// confirm model attr wasn't updated
			expect(model.get("locked")).toBeTruthy();
		});

		it("should require 'startTime' to be < 'endTime'", function() {
			model.set("startTime", new Rocketboots.date.Time(22, 44));

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("End Time must be after Start Time");

			// confirm model attr wasn't updated
			expect(model.get("startTime").toInteger()).toEqual(startTime.toInteger());
		});

		it("should require 'endTime' to be > 'startTime'", function() {
			model.set("endTime", new Rocketboots.date.Time(2, 44));

			// confirm error event was dispatched
			expect(eventSpy.callCount).toEqual(1);

			// confirm error message
			expect(eventSpy.mostRecentCall.args[1]).toEqual("End Time must be after Start Time");

			// confirm model attr wasn't updated
			expect(model.get("endTime").toInteger()).toEqual(endTime.toInteger());
		});

	});

	describe("when starting yesterday at 10:00", function() {

		var startDay =  new Rocketboots.date.Day().addDays(-1),
			startTime = new Rocketboots.date.Time(10, 0),
			endTime = new Rocketboots.date.Time(11, 0);

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Event({
				"startDay" : startDay,
				"startTime" : startTime,
				"endTime" : endTime
			});
		});

		// isWithinDay() specs

		it("should be within the day: yesterday", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			expect(model.isWithinDay(day)).toBeTruthy();
		});

		it("should not be within the day: two days ago", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		it("should not be within the day: today", function() {
			var day = new Rocketboots.date.Day();
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		// isWithinTimeFrame() specs

		it("should be within the time frame: yesterday, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
		});

		it("should be within the time frame: yesterday, 09:00-10:01", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(10, 01);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
		});

		it("should be within the time frame: yesterday, 10:00-11:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(10, 0);
			var endTime = new Rocketboots.date.Time(11, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeTruthy();
		});

		it("should not be within the time frame: yesterday, 09:00-10:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(10, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: yesterday, 10:20-10:40", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(10, 20);
			var endTime = new Rocketboots.date.Time(10, 40);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: two days ago, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: today, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day();
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

	});

	describe("when starting yesterday (no time)", function() {

		var startDay =  new Rocketboots.date.Day().addDays(-1);

		beforeEach(function() {
			// create a new, valid model
			model = new diary.model.Event({
				"startDay" : startDay
			});
		});

		// isWithinDay() specs

		it("should be within the day: yesterday", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			expect(model.isWithinDay(day)).toBeTruthy();
		});

		it("should not be within the day: two days ago", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		it("should not be within the day: today", function() {
			var day = new Rocketboots.date.Day();
			expect(model.isWithinDay(day)).toBeFalsy();
		});

		// isWithinTimeFrame() specs

		it("should not be within the time frame: yesterday, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-1);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: two days ago, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day().addDays(-2);
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

		it("should not be within the time frame: today, 09:00-14:00", function() {
			var day = new Rocketboots.date.Day();
			var startTime = new Rocketboots.date.Time(9, 0);
			var endTime = new Rocketboots.date.Time(14, 0);
			expect(model.isWithinTimeFrame(day, startTime, endTime)).toBeFalsy();
		});

	});

});
