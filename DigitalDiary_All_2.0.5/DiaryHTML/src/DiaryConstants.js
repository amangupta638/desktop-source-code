/**
 * @enum {string} The Template names across the application
 */
Template = {
    FRAME:  'frame'
};

/**
 * @enum {string} The Service names across the application
 */
ServiceName = {
    APPDATABASE : "diaryDatabase",                      // the application database service
    SCHEMAMANAGER : "schemaManager",                    // the manager to keep track of schema updates
    DEFAULTDATAMANAGER : "dataManager",                 // the manager for default data
    DEMODATAMANAGER : "demoManager",                    // the manager for demo data, will install all the required data for the demo
    MANIFESTMANAGER : "manifestManager",                // the manager that will control reading of the manifest file
    CONTENTITEMFORMDELEGATE: "contentItemFormDelegate", // the delegate that is to be used to get content item forms
    CONTENTPACKAGEDELEGATE: "contentPackageDelegate",   // the delegate that is to be used to get content packages
    DAYOFNOTEDELEGATE : "dayOfNoteDelegate",            // the delegate that is to be used to get day of note data
    SCHOOLPROFILEDELEGATE : "schoolProfileDelegate",    // the delegate that is to be used to get school profile data
    STUDENTPROFILEDELEGATE : "studentProfileDelegate",  // the delegate that is to be used to get student profile data
    NEWSANNOUNCEMENTDELEGATE : "announcementDelegate",  // the delegate that is to be used to get news and announcement data
    NEWSANNOUNCEMENTMANAGER	: "newsAnnouncmentManager",
    
    
    
    DIARYITEMDELEGATE : "diaryItemDelegate",            // the delegate that is to be used to get diary item data
    ATTACHMENTFILEDELEGATE : "attachmentFileDelegate",  // the delegate that is to be used to manage the attachment files
    ATTACHMENTDBDELEGATE : "attachmentDBDelegate",      // the delegate that is to be used to manage the attachment metadata in the database
    SUBJECTDELEGATE : "subjectDelegate",                // the delegate that is to be used to manage the subjects in the database
    CLASSDELEGATE : "classDelegate",                // the delegate that is to be used to manage the classes in the database
    TIMETABLEDELEGATE : "timetableDelegate",            // the delegate that is to be used to get timetable data
    DOWNLOADCONTENTDELEGATE : "downloadContentDelegate",// the delegate that is to be used to get the downloadable content
    DOCUMENTDELEGATE : "documentDelegate",              // the delegate that is to be used to query the document index
    SYSTEMCONFIGURATIONDELEGATE : "systemConfigurationDelegate", // the delegate that is to be used to manage the system configuration
    THECLOUDSERVICEDELEGATE: "theCloudServiceDelegate",     // the delegate that is used to communicate with "The Cloud"
    USAGELOGDELEGATE : "usageLogDelegate",              // the delegate that is used to manage the usage log
    TEACHERDETAILSDELEGATE : "teacherDetailsDelegate",
    
    CALENDARMANAGER : "calendarManager",                // the manager that will control the generation of a calendar in the diary
    CONTENTMANAGER : "contentManager",                  // the manager that will control the downloading of contents (articles) for the application
    CONTENTITEMFORMMANAGER: "contentItemFormManager",   // the manager that will control saving of content page form data
    DAYOFNOTEMANAGER : "dayOfNoteManager",              // the manager that will control the days of note
    DOCUMENTMANAGER : "documentManager",                // the document indexing manager
    NOTIFICATIONMANAGER : "notificationManager",        // the manager that will control the notifications of the application
    SCHOOLMANAGER : "schoolManager",                    // the manager that will take care of school details
    STUDENTMANAGER : "studentManager",                  // the manager that will take care of student details
    DIARYITEMMANAGER : "diaryItemManager",              // the manager that will control the diary items in the application
    DAYOFNOTEMANAGER : "dayOfNoteManager",              // the manager that will control the day of notes in the application
    TIMETABLEMANAGER : "timetableManager",              // the manager that will control the timetables in the application
    DRAGMANAGER : "dragManager",                        // the ui drag/drop manager
    REGISTRATIONMANAGER : "registrationManager",        // the manager that will control all aspects of application registration
    SYSTEMCONFIGURATIONMANAGER: "systemConfigurationManager", // the manager that will control the system configuration
    SEARCHMANAGER : "searchManager",                    // the manager that will search the diary
    THECLOUDSERVICEMANAGER : "theCloudServiceManager",  // the manager that will control communication with "The Cloud"
    USAGELOGMANAGER : "usageLogManager",                // the manager that will control the usage log
    CSVPARSER: "csvParser",								// CSV data parser
    TEACHERMANAGER: "teacherManager",                    // the manager that will take care of teacher details      
	SIDEARTICLE: "sideArticle",
	CLASSMANAGER: "classManager",
	SUBJECTMANAGER: "subjectManager"
};

/**
 * @enum {string} The Event names across the application
 */
EventName = {
    INITAPPLICATION : "initApplication",                // the event that is fired to init the application
    REGISTERAPPLICATION : "registerApplication",        // the event that is fired to register the application
    STARTAPPLICATION : "startApplication",              // the event that is fired to start the application
    RESETAPPLICATION: "resetApplication",               // the event that is fired to reset the application configuration and then restart

    ADDUSAGELOG : "addUsageLog",                        // the event that is fired to add a new usage log event entry
    SYNCUSAGELOG : "syncUsageLog",                      // the event that is fired to sync the usage log with the cloud

    APPENDNOTEENTRY : "appendNoteEntry",                // the event that is fired to append a note entry
    CONTENTITEMSFETCHED : "contentItemsFetched",        // the event that is fired when the content items are loaded
    CLEARSEARCHCACHE : "clearSearchCache",              // the event that is fired to invoke the clearing of the cache
    CREATEDIARYITEM : "createDiaryItem",                // the event that is fired when a new diary item is to be added
    DELETEDIARYITEM : "deleteDiaryItem",                // the event that is fired when an existing diary item is to be deleted
    EXITAPPLICATION : "exitApplication",                // the event that is fired when the application is to be closed
    OPENATTACHMENT : "openAttachment",                  // the event that is fired to open an attachment
    FETCHCONTENTITEMS : "fetchContentItems",            // the event that is fired to fetch the content items
    RELOADMANIFEST: "reloadManifest",                   // the event that is fired to re-fetch the manifest file
    CONTENTITEMSRELOADED: "contentItemsReloaded",       // the event that is fired when the content items are reloaded
    FETCHDIARYITEMS : "fetchDiaryItems",                // the event that is fired to fetch the diary items for the scholastic year
    LOADMANIFEST : "loadManifest",                      // the event that is fired to load the manifest file contents
    LOADSCHOOLPROFILE : "loadSchoolProfile",            // the event that is fired to load the school profile
    FETCHNEXTCONTENTPACKAGE: "fetchNextContentPackage", // the event that is fired to fetch the next content package in the queue
    FETCHSTUDENTPROFILE : "fetchStudentProfile",        // the event that is fired to fetch the student profile
    FETCHSUBJECTS : "fetchSubjects",                    // the event that is fired to fetch the subjects in the diary
    FETCHSCHOOLCONFIGURATION : "fetchSchoolConfiguration",  // the event that is fired to fetch the school configuration of the diary
    FETCHSYSTEMCONFIGURATION : "fetchSystemConfiguration",  // the event that is fired to fetch the system configuration of the diary
    INBOXITEMS : 'fetchInboxItems',
    FETCHNEWSANNOUNCEMENT : 'fetchNewsAnnouncements',
    SENDMESSAGE : 'sendMessage',
    
    DATACONTENT : "dataContents",
    PERIODSYNC:"periodSyncs",
	TIMETABLESYNC:"timetablesyncs",  
    RENDERCONTENT : "renderContents",
    CHECKSCHOOLID:"checkSchoolIds",
 
    
    UPDATESTUDENTPROFILE : "updateStudentProfile",      // the event that is fired to update the student profile
    FETCHSCHOOLPROFILE : "fetchSchoolProfile",          // the event that is fired to fetch a school profile
    NEWNOTIFICATION : "notification",                   // the event that is fired when a new notification is generated
    NOTEENTRYAPPENDED : "noteEntryAppended",            // the event that is fired when a note entry is appended
    CHROMEDRAGGABLE : "chromeDraggable",                // the event that is fired when the chrome indicates to the container it can be dragged
    GENERATECALENDAR : "generateCalendar",              // the event that is fired when a calendar is to be generated for the scholastic year
    GENERATETIMETABLE : "generateTimeTable",			// the event that is fired when a time table is to be generated for the scholastic year
    UPDATEDIARYITEM : "updateDiaryItem",                // the event that is fired when an existing diary item is to be updated
    UPDATEDIARYITEMONDROP : "updateDiaryItemOnDrop",    // the event that is fired when an existing diary item is to be updated after a drop 
    VIEWMONTH: "viewMonth",                             // the event that is fired to indicate a switch to month view for a particular month
    VIEWDAY: "viewDay",                                 // the event that is fired to indicate a switch to day view for a particular day
    REFRESHCALENDAR: "refreshCalendar",                 // the event that is fired to refresh the current open calendar
    REFRESHDIARYITEMS: "refreshDiaryItems",             // the event that is fired to refresh the diary items
    CREATECLASS: "createClass",                         // the event that is fired to create a new class
    CLASSCREATED: "classCreated",                       // the event that is fired when a class is created
    DELETECLASS: "deleteClass",                         // the event that is fired to delete an existing class
    CLASSDELETED: "classDeleted",                       // the event that is fired when a class is deleted
    UPDATECLASS: "updateClass",                         // the event that is fired to update an existing class
    CLASSUPDATED: "classUpdated",                       // the event that is fired when a class is updated  
    UPDATESUBJECT: "updateSubject",                     // the event that is fired to update an existing subject
    UPDATESUBJECTFAILED: "updateSubjectFailed",         // the event that is fired to indicate that the update of a subject failed
    REFRESHSUBJECTS: "refreshSubjects",                 // the event that is fired to indicate a refresh of the subjects view
    SEARCH: "search",                                   // the event that is fired to start a search query
    SEARCHCOMPLETE: "searchComplete",                   // the event that is fired when a search is completed
    REGISTERTOKEN: "registerToken",                     // the event that is fired to request a token to be registered
    REGISTERUSER: "registerUser", 
    TOKENREGISTERED: "tokenRegistered",                 // indicates successful token registration
    TOKENREGISTRATIONFAILED: "tokenRegistrationFailed", // indicates failed token registration
    USERREGISTERED: "userRegistered",					// indicates successful user registration
    USERREGISTRATIONFAILED: "userRegistrationFailed",	// indicates failed user registration
    UPDATEREGISTRATIONKEYPRESSED: "updateRegistrationKeyPressed", // indicates that the update token key combination was activated
    NETWORKSTATUSCHANGE: "networkStatusChange",          // indicates a change in the internet availability
    FETCHTEACHERDETAILS: "fetchTeacherDetails"			// the event that is fired to fetch a teacher
};

/**
 * @enum {string} The Model across the application
 */
ModelName = {
    APPLICATIONSTATE : "applicationState",              // the model for the general state of the application
    SCHOOLPROFILE : "school",                           // the model for the school profile
    STUDENTPROFILE : "student",                         // the model for the student profile
    NEWSANNOUNCEMENTS : "newsAnnouncements",            // the model for the news and announcements
    INBOXITEMS : "inboxItems",            				// the model for the inbox Items
    DIARYITEMTYPES : "diaryItemTypes",					// the model for the diary item types
    CALENDAR : "calendar",                              // the model for the calendar
    DIARYITEMS : "diaryItems",                          // the model for the diary items for the scholastic year
    MANIFEST : "manifest",                              // the model for the application manifest file
    CONTENTPACKAGEQUEUE: "contentPackageUpdateQueue",   // the model for the queue of content packages that need to be updated
    SUBJECTS : "subjects",                              // the model for the subjects for the scholastic year
    SYSTEMCONFIGURATION : "systemConfiguration",        // the model for the system configuration of the application
    TIMETABLE: "timetable",                             // the model for the timetable
    LASTUPDATE: "lastUpdate"       ,                      // the model for the LastUpdate
	RENDERCONTENTMODEL: "rendercontentmodels",
	CLASSUSERS: "classUsers",                            // the model for the class users   
	CLASSES : "classes",
	ALLCLASSES : "allClasses",
	GRADES : "grades",
	STATES : "states",                          
	COUNTRY : "country",
	REPORTS : "reports",
	PARENTS : "parents"
};


/**
 * @enum {number|string} Other system settings across the application 
 */
SystemSettings = {
    HARDCODDEDYEAR: 2014,                           // locking the diary to a particular year
    CONTENTDOWNLOADRETRYINTERVAL: 60000,            // the number of milliseconds to wait before trying to download a content package again
    USAGELOGSYNCINTERVAL : 6000,                    // the number of milliseconds to wait between running the command
    MAXUSAGELOGLENGTH : 1000,
    FADEANIMATIONDURATION: 400,
    TIMEMARKERDELAY: 10000,                         // the number of milliseconds to wait before update the time marker on the Day View (set to 5 min * 60 * 1000 / TIMEINCREMENTHEIGHT to move marker by only 1 pixel each time)
    SPLASHDELAY : 3000,                             // the number of milliseconds to wait before dismissing the splash screen
    SPLASHDELAY_DEMOMODE : 30000,                   // the number of milliseconds to wait before dismissing the splash screen when running in demo mode
    NOTIFICATIONDELAY : 30000,                      // the number of milliseconds to wait before dismissing a notification
    SERVERURL : "http://54.253.121.67",
    SERVERIMAGEURL : "http://54.253.121.67",
    UPDATEURL : "",
    DEBUG : false,                                  // whether to allow logging (trace and optionally the AIR Introspector). If false, console.log becomes a no-op.
    DEBUGCONSOLE : false,                           // whether to include the AIR Introspector (accessible via Apple+Shift+1 shortcut)
    DEBUGCONSOLELOGALL : false                      // whether to log everything to the AIR Introspector
};


/**
 * Constants defined for displaying about daisy, educational content and school information 
 */
ContentSetting = {
	ABOUTDAISYDIR: "content/AboutDaisy",
	ABOUTDAISYIMGDIR: "content/AboutDaisy/images",
	ABOUTDAISYPACKAGEID: "AboutDaisy",
	USERROLESTUDENT:	"student",
	USERROLETEACHER:	"teacher",
	EDUCATIONALPACKAGEID: "EducationalContent",
	EDUCATIONALJRPACKAGEID: "EducationalContentJr",
	EDUCATIONALSRPACKAGEID: "EducationalContentSr",
	EDUCATIONALELEMPACKAGEID: "EducationalContentElem",
	CATHOLICPULLBARPACKAGEID: "CatholicPullbar",
	EMPOWERJRPULLBARPACKAGEID: "EmpowerPullbarJr",
	EMPOWERSRPULLBARPACKAGEID: "EmpowerPullbarSr",
	EMPOWERELEMPULLBARPACKAGEID: "EmpowerPullbarElem",
	ENABLEEMPOWER: 1,
	ENABLECATHOLIC: 0,
	SCHOOLINFOPACKAGEID: "SchoolInformation",
	EMPOWERJUNIOR: "Junior",
	EMPOWERSENIOR: "Senior",
	EMPOWERELEM: "Elementary",
	ABOUTDAISYHOMEPAGE: "aboutDaisyT.html",
	ABOUTDAISYHOMEPAGESTUDENT: "aboutDaisyS.html",
	ABOUTDAISYHOMEPAGETEACHER: "aboutDaisyT.html"	
};

/**
 * Constants defined directory for user profile image  
 */
UserProfileImage = {
		USERPROFILEIMAGEDIR: "UserProfileImages"
}