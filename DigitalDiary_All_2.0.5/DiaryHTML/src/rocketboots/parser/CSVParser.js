/**
 * $Id: CSVParser.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.parser) === "undefined") {
	Rocketboots.parser = {};
}

Rocketboots.parser.CSVParser = (function() {

	var parser = function() {
	};

	/**
	 * Parses the first line from CSV data, taking into account escaped quotes and whatnot
	 * @param {string} s 
	 * @param {string} sep Field separator (Defaults to comma)
	 * @return {array} fields parsed from s
	 */
	function parseFirstCSVLine(s,sep) {
		var entireFile = parseCSVFile(s, sep);
		return entireFile[0];
	}

	/**
	 * Parses the CSV data, taking into account escaped quotes and whatnot
	 * @param {string} s 
	 * @param {string} sep Field separator (Defaults to comma)
	 * @return {array} each line from the CSV data, each line being an array of fields parsed
	 */
	function parseCSVFile(s,sep) {
		// modified from http://stackoverflow.com/questions/1155678/javascript-string-newline-character
		var universalNewline = /\r\n|\r|\n/g;
		var a = s.split(universalNewline);
		for(var i in a){
			for (var f = a[i].split(sep = sep || ","), x = f.length - 1, tl; x >= 0; x--) {
				if (f[x].replace(/"\s+$/, '"').charAt(f[x].length - 1) == '"') {
					if ((tl = f[x].replace(/^\s+"/, '"')).length > 1 && tl.charAt(0) == '"') {
						f[x] = f[x].replace(/^\s*"|"\s*$/g, '').replace(/""/g, '"');
					  } else if (x) {
					f.splice(x - 1, 2, [f[x - 1], f[x]].join(sep));
				  } else f = f.shift().split(sep).concat(f);
				} else f[x].replace(/""/g, '"');
			  } a[i] = f;
		}
//		console.log('parseCSVFile()');
//		console.log(a);
		return a;
	}
	
	/**
	 * Parses CSV data, returns an array of object with the keys defined in expectedColumns
	 * @param {string} csvdata
	 * @param {array} expectedColumns an array of strings of known column names
	 * @return {array} 
	 */
	parser.prototype.parseCSV = function(csvdata, expectedColumns) {
		
//		console.log('parseCSV()');
//		console.log(csvdata);
		
		var hasHeaders = false,
			records = parseCSVFile(csvdata),
			fieldIndexes = {},
			capitalExpectedColumns = [],
			results = [];
		
		for(var columnIndex in expectedColumns) {
			var column = expectedColumns[columnIndex];
			capitalExpectedColumns.push(column.toUpperCase());
		}
		
//		console.log('expected cols=');
//		console.log(capitalExpectedColumns);
		
		for(var recordIndex in records) {
			var record = records[recordIndex];
			
//			console.log('record = ');
//			console.log(record);
			
			// skip over any empty lines
			if (record.length == 0 || (record.length == 1 && record[0] == '')) continue;
			
			if (!hasHeaders) {
			
				for(var i=0; i<record.length; i++) {
				
					var thisField = record[i].toUpperCase(),
						foundIndex = capitalExpectedColumns.indexOf( thisField );
						
					if (foundIndex == -1) {
						throw new Error("Unknown column " + thisField);
					}
			
//					console.log(' Setting Header = ' + thisField + ' index = ' + i );
					fieldIndexes[ thisField ] = i;
				}
			
				hasHeaders = true;
				continue;
			}
			
			var thisItem = {};
			
			for(var columnIndex in expectedColumns) {
				var column = expectedColumns[columnIndex];
				
				var thisIndex = fieldIndexes[column.toUpperCase()];
				if (isNaN(thisIndex)) continue;

//				console.log(' Setting ' +column +  ' = ' + record[thisIndex]);
				
				thisItem[column] = record[thisIndex];
			}
			
			results.push(thisItem);
		}
		
		return results;
	};
	
	/**
	 * Convert a string value DD/MM/YYYY into a Date
	 * @param {string} value A date in the string format DD/MM/YYYY
	 * @return {Rocketboots.date.Day} The date representation of the string
	 */
	parser.prototype.convertToDate = function(value) {
		// TODO: find library to handle any date format string
		var source = value.split("/");
		return new Rocketboots.date.Day(parseInt(source[2], 10), parseInt(source[1], 10)-1, parseInt(source[0], 10));
	};
	
	/**
	 * Convert a string in the format HH:MM into a Rocketboots.date.Time object.
	 * @param {String} value A time string in the format HH:MM
	 * @return {Rocketboots.date.Time} A date represnting the time value on baseDate
	 */
	parser.prototype.convertToTime = function(value) {
	
		var timeParts = value.split(' '),
			hourMinutes = timeParts[0].split(':'),
			hour = parseInt(hourMinutes[0], 10),
			minutes = parseInt(hourMinutes[1], 10),
			amPm = timeParts[1],
			isPm = typeof(amPm) == "string" && amPm.toUpperCase() == 'PM';
	
		if (isNaN(hour)) {
			return null;
		}
		
		if (isPm && hour < 12) {
			hour += 12
		}
		else if (!isPm && hour == 12) {
			hour -= 12;
		}

		return new Rocketboots.date.Time(hour, minutes);
		
	};
	
	
	/**
	* Given an array of Objects, convert the property field on each Object from a String date to Rocketboots.date.Day
	* @param {array} array An array of objects, each containing field
	* @param {String} field The string property on each object to convert to a date using the convertToDate function.  The existing property will be modified
	*/
	parser.prototype.convertArrayFieldToDate = function(array, field) {
		for(var i=0; i<array.length; i++) {
			if (array[i][field] != undefined) {
				array[i][field] = this.convertToDate(array[i][field]);
			}
		}
	};
	
	/**
	* Given an array of Objects, convert the property field on each Object from a String time to Rocketboots.date.Time
    * @param {array} array An array of objects, each containing field
	* @param {String} field The string property on each object to convert to a date using the convertTimeToDate function.  The existing property will be modified
	*/
	parser.prototype.convertArrayFieldToTime = function(array, field) {
		for(var i=0; i<array.length; i++) {
			if (array[i][field] != undefined) {
				array[i][field] = this.convertToTime(array[i][field]);
			}
		}
	};

	return parser;

})();