/**
 * $Id: Time.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 *
 * Represent the time part for a particular day
 *
 * author - Brian Bason
 */
if (typeof(Rocketboots) === "undefined") {
    Rocketboots = {};
}

if (typeof(Rocketboots.date) === "undefined") {
    Rocketboots.date = {};
}

/**
 * Defines the Time class
 * @constructor
 */
Rocketboots.date.Time = (function() {

    // determines if the given value is a valid hour or minute
    // that is if it is an integer
    function isPositiveInteger(value) {
        return (/^\d*$/.test(value));
    }

    // converts the given value to a string, placing leading zeros
    // if required
    function toString(value) {
        var parsedValue;

        parsedValue = value.toString(10);
        return ((parsedValue.length == 1) ? "0" + parsedValue : parsedValue);
    }

    // parses the given string time to a Time object
    function fromString(strTime) {
        // holds the hour and minute of the given
        // time value
        var hour,
            minute,
            separatorIndx = strTime.indexOf(':');


        // try to parse the string
        if ((separatorIndx == -1 && strTime.length != 4) || (separatorIndx != -1 && strTime.length < 3 || strTime.length > 5)) {
            throw new Error("Time is not valid, time must be in the format HHmm or HH:mm");
        }

        // read the hour and the minute part of the
        // time
        if (separatorIndx != -1) {
            // we have a separator so we will use it to
            // split the hour and the min
            hour = strTime.substring(0,separatorIndx);
            minute = strTime.substring(separatorIndx + 1);
        } else {
            // read the hours and minutes
            hour = strTime.substring(0,2);
            minute = strTime.substring(2);
        }

        // return the time object for the given time string
        return new Rocketboots.date.Time(hour, minute);
    }

    // parses the given integer time to a Time object
    function fromInteger(intTime) {

        // holds the hour and minute of the given
        // time value
        var hour,
            minute;

        // check that the time is an integer
        if(!isPositiveInteger(intTime.toString(10))) {
            throw new Error("Time is not valid, time must be a positive integer value");
        }

        // read the hour and the minute part of the
        // time
        hour = Math.floor(intTime / 100);
        minute = intTime - (hour * 100);

        // return the time object for the given time string
        return new Rocketboots.date.Time(hour, minute);

    }

    /**
     * Creates a new Time.  Can be created in the following formats
     *
     * Create a Time for specified time
     * 		var time = new Rocketboots.date.Time(23, 2);
     *
     * Create a Time from strings
     * 		var time = new Rocketboots.date.Time("23", "2");
     *
     * Create a Time with current time
     *      var time = new Rocketboots.date.Time();
     *
     * Create a Time with only the hour will default the minute to 0
     *      var time = new Rocketboots.date.Time(10);
     *
     * @constructor
     *
     * @param hour The hour of the time in 24 hour format as either a String or an Integer value
     * @param minute The minute of the time as either a String or an Integer value
     */
    var timeObj = function(hour, minute) {

        // holds the parsed values of the hours and minutes
        // of the specified values
        var parsedHour,
            parsedMinute,
            today;

        // default to current hour and minute
        if (hour == undefined && minute == undefined) {
        	today = new Date();
        	hour = today.getHours();
        	minute = today.getMinutes();
        }

        // try to parse the hour
        if (typeof(hour) === "string" || hour instanceof String) {

            // determine if the given hour is valid
            if (!isPositiveInteger(hour)) {
                throw new Error("Hour is not valid, hour must be a positive integer value");
            }
            parsedHour = parseInt(hour, 10);

        } else if (typeof(hour) === "number") {

            // determine if the given hour is valid
            if (!isPositiveInteger(hour.toString(10))) {
                throw new Error("Hour is not valid, hour must be a positive integer value");
            }
            parsedHour = hour;

        } else {
            throw new Error("Hour is not valid, hour must be a string or integer value");
        }

        // try to parse the minute
        if (typeof(minute) === "string" || minute instanceof String) {

            // determine if the given minute is valid
            if (!isPositiveInteger(minute)) {
                throw new Error("Minute is not valid, minute must be a positive integer value");
            }
            parsedMinute = parseInt(minute, 10);

        } else if (typeof(minute) === "number") {

            // determine if the given minute is valid
            if (!isPositiveInteger(minute.toString(10))) {
                throw new Error("Minute is not valid, minute must be a positive integer value");
            }
            parsedMinute = minute;

        } else {

            parsedMinute = 0;

        }

        // check that a valid hour was specified
        if (parsedHour < 0 || parsedHour > 23) {
            throw new Error("Hour is not valid, hour must be between 0 and 23");
        }

        // check that a valid minute was specified
        if (parsedMinute < 0 || parsedMinute > 59) {
            throw new Error("Minute is not valid, minute must be between 0 and 59");
        }

        // all good we can accept the given
        // values
        this.hour = parsedHour;
        this.minute = parsedMinute;

    };

    /**
     * Parses the given string, integer time or Date to a time object. The time string must be in the
     * format HHmm (i.e. 24 hour format), the time integer must be a positive value
     *
     * @param time The time as a string (format HHmm), integer (positive value) or Date that is to be parsed
     */
    timeObj.parseTime = function(time) {

        // holds the parsed time
        var parsedTime;

        // check that a valid time was given
        if (time == undefined) {
            throw new Error("Time must be specified, time cannot be undefined");
        }

        // try to parse the given time
        if (typeof(time) === "string" || time instanceof String) {

            // parse the time as a string
            parsedTime = fromString(time);

        } else if (typeof(time) === 'number') {

            // parse the time as an integer
            parsedTime = fromInteger(time);

        } else if (time instanceof Date) {

            // parse the date object to a time object
            parsedTime = new Rocketboots.date.Time(time.getHours(), time.getMinutes());

        } else {
            throw new Error("Time is not valid, time must be a string, integer or Date");
        }

        // return the time object for the given time string or integer value
        return parsedTime;

    };

    /**
     * Returns the Time object as a string in the format HHmm
     */
    timeObj.prototype.toString = function() {
        return toString(this.hour) + toString(this.minute);
    };

    /**
     * Returns the Time object as an integer value
     *
     * @return The time object as an integer value representation
     */
    timeObj.prototype.toInteger = function() {
        return ((this.hour * 100) + this.minute);
    };

    /**
     * Returns the Time object as a date class with the date set to Jan 1 1970
     *
     * @return The time object as a date representation
     */
    timeObj.prototype.toDate = function() {
        return new Date("Jan 1, 1970 " + this.hour + ":" + this.minute + ":00");
    };

    /**
     * Returns a new instance of the Time object have a time equal to
     * the time on the current time instance plus the minutes difference
     */
    timeObj.prototype.addMinutes = function(minutes) {

        var hour,
            minute;

        hour = this.hour + (minutes > 0  ? Math.floor(minutes / 60) : Math.ceil(minutes / 60));
        minute = this.minute + (minutes % 60);

        if (minute >= 60) {
            hour += 1;
            minute -= 60;
        } else if (minute < 0) {
			hour -= 1;
			minute += 60;
		}

        if (hour >= 24) {
            hour -= 24;
        } else if (hour < 0) {
			hour += 24;
		}

        return new Rocketboots.date.Time(hour, minute);
    };

    /**
     * Gets the Hour part of the time
     */
    timeObj.prototype.getHour = function() {
        return this.hour;
    };

    /**
     * Gets the Minute part of the time
     */
    timeObj.prototype.getMinute = function() {
        return this.minute;
    };

    // return the time object
    return timeObj;

})();