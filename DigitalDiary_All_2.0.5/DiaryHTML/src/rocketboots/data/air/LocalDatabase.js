/**
 * $Id: LocalDatabase.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.data) === "undefined") {
	Rocketboots.data = {};
}

if (typeof(Rocketboots.data.air) === "undefined") {
	Rocketboots.data.air = {};
}

Rocketboots.data.air.LocalDatabase = (function() {
	
	/**
	  * Creates a new LocalDatabase
	  * @constructor
	  */
	var service = function() {
		this.cachedConnections = {};
	};
	
	/**
	 * Open and retrieve the SQLConnection to the local SQLLite database file in async mode.
	 * @param {Rocketboots.data.air.LocalDatabase} service The service manager
	 * @param {air.File} dbFile File representing the SQLLite database to create/open
	 * @param {function(air.SQLConnection)} successCallback Success callback once the database connection is established
	 * @param {function()} errorCallback Error callback if the database connection could not be established
	 * @private
	 */
	function connectionAsync(service, dbFile, successCallback, errorCallback) {

		var connectionCacheKey = dbFile.nativePath,
			cachedConnection;

		// If the database has been dropped (via file deletion), clear its cache
		if (!dbFile.exists) {
			delete service.cachedConnections[connectionCacheKey];
		}

		cachedConnection = service.cachedConnections[connectionCacheKey];

		if (typeof(cachedConnection) !== "undefined") {
			successCallback(cachedConnection);
			return;
		}
		
		var connection = new air.SQLConnection();
		connection.addEventListener(air.SQLEvent.OPEN, connectionOpened);
		connection.addEventListener(air.SQLErrorEvent.ERROR, connectionError);
		
		/**
		 * Database connection opened successfully
		 * @private
		 */
		function connectionOpened(event) {
			connection.removeEventListener(air.SQLEvent.OPEN, connectionOpened);
			connection.removeEventListener(air.SQLErrorEvent.ERROR, connectionError);

			service.cachedConnections[connectionCacheKey] = connection;
			successCallback(connection);
		}
		
		/**
		 * Database connection unable to be opened
		 * @private
		 */
		function connectionError(event) {
			connection.removeEventListener(air.SQLEvent.OPEN, connectionOpened);
			connection.removeEventListener(air.SQLErrorEvent.ERROR, connectionError);
		
			errorCallback();
		}
	
		connection.openAsync(dbFile);
		
	};
	
	/**
	 * Open and retrieve the SQLConnection to the local SQLLite database file in sync mode. 
	 * @param {Rocketboots.data.air.LocalDatabase} service The service manager
	 * @param {air.File} dbFile File representing the SQLLite database to create/open
	 * 
	 * @return {air.SQLConnection} The connection to the DB
	 * @private
	 */
	function connectionSync(service, dbFile) {

		var connection,
			connectionCacheKey = dbFile.nativePath,
			cachedConnection;

		// If the database has been dropped (via file deletion), clear its cache
		if (!dbFile.exists) {
			delete service.cachedConnections[connectionCacheKey];
		}

		cachedConnection = service.cachedConnections[connectionCacheKey];
		
		// check if we have the connection already to the specified file
		// in the cache if not we will have to create one
		// Also handle the case where the DB has been dropped (via deleting the file)
		if (typeof(cachedConnection) !== "undefined") {
			// the connection is already cached
			connection = cachedConnection;
		} else {
			// create the connection
			connection = new air.SQLConnection();
			// open and cache the connection
			connection.open(dbFile);
			service.cachedConnections[connectionCacheKey] = connection;
		}
		
		// return the connection to the DB
		return connection;
	};
	
	/**
	 * Add parameters to a SQL statement object.
	 * @param {!Object} parameters The object map of parameters to add to the SQL statement
	 * @param {!air.SQLStatement} statement The SQL statement to be updated with the parameters.
	 * @return {air.SQLStatement} The statement that was passed in
	 */
	function applyParametersToStatement(statement, parameters) {
		if (parameters) {
			_.forEach(parameters, function (value, key) {
				statement.parameters[':' + key] = value;
			});
		}
		return statement;
	}
	
	/**
	  * Run a SQL statement against a SQLConnection in async mode.
	  * @param {string} text A SQL statement to execute
	  * @param {?Object} parameters An object map of parameters for the SQL query
	  * @param {air.SQLConnection} sqlConnection A SQLConnection as returned by connection()
	  * @param {function()} successCallback Called once all sql statements in scripts have been run
	  * @param {function(boolean)} errorCallback Called if error while executing sql statements. Boolean indicates if all rollbacks were executed successfully.
	  * @private
	  */
	function runQueryAsync(text, parameters, sqlConnection, successCallback, errorCallback) {
		var statement = new air.SQLStatement();
		
		if (typeof text !== "string") {
			console.log("LocalDatabase.runQueryAsync - Cannot run query on non-textual element \"" + text + "\"");
			throw new Error("Cannot run query on non-textual element \"" + text + "\"");
		}
		
		statement.text = text;
		statement.sqlConnection = sqlConnection;
		statement.addEventListener(air.SQLEvent.RESULT, success);
		statement.addEventListener(air.SQLErrorEvent.ERROR, error);
		applyParametersToStatement(statement, parameters);
		
		console.log('Running SQL Async: ', text, parameters);
		statement.execute();
		
		function success() {
			cleanupEventListeners();
			successCallback(statement);
		}
		
		function error() {
			cleanupEventListeners();
			errorCallback(statement);
		}
		
		function cleanupEventListeners() {
			statement.removeEventListener(air.SQLEvent.RESULT, success);
			statement.removeEventListener(air.SQLErrorEvent.ERROR, error);
		}
	};
	
	/**
	  * Run a SQL statement against a SQLConnection in sync mode
	  * 
	  * @param {string} text A SQL statement to execute
	  * @param {?Object} parameters An object map of parameters for the SQL query
	  * @param {air.SQLConnection} sqlConnection A SQLConnection as returned by connection()
	  * 
	  * @return {air.SQLStatement} The statement that was executed
	  * @private
	  */
	function runQuerySync(text, parameters, sqlConnection) {
		
		/** @type{air.SQLStatement} */
		var statement = new air.SQLStatement();
		
		if (typeof text !== "string") {
			console.log("LocalDatabase.runQuerySync - Cannot run query on non-textual element \"" + text + "\"");
			throw new Error("Cannot run query on non-textual element \"" + text + "\"");
		}
		
		statement.text = text;
		statement.sqlConnection = sqlConnection;
		applyParametersToStatement(statement, parameters);
		//console.log('Running SQL Sync: ', text, parameters);
		
		try {
		
			// execute the query
			statement.execute();
			// get the result set for the query
			return statement;
			
		} catch (error) {
			// log the error so that we can keep track of the problem
			//console.log("Error while trying to execute query against database");
			//console.log(error);

			// can't just throw the error because it is an Air error - go figure???
			throw new Error(error.message);
		}
		
	};
	
	/**
	  * Run a series of SQL statements in async mode.  Supports separate programatic rollback where a SQL statement fails.
	  * @param {Array.<Object.<string, string>>} scripts An array of objects containing keys sql and rollback, for example
	  * 	[
	  *			{
	  *				sql: "CREATE TABLE INFO (schemaVersion int)",
	  *				rollback: "DROP TABLE INFO"
	  *			},
	  *			{
	  *				sql: "INSERT INTO INFO (schemaVersion) values (:value)",
	  *				params: { value : 1 },
	  *				rollback: "DELETE FROM INFO WHERE schemaVersion = :value"
	  *			}
	  *		]
	  * @param {air.SQLConnection} sqlConnection A SQLConnection as returned by connection()
	  * @param {function()} successCallback Called once all sql statements in scripts have been run
	  * @param {function(boolean)} errorCallback Called if error while executing sql statements. Boolean indicates if all rollbacks were executed successfully.
	  * @private
	  */
	function runScriptsAsync(scripts, sqlConnection, successCallback, errorCallback) {

		runDBScripts(scripts, sqlConnection, successCallback, errorCallback);
		
		/**
		 * Executes a single step within a series of SQL statements
	 	 * @param {Array.<Object.<string, string>>} scripts An array of objects containing keys sql and rollback
		 * @param {air.SQLConnection} sqlConnection A SQLConnection as returned by connection()
		 * @param {function()} successCallback Called once all sql statements in scripts have been run
		 * @param {function(boolean)} errorCallback Called if error while executing sql statements. Boolean indicates if all rollbacks were executed successfully.
		 * @param {number=} step Step index to execute. Defaults to 0
		 * @private
		 */
		function runDBScripts(script, sqlConnection, successCallback, errorCallback, step) {
		
			if (typeof(step) === "undefined") {
				step = 0;
			}
			
			if (step >= script.length) {
				return successCallback();
			}
			
			var thisStep = script[step];
			
			//console.log('Running SQL Async step ' + step + ' of ' + script.length);
			runQueryAsync(thisStep.sql, thisStep.params, sqlConnection, stepComplete, stepError);
			
			function stepComplete() {
				// Introduce a defer statement to prevent a stack overflow upon running lots of scripts!
				// This is valid as the call is a async call anyway.
				_.defer(runDBScripts, script, sqlConnection, successCallback, errorCallback, ++step);
			}
			
			function stepError(event) {
				//console.log('Error running SQL step ' + step + ': ' + thisStep.sql);
				//console.log(event);
				
				if (step == 0) {
					return errorCallback(true);
				}
				
				rollbackStep(--step);
			}
			
			function rollbackStep(step) {
				thisStep = script[step];
				
				if (typeof(thisStep.rollback) === "undefined" || thisStep.rollback == "") {
					//console.log('Nothing to roll =back for SQL step ' + step);					
					return rollbackStepComplete();
				}
	
				//console.log('Rolling back SQL step ' + step);					
				runQueryAsync(thisStep.rollback, thisStep.params, sqlConnection, rollbackStepComplete, rollbackStepError);
		
			}
			
			function rollbackStepComplete() {
				if (step == 0) {
					return errorCallback(true);
				}
				
				rollbackStep(--step);
			}
			
			function rollbackStepError(event) {
				//console.log('Error running Rollback SQL step ' + step + ': ' + thisStep.rollback);
				//console.log(event);
				return errorCallback(false);
			}		
		}
		
	};
	
	/**
	  * Run a series of SQL statements in sync mode.  Supports separate programatic rollback where a SQL statement fails.
	  * @param {Array.<Object.<string, string>>} scripts An array of objects containing keys sql and rollback, for example
	  * 	[
	  *			{
	  *				sql: "CREATE TABLE INFO (schemaVersion int)",
	  *				rollback: "DROP TABLE INFO"
	  *			},
	  *			{
	  *				sql: "INSERT INTO INFO (schemaVersion) values (:value)",
	  *				params: { value : 1 },
	  *				rollback: "DELETE FROM INFO WHERE schemaVersion = :value"
	  *			}
	  *		]
	  * @param {air.SQLConnection} sqlConnection A SQLConnection as returned by connection()
	  * @private
	  */
	function runScriptsSync(scripts, sqlConnection) {
		
		// go through each of the given scripts and execute each one of them
		// if we encounter a problem we will roll back all the changes, if we
		// can
		for (var step = 0 ; step < scripts.length ; step++) {
			
			// holds the current step that is being executed
			var thisStep = scripts[step];
			
			try {
			
				// run the query
				//console.log('Running SQL Sync step ' + step);
				runQuerySync(thisStep.sql, thisStep.params, sqlConnection);
			
			} catch (error) {
				
				// log the error
				console.log('Error running SQL step ' + step + ': ' + thisStep.sql);
				console.log(error);
				
				// roll back all the changes that have been performed
				if (step != 0) {
					rollback(step - 1);
				}
				
				// throw the error back to the calling
				// method
				throw error;
			}
		}
		
		/**
		 * Roll backs all the statements from the given step
		 * 
		 * @param {long} fromStep The first step that is to be rolled back
		 * 
		 * @private
		 */
		function rollback(fromStep) {
			
			// go through each of the executed scripts and execute the roll back
			// command
			for (var step = fromStep ; step >= 0 ; step--) {
				
				// holds the current step that is being rolled back
				var thisStep = scripts[step];
				
				try {
					
					// try to roll back
					if (typeof(thisStep.rollback) === "undefined" || thisStep.rollback == "") {
						console.log('Nothing to roll back for SQL step ' + step);
					} else {
						console.log('Rolling back SQL step ' + step);					
						runQuerySync(thisStep.rollback, thisStep.params, sqlConnection);
					}
					
				} catch (error) {
					
					// log the error
					//console.log('Error running roll back SQL step ' + step + ': ' + thisStep.rollback);
					//console.log(error);
					
					// stop the roll back process
					break;
					
				}
			}
	
		};
		
	};
	
	/**
	 * Open and retrieve the SQLConnection to the local SQLLite database file. 
	 * @param {air.File} dbFile File representing the SQLLite database to create/open
	 * @param {function(air.SQLConnection)} successCallback Success callback once the database connection is established [OPTIONAL]
	 * @param {function()} errorCallback Error callback if the database connection could not be established [OPTIONAL]
	 * 
	 * @return {air.Connection} The connection that was opened to the DB or UNDEFINED if the callback functions are specified
	 */
	service.prototype.connection = function(dbFile, successCallback, errorCallback) {

		if (successCallback || errorCallback) {
			// opens the connection in async mode
			connectionAsync(this, dbFile, successCallback, errorCallback);
		} else {
			// opens the connection in sync mode
			return connectionSync(this, dbFile);
		}
		
	};

	/**
	  * Run a series of SQL statements.  Supports separate programatic rollback where a SQL statement fails.
	  * @param {Array.<Object.<string, string>>} scripts An array of objects containing keys sql and rollback, for example
	  * 	[
	  *			{
	  *				sql: "CREATE TABLE INFO (schemaVersion int)",
	  *				rollback: "DROP TABLE INFO"
	  *			},
	  *			{
	  *				sql: "INSERT INTO INFO (schemaVersion) values (:value)",
	  *				params: { value : 1 },
	  *				rollback: "DELETE FROM INFO WHERE schemaVersion = :value"
	  *			}
	  *		]
	  * @param {air.SQLConnection} sqlConnection A SQLConnection as returned by connection()
	  * @param {function()} successCallback Called once all sql statements in scripts have been run [OPTIONAL]
	  * @param {function(boolean)} errorCallback Called if error while executing sql statements. Boolean indicates if all rollbacks were executed successfully. [OPTIONAL]
	  */
	service.prototype.runScripts = function(scripts, sqlConnection, successCallback, errorCallback) {

		// if any one of the call backs is specified we will
		// run the queries in async otherwise run in sync
		if (successCallback || errorCallback) {
			// run queries in async mode
			runScriptsAsync(scripts, sqlConnection, successCallback, errorCallback);
		} else {
			// run queries in sync mode
			runScriptsSync(scripts, sqlConnection);
		}
		
	};
	
	/**
	  * Run a SQL statement against a SQLConnection.  If the success and error callbacks are specified the query is
	  * executed in asynchronous mode otherwise the call is synchronous mode and the result is returned back
	  * 
	  * @param {string} text A SQL statement to execute
	  * @param {?Object} parameters An object map of parameters
	  * @param {air.SQLConnection} sqlConnection A SQLConnection as returned by connection()
	  * @param {function()} successCallback Called once all sql statements in scripts have been run [OPTIONAL]
	  * @param {function(boolean)} errorCallback Called if error while executing sql statements. Boolean indicates if all rollbacks were executed successfully. [OPTIONAL]
	  * 
	  * @return {?air.SQLStatement} The statement that was executed or UNDEFINED if the callback functions are specified
	  */
	service.prototype.runQuery = function(text, parameters, sqlConnection, successCallback, errorCallback) {
		
		// if any one of the call backs is specified we will
		// run the query in async otherwise run in sync
		if (successCallback || errorCallback) {
			// run query in async mode
			runQueryAsync(text, parameters, sqlConnection, successCallback, errorCallback);
		} else {
			// run query in sync mode
			return runQuerySync(text, parameters, sqlConnection);
		}
		
	};
	
	return service;
	
})();
