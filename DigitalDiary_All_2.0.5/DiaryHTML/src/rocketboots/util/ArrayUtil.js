/**
 * $Id: ArrayUtil.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.util) === "undefined") {
	Rocketboots.util = {};
}


Rocketboots.util.ArrayUtil = (function() {

	var util = function () {
		throw new Error("Cannot construct util class. All methods are static.");
	};

	/**
	 * Push a collection of items into an array, returning the modified array.
	 * Supports pushing different types of collections - arrays, argument lists, object values (keys are ignored).
	 *
	 * @param array {Array|Backbone.Collection} The array to modify.
	 * @param objectsToAdd {Array|Arguments|Object} The items to add to the array.
	 * @return {Array|Backbone.Collection} The modified array.
	 */
	util.pushAll = function (array, objectsToAdd) {
		_.each(objectsToAdd, function (objectToAdd) {
			array.push(objectToAdd);
		});
		return array;
	};

	/**
	 * Convert an argument array (referenced as "arguments" within a function) into an array.
	 * @param args {Arguments} Argument list.
	 * @return {Array} Array copy of the arguments list.
	 */
	util.argListToArray = function (args) {
		return util.pushAll([], args);
	};

	return util;

})();
