/**
 * $Id: AsyncOperationRunner.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.util) === "undefined") {
	Rocketboots.util = {};
}

/**
 * Allows asynchronous functions (specifying success and error callbacks as arguments)
 * to be called one after the other.
 *
 * Requires that success and error handlers are the second last and last arguments respectively.
 */
Rocketboots.util.AsyncOperationRunner = (function() {

	var defaults = {
		defaultArgs: [],
		defaultContext: window,
		allSuccessful: $.noop,
		anyError: $.noop
	};

	/**
	 * @constructor
	 *
	 * Define a set of operations to be queued.
	 * The operation objects passed here must be in an appropriate format for constructing a
	 * AsyncOperation objects (passed as the first argument in the AsyncOperation constructor).
	 * Additionally operations objects may define "success" and "error" handlers as functions.
	 *
	 * @param operations {!Array} A list of operations to be run.
	 * @param options {?Object} Class options.
	 *
	 * @see Rocketboots.util.AsyncOperation
	 */
	var opRunner = function (operations, options) {

		var runner = this;

		runner._settings = _.extend({}, defaults, options ? options : {});
		runner._currentOperation = 0;
		runner._operations = [];
		runner._isRunning = false;
		runner._hasRun = false;
		runner._results = [];
		runner._hasError = false;

		if (!_.isArray(operations)) {
			throw new Error("Operations must be an array");
		}
		if (!_.isFunction(runner._settings.allSuccessful)) {
			throw new Error("All successful must be a function");
		}
		if (!_.isFunction(runner._settings.anyError)) {
			throw new Error("Any error must be a function");
		}
		if (!_.isArray(runner._settings.defaultArgs)) {
			throw new Error("Default args must be an array");
		}
		if (!runner._settings.defaultContext) {
			throw new Error("Context must be defined");
		}

		// Queue operations
		runner._operations = _.map(operations, function (operation, operationIndex) {
			
			var operationModified = {
				args : runner._settings.defaultArgs,
				context : runner._settings.defaultContext,
				success : $.noop, // ignored by AsyncOperation, will be used privately by this class
				error : $.noop // ignored by AsyncOperation, will be used privately by this class
			};

			if (_.isFunction(operation)) {
				operationModified.exec = operation;
			} else if (_.isObject(operation)) {
				_.extend(operationModified, operation);
			} else {
				throw new Error("Poorly defined operation " + operationIndex + " - should be function or object.");
			}

			return new Rocketboots.util.AsyncOperation(operationModified, runner._currentOperationComplete, runner._currentOperationComplete, runner);
		});
	};

	opRunner.prototype._currentOperationComplete = function (operation, handlerArgs) {
		var runner = this,
			success = operation.success();

		// Store the results
		runner._results.push({
			'success' : success,
			'args' : handlerArgs,
			'operation' : operation
		});

		// Call success/error handler, allowing it to explicitly alter the default behaviour by returning false
		var callback = (success ? operation.getSettings().success : operation.getSettings().error);
		var preventDefault = callback.apply(operation.getSettings().context, handlerArgs) === false;

		// Has error?
		if (!success && !preventDefault) {
			this._hasError = true;
		}

		// Continue?
		if ((success && !preventDefault) || (!success && preventDefault)) {
			// Run next after the current stack has cleared to prevent stack overflow
			_.defer(function () { runner._runNext(); });
		} else {
			runner._onStop();
		}
	};

	opRunner.prototype._runNext = function () {

		var runner = this;

		// Check for complete?
		if (runner._currentOperation >= runner._operations.length) {
			runner._onStop();
			return;
		}

		// Run operation, and update state
		runner._operations[runner._currentOperation++].run();
		//console.log('runner._currentOperation : ' + runner._currentOperation);
	};

	opRunner.prototype._onStop = function () {
		this._isRunning = false;
		this._hasRun = true;

		if (this._hasError) {
			this._settings.anyError.call(this._settings.defaultContext, this);
		} else {
			this._settings.allSuccessful.call(this._settings.defaultContext, this);
		}
	};

	/**
	 * Initiate the execution of all queued operations.
	 */
	opRunner.prototype.run = function () {
		if (this._isRunning || this._hasRun) {
			throw new Error("Cannot run the operations multiple times");
		}
		this._isRunning = true;
		
		this._runNext();
	};

	/**
	 * @return True if operations are currently being run.
	 */
	opRunner.prototype.isRunning = function () {
		return this._isRunning;
	};

	/**
	 * @return True if operations have been run and are no longer running.
	 */
	opRunner.prototype.hasCompleted = function () {
		return this._hasRun;
	};

	/**
	 * Query whether all operations were run. This is independent of whether there were errors.
	 * @return {Boolean} True if all operations were executed.
	 */
	opRunner.prototype.didRunAll = function () {
		return this._results.length == this._operations.length;
	};

	/**
	 * Use this method to apply error arguments to a function.
	 * The error arguments used will be the first argument set received from the async callbacks.
	 * This method is useful to replicate the error callback from a single operation.
	 * @param func {!Function} The function which should have the error arguments applied.
	 * @param scope {?Object} The scope used as 'this' within the function.
	 */
	opRunner.prototype.applyAnyErrorArgs = function (func, scope) {
		if (!this._hasRun) throw new Error("Cannot apply error arguments until all operations completed");

		var errorResult = _.find(this._results, function (result) {
			return !result.success;
		});
		if (errorResult) {
			func.apply(scope || window, errorResult.args);
		} else {
			func.call(scope || window);
		}
	};

	return opRunner;

})();