/**
 * $Id: CallbackAggregator.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.util) === "undefined") {
	Rocketboots.util = {};
}

/**
 * A helper manager to assist classes wishing to call multiple methods
 * with callbacks, and know when they have all completed.
 *
 * This class will continue to work as expected regardless of whether the callbacks
 * are called synchronously or asynchronously.
 *
 * When using this class, make sure to match every call to addCall() or addCalls()
 * with a call to returnSuccess() or returnError(), and vice-versa.
 *
 * Additionally make sure to call done(), otherwise your handlers will never be called.
 *
 * A typical usage may look like this:
 *
 *	var callbacks = new Rocketboots.util.CallbackAggregator( successHandler, errorHandler );
 *	for (item in items) {
 *		callbacks.addCall();
 *		item.callAsync( callbacks.getSuccessFunction(), callbacks.getErrorFunction() );
 *	}
 *	callbacks.done();
 */
Rocketboots.util.CallbackAggregator = (function() {

	/**
	 * @constructor
	 * @param completeSuccessHandler {?Function} Called when all callbacks are successful.
	 * @param completeErrorHandler {?Function} Called when all callbacks are completed and some had errors.
	 * @param context {?Object} Used to specify the 'this' variable within the handler functions.
	 */
	var manager = function (completeSuccessHandler, completeErrorHandler, context) {

		this._isDoneAddingCalls = false;
		this._calls = 0;
		this._handlerContext = context || window;
		this._successHandler = completeSuccessHandler || $.noop;
		this._errorHandler = completeErrorHandler || $.noop;
		this._errorArgs = [];
	};

	/**
	 * Called in both success and error cases.
	 * @private
	 */
	manager.prototype._return = function () {
		--this._calls;
		if (this._calls < 0) {
			throw new Error("More returns than calls made");
		}

		if (this.isComplete()) {
			this._callCompleteHandler();
		}
	};

	/**
	 * Explicitly call one of the handlers.
	 * (may be success or error, depending on the state).
	 * This should only ever be called once.
	 * @private
	 */
	manager.prototype._callCompleteHandler = function () {
		if (!this.isComplete()) throw new Error("Cannot call complete handlers more than once");

		if (this._errorArgs.length == 0) {
			this._successHandler.call(this._handlerContext);
		} else {
			this._errorHandler.apply(this._handlerContext, this._errorArgs[0]);
		}
	};

	/**
	 * Record that a new call has been made.
	 */
	manager.prototype.addCall = function () {
		if (this._isDoneAddingCalls) throw new Error("Cannot add calls after done has been called");
		++this._calls;
	};

	/**
	 * Record that a number of new calls have been made.
	 * @param callCount {!Number} The number of new calls made.
	 */
	manager.prototype.addCalls = function (callCount) {
		if (this._isDoneAddingCalls) throw new Error("Cannot add calls after done has been called");
		if (callCount < 0) throw new Error("Cannot add a negative number of calls");
		this._calls += callCount;
	};

	/**
	 * Record a successful callback.
	 * Note that you would usually not call this directly, but use getSuccessFunction instead.
	 * @see getSuccessFunction
	 */
	manager.prototype.returnSuccess = function () {
		this._return();
	};

	/**
	 * Record an unsuccessful callback. The arguments will be stored.
	 * Note that you would usually not call this directly, but use getErrorFunction instead.
	 * @see getErrorFunction
	 */
	manager.prototype.returnError = function () {
		this._errorArgs.push(Rocketboots.util.ArrayUtil.argListToArray(arguments));
		this._return();
	};

	/**
	 * Get a function which will call returnSuccess (properly scoped).
	 * This should be passed into any asynchronous method call as the success handler.
	 * @return {Function} The success handler function.
	 * @see CallbackAggregator.returnSuccess
	 */
	manager.prototype.getSuccessFunction = function () {
		if (this._isDoneAddingCalls) throw new Error("Cannot get the success function after done has been called");

		var scope = this;
		return function () {
			scope.returnSuccess.apply(scope, arguments);
		};
	};

	/**
	 * Get a function which will call returnError (properly scoped).
	 * This should be passed into any asynchronous method call as the error handler.
	 * @return {Function} The error handler function.
	 * @see CallbackAggregator.returnError
	 */
	manager.prototype.getErrorFunction = function () {
		if (this._isDoneAddingCalls) throw new Error("Cannot get the error function after done has been called");

		var scope = this;
		return function () {
			scope.returnError.apply(scope, arguments);
		};
	};

	/**
	 * Mark this object as being done adding calls. This must be called once.
	 * If no calls have been made, calling this method will call the success handler.
	 * Without calling this, your success and error handlers will never be called.
	 */
	manager.prototype.done = function () {
		if (this._isDoneAddingCalls) throw new Error("Cannot call done multiple times");
		this._isDoneAddingCalls = true;

		if (this.isComplete()) {
			this._callCompleteHandler();
		}
	};

	/**
	 * @return {Boolean} True if done() has been called and all calls have come back (either successful or failure); False otherwise.
	 */
	manager.prototype.isComplete = function () {
		return this._isDoneAddingCalls && this._calls <= 0;
	};

	/**
	 * Get the set of argument lists returned by error callbacks.
	 * @return {Array}
	 */
	manager.prototype.getAllErrorArgs = function () {
		if (!this._isDoneAddingCalls) throw new Error("Cannot retrieve the error arguments until all calls have been added. Did you forget to call 'done', punk?");
		return this._errorArgs;
	};

	return manager;

})();