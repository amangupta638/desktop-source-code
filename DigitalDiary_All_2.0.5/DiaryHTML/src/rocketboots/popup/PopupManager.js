/**
 * $Id: PopupManager.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.popup) === "undefined") {
	Rocketboots.popup = {};
}

// TODO: This should implement an Interfacce
Rocketboots.popup.PopupManager = (function() {

	var debug = true;

	/**
	 * Manage the positioning of a single popup.
	 * @private
	 * @inner
	 * @class
	 */
	var _Popup = (function () {

		var
			_ALIGN = {
				"LEFT": "left",
				"CENTER" : "center",
				"RIGHT" : "right",
				"TOP" : "top",
				"MIDDLE" : "middle",
				"BOTTOM" : "bottom"
			},
			_H_ALIGNS = [_ALIGN.LEFT, _ALIGN.CENTER, _ALIGN.RIGHT],
			_V_ALIGNS = [_ALIGN.TOP, _ALIGN.MIDDLE, _ALIGN.BOTTOM],
			_ALIGN_SIDES = [_ALIGN.LEFT, _ALIGN.RIGHT, _ALIGN.TOP, _ALIGN.BOTTOM],
			_MIN_EDGE_GAP = 8;

		/**
		 * @constructor
		 */
		var popupConstructor = function ($popup, $anchor, anchorSide, anchorPoint) {
			this.$popup = $popup;
			this.$anchor = $anchor;
			this.$callout = $(".callout", $popup);
			this.$body = $(document);

			// Defaults
			if (!anchorSide) anchorSide = _ALIGN.RIGHT;

			if (!anchorPoint) {
				if (anchorSide == "left") anchorPoint = "right-middle";
				else if (anchorSide == "top") anchorPoint = "center-bottom";
				else if (anchorSide == "bottom") anchorPoint = "center-top";
				else anchorPoint = "left-middle";
			}

			// Store anchor parameters
			this.originalAnchorSide = anchorSide;
			this.originalAnchorPoint = anchorPoint;

			// Clear any previous state on the Elements
			this.clearMaxSize();
		};

		popupConstructor.prototype.setAnchorParams = function (anchorSide, anchorPoint) {
			var popup = this,
				anchorPointComponents = anchorPoint ? anchorPoint.split("-") : [],
				anchorPointH = anchorPointComponents.length > 0 ? anchorPointComponents[0] : null, // left, center, right
				anchorPointV = anchorPointComponents.length > 1 ? anchorPointComponents[1] : null; // top, middle, bottom

			// Check anchors
			{
				if (!_.contains(_ALIGN_SIDES, anchorSide)) throw new Error("Anchor side '" + anchorSide + "' is not valid, must be one of " + popupConstructor._arrayToString(_ALIGN_SIDES));
				if (!_.contains(_H_ALIGNS, anchorPointH)) throw new Error("Anchor point horizontal component '" + anchorPointH + "' is not valid, must be one of " + popupConstructor._arrayToString(_H_ALIGNS));
				if (!_.contains(_V_ALIGNS, anchorPointV)) throw new Error("Anchor point vertical component '" + anchorPointV + "' is not valid, must be one of " + popupConstructor._arrayToString(_V_ALIGNS));

				if ((anchorSide == _ALIGN.TOP && anchorPointV != _ALIGN.BOTTOM) ||
					(anchorSide == _ALIGN.BOTTOM && anchorPointV != _ALIGN.TOP) ||
					(anchorSide == _ALIGN.LEFT && anchorPointH != _ALIGN.RIGHT) ||
					(anchorSide == _ALIGN.RIGHT && anchorPointH != _ALIGN.LEFT)) {
					throw new Error("Anchor side '" + anchorSide + "' is not compatible with anchor point '" + anchorPoint + "', " +
						"the anchor side should be opposite the point at which the popup and anchor elements join.");
				}
			}

			// Store
			popup.anchorSide = anchorSide;
			popup.anchorPoint = anchorPoint;
			popup.anchorPointH = anchorPointH;
			popup.anchorPointV = anchorPointV;

			// Update call-out
			{
				// Remove any existing callout style classes
				popup.$callout.removeClass("callouttop calloutright calloutbottom calloutleft");

				switch(anchorSide) {
					case _ALIGN.TOP:
						popup.$callout.addClass("calloutbottom");
						break;
					case _ALIGN.LEFT:
						popup.$callout.addClass("calloutright");
						break;
					case _ALIGN.BOTTOM:
					case "search":
						popup.$callout.addClass("callouttop");
						break;
					case _ALIGN.RIGHT:
					default:
						popup.$callout.addClass("calloutleft");
						break;
				}
			}

			popup.readElementProperties();
			popup.calculatePopupPositions();

			debug && console.log("Popup.setAnchorParams(" + anchorSide + ", " + anchorPointH + "-" + anchorPointV + ")");

		};

		popupConstructor.prototype.readElementProperties = function () {
			var t = this;
			t.popup = popupConstructor.getElementProperties(t.$popup);
			t.anchor = popupConstructor.getElementProperties(t.$anchor);
			t.callout = popupConstructor.getElementProperties(t.$callout);
			t.body = popupConstructor.getElementProperties(t.$body);
		};

		popupConstructor.prototype.calculatePopupPositions = function () {
			var t = this,
				left = 0, top = 0, calloutLeft = 0, calloutTop = 0;

			// Set the components dependent on horizontal or vertical positioning
			switch (t.anchorSide) {
				// vertical - 'left' stays the same
				case _ALIGN.TOP:
				case _ALIGN.BOTTOM:
					switch (t.anchorPointH) {
						case _ALIGN.LEFT :
							left = t.anchor.left;
							calloutLeft = 0;
							break;

						case _ALIGN.CENTER :
							left = t.anchor.left + (t.anchor.width / 2) - (t.popup.width / 2);
							calloutLeft = (t.anchor.left - t.popup.left) + (t.anchor.width/2) - (t.callout.width/2);
							break;

						case _ALIGN.RIGHT :
							left = t.anchor.left + t.anchor.width - t.popup.width;
							calloutLeft = t.popup.width - t.callout.width;
							break;
					}
					break;

				// horizontal - 'top' stays the same
				case _ALIGN.LEFT:
				case _ALIGN.RIGHT:
					switch (t.anchorPointV) {
						case _ALIGN.TOP:
							top = t.anchor.top;
							calloutTop = 0;
							break;

						case _ALIGN.MIDDLE:
							top = t.anchor.top + t.anchor.height / 2 - t.popup.height / 2;
							calloutTop = t.anchor.top - t.popup.top + t.anchor.height/2 - t.callout.height/2;
							break;

						case _ALIGN.BOTTOM:
							top = t.anchor.top + t.anchor.height - t.popup.height;
							calloutTop = t.popup.height - t.callout.height;
							break;
					}
					break;
			}

			// set the remaining components
			switch (t.anchorSide) {
				case _ALIGN.TOP :
					top = t.anchor.top - t.popup.height - t.callout.height;
					calloutTop = t.popup.height;
					break;

				case _ALIGN.BOTTOM:
					top = t.anchor.top + t.anchor.height + t.callout.height;
					calloutTop = -t.callout.height;
					break;

				case _ALIGN.LEFT:
					left = t.anchor.left - t.popup.width - t.callout.width;
					calloutLeft = t.popup.width;
					break;

				case _ALIGN.RIGHT:
					left = t.anchor.left + t.anchor.width + t.callout.width;
					calloutLeft = -t.callout.width;
					break;
			}

			// Store calculated values
			t.popup.left = left;
			t.popup.top = top;
			t.callout.left = calloutLeft;
			t.callout.top = calloutTop;
		};

		/**
		 * Switch the popup to the opposite side of the anchor if there is not enough room on the screen to display it.
		 * This does not guarentee the entire popup will be visible, it just attempts to move the popup to a more appropriate
		 * side of the anchor.
		 * @return {Boolean} Whether the anchor position was changed.
		 */
		popupConstructor.prototype.autoAdjustAnchorParams = function () {

			var t = this;

			// test if the popup is already in a good position
			if (t.isAllVisible()) return false;

			var newAnchorSide = t.anchorSide, newAnchorPoint = t.anchorPoint;

			// Move popup around the anchor if necessary
			if (t.anchorSide == "right" && t.popup.left + t.popup.width > t.body.width - _MIN_EDGE_GAP) {
				// Move popup around to the left
				// Only do the move if there's room on the other side
//				if (t.anchor.left - t.popup.width - t.callout.width >= _MIN_EDGE_GAP) {
					newAnchorSide = "left";
					if (t.anchorPoint == "left-bottom") newAnchorPoint = "right-bottom";
					else if (t.anchorPoint == "left-top") newAnchorPoint = "right-top";
					else newAnchorPoint = "right-middle";
//				}

			} else if (t.anchorSide == "left" && t.popup.left < _MIN_EDGE_GAP) {
				// Move popup around to the right
				// Only do the move if there's room on the other side
//				if (t.anchor.left + t.anchor.width + t.callout.width + t.popup.width <= t.body.width - _MIN_EDGE_GAP) {
					newAnchorSide = "right";
					if (t.anchorPoint == "right-bottom") newAnchorPoint = "left-bottom";
					else if (t.anchorPoint == "right-top") newAnchorPoint = "left-top";
					else newAnchorPoint = "left-middle";
//				}

			} else if (t.anchorSide == "bottom" && t.popup.top + t.popup.height > t.body.height - _MIN_EDGE_GAP) {
				// Move popup around to the top
				// Only do the move if there's room on the other side
//				if (t.anchor.top - t.callout.height - t.popup.height >= _MIN_EDGE_GAP) {
					newAnchorSide = "top";
					if (t.anchorPoint == "left-top") newAnchorPoint = "left-bottom";
					else if (t.anchorPoint == "right-top") newAnchorPoint = "right-bottom";
					else newAnchorPoint = "center-bottom";
//				}

			} else if (t.anchorSide == "top" && t.popup.top < _MIN_EDGE_GAP) {
				// Move popup around to the bottom
				// Only do the move if there's room on the other side
//				if (t.anchor.top + t.anchor.height + t.callout.height + t.popup.height <= t.body.height - _MIN_EDGE_GAP) {
					newAnchorSide = "bottom";
					if (t.anchorPoint == "left-bottom") newAnchorPoint = "left-top";
					else if (t.anchorPoint == "right-bottom") newAnchorPoint = "right-top";
					else newAnchorPoint = "center-top";
//				}
			}

			// Apply change
			if (newAnchorSide != t.anchorSide || newAnchorPoint != t.anchorPoint) {
				debug && console.log("Popup.autoAdjustAnchorParams - repositioning to the " + newAnchorSide + " (was " + t.anchorSide + ")");
				t.setAnchorParams(newAnchorSide, newAnchorPoint);
				return true; // indicate the element was repositioned
			}

			return false; // indicate the element was not repositioned
		};

		/**
		 * Query whether any of the popup is hidden off the edge of the screen.
		 * @return {Boolean} True if the entire popup is visible, false if it is partially clipped.
		 */
		popupConstructor.prototype.isAllVisible = function () {
			var t = this;
			return (t.popup.left + t.popup.width <= t.body.width - _MIN_EDGE_GAP &&
				t.popup.left >= _MIN_EDGE_GAP &&
				t.popup.top + t.popup.height <= t.body.height - _MIN_EDGE_GAP &&
				t.popup.top >= _MIN_EDGE_GAP);
		};

		/**
		 * Adjust the position of the popup so all its contents is displayed on screen.
		 *
		 * This must be called after the last call to #getPopupPositions
		 *
		 * @return {Boolean} True if a reposition took place.
		 */
		popupConstructor.prototype.repositionToFit = function () {

			var t = this, oldPopupTop = t.popup.top, oldPopupLeft = t.popup.left;

			if (t.isAllVisible()) return false;

			// If the anchor is to the left or right, allow vertical position adjustment only
			if (t.anchorSide == _ALIGN.LEFT || t.anchorSide == _ALIGN.RIGHT) {
				// Vertical adjustment - only do one
				if (t.popup.top < _MIN_EDGE_GAP) {
					// Top of page overflow
					t.popup.top = _MIN_EDGE_GAP;
					t.callout.top += (oldPopupTop - t.popup.top);

				} else if (t.popup.top + t.popup.height > t.body.height - _MIN_EDGE_GAP) {
					// Bottom of page overflow
					// Don't adjust unless the whole height will fit
					if ((t.body.height - t.popup.height - _MIN_EDGE_GAP) >= _MIN_EDGE_GAP) {
						t.popup.top = t.body.height - t.popup.height - _MIN_EDGE_GAP;
						// If the popup has to move, then adjust the callout accordingly so it still points to the anchor
						t.callout.top += (oldPopupTop - t.popup.top);
					}
				}

			// If the anchor is above or below the popup, allow horizontal position adjustment only
			} else {
				// Horizontal adjustment - only do one
				if (t.popup.left < _MIN_EDGE_GAP) {
					// Left of page overflow
					t.popup.left = _MIN_EDGE_GAP;
					t.callout.left += (oldPopupLeft - t.popup.left);

				} else if (t.popup.left + t.popup.width > t.body.width - _MIN_EDGE_GAP) {
					// Right of page overflow
					// Don't adjust unless the whole width will fit
					if (t.body.width - t.popup.width - _MIN_EDGE_GAP >= _MIN_EDGE_GAP) {
						t.popup.left = t.body.width - t.popup.width - _MIN_EDGE_GAP;
						t.callout.left += (oldPopupLeft - t.popup.left);
					}
				}
			}

			return t.popup.left != oldPopupLeft || t.popup.top != oldPopupTop; // change
		};

		popupConstructor.prototype.setMaxSize = function () {
			var t = this;
			var maxheight = t.body.height - t.popup.top - (t.$popup.outerHeight() - t.$popup.height()) - _MIN_EDGE_GAP;
			var height = parseInt(t.$popup.height());
			var overflow = "hidden";
			if(height > 400){
				overflow = "scroll";
			}
			t.$popup.css({
				"max-height" : 450,//maxheight,
				"max-width" : t.body.width - t.popup.left - (t.$popup.outerWidth() - t.$popup.width()) - _MIN_EDGE_GAP,
				"overflow-y" : overflow,
				"overflow-x" : "hidden",
				"z-index" : "9999"
			});
		};

		popupConstructor.prototype.clearMaxSize = function () {
			this.$popup.css({
				"max-height" : 9999,
				"max-width" : 9999,
				"overflow-y" : "hidden",
      			"overflow-x" : "hidden"
			});
		};

		popupConstructor.prototype.positionAndShow = function () {
			var t = this;

			// The element must be displayed before positioning is calculated, otherwise querying elements dimensions and positions will be incorrect.
			t.$popup.show();

			// Perform all calculations
			t.setAnchorParams(t.originalAnchorSide, t.originalAnchorPoint);
			t.autoAdjustAnchorParams();
			t.autoAdjustAnchorParams(); // do it twice, so that if it doesn't fit either way it will remain in the original position
			t.calculatePopupPositions();
			t.repositionToFit();
			t.setMaxSize();

			// Apply calculated positions
			t.$callout.css({ "left" : t.callout.left, "top" : t.callout.top }); // position relative to popup
			t.$popup.offset({ "left" : t.popup.left, "top" : t.popup.top }); // position relative to document
		};

		/**
		 * @static
		 */
		popupConstructor.getElementProperties = function ($element) {
			return _.extend({}, $element.offset(), {
				"width" : $element.outerWidth(),
				"height" : $element.outerHeight()
			});
		};

		/**
		 * @static
		 */
		popupConstructor._arrayToString = function (array) {
			return _.reduce(array, function (memo, arrayItem) {
				return (memo.length > 0 ? ", " : "") + arrayItem;
			}, "");
		};

		return popupConstructor;
	})();


	/**
	 * Upon construction, a click handler is added to the body to dismiss any popup elements.
	 * @constructor
	 */
	var popupManager = function(defaults) {
		$(document).ready(function(e) {
			$("body").on("click", captureOutsidePopupClick);
		});
	};

	/**
	 * Display a popup as a call-out bubble. The bubble will originate from a given element (the anchor)
	 * and will display a given element (popup).
	 * @param $popup {jQuery} The Element to display as the popup. It is assumed to have a child with the class "callout".
	 * @param $anchor {jQuery} The Element to use as the source of the popup. The popup will be displayed in a location relative to this element.
	 * @param anchorSide {String} The direction from the anchor in which the popup will be displayed.
	 * @param anchorPoint {?String} Optional. The positioning of the call-out (the icon joining the anchor and popup, usually an arrow).
	 * @param sourceEvent {?Event} Optional. If provided, the event will have its propagation stopped.
	 * @param $scrollContainer {jQuery} Optional. If specified, the scroll container will have an event handler added to dismiss the popup upon scroll.
	 */
	popupManager.prototype.show = function ($popup, $anchor, anchorSide, anchorPoint, sourceEvent, $scrollContainer) {

		var manager = this,
			popup;

		// Event
		if (sourceEvent) {
			sourceEvent.stopPropagation();
		}

		// Hide all non-static popups
		$(".popup-visible").hide().removeClass("popup-visible").removeClass("popup-static-visible").off("click", capturePopupClick);

		popup = new _Popup($popup, $anchor, anchorSide, anchorPoint);
//		{ // randomize popup location (for testing)
//			var popupAnchorSideIndex = Math.floor(Math.random()*4);
//			popup = new _Popup($popup, $anchor,
//				["left", "right", "top", "bottom"][popupAnchorSideIndex],
//				[
//					["right-top", "right-middle", "right-bottom"],
//					["left-top", "left-middle", "left-bottom"],
//					["left-bottom", "center-bottom", "right-bottom"],
//					["left-top", "center-top", "right-top"]
//				][popupAnchorSideIndex][Math.floor(Math.random()*3)]);
//		}

		// Show popup
		popup.positionAndShow();
		// Show the popup again after a delay, to allow for rendering slowness to be resolved for position calculations
		_.defer(function () { popup.positionAndShow(); });

		$popup.on("click", capturePopupClick);
		$popup.addClass("popup-visible");

		// Handle scrolling
		if ($scrollContainer) {
			// remove any prior event handler
			$scrollContainer.off("scroll", scrollHandler);
			// assign new event handler
			$scrollContainer.on("scroll", {
				"manager" : manager,
				"$popup" : $popup,
				"$scrollContainer" : $scrollContainer
			}, scrollHandler);

			debug && console.log("PopupManager.show - assigning scroll event handler to " + $scrollContainer.length + " objects (should dismiss popup upon scroll)");
		}
	};

	// TODO: remove this method. It is the old implementation of this class.
	popupManager.prototype._showOld = function() {

		// custom stuff for search ....
		var _SEARCH_LEFT_OFFSET = 105, _SEARCH_BOTTOM_GAP = 20, _SEARCH_RESULT_ROW_HEIGHT = 30;

		// Ensure popup position remain visible on screen
		if (anchorSide == 'search') {
			var maxAllowedHeight = bodyHeight - popupTop - _MIN_EDGE_GAP - _SEARCH_BOTTOM_GAP;
			if (popupHeight > maxAllowedHeight) {
				popupHeight = Math.floor(maxAllowedHeight / _SEARCH_RESULT_ROW_HEIGHT) * _SEARCH_RESULT_ROW_HEIGHT;
				var searchResults = $popup.find(".searchResults:first");
				searchResults.height(popupHeight);
			}
		}
	};
	
	/**
	 * Hide the requested popup(s)
	 */
	popupManager.prototype.hide = function($popup) {
		$popup.hide().removeClass("popup-visible").removeClass("popup-static-visible").off("click", capturePopupClick);
	};
	
	/**
	 * Hide all open popups
	 */
	popupManager.prototype.hideAll = function() {
		popupManager.hide($(".popup-visible"));
		popupManager.hide($(".popup-static-visible"));
	};
	
	/**
	 * Stop propagation of click events over a popup.  Allows the body click handler to capture mouse clicks outside
	 * @private
	 */
	function capturePopupClick(event) {
		event.stopPropagation();
	}
	
	/**
	 * Any click events captured here have not been stopped by a popup, and therefore are a click outside.  Hide any visible popups
	 * @private
	 */
	function captureOutsidePopupClick(event) {
		$(".popup-visible").hide().removeClass("popup-visible").removeClass("popup-static-visible").off("click", capturePopupClick);
		//popupManager.hide($(".popup-visible"));
	}

	function scrollHandler(event) {
		debug && console.log("PopupManager.scrollHandler - hiding popup");
		// hide the popup
		event.data.manager.hide(event.data.$popup);
		// stop listening for the scroll event
		event.data.$scrollContainer.off("scroll", scrollHandler);
	}
	
	return popupManager;
	
})();
