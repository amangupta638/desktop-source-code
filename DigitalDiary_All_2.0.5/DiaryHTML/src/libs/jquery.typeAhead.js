(function($){
    var debug = false;

    var methods = {
        init : function( options ) {

            return this.each(function() {
                var $this = $(this),
                    data = $this.data('typeAhead');

                if (!data) {
                    $this.data('typeAhead', $.extend({
                        minCharLimit: 1,
                        delay: 0,
                        source: [],
                        charCount: 0
                    }, options));

                }

                var doTypeAhead = function($input) {
                    var search = $input.val().toLowerCase();
                    var results = [];
                    var data = $this.data('typeAhead');
                    debug && console.log("doing type ahead on ", search, JSON.stringify(data.source));

                    data.charCount = search.length;
                    debug && console.log('setting charCount to ', data.charCount);

                    if (data.source instanceof diary.collection.SubjectCollection) {
                        var matches = _.filter(data.source.toArray(), function(subject) {
                            return (subject.get('title').toLowerCase()).indexOf(search) == 0;
                        });

                        results = _.map(matches, function(subject) {
                            return subject.get('title');
                        });
                    }

                    if (results.length > 0) {
                        if (results[0] == $input.val()) {
                            return;
                        }

                        $input.val(results[0]);
                        $.createSelection($input[0], search.length, results[0].length);
                    }
                    $this.data('typeAhead', data);
                };

                $this.keyup(function(event) {
                    var charCount = $this.val().length;
                    var data = $this.data('typeAhead');
                    clearTimeout(data.timeout);

                    if (event.keyCode == 8) { // backspace
                        return;
                    }

                    if (data.charCount == charCount) {
                        debug && console.log("no change in char count", data.charCount, charCount);
                        return; // no change
                    }

                    if (charCount < data.minCharLimit) {
                        debug && console.log("char count less than min char count");
                        return;
                    }

                    data.timeout = setTimeout(function() {
                        doTypeAhead($this);
                    }, data.delay);

                    $this.data('typeAhead', data);
                });

                $this.blur(function(event) {
                    var data = $this.data('typeAhead');
                    clearTimeout(data.timeout);
                    data.charCount = 0;
                    $this.data('typeAhead', data);
                });
            });
        },
        update : function( options ) {
            var $this = $(this);
            var data = $this.data('typeAhead');

            $this.data('typeAhead', $.extend(data, options));
        }
    };

	$.fn.typeAhead = function(method){

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
        }
    };
})(jQuery);

