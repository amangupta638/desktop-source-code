﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.width=240;
	config.height=50;
	config.toolbarCanCollapse = false;
	config.fontSize_sizes = '5 Pixels/5px';
	
	
	config.toolbar_Basic =
[
	['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-']
];
	
	config.toolbar = 'Basic';
	
	config.toolbarStartupExpanded = false;
};
