/**
 * The model that represents a collection of class and user for my class.
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.ClassUserCollection = (function() {

	/**
	 * Creates a collection of Classes
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({
		model : diary.model.ClassUser
	});
	
})();