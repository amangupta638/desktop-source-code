/**
 * The model that represents a collection of DayOfNote items in a diary.
 * 
 * author - Justin Judd
 */
diary = diary || {};
diary.collection = diary.collection || {};

diary.collection.DayOfNoteCollection = (function() {

	/**
	 * Constructor
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

		initialize: function () {
			this.on("change:date", function () {this.sort();}, this);
		},
		
		model : diary.model.DayOfNote,
		
		/**
		 * The function that is used to keep the collection sorted.  The day of note collection
		 * will be sorted by the date of the day of note so that we will have the list of day of notes
		 * sorted in the natural order
		 * 
		 * @param {diary.model.DayOfNote} dayOfNote The day of note that is to be sorted in the collection
		 * 
		 * @return {integer} The timestamp of the date that represents the day of note
		 */
		comparator : function(dayOfNote) {
			// the sort field is the date of the day of note
			return dayOfNote.get('date').getKey();
		}
	});

})();