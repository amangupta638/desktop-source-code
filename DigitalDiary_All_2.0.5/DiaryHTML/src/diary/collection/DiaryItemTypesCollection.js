var diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.DiaryItemTypesCollection = (function() {

	return Backbone.Collection.extend({
		
		model: diary.model.DiaryItemType,
		
		getNameForDiaryItem: function(name) {
			var diaryItemTypes = _.filter(this.toArray(), function(diaryItemType) {
				return diaryItemType.get('formTemplate').toLowerCase() == name.toLowerCase();
			});
			return diaryItemTypes[0] ? diaryItemTypes[0].get('name') : null;
		},
		
		getFormTemplate: function(name) {
			var diaryItemTypes = _.filter(this.toArray(), function(diaryItemType) {
				return diaryItemType.get('formTemplate').toLowerCase() == name.toLowerCase();
			});
			return diaryItemTypes[0] ? diaryItemTypes[0].get('formTemplate') : null;
		},
		
		isDiaryItemTypeActive: function(name) {
			var isActive = false;
			var diaryItemTypes = _.filter(this.toArray(), function(diaryItemType) {
				return diaryItemType.get('formTemplate').toLowerCase() == name.toLowerCase();
			});
			
			_.forEach(diaryItemTypes, function(diaryItemType) {
				if(!isActive)
					isActive =  diaryItemType.get('isActive') ? true : false;
			});
				
			
			return isActive;
		},
		
		isTypeActive: function(name) {
			var isActive = false;
			var diaryItemTypes = _.filter(this.toArray(), function(diaryItemType) {
				return diaryItemType.get('name').toLowerCase() == name.toLowerCase();
			});
			
			isActive =  (diaryItemTypes[0] && diaryItemTypes[0].get('isActive')) ? true : false
			return isActive;
		}
	
	});
	
})();
