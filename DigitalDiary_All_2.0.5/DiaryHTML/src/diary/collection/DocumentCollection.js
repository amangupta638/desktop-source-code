diary = diary || {};
diary.collection = diary.collection || {};

diary.collection.DocumentCollection = (function() {

	return Backbone.Collection.extend({
		model : diary.model.Document
	});

})();