/**
 * The model that represents a collection of Attachments in a diary.
 * 
 * author - John Pearce
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.AttachmentCollection = (function() {

	/**
	 * Creates a collection of Attachments
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

		initialize: function () {
			this.on("change:dateAttached", function () {this.sort();}, this);
		},
		
		model: diary.model.Attachment,
		
		/**
		 * The function that is used to keep the collection sorted.  The attachment collection
		 * will be sorted by the date attachments were added to the diary item.
		 * 
		 * @param {diary.model.Attachment} attachment The attachment that is to be sorted in the collection
		 * 
		 * @return {number} The timestamp of the date that represents when the attachment was added.
		 */
		comparator : function(attachment) {
			// the sort field is the date of the diary item
			return (attachment.get('dateAttached') ? attachment.get('dateAttached').getTime() : 0);
		}
		
	});
	
})();
