/**
 * The model that represents a collection of grade
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.GradeCollection = (function() {

	/**
	 * Creates a collection of Grades
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

	
		
		model : diary.model.Grade,
		
	
	});
	
})();