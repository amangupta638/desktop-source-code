/**
 * The local SQL delegate for ContentPackage
 *
 * author - Phil Haeusler
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.ContentPackageSQLDelegate = (function() {

    /**
     * Constructs a Content Package Delegate
     *
     * @constructor
     */
    var _delegate = function() {
        this._database = app.locator.getService(ServiceName.APPDATABASE);
    };

	/**
	 * Gets the saved content packages for the application
	 *
	 * @param {!function(diary.collection.ContentPackageCollection)} successCallback The function that will be executed on a successful read of the content packages
	 * @param {?function(message)} errorCallback The function that will be executed on failure of reading the content packages [OPTIONAL]
	 */
	_delegate.prototype.getContentPackages = function(successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"SELECT * FROM storedContentPackages",

				// params
				{},

				// success
				function (statement) {
					successCallback(new diary.collection.ContentPackageCollection(_.map(statement.getResult().data, function(item){
						return new diary.model.ContentPackage({
							'id' 						: item.id,
							'package'					: item.package,
							'dateLastUpdated'			: item.dateLastUpdated
						});
					})));
				},

				// error
				errorCallback, "Error while trying to read stored content packages"
		);
	};

	/**
	 *
	 * @param contentPackage
	 * @param successCallback
	 * @param errorCallback
	 */
	_delegate.prototype.addContentPackage = function(contentPackage, successCallback, errorCallback) {
        if (!(contentPackage instanceof diary.model.ContentPackage)) {
            throw new Error("contentPackage must be of type diary.model.ContentPackage");
        }
		this._database.runQuery(
				// query
				"INSERT INTO storedContentPackages (package, dateLastUpdated) " +
						"VALUES (:package, :dateLastUpdated)",

				// params
				{
					'package'           : contentPackage.get('package'),
					'dateLastUpdated'   : contentPackage.get('dateLastUpdated')
				},

				// success
				function(statement) {
					successCallback(statement.getResult().lastInsertRowID);
				},

				// error
				errorCallback, "Error while trying to create content package"
		);
    };

    /**
	 *
     * Deletes all content packages, or if supplied, just the content packages
     *
	 * @param {!function()} successCallback The function that will be executed on a successful deletion
     * @param {?function(message)} errorCallback The function that will be executed on failure of deletion [OPTIONAL]
	 * @param {?diary.collection.ContentPackageCollection} contentPackages the specific content packages to delete [OPTIONAL]
     */
    _delegate.prototype.deleteContentPackages = function(successCallback, errorCallback, contentPackages) {
        this._database.runQuery(
				// query
				"DELETE FROM storedContentPackages" + (contentPackages ?
					" WHERE package IN (" + contentPackages.map(function(p) {return "'" + p.get('package') + "'";}).join(',') + ")" :
					""),

				// params
				{},

				// success
				successCallback || $.noop,

				// error
				errorCallback, "Error while trying to delete content packages"
        );
    };

    // return the generated delegate
    return _delegate;
})();
