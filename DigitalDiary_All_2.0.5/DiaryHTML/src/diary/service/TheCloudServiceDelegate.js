/**
 * The local delegate for the cloud service. It does the actual ajax API calls to the cloud.
 * 
 * author - Jane Sivieng
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.TheCloudServiceDelegate = (function() {
	
	/**
	 * Constructs a "The Cloud" Service Delegate.  
	 * 
	 * @constructor
	 */
	var _delegate = function() {	

	};
	
	
	/**
	 * Calls the "The Cloud" Service 
	 */
	_delegate.prototype.callService = function(method, ajaxType, data, successCallback, errorCallback) {

		if (!isSpecified(method)) {
			throw new Error("Method must be specified.");
		} 
		
		if (typeof(method) !== "string") {
			throw new Error("Method must be a string");
		}
				
		if (typeof(successCallback) !== "function") {
			throw new Error("Success callback must be defined.");
		}
				
		if (typeof(errorCallback) !== "function") {
			throw new Error("Error callback must be defined.");
		}
		var url = SystemSettings.SERVERURL + "/" + method+"?" + data; 
		//var url = "http://e-planner.com.au" + "/desktop/" + method+"?" + "data=" + JSON.stringify(data); 
		var newData = "";
		if(method == 'login'){
			url = SystemSettings.SERVERURL + "/login?"; 
			newData = '{"email_id":"'+data.email_id+'","password":"'+data.password+'","domain":"'+data.domain+'","device_type":'+1+',"device_token":"'+1+'"}';
		}
		data = data || {};
		ajaxType = ajaxType || 'GET'; 
		console.log("newData : "+JSON.stringify(newData));
		$.ajax({
			'url' : url,
			'type': ajaxType,
			'timeout' : 120000,
			'data': JSON.stringify(newData),
			'dataType' : "json",
			'success': function(result) {
				console.log("result cloud service login:"+JSON.stringify(result))	
				if(result.ErrorCode == '0'){
					//$(".mainLogin #busy").hide();
					errorCallback(result.Error);
				}
				else{
					successCallback(result);	
				}
			}, 
			'error': function(response) {
				//$(".mainLogin #busy").hide();
				console.log("response : "+JSON.stringify(response));
				errorCallback("Error contacting server");
			}
		});
	};
	
/*method to insert new data item using the web service*/
_delegate.prototype.Sync = function(DiaryObject) {
        var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
               var token=systemConfig.get('token');

        var url = SystemSettings.SERVERURL + "/insertupdate"; 
        data =  {
        "id":"12",
        "student_id":token,
        "type":"ASSIGNMENT1",
        "title":"sdsdsdsdsd",
        "description":"sdxc sfsd cdfgdf d1",
        "created":"1365075619846",
        "assignedday":"20130404",
        "assignedtime":"1000",
        "dueday":"20130404",
        "duetime":"1000",
        "completed":"",
        "subjectid":"",
        "startday":"20130404",
        "starttime":"2000",
        "endtime":"2359",
        "messagefrom":"",
        "messagefromtext":"",
        "lastupdated":"",
      

        };
        ajaxType ='POST'; 
        
        /*console.log("TheCloud.callService: url=" + url + ", type=" + ajaxType + " data=" + JSON.stringify(data));
                
        $.ajax({
            'url' : url,
            'type': ajaxType,
            'data': data,
            'dataType' : "json",
            'success': function(result) {
              // console.log("TheCloud.response: ", JSON.stringify(result));
            }, 
            'error': function(response) {
               // console.log("TheCloud.error: ", JSON.stringify(response));
            }
        });*/
    };

/*=======*/
    /**
     * Downloads a file from "The Cloud"
     */
    _delegate.prototype.downloadFile = function(method, data, successCallback, errorCallback) {
    	
        var request,
            loader,
            responded = false,
            url = SystemSettings.SERVERURL + "/" + method+"?";
      
        data = data || {};
        for (var key in data) {
            url += "&" + key + "=" + encodeURIComponent(data[key]);
        }
       // alert("url for downloadfile : "+url);
        
       console.log("TheCloud.callService: url=" + url);

        request = new air.URLRequest(url);
        loader = new air.URLLoader();

        loader.dataFormat = "binary";
        loader.addEventListener("complete", function(event) {
            if (responded) return;

            successCallback(loader.data);
            responded = true;
        });
        loader.addEventListener("ioError", function(event) {
            if (responded) return;

            errorCallback(diary.service.TheCloudServiceDelegate.ERRORS.INCOMPLETEDOWNLOAD);
            responded = true;
        });
        loader.addEventListener("httpStatus", function(event) {
            if (responded) return;

            if (event.status == "200") {
                return; // let the complete event listener handle this;
            } else if (event.status == "401") {
                errorCallback(diary.service.TheCloudServiceDelegate.ERRORS.AUTHENTICATIONERROR);
            } else if (event.status == "404") {
                errorCallback(diary.service.TheCloudServiceDelegate.ERRORS.FILENOTFOUND);
            } else if (event.status == "0") {
                errorCallback(diary.service.TheCloudServiceDelegate.ERRORS.SERVERUNAVAILABLE);
            } else {
                errorCallback(event.status);
            }
            responded = true;

        });
        loader.load(request);

    };
	
	/* 
	 * Valid API Calls 
	 */
	_delegate.TYPE = {
		GET : "GET",
		POST : "POST"
	};
	 
	_delegate.METHODS = {
        DOWNLOADSCHOOLCONFIG: "downloadcontentpackage",
        DOWNLOADCONTENTPACKAGE: "downloadcontentpackage",
        REGISTER : "register",
		ADDUSAGELOGS : "addusagelogs",
		LOGIN : "login",
		GETTEACHER : "teachers"
	};

    _delegate.ERRORS = {
        AUTHENTICATIONERROR:    "Authentication Error",
        FILENOTFOUND:           "File not found",
        INCOMPLETEDOWNLOAD:     "Incomplete - error downloading file",
        SERVERUNAVAILABLE:      "Server Unavailable"
    };

	// return the generated delegate
	return _delegate;
		
})();