/**
 * The manager for parsing and manipulating manifest files
 *
 * author - Phil Haeusler
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.ManifestManager = (function() {

    /**
     * Reads the JSON object from the content of the given file
     * TODO: Move into separate AIR package (FileReader)
     * @param {!air.File} file The file that contains the text representing the Object that is to be build
     *
     * @private
     */
    function readObjectFromFile(file) {
        var json = {};
        // holds the file stream for reading the file
        var fileStream;

        fileStream = new air.FileStream();
        
        try {
        	fileStream.open(file, "read");
        	// read the json object that is stored in the given file
            json = $.parseJSON(fileStream.readUTFBytes(fileStream.bytesAvailable));
            //console.log("manifest mgr : " + JSON.stringify(json));
        } catch (e) {
            //console.log(e);
            json = {};
        } finally {
            fileStream.close();
        }

        return json;
    }

    /**
     * Reads the CSV data from the content of the given file. Needs to be called as mapDyasOfNote.call(this, ...) as it needs to reference the manager._csvParser
     *
     * TODO: Move into separate AIR package (FileReader)
     * @param {!air.File} file The file that contains the text representing the Object that is to be build
     * @param {array} columsn an array of column names to accept
     * @private
     */
    function readCSVFromFile(file, columns) {
		
		var manager = this;
		
        var records = [];

		if (file.exists) {
			// holds the file stream for reading the file
			var fileStream = new air.FileStream();
	
			try {
				fileStream.open(file, "read");
				
				// read the json object that is stored in the given file
				var csvdata = fileStream.readUTFBytes(fileStream.bytesAvailable);
				
				records = manager._csvParser.parseCSV(csvdata, columns);
			} catch (e) {
				//console.log(e);
			} finally {
				fileStream.close();
			}
		}
		
        return records;
    }

    /**
     * Maps the content item from the JSON file to the Content Item model
     *
     * @param {Object} contentItemObject The content item object from the JSON file
     *
     * @return {diary.model.ContentItem} The content item model that was mapped
     */
    function mapContentItem(contentItemObject) {

		// holds the title of the content item
        var title;
        // holds the URL that is mapped to the content item
        var url;
        // holds the start date of the content item
        var startDate = null;
        // holds the end date of the content item
        var endDate = null;
        // holds the identifier of the sub items that are to be mapped to the article
        var subItems = null;
        
        // Added role to display content based on role
        var role = null;

		// holds the name of the article
		var article,
			documentId;
		
		// read the title that is coming from the JSON file
        if (!contentItemObject.title) {
            throw new Error("Title must be specified, title is not optional");
        } else {
            // read the title from the JSON object
            title = contentItemObject.title;
        }
        
        
        
        // check if we have a start date
        if (contentItemObject.startDate) {
            startDate = Rocketboots.date.Day.parseDateString(contentItemObject.startDate);
        }

        // check if we have a end date
        if (contentItemObject.endDate) {
            endDate = Rocketboots.date.Day.parseDateString(contentItemObject.endDate);
        }
        
        // read the role that is coming from the JSON file
        if (contentItemObject.role) {
        	role = contentItemObject.role;
        } else {
            
        }

		// read the package identifier
        var packageId = $.trim(contentItemObject.packageId);
        
        // if a URL is specified we will just read that otherwise we will build
        // it from the specified package identifier and the article
        if (contentItemObject.url) {	// Set by UseLocalPaths elsewhere
            // we can read the URL directly
            url = contentItemObject.url;
			documentId = contentItemObject.documentId;
			article = contentItemObject.article;
			role = contentItemObject.role;
			
        } else {

            // we need to build it from the package identifier
            if (!contentItemObject.packageId) {
                throw new Error("Package Identifier must be specified, identifier is not optional");
            }

           
            // we know the package identifier so now we can determine the article that we need to display
            if (packageId != diary.model.ContentItem.SCHOOL_INFO_URL) {

                // holds the pointer to a section in the article if any
                var articleSection = "";

                // if we have an article specified we will use that otherwise
                // we will use the default one
                if (!contentItemObject.article) {
                    // use the default article for the package which would be the
                    // index page
                    article = "index.html";
                } else {
                    // we will assume that we have a specified article to use
                    article = $.trim(contentItemObject.article);
                    // check if the article has a pointer to a section
                    var sectionIndex = article.indexOf("#");

                    // if we don't have a section it is easy just use the specified article
                    // otherwise we will have to get out the specific section
                    // NOTE - we need to do this so that we can check if the file exists before we continue
                    if (sectionIndex != -1) {
                        articleSection = article.substring(sectionIndex);
                        article = article.substring(0, sectionIndex);
                    }
                }

                // check that we have the article that is being mapped in the content
                // folder of the application
                // TODO - Eventually we might want to un-comment this so that we can check if the article
                // exists before including it in the list of content items for the application

                // var articleFile = air.File.applicationStorageDirectory.resolvePath("content" + air.File.separator + packageId + air.File.separator + article);

                // //check that the file of the article exists
                // if (!articleFile.exists) {
                // 	throw new Error("Article [" + articleFile.nativePath + "] was not found, article will not be mapped" );
                // }

                // map the URL for the article

				documentId = packageId + "/" + article;
                url = diary.model.ContentItem.getContentFolder() + documentId + articleSection;
                
                

            } else {

                // set the URL to the school info path
                url = diary.model.ContentItem.SCHOOL_INFO_URL;
            }

        }

        // check if we have any sub items for the article
        if (contentItemObject.subItems) {
            // check that the identifier of the sub items mapping is correct
            subItems = $.trim(contentItemObject.subItems);
            
        
        }

        // create the content item
        return new diary.model.ContentItem({ 'id' : url,
            'title' : title,
            'url' : url,
            'fromDate' : startDate,
            'toDate' : endDate,
            'subItems' : subItems,
			'packageId' : packageId,
			'article' : article,
			'documentId' : documentId,
			'role'		: role		
        });

    }


    /**
     * Parses all the given content item objects to a model.  If an error occurs while trying to map
     * a particular content item, the item will be ignored
     *
     * @param {Array<Object>} contentItemObjects The content item objects that are to be mapped
     *
     * @return {Array<diary.model.ContentItem>} The mapped content items
     */
    function mapContentItems(contentItemObjects) {

        // holds the mapped content items
        var contentItems = [];

        // try to map all the given content items
        _.forEach(contentItemObjects, function(contentItemObject) {

            try {

                contentItems.push(mapContentItem(contentItemObject));

            } catch (error) {
                // log the error other then that we cannot do anything
                //console.log("Error while trying to parse content item");
               // console.log(error);
            }

        });

        // return all mapped content items
        return contentItems;

    }

    /**
     * Maps subItems, banners and shortcuts into a contentMapping model
     *
     * @param subItems A struct of subItem arrays
     * @param banners An array of calendarBanner objects from the manifest
     * @param shortcuts An array of shortcut objects from the manifest
     * @return {diary.model.ContentMapping}
     */
    function mapContentMappings(manifestSubItems, manifestBanners, manifestShortcuts) {
        var subItems = {};

        // holds the collection of calendar banner mappings
        var calendarBanners = new diary.collection.ContentItemCollection();

        // holds the collection of shortcut mappings
        var shortcuts = new diary.collection.ContentItemCollection();

        // if we have any sub items in the mapping file we will process them
        if (manifestSubItems) {

            // go through each of the specified sub items and parse them
            for (var subItemId in manifestSubItems) {

                // map each article to the model
                var articles = new diary.collection.ContentItemCollection();
                articles.add(mapContentItems(manifestSubItems[subItemId]));
                subItems[subItemId] = articles;
            }
        }

        // if we have any calendar banners in the mapping file we will process them
        if (manifestBanners) {
            // map all the sub items
            calendarBanners.add(mapContentItems(manifestBanners));
        }


        // if we have any calendar banners in the mapping file we will process them
        if (manifestShortcuts) {
            // map all the sub items
            shortcuts.add(mapContentItems(manifestShortcuts));
        }

        return new diary.model.ContentMapping({
            'subItems' : subItems,
            'calendarBanners' : calendarBanners,
            'shortcuts' : shortcuts
        });
    }



    /**
     * Maps an array of packages into a ContentPackageCollection
     *
     * @param packages
     * @return {diary.collection.ContentPackageCollection}
     */
    function mapContentPackages(packages) {
        return new diary.collection.ContentPackageCollection(

            _.map(packages, function(p) {

                return new diary.model.ContentPackage({
                    'package' : p.id,
                    'dateUpdated' : p.dateUpdated,
                    'dateLastUpdated' : null
                });
            })

        );
    }

    function mapTimetables(timetables, manifestFileUrl) {
    	//console.log("ManifestManager::mapTimetables()");
        var timetableCollection = new diary.collection.TimetableCollection();


        _.forEach(timetables, function(timetableEntry) {
            try {
                var file = manifestFileUrl.resolvePath(timetableEntry.path),
                    timetableJson = readObjectFromFile(file),
                    timetable;

                timetable = new diary.model.Timetable({
					'name' : timetableEntry.name,
                    'startDay' : new Rocketboots.date.Day(timetableEntry.startDate),
                    'cycleLength' : timetableJson.cycleLength,
					'cycleLabel' : timetableJson.cycleLabel,
					'cycleDays' : timetableJson.cycleDays
                });

                _.forEach(timetableJson.periods, function(period) {
                    var attrs = {
                       'startTime': Rocketboots.date.Time.parseTime(period.startTime),
                       'endTime': Rocketboots.date.Time.parseTime(period.endTime),
                       'title': period.title,
                       'isBreak' : period.isBreak,
                       'cycleDay' : period.cycleDay
					};
                    timetable.get('periods').add(new diary.model.Period(attrs));
                });

                timetableCollection.add(timetable);

            } catch (e) {
               // we'll ignore this timetable
              //  console.log(e);
            }

        });

        return timetableCollection;
    }


    function mapSchoolProfile(schoolProfile, manifestFileUrl) {

        if (schoolProfile) {
            // map asset paths in the school profile
            var imageKeys = ["schoolLogo", "schoolImage"];

            _.forEach(imageKeys, function(key) {
                var imageFile = manifestFileUrl.resolvePath(schoolProfile[key]);

                // if image files specified don't exist, then set them to empty string
                // so that the templates don't try to display them
                if (!imageFile.exists) {
                   // console.log(key, schoolProfile[key], "doesn't exist");
                    schoolProfile[key] = "";
                } else {
                    schoolProfile[key] = imageFile.url;
                }
            });
        }
        else {
            //console.log("Missing school profile");
            // TODO: Handle no profile present
            schoolProfile = {
                'schoolLogo' : "",
                'schoolImage' : "",
                'schoolMotto' : "",
                'missionStatement' : "",
                'diaryTitle' : "",
                'name' : "",
				'theme' : ""
            };
        }

        return new diary.model.SchoolProfile(schoolProfile);
    }

    /**
     * Maps an array of daysOfNote CSV files into a DayOfNoteCollection.  Needs to be called as mapDyasOfNote.call(this, ...) as it needs to reference the manager._csvParser
     *
     * @param daysOfNote
     * @return {diary.collection.DayOfNoteCollection}
     */
	function mapDaysOfNote(daysOfNote, manifestFileUrl) {
		
		var manager = this;
			
		var supportedDaysOfNoteColumns = ["date","title","shaded","start_time","location","end_time","period",
			"include_in_cycle","set_cycle_day","set_cycle_week","set_cycle","set_term_wk","set_term"];

		var result = {
			daysOfNote: new diary.collection.DayOfNoteCollection(),
			events: new diary.collection.DiaryItemCollection()
		};
		
		// For each daysOfNote CSV fille defined in the manifest
		_.each(daysOfNote, function(dayOfNote) {
		
			var dayOfNoteFileUrl;
			
			// DayOfNote.csv files are located in the days folder relative to the manifest file
			dayOfNoteFileUrl = manifestFileUrl.resolvePath(dayOfNote.path);
			
			// read the CSV file
			var dayOfNoteFileData = readCSVFromFile.call(manager, dayOfNoteFileUrl, supportedDaysOfNoteColumns);
			
			
			// Convert the DD/MM/YYYY date string into a Date
			manager._csvParser.convertArrayFieldToDate(dayOfNoteFileData, "date");
			
			// Convert the HH:MM time strings into a Date
			manager._csvParser.convertArrayFieldToTime(dayOfNoteFileData, "start_time");
			manager._csvParser.convertArrayFieldToTime(dayOfNoteFileData, "end_time");
		
			// Items with a start_time column is a Diary Item.  All others are a DayOfNote		
			var isDiaryItem = function(day) {
//					//console.log('Diary Item: ' + (!isNaN(day.date) && ((day.start_time && day.end_time) || day.period)) + ' - ' + day.date + ' ' + day.start_time + ' ' + day.end_time + ' ' + day.period);
					return typeof day.start_time != 'undefined';
				},
				isDayOfNote = function(day) {
//					console.log('Day Of Note: ' + (!isNaN(day.date) && ((!day.start_time || !day.end_time) && !day.period)) + ' - ' + day.date + ' ' + day.start_time + ' ' + day.end_time + ' ' + day.period);
					return typeof day.start_time == 'undefined';
				};

			// Get the Day Of Notes defined, convert them to a DayOfNote and add them to the daysOfNoteCollection
            _.forEach( _.filter(dayOfNoteFileData, isDayOfNote),
                function(day) {

					var dayOfNote = new diary.model.DayOfNote(), newAttrs = {
						'date': day.date,
						'isGreyedOut': day.shaded && day.shaded.toUpperCase() == 'Y',
						'title': day.title
					};

                    if (day.set_cycle_day) {
                        var overrideCycleDay = parseInt(day.set_cycle_day, 10);
                        if (!isNaN(overrideCycleDay)) {
                            newAttrs.overrideCycleDay = overrideCycleDay;
                        }
                    }

                    if (day.include_in_cycle) {
						newAttrs.includeInCycle = day.include_in_cycle.toUpperCase() == 'Y';
                    }

					if (day.set_cycle) {
						var overrideCycle = parseInt(day.set_cycle, 10);
						if (!isNaN(overrideCycle)) {
							newAttrs.overrideCycle = overrideCycle;
						}
					}

					if(dayOfNote.set(newAttrs)) {
                       // console.log("adding day of note", JSON.stringify(dayOfNote.attributes));
						result.daysOfNote.add(dayOfNote);
					} else {
						//console.log("invalid day of note", JSON.stringify(dayOfNote.validate(newAttrs)));
					}
                }
            );

			// Get the Diary Item events defined, convert them to a Events and add them to the diaryItemEventsCollection

            _.forEach( _.filter(dayOfNoteFileData, isDiaryItem),
                function(day) {

                    var event = new diary.model.Event(),
                        newAttrs = {
						    'startDay': day.date,
						    'title': day.title,
						    'locked': true
					    };

					// if the start/end time were specified use them
					if (day.start_time && day.end_time) {

						newAttrs.startTime = day.start_time;
						newAttrs.endTime = day.end_time;

					}

					// only add the event if validation succeeds
                    if (event.set(newAttrs)) {
						//console.log("adding event", JSON.stringify(event.attributes));
						result.events.add(event);
                    } else {
						//console.log("invalid event", JSON.stringify(event.validate(newAttrs)));
                    }
                }
			);

		});
		
		return result;
		
	}
	
    /**
     * @constructor
     */
    var _manager = function() {
		this._csvParser = app.locator.getService(ServiceName.CSVPARSER);
    };


	_manager.prototype.parseSchoolProfile = function(manifestFile) {
		var manifestRaw = readObjectFromFile(manifestFile);
		var manifestFileUrl = manifestFile.resolvePath("..");
		return mapSchoolProfile(manifestRaw.schoolProfile, manifestFileUrl);
	}

    /**
     * Parse a manifest (.ddm) file.
     *
     * @param {!air.File} manifestFile The manifest file to parse
     * @param {?boolean} useLocalPaths Resolve content files to explicit local file system paths when using Preview mode
     */
	_manager.prototype.parseManifestFile = function(manifestFile, useLocalPaths) {

		var manager = this;
		var manifestRaw = readObjectFromFile(manifestFile);

		if (typeof(useLocalPaths) != "boolean") {
			useLocalPaths = false;
		}

		var manifestFileUrl = manifestFile.resolvePath("..");

		if (useLocalPaths) {  // TODO: Move this somewhere that makes more sense
			var contentFolderURL = manifestFileUrl,
					packagePaths = {},
					mapURL = function(item) {
						if (!item.url && item.packageId != "SCHOOL_INFO") {
							item.documentId = (packagePaths[item.packageId] || "") + (item.article || "index.html");
							item.url = contentFolderURL.url + "/" + (packagePaths[item.packageId] || "") + (item.article || "index.html");
							
							//console.log('setting documentid to ' + item.documentId);
						}
					};

			// map any packages
			_.forEach(manifestRaw.packages, function(p) {
				packagePaths[p.id] = p.path + "/";
			});

			// remap all articles to absolute paths in the file system
			_.forEach(manifestRaw.calendarBanner, mapURL);

			_.forEach(manifestRaw.shortcut, mapURL);

			for(var subitemId in manifestRaw.subItems) {
				_.forEach(manifestRaw.subItems[subitemId], mapURL);
			}
		}

		var daysOfNote = mapDaysOfNote.call(manager, manifestRaw.daysOfNote, manifestFileUrl);

		// return the  created manifest
		return new diary.model.Manifest({
			'contentMapping'    : mapContentMappings(manifestRaw.subItems, manifestRaw.calendarBanner, manifestRaw.shortcut),
			'contentPackages'   : mapContentPackages(manifestRaw.packages),
			'timetables'        : mapTimetables(manifestRaw.timetables, manifestFileUrl),
			'daysOfNote'		: daysOfNote.daysOfNote,
			'events'			: daysOfNote.events,
			// we get the schoolProfile separately so we can display the splash screen before parsing the rest of the manifest
//			'schoolProfile'     : mapSchoolProfile(manifestRaw.schoolProfile, manifestFileUrl),
			'timestamp'         : manifestRaw.timestamp || null
		});
	}

    // return the generated manifest manager
    return _manager;

})();

