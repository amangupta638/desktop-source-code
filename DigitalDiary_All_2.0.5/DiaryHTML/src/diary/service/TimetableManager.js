/**
 * The manager for Timetable Models.  The idea of the manager is to abstract the application from
 * the data access storage methods.  All the business logic that is related to a Task needs to
 * be in here
 * 
 * author - John Pearce
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.TimetableManager = (function() {
	
	/**
	 * Constructs a Timetable Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		this._timetableDelegate = app.locator.getService(ServiceName.TIMETABLEDELEGATE);
		this._subjectDelegate = app.locator.getService(ServiceName.SUBJECTDELEGATE);
	};
	
	/**
	 * Determines if the subject is in use or not.  The subject is linked to a diary item and a class, so if either one of the two has
	 * a reference to the subject the function returns true
	 * 
	 * @param {!number} subject The identifier of the subject that is being checked
	 * @param {!function(!boolean)} successCallback The function that is to be called upon successful determination if the subject is in use
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.isSubjectInUse = function(subjectId, successCallback, errorCallback) {
		this._subjectDelegate.isSubjectInUse(subjectId, successCallback, errorCallback);
	},
	
	/**
	 * Checks if the subject is unique (by subject title) and if so creates the subject otherwise it is ignored.
	 * NOTE - The success callback handler will return either the created subject or a subject that contains the same title
	 * 
	 * @param {!diary.model.Subject} subject The details of the subject that is being created
	 * @param {!function(!diary.model.Subject)} successCallback The function that is to be called upon successful call of the function
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.createSubject = function(subject, successCallback, errorCallback) {
		
		//console.log('timetable manager create subject', JSON.stringify(subject));
		
		// holds the manager itself
		var manager = this;
		
		// check that a valid subject was specified
		if (!subject) {
			throw new Error("Subject must be specified, subject is not optional");
		}
		
		if (!(subject instanceof diary.model.Subject)) {
			throw new Error("Subject is not valid, subject must be of type diary.model.Subject");
		}
		
		// check that the success function was specified
		if (!successCallback) {
			throw new Error("Success callback must be specified, callback is not optional");
		}
		
		// the error handler for the function
		function error(message) {
			if (errorCallback) {
				errorCallback(message);
			}
		}
		
		/*
		 * @param {!diary.model.Subject} subject The details of the subject that is being created
		 */
		function createSubject(subject) {
			
			// we need to first validate the given subject details, even if it was checked by some other
			// layer before the manager, that manager needs to make sure that it is a valid subject before saving it
			var validationError = subject.validate(subject.attributes);
			
			// if we have a problem with the validation we will not
			// save the new subject
			if (validationError != null) {
				// indicate that an error was encountered
				error(validationError);
			} else {
				// create the new subject
				manager._subjectDelegate.createSubject(subject, 
					
					// the success handler
					function (subjectId) {
						// populate the subject identifier and pass back
						subject.set({'id' : subjectId}, {'silent' : true});
						successCallback(subject);
					}, 
					
					// the error handler
					error
				);
			}
			
		};
		
		
		// determine if the subject is unique or not this will be done by querying the title
		// of the subject
		manager._subjectDelegate.subjectExistsWithTitle(subject.get('title'), subject.get('id'),
				
				// the success handler for the subject exists with title call
				function(existentSubject) {
		
					// if the subject exists we will read the subject and return it
					// else we will create a new subject
					if (existentSubject != null) {
                        // check if we have to update the subject colour
                        if (existentSubject.get('colour') != subject.get('colour')) {
                            existentSubject.set('colour', subject.get('colour'));
                            manager.updateSubject(existentSubject, successCallback, error);
                        } else {
                            successCallback(existentSubject);
                        }

					} else {
						createSubject(subject);
					}
			
				}, 
		
				// the error handler for the subject exists with title call
				error
				
		);
		
	};
	
	/**
	 * Deletes a subject from the system ONLY if it is not in use (not assigned)
	 * 
	 * @param {!diary.model.Subject} subject The subject that is to be deleted
	 * @param {!function(boolean)} successCallback The function that is to be called if the call is successful (TRUE if subject is deleted, FALSE otherwise) 
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.deleteSubject = function(subject, successCallback, errorCallback) {

		// the manager instance itself
		var maneger = this;
		
		// check that a valid subject was specified
		if (!subject) {
			throw new Error("Subject must be specified, subject is not optional");
		}
		
		if (!(subject instanceof diary.model.Subject)) {
			throw new Error("Subject is not valid, subject must be of type diary.model.Subject");
		}
		
		// check that the success function was specified
		if (!successCallback) {
			throw new Error("Success callback must be specified, callback is not optional");
		}
		
		// the error function
		function error(message) {
			if (errorCallback) {
				errorCallback(message);
			}
		}
		
		// We need to make sure that the subject is not being used
		// anymore before we delete it otherwise we will end up with items
		// pointing to a subject which doesn't exist
		// NOTE - At the time of writing this code the subject was being used by a diary item
		// and a class
		maneger.isSubjectInUse(subject.get('id'), 
				
				// the success operation of check if the subject is still in use
				function(isInUse) {
			
					// if the subject is in use we will not delete it
					if (!isInUse) {
						// delete the subject
						// delete the subject
						maneger._subjectDelegate.deleteSubject(subject.get('id'), 
						
								// successfully deleted a subject
								function() { 
									successCallback(true); 
								},
								
								// error function for deleting a subject
								error
						);
					} else {
						successCallback(false);
					}
					
				}, 
				
				// error function for checking if subject is still in use
				error
		
		);
		
	};
	
	/**
	 * Gets the list of subjects which are currently in the diary
	 * 
	 * @param {!function(!diary.collection.SubjectCollection)} successCallback The function that is to be called if all subjects are fetched
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.getSubjects = function(successCallback, errorCallback) {
		
		// check that both call backs were specified
		if (!successCallback) {
			throw new Error("Success Callback must be defined, callback is not optional");
		}
		
		// the success function
		function success(subjects) {
			// we just call the success handler
			successCallback(subjects);
		}
		
		// the error function
		function error(message) {
			// call the error function that was passed if any
			if (errorCallback) {
				errorCallback(message);
			}
		}
		
		// get the list of subjects
		this._subjectDelegate.getSubjects(success, error);
	};
	
	/**
	 * Update a subject with the specified new details
	 * 
	 * @param {!diary.model.Subject} subject The subject that is to be deleted
	 * @param {!function(!diary.model.Subject)} successCallback The function that is to be called if subject is deleted
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.updateSubject = function(subject, successCallback, errorCallback) {
		
		var _manager = this;
		
		// check that a valid subject was specified
		if (!subject) {
			throw new Error("Subject must be specified, subject is not optional");
		}
		
		if (!(subject instanceof diary.model.Subject)) {
			throw new Error("Subject is not valid, subject must be of type diary.model.Subject");
		}
		
		// check that the success function was specified
		if (!successCallback) {
			throw new Error("Success callback must be specified, callback is not optional");
		}
		
		// check that the specified subject details contain an identifier
		if (!subject.get('id') || subject.get('id') == null) {
			throw new Error("Subject is not valid, subject identifier must be specified");
		}
		
		// the success function
		function success() {
			// populate the subject identifier and pass back
			successCallback(subject);
		}
		
		// the error function
		function error(message) {
			if (errorCallback) {
				errorCallback(message);
			}
		}

		// we need to first validate the given subject details, even if it was checked by some other
		// layer before the manager, that manager needs to make sure that it is a valid subject before saving it
		var validationError = subject.validate(subject.attributes);
		
		// if we have a problem with the validation we will not
		// save the new subject
		if (validationError != null) {
			// indicate that an error was encountered
			error(validationError);
		} else {
			
			// check if we have a subject with the same title already if so we will not proceed with the update
			// of the subject
			// determine if the subject is unique or not this will be done by querying the title
			// of the subject
			_manager._subjectDelegate.subjectExistsWithTitle(subject.get('title'), subject.get('id'), 
					
					// the success handler for the subject exists with title call
					function(existentSubject) {
			
						// if a subject exists with the same title we will not update
						if (existentSubject != null && existentSubject.get('id') !== subject.get('id')) {
							// the subject details are not valid so we will stop the update
							error("Subject with title [" + subject.get('title') + "] already exists");
						} else {
							// update the subject details
							_manager._subjectDelegate.updateSubject(subject, success, error);
						}
				
					}, 
			
					// the error handler for the subject exists with title call
					error
					
			);
			
		}
		
	};
	
	/**
	 * Gets the list of unique rooms which are currently in the diary
	 * 
	 * @param {!function(!diary.collection.Collection)} successCallback The function that is to be called if the rooms are fetched
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.getUniqueRooms = function(successCallback, errorCallback) {
		
		// check that both call backs were specified
		if (!successCallback) {
			throw new Error("Success Callback must be defined, callback is not optional");
		}
		
		// the success function
		function success(rooms) {
			// we just call the success handler
			successCallback(rooms);
		}
		
		// the error function
		function error(message) {
			// call the error function that was passed if any
			if (errorCallback) {
				errorCallback(message);
			}
		}
		
		// get the list of rooms
		this._timetableDelegate.getUniqueRooms(success, error);
	};
	/**
		Adding room
	**/
	_manager.prototype.addRoomtoTimetable=function(room,successCallback,errorCallback)
	{
		this._timetableDelegate.addRoomtoTimetable(room,
			
			//success
			function (roomID)
			{
				successCallback(roomID);
			},
			function (errorMessage) {
				if (errorCallback) {
					errorCallback(errorMessage);
				}
			}
		);
		
	}
	/**
	 * Add a new class to an existing timetable.
	 * 
	 * IMPORTANT: This assumes the subject and period on the class both already exist.
	 * 
	 * @param {!diary.model.Timetable|number} timetable Timetable ID or model. If a model is passed, its classes attribute will be updated on success.
	 * @param {!diary.model.Class} classModel The class to be added. Upon success it's identifier will be updated.
	 * @param {!function(diary.model.Timetable|number, diary.model.Class} successCallback Called upon success with the modified timetable (or ID) and class.
	 * @param {?function(string)} errorCallback Called upon failure with an error message.
	 */
	_manager.prototype.addClassToTimetable = function (timetable, classModel, successCallback, errorCallback) {
		
		// check parameters
		if (typeof timetable !== "number" && !(timetable instanceof diary.model.Timetable)) {
			throw new Error("timetable must be of type identifier or timetable model");
		}
		if (!(classModel instanceof diary.model.Class)) {
			throw new Error("class must be of type class model");
		}
		if (typeof successCallback !== "function") {
			throw new Error("success callback must be defined");
		}
		if (classModel.id) {
			throw new Error("class cannot have an ID before it is added to the DB");
		}
		if (!classModel.get('subject')) {
			throw new Error("class must have a subject");
		}
		
		this._timetableDelegate.addClassToTimetable(
				(typeof timetable === "number" ? timetable : timetable.id),
				classModel,
				
				// success
				function (classId) {
					
					// update the class and timetable appropriately
					classModel.set('id', classId);
					if (typeof timetable !== "number") {
						timetable.get('classes').add(classModel);
					}
					
					// return
					successCallback(classId, timetable, classModel);
				},
				
				// error
				function (errorMessage) {
					if (errorCallback) {
						errorCallback(errorMessage);
					}
				}
		);
		
	};
	
	/**
	 * Remove an existing class from an existing timetable.
	 * 
	 * @param {!diary.model.Timetable|number} timetable Timetable ID or model. If a model, it will be modified upon success.
	 * @param {!diary.model.Class|number} classModel Class ID or model.
	 * @param {!function(diary.model.Timetable|number)} successCallback Called upon success with the modified timetable (or id).
	 * @param {?function(string)} errorCallback Called upon failure with an error message.
	 */
	_manager.prototype.removeClassFromTimetable = function(timetable, classModel, successCallback, errorCallback) {
		
		var timetableId, classId;
		
		// check parameters
		if (typeof timetable !== "number" && !(timetable instanceof diary.model.Timetable)) {
			throw new Error("timetable must be of type identifier or timetable model");
		}
		if (typeof classModel !== "number" && !(classModel instanceof diary.model.Class)) {
			throw new Error("class must be of type identifier or class model");
		}
		if (typeof successCallback !== "function") {
			throw new Error("success callback must be defined");
		}
		
		timetableId = (typeof timetable === "number" ? timetable : timetable.id);
		classId = (typeof classModel === "number" ? classModel : classModel.id);
		
		if (!timetableId) {
			throw new Error("Timetable id must be valid");
		}
		if (!classId) {
			throw new Error("Class id must be valid");
		}
		if (typeof timetable !== "number" && !timetable.get('classes').get(classId)) {
			throw new Error("Timetable must contain the class which is to be removed");
		}
		
		this._timetableDelegate.removeClassFromTimetable(
				classId,
				
				// success
				function () {
					// if we have a timetable model, update it
					if (typeof timetable !== "number") {
						timetable.get('classes').remove(classId);
					}
					// return success
					successCallback(timetable);
				},
				
				// error
				function (errorMessage) {
					if (errorCallback) {
						errorCallback(errorMessage);
					}
				}
		);
	};
	
	/**
	 * Update a class in a timetable. 
	 * 
	 * 
	 * @param {!diary.model.Timetable|number} timetable Timetable model or ID. If a model, it will be updated.
	 * @param {!diary.model.Class} classModel Class model to be used to replace the existing model with the same ID.
	 * @param {!function(diary.model.Timetable|number, diary.model.Class)} successCallback Called upon success, with the modified timetable (or id) and class.
	 * @param {?function(string)} errorCallback Called upon failure with error message.
	 */
	_manager.prototype.updateClassInTimetable = function(timetable, classModel, room, isAdmin, classUniqueId, successCallback, errorCallback) {
		
		// check parameters
		if (typeof timetable !== "number" && !(timetable instanceof diary.model.Timetable)) {
			throw new Error("timetable must be of type identifier or timetable model");
		}
		if (!(classModel instanceof diary.model.Class)) {
			throw new Error("class must be of type class model");
		}
		if (typeof successCallback !== "function") {
			throw new Error("success callback must be defined");
		}
		if (typeof timetable !== "number" && !timetable.get('classes').get(classModel.id)) {
			throw new Error("Timetable must contain the class which is to be updated");
		}
		if (!classModel.id) {
			throw new Error("Class id must be valid");
		}
		// check that subject is defined 
		if (!classModel.get('subject')) {
			throw new Error("Class subject must be defined");
		}
	
		if(isAdmin && isAdmin == 'true'){
			this._timetableDelegate.updateAdminClass(
					(typeof timetable === "number" ? timetable : timetable.id),
					classModel,
					classUniqueId,
					
					// success
					function () {
						// update timetable
						if (typeof timetable !== "number") {
							classModel.attributes.room=room;
							timetable.get('classes').get(classModel.get('id')).set(classModel.attributes);
						}
						
						getAllClasses();
						// return
						
						successCallback(timetable, classModel);
						
						
					},
					
					// error
					function (errorMessage) {
						if (errorCallback) {
							errorCallback(errorMessage);
						}
					}
			);
			
		} else {
			this._timetableDelegate.updateClass(
					(typeof timetable === "number" ? timetable : timetable.id),
					classModel,
					
					// success
					function () {
						// update timetable
						if (typeof timetable !== "number") {
							classModel.attributes.room=room;
							timetable.get('classes').get(classModel.get('id')).set(classModel.attributes);
						}
						
						getAllClasses();
						// return
						
						successCallback(timetable, classModel);
						
						
					},
					
					// error
					function (errorMessage) {
						if (errorCallback) {
							errorCallback(errorMessage);
						}
					}
			);
		}
		
	};
	
	/**
	 * Gets the timetable which is current at a particular day.
	 * 
	 * @param {!Rocketboots.date.Day} day The day at which the returned timetable will be current
	 * @param {!diary.collection.SubjectCollection} subjects The list of subjects that are to be related to the loaded classes
	 * @param {function(?diary.model.Timetable)} successCallback The function that is to be called if all overdue tasks are fetched
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.getCurrentTimetable = function(day, subjects, successCallback, errorCallback) {
		//console.log('TimetableManager.getCurrentTimetable(', day.getKey(), ')');
		
		if (!day) {
			throw new Error("day is required");
		}
		
		if (!subjects) {
			throw new Error("subjects is required");
		}
		
		// check that both call backs were specified
		if (!successCallback) {
			throw new Error("successCallback must be defined, callback is not optional");
		}

		// check the day argument
		if (!(day instanceof Rocketboots.date.Day)) {
			throw new Error("day must be of type Rocketboots.date.Day");
		}
		
		// fetch the timetable via the delegate
		this._timetableDelegate.getCurrentTimetable(day, subjects, success, error);
		
		// the success function
		function success(timetable) {
			//console.log('TimetableManager.getCurrentTimetable::success()');
			
			// just call the success callback with the timetable
			successCallback(timetable);
		}
		
		// the error function
		function error(message) {
			//console.log('TimetableManager.getCurrentTimetable::error()');
			
			// call the error function that was passed if any
			if (errorCallback) {
				errorCallback(message);
			}	
		}
		
	};
	
	/**
	 * Gets the timetables for a given day range
	 * 
	 * @param {!Rocketboots.date.Day} startDay The day from where we need to get the timetables
	 * @param {!Rocketboots.date.Day} endDay The day to where we need to get the timetables
	 * @param {!diary.collection.SubjectCollection} subjects The list of subjects that are to be related to the loaded classes
	 * @param {function(!diary.collection.TimetableCollection)} successCallback The function that is to be called if all overdue tasks are fetched
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.getTimetables = function(startDay, endDay, subjects, successCallback, errorCallback) {
		//console.log('TimetableManager.getTimetables('+ startDay+ ', '+ endDay+')');
		
		// check that the days were specified
		if (!startDay) {
			throw new Error("startDay is required");
		}
		
		if (!endDay) {
			throw new Error("endDay is requied");
		}
		
		if (!subjects) {
			throw new Error("subjects is required");
		}
		
		// check that both call backs were specified
		if (!successCallback) {
			throw new Error("successCallback is required");
		}
		
		// check the startDay argument
		if (!(startDay instanceof Rocketboots.date.Day)) {
			throw new Error("startDay must be of type Rocketboots.date.Day");
		}
		
		// check the endDay argument
		if (!(endDay instanceof Rocketboots.date.Day)) {
			throw new Error("endDay must be of type  Rocketboots.date.Day");
		}
		
		// fetch the timetables via the delegate
		this._timetableDelegate.getTimetables(startDay, endDay, subjects, success, error);
		
		// the success function
		function success(timetables) {
			//console.log('TimetableManager.getTimetables::success()');
			
			// just call the success callback with the timetable
			successCallback(timetables);
		}
		
		// the error function
		function error(message) {
			//console.log('TimetableManager.getTimetables::error()');
			
			// call the error function that was passed if any
			if (errorCallback) {
				errorCallback(message);
			}	
		}
		
	};

    /**
     * Gets all the (base) timetable records, without mapping periods and classes
     *
     * @param successCallback
     * @param errorCallback
     */
    _manager.prototype.getAllTimetables = function(successCallback, errorCallback) {
        if (typeof(successCallback) != "function") {
            throw Error("Success Callback must be defined, callback is not optional");
        }

        // the success function
        function success(timetables) {
           // console.log('TimetableManager.getAllTimetables::success()');


            // just call the success callback with the timetable
            successCallback(timetables);
        }

        // the error function
        function error(message) {
          //  console.log('TimetableManager.getAllTimetables::error()');

            // call the error function that was passed if any
            if (errorCallback) {
                errorCallback(message);
            }
        }

        this._timetableDelegate.getAllTimetables(success, error);
    };


	/**
	 * Add a timetable model to the database.
	 *
	 * Note that an ID will be automatically assigned (any ID on the model passed in will be ignored),
	 * and the timetable model passed in will have its ID adjusted to the new value.
	 *
	 * @param timetable The timetable to be added.
	 * @param successCallback Called upon success, with the adjusted timetable model passed as argument.
	 * @param errorCallback Called upon failure, with a generic error message passed as argument.
	 *
	 * @throws {Error} Errors if the arguments are not of the expected types.
	 */
    _manager.prototype.addTimetable = function(timetable, successCallback, errorCallback) {
        if (!(timetable instanceof diary.model.Timetable)) {
            throw Error("Timetable must be of type diary.model.Timetable");
        }

        if (typeof(successCallback) != "function") {
            throw Error("Success Callback must be defined, callback is not optional");
        }

        // the success function
        function success(timetable) {
           // console.log('TimetableManager.addTimetable::success()');


            // just call the success callback with the updated timetable
            successCallback(timetable);
        }

        // the error function
        function error(message) {
          //  console.log('TimetableManager.addTimetable::error()');

            // call the error function that was passed if any
            if (errorCallback) {
                errorCallback(message);
            }
        }

        var validationError = timetable.validate(timetable.attributes);

        if (validationError != null) {
            errorCallback(validationError);
        } else {
            this._timetableDelegate.addTimetable(timetable, success, error);
        }
    };
	
	// return the generated task manager
	return _manager;
		
})();
