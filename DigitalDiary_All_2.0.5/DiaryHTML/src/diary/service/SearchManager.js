/**
 * The manager for search the diary application.  This will take care to query the different parts of the application and produce
 * a search result back for different searches
 *
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SearchManager = (function() {

	/**
	 * Constructs a Search Manager.
	 *
	 * @constructor
	 */
	var _manager = function() {
		this._diaryItemDelegate = app.locator.getService(ServiceName.DIARYITEMDELEGATE);
		this._documentDelegate = app.locator.getService(ServiceName.DOCUMENTDELEGATE);
		this._schoolProfileDelegate = app.locator.getService(ServiceName.SCHOOLPROFILEDELEGATE);
		this._studentProfileDelegate = app.locator.getService(ServiceName.STUDENTPROFILEDELEGATE);
		this._classDelegate = app.locator.getService(ServiceName.CLASSDELEGATE);
		this._subjectDelegate = app.locator.getService(ServiceName.SUBJECTDELEGATE);
		this._cache = new diary.service.SearchResultsCacheManager();
		this._stemming = new diary.service.StemmerManager();
	};

	/**
	 * Searches Diary Items for the provided criteria.
	 *
	 * @param {!string} criteria The search criteria that is to be used to search the diary items
	 * @param {!function(diaryItemIds)} successCallback The function that is to be called on successful Diary Items search
	 * @param {?function(message)} errorCallback The function that is to be called on failure [OPTIONAL]
	 */
	function searchDiaryItems(criteria, successCallback, errorCallback) {

		this._diaryItemDelegate.searchDiaryItems(criteria,

		// success
		successCallback,

		// error
		errorCallback || $.noop);
	}
	
	function searchClasses(criteria, successCallback, errorCallback) {

		this._classDelegate.searchClasses(criteria,

		// success
		successCallback,

		// error
		errorCallback || $.noop);
	}
	
	function searchSubjects(criteria, successCallback, errorCallback) {

		this._subjectDelegate.searchSubjects(criteria,

		// success
		successCallback,

		// error
		errorCallback || $.noop);
		
	}

	
	
	/**
	 * Searches Documents for the provided criteria.
	 *
	 * @param {!string} criteria The search criteria that is to be used to search the diary items
	 * @param {!function(documents)} successCallback The function that is to be called on successful Documents search
	 * @param {?function(message)} errorCallback The function that is to be called on failure [OPTIONAL]
	 */
	function searchDocuments(criteria, successCallback, errorCallback) {

		// stem search criteria
		var stemmedWords = this._stemming.stemmifyPhrase(criteria);

		//console.log("criteria", criteria, "stemmedWords", stemmedWords);

		// if no stemmed words exist, return with no results
		if (!stemmedWords || stemmedWords.length == 0) {

			successCallback(new diary.collection.DocumentCollection());

		} else {

			this._documentDelegate.searchDocuments(stemmedWords,

			// success
			successCallback,

			// error
			errorCallback || $.noop);

		}
	}

	/**
	 * Clears the cache for the search results
	 */
	_manager.prototype.clearSearchResultsCache = function() {
		//console.log("SearchManager::clearSearchResultsCache()");
		this._cache.clear();
	};

	/**
	 * Searches Diary Items, Content Items, School Profile and Student Profile.
	 *
	 * @param {!string} criteria The search criteria that is to be used to search the diary items
	 * @param {!diary.collection.DiaryItemCollection} diaryItems The diary items that are to be searched
	 * @param {!diary.collection.ContentItemCollection} articles The articles that are to be searched
	 * @param {!function(diary.model.SearchResult} successCallback The function that is to be called on successful diary search
	 * @param {?function(message)} errorCallback The function that is to be called on failure [OPTIONAL]
	 */
	_manager.prototype.searchDiary = function(criteria, diaryItems, articles, successCallback, errorCallback) {
		//console.log("SearchManager::searchDiary('" + criteria+ "', <" + diaryItems.length + " DiaryItems>, <" + articles.length + " ContentItems>)");

		var searchResult, manager = this;

		// validate criteria
		if (!criteria) {
			throw new Error("Criteria must be defined, criteria is not optional");
		}

		// validate diaryItems
		if (!diaryItems) {
			throw new Error("Diary Items must be defined, diary items is not optional");
		}

		if (!( diaryItems instanceof diary.collection.DiaryItemCollection)) {
			throw new Error("Diary Items is not valid, diary items must be of type diary.collection.DiaryItemCollection");
		}

		// validate articles
		if (!articles) {
			throw new Error("Articles must be defined, articles is not optional");
		}

		if (!( articles instanceof diary.collection.ContentItemCollection)) {
			throw new Error("Articles is not valid, articles must be of type diary.collection.ContentItemCollection");
		}

		criteria = criteria.toLowerCase().split(/[^a-z0-9\']/).join(" ").trim();

		// return the cached result if it already exists
		searchResult = manager._cache.search(criteria);
		if (searchResult != null) {
			//console.log("Found cached search result");
			successCallback(searchResult);
			return;
		}

		// create the search criteria that will be used for searching
		searchResult = new diary.model.SearchResult({
			'criteria' : criteria
		});

		// start by searching the diary items
		//console.log("Searching Diary Items");
		searchDiaryItems.call(manager, criteria,

		// success
		function(diaryItemIds) {
			//console.log("Diary Items results found: " + diaryItemIds.length);

			var tasks = new diary.collection.DiaryItemCollection(), events = new diary.collection.DiaryItemCollection(), messages = new diary.collection.DiaryItemCollection(), notes = new diary.collection.DiaryItemCollection();

			_.forEach(diaryItemIds, function(diaryItemId) {
				var diaryItem = diaryItems.get(diaryItemId);

				if (diaryItem != null) {
					if ( diaryItem instanceof diary.model.Task) {
						tasks.add(diaryItem);
					} else if ( diaryItem instanceof diary.model.Event) {
						events.add(diaryItem);
					} else if ( diaryItem instanceof diary.model.Message) {
						messages.add(diaryItem);
					} else if ( diaryItem instanceof diary.model.Note) {
						notes.add(diaryItem);
					}
				}
			});

			// inject the results inside the search result
			// start with the TASKS
			if (tasks.length != 0) {

				var overdueTasks = tasks.getOverdueTasks(), nonOverdueTasks = tasks.getNonOverdueTasks();

				if (overdueTasks.length > 0) {
					searchResult.get("results").add(new diary.model.SearchResultSection({
						'heading' : "Overdue Tasks",
						'itemType' : diary.model.SearchResultSection.ITEMTYPE.TASK,
						'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.DIARYITEM,
						'sectionResults' : overdueTasks.toArray()
					}));
				}

				if (nonOverdueTasks.length > 0) {
					searchResult.get("results").add(new diary.model.SearchResultSection({
						'heading' : "Tasks",
						'itemType' : diary.model.SearchResultSection.ITEMTYPE.TASK,
						'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.DIARYITEM,
						'sectionResults' : nonOverdueTasks.toArray()
					}));
				}
			}

			// then the MESSAGES
			if (messages.length != 0) {
				searchResult.get("results").add(new diary.model.SearchResultSection({
					'heading' : "Messages",
					'itemType' : diary.model.SearchResultSection.ITEMTYPE.MESSAGE,
					'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.DIARYITEM,
					'sectionResults' : messages.toArray()
				}));
			}

			// then the EVENTS
			if (events.length != 0) {
				searchResult.get("results").add(new diary.model.SearchResultSection({
					'heading' : "Events",
					'itemType' : diary.model.SearchResultSection.ITEMTYPE.EVENT,
					'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.DIARYITEM,
					'sectionResults' : events.toArray()
				}));
			}

			// finally the NOTES
			if (notes.length != 0) {
				searchResult.get("results").add(new diary.model.SearchResultSection({
					'heading' : "Notes",
					'itemType' : diary.model.SearchResultSection.ITEMTYPE.NOTE,
					'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.DIARYITEM,
					'sectionResults' : notes.toArray()
				}));
			}

			//search class or subject
			searchClasses.call(manager, criteria,

			// success
			function success(classIds) {
				
				  var classes = app.model.get(ModelName.CLASSES);
				 // console.log("classId : "+JSON.stringify(classes));
				  var ObjClasses = new diary.collection.ClassCollection()
				_.forEach(classIds, function(classId) {
					console.log("classId : "+classId);
					
					var objClass = getObjectByValue(classes, classId, "classId");
					 console.log("objClass : "+objClass);
					if (objClass != null) {
						if (objClass instanceof diary.model.Class) {
							ObjClasses.add(objClass);
						}
					}
				});
				  
				// holds the results of all categories
				  if (ObjClasses .length != 0) {
						searchResult.get("results").add(new diary.model.SearchResultSection({
							'heading' : "Classes",
							'itemType' : diary.model.SearchResultSection.ITEMTYPE.CLASS,
							'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.CLASSES,
							'sectionResults' : ObjClasses.toArray()
						}));
					}
				

				searchSubjects.call(manager, criteria,

				// success
				function success(subjectIds) {
					  var subjects = app.model.get(ModelName.SUBJECTS);
					  var objSubjects = new diary.collection.SubjectCollection()
					// process results
					_.forEach(subjectIds, function(subjectId) {
						var objSubject = subjects.get(subjectId);
						if ( objSubject instanceof diary.model.Subject) {
							objSubjects.add(objSubject);
						}
					});
					if (objSubjects.length != 0) {
							searchResult.get("results").add(new diary.model.SearchResultSection({
								'heading' : "Subjects",
								'itemType' : diary.model.SearchResultSection.ITEMTYPE.SUBJECT,
								'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.SUBJECTS,
								'sectionResults' : objSubjects.toArray()
							}));
					}
					
					
					// next, search documents
					//console.log("Searching Documents " + criteria);
					searchDocuments.call(manager, criteria,
					// success
					function success(documents) {
						//console.log("Documents results found: " + documents.length);

						// holds the results of all categories
						var results = {
							'SCHOOL' : {
								'title' : "School Info",
								'itemType' : diary.model.SearchResultSection.ITEMTYPE.SCHOOL,
								'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.CONTENTITEM,
								'results' : new diary.collection.ContentItemCollection()
							},
							'EMPOWER' : {
								'title' : "Empower",
								'itemType' : diary.model.SearchResultSection.ITEMTYPE.EMPOWER,
								'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.CONTENTITEM,
								'results' : new diary.collection.ContentItemCollection()
							},
							'EMPOWER_BANNER' : {
								'title' : "Empower",
								'itemType' : diary.model.SearchResultSection.ITEMTYPE.BANNER,
								'resultsType' : diary.model.SearchResultSection.RESULTSTYPE.CONTENTITEM,
								'results' : new diary.collection.ContentItemCollection()
							}
						};

						// confirm each found document actually exists in collection being searched
						_.forEach(documents.toArray(), function(testDocument) {
							var article = null;

							articles.any(function(testArticle) {
								if (testArticle.get('documentId') == testDocument.get('documentId')) {
									article = testArticle;
									return true;
								}

								return false;
							});

							// if the article was found we will include it in the results
							if (article != null) {
								results[testDocument.get('type')].results.add(article);
							}
						});

						// search School Profile and Student Profile, if necessary
						var includeInfo = false;

						if (articles.get('SCHOOL_INFO')) {
							//console.log("Searching School Profile");
							manager._schoolProfileDelegate.searchSchoolProfile(criteria,

							// success
							function(matches) {
								includeInfo = (matches > 0);
								if (includeInfo)
									console.log("School Profile matches search criteria");
							},

							// error
							errorCallback || $.noop);

							if (!includeInfo) {
								//console.log("Searching Student Profile");
								manager._studentProfileDelegate.searchStudentProfile(criteria,

								// success
								function(matches) {
									includeInfo = (matches > 0);
									if (includeInfo)
										console.log("Student Profile matches search criteria");
								},

								// error
								errorCallback || $.noop);
							}
						}

						if (includeInfo) {
							results['SCHOOL'].results.add(articles.get('SCHOOL_INFO'));
						}

						// process results
						_.forEach(results, function(result) {
							if (result.results.length > 0) {
								
								// create the search criteria for the section
								searchResult.get("results").add(new diary.model.SearchResultSection({
									'heading' : result.title,
									'itemType' : result.itemType,
									'resultsType' : result.resultsType,
									'sectionResults' : result.results.toArray()
								}));
							}
						});
						//console.log("searchResult : "+JSON.stringify(searchResult));
						// cache search result
						manager._cache.add(searchResult);

						// return
						successCallback(searchResult);
					},

					// error
					errorCallback || $.noop);
				},

				// error
				errorCallback || $.noop);
			},

			// error
			errorCallback || $.noop);
		},

		// error
		errorCallback || $.noop);

	};

	return _manager;
})();
