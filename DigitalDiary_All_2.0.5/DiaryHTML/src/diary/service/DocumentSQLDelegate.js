/**
 * The local SQL delegate for Document. The delegate to search content items
 * (articles) that are downloaded from the a remote location.
 * 
 * author - Brian Bason (aka Justin Judd)
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DocumentSQLDelegate = (function() {

	/**
	 * Constructs a Document Delegate.
	 * 
	 * @constructor
	 */
	var _delegate = function() {
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};

	/*
	 * Maps a document from the db record set to the model in the application
	 * 
	 * @param {Object} dbRecord The record that was read from the database
	 * 
	 * @return {diary.model.Document} The model object in the application
	 */
	function documentMapper(dbRecord) {
		return new diary.model.Document({
			'id' : dbRecord.id,
			'type' : dbRecord.type,
			'title' : dbRecord.title,
			'documentId' : dbRecord.documentId
		});
	}

	/**
	 * Delete all the documents not in the supplied packages
	 *
	 * @param packageIds an array of packageIds that we want to keep
	 * @param successCallback
	 * @param errorCallback
	 */
	_delegate.prototype.deleteDocumentsNotInPackages = function(packageIds, successCallback, errorCallback) {
		var delegate = this;
		delegate._database.runQuery(
				// query
				"SELECT * FROM document" + (packageIds.length > 0 ? (" WHERE " + _.map(packageIds, function(pId) { return  "documentId NOT LIKE '" + pId + "/%'";}).join(' AND ')) : ""),

				// params
				{},

				// success
				function(statement) {
					var result = statement.getResult().data;
					if(result && result.length > 0) {
						var documentIds = _.map(result, function(d) { return d.id; }).join(",");
						delegate._database.runQuery(
								// query
								"DELETE FROM occurs WHERE documentId IN(" + documentIds + ")",

								// params
								{},

								// success
								function() {
									delegate._database.runQuery(
											// query
											"DELETE FROM document WHERE documentId IN(" + documentIds + ")",

											// params
											{},

											// success
											successCallback,

											// error
											errorCallback, "Failed to delete documents not in packageIds [" + packageIds.join(',') + "]"
									);
								},

								// error
								errorCallback, "Failed to delete from occurs for documents not in packageIds [" + packageIds.join(',') + "]"
						);
					} else {
						successCallback();
					}
				},

				// error
				errorCallback, "Failed to select documents not in packageIds [" + packageIds.join(',') + "]"
		);
	};


	_delegate.prototype.deleteDocument = function(documentId, successCallback, errorCallback) {
		var delegate = this, params = {'documentId' : documentId};
		delegate._database.runQuery(
				// query
				"DELETE FROM occurs WHERE documentId = :documentId",

				// params
				params,

				// success
				function() {
					delegate._database.runQuery(
							// query
							"DELETE FROM document WHERE ID = :documentId",

							// params
							params,

							// success
							successCallback || $.noop,

							// error
							errorCallback, "Failed to delete document with id:" + documentId
					);
				},

				// error
				errorCallback, "Failed to delete document with id:" + documentId
		);
	};

	_delegate.prototype.addDocument = function(document, words, successCallback, errorCallback) {
		// is there as existing document with this documentId (path) ?
		var delegate = this;
		delegate._database.runQuery(
				// query
				"SELECT * FROM document WHERE documentId = :documentId",

				// params
				{documentId: document.get('documentId')},

				// success
				function(statement) {
					// if there is already a document with this documentId, get rid of all the entries first
					var result = statement.getResult().data;
					if(result && result.length > 0) {
						delegate.deleteDocument(result[0].id, insertDocument, errorCallback);
					} else {
						insertDocument();
					}
				},

				// error
				errorCallback, "Error while trying to add document"
		);

		function insertDocument() {
			delegate._database.runQuery(
					// query
					"INSERT INTO document (type, title, documentId) VALUES (:type,:title,:documentId)",

					// params
					{
						type: document.get('type'),
						title: document.get('title'),
						documentId: document.get('documentId')
					},

					// success
					function(statement) {
						document.set('id', statement.getResult().lastInsertRowID);
						if(words) {
							//delegate.indexDocument(document, words, successCallback, errorCallback);
						} else {
							successCallback();
						}
					},

					// error
					errorCallback, "Could not add new document to database"
			);
		}
	};

	/**
	 * Store the index of stemmed search terms
	 */
	_delegate.prototype.indexDocument = function(theDocument, words, successCallback, errorCallback) {
	
		var delegate = this;
		if (words.length == 0) {
			// delete the index
			delegate.runQuery(
					// query
					'DELETE FROM occurs WHERE documentId = :documentId',

					// params
					{documentId: theDocument.get('id')},

					// success
					successCallback || $.noop,

					// error
					errorCallback, "Error indexing document"
			);
		} else {
			delegate._database.runScripts(_.map(words, function (word) {
				return { 
					'sql' : "INSERT OR IGNORE INTO stemmedWords (word) VALUES (:word)",
					'params' : { 'word' : word }
				};
			}), checkExistingWords, errorCallback);
		}
		
		function checkExistingWords() {
		
			var params = {},
				query = "SELECT sw.id, sw.word " +
						"FROM stemmedWords sw " +
						"WHERE sw.word IN ( ";
						
			for(var i=0; i<words.length; i++) {
				if (i) query += ",";
				query += ":param" + i;
				params["param" + i] = words[i]
			}
			
			query += ")";
			
			delegate._database.runQuery(query, params, gotExistingWords, errorCallback);
		}
		
		
		function gotExistingWords(statement) {
			var scripts = [
					{sql: 'DELETE FROM occurs WHERE documentId = :documentId', params: {documentId: theDocument.get('id')}}
				];
			
			var stemmedDb = statement.getResult().data;
			
			// Get our stemmed word ids
			var stemmedIndex = {};
			_.each(stemmedDb, function(record) {
				stemmedIndex[record.word] = record.id;
			});
			
			_.each(words, function (word) {
				scripts.push({
					'sql' : "INSERT OR IGNORE INTO occurs (documentId, stemmedWordId) VALUES (:documentId, :stemmedWordId)",
					'params' : { 
						'documentId' : theDocument.get('id'), 
						'stemmedWordId' : stemmedIndex[word]
					}
				});
			});
			
			delegate._database.runScripts(scripts, successCallback, errorCallback);
		}
	};

	_delegate.prototype.searchDocuments = function(words, successCallback, errorCallback) {
		// check that the words array has values
		if (!words || words.length < 1) {
			throw new Error("The array of Words must not be empty");
		}

		this._database.runQuery(
				// query
				"SELECT * from document WHERE id IN (" +
						_.map(words, function(w) {
							return "SELECT DISTINCT o.documentId FROM occurs AS o " +
									"LEFT OUTER JOIN stemmedWords AS sw ON o.stemmedWordId = sw.id " +
									"WHERE sw.word LIKE '" + w + "%'";
						}).join(" INTERSECT ") + ")",

				// params
				{},

				// success
				function(statement) {
                    successCallback(new diary.collection.DocumentCollection(_.map(statement.getResult().data, function(item){
						return documentMapper(item);
					})));
				},

				// error
				errorCallback, "Error while trying to read documents"
		);
	};

	// return the generated document delegate
	return _delegate;

})();
