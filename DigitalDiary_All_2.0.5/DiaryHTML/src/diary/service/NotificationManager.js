/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: NotificationManager.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.service = diary.service || {};

diary.service.NotificationManager = (function() {

	var MAX_TASK_TITLE_CHARS = 20;

	/**
	 * Constructor.
	 */
	var _manager = function() {
	};

	/**
	 * Generate all the pending notifications. The manager will generate an event for
	 * every notification that is generated.
	 */
	_manager.prototype.generatePendingNotifications = function() {
		//console.log("diary.service.NotificationManager::generatePendingNotifications()");

		// holds the list of all the over due tasks
		var overDueTasks;

		try {

			// get the list of overdue tasks
			overDueTasks = app.model.get(ModelName.DIARYITEMS).getOverdueTasks();

			// sort them by the due date and time
			overDueTasks.comparator = function(task) {
				// if it is due for all day we will use midnight as the due time so that the task will be sorted in
				//alert(task.get('dueDay').getKey() + ((task.get('dueTime'))));
				// the beginning of the list
				return task.get('dueDay').getKey() + ((task.get('dueTime')) ? task.get('dueTime').toString() : "0000");
			};
			overDueTasks.sort({silent : true});

			// for each found overdue task generate a notification and send the events
			overDueTasks.forEach(function(task) {
					var duetimechange=0
					//console.log("DUETIME="+task.get('dueTime'))
					//console.log("DUEDAY="+task.get('dueDay'));
					
					if(task.get('dueTime')!=null)
					{
						//var timeset=new String(task.get('dueTime'));
						var timeset=convertTime(task.get('dueTime'),task.get('dueDay'));
						//console.log("timeset : "+timeset);
						var firsthr=timeset.substr(0,2)

						var firsmin=timeset.substr(2,4)
						

						duetimechange=firsthr*60*60*1000+firsmin*60*1000;
						//alert(firsthr+" v  firsthr     "+firsmin+"          firsmin    "+firsthr*60*60+"     "+firsmin*60)
					}
					//console.log(new Date()+"        " +task.get('dueDay').getDate())

					var checktime= (((new Date() - task.get('dueDay').getDate())-duetimechange) / 86400000)*24;

					if(checktime>24)
					{checktime=Math.ceil(Math.round(checktime)/24)
						checktime=checktime+"days"
	
					}else 
					{
	
						if(checktime<(1-0.44))
						{
							checktime=parseInt(checktime*100);
	
							checktime=checktime+"mins"
						}else{
							checktime=Math.ceil(checktime);
							checktime=checktime+"hrs"
						}
	
	
	
					}



				var dueDate = task.get('dueDay').getDate(),
					title = "Overdue " + task.get("type").toLowerCase(),
					overdueDays=checktime,
					message = "'" + Rocketboots.util.StringUtil.condense(task.get("title"), MAX_TASK_TITLE_CHARS)
						+ "' is overdue by " + overdueDays ,
					notification = new diary.model.Notification({
						"title" : title,
						"message" : message,
						"colour" : task.get('subject') && task.get('subject').get('colour') ? task.get('subject').get('colour').toLowerCase() : null,
						"type" : task.get('type').toLowerCase()
					});

				// raise the event for the notification
				app.context.trigger(EventName.NEWNOTIFICATION, notification);

			});
		} catch (error) {
			console.log("failed to generate notifications", error);
			return false;
		}

		return true;
	};

	return _manager;

})();
