/**
 * The manager for handling class data
 *
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SubjectManager = (function() {

	var _manager = function() {
		this._delegate = app.locator.getService(ServiceName.SUBJECTDELEGATE);
	};

	
	
	// return the generated manager
	return _manager;

})();

