/**
 * The local SQL delegate for System Configuration.  The delegate will use SQL queries to actually
 * get data from a local database. 
 * 
 * author - Jane Sivieng
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SystemConfigurationSQLDelegate = (function() {
	
	/**
	 * Constructs a System Configuration Delegate.  
	 * 
	 * @constructor
	 */
	var _delegate = function() {	
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};
	
	/**
	 * Gets the System Configuration for the application
	 */
	_delegate.prototype.getSystemConfiguration = function(successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"SELECT * FROM systemConfig ",

				// params
				{},

				// success
				function(statement) {
					var data =  statement.getResult().data,
							lastSchoolProfileCheck = null;
					if(data && data.length > 0) {
						data = data[0];
						//alert("data : "+data.id+" schoolConfigTimestamp : "+data.schoolConfigTimestamp);
						//alert("sys config sqldelegate email : "+data.email);
						if (data.lastSchoolProfileCheck != null && data.lastSchoolProfileCheck != "") {
							lastSchoolProfileCheck = new Date(parseInt(data.lastSchoolProfileCheck, 10));
						}

						successCallback(new diary.model.SystemConfiguration({
							'id' 						: data.id,
							'token' 					: data.email,
							'schoolConfigTimestamp'     : data.schoolConfigTimestamp,
							'lastSchoolProfileCheck'	: lastSchoolProfileCheck,
							'email'						: data.email
						}));
					} else {
						successCallback(null);
					}
				},

				// error
				errorCallback, "Error while trying to read system configuration"
		);
	},
	
	
	/**
	 * Updates the system configuration for application
	 * 
	 * @param {number} id The identifier of the system configuration that is to be updated (It is true that there is ONLY one system configuration but this makes life much easier)
	 * @param {diary.model.SystemConfiguration} systemConfiguration The new systemConfiguration that is to be updated
	 * @param {!function()} successCallback The function that will be executed on a successful update of the systemConfiguration
	 * @param {?function(message)} errorCallback The function that will be executed on failure of updating the systemConfiguration [OPTIONAL]
	 */
	_delegate.prototype.updateSystemConfiguration = function(systemConfiguration, successCallback, errorCallback) {
			
		//alert("successCallback in updateSystemConfiguration : "+successCallback);
		
		if (!systemConfiguration) {
			throw new Error("System Configuration must be defined, system configuration is not optional");
		}
		
		var lastSchoolProfileCheck = systemConfiguration.get('lastSchoolProfileCheck');
		if (lastSchoolProfileCheck != null) {
			lastSchoolProfileCheck = lastSchoolProfileCheck.getTime();
		}

		this._database.runQuery(
				// query
				"UPDATE systemConfig SET " +
						"email = :token, " +
						"lastSchoolProfileCheck = :lastSchoolProfileCheck, " +
						"schoolConfigTimestamp = :schoolConfigTimestamp " +
						"WHERE id = 1", // id will always be 1

				// params
				{
					"token"                 : systemConfiguration.get('token'),
					"lastSchoolProfileCheck": lastSchoolProfileCheck,
					"schoolConfigTimestamp" : systemConfiguration.get('schoolConfigTimestamp')
				},

				// success
				successCallback || $.noop,

				// error
				errorCallback, "Error while trying to read system configuration"
		);
	};
	
	/**
	 * Updates the diary database in preparation for updating to a new token 
	 * 
	 * @param {!function()} successCallback The function that will be executed on a successful update of the systemConfiguration
	 * @param {?function(message)} errorCallback The function that will be executed on failure of updating the systemConfiguration [OPTIONAL]
	 */
	_delegate.prototype.prepareForTokenRefresh = function(successCallback, errorCallback) {
	
		// check that the success callback function was specified
		if (!successCallback) {
			throw new Error("Success Callback must be defined, callback is not optional");
		}
		
		var queries = [
			{ 'sql': 'DELETE FROM class' },	// This will lose the students subject to period assignments
			{ 'sql': 'DELETE FROM period' },
			{ 'sql': 'DELETE FROM timetable' },
			{ 'sql': 'DELETE FROM dayOfNote' },
			{ 'sql': 'DELETE FROM schoolProfile' },
			{ 'sql': 'DELETE FROM document' },
			{ 'sql': 'DELETE FROM stemmedWords' },
			{ 'sql': 'DELETE FROM occurs' },
			{ 'sql': 'DELETE FROM storedContentPackages' }
		];
		
		this._database.runScripts(queries, successCallback, errorCallback);

	}
	
	// return the generated delegate
	return _delegate;
		
})();
