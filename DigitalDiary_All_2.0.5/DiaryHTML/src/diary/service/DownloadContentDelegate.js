/**
 * The delegate that is to be used to get content from a remote location and
 * save it to a given file
 *
 * @author Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DownloadContentDelegate = (function() {

	/**
	 * Constructs the Download Content Delegate
	 *
	 * @constructor
	 */
	var _service = function() {
		this._cloudManager = app.locator.getService(ServiceName.THECLOUDSERVICEMANAGER);
	};

	function installZipToPath(zipData, localPath, successCallback, errorCallback) {
		
		//alert("installZipToPath")
		
	//console.log("zipData==");
	//console.log(zipData);
		// holds the content folder
		var contentFolder;
		// holds the folder that is to be used to save the content
		var localFolder;
		

		try {

			// get the content folder where all the contents are saved
			// check that it exists]
			
			contentFolder = air.File.applicationStorageDirectory.resolvePath("content");console.log(contentFolder.parent.name)
			
				

			




			//console.log("contentFolder=:"+contentFolder);
			if (!contentFolder.exists) {
				contentFolder.createDirectory();
			}
	

//	alert("yes")



			// get the local path where the content is to be saved
			// NOTE - this is relative to the application content folder
			
			
			//newchange
			localFolder = contentFolder.resolvePath("../config");
			//newchnage
			if (!localFolder.exists) {
				localFolder.createDirectory();
			}

			// create a temporary folder to extract data to
			var tmpFolder = localFolder.resolvePath("../" + localFolder.name + ".tmp");
			
			if (tmpFolder.exists) {
				//console.log('Deleting directory....');
				tmpFolder.deleteDirectory(true);
			}

			//console.log('Creating directory....');
			tmpFolder.createDirectory();
//}

			var zipFile = new window.deng.fzip.FZip();

			var i = 0;
			var start = new Date().getTime();
			var doNextFile = function() {
				try {
				//console.log("i="+i+"------zipFile.getFileCount()-------"+zipFile.getFileCount());
					if(i == zipFile.getFileCount()) {
						//console.log('Moving folder....');
						//console.log('Done in ' + (new Date().getTime() - start) + " milliseconds");
					
					tmpFolder.moveTo(localFolder, true);
						successCallback();
						
						//Aayshi
						
						var sc=localFolder.resolvePath("../config/content");//alert(sc.name)
					
						var dest=localFolder.parent;
						
						dest=dest.resolvePath("content");
						
						
						
						sc.moveTo(dest, true);
						//alert(dest.name)
						
						successCallback();

						//Aayshi 
						//alert("done")
						
					} else {
						var entry = zipFile.getFileAt(i);
						i++;

						if (entry.filename.substring(0, 2) != "__") {
						
							// extract the entry's data from the zip
							var fileDataByteArray = entry.content;

							// if the size of the entry is 0 we are going to assume that the
							// entry is a folder
							if (fileDataByteArray.length == 0) {

								// create the folder
								//console.log('Creating directory.... ' + entry.filename);
								var newFolder = tmpFolder.resolvePath(entry.filename);
								
							
								if (!newFolder.exists) {
									newFolder.createDirectory();
								}

								_.defer(doNextFile);

							} else {

								// create a file
								//console.log('Creating file.... ' + entry.filename);
								var entryFile = tmpFolder.resolvePath(entry.filename);
								var entryFileStream = new air.FileStream();
								if(entry.filename=="manifest.ddm")
                                {
                                    //console.log("manifest.ddm");
                                }
								try {
									entryFileStream.openAsync(entryFile, "write");
									//console.log(fileDataByteArray);
									entryFileStream.writeBytes(fileDataByteArray);
									
									
									entryFileStream.addEventListener(air.Event.CLOSE, doNextFile);
									entryFileStream.addEventListener(air.IOErrorEvent.IO_ERROR, errorCallback);
								} catch (e) {
									//console.log("Error extracting zip", e);
									errorCallback(e);
								} finally {
									entryFileStream.close();
								}
							}
						}
					}
				} catch (error) {
					errorCallback(error);
				}
			};

			zipFile.addEventListener(air.Event.COMPLETE, doNextFile);
			zipFile.addEventListener(air.IOErrorEvent.IO_ERROR, errorCallback);
			zipFile.addEventListener(window.deng.fzip.FZipErrorEvent.PARSE_ERROR, errorCallback);
			zipFile.loadBytes(zipData);
			
			


		} catch (error) {
			errorCallback(error);
		}
		
		
	}

	/**
	 * Download and install the school configuration from the remote server
	 */
	_service.prototype.downloadAndInstallSchoolConfiguration = function(successCallback, errorCallback) {
		//console.log('DownloadContentDelegate.downloadAndInstallSchoolConfiguration()');
		//alert("downloadAndInstallSchoolConfiguration ");
		this._cloudManager.downloadSchoolConfiguration(
				
		
				function success(zipData) {
					//alert("downloadAndInstallSchoolConfiguration success: "+JSON.stringify(zipData));
				
					installZipToPath(zipData, "../config", successCallback, errorCallback);
				},
				errorCallback || $.noop
		);
	}

	/**
	 * Download and install a content package from the remote server
	 */
	_service.prototype.downloadAndInstallContentPackage = function(packageName, successCallback, errorCallback) {
		//console.log('DownloadContentDelegate.downloadAndInstallContentPackage(' + packageName + ')');
		this._cloudManager.downloadContentPackage(packageName,
				function success(zipData) {
				
								var myfolder = air.File.applicationStorageDirectory.resolvePath("content");
totNumOfFiles=myfolder.getDirectoryListing()

if(totNumOfFiles.length==0)	
{
					installZipToPath(zipData, packageName, successCallback, errorCallback);
}

				},
				errorCallback || $.noop
		);
	}

	/**
	 * Delete all the directories
	 */
	_service.prototype.deleteAllDirectories = function() {
		try {
			var config  = air.File.applicationStorageDirectory.resolvePath("config");
			var content = air.File.applicationStorageDirectory.resolvePath("content");
			var sandbox = air.File.applicationStorageDirectory.resolvePath("sandbox");

			if(config.exists) {
				config.deleteDirectory(true);
			}

			if(content.exists) {
				content.deleteDirectory(true);
			}

			if(sandbox.exists) {
				sandbox.deleteDirectory(true);
			}

		} catch (error){
			//console.log("failed to delete all the directories", error);
		}
	}

	/**
	 * Download remote content to the given location
	 *
	 * @param {!string} remotePath The remote path from where the content is to be downloaded
	 * @param {!string} localPath The local path to where the content is to be saved
	 * @param {!function()} successCallback The function that is to be called if the download is successful
	 * @param {?function(message)} errorCallback The function that is to be called if the download fails
	 */
	_service.prototype.downloadContent = function(remotePath, localPath, successCallback, errorCallback) {

	//	console.log('DownloadContentDelegate.downloadContent(' + remotePath + ',' + localPath + ')');

		// holds the request that is to be made to the remote location
		var request;
		// holds the file loader
		var loader = null;

		// check that all the required details were given
		if (!remotePath) {
			throw new Error("Remote path must be specified, path is not optional");
		}

		if (!localPath) {
			throw new Error("Local path must be specified, path is not optional");
		}

		try {
			// make the request to the remote machine and try to get the file
			request = new air.URLRequest(remotePath);
			loader = new air.URLLoader();
			loader.dataFormat = "binary";
			loader.addEventListener("complete", function(){
				installZipToPath(loader.data, localPath, successCallback, errorCallback);
			});
			loader.addEventListener("ioError", function(event) {
				// console.log(event.text);
				errorCallback && errorCallback("Failed while trying to get content from remote location [" + remotePath + "]");
			});
			loader.load(request);

		} catch (error) {
			// log the error so that we can take action later and fix it
			// console.log("Error while trying to get remote content", error);
			// send a reply back if we have a callback error
			if (errorCallback) {
				errorCallback("Failed while trying to get content from remote location [" + remotePath + "]");
			}
		}
	};

	return _service;

})();
