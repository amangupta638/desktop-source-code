/**
 * The manager that will take care of the schema required for the application.  This
 * will cater to keep the schema up to date according to the version that is being run
 *
 * @author Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DiarySchemaManager = (function() {

	/**
	 * Constructs a service to manage the schema of the local database.
	 * @constructor
	 */
	var _service = function() {

		// get the database that is being used for the application
		this._database = app.locator.getService(ServiceName.APPDATABASE);

	};

	/**
	 * Any schema version before this value will be treated as unsupported -
	 * the application will refuse to update the DB schema.
	 * @type {Number}
	 * @private
	 */
	var _MINIMUM_SUPPORTED_VERSION = 3;
	var _SQL_NO_OP = "SELECT * FROM info";

	/**
	 * Append to this list if you wish to add a new schema version.
	 * If a version is specified as a function, it will be executed just prior to the upgrade
	 * being performed, and its return value is expected to be in the defined object format.
	 * 
	 * @type {Array} List of versions. May be specified as objects or functions. Each version should have a list of query objects.
	 * @private
	 */
	/*var _schema_versions = [

		// Version 3 (earliest supported version)
		{
			queries: [
				{ 'sql' : "CREATE TABLE studentProfile (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NULL, " +
					" dateOfBirth INTEGER(20) NULL, " +
					" addressStreet TEXT(200) NULL, " +
					" addressSuburb TEXT(100) NULL, " +
					" addressState TEXT(100) NULL, " +
					" addressPostcode TEXT(7) NULL, " +
					" telephone TEXT(50) NULL, " +
					" email TEXT(100) NULL, " +
					" grade TEXT(50) NULL, " +
					" teacher TEXT(500) NULL, " +
					" classCaptain TEXT(100) NULL, " +
					" house TEXT(100) NULL, " +
					" houseCaptain TEXT(100) NULL, " +
					" houseColours TEXT(200) NULL " +
					")",
					'rollback' : "DROP TABLE studentProfile" },

				{ 'sql' : "CREATE TABLE schoolProfile (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NULL, " +
					" schoolLogo TEXT(255) NULL, " +
					" schoolImage TEXT(255) NULL, " +
					" schoolMotto TEXT NULL, " +
					" missionStatement TEXT NULL, " +
					" diaryTitle TEXT(100) NULL, " +
					" includeEmpower INTEGER(1) NOT NULL DEFAULT 0 " +
					")",
					'rollback' : "DROP TABLE schoolProfile" },

				{ 'sql' : "CREATE TABLE diaryItem (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" type TEXT(100) NOT NULL, " +
					" date INTEGER(20) NOT NULL, " +
					" startTime INTEGER(20) NULL, " +
					" endTime INTEGER(20) NULL, " +
					" period INTEGER(20) NULL, " +
					" title TEXT(100) NOT NULL, " +
					" locked INTEGER(1) NOT NULL, " +
					" completed INTEGER(20) NULL, " +
					" reminder INTEGER(20) NULL, " +
					" subjectId INTEGER(20) NULL, " +
					" description TEXT NULL, " +
					" dateCreated INTEGER(20) NOT NULL DEFAULT 0 " +
					")",
					'rollback' : "DROP TABLE diaryItem" },

				{ 'sql' : "CREATE TABLE attachment (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" diaryItemId INTEGER NOT NULL, " +
					" originalFilePath TEXT NOT NULL, " +
					" displayName TEXT(100) NOT NULL, " +
					" fileType TEXT(100) NULL, " +
					" localFileName TEXT(100) NOT NULL UNIQUE, " +
					" dateAttached INTEGER(20) " +
					")",
					'rollback' : "DROP TABLE attachment" },

				{ 'sql' : "CREATE TABLE subject (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NOT NULL, " +
					" colour TEXT(100)" +
					")",
					'rollback' : "DROP TABLE subject" },

				{ 'sql' : "CREATE TABLE dayOfNote (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" date INTEGER(20) NOT NULL, " +
					" overrideCycleDay INTEGER(20), " +
					" includeInCycle INTEGER(1), " +
					" isGreyedOut INTEGER(1), " +
					" title TEXT(100), " +
					" overrideCycle INTEGER(20) " +
					" )",
					'rollback' : "DROP TABLE dayOfNote" },

				{ 'sql' : "CREATE TABLE timetable (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" startDate INTEGER(20) NOT NULL, " +
					" cycleLength INTEGER(20) NOT NULL " +
					")",
					'rollback' : "DROP TABLE timetable" },

				{ 'sql' : "CREATE TABLE period (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" timetable INTEGER NOT NULL, " +
					" startTime INTEGER(20) NOT NULL, " +
					" endTime INTEGER(20) NULL, " +
					" title TEXT(100) NULL, " +
					" cycleDay INTEGER(20) NULL, " +
					"isBreak INTEGER(1)" +
					")",
					'rollback' : "DROP TABLE period" },

				{ 'sql' : "CREATE TABLE class ( " +
					"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					"timetable INTEGER NOT NULL, " +
					"cycleDay INTEGER(20) NOT NULL, " +
					"periodId INTEGER(20) NOT NULL, " +
					"subjectId INTEGER(20) NOT NULL, " +
					"room TEXT(100) NULL " +
					")",
					'rollback' : "DROP TABLE class" },

				{ 'sql' : "CREATE TABLE noteEntry (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" noteId INTEGER , " +
					" timestamp INTEGER(20) NOT NULL, " +
					" text TEXT NULL, " +
					" noteFrom TEXT(100) NULL " +
					")",
					'rollback' : "DROP TABLE noteEntry" },

				// Any document that is to be searched
				{ 'sql' : "CREATE TABLE document (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" type TEXT(100) NOT NULL, " +
					" title TEXT(255) NOT NULL, " +
					" documentId TEXT(300) NOT NULL " +
					")",
					'rollback' : "DROP TABLE document" },

				// The words that a user can search by
				{ 'sql' : "CREATE TABLE stemmedWords (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" word TEXT(150) NOT NULL, " +
					" UNIQUE (word) " +
					")",
					'rollback' : "DROP TABLE stemmedWords" },

				// For mapping words to documents
				{ 'sql' : "CREATE TABLE occurs (" +
					" documentId INTEGER NOT NULL, " +
					" stemmedWordId INTEGER NOT NULL, " +
					" PRIMARY KEY (documentId, stemmedWordId) " +
					")",
					'rollback' : "DROP TABLE stemmedWords" },

				{ 'sql' : "CREATE TABLE systemConfig (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" email TEXT(100) NULL, " +
					" lastSchoolProfileCheck INTEGER," +
					" schoolConfigTimestamp TEXT(100) " +
					")",
					'rollback' : "DROP TABLE systemConfig" },

				{ 'sql' : "CREATE TABLE storedContentPackages (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" package TEXT(100) NULL, " +
					" dateLastUpdated INTEGER NULL" +
					")",
					'rollback' : "DROP TABLE storedContentPackages" },

				{ 'sql' : "CREATE TABLE contentItemForm (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" url TEXT NOT NULL, " +
					" form TEXT NOT NULL " +
					")",
					'rollback' : "DROP TABLE contentItemForm" },

				{ 'sql' : "CREATE TABLE usageLog (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" time INTEGER(20) NOT NULL, " +
					" type TEXT(100) NOT NULL, " +
					" detail TEXT NULL, " +
					" token TEXT(10) NOT NULL, " +
					" macAddresses TEXT NOT NULL, " +
					" schoolConfiguration TEXT NOT NULL, " +
					" version INTEGER NOT NULL, " +
					" clientVersion TEXT NOT NULL, " +
					" synced INTEGER(1) NOT NULL" +
					")",
					'rollback' : "DROP TABLE usageLog" },
				//aayshi		
				{ 'sql' : "CREATE TABLE contentStorage (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" ContentType TEXT(20)  , " +
					" Title TEXT(200) NOT NULL, " +
					" Content TEXT NOT NULL, " +
					" uniqueID INTEGER NOT NULL , " +
					" respectedPanel TEXT  , " +
					" StartDate TEXT  , " +
					" EndDate TEXT   " +					
					")",
					'rollback' : "DROP TABLE contentStorage" }
			]
		},

		// Version 4 - timetable names (specified as a function, rather than a simple object)
		function () {

			var queries = [];

			// Add `timetable`.`name` column
			this._pushQueriesToAddColumn(queries, "timetable", "name TEXT(100) NULL", "id, startDate, cycleLength",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"startDate INTEGER(20) NOT NULL, " +
				"cycleLength INTEGER(20) NOT NULL ");

			// Return the queries
			return {
				'queries' : queries
			};
		},

		// Version 5 - cycle day labels
		function () {
			var queries = [];

			this._pushQueriesToAddColumn(queries, "timetable", "cycleLabel TEXT(100) NULL", "id, startDate, cycleLength, name",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"startDate INTEGER(20) NOT NULL, " +
				"cycleLength INTEGER(20) NOT NULL, " +
				"name TEXT(100) NULL");

			this._pushQueriesToAddColumn(queries, "timetable", "cycleDays TEXT NULL", "id, startDate, cycleLength, name, cycleLabel",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"startDate INTEGER(20) NOT NULL, " +
				"cycleLength INTEGER(20) NOT NULL, " +
				"name TEXT(100) NULL, " +
				"cycleLabel TEXT(100) NULL");

			return {
				'queries' : queries
			};
		},

		// Version 6 - delete noteFrom column
		function () {

			var queries = [];

			// Drop `noteEntry`.`noteFrom` column
			this._pushQueriesToDropColumn(queries, "noteEntry",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					"noteId INTEGER , " +
					"timestamp INTEGER(20) NOT NULL, " +
					"text TEXT NULL",
				"id, noteId, timestamp, text",
				"noteFrom TEXT(100) NULL"
			);

			// Return the queries
			return {
				'queries' : queries
			};
		},

		// Version 7 - message fields
		function () {

			var queries = [];

			// Add `diaryItem`.`messageFrom` column
			this._pushQueriesToAddColumn(queries, "diaryItem", "messageFrom TEXT(100) NULL",
				"id, type, date, startTime, endTime, period, title, locked, completed, reminder, subjectId, description, dateCreated",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT(100) NOT NULL, " +
				"date INTEGER(20) NOT NULL, " +
				"startTime INTEGER(20) NULL, " +
				"endTime INTEGER(20) NULL, " +
				"period INTEGER(20) NULL, " +
				"title TEXT(100) NOT NULL, " +
				"locked INTEGER(1) NOT NULL, " +
				"completed INTEGER(20) NULL, " +
				"reminder INTEGER(20) NULL, " +
				"subjectId INTEGER(20) NULL, " +
				"description TEXT NULL, " +
				"dateCreated INTEGER(20) NOT NULL DEFAULT 0"
			);

			// Add `diaryItem`.`messageFromText` column
			this._pushQueriesToAddColumn(queries, "diaryItem", "messageFromText TEXT(100) NULL",
				"id, type, date, startTime, endTime, period, title, locked, completed, reminder, subjectId, messageFrom, description, dateCreated",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT(100) NOT NULL, " +
				"date INTEGER(20) NOT NULL, " +
				"startTime INTEGER(20) NULL, " +
				"endTime INTEGER(20) NULL, " +
				"period INTEGER(20) NULL, " +
				"title TEXT(100) NOT NULL, " +
				"locked INTEGER(1) NOT NULL, " +
				"completed INTEGER(20) NULL, " +
				"reminder INTEGER(20) NULL, " +
				"subjectId INTEGER(20) NULL, " +
				"messageFrom TEXT(100) NULL, " +
				"description TEXT NULL, " +
				"dateCreated INTEGER(20) NOT NULL DEFAULT 0"
			);
			
			// Add `diaryItem`.`responseRequired` column
			this._pushQueriesToAddColumn(queries, "diaryItem", "responseRequired INTEGER(1) NOT NULL DEFAULT 0",
				"id, type, date, startTime, endTime, period, title, locked, completed, reminder, subjectId, messageFrom, messageFromText, description, dateCreated",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT(100) NOT NULL, " +
				"date INTEGER(20) NOT NULL, " +
				"startTime INTEGER(20) NULL, " +
				"endTime INTEGER(20) NULL, " +
				"period INTEGER(20) NULL, " +
				"title TEXT(100) NOT NULL, " +
				"locked INTEGER(1) NOT NULL, " +
				"completed INTEGER(20) NULL, " +
				"reminder INTEGER(20) NULL, " +
				"subjectId INTEGER(20) NULL, " +
				"messageFrom TEXT(100) NULL, " +
				"messageFromText TEXT(100) NULL, " +
				"description TEXT NULL, " +
				"dateCreated INTEGER(20) NOT NULL DEFAULT 0"
			);

			// Return the queries
			return {
				'queries' : queries
			};
		},

        // Version 8 - delete studentProfile.classCaptain column
        function () {

            var queries = [];

            // Drop `studentProfile`.`classCaptain` column
            this._pushQueriesToDropColumn(queries, "studentProfile",
                " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    " name TEXT(100) NULL, " +
                    " dateOfBirth INTEGER(20) NULL, " +
                    " addressStreet TEXT(200) NULL, " +
                    " addressSuburb TEXT(100) NULL, " +
                    " addressState TEXT(100) NULL, " +
                    " addressPostcode TEXT(7) NULL, " +
                    " telephone TEXT(50) NULL, " +
                    " email TEXT(100) NULL, " +
                    " grade TEXT(50) NULL, " +
                    " teacher TEXT(500) NULL, " +
                    " house TEXT(100) NULL, " +
                    " houseCaptain TEXT(100) NULL, " +
                    " houseColours TEXT(200) NULL ",
                "id, name, dateOfBirth, addressStreet, addressSuburb, addressState, " +
                "addressPostcode, telephone, email, grade, teacher, house, houseCaptain, houseColours",
                "classCaptain TEXT(100) NULL"
            );

            // Return the queries
            return {
                'queries' : queries
            };
        },

		// Version 9 - [PD-659]
		function () {

			var queries = [];

			// Drop diaryItem.reminder column
			this._pushQueriesToDropColumn(queries, "diaryItem",
				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +

				// Task fields
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"dateCreated INTEGER, " +
				"date INTEGER, " +
				"period INTEGER",

				"id, type, title, description, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, dateCreated, date, period",
				"reminder INTEGER"
			);

			// Rename diaryItem.dateCreated to diaryItem.created
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"created INTEGER",
				"id, type, title, description, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, dateCreated, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +

				// Task fields
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"dateCreated INTEGER",
				"date INTEGER, " +
				"period INTEGER"
			);

			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET created = dateCreated",
				'rollback' : _SQL_NO_OP
			});

			this._pushQueriesToDropColumn(queries, "diaryItem",
				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER",

				"id, type, title, description, created, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",
				"dateCreated INTEGER"
			);

			// Add diaryItem.assignedDay column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"assignedDay INTEGER",
				"id, type, title, description, created, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Add diaryItem.assignedTime column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"assignedTime INTEGER",
				"id, type, title, description, created, assignedDay, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Add diaryItem.dueDay column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"dueDay INTEGER",
				"id, type, title, description, created, assignedDay, assignedTime, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER DEFAULT 0, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Add diaryItem.dueTime column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"dueTime INTEGER",
				"id, type, title, description, created, assignedDay, assignedTime, dueDay, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"dueDay INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Add diaryItem.startDay column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"startDay INTEGER",
				"id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"dueDay INTEGER, " +
				"dueTime INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Move data from date to startDay (Events, Notes, Messages)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startDay = strftime('%Y%m%d', date/1000, 'unixepoch', 'localtime') " +
					"WHERE type = 'NOTE' OR type = 'MESSAGE' OR type = 'EVENT'",
				'rollback' : _SQL_NO_OP
			});

			// Convert startTime and endTime to new time format (Events)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startTime = strftime('%H%M', startTime/1000, 'unixepoch', 'localtime'), " +
					"endTime = strftime('%H%M', endTime/1000, 'unixepoch', 'localtime') " +
					"WHERE type = 'EVENT'",
				'rollback' : _SQL_NO_OP
			});

			// Move data from period to startTime (Events)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startTime = (SELECT strftime('%H%M', startTime/1000, 'unixepoch', 'localtime') FROM period WHERE id = period) " +
					"WHERE type = 'EVENT' AND startTime IS NULL AND period IS NOT NULL",
				'rollback' : _SQL_NO_OP
			});

			// Move data from period to endTime (Events)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET endTime = (SELECT strftime('%H%M', endTime/1000, 'unixepoch', 'localtime') FROM period WHERE id = period) " +
					"WHERE type = 'EVENT' AND endTime IS NULL AND period IS NOT NULL",
				'rollback' : _SQL_NO_OP
			});

			// Convert startTime and endTime to new time format (Notes, Messages)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startTime = strftime('%H%M', startTime/1000, 'unixepoch', 'localtime'), " +
					"endTime = NULL " +
					"WHERE type = 'NOTE' OR type = 'MESSAGE'",
				'rollback' : _SQL_NO_OP
			});

			// Move data from period to startTime (Notes, Messages)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startTime = (SELECT strftime('%H%M', startTime/1000, 'unixepoch', 'localtime') FROM period WHERE id = period) " +
					"WHERE (type = 'NOTE' OR type = 'MESSAGE') AND startTime IS NULL AND period IS NOT NULL",
				'rollback' : _SQL_NO_OP
			});

			// Move data from created to assignedDay and assignedTime (Tasks)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET assignedDay = strftime('%Y%m%d', created/1000, 'unixepoch', 'localtime'), " +
					"assignedTime = strftime('%H%S', created/1000, 'unixepoch', 'localtime') " +
					"WHERE type = 'HOMEWORK' OR type = 'ASSIGNMENT'",
				'rollback' : _SQL_NO_OP
			});

			// Move data from date to dueDay and startTime to dueTime (Tasks)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET dueDay = strftime('%Y%m%d', date/1000, 'unixepoch', 'localtime'), " +
					"dueTime = strftime('%H%M', startTime/1000, 'unixepoch', 'localtime'), " +
					"startTime = NULL, " +
					"endTime = NULL " +
					"WHERE type = 'HOMEWORK' OR type = 'ASSIGNMENT'",
				'rollback' : _SQL_NO_OP
			});

			// Move data from period to dueTime (Tasks)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET dueTime = (SELECT strftime('%H%M', startTime/1000, 'unixepoch', 'localtime') FROM period WHERE id = period) " +
					"WHERE (type = 'HOMEWORK' OR type = 'ASSIGNMENT') AND dueTime IS NULL AND period IS NOT NULL",
				'rollback' : _SQL_NO_OP
			});

			// Drop diaryItem.date column
			this._pushQueriesToDropColumn(queries, "diaryItem",
				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"dueDay INTEGER, " +
				"dueTime INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startDay INTEGER, " +
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"period INTEGER",

				"id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, period",
				"date INTEGER"
			);

			// Drop diaryItem.period column
			this._pushQueriesToDropColumn(queries, "diaryItem",
				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"dueDay INTEGER, " +
				"dueTime INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startDay INTEGER, " +
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER",

				"id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFrom, messageFromText, responseRequired",
				"period INTEGER"
			);

			// Return the queries
			return {
				'queries' : queries
			};
		},

		// Version 10 - add 'theme' to school profile
		function () {
			var queries = this._pushQueriesToAddColumn([], "schoolProfile",
				"theme TEXT(100) NULL",
				"id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower",
				" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NULL, " +
					" schoolLogo TEXT(255) NULL, " +
					" schoolImage TEXT(255) NULL, " +
					" schoolMotto TEXT NULL, " +
					" missionStatement TEXT NULL, " +
					" diaryTitle TEXT(100) NULL, " +
					" includeEmpower INTEGER(1) NOT NULL DEFAULT 0 ");
			return { 'queries' : queries };
		},

        // Version 11 - [PD-689]
        function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({
                'sql' : "UPDATE period SET " +
                        "startTime = strftime('%H%M', startTime/1000, 'unixepoch', 'localtime'), " +
                        "endTime = strftime('%H%M', endTime/1000, 'unixepoch', 'localtime') ",
                'rollback' : _SQL_NO_OP
            });

            // update the dayOfNote date to a format compatible with Rocketboots.date.Day
            queries.push({
                'sql' : "UPDATE dayOfNote SET " +
                        "date = strftime('%Y%m%d', date/1000, 'unixepoch', 'localtime')",
                'rollback' : _SQL_NO_OP
            });

            // update the student date of birth to a format compatible with Rocketboots.date.Day
            queries.push({
                'sql' : "UPDATE studentProfile SET " +
                    "dateOfBirth = strftime('%Y%m%d', dateOfBirth/1000, 'unixepoch', 'localtime')",
                'rollback' : _SQL_NO_OP
            });

            // update the timetable start time to a format compatible with Rocketboots.date.Day
            queries.push({
            	'sql' : "UPDATE timetable SET " +
            	"startDate = strftime('%Y%m%d', startDate/1000, 'unixepoch', 'localtime')",
            	'rollback' : _SQL_NO_OP
            });
            
            // Return the queries
            return {
                'queries' : queries
            };
        },
	//version 12 created by appstudioz
        function()
        {
        var queries = this._pushQueriesToAddColumn([], "diaryItem",
                       "lastupdated INTEGER",
                       "id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "created INTEGER, " +
                       
                       // Task fields
                       "assignedDay INTEGER, " +
                       "assignedTime INTEGER, " +
                       "dueDay INTEGER, " +
                       "dueTime INTEGER, " +
                       "completed INTEGER, " +
                       "subjectId INTEGER, " +
                       
                       // Event fields
                       "startTime INTEGER, " +
                       "endTime INTEGER, " +
                       "locked INTEGER, " +
                       
                       // Note fields
                       // atDay = startDay
                       // atTime = startTime
                       
                       // Message fields
                       // atDay = startDay
                       // atTime = startTime
                       "messageFrom TEXT, " +
                       "messageFromText TEXT, " +
                       "responseRequired INTEGER, " 
                       
                       
                       );
        return { 'queries' : queries };
        },
        //version 13 created by appstudioz
        function()
        {
        var queries = this._pushQueriesToAddColumn([], "diaryItem",
                       "Universal_ID TEXT",
                       "id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "created INTEGER, " +
                       
                       // Task fields
                       "assignedDay INTEGER, " +
                       "assignedTime INTEGER, " +
                       "dueDay INTEGER, " +
                       "dueTime INTEGER, " +
                       "completed INTEGER, " +
                       "subjectId INTEGER, " +
                       
                       // Event fields
                       "startTime INTEGER, " +
                       "endTime INTEGER, " +
                       "locked INTEGER, " +
                       
                       // Note fields
                       // atDay = startDay
                       // atTime = startTime
                       
                       // Message fields
                       // atDay = startDay
                       // atTime = startTime
                       "messageFrom TEXT, " +
                       "messageFromText TEXT, " +
                       "responseRequired INTEGER, " 
                       
                       
                       );
        return { 'queries' : queries };
        },
        //version 14 created by appstudioz
        function()
        {
        var queries = this._pushQueriesToAddColumn([], "diaryItem",
                       "isSync INTEGER",
                       "id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, Universal_ID,lastupdated",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "created INTEGER, " +
                       
                       // Task fields
                       "assignedDay INTEGER, " +
                       "assignedTime INTEGER, " +
                       "dueDay INTEGER, " +
                       "dueTime INTEGER, " +
                       "completed INTEGER, " +
                       "subjectId INTEGER, " +
                       
                       // Event fields
                       "startTime INTEGER, " +
                       "endTime INTEGER, " +
                       "locked INTEGER, " +
                       
                       // Note fields
                       // atDay = startDay
                       // atTime = startTime
                       
                       // Message fields
                       // atDay = startDay
                       // atTime = startTime
                       "messageFrom TEXT, " +
                       "messageFromText TEXT, " +
                       "responseRequired INTEGER, " +
                      "Universal_ID:INTEGER,"+
                      "lastupdated INTEGER, "
                       
                       
                       );
        return { 'queries' : queries };
        },
        //version 15 created by appstudioz
        function()
        {
        var queries = this._pushQueriesToAddColumn([], "diaryItem",
                       "createdTimesatmp TEXT",
                       "id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, Universal_ID,lastupdated,isSync",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "created INTEGER, " +
                       
                       // Task fields
                       "assignedDay INTEGER, " +
                       "assignedTime INTEGER, " +
                       "dueDay INTEGER, " +
                       "dueTime INTEGER, " +
                       "completed INTEGER, " +
                       "subjectId INTEGER, " +
                       
                       // Event fields
                       "startTime INTEGER, " +
                       "endTime INTEGER, " +
                       "locked INTEGER, " +
                       
                       // Note fields
                       // atDay = startDay
                       // atTime = startTime
                       
                       // Message fields
                       // atDay = startDay
                       // atTime = startTime
                       "messageFrom TEXT, " +
                       "messageFromText TEXT, " +
                       "responseRequired INTEGER, " +
                      "Universal_ID:INTEGER,"+
                      "lastupdated INTEGER, "+
                      "isSync INTEGER, "
                       
                       
                       );
        return { 'queries' : queries };
        },
        //version 16 created by appstudioz
        function()
        {
        var queries = this._pushQueriesToAddColumn([], "attachment",
                       "isSync INTEGER",
                       "id, diaryItemId, originalFilePath, displayName, fileType, localFileName, dateAttached",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "diaryItemId INTEGER, " +
                       "originalFilePath TEXT, " +
                       "displayName TEXT, " +
                       "fileType TEXT, " +
                       
                       // Task fields
                       "localFileName TEXT, " +
                       "dateAttached INTEGER, " 
                       
                       
                       );
        return { 'queries' : queries };
        },
        //version 17 created by appstudioz
        function()
        {
        var queries = this._pushQueriesToAddColumn([], "schoolProfile",
                       "AllowAutoUpdates INTEGER",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, " 
                       
                       
                       );
        return { 'queries' : queries };
        }
        ,
        //version 18 created by appstudioz
        function()
        {
        var queries = this._pushQueriesToAddColumn([], "usageLog",
                       "createdDate TEXT",
                       "id, time, type, detail, token, macAddresses, schoolConfiguration, version, clientVersion, synced",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "time INTEGER, " +
                       "type TEXT, " +
                       "detail TEXT, " +
                       "token TEXT, " +
                       
                       // Task fields
                       "macAddresses TEXT, " +
                       "schoolConfiguration TEXT, " +
                       "version INTEGER, "+
                       "clientVersion TEXT, "+
                       "synced INTEGER, "
                       
                       );
        return { 'queries' : queries };
        },
        //version 19 created by appstudioz
        function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({ 'sql' : "CREATE TABLE UsageLogSync (" +
                    " dateLastSynced TEXT " +
                    ")",
                    'rollback' : "DROP TABLE UsageLogSync" });
                    
             return {
                'queries' : queries
            };
            
            },
        //version 20 created by appstudioz
        function()
        {
        var queries = this._pushQueriesToAddColumn([], "UsageLogSync",
                       "id INTEGER",
                       "dateLastSynced",
                       
                       // Diary Item fields
                       "dateLastSynced TEXT, " 
                       
                       );
        return { 'queries' : queries };
        },
        //version 21 created by appstudioz
        function()
        {
        var queries = this._pushQueriesToAddColumn([], "schoolProfile",
                       "RegionalFormat TEXT",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, " 
                       
                       
                       );
        return { 'queries' : queries };
        },
        //version 22 by appstudioz
        function()
        {
         var queries =[];
         
        queries.push({
				'sql' : "ALTER TABLE studentProfile RENAME TO User",
				'rollback' : _SQL_NO_OP
			});
        return { 'queries' : queries };
        },
        // 23
        function()
        {
        var queries =[];
        	this._pushQueriesToAddColumn(queries, "attachment",
                       "userId INTEGER ",
                       "id, name, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, teacher, classCaptain, house, houseCaptain, houseColours",
                       
                       // Diary Item fields
                       " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
						" name TEXT, " +
						" dateOfBirth INTEGER, " +
						" addressStreet TEXT, " +
						" addressSuburb TEXT, " +
						" addressState TEXT, " +
						" addressPostcode TEXT, " +
						" telephone TEXT, " +
						" email TEXT(100), " +
						" grade TEXT(50), " +
						" teacher TEXT(500), " +
						" classCaptain TEXT, " +
						" house TEXT(100), " +
						" houseCaptain TEXT, " +
						" houseColours TEXT, " 
                       
                       );
                        return { 'queries' : queries };
        },
        //24
        function()
        {
        var queries =[];
        	
        	this._pushQueriesToAddColumn(queries, "User",
                       "SchoolId INTEGER ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState ",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, "+
                       "houseCaptain TEXT, "+
                       "houseColours TEXT, "+                       
                       "addressState TEXT, " 
                       
                       );
             this._pushQueriesToAddColumn(queries, "User",
                       "Country TEXT ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState, SchoolId",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       "SchoolId INTEGER, " +
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, " +
                       "houseCaptain TEXT, " +
                       "houseColours TEXT, " +                       
                       "addressState TEXT, " 
                       
                       );
             this._pushQueriesToAddColumn(queries, "User",
                       "LastName TEXT ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState, SchoolId, Country",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       "SchoolId INTEGER, " +
                       "Country TEXT, " +
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, " +
                       "houseCaptain TEXT, " +
                       "houseColours TEXT, " +                       
                       "addressState TEXT, " 
                       
                       );
             this._pushQueriesToAddColumn(queries, "User",
                       "IsStudent INTEGER ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState, SchoolId, Country, LastName",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       "SchoolId INTEGER, " +
                       "Country TEXT, " +
                       "LastName TEXT, " +
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, " +
                       "houseCaptain TEXT, " +
                       "houseColours TEXT, " +                       
                       "addressState TEXT, " 
                       
                       );
                       
              this._pushQueriesToAddColumn(queries, "User",
                       "IsTeacher INTEGER ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState, SchoolId, Country, LastName, IsStudent",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       "SchoolId INTEGER, " +
                       "Country TEXT, " +
                       "LastName TEXT, " +
                       "IsStudent INTEGER, "+
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, " +
                       "houseCaptain TEXT, " +
                       "houseColours TEXT, " +                       
                       "addressState TEXT, " 
                       
                       );
                       
                this._pushQueriesToAddColumn(queries, "User",
                       "IsParent INTEGER ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState, SchoolId, Country, LastName, IsStudent, IsTeacher",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       "SchoolId INTEGER, " +
                       "Country TEXT, " +
                       "LastName TEXT, " +
                       "IsStudent INTEGER, "+
                       "IsTeacher INTEGER, "+
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, " +
                       "houseCaptain TEXT, " +
                       "houseColours TEXT, " +                       
                       "addressState TEXT, " 
                       
                       );
                this._pushQueriesToAddColumn(queries, "User",
                       "ProfileImage TEXT ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState, SchoolId, Country, LastName, IsStudent, IsTeacher, IsParent",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       "SchoolId INTEGER, " +
                       "Country TEXT, " +
                       "LastName TEXT, " +
                       "IsStudent INTEGER, "+
                       "IsTeacher INTEGER, "+
                       "IsParent INTEGER, "+
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, " +
                       "houseCaptain TEXT, " +
                       "houseColours TEXT, " +                       
                       "addressState TEXT, " 
                       
                       );
               this._pushQueriesToAddColumn(queries, "User",
                       "UniqueID INTEGER ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState, SchoolId, Country, LastName, IsStudent, IsTeacher, IsParent, "+
                       "ProfileImage ",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       "SchoolId INTEGER, " +
                       "Country TEXT, " +
                       "LastName TEXT, " +
                       "IsStudent INTEGER, " +
                       "IsTeacher INTEGER, " +
                       "IsParent INTEGER, " +
                       "ProfileImage TEXT, " +
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, " +
                       "houseCaptain TEXT, " +
                       "houseColours TEXT, " +                       
                       "addressState TEXT, " 
                       
                       );
                 this._pushQueriesToAddColumn(queries, "User",
                       "lastupdated INTEGER ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState, SchoolId, Country, LastName, IsStudent, IsTeacher, IsParent, "+
                       "ProfileImage, UniqueID",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       "SchoolId INTEGER, " +
                       "Country TEXT, " +
                       "LastName TEXT, " +
                       "IsStudent INTEGER, " +
                       "IsTeacher INTEGER, " +
                       "IsParent INTEGER, " +
                       "ProfileImage TEXT, " +
                       "UniqueID INTEGER, " +
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, " +
                       "houseCaptain TEXT, " +
                       "houseColours TEXT, " +                       
                       "addressState TEXT, " 
                       
                       );
                       
                       
             return { 'queries' : queries };
        },
        //25
        function()
        {
        	var queries=[];
        	 this._pushQueriesToDropColumn(queries, "diaryItem",
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+ 
                       "created INTEGER " ,
                       
                       "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created",
                       
                       "lastupdated INTEGER"
                       ); 
                this._pushQueriesToDropColumn(queries, "diaryItem",
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+ 
                       "created INTEGER " ,
                       
                       "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created",
                       
                       "Universal_ID TEXT"
                       ); 
                
                this._pushQueriesToDropColumn(queries, "diaryItem",
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+ 
                       "created INTEGER " ,
                       
                       "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created",
                       
                       "createdTimesatmp TEXT"
                       );
                       
              this._pushQueriesToAddColumn(queries, "diaryItem",
                       "SchoolId  INTEGER ",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+ 
                       "created INTEGER, "
                      
                       );
                       
             this._pushQueriesToAddColumn(queries, "diaryItem",
                       "ClassId INTEGER",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId ",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER " +
                       "created INTEGER, "
                       
                       
                       
                       );  
              this._pushQueriesToAddColumn(queries, "diaryItem",
                       "UniqueID INTEGER ",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "created INTEGER, "
                       
                       ); 
                       
               this._pushQueriesToAddColumn(queries, "diaryItem",
                       "AssingedBy  INTEGER ",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "created INTEGER, "
                       
                       );
                this._pushQueriesToAddColumn(queries, "diaryItem",
                       "EstimatedHours TEXT (10)",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "AssingedBy  INTEGER, "+
                       "created INTEGER, "
                       
                       );
                       
               this._pushQueriesToAddColumn(queries, "diaryItem",
                       "EstimatedMins TEXT (10)",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "EstimatedHours TEXT, "+
                       "AssingedBy  INTEGER, "+
                       "created INTEGER, "
                       
                       );
                       
               this._pushQueriesToAddColumn(queries, "diaryItem",
                       "lastupdated INTEGER",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "EstimatedHours TEXT, "+
                       "EstimatedMins TEXT, "+
                       "AssingedBy  INTEGER, "+
                       "created INTEGER, "
                       
                       );
              this._pushQueriesToAddColumn(queries, "diaryItem",
                       "isSync INTEGER",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "EstimatedHours TEXT, "+
                       "EstimatedMins TEXT, "+
                       "AssingedBy  INTEGER, "+
                       "lastupdated INTEGER, "+
                       "created INTEGER, "
                       
                       );       
                       
               this._pushQueriesToAddColumn(queries, "diaryItem",
                       "Progress INTEGER(1)",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated, isSync",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "EstimatedHours TEXT, "+
                       "EstimatedMins TEXT, "+
                       "AssingedBy  INTEGER, "+
                       "lastupdated INTEGER, "+
                       "isSync INTEGER, "+
                       "created INTEGER, "
                       
                       );     
               this._pushQueriesToAddColumn(queries, "diaryItem",
                       "isDelete INTEGER(1)",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated, isSync, Progress",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "EstimatedHours TEXT, "+
                       "EstimatedMins TEXT, "+
                       "AssingedBy  INTEGER, "+
                       "lastupdated INTEGER, "+
                       "isSync INTEGER, "+
                       "Progress INTEGER, "+
                       "created INTEGER, "
                       
                       ); 
            this._pushQueriesToAddColumn(queries, "diaryItem",
                       "priority INTEGER(1)",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated, isSync, Progress, isDelete",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "isDelete INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "EstimatedHours TEXT, "+
                       "EstimatedMins TEXT, "+
                       "AssingedBy  INTEGER, "+
                       "lastupdated INTEGER, "+
                       "isSync INTEGER, "+
                       "Progress INTEGER, "+
                       "created INTEGER, "
                       
                       );    
              this._pushQueriesToAddColumn(queries, "diaryItem",
                       "endDay DATE",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated, isSync, Progress, isDelete, "+
                        "priority ",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "isDelete INTEGER, "+
                       "priority INTEGER, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "EstimatedHours TEXT, "+
                       "EstimatedMins TEXT, "+
                       "AssingedBy  INTEGER, "+
                       "lastupdated INTEGER, "+
                       "isSync INTEGER, "+
                       "Progress INTEGER, "+
                       "created INTEGER, "
                       
                       );
            this._pushQueriesToAddColumn(queries, "diaryItem",
                       "UserID INTEGER",
                        "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated, isSync, Progress, isDelete, "+
                        "priority, endDay",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "isDelete INTEGER, "+
                       "priority INTEGER, "+
                       "endDay DATE, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "EstimatedHours TEXT, "+
                       "EstimatedMins TEXT, "+
                       "AssingedBy  INTEGER, "+
                       "lastupdated INTEGER, "+
                       "isSync INTEGER, "+
                       "Progress INTEGER, "+
                       "created INTEGER, "
                       
                       );
                        
                   return { 'queries' : queries };                         
        },
        //26
        function ()
        {
        
        	var queries=[];
        	this._pushQueriesToAddColumn(queries, "noteEntry",
                       "UniqueID INTEGER",
                        "id, noteId, timestamp, text",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "noteId INTEGER, " +
                       "timestamp INTEGER, " +
                       "text TEXT, " 
                       
                       );
             this._pushQueriesToAddColumn(queries, "noteEntry",
                       "lastupdated INTEGER",
                        "id, noteId, timestamp, text, UniqueID",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "noteId INTEGER, " +
                       "timestamp INTEGER, " +
                       "UniqueID INTEGER, " +
                       "text TEXT, " 
                       
                       );
              this._pushQueriesToAddColumn(queries, "noteEntry",
                       "isDelete INTEGER",
                        "id, noteId, timestamp, text, UniqueID, lastupdated",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "noteId INTEGER, " +
                       "timestamp INTEGER, " +
                       "lastupdated INTEGER, "+
                       "UniqueID INTEGER, " +
                       "text TEXT, " 
                       
                       );
                        
                   return { 'queries' : queries };   
        },
        //27
        function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({ 'sql' : "CREATE TABLE LastUpdates (" +
                    " TableName TEXT, " +
                    "lastupdated INTEGER " +
                    ")",
                    'rollback' : "DROP TABLE LastUpdates" });
                    
             return {
                'queries' : queries
            };
            
            },
            //28
            function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({ 'sql' : "INSERT INTO LastUpdates (" +
                    " TableName,lastupdated ) VALUES (" +
                    "'diaryItem' ,null " +
                    ")",
                    'rollback' : "" });
                    
             return {
                'queries' : queries
            };
            
            },
            //29
            function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({ 'sql' : "INSERT INTO LastUpdates (" +
                    " TableName,lastupdated ) VALUES (" +
                    "'noteEntry' ,null " +
                    ")",
                    'rollback' : "" });
                    
             return {
                'queries' : queries
            };
            
            },
            //30
            function ()
            {
            var queries = [];
            		this._pushQueriesToAddColumn(queries, "User",
                       "Password TEXT ",
                       "id, name, Country, dateOfBirth, addressStreet, addressSuburb, addressState, addressPostcode, telephone, email, grade, "+
                       "teacher, house, houseCaptain, houseColours, addressState, SchoolId, Country, LastName, IsStudent, IsTeacher, IsParent, "+
                       "ProfileImage, UniqueID, lastupdated",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "Country TEXT, " +
                       "dateOfBirth INTEGER, " +
                       "addressStreet TEXT, " +
                       "SchoolId INTEGER, " +
                       "Country TEXT, " +
                       "LastName TEXT, " +
                       "IsStudent INTEGER, " +
                       "IsTeacher INTEGER, " +
                       "IsParent INTEGER, " +
                       "ProfileImage TEXT, " +
                       "UniqueID INTEGER, " +
                       // Task fields
                       "addressSuburb TEXT, " +
                       "addressPostcode TEXT, " +
                       "telephone TEXT, " +
                       "email TEXT, " +
                       "grade TEXT, " +
                       "teacher TEXT, " +
                       "house TEXT, " +
                       "houseCaptain TEXT, " +
                       "houseColours TEXT, " +                       
                       "addressState TEXT, " +
                       "lastupdated INTEGER, "
                       
                       );
                  return {
                'queries' : queries
           		 };
            },
            //31
            function ()
            {
            var queries = [];
            		this._pushQueriesToAddColumn(queries, "timetable",
                       "TimeTableCycleId INTEGER ",
                       "id, startDate, cycleLength, name, cycleLabel, cycleDays",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "startDate INTEGER, " +
                       "cycleLength INTEGER, " +
                       "name TEXT, " +
                       "cycleLabel TEXT, " +
                       "cycleDays TEXT, " 
                      
                       );
                       
                    this._pushQueriesToAddColumn(queries, "timetable",
                       "EndDate INTEGER ",
                       "id, startDate, cycleLength, name, cycleLabel, cycleDays, TimeTableCycleId",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "startDate INTEGER, " +
                       "cycleLength INTEGER, " +
                       "name TEXT, " +
                       "cycleLabel TEXT, " +
                       "cycleDays TEXT, " +
                       "TimeTableCycleId INTEGER, " 
                      
                       );
                    this._pushQueriesToAddColumn(queries, "timetable",
                       "UniqueID INTEGER ",
                       "id, startDate, cycleLength, name, cycleLabel, cycleDays, TimeTableCycleId, EndDate",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "startDate INTEGER, " +
                       "cycleLength INTEGER, " +
                       "name TEXT, " +
                       "cycleLabel TEXT, " +
                       "cycleDays TEXT, " +
                       "TimeTableCycleId INTEGER, " +
                       "EndDate INTEGER, "
                      
                       );
                   this._pushQueriesToAddColumn(queries, "timetable",
                       "lastupdated INTEGER ",
                       "id, startDate, cycleLength, name, cycleLabel, cycleDays, TimeTableCycleId, EndDate, UniqueID",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "startDate INTEGER, " +
                       "cycleLength INTEGER, " +
                       "name TEXT, " +
                       "cycleLabel TEXT, " +
                       "cycleDays TEXT, " +
                       "TimeTableCycleId INTEGER, " +
                       "EndDate INTEGER, " +
                       "UniqueID INTEGER, "
                      
                       );
                   this._pushQueriesToAddColumn(queries, "subject",
                       "SchoolId INTEGER ",
                       "id, colour, name",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "colour TEXT, " +
                       "name TEXT, " 
                      
                       );
                       
                  this._pushQueriesToAddColumn(queries, "subject",
                       "CampusId INTEGER ",
                       "id, colour, name, SchoolId",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "colour TEXT, " +
                       "name TEXT, " +
                       "SchoolId INTEGER, "
                      
                       );
                 this._pushQueriesToAddColumn(queries, "subject",
                       "UniqueID INTEGER ",
                       "id, colour, name, SchoolId, CampusId",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "colour TEXT, " +
                       "name TEXT, " +
                       "SchoolId INTEGER, "+
                       "CampusId INTEGER, "
                      
                       );
                  this._pushQueriesToAddColumn(queries, "subject",
                       "lastupdated INTEGER ",
                       "id, colour, name, SchoolId, CampusId, UniqueID",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "colour TEXT, " +
                       "name TEXT, " +
                       "SchoolId INTEGER, "+
                       "CampusId INTEGER, " +
                       "UniqueID INTEGER, "
                      
                       );
                       
                 this._pushQueriesToAddColumn(queries, "period",
                       "UniqueID INTEGER ",
                       "id, timetable, startTime, endTime, cycleDay, isBreak",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "timetable INTEGER, " +
                       "startTime INTEGER, " +
                       "endTime INTEGER, "+
                       "cycleDay INTEGER, " +
                       "isBreak INTEGER, "
                      
                       );
                 this._pushQueriesToAddColumn(queries, "period",
                       "lastupdated INTEGER ",
                       "id, timetable, startTime, endTime, cycleDay, isBreak, UniqueID",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "timetable INTEGER, " +
                       "startTime INTEGER, " +
                       "endTime INTEGER, "+
                       "cycleDay INTEGER, " +
                       "isBreak INTEGER, " +
                       "UniqueID INTEGER, "
                      
                       );
                       
                       
                  return {
                'queries' : queries
           		 };
            },
            //32
        function () {
            var queries = [];

            queries.push({ 'sql' : "CREATE TABLE Room (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "SchoolId INTEGER, " +
                    "CampusId INTEGER, " +
                    "Name TEXT(100), " +
                    "Code TEXT(100), " +
                    "UniqueID INTEGER, " +
                    "lastupdated INTEGER "+
                    ")",
                    'rollback' : "DROP TABLE Room" });
                    
             return {
                'queries' : queries
            };
            },
            //33
        function () {
            var queries = [];

            queries.push({ 'sql' : "CREATE TABLE TimeTableCycles (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "SchoolId INTEGER, " +
                    "CampusId INTEGER, " +
                    "CycleDays INTEGER, " +
                    "CycleLength INTEGER, " +
                    "CycleLabel TEXT, " +
                    "UniqueID INTEGER, "+
                    "lastupdated INTEGER "+
                    ")",
                    'rollback' : "DROP TABLE TimeTableCycles" });
                    
             return {
                'queries' : queries
            };
            
            },
            //34
        function () {
            var queries = [];

            this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "UniqueID INTEGER",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, " 
                       );
                       
             this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "lastupdated INTEGER",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, "
                       );
               this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "Address_line_1 TEXT(200)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, "
                       );
                       
              this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "Address_line_2 TEXT(200)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, "
                       );
                 this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "CityTownSuburb TEXT(100)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1, Address_line_2",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, " +
                       "Address_line_2 TEXT, "
                       );
                 this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "PostalCode TEXT(10)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1, Address_line_2, CityTownSuburb",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, " +
                       "Address_line_2 TEXT, " +
                       "CityTownSuburb TEXT, "
                       );
                 this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "State TEXT(100)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1, Address_line_2, CityTownSuburb, PostalCode",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, " +
                       "Address_line_2 TEXT, " +
                       "CityTownSuburb TEXT, " +
                       "PostalCode TEXT"
                       );
                this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "Country TEXT(100)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1, Address_line_2, CityTownSuburb, PostalCode, State",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, " +
                       "Address_line_2 TEXT, " +
                       "CityTownSuburb TEXT, " +
                       "PostalCode TEXT, " +
                       "State TEXT, "
                       );
                this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "Number_of_Campuses INTEGER",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1, Address_line_2, CityTownSuburb, PostalCode, State, Country",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, " +
                       "Address_line_2 TEXT, " +
                       "CityTownSuburb TEXT, " +
                       "PostalCode TEXT, " +
                       "State TEXT, " +
                       "Country TEXT, "
                       );
                 this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "Phone_Number TEXT(30)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1, Address_line_2, CityTownSuburb, PostalCode, State, Country, Number_of_Campuses",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, " +
                       "Address_line_2 TEXT, " +
                       "CityTownSuburb TEXT, " +
                       "PostalCode TEXT, " +
                       "State TEXT, " +
                       "Country TEXT, " +
                       "Number_of_Campuses INTEGER, "
                       );
                 this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "Fax_Number TEXT(30)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1, Address_line_2, CityTownSuburb, PostalCode, State, Country, Number_of_Campuses, "+
                       "Phone_Number",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, " +
                       "Address_line_2 TEXT, " +
                       "CityTownSuburb TEXT, " +
                       "PostalCode TEXT, " +
                       "State TEXT, " +
                       "Country TEXT, " +
                       "Number_of_Campuses INTEGER, "+
                       "Phone_Number TEXT, "
                       );
                   this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "Website_URL TEXT(200)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1, Address_line_2, CityTownSuburb, PostalCode, State, Country, Number_of_Campuses, "+
                       "Phone_Number, Fax_Number",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, " +
                       "Address_line_2 TEXT, " +
                       "CityTownSuburb TEXT, " +
                       "PostalCode TEXT, " +
                       "State TEXT, " +
                       "Country TEXT, " +
                       "Number_of_Campuses INTEGER, "+
                       "Phone_Number TEXT, " +
                       "Fax_Number TEXT"
                       );
                   
                    
             return {
                'queries' : queries
            };
            
            },
			//35
			function()
			{
				var queries=[];
				this._pushQueriesToDropColumn(queries, "class",
				                       // Diary Item fields
				                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				                       "timetable INTEGER, " +
				                       "cycleDay INTEGER, " +
				                       "periodId INTEGER, " +
				                       "subjectId INTEGER " ,
                       
				                       "id, timetable, cycleDay, periodId, subjectId ",
                       
				                       "room TEXT"
				                       );
				             this._pushQueriesToAddColumn(queries, "class",
				                       "Name TEXT",
				                       "id, timetable, cycleDay, periodId, subjectId ",
                       
				                       // Diary Item fields
				                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				                       "timetable INTEGER, " +
				                       "cycleDay INTEGER, " +
				                       "periodId INTEGER, " +
				                       "subjectId INTEGER, " 
                       
				                       );
             
				            this._pushQueriesToAddColumn(queries, "class",
				                       "Code TEXT",
				                       "id, timetable, cycleDay, periodId, subjectId, Name ",
                       
				                       // Diary Item fields
				                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				                       "timetable INTEGER, " +
				                       "cycleDay INTEGER, " +
				                       "periodId INTEGER, " +
				                       "subjectId INTEGER, "+
				                       "Name TEXT, " 
                       
				                       );
				            this._pushQueriesToAddColumn(queries, "class",
				                       "UniqueID INTEGER ",
				                       "id, timetable, cycleDay, periodId, subjectId, Name, Code ",
                       
				                       // Diary Item fields
				                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				                       "timetable INTEGER, " +
				                       "cycleDay INTEGER, " +
				                       "periodId INTEGER, " +
				                       "subjectId INTEGER, "+
				                       "Name TEXT, "+
				                       "Code TEXT, " 
                       
				                       );           
				             this._pushQueriesToAddColumn(queries, "class",
				                       "lastupdated DATE",
				                       "id, timetable, cycleDay, periodId, subjectId, Name, Code ",
                       
				                       // Diary Item fields
				                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				                       "UniqueID INTEGER , "+
				                       "timetable INTEGER, " +
				                       "cycleDay INTEGER, " +
				                       "periodId INTEGER, " +
				                       "subjectId INTEGER, "+
				                       "Name TEXT, "+
				                       "Code TEXT, " 
                       
				                       );  
				              this._pushQueriesToAddColumn(queries, "class",
				             		   " RoomId INTEGER ", 
				                       "id, timetable, cycleDay, periodId, subjectId, Name, Code, lastupdated",
                       
				                       // Diary Item fields
				                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				                       "UniqueID INTEGER, "+
				                       "lastupdated DATE, "+
				                       "timetable INTEGER, " +
				                       "cycleDay INTEGER, " +
				                       "periodId INTEGER, " +
				                       "subjectId INTEGER, "+
				                       "Name TEXT, "+
				                       "Code TEXT, " 
                       
				                       );
		 				              this._pushQueriesToAddColumn(queries, "class",
		 				             		   " isSync INTEGER ", 
		 				                       "id, timetable, cycleDay, periodId, subjectId, Name, Code, lastupdated, RoomId",
                       
		 				                       // Diary Item fields
		 				                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		 				                       "UniqueID INTEGER, "+
		 				                       "lastupdated DATE, "+
		 				                       "timetable INTEGER, " +
		 				                       "cycleDay INTEGER, " +
		 				                       "periodId INTEGER, " +
		 				                       "subjectId INTEGER, "+
		 				                       "Name TEXT, "+
		 				                       "Code TEXT, "+
											   "RoomId INTEGER, "
                       
		 				                       );
									   
									   
						               return {
						                  'queries' : queries
						              };
			},
			//36
			function () {
            var queries = [];

            queries.push({ 'sql' : "CREATE TABLE ClassUser (" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "classId INTEGER, " +
                    "userId INTEGER, " +
                    "cycleDay INTEGER, " +
                    "code TEXT, " +
                    "UniqueID INTEGER, " +
                    "lastupdated INTEGER "+
                    ")",
                    'rollback' : "DROP TABLE TimeTableCycles" });
                    
             return {
                'queries' : queries
            };
            
            },
            //37
			function () {
            var queries = [];

            queries.push({ 'sql' : "CREATE TABLE Country (" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "UniqueID INTEGER, " +
                    "name TEXT " +
                    ")",
                    'rollback' : "DROP TABLE Country" });
                    
                    
             queries.push({ 'sql' : "CREATE TABLE State (" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "UniqueID INTEGER, " +
                    "CountryId INTEGER, " +
                    "Name TEXT " +
                    ")",
                    'rollback' : "DROP TABLE State" });
                    
             return {
                'queries' : queries
            };
            
            },
            //38
			function ()
			{
				var queries=[];
				 this._pushQueriesToAddColumn(queries, "ClassUser",
		 				             		   "isSync INTEGER", 
		 				                       "id, classId, userId, cycleDay, code, UniqueID, lastupdated",
                       
		 				                       // Diary Item fields
						                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
						                       "classId INTEGER, " +
						                       "userId INTEGER, " +
						                       "cycleDay INTEGER, " +
						                       "code TEXT, " +
						                       "UniqueID INTEGER, " +
						                       "lastupdated INTEGER, "
		 				                       );
									   
									   
						               return {
						                  'queries' : queries
						              };
			},
			//39
			function ()
			{
				var queries=[];
				this._pushQueriesToDropColumn(queries, "diaryItem",
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "type TEXT, " +
                       "title TEXT, " +
                       "description TEXT, " +
                       "assignedDay INTEGER, "+
                       "assignedTime INTEGER, "+
                       "isDelete INTEGER, "+
                       "Progress INTEGER, "+
                       "endDay DATE, "+
                       "dueDay INTEGER, "+
                       "dueTime INTEGER, "+
                       "completed INTEGER, "+
                       "subjectId INTEGER, "+
                       "startDay INTEGER, "+
                       "startTime INTEGER, "+
                       "endTime INTEGER, "+
                       "locked INTEGER, "+
                       "messageFromText TEXT, "+
                       "responseRequired INTEGER, "+
                       "messageFrom TEXT, "+
                       "SchoolId  INTEGER, "+
                       "ClassId  INTEGER, " +
                       "UniqueID INTEGER, "+
                       "EstimatedHours TEXT, "+
                       "EstimatedMins TEXT, "+
                       "AssingedBy  INTEGER, "+
                       "lastupdated INTEGER, "+
                       "isSync INTEGER, "+
                       "created INTEGER ",
                       
                       "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                        "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated, isSync, Progress, isDelete, "+
                        "endDay",
                       
                       "priority INTEGER"
                       );
					return {
						                  'queries' : queries
						              };
			},
			//40
			function ()
			{
				var queries=[];
				 this._pushQueriesToAddColumn(queries, "diaryItem",
		 				             		   "priority TEXT", 
		 				                       "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                         					   "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated, isSync, Progress, isDelete, "+
                         					   "endDay",
                       
		 				                       // Diary Item fields
						                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       						   "type TEXT, " +
                       						   "title TEXT, " +
                       						   "description TEXT, " +
                        					   "assignedDay INTEGER, "+
                        					   "assignedTime INTEGER, "+
                        					   "isDelete INTEGER, "+
                        					   "Progress INTEGER, "+
                        					   "endDay DATE, "+
                        					   "dueDay INTEGER, "+
                        					   "dueTime INTEGER, "+
                        					   "completed INTEGER, "+
                        					   "subjectId INTEGER, "+
                        					   "startDay INTEGER, "+
                        					   "startTime INTEGER, "+
                        					   "endTime INTEGER, "+
                        					   "locked INTEGER, "+
                        					   "messageFromText TEXT, "+
                        					   "responseRequired INTEGER, "+
                        					   "messageFrom TEXT, "+
                        					   "SchoolId  INTEGER, "+
                        					   "ClassId  INTEGER, " +
                        					   "UniqueID INTEGER, "+
                        					   "EstimatedHours TEXT, "+
                        					   "EstimatedMins TEXT, "+
                        					   "AssingedBy  INTEGER, "+
                        					   "lastupdated INTEGER, "+
                        					   "isSync INTEGER, "+
                        					   "created INTEGER, "
		 				                       );
									   
									   
						               return {
						                  'queries' : queries
						              };
			},
			//41
			function ()
			{
				var queries=[];
				 this._pushQueriesToAddColumn(queries, "diaryItem",
		 				             		   "UserID INTEGER", 
		 				                       "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                         					   "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated, isSync, Progress, isDelete, "+
                         					   "endDay, priority",
                       
		 				                       // Diary Item fields
						                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       						   "type TEXT, " +
                       						   "title TEXT, " +
                       						   "description TEXT, " +
                        					   "assignedDay INTEGER, "+
                        					   "assignedTime INTEGER, "+
                        					   "isDelete INTEGER, "+
                        					   "Progress INTEGER, "+
                        					   "endDay DATE, "+
                        					   "dueDay INTEGER, "+
                        					   "dueTime INTEGER, "+
                        					   "completed INTEGER, "+
                        					   "subjectId INTEGER, "+
                        					   "startDay INTEGER, "+
                        					   "startTime INTEGER, "+
                        					   "endTime INTEGER, "+
                        					   "locked INTEGER, "+
                        					   "messageFromText TEXT, "+
                        					   "responseRequired INTEGER, "+
                        					   "messageFrom TEXT, "+
                        					   "SchoolId  INTEGER, "+
                        					   "ClassId  INTEGER, " +
                        					   "UniqueID INTEGER, "+
                        					   "EstimatedHours TEXT, "+
                        					   "EstimatedMins TEXT, "+
                        					   "AssingedBy  INTEGER, "+
                        					   "lastupdated INTEGER, "+
                        					   "isSync INTEGER, "+
                        					   "priority TEXT, "+
                        					   "created INTEGER, "
		 				                       );
									   
									   
						               return {
						                  'queries' : queries
						              };
			},
            //42
            function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({ 'sql' : "INSERT INTO LastUpdates (" +
                    "TableName, lastupdated ) VALUES (" +
                    "'ClassUser',null " +
                    ")",
                    'rollback' : "" });
                    
             return {
                'queries' : queries
            };
            
            },
            //43
			function ()
			{
				var queries=[];
				this._pushQueriesToDropColumn(queries, "class",
				                       // Diary Item fields
				                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				                       "timetable INTEGER, " +
				                       "cycleDay INTEGER, " +
				                       "periodId INTEGER, " +
				                       "subjectId INTEGER, " +
									   "Name TEXT, " +
									   "Code TEXT, " +
									   "UniqueID INTEGER, " +
									   "RoomId INTEGER, " +
									   "isSync INTEGER " ,
                       
				                       "id, timetable, cycleDay, periodId, subjectId, Name, Code, UniqueID, RoomId, isSync",
                       
				                       "lastupdated DATE"
				                       );
									   
 		 	   this._pushQueriesToAddColumn(queries, "class",
 		 				             		   " lastupdated INTEGER ", 
 		 				                       "id, timetable, cycleDay, periodId, subjectId, UniqueID, Name, Code, RoomId, isSync",
                       
 		 				                       // Diary Item fields
 		 				                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
 		 				                       "timetable INTEGER, "+
 		 				                       "UniqueID INTEGER, " +
 		 				                       "cycleDay INTEGER, " +
 		 				                       "periodId INTEGER, " +
 		 				                       "subjectId INTEGER, "+
 		 				                       "Name TEXT, " +
 		 				                       "Code TEXT, " +
 											   "RoomId INTEGER, " +
											   "isSync INTEGER, "
                       
 		 				                       );
								               return {
								                  'queries' : queries
								              };
			},
			//44
			function ()
			{
				var queries=[];
				this._pushQueriesToAddColumn(queries, "schoolProfile",
                       "AllowSynch INTEGER(1)",
                       "id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower, theme, AllowAutoUpdates, RegionalFormat, UniqueID, "+
                       "lastupdated, Address_line_1, Address_line_2, CityTownSuburb, PostalCode, State, Country, Number_of_Campuses,Website_URL,  "+
                       "Phone_Number, Fax_Number",
                       
                       // Diary Item fields
                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       "name TEXT, " +
                       "schoolLogo TEXT, " +
                       "schoolImage TEXT, " +
                       "schoolMotto TEXT, " +
                       
                       // Task fields
                       "missionStatement TEXT, " +
                       "diaryTitle TEXT, " +
                       "includeEmpower INTEGER, "+
                       "theme TEXT, "+
                       "AllowAutoUpdates INTEGER, "+
                       "RegionalFormat TEXT, "+
                       "UniqueID INTEGER, " +
                       "lastupdated INTEGER, " +
                       "Address_line_1 TEXT, " +
                       "Address_line_2 TEXT, " +
                       "CityTownSuburb TEXT, " +
                       "PostalCode TEXT, " +
                       "State TEXT, " +
                       "Country TEXT, " +
                       "Number_of_Campuses INTEGER, "+
                       "Phone_Number TEXT, " +
                       "Fax_Number TEXT, " +
                       "Website_URL TEXT(200) "
                       );
                   
                    
             return {
                'queries' : queries
            };
			},
			//45
	        function()
	        {
	         var queries =[];
         
	        queries.push({
					'sql' : "ALTER TABLE LastUpdates RENAME TO lastSynch",
					'rollback' : _SQL_NO_OP
				});
			 this._pushQueriesToAddColumn(queries, "ClassUser",
	 				             		   "isDelete INTEGER", 
	 				                       "id, classId, userId, cycleDay, code, UniqueID, lastupdated, isSync",
                      
	 				                       // Diary Item fields
					                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					                       "classId INTEGER, " +
					                       "userId INTEGER, " +
					                       "cycleDay INTEGER, " +
					                       "code TEXT, " +
					                       "UniqueID INTEGER, " +
					                       "lastupdated INTEGER, "+
										   "isSync INTEGER, "
	 				                       );
			
	        return { 'queries' : queries };
	        },
			//46
	        function()
	        {
	         var queries =[];

	        
                this._pushQueriesToAddColumn(queries, "noteEntry",
                         "userId INTEGER",
                          "id, noteId, timestamp, text, UniqueID, lastupdated, isDelete",
                       
                         // Diary Item fields
                         "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                         "noteId INTEGER, " +
                         "timestamp INTEGER, " +
                         "lastupdated INTEGER, "+
                         "UniqueID INTEGER, " +
                         "text TEXT, " +
						 "isDelete INTEGER, " 
                       
                         );
		                 this._pushQueriesToAddColumn(queries, "noteEntry",
		                          "isSynch INTEGER",
		                           "id, noteId, timestamp, text, UniqueID, lastupdated, isDelete, userId",
                       
		                          // Diary Item fields
		                          "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		                          "noteId INTEGER, " +
		                          "timestamp INTEGER, " +
		                          "lastupdated INTEGER, "+
		                          "UniqueID INTEGER, " +
		                          "text TEXT, " +
		 						 "isDelete INTEGER, " +
								 "userId INTEGER, " 
                       
		                          );			 

	        return { 'queries' : queries };
			
	        },
	        //47
	        function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({ 'sql' : "DROP TABLE lastSynch",
                    'rollback' : "" });
                    
            
            queries.push({ 'sql' : "CREATE TABLE lastSynch (" +
           			"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
                    "tableName TEXT, " +
                    "lastSynch INTEGER, " +
                    "appType TEXT, "+
     				"contentsync TEXT "+
                    ")",
                    'rollback' : "DROP TABLE lastSynch" });
                    
             return {
                'queries' : queries
            };
            
            },
            //48
             function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({ 'sql' : "INSERT INTO lastSynch (" +
                    "tableName, lastSynch, appType) VALUES (" +
                    "'ClassUser',null, 'desktop' " +
                    ")",
                    'rollback' : "" });
           queries.push({ 'sql' : "INSERT INTO lastSynch (" +
                    " tableName,lastSynch,appType ) VALUES (" +
                    "'diaryItem' ,null, 'desktop'" +
                    ")",
                    'rollback' : "" });
           queries.push({ 'sql' : "INSERT INTO lastSynch (" +
                    " tableName,lastSynch,appType) VALUES (" +
                    "'noteEntry' ,null, 'desktop'" +
                    ")",
                    'rollback' : "" });
                    
             return {
                'queries' : queries
            };
            },
			//49
            function()
            {
             var queries = [];
            	this._pushQueriesToAddColumn(queries, "ClassUser",
	 				             		   "createdBy INTEGER", 
	 				                       "id, classId, userId, cycleDay, code, UniqueID, lastupdated, isSync, isDelete",
                      
	 				                       // Diary Item fields
					                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					                       "classId INTEGER, " +
					                       "userId INTEGER, " +
					                       "cycleDay INTEGER, " +
					                       "code TEXT, " +
					                       "UniqueID INTEGER, " +
					                       "lastupdated INTEGER, "+
					                       "isDelete INTEGER, "+
										   "isSync INTEGER, "
	 				                       );
	 				                       
				return {
                'queries' : queries
            };
            
            },
			//50
			function ()
			{
				var queries=[];
				 this._pushQueriesToAddColumn(queries, "diaryItem",
		 				             		   "createdBy INTEGER", 
		 				                       "id, type, title, description, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFromText, responseRequired, messageFrom, created, "+
                         					   "SchoolId, ClassId, UniqueID, AssingedBy, EstimatedHours, EstimatedMins, lastupdated, isSync, Progress, isDelete, "+
                         					   "endDay, priority, UserID",
                       
		 				                       // Diary Item fields
						                       "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                       						   "type TEXT, " +
                       						   "title TEXT, " +
                       						   "description TEXT, " +
                        					   "assignedDay INTEGER, "+
                        					   "assignedTime INTEGER, "+
                        					   "isDelete INTEGER, "+
                        					   "Progress INTEGER, "+
                        					   "endDay DATE, "+
                        					   "dueDay INTEGER, "+
                        					   "dueTime INTEGER, "+
                        					   "completed INTEGER, "+
                        					   "subjectId INTEGER, "+
                        					   "startDay INTEGER, "+
                        					   "startTime INTEGER, "+
                        					   "endTime INTEGER, "+
                        					   "locked INTEGER, "+
                        					   "messageFromText TEXT, "+
                        					   "responseRequired INTEGER, "+
                        					   "messageFrom TEXT, "+
                        					   "SchoolId  INTEGER, "+
                        					   "ClassId  INTEGER, " +
                        					   "UniqueID INTEGER, "+
                        					   "EstimatedHours TEXT, "+
                        					   "EstimatedMins TEXT, "+
                        					   "AssingedBy  INTEGER, "+
                        					   "lastupdated INTEGER, "+
                        					   "isSync INTEGER, "+
                        					   "priority TEXT, "+
                        					   "created INTEGER, "+
											   "UserID INTEGER, "
		 				                       );
									   
									   
						               return {
						                  'queries' : queries
						              };
			},
   //50
   function ()
   {
     var queries=[];
    
    
     queries.push({ 'sql' : "INSERT INTO lastSynch (" +
                    "tableName, lastSynch, appType ,contentsync) VALUES (" +
                    "'contentsyncData',null, 'desktop', null" +
                    ")",
                    'rollback' : "" });
				
	 queries.push({ 'sql' : "INSERT INTO lastSynch (" +
                    "tableName, lastSynch, appType ,contentsync) VALUES (" +
                    "'imagesyncing',null, 'desktop', null" +
                    ")",
                    'rollback' : "" });
     
     queries.push({ 'sql' : "INSERT INTO lastSynch (" +
                    "tableName, lastSynch, appType ,contentsync) VALUES (" +
                    "'inboxItem',null, 'desktop', null" +
                    ")",
                    'rollback' : "" });
					
     
       return {
                        'queries' : queries
                    };
     
     },
	 function ()
	 {
		  var queries=[];
	 		
            this._pushQueriesToAddColumn(queries, "noteEntry",
                     "noteUniqueId INTEGER",
                      "id, noteId, timestamp, text, UniqueID, lastupdated, isDelete, userId, isSynch",
          
                     // Diary Item fields
                     "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                     "noteId INTEGER, " +
                     "timestamp INTEGER, " +
                     "lastupdated INTEGER, "+
                     "UniqueID INTEGER, " +
                     "text TEXT, " +
				 "isDelete INTEGER, " +
				 "userId INTEGER, " +
				 "isSynch INTEGER, "
          
                     );	
			         return {
			                          'queries' : queries
			                      };
	 }
			
							
    ];*/
	
	
	
	
	///**********************SHRUTI************************//
	var _schema_versions = [

		// Version 3 (earliest supported version)
		{
			
			queries: [
				{ 'sql' : "CREATE TABLE studentProfile (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NULL, " +
					" dateOfBirth VARCHAR(20) NULL, " +
					" addressStreet TEXT(200) NULL, " +
					" addressSuburb TEXT(100) NULL, " +
					" addressState TEXT(100) NULL, " +
					" addressPostcode TEXT(7) NULL, " +
					" telephone TEXT(50) NULL, " +
					" email TEXT(100) NULL, " +
					" grade TEXT(50) NULL, " +
					" teacher TEXT(500) NULL, " +
					" classCaptain TEXT(100) NULL, " +
					" house TEXT(100) NULL, " +
					" houseCaptain TEXT(100) NULL, " +
					" houseColours TEXT(200) NULL" +
				//	" image BLOB NULL," +
				//	" isSync INTEGER(1) NULL" +
					")",
					'rollback' : "DROP TABLE studentProfile" },

				{ 'sql' : "CREATE TABLE schoolProfile (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NULL, " +
					" schoolLogo TEXT(255) NULL, " +
					" schoolImage TEXT(255) NULL, " +
					" schoolMotto TEXT NULL, " +
					" missionStatement TEXT NULL, " +
					" diaryTitle TEXT(100) NULL, " +
					" includeEmpower INTEGER(1) NOT NULL DEFAULT 0 " +
					")",
					'rollback' : "DROP TABLE schoolProfile" },

				{ 'sql' : "CREATE TABLE diaryItem (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" type TEXT(100) NOT NULL, " +
					" date INTEGER(20) NOT NULL, " +
					" startTime INTEGER(20) NULL, " +
					" endTime INTEGER(20) NULL, " +
					" period INTEGER(20) NULL, " +
					" title TEXT(100) NOT NULL, " +
					" locked INTEGER(1) NOT NULL, " +
					" completed INTEGER(20) NULL, " +
					" reminder INTEGER(20) NULL, " +
					" subjectId INTEGER(20) NULL, " +
					" description TEXT NULL, " +
					" isDeletedFromList INTEGER(1) NOT NULL, " +
					" dateCreated INTEGER(20) NOT NULL DEFAULT 0 " +
					")",
					'rollback' : "DROP TABLE diaryItem" },

				{ 'sql' : "CREATE TABLE attachment (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" diaryItemId INTEGER NOT NULL, " +
					" originalFilePath TEXT NOT NULL, " +
					" displayName TEXT(100) NOT NULL, " +
					" fileType TEXT(100) NULL, " +
					" localFileName TEXT(100) NOT NULL UNIQUE, " +
					" dateAttached INTEGER(20)  " +
					")",
					'rollback' : "DROP TABLE attachment" },

				{ 'sql' : "CREATE TABLE subject (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NOT NULL, " +
					" colour TEXT(100)" +
					")",
					'rollback' : "DROP TABLE subject" },

				{ 'sql' : "CREATE TABLE dayOfNote (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" date INTEGER(20) NOT NULL, " +
					" overrideCycleDay INTEGER(20), " +
					" includeInCycle INTEGER(1), " +
					" isGreyedOut INTEGER(1), " +
					" title TEXT(100), " +
					" overrideCycle INTEGER(20) " +
					" )",
					'rollback' : "DROP TABLE dayOfNote" },

				{ 'sql' : "CREATE TABLE timetable (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" startDate INTEGER(20) NOT NULL, " +
					" cycleLength INTEGER(20) NOT NULL " +
					")",
					'rollback' : "DROP TABLE timetable" },

				{ 'sql' : "CREATE TABLE period (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" timetable INTEGER NOT NULL, " +
					" startTime INTEGER(20) NOT NULL, " +
					" endTime INTEGER(20) NULL, " +
					" title TEXT(100) NULL, " +
					" cycleDay INTEGER(20) NULL, " +
					"isBreak INTEGER(1)" +
					")",
					'rollback' : "DROP TABLE period" },

				{ 'sql' : "CREATE TABLE class ( " +
					"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					"timetable INTEGER NOT NULL, " +
					"cycleDay INTEGER(20) NOT NULL, " +
					"periodId INTEGER(20) NOT NULL, " +
					"subjectId INTEGER(20) NOT NULL, " +
					"room TEXT(100) NULL " +
					")",
					'rollback' : "DROP TABLE class" },

				{ 'sql' : "CREATE TABLE noteEntry (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" noteId INTEGER, " +
					" timestamp INTEGER(20) NOT NULL, " +
					" text TEXT NULL, " +
					" noteFrom TEXT(100) NULL " +
					")",
					'rollback' : "DROP TABLE noteEntry" },

				// Any document that is to be searched
				{ 'sql' : "CREATE TABLE document (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" type TEXT(100) NOT NULL, " +
					" title TEXT(255) NOT NULL, " +
					" documentId TEXT(300) NOT NULL " +
					")",
					'rollback' : "DROP TABLE document" },

				// The words that a user can search by
				{ 'sql' : "CREATE TABLE stemmedWords (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" word TEXT(150) NOT NULL, " +
					" UNIQUE (word) " +
					")",
					'rollback' : "DROP TABLE stemmedWords" },

				// For mapping words to documents
				{ 'sql' : "CREATE TABLE occurs (" +
					" documentId INTEGER NOT NULL, " +
					" stemmedWordId INTEGER NOT NULL, " +
					" PRIMARY KEY (documentId, stemmedWordId) " +
					")",
					'rollback' : "DROP TABLE stemmedWords" },

				{ 'sql' : "CREATE TABLE systemConfig (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" email TEXT(100) NULL, " +
					" lastSchoolProfileCheck INTEGER," +
					" schoolConfigTimestamp TEXT(100) " +
					")",
					'rollback' : "DROP TABLE systemConfig" },

				{ 'sql' : "CREATE TABLE storedContentPackages (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" package TEXT(100) NULL, " +
					" dateLastUpdated INTEGER NULL" +
					")",
					'rollback' : "DROP TABLE storedContentPackages" },

				{ 'sql' : "CREATE TABLE contentItemForm (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" url TEXT NOT NULL, " +
					" form TEXT NOT NULL " +
					")",
					'rollback' : "DROP TABLE contentItemForm" },

				{ 'sql' : "CREATE TABLE usageLog (" +
					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" time INTEGER(20) NOT NULL, " +
					" type TEXT(100) NOT NULL, " +
					" detail TEXT NULL, " +
					" token TEXT(10) NOT NULL, " +
					" macAddresses TEXT NOT NULL, " +
					" schoolConfiguration TEXT NOT NULL, " +
					" version INTEGER NOT NULL, " +
					" clientVersion TEXT NOT NULL, " +
					" synced INTEGER(1) NOT NULL" +
					")",
					'rollback' : "DROP TABLE usageLog" }
				
			]
		},

		// Version 4 - timetable names (specified as a function, rather than a simple object)
		function () {

			var queries = [];

			// Add `timetable`.`name` column
			this._pushQueriesToAddColumn(queries, "timetable", "name TEXT(100) NULL", "id, startDate, cycleLength",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"startDate INTEGER(20) NOT NULL, " +
				"cycleLength INTEGER(20) NOT NULL ");

			// Return the queries
			return {
				'queries' : queries
			};
		},

		// Version 5 - cycle day labels
		function () {
			var queries = [];

			this._pushQueriesToAddColumn(queries, "timetable", "cycleLabel TEXT(100) NULL", "id, startDate, cycleLength, name",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"startDate INTEGER(20) NOT NULL, " +
				"cycleLength INTEGER(20) NOT NULL, " +
				"name TEXT(100) NULL");

			this._pushQueriesToAddColumn(queries, "timetable", "cycleDays TEXT NULL", "id, startDate, cycleLength, name, cycleLabel",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"startDate INTEGER(20) NOT NULL, " +
				"cycleLength INTEGER(20) NOT NULL, " +
				"name TEXT(100) NULL, " +
				"cycleLabel TEXT(100) NULL");

			return {
				'queries' : queries
			};
		},

		// Version 6 - delete noteFrom column
		function () {

			var queries = [];

			// Drop `noteEntry`.`noteFrom` column
			this._pushQueriesToDropColumn(queries, "noteEntry",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					"noteId INTEGER, " +
					"timestamp INTEGER(20) NOT NULL, " +
					"text TEXT NULL",
				"id, noteId, timestamp, text",
				"noteFrom TEXT(100) NULL"
			);

			// Return the queries
			return {
				'queries' : queries
			};
		},

		// Version 7 - message fields
		function () {

			var queries = [];

			// Add `diaryItem`.`messageFrom` column
			this._pushQueriesToAddColumn(queries, "diaryItem", "messageFrom TEXT(100) NULL",
				"id, type, date, startTime, endTime, period, title, locked, completed, reminder, subjectId, description, dateCreated",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT(100) NOT NULL, " +
				"date INTEGER(20) NOT NULL, " +
				"startTime INTEGER(20) NULL, " +
				"endTime INTEGER(20) NULL, " +
				"period INTEGER(20) NULL, " +
				"title TEXT(100) NOT NULL, " +
				"locked INTEGER(1) NOT NULL, " +
				"completed INTEGER(20) NULL, " +
				"reminder INTEGER(20) NULL, " +
				"subjectId INTEGER(20) NULL, " +
				"description TEXT NULL, " +
				"dateCreated INTEGER(20) NOT NULL DEFAULT 0"
			);

			// Add `diaryItem`.`messageFromText` column
			this._pushQueriesToAddColumn(queries, "diaryItem", "messageFromText TEXT(100) NULL",
				"id, type, date, startTime, endTime, period, title, locked, completed, reminder, subjectId, messageFrom, description, dateCreated",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT(100) NOT NULL, " +
				"date INTEGER(20) NOT NULL, " +
				"startTime INTEGER(20) NULL, " +
				"endTime INTEGER(20) NULL, " +
				"period INTEGER(20) NULL, " +
				"title TEXT(100) NOT NULL, " +
				"locked INTEGER(1) NOT NULL, " +
				"completed INTEGER(20) NULL, " +
				"reminder INTEGER(20) NULL, " +
				"subjectId INTEGER(20) NULL, " +
				"messageFrom TEXT(100) NULL, " +
				"description TEXT NULL, " +
				"dateCreated INTEGER(20) NOT NULL DEFAULT 0"
			);
			
			// Add `diaryItem`.`responseRequired` column
			this._pushQueriesToAddColumn(queries, "diaryItem", "responseRequired INTEGER(1) NOT NULL DEFAULT 0",
				"id, type, date, startTime, endTime, period, title, locked, completed, reminder, subjectId, messageFrom, messageFromText, description, dateCreated",
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT(100) NOT NULL, " +
				"date INTEGER(20) NOT NULL, " +
				"startTime INTEGER(20) NULL, " +
				"endTime INTEGER(20) NULL, " +
				"period INTEGER(20) NULL, " +
				"title TEXT(100) NOT NULL, " +
				"locked INTEGER(1) NOT NULL, " +
				"completed INTEGER(20) NULL, " +
				"reminder INTEGER(20) NULL, " +
				"subjectId INTEGER(20) NULL, " +
				"messageFrom TEXT(100) NULL, " +
				"messageFromText TEXT(100) NULL, " +
				"description TEXT NULL, " +
				"dateCreated INTEGER(20) NOT NULL DEFAULT 0"
			);

			// Return the queries
			return {
				'queries' : queries
			};
		},

        // Version 8 - delete studentProfile.classCaptain column
        function () {

            var queries = [];

            // Drop `studentProfile`.`classCaptain` column
            this._pushQueriesToDropColumn(queries, "studentProfile",
                " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    " name TEXT(100) NULL, " +
                    " dateOfBirth VARCHAR(20) NULL, " +
                    " addressStreet TEXT(200) NULL, " +
                    " addressSuburb TEXT(100) NULL, " +
                    " addressState TEXT(100) NULL, " +
                    " addressPostcode TEXT(7) NULL, " +
                    " telephone TEXT(50) NULL, " +
                    " email TEXT(100) NULL, " +
                    " grade TEXT(50) NULL, " +
                    " teacher TEXT(500) NULL, " +
                    " house TEXT(100) NULL, " +
                    " houseCaptain TEXT(100) NULL, " +
                    " houseColours TEXT(200) NULL ",
                "id, name, dateOfBirth, addressStreet, addressSuburb, addressState, " +
                "addressPostcode, telephone, email, grade, teacher, house, houseCaptain, houseColours",
                "classCaptain TEXT(100) NULL"
            );

            // Return the queries
            return {
                'queries' : queries
            };
        },

		// Version 9 - [PD-659]
		function () {

			var queries = [];

			// Drop diaryItem.reminder column
			this._pushQueriesToDropColumn(queries, "diaryItem",
				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +

				// Task fields
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"dateCreated INTEGER, " +
				"date INTEGER, " +
				"period INTEGER",

				"id, type, title, description, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, dateCreated, date, period",
				"reminder INTEGER"
			);

			// Rename diaryItem.dateCreated to diaryItem.created
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"created INTEGER",
				"id, type, title, description, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, dateCreated, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +

				// Task fields
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"dateCreated INTEGER",
				"date INTEGER, " +
				"period INTEGER"
			);

			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET created = dateCreated",
				'rollback' : _SQL_NO_OP
			});

			this._pushQueriesToDropColumn(queries, "diaryItem",
				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER",

				"id, type, title, description, created, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",
				"dateCreated INTEGER"
			);

			// Add diaryItem.assignedDay column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"assignedDay INTEGER",
				"id, type, title, description, created, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Add diaryItem.assignedTime column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"assignedTime INTEGER",
				"id, type, title, description, created, assignedDay, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Add diaryItem.dueDay column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"dueDay INTEGER",
				"id, type, title, description, created, assignedDay, assignedTime, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER DEFAULT 0, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Add diaryItem.dueTime column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"dueTime INTEGER",
				"id, type, title, description, created, assignedDay, assignedTime, dueDay, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"dueDay INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Add diaryItem.startDay column
			this._pushQueriesToAddColumn(queries, "diaryItem",
				"startDay INTEGER",
				"id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, date, period",

				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"dueDay INTEGER, " +
				"dueTime INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"date INTEGER, " +
				"period INTEGER"
			);

			// Move data from date to startDay (Events, Notes, Messages)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startDay = strftime('%Y%m%d', date/1000, 'unixepoch', 'localtime') " +
					"WHERE type = 'NOTE' OR type = 'MESSAGE' OR type = 'EVENT'",
				'rollback' : _SQL_NO_OP
			});

			// Convert startTime and endTime to new time format (Events)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startTime = strftime('%H%M', startTime/1000, 'unixepoch', 'localtime'), " +
					"endTime = strftime('%H%M', endTime/1000, 'unixepoch', 'localtime') " +
					"WHERE type = 'EVENT'",
				'rollback' : _SQL_NO_OP
			});

			// Move data from period to startTime (Events)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startTime = (SELECT strftime('%H%M', startTime/1000, 'unixepoch', 'localtime') FROM period WHERE id = period) " +
					"WHERE type = 'EVENT' AND startTime IS NULL AND period IS NOT NULL",
				'rollback' : _SQL_NO_OP
			});

			// Move data from period to endTime (Events)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET endTime = (SELECT strftime('%H%M', endTime/1000, 'unixepoch', 'localtime') FROM period WHERE id = period) " +
					"WHERE type = 'EVENT' AND endTime IS NULL AND period IS NOT NULL",
				'rollback' : _SQL_NO_OP
			});

			// Convert startTime and endTime to new time format (Notes, Messages)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startTime = strftime('%H%M', startTime/1000, 'unixepoch', 'localtime'), " +
					"endTime = NULL " +
					"WHERE type = 'NOTE' OR type = 'MESSAGE'",
				'rollback' : _SQL_NO_OP
			});

			// Move data from period to startTime (Notes, Messages)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET startTime = (SELECT strftime('%H%M', startTime/1000, 'unixepoch', 'localtime') FROM period WHERE id = period) " +
					"WHERE (type = 'NOTE' OR type = 'MESSAGE') AND startTime IS NULL AND period IS NOT NULL",
				'rollback' : _SQL_NO_OP
			});

			// Move data from created to assignedDay and assignedTime (Tasks)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET assignedDay = strftime('%Y%m%d', created/1000, 'unixepoch', 'localtime'), " +
					"assignedTime = strftime('%H%S', created/1000, 'unixepoch', 'localtime') " +
					"WHERE type = 'HOMEWORK' OR type = 'ASSIGNMENT'",
				'rollback' : _SQL_NO_OP
			});

			// Move data from date to dueDay and startTime to dueTime (Tasks)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET dueDay = strftime('%Y%m%d', date/1000, 'unixepoch', 'localtime'), " +
					"dueTime = strftime('%H%M', startTime/1000, 'unixepoch', 'localtime'), " +
					"startTime = NULL, " +
					"endTime = NULL " +
					"WHERE type = 'HOMEWORK' OR type = 'ASSIGNMENT'",
				'rollback' : _SQL_NO_OP
			});

			// Move data from period to dueTime (Tasks)
			queries.push({
				'sql' : "UPDATE diaryItem " +
					"SET dueTime = (SELECT strftime('%H%M', startTime/1000, 'unixepoch', 'localtime') FROM period WHERE id = period) " +
					"WHERE (type = 'HOMEWORK' OR type = 'ASSIGNMENT') AND dueTime IS NULL AND period IS NOT NULL",
				'rollback' : _SQL_NO_OP
			});

			// Drop diaryItem.date column
			this._pushQueriesToDropColumn(queries, "diaryItem",
				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"dueDay INTEGER, " +
				"dueTime INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startDay INTEGER, " +
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER, " +

				// TO DELETE
				"period INTEGER",

				"id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFrom, messageFromText, responseRequired, period",
				"date INTEGER"
			);

			// Drop diaryItem.period column
			this._pushQueriesToDropColumn(queries, "diaryItem",
				// Diary Item fields
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
				"type TEXT, " +
				"title TEXT, " +
				"description TEXT, " +
				"created INTEGER, " +

				// Task fields
				"assignedDay INTEGER, " +
				"assignedTime INTEGER, " +
				"dueDay INTEGER, " +
				"dueTime INTEGER, " +
				"completed INTEGER, " +
				"subjectId INTEGER, " +

				// Event fields
				"startDay INTEGER, " +
				"startTime INTEGER, " +
				"endTime INTEGER, " +
				"locked INTEGER, " +

				// Note fields
				// atDay = startDay
				// atTime = startTime

				// Message fields
				// atDay = startDay
				// atTime = startTime
				"messageFrom TEXT, " +
				"messageFromText TEXT, " +
				"responseRequired INTEGER",

				"id, type, title, description, created, assignedDay, assignedTime, dueDay, dueTime, completed, subjectId, startDay, startTime, endTime, locked, messageFrom, messageFromText, responseRequired",
				"period INTEGER"
			);

			// Return the queries
			return {
				'queries' : queries
			};
		},

		// Version 10 - add 'theme' to school profile
		function () {
			var queries = this._pushQueriesToAddColumn([], "schoolProfile",
				"theme TEXT(100) NULL",
				"id, name, schoolLogo, schoolImage, schoolMotto, missionStatement, diaryTitle, includeEmpower",
				" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
					" name TEXT(100) NULL, " +
					" schoolLogo TEXT(255) NULL, " +
					" schoolImage TEXT(255) NULL, " +
					" schoolMotto TEXT NULL, " +
					" missionStatement TEXT NULL, " +
					" diaryTitle TEXT(100) NULL, " +
					" includeEmpower INTEGER(1) NOT NULL DEFAULT 0 ");
			return { 'queries' : queries };
		},

        // Version 11 - [PD-689]
        function () {
            var queries = [];

            // update the period start and end times to a format compatible with Rocketboots.date.Time
            queries.push({
                'sql' : "UPDATE period SET " +
                        "startTime = strftime('%H%M', startTime/1000, 'unixepoch', 'localtime'), " +
                        "endTime = strftime('%H%M', endTime/1000, 'unixepoch', 'localtime') ",
                'rollback' : _SQL_NO_OP
            });

            // update the dayOfNote date to a format compatible with Rocketboots.date.Day
            queries.push({
                'sql' : "UPDATE dayOfNote SET " +
                        "date = strftime('%Y%m%d', date/1000, 'unixepoch', 'localtime')",
                'rollback' : _SQL_NO_OP
            });

            // update the student date of birth to a format compatible with Rocketboots.date.Day
         /*  queries.push({
                'sql' : "UPDATE studentProfile SET " +
                    "dateOfBirth = strftime('%Y%m%d', dateOfBirth/1000, 'unixepoch', 'localtime')",
                'rollback' : _SQL_NO_OP
            });
*/
            // update the timetable start time to a format compatible with Rocketboots.date.Day
            queries.push({
            	'sql' : "UPDATE timetable SET " +
            	"startDate = strftime('%Y%m%d', startDate/1000, 'unixepoch', 'localtime')",
            	'rollback' : _SQL_NO_OP
            });
            
            // Return the queries
            return {
                'queries' : queries
            };
        },
        	// Version 12 - [SBC-2.0.0]
            function()
            {
            var queries=[];
            queries.push({ 'sql' : "ALTER TABLE attachment ADD " +
                    "userId INTEGER" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE attachment ADD " +
                    "isSync INTEGER " ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE class ADD " +
                    "Name TEXT " ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE class ADD " +
                    "Code TEXT" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE class ADD " +
                    "UniqueID INTEGER" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE class ADD " +
                    "RoomId INTEGER" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE class ADD " +
                    "isSync INTEGER" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE class ADD " +
                    "lastupdated INTEGER" ,
                    'rollback' : "" });
           
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "isDelete INTEGER" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "Progress INTEGER" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "endDay DATE",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "SchoolId INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "ClassId INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "UniqueID INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "EstimatedHours TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "EstimatedMins TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "AssingedBy  INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "lastupdated INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "isSync INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "priority TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "UserID INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "createdBy INTEGER",
                    'rollback' : "" });
            
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "appType TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "assignedtoMe INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "weight TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "hideFromDue INTEGER",
                    'rollback' : "" });
           	queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "hideFromAssigned INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "hideFromNotifications INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "onTheDay INTEGER DEFAULT 0",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "webLink TEXT",
                    'rollback' : "" });
            
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "Empower_Type TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "Allow_Parent INTEGER DEFAULT 0",
                    'rollback' : "" });
                    
            queries.push({ 'sql' : "ALTER TABLE period ADD " +
                    "forTeacher INTEGER DEFAULT 0",
                    'rollback' : "" });
                    
            queries.push({ 'sql' : "ALTER TABLE attachment ADD " +
                    "UniqueID INTEGER" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE attachment ADD " +
                    "serverPath TEXT" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE attachment ADD " +
                    "isDelete INTEGER DEFAULT 0" ,
                    'rollback' : "" });
                    
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "appInstallStatus INTEGER DEFAULT 0",
                    'rollback' : "" });
            
            queries.push({ 'sql' : "ALTER TABLE subject ADD " +
                    "classId INTEGER DEFAULT 0",
                    'rollback' : "" });
                    
            queries.push({ 'sql' : "ALTER TABLE noteEntry ADD " +
                    "UniqueID INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE noteEntry ADD " +
                    "lastupdated INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE noteEntry ADD " +
                    "isDelete INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE noteEntry ADD " +
                    "isSynch INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE noteEntry ADD " +
                    "noteUniqueId INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE noteEntry ADD " +
                    "userId INTEGER",
                    'rollback' : "" });
					
					
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "AllowAutoUpdates INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "RegionalFormat TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "UniqueID INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "lastupdated INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "Address_line_1 TEXT(200)",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "Address_line_2 TEXT(200)",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "CityTownSuburb TEXT(100)",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "PostalCode TEXT(10)",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "State TEXT(100)",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "Country TEXT(100)",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "Number_of_Campuses INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "Phone_Number TEXT(30)",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "Fax_Number TEXT(30)",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "Website_URL TEXT(200)",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "AllowSynch INTEGER(1)",
                    'rollback' : "" });
            
            queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "schoolBgImage TEXT(255)",
                    'rollback' : "" });        
					
            queries.push({ 'sql' : "ALTER TABLE usageLog ADD " +
                    "createdDate TEXT",
                    'rollback' : "" });
            
           
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "SchoolId INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "Country TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "LastName TEXT",
                    'rollback' : "" });	
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "IsStudent INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "IsTeacher INTEGER",
                    'rollback' : "" });	   
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "IsParent INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "ProfileImage TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "UniqueID INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "lastupdated INTEGER",
                    'rollback' : "" });    
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "Password TEXT",
                    'rollback' : "" }); 
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "image BLOB",
                    'rollback' : "" }); 
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "isSync INTEGER",
                    'rollback' : "" }); 
                    
            queries.push({ 'sql' : "ALTER TABLE studentProfile ADD " +
                    "TimeZone TEXT",
                    'rollback' : "" });         
					
            queries.push({ 'sql' : "ALTER TABLE timetable ADD " +
                    "TimeTableCycleId INTEGER",
                    'rollback' : "" }); 
            queries.push({ 'sql' : "ALTER TABLE timetable ADD " +
                    "EndDate INTEGER",
                    'rollback' : "" }); 
            queries.push({ 'sql' : "ALTER TABLE timetable ADD " +
                    "UniqueID INTEGER",
                    'rollback' : "" }); 
            queries.push({ 'sql' : "ALTER TABLE timetable ADD " +
                    "lastupdated INTEGER",
                    'rollback' : "" });
					
            queries.push({ 'sql' : "ALTER TABLE subject ADD " +
                    "SchoolId INTEGER",
                    'rollback' : "" }); 
            queries.push({ 'sql' : "ALTER TABLE subject ADD " +
                    "CampusId INTEGER",
                    'rollback' : "" });  
            queries.push({ 'sql' : "ALTER TABLE subject ADD " +
                    "UniqueID INTEGER",
                    'rollback' : "" }); 
            queries.push({ 'sql' : "ALTER TABLE subject ADD " +
                    "lastupdated INTEGER",
                    'rollback' : "" }); 
            queries.push({ 'sql' : "ALTER TABLE period ADD " +
                    "UniqueID INTEGER",
                    'rollback' : "" }); 
            queries.push({ 'sql' : "ALTER TABLE period ADD " +
                    "lastupdated INTEGER",
                    'rollback' : "" }); 		
					
					 
            
							
            queries.push({ 'sql' : "CREATE TABLE ClassUser (" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "classId INTEGER, " +
					"userId INTEGER, "+
					"cycleDay INTEGER, "+
					"code TEXT, "+
					"UniqueID INTEGER, "+
					"lastupdated INTEGER, "+
					"isSync INTEGER, "+
					"isDelete INTEGER, "+
					"createdBy INTEGER, " +
					"classUniqueId INTEGER " +
                    ")",
                    'rollback' : "DROP TABLE LastUpdates" });
					
					
            queries.push({ 'sql' : "CREATE TABLE Country (" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "UniqueID INTEGER NOT NULL UNIQUE, " +
                    "name TEXT " +
                    ")",
                    'rollback' : "DROP TABLE Country" });
					
            queries.push({ 'sql' : "CREATE TABLE Room (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "SchoolId INTEGER, " +
                    "CampusId INTEGER, " +
                    "Name TEXT(100), " +
                    "Code TEXT(100), " +
                    "UniqueID INTEGER, " +
                    "lastupdated INTEGER "+
                    ")",
                    'rollback' : "DROP TABLE Room" });
					
	        queries.push({ 'sql' : "CREATE TABLE State (" +
	               "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
	               "UniqueID  INTEGER NOT NULL UNIQUE, " +
	               "CountryId INTEGER, " +
	               "Name TEXT " +
	               ")",
	               'rollback' : "DROP TABLE State" });
				   
           queries.push({ 'sql' : "CREATE TABLE TimeTableCycles (" +
                   " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                   "SchoolId INTEGER, " +
                   "CampusId INTEGER, " +
                   "CycleDays INTEGER, " +
                   "CycleLength INTEGER, " +
                   "CycleLabel TEXT, " +
                   "UniqueID INTEGER, "+
                   "lastupdated INTEGER "+
                   ")",
                   'rollback' : "DROP TABLE TimeTableCycles" });
				  
           queries.push({ 'sql' : "CREATE TABLE contentStorage (" +
		   					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   					" ContentType TEXT(20)  , " +
		   					" Title TEXT(200) NOT NULL, " +
		   					" Content TEXT NOT NULL, " +
		   					" uniqueID INTEGER NOT NULL , " +
		   					" respectedPanel TEXT  , " +
		   					" StartDate TEXT  , " +
		   					" EndDate TEXT   " +					
		   					")",
		   					'rollback' : "DROP TABLE contentStorage" });
				   
           queries.push({
   				'sql' : "ALTER TABLE studentProfile RENAME TO User",
   				'rollback' : _SQL_NO_OP
   			});
			
            
								
            queries.push({ 'sql' : "CREATE TABLE lastSynch (" +
           			"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
                    "tableName TEXT, " +
                    "lastSynch INTEGER, " +
                    "appType TEXT, "+
					"contentsync TEXT"+
                    ")",
                    'rollback' : "DROP TABLE lastSynch" });
					
	        queries.push({ 'sql' : "CREATE TABLE UsageLogSync (" +
					"id INTEGER, "+
	                " dateLastSynced TEXT " +
	                ")",
	                'rollback' : "DROP TABLE UsageLogSync" });
				
	        queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                "tableName, lastSynch, appType) VALUES (" +
	                "'ClassUser',null, 'desktop' " +
	                ")",
	                'rollback' : "" });
				
	       queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                " tableName,lastSynch,appType ) VALUES (" +
	                "'diaryItem' ,null, 'desktop'" +
	                ")",
	                'rollback' : "" });
				
	       queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                " tableName,lastSynch,appType) VALUES (" +
	                "'noteEntry' ,null, 'desktop'" +
	                ")",
	                'rollback' : "" });
				
	        queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                   "tableName, lastSynch, appType ,contentsync) VALUES (" +
	                   "'contentsyncData',null, 'desktop', null" +
	                   ")",
	                   'rollback' : "" });
				
		   	 queries.push({ 'sql' : "INSERT INTO lastSynch (" +
		                       "tableName, lastSynch, appType ,contentsync) VALUES (" +
		                       "'imagesyncing',null, 'desktop', null" +
		                       ")",
		                       'rollback' : "" });
		                       
		      queries.push({ 'sql' : "INSERT INTO lastSynch (" +
		                       "tableName, lastSynch, appType ,contentsync) VALUES (" +
		                       "'inboxItem',null, 'desktop', null" +
		                       ")",
		                       'rollback' : "" });
		                       
		      queries.push({ 'sql' : "INSERT INTO lastSynch (" +
		                       "tableName, lastSynch, appType ,contentsync) VALUES (" +
		                       "'periodSyncing',null, 'desktop', null" +
		                       ")",
		                       'rollback' : "" });	
							   
							   
			  queries.push({ 'sql' : "INSERT INTO lastSynch (" +
		                       "tableName, lastSynch, appType ,contentsync) VALUES (" +
		                       "'timetableSyncing',null, 'desktop', null" +
		                       ")",
		                       'rollback' : "" });
			  queries.push({ 'sql' : "INSERT INTO lastSynch (" +
                      "tableName, lastSynch, appType ,contentsync) VALUES (" +
                      "'getClassUserSyncing',null, 'desktop', null" +
                      ")",
                      'rollback' : "" });
	           queries.push({ 'sql' : "ALTER TABLE dayOfNote ADD " +
	                   "UniqueID INTEGER",
	                   'rollback' : "" });
					   
	           queries.push({ 'sql' : "ALTER TABLE dayOfNote ADD " +
	                   "SchoolId INTEGER",
	                   'rollback' : "" });
	           queries.push({ 'sql' : "ALTER TABLE dayOfNote ADD " +
	                   "CampusId INTEGER",
	                   'rollback' : "" });
	           queries.push({ 'sql' : "ALTER TABLE dayOfNote ADD " +
	                   "lastupdated INTEGER",
	                   'rollback' : "" });
	         queries.push({ 'sql' : "INSERT INTO lastSynch (" +
	                " tableName,lastSynch,appType) VALUES (" +
	                "'dayOfNote' ,null, 'desktop'" +
	                ")",
	                'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE systemConfig ADD " +
               "MacId TEXT",
               'rollback' : "" });
            
            queries.push({ 'sql' : "CREATE TABLE Announcements (" +
		   					" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   					" SchoolId INTEGER NOT NULL , " +
		   					" All_Students INTEGER, " +
		   					" All_Parents INTEGER, " +
		   					" All_Teachers INTEGER, " +
		   					" Details TEXT  NOT NULL, " +
		   					" CampusIds TEXT, " +
		   					" Isdeleted INTEGER NOT NULL DEFAULT 0,   " +		
		   					" UniqueID INTEGER," +			
		   					" Created TEXT," +
		   					" CreatedBy TEXT,   " +
		   					" Updated TEXT,   " +
		   					" UpdatedBy TEXT,  " +
		   					" readStatus INTEGER DEFAULT 0" +
		   					")",
		   					'rollback' : "DROP TABLE Announcements" });
	          
	          queries.push({ 'sql' : "CREATE TABLE News (" +
		   					" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   					" SchoolId INTEGER NOT NULL , " +
							" All_Administrators INTEGER, " +
		   					" All_Students INTEGER, " +
		   					" All_Parents INTEGER, " +
		   					" All_Teachers INTEGER, " +
		   					" Title TEXT NOT NULL, " +
		   					" Body TEXT NOT NULL, " +
		   					" Tags TEXT , " +
		   					" CampusIds TEXT, " +
		   					" Isdeleted INTEGER NOT NULL DEFAULT 0, " +		
		   					" UniqueID INTEGER," +			
		   					" Created TEXT," +
		   					" CreatedBy TEXT,   " +
		   					" Updated TEXT,   " +
		   					" UpdatedBy TEXT,  " +
		   					" Status INTEGER NOT NULL, " +
		   					" readStatus INTEGER DEFAULT 0" +
		   					")",
		   					'rollback' : "DROP TABLE News" });  
		   		
		   		queries.push({ 'sql' : "CREATE TABLE News_Attachments (" +
		   					" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   					" NewsId INTEGER NOT NULL , " +
							" Attachments BLOB, " +
		   					" Created TEXT," +
		   					" CreatedBy INTEGER,   " +
		   					" Updated TEXT,   " +
		   					" UpdatedBy INTEGER  " +
		   					")",
		   					'rollback' : "DROP TABLE News_Attachments" });   
		   					
		   		queries.push({ 'sql' : "CREATE TABLE News_Images (" +
		   					" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   					" NewsId INTEGER NOT NULL , " +
							" Image BLOB, " +
		   					" Created INTEGER(20)," +
		   					" CreatedBy INTEGER,   " +
		   					" Updated INTEGER(20),   " +
		   					" UpdatedBy INTEGER  " +
		   					")",
		   					'rollback' : "DROP TABLE News_Images" });
		   					
		   		queries.push({ 'sql' : "CREATE TABLE emailtemplates (" +
		   					" id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   					" from_name TEXT NOT NULL , " +
							" from_email TEXT NOT NULL, " +
		   					" reply_to TEXT NOT NULL," +
		   					" email_title TEXT NOT NULL,   " +
		   					" subject TEXT NOT NULL,   " +
		   					" message TEXT NOT NULL, " +
		   					" status INTEGER NOT NULL DEFAULT 1 " +
		   					")",
		   					'rollback' : "DROP TABLE emailtemplates" });  
		   					//change : reciverId to varchar
		   		queries.push({ 'sql' : "CREATE TABLE Messages (" +
		   					" Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   					" ParentId INTEGER DEFAULT 0, " +
		   					" UserId INTEGER NOT NULL, " +
		   					" ReceiverId TEXT, " +
		   					" UserName TEXT, " +
		   					" ReceiverName TEXT, " +
		   					" Subject TEXT, " +
		   					" Description TEXT, " +
		   					" AppId INTEGER, " +
		   					" AppType TEXT, " +	
		   					" IpAddress TEXT, " +
		   					" IsSenderRead INTEGER , " +
		   					" IsReceiverRead INTEGER , " +
		   					" SentDate INTEGER , " +
		   					" DeletedStatus INTEGER NOT NULL DEFAULT 0, " +	
		   					" UniqueID INTEGER," +	
		   					" isSync INTEGER DEFAULT 1," +			
		   					" Created TEXT," +
		   					" CreatedBy TEXT,   " +
		   					" Updated TEXT,   " +
		   					" UpdatedBy TEXT," +
		   					" ParentEmails TEXT  " +
		   					")",
		   					'rollback' : "DROP TABLE Messages" });  
		   	
		   		queries.push({ 'sql' : "CREATE TABLE myTeacher (" +
	   					" profile INTEGER, " +
	   					" email VARCHAR, " +
	   					" image_url VARCHAR, " +
	   					" name VARCHAR, " +
	   					" phone VARCHAR, " +
	   					" salutation VARCHAR, " +
	   					" subjectName VARCHAR, " +
	   					" teacher_id VARCHAR, " +
	   					" status INTEGER, " +
	   					" image BLOB, " +
	   					" PRIMARY KEY (teacher_id) "+
	   					")",
	   					'rollback' : "DROP TABLE myTeacher" });  

		   	
		   		
		   		queries.push({ 'sql' : "CREATE TABLE myParent ( " +
		   				"		parent_id INTEGER, " +
		   				"		profile INTEGER, " +
		   				"		email VARCHAR, " +
		   				"		image_url VARCHAR, " +
		   				"		name VARCHAR, " +
		   				"		phone VARCHAR, " +
		   				" 		status INTEGER, " +
		   				"		image BLOB," +
		   				"		studentId INTEGER," +
		   				"		PRIMARY KEY (parent_id,studentId) "+
		   				")",
		   				'rollback' : "DROP TABLE myParent" });  
		   		
		   		queries.push({ 'sql' : "CREATE TABLE myProfile (" +
		   				"		id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   				"		isSync INTEGER, " +
		   				"		school_id INTEGER, " +
		   				"		birthdate TIMESTAMP, " +
		   				"		address VARCHAR, " +
		   				"		country VARCHAR, " +
		   				"		email VARCHAR, " +
		   				"		image_url VARCHAR, " +
		   				"		lastName VARCHAR, " +
		   				"		name VARCHAR, " +
		   				"		phone VARCHAR, " +
		   				"		profile_id VARCHAR, " +
		   				"		state VARCHAR, " +
		   				"		street VARCHAR, " +
		   				"		type VARCHAR, " +
		   				"		year VARCHAR, " +
		   				"		zipCode VARCHAR, " +
		   				"		image BLOB "+
		   				")",
		   				'rollback' : "DROP TABLE myProfile" }); 
		   		
		   		
		   		queries.push({ 'sql' : "CREATE TABLE diaryItemUser (" +
		   				"		id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   				"		item_id INTEGER, " +
		   				"		unique_id INTEGER NOT NULL UNIQUE, " +
		   				"		reminder VARCHAR, " +
		   				"		updateDate INTEGER(20), " +
		   				"		progress VARCHAR, " +
		   				"		student_id VARCHAR, " +
		   				"		user_id VARCHAR" +
		   				")",
		   				'rollback' : "DROP TABLE diaryItemUser" }); 
		   		
		   		queries.push({ 'sql' : "CREATE TABLE student (" +
		   				
		   				"		class_id INTEGER NOT NULL, " +
		   				"		school_id INTEGER, " +
		   				"		my_class INTEGER, " +
		   				"		student2 INTEGER, " +
		   				"		updateTime TIMESTAMP, " +
		   				"		email_id VARCHAR, " +
		   				"		firstName VARCHAR, " +
		   				"		image_url VARCHAR, " +
		   				"		lastName VARCHAR, " +
		   				"		phone_no VARCHAR, " +
		   				"		student_id VARCHAR NOT NULL, " +
		   				"		student_name VARCHAR, " +
		   				"		teacher_id VARCHAR, " +
		   				"		status INTEGER DEFAULT 1, " +
		   				"		image BLOB," +
		   				"		DeletedStatus INTEGER DEFAULT 0," +
		   				"		PRIMARY KEY (student_id, class_id) "+
		   				")",
		   				'rollback' : "DROP TABLE student" }); 
		   				
		   		queries.push({ 'sql' : "CREATE TABLE DiaryItemTypes (" +
		   				"		id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   				"		Name VARCHAR, " +
		   				"		FormTemplate VARCHAR, " +
		   				"		SchoolId INTEGER, " +
		   				"		Active INTEGER, " +
		   				" 		UniqueID INTEGER," +	
		   				"		Icon VARCHAR " +
		   				")",
		   				'rollback' : "DROP TABLE DiaryItemTypes" });
		   		
		   		queries.push({ 'sql' : "CREATE TABLE grade (" +
	                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
	                    "UniqueID INTEGER NOT NULL UNIQUE, " +
	                    "name TEXT " +
	                    ")",
	                    'rollback' : "DROP TABLE grade" });
	                    
	           queries.push({ 'sql' : "CREATE TABLE messageUser (" +
	                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
	                    "UniqueID INTEGER NOT NULL UNIQUE, " +
	                    "messageId INTEGER," +
	                    "receiverId INTEGER" +
	                    ")",
	                    'rollback' : "DROP TABLE meassageUser" });
	           
	           queries.push({ 'sql' : "CREATE TABLE eventUser (" +
	                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
	                    "uniqueId INTEGER NOT NULL UNIQUE, " +
	                    "eventId INTEGER," +
	                    "userId INTEGER, " +
	                    "isDeleted INTEGER" +
	                    ")",
	                    'rollback' : "DROP TABLE eventUser" });


             return {
                'queries' : queries
             };
		},
		
		// Version 13(2.0.2)
		function () {
			var queries = [];

			queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "allStudents INTEGER DEFAULT 1",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "allTeachers INTEGER DEFAULT 1",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "type_id INTEGER",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "type_name TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE attachment ADD " +
                    "fileSize INTEGER DEFAULT 0" ,
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE contentStorage ADD " +
                    "allStudents INTEGER DEFAULT 1",
                    'rollback' : "" });  
            queries.push({ 'sql' : "ALTER TABLE contentStorage ADD " +
                    "allTeachers INTEGER DEFAULT 1",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE myTeacher ADD " +
                    "isDeleted INTEGER DEFAULT 0",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE myParent ADD " +
                    "isDeleted INTEGER DEFAULT 0",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItemUser ADD " +
                    "isDeleted INTEGER DEFAULT 0",
                    'rollback' : "" });
            
//            queries.push({ 'sql' : "ALTER TABLE myParent " +
//                    " ADD CONSTRAINT PRIMARY KEY (parent_id,studentId) ",
//                    'rollback' : "" });
            
            //Migration script for Diary item types change.
            queries.push({ 'sql' : "UPDATE diaryItem SET "+
            		" type_id = (Select dit.UniqueID from DiaryItemTypes dit where UPPER(dit.Name) = UPPER(type) LIMIT 1),"+
            		" type_name = (Select dit.name from DiaryItemTypes dit where UPPER(dit.Name) = UPPER(type) LIMIT 1) "+
            		" where type_name IS NULL",
                    'rollback' : _SQL_NO_OP });
            
            queries.push({ 'sql' : "UPDATE Announcements SET All_Students = 1, All_Teachers = 1, Updated = Created where 1",
                    'rollback' : _SQL_NO_OP });
           
            queries.push({ 'sql' : "UPDATE News SET All_Students = 1, All_Teachers = 1, Updated = Created where 1",
                    'rollback' : _SQL_NO_OP });
            
            queries.push({ 'sql' : "UPDATE eventUser SET "+
            		" isDeleted = 0 "+
            		" where isDeleted IS NULL",
                    'rollback' : _SQL_NO_OP });
            
            
            
            // Return the queries
            return {
                'queries' : queries
            };
		},
		
		//Version 14
		function () {
			var queries = [];

			queries.push({ 'sql' : "ALTER TABLE schoolProfile ADD " +
                    "domainName TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE User ADD " +
                    "EncrptedUserId TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "ALTER TABLE diaryItem ADD " +
                    "creatorUserName TEXT",
                    'rollback' : "" });
            queries.push({ 'sql' : "CREATE TABLE Report (" +
            		 	"		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
		   				"		UniqueID INTEGER, " +
		   				"		isStudent INTEGER, " +
		   				"		isTeacher INTEGER, " +
		   				"		isReportActive INTEGER, " +
		   				"		isSchoolActive INTEGER, " +
		   				"		ReportDescription VARCHAR, " +
		   				"		ReportName VARCHAR, " +
		   				"		ReportURL VARCHAR " +
		   				")",
		   				'rollback' : "DROP TABLE report" });
            
            
            // Return the queries
            return {
                'queries' : queries
            };
		},

							
    ];
	//****************************************************/

	/**
	 * A helper function to modify an array of query objects by appending the query objects necessary
	 * to add a column to a table.
	 *
	 * The reason this is non-trivial is because SQLite does not support
	 * removing a column, which is required for a rollback operation.
	 *
	 * @param queryArray The array of query objects which is to be appended to.
	 * @param tableName The name of the table to have a column added, e.g. "timetable"
	 * @param newColumnDefinition The column definition for the new column, e.g. "name TEXT(100) NOT NULL DEFAULT 'hello world'"
	 * @param rollbackColumnNames The comma-separated column names for the unmodified table, e.g. "id, type, value"
	 * @param rollbackColumnDefinitions The comma-separated column definitions for the unmodified table, e.g. "id INTEGER, type TEXT, value INTEGER NULL"
	 * @return {Array} The modified query array (as passed in). Note that the original will also be modified.
	 * @private
	 */
	_service.prototype._pushQueriesToAddColumn = function (queryArray, tableName, newColumnDefinition, rollbackColumnNames, rollbackColumnDefinitions) {

		var tableCopyName = tableName + "Copy";

		queryArray.push({
			'sql' : _SQL_NO_OP,
			// Re-name the new table. Note: rollbacks should be read in reverse order.
			'rollback' : "ALTER TABLE " + tableCopyName + " RENAME TO " + tableName
		});
		queryArray.push({
			'sql' : _SQL_NO_OP,
			// Delete the table (the one with the new column)
			'rollback' : "DROP TABLE " + tableName
		});
		queryArray.push({
			'sql' : _SQL_NO_OP,
			// Copy across all the data
			'rollback' : "INSERT INTO " + tableCopyName + " (" + rollbackColumnNames + ") SELECT " + rollbackColumnNames + " FROM " + tableName
		});
		queryArray.push({
			'sql' : "ALTER TABLE " + tableName + " ADD COLUMN " + newColumnDefinition,
			// Create a table in the old format (without the new column)
			'rollback' : "CREATE TABLE " + tableCopyName + " (" + rollbackColumnDefinitions + ")"
		});

		return queryArray;
	};

	/**
	 * A helper function to modify an array of query objects by appending the query objects necessary
	 * to remove a column to a table.
	 *
	 * The reason this is non-trivial is because SQLite does not support removing a column.
	 * 
	 * Note also that rollback is destructive; while the column will be readded to the table, the data will have been deleted.
	 *
	 * @param queryArray The array of query objects which is to be appended to.
	 * @param tableName The name of the table to have a column removed, e.g. "timetable"
	 * @param newColumnDefinitions The comma-separated column definitions for the modified table, e.g. "id INTEGER, type TEXT, value INTEGER NULL"
	 * @param newColumnNames The comma-separated column names for the modified table, e.g. "id, type, value"
	 * @param oldColumnDefinition The column definition for the dropped column, e.g. "name TEXT(100) NOT NULL DEFAULT 'hello world'"
	 * @return {Array} The modified query array (as passed in). Note that the original will also be modified.
	 * @private
	 */
	_service.prototype._pushQueriesToDropColumn = function (queryArray, tableName, newColumnDefinitions, newColumnNames, oldColumnDefinition) {

		var tableCopyName = tableName + "Copy";
		
		queryArray.push({
			// Create a table with with the new column definitions.
			'sql' : "CREATE TABLE " + tableCopyName + " (" + newColumnDefinitions + ")",
			'rollback' : "ALTER TABLE " + tableName + " ADD COLUMN " + oldColumnDefinition
		});
		queryArray.push({
			// Copy across all the data from the old table to the new created one.
			'sql' : "INSERT INTO " + tableCopyName + " (" + newColumnNames + ") SELECT " + newColumnNames + " FROM " + tableName,
			'rollback' : _SQL_NO_OP
		});
		queryArray.push({
			// Delete the old table (the one with the old column).
			'sql' : "DROP TABLE " + tableName,
			'rollback' : _SQL_NO_OP
		});
		queryArray.push({
			// Re-name the new table to replace the old table.
			'sql' : "ALTER TABLE " + tableCopyName + " RENAME TO " + tableName,
			'rollback' : _SQL_NO_OP
		});

		return queryArray;
	};

	/**
	 * @private
	 * Retrieve the schema version.
	 * Has the side affect of creating the 'info' table (used to store the schema version) if
	 * the database is unversioned.
	 *
	 * @return Schema version, or zero if the database is unversioned.
	 */
	_service.prototype._getSchemaVersion = function () {

		// holds the version that is currently installed in the local store
		var version = null;
		try {

			// get the last installed version
			var result = this._database.runQuery("SELECT schemaVersion FROM info").getResult().data;

			if (result != null) {
				version = result[0].schemaVersion;
			}

		} catch (error) {

			// if an error occurs we will assume that no schema is yet installed so we will
			// try to install version 1
			//console.log("DiarySchemaManager._getSchemaVersion() - Unable to read database schema version, assuming no schema exists");
		}

		// No schema installed, so create info table to store schema version - starting at version 0
		if (version === null) {
			var queries = [];

			// Manage schema version
			queries.push({ 'sql' : "CREATE TABLE info (" +
				" schemaVersion INT NOT NULL " +
				")",
				'rollback' : "DROP TABLE info" });

			queries.push({ 'sql' : "INSERT INTO info (schemaVersion) VALUES (0)",
				'rollback' : "DELETE FROM info" });

			this._database.runScripts(queries);

			version = 0;
		}

		return version;
	};

	/**
	 * Check if the current version of the schema can be upgraded.
	 * @return {Boolean} True if the schema version is supported, false otherwise.
	 */
	_service.prototype.isSchemaVersionSupported = function () {
		var schemaVersion = this._getSchemaVersion();
		return schemaVersion == 0 || schemaVersion >= _MINIMUM_SUPPORTED_VERSION;
	};

	/**
	 * Will update the schema to the latest version of the application.  If more than one version
	 * upgrade is required, the schema updates will be executed in sequence.
	 */
	_service.prototype.installSchemaUpdates = function () {

		var initialVersion = this._getSchemaVersion();
		var currentVersion;

		if (!this.isSchemaVersionSupported()) {
			throw new Error("Your database version " + initialVersion + " is unsupported. Cannot upgrade database.");
		}

		for (currentVersion = initialVersion; currentVersion - _MINIMUM_SUPPORTED_VERSION + 1 < _schema_versions.length; ++currentVersion) {
			// Start at the version just before the minimum
			if (currentVersion < 1) {
				currentVersion = _MINIMUM_SUPPORTED_VERSION - 1;
			}
			// Guard for empty version list
			if (_schema_versions.length == 0) {
				throw new Error("No schema to install - the schema has not been defined.");
			}

			// Perform the update
			{
				var nextVersionObject = _schema_versions[currentVersion - _MINIMUM_SUPPORTED_VERSION + 1];

				// Handle version objects specified as functions
				if (_.isFunction(nextVersionObject)) {
					nextVersionObject = nextVersionObject.call(this);
				}

				// Copy the queries array, so it can be appended to
				var updateSchemaQueries = (nextVersionObject && nextVersionObject.queries ? nextVersionObject.queries.slice(0) : null);
				if (!updateSchemaQueries || !updateSchemaQueries.length) {
					throw new Error("Poorly defined schema update " + (1 + currentVersion));
				}

				// Update the schema version in the DB
				updateSchemaQueries.push({ 'sql' : "UPDATE info SET schemaVersion = " + (1 + currentVersion),
					'rollback' : "UPDATE info SET schemaVersion = " + (currentVersion == (_MINIMUM_SUPPORTED_VERSION - 1) ? 0 : currentVersion) });
					//console.log(updateSchemaQueries);
				this._database.runScripts(updateSchemaQueries);
			}
		}

		//console.log("DiarySchemaManager.installSchemaUpdates() - completed at schema version " + currentVersion + " (was version " + initialVersion + ")");

	};

	/**
	 * Drops the database
	 */
	_service.prototype.dropDatabase = function() {
		this._database.dropDatabase();
	};

	// return the created service
	return _service;

})();
