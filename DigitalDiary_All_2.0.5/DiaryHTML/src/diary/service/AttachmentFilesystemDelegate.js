/**
 * The delegate that is to be used to manage attachments,
 * storing them in the local file system.
 * 
 * @author Brian Bason and John Pearce
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.AttachmentFilesystemDelegate = (function() {
	
	/**
	 * Constructs the Delegate.
	 * @constructor
	 */
	var _service = function () {
		//console.log('AttachmentFilesystemDelegate()');
	};
	
	/**
	 * @public
	 * Adds a new attachment into the system, storing its source location,
	 * copying it into the systems folder.
	 * 
	 * NOTE: this does NOT store the attachments meta data in the database, nor does it generate
	 * 		a unique ID for the attachment.
	 * 
	 * The success callback provides the files meta data via a Attachment object.
	 * 
	 * @param {!string} sourcePath The file-system URL of the file to upload.
	 * @param {!function(!diary.model.Attachment)} successCallback The function to be called upon success. 
	 * @param {!function(string)} errorCallback The function to be called upon failure.
	 */
	_service.prototype.upload = function (sourcePath, successCallback, errorCallback) {
	console.log("sourcePath==="+sourcePath);
	//console.log(sourcePath);
		try {
			// get the content folder where all the contents are saved
			// check that it exists
			// NOTE - this is relative to the application content folder
			var localFolder = air.File.applicationStorageDirectory.resolvePath("attachments"),
					sourceFile = new air.File(sourcePath),
					localFileName = sourceFile.name,
					destinationFile;

			if (!localFolder.exists) {
				localFolder.createDirectory();
			}
			
			// check the source file
			if (!sourceFile.exists || sourceFile.isDirectory) {
				errorCallback("Source file does not exist, or is a directory");
				return;
			}
			
			var timstamp = getTimestampmilliseconds();
			// derive the local file name
			for (var i=1; localFolder.resolvePath(localFileName).exists; ++i) {
				localFileName = sourceFile.name.replace(/\.[^\.]+$/, "") + i + (sourceFile.extension ? "." + sourceFile.extension : "");
			}
			
			localFileName = timstamp+localFileName;
			//alert("now localFileName : " + localFileName);
			destinationFile = localFolder.resolvePath(localFileName);
			
			// copy the source file to the new destination
			sourceFile.addEventListener("complete", function () {
				successCallback(new diary.model.Attachment({
					'id' : -1,
					'originalFilePath' : sourcePath,
					'displayName' : sourceFile.name,
					'fileType' : sourceFile.type,
					'localFileName' : localFileName,
					'localFilePath' : destinationFile.nativePath,
					'dateAttached' : new Date(),
					'fileSize' : sourceFile.size
				}));
				
			});
			sourceFile.addEventListener("ioError", function (error) {
				errorCallback("Error copying file into application storage location: " + error);
			});
			sourceFile.copyToAsync(destinationFile);
			
		} catch (error) {
			errorCallback(error);
		}
	};
	
	/**
	 * @public
	 * resolves an attachment's 'localFilePath'
	 * This method is necessary as the local file path is not stored in the database.
	 * 
	 * @param {!string} the path to resolve
	 * @return {string} the resolved path
	 */
	_service.prototype.resolveLocalFilePath = function (filePath) {
		try {
			return air.File.applicationStorageDirectory.resolvePath("attachments").resolvePath(filePath).nativePath;
		} catch (error) {
			//console.log("Error resolving attachment local file path: " + error);
			return null;
		}
	};

	/**
	 * @public
	 * Opens the (copied version internal to the app of the) file using the systems default editor/viewer.
	 *  
	 * @param {!diary.model.Attachment} attachment The attachment to act upon.
	 * @return {boolean} Success
	 */
	_service.prototype.open = function (attachment) {
		try {
			new air.File(attachment.get('localFilePath')).openWithDefaultApplication();
		} catch (error) {
			//alert("Error in opening attachment '" + attachment.get('displayName') + "' with default application.");
			//return false;
			if((attachment.get('fileType') && attachment.get('fileType').toUpperCase() != '.GDOC') 
				     || (attachment.get('displayName') && (attachment.get('displayName').toUpperCase()).indexOf(".GDOC") > 0)){
				downloadAttachmentFile(attachment.get('serverPath'),attachment.get('displayName'),attachment.get('localFilePath'));
			}
			
			var link = attachment.get('serverPath');
			var urlReq = new air.URLRequest(link); 
			air.navigateToURL(urlReq);
		}
		return true;
	};
	
	/**
	 * @public
	 * Delete an attachment by removing it from the file system.
	 * Note this only removes the file which was copied from the file system during the 
	 * upload process, and does not affect the original.
	 * 
	 * @param {!diary.model.Attachment} attachment The attachment to act upon.
	 * @param {?function} successCallback The function to be called upon success. 
	 * @param {?function} errorCallback The function to be called upon failure.
	 */
	_service.prototype.destroy = function (attachment, successCallback, errorCallback) {
		try {
			var file = new air.File(attachment.get('localFilePath'));
			file.addEventListener("complete", successCallback);
			file.addEventListener("ioError", function (error) {
				errorCallback("Failed to delete attachment with IOError: " + error);
			});
			file.deleteFileAsync();
			
		} catch (error) {
			errorCallback("Failed to delete attachment " + attachment.get('displayName'));
		}
	};
	
	return _service;
	
})();
