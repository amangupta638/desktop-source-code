/**
 * The local SQL delegate for Student Profile.  The delegate will use SQL queries to actually
 * get data from a local database.  In future there might be another or replacement of this
 * delegate to get data from a remote service 
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.StudentProfileSQLDelegate = (function() {
	/**
	 * Constructs a Student Profile Delegate.
	 * 
	 * @constructor
	 */
	var _delegate = function() {	
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};
	
	/*
	 * Maps the student profile from the db record set to the model in the application
	 */
	function studentProfileMapper(statement) {
	//alert("studentProfileMapper");
		var result = statement.getResult().data;
		if(result && result.length > 0) {
			//alert("result[0].dateOfBirth="+result[0].dateOfBirth);
			
			
            return new diary.model.StudentProfile({
									'id' : result[0].id,
									'name' : result[0].name,
									//'dateOfBirth' : (result[0].dateOfBirth != null || result[0].dateOfBirth != "" ) ? dateOfBirth) : null,
									'dateOfBirth' : result[0].dateOfBirth,
									'address' : new diary.model.Address({
										'street' : result[0].addressStreet,
										'suburb' : result[0].addressSuburb,
										'state' : result[0].addressState,
										'postcode' : result[0].addressPostcode
									}),
									'telephone' : result[0].telephone,
									'email' : result[0].email,
									'grade' : result[0].grade,
									'teacher' : result[0].teacher,
									'house' : result[0].house,
									'houseCaptain' : result[0].houseCaptain,
									'houseColours' : result[0].houseColours,
									'UniqueID' : result[0].UniqueID,
									'password' : result[0].Password,
									'lastName' :result[0].LastName,
									'isTeacher':result[0].IsTeacher,
									'isStudent':result[0].IsStudent,
									'profileImage' : result[0].ProfileImage,
									'timeZone' : result[0].TimeZone
								});
		}
		return null;
	}
	
	/**
	 * Gets the student profile for the student that owns the diary
	 */
	_delegate.prototype.getStudentProfile = function(successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"SELECT * FROM User ",

				// params
				{},

				// success
				function(statement) {
					successCallback(studentProfileMapper(statement));
				},
				// error
				errorCallback, "Error while trying to read student profile"
		);
	};

	/**
	 * Updates the student profile for the student that owns the diary
	 * 
	 * @param {diary.model.StudentProfile} studentProfile The new student profile that is to be updated
	 * @param {!function()} successCallback
	 * @param {?function(message)} errorCallback [OPTIONAL]
	 */
	_delegate.prototype.updateStudentProfile = function(studentProfile, successCallback, errorCallback) {
		//console.log("update student");
		if (!studentProfile) {
			throw new Error("Student Profile must be defined, profile is not optional");
		}
		
		/*if(studentProfile.get('email')&&studentProfile.get('password'))
		{
			emailExits=true;
		}
		else
		{
			emailExits=false;
		}*/
		
		//console.log("profileImage to save : "+studentProfile.get('profileImage'));
		this._database.runQuery(
				// query
				"UPDATE User SET " +
						"name = :name, " +
						"dateOfBirth = :dateOfBirth, " +
						/*"addressStreet = :addressStreet, " +
						"addressSuburb = :addressSuburb, " +
						"addressState = :addressState, " +
						"addressPostcode = :addressPostcode," +
						"country = :addressCountry, " +*/
						"telephone = :telephone, " +
						"email = :email, " +
						"grade = :grade, " +
						"teacher = :teacher," +
						"schoolId = :schoolId," +
						"ProfileImage = :profileImage, " +
						"house = :house, " +
						"houseCaptain = :houseCaptain, " +
						"Password = :Password, "+
						"LastName = :lastName, "+
						"houseColours = :houseColours, " +
						"IsStudent = :isStudent, " +
						"IsTeacher = :isTeacher, " +
						"UniqueID =  :uniqueId, " +
						"image = :image," +
						"EncrptedUserId = :EncrptedUserId," +
						"isSync = :isSync, " +
						"TimeZone = :TimeZone " +
						"WHERE id = 1", // id will always be 1

				// params
				{
					"name": studentProfile.get('name'),
					//"dateOfBirth": (studentProfile.get('dateOfBirth') ? studentProfile.get('dateOfBirth').getKey() : null),
					"dateOfBirth": studentProfile.get('dateOfBirth'),
					/*"addressStreet": studentProfile.get('address') ? studentProfile.get('address').get('street') : null,
					"addressSuburb": studentProfile.get('address') ? studentProfile.get('address').get('suburb') : null,
					"addressState": studentProfile.get('address') ? studentProfile.get('address').get('state') : null,
					"addressPostcode": studentProfile.get('address') ? studentProfile.get('address').get('postcode') : null,
					"addressCountry": studentProfile.get('address') ? studentProfile.get('address').get('country') : null,*/
					"telephone": studentProfile.get('telephone'),
					"email": studentProfile.get('email'),
					"grade": studentProfile.get('grade'),
					"teacher": studentProfile.get('teacher'),
					"house": studentProfile.get('house'),
					"houseCaptain": studentProfile.get('houseCaptain'),
					"Password": studentProfile.get('password'),
					"houseColours": studentProfile.get('houseColours'),
					"isStudent": studentProfile.get('isStudent'),
					"isTeacher": studentProfile.get('isTeacher'),
					"lastName": studentProfile.get('lastName'),
					"uniqueId": studentProfile.get('UniqueID'),
					"image"	: studentProfile.get('image'),
					"isSync" : studentProfile.get('isSync'),
					"profileImage" : studentProfile.get('profileImage'),
					"TimeZone" : studentProfile.get('timeZone'),
					"EncrptedUserId" : studentProfile.get('EncrptedUserId'),
					"schoolId" : studentProfile.get('schoolId')
				},
				function(statement) {
					
					//successCallback || $.noop,
					successCallback(studentProfile);
				},
				// success
				

				// error
				errorCallback, "Error while trying to read student profile"
		);
	};
	
	/**
	 * Searches the student profile for fields that match a search criteria
	 */
	_delegate.prototype.searchStudentProfile = function(criteria, successCallback, errorCallback) {
		if (!criteria) {
			throw new Error("Search criteria must be defined");
		}

		this._database.runQuery(
				// query
				"SELECT COUNT(*) AS matches " +
						"FROM User " +
						"WHERE (name LIKE :searchCriteria " +
						"OR addressStreet LIKE :searchCriteria " +
						"OR addressSuburb LIKE :searchCriteria " +
						"OR addressState LIKE :searchCriteria " +
						"OR addressPostcode LIKE :searchCriteria " +
						"OR telephone LIKE :searchCriteria " +
						"OR email LIKE :searchCriteria " +
						"OR grade LIKE :searchCriteria " +
						"OR teacher LIKE :searchCriteria " +
						"OR house LIKE :searchCriteria " +
						"OR houseCaptain LIKE :searchCriteria " +
						"OR houseColours LIKE :searchCriteria)",

				// params
				{ 'searchCriteria' : "%" + criteria + "%" },

				// success
				function(statement) {
					successCallback(statement.getResult().data[0].matches);
				},

				// error
				errorCallback, "Error while trying to read student profile"
		);
	};
	
	// return the generated student profile delegate
	return _delegate;
})();

