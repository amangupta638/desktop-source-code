/**
 * The local SQL delegate for ContentFormData
 *
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.ContentItemFormSQLDelegate = (function() {

	var _delegate = function() {
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};

	_delegate.prototype.getContentItemFormData = function(article, successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"SELECT * FROM contentItemForm WHERE url = :url ",

				// params
				{'url' : article.get('url')},

				// success
				function(statement) {
					var formData = {};
					var dbFormData = statement.getResult().data;
					try {
						formData = JSON.parse(dbFormData[0].form);
						formData.id = dbFormData[0].id;
					} catch (e) {
						// ignore
					}
					successCallback(formData);
				},

				// error
				errorCallback, "Error while trying to get stored content item form data"
		);
	};

	_delegate.prototype.updateContentItemFormData = function(contentItemFormData, successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"UPDATE contentItemForm SET form = :form WHERE id = :id",

				// params
				{
					'form' : JSON.stringify(_.omit(contentItemFormData, 'id')),
					'id' : contentItemFormData.id
				},

				// success
				successCallback || $.noop,

				// error
				errorCallback, "Error while trying to update content item form data"
		);
	};

	_delegate.prototype.addContentItemFormData = function(contentItemFormData, article, successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"INSERT INTO contentItemForm (url, form) VALUES (:url, :form)",

				// params
				{
					'url' : article.get('url'),
					'form' : JSON.stringify(contentItemFormData)
				},

				// success
				function (statement) {
					successCallback(statement.getResult().lastInsertRowID);
				},

				// error
				errorCallback, "Error while trying to add content item form data"
		);
	};

	// return the generated delegate
	return _delegate;
})();
