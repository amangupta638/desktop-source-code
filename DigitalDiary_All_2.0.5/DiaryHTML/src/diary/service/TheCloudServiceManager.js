/**
 * The manager for "The Cloud"
 * 
 * author - Jane Sivieng
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.TheCloudServiceManager = (function() {
	
	/**
	 * Constructs a "The Cloud" Service Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		this._delegate = app.locator.getService(ServiceName.THECLOUDSERVICEDELEGATE);
	};


    var getApplicationAuthenticationData = function() {
        var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
        //alert("email : "+systemConfig.get('email'));
        //TODO : change hardcoded token id
        return {
        	'token' : '387b083',
            'macAddresses' : systemConfig.get('macAddresses'),
			'clientVersion' : systemConfig.get('versionNumber'),
			'emailId' : systemConfig.get('email')
        };
    };
	
	/**
	 * Register a token with "The Cloud"
	 * 
	 * @param token
	 * @param successCallback
	 * @param errorCallback
	 */
	_manager.prototype.registerToken = function(token, successCallback, errorCallback) {
		//alert("registerToken : "+token);
		var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION),
			onSuccess = function(result) {
				alert("token register successful : "+JSON.stringify(result));
				//console.log("registerToken succeeded ", JSON.stringify(result));
				successCallback(result);
			},
			onError = function(message) {
				alert("token register failed : "+result);
				
				//console.log("registerToken failed: " + message);
				errorCallback && errorCallback(message);
			};
		
		if (!isSpecified(token)) {
			throw new Error("Token must be specified.");
		}
		
		if (typeof(successCallback) !== "function") {
			
			throw new Error("Success callback must be defined.");
		}
			
		this._delegate.callService(diary.service.TheCloudServiceDelegate.METHODS.REGISTER, diary.service.TheCloudServiceDelegate.TYPE.GET, {
				'token' : token, 
				'macAddresses' : systemConfig.get('macAddresses'),
				'clientVersion' : systemConfig.get('versionNumber'),
				'deviceType':1
			}, 
			onSuccess,
			onError
		);
		
	};

/*_manager.prototype.registerToken = function(Diaryitem, successCallback, errorCallback) {

this._delegate.Sync(Diaryitem, 
            onSuccess,
            onError
        );
});*/
	/**
	 * Send usage logs to the "The Cloud"
	 * 
	 * @param {diary.collection.UsageLogEventCollection} usageLogs
	 * @param successCallback
	 * @param errorCallback
	 */
	_manager.prototype.sendUsageLogs = function(usageLogs, successCallback, errorCallback) {
		
		var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION),
			onSuccess = function(result) {
				//console.log("sendUsageLogs succeeded ", JSON.stringify(result));
				successCallback(result);
			}, 
			onError = function(message) {
				//console.log("sendUsageLogs failed: " + message);
				errorCallback && errorCallback(message);
			};

		var usageLogManager = app.locator.getService(ServiceName.USAGELOGMANAGER);

		var usageLogsToSend = usageLogs.map(function(usageLog) {
			return _.extend({
				'token' : usageLog.get('token'),
				'configuration' : usageLog.get('schoolConfiguration'),
				'id' : usageLog.get('id'),
				'time' : usageLog.get('time').getTime(),
				'type' : usageLog.get('type'),
				'detail' : usageLog.get('detail'),
				'version' : usageLog.get('version'),
				'clientVersion' : usageLog.get('clientVersion')
			}, usageLogManager.getAdditionalFields(usageLog));
		});


	this._delegate.callService(diary.service.TheCloudServiceDelegate.METHODS.ADDUSAGELOGS, diary.service.TheCloudServiceDelegate.TYPE.POST, {
				'token' :  systemConfig.get('token'), 
				'macAddresses' : systemConfig.get('macAddresses'),
				'usageLogs' : JSON.stringify(usageLogsToSend),
				'log_created_time':'1367240578333',
				'clientVersion' : systemConfig.get('versionNumber')
			}, 	
			onSuccess, 
			onError
		); 
		
	};

    /**
     * Downloads the school configuration zip file from "The Cloud" for the registered token
     *
     * @param successCallback
     * @param errorCallback
     */
    _manager.prototype.downloadSchoolConfiguration = function(successCallback, errorCallback) {
        var data = getApplicationAuthenticationData(),
            onSuccess = function(zipFile) {
               // console.log("downloadSchoolConfiguration succeeded ");
                successCallback(zipFile);
            },
            onError = function(message) {
               // console.log("downloadSchoolConfiguration failed: " + message);
                errorCallback && errorCallback(message);
            };

        if (typeof(successCallback) !== "function") {
            throw new Error("Success callback must be defined.");
        }
        //alert("downloadSchoolConfiguration : cloud service manager"+JSON.stringify(data));
       // this._delegate.Sync();
        this._delegate.downloadFile(diary.service.TheCloudServiceDelegate.METHODS.DOWNLOADSCHOOLCONFIG,
            data,
            onSuccess,
            onError
        );
    };
    
    


    /**
     *
     * Downloads the given content package from "The Cloud"
     *
     * @param package
     * @param successCallback
     * @param errorCallback
     */
    _manager.prototype.downloadContentPackage = function(packageName, successCallback, errorCallback) {
        var data = getApplicationAuthenticationData(),
            onSuccess = function(zipFile) {
               // console.log("downloadContentPackage succeeded ");
                successCallback(zipFile);
            },
            onError = function(message) {
              //  console.log("downloadContentPackage failed: " + message);
                errorCallback && errorCallback(message);
            };

        if (typeof(successCallback) !== "function") {
            throw new Error("Success callback must be defined.");
        }

		//Try Commenting below line
        /* this._delegate.downloadFile(diary.service.TheCloudServiceDelegate.METHODS.DOWNLOADCONTENTPACKAGE,
            $.extend(data, {
                'package' : packageName
            }),
            onSuccess,
            onError
        );  */  
    };

    
    /**
	 * Register a user with "The Cloud"
	 * 
	 * @param email, pwd
	 * @param successCallback
	 * @param errorCallback
	 */
    
_manager.prototype.registerUser = function(email, password, domain, successCallback, errorCallback) {
		//alert("registerUser : ");
		var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION),
			onSuccess = function(result) {
				systemConfig.set('email',email)
				emailExits = true;
				var schoolDetails = result.school_details;
				var studentDetails = result.user_details;
				
				console.log("school_details : "+JSON.stringify(schoolDetails));
				var isteacher = studentDetails.IsTeacher;
			
				
				(app.model.get(ModelName.SCHOOLPROFILE)).set('name',schoolDetails.Name);
				(app.model.get(ModelName.SCHOOLPROFILE)).set('theme',schoolDetails.Theme);
				(app.model.get(ModelName.SCHOOLPROFILE)).set('UniqueID',schoolDetails.Id);
				(app.model.get(ModelName.SCHOOLPROFILE)).set('empowerType', schoolDetails.Empower_Type);
				
				(app.model.get(ModelName.SCHOOLPROFILE)).set('schoolLogo', schoolDetails.School_Logo);
				(app.model.get(ModelName.SCHOOLPROFILE)).set('schoolImage', schoolDetails.School_Image);
				(app.model.get(ModelName.SCHOOLPROFILE)).set('schoolBgImage', schoolDetails.school_bgImage);
				(app.model.get(ModelName.SCHOOLPROFILE)).set('domainName', schoolDetails.DomainName);
				
				//console.log("studentDetails.empowerType : " + studentDetails.empowerType);
				if("\1" === schoolDetails.Include_Empower || schoolDetails.Include_Empower == '1'){
					//console.log("studentDetails.Include_Empower true : ");
					(app.model.get(ModelName.SCHOOLPROFILE)).set('includeEmpower', true);
				}
				else{
					//console.log("studentDetails.Include_Empower false : ");
					(app.model.get(ModelName.SCHOOLPROFILE)).set('includeEmpower', false);
				}
				
				if("\1" === schoolDetails.Allow_Parent || schoolDetails.Allow_Parent == '1'){
					//console.log("studentDetails.Allow_Parent true : ");
					(app.model.get(ModelName.SCHOOLPROFILE)).set('allowParents', 1);
				}
				else{
					//console.log("studentDetails.Allow_Parent false : ");
					(app.model.get(ModelName.SCHOOLPROFILE)).set('allowParents', 0);
				}
				
				(app.model.get(ModelName.STUDENTPROFILE)).set('name',studentDetails.FirstName);
				(app.model.get(ModelName.STUDENTPROFILE)).set('dateOfBirth',studentDetails.DateOfBirth);
				//check db for addressId field
				//(app.model.get(ModelName.STUDENTPROFILE)).set('address',studentDetails.AddressId);	
				(app.model.get(ModelName.STUDENTPROFILE)).set('telephone',studentDetails.Telephone);
				
				(app.model.get(ModelName.STUDENTPROFILE)).set('grade',studentDetails.grade_name);
				(app.model.get(ModelName.STUDENTPROFILE)).set('password',password);
				(app.model.get(ModelName.STUDENTPROFILE)).set('email',studentDetails.Email);
				(app.model.get(ModelName.STUDENTPROFILE)).set('lastName',studentDetails.LastName);
				(app.model.get(ModelName.STUDENTPROFILE)).set('diaryTitle',studentDetails.Diary_Title);
				(app.model.get(ModelName.STUDENTPROFILE)).set('profileImage',studentDetails.ProfileImage);
				//(app.model.get(ModelName.UniqueID)).set('UniqueID',studentDetails.Id);
				(app.model.get(ModelName.STUDENTPROFILE)).set('UniqueID',studentDetails.Id);
				(app.model.get(ModelName.STUDENTPROFILE)).set('EncrptedUserId',studentDetails.EncrptedUserId);
				
				/*(app.model.get(ModelName.STUDENTPROFILE)).set('address', new diary.model.Address({
			 		'street' : address_line_1,
					'suburb' : cityTownSuburb,
					'state' : state,
					'postcode' : postalCode,
					'country' : country
				}));
				*/
				if("\1" === studentDetails.IsTeacher || studentDetails.IsTeacher == '1'){
					(app.model.get(ModelName.STUDENTPROFILE)).set('isTeacher',true);
					(app.model.get(ModelName.STUDENTPROFILE)).set('isStudent',false);
				}
				else{
					(app.model.get(ModelName.STUDENTPROFILE)).set('isTeacher',false);
					(app.model.get(ModelName.STUDENTPROFILE)).set('isStudent',true);
				}
				
				
				// update student profile
				app.context.trigger(EventName.UPDATESTUDENTPROFILE, app.model.get(ModelName.STUDENTPROFILE));
				setTimeout(function() {
	        		app.view.loginView.hideLogin();
	        	}, 2000);
				
				app.view.themeView.showTheme(app.model.get(ModelName.SCHOOLPROFILE), app.model.get(ModelName.STUDENTPROFILE));
				//app.view.splashView = new diary.view.air.ContainerSplashView();
				//app.view.splashView.showSplash(app.model.get(ModelName.STUDENTPROFILE));
				var result = {
						schoolid : schoolDetails.Id,
						success : 'true'
					}
				
				saveschoolId(schoolDetails.Id);
				saveSchoolInformation(schoolDetails.Include_Empower,schoolDetails.Empower_Type, schoolDetails.Allow_Parent, schoolDetails.Theme, schoolDetails.school_bgImage, schoolDetails.DomainName);
				
				downloadSchoolBgImageFile();
				downloadSchoolLogoFile();
				downloadSchoolImageFile();
				
				successCallback(result);
			},
			onError = function(message) {
				$(".mainLogin #busy").hide();
				console.log("#busy : "+$(".mainLogin #busy")+"busy : "+$("#busy"));
				alert("Login failed : "+message);
				errorCallback && errorCallback(message);
			};
		
		if (!isSpecified(email)) {
			throw new Error("email must be specified.");
		}
		
		if (typeof(successCallback) !== "function") {
			throw new Error("Success callback must be defined.");
		}
		
		this._delegate.callService(diary.service.TheCloudServiceDelegate.METHODS.LOGIN, diary.service.TheCloudServiceDelegate.TYPE.POST, {
				'email_id':email, 
				'password':password,
				'domain':domain,
				'device_token':'1',
				'device_type':1
			}, 
			onSuccess,
			onError
		);
	};
	
	
	// return the generated system configuration manager
	return _manager;
})();

