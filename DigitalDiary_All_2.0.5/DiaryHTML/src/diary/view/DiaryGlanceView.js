diary = typeof (diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};
var isOpenPopup = false;
var popupOnDay = "", selectedDivPosition = 0;
diary.view.DiaryGlanceView = (function() {

	var calendar = [];

	function getMonthIndex(monthNumber) {

		var offset = [0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8];
		return offset[monthNumber];

	}

		var view = Rocketboots.view.View.extend({
			events: {
			"click .glanceView .glanceEvents li"					: "showDiaryItemForm",
			"click .glanceView .summarymultiplewidget"				: "showTasksInPopup",		// pop-up for multiple subjects
			"dblclick .glanceView .summarymultiplewidget"			: "stopDoubleClickSummary",	// stop the double click of the summary
			"click .glanceView .subjectwidget"						: "showTasksInPopup",		// pop-up for single subject
			"dblclick .glanceView .subjectwidget"					: "showDiaryItemForm",		// pop-up the edit for single subject
			"click .glanceView .calendarDateSelect"					: "selectMonth",			// month selector
			"click .glanceView .calendarNav li"						: "changeMonth",			// previous/next month
			"click .glanceView .glanceCalendar .monthtitlecell"		: "navigateToMonth",		// navigate to specific month
			//"click .glanceView .daycell"							: "selectDay",				
			"click .glanceView .daycell"							: "navigateToDay",			// navigate to a particular day
			"onload"												: "renderGlanceView",
			"click .jTscroller .ts2Ancher"							: "monthSelect",
			"click .studCalendarAccess"								: "accessStudCalendar"
		},

		initialize : function() {

			var startDate = new Rocketboots.date.Day(app.model.get(ModelName.APPLICATIONSTATE).get('startDate'));
			var today = new Rocketboots.date.Day();
			//console.log("INITIALIZE GLANCE VIEW");
			//GetStartDate();
			Rocketboots.view.View.prototype.initialize.call(this);

			var view = this, updateMonth = function() {
				//console.log("***************************");
				// console.log("DiaryGlanceView="+app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'));
				var month = app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay').getMonth();
				//console.log("DiaryGlanceView MONTH="+month);
				var monthName = monthNames[month];
				// console.log("monthName="+monthName);
				// var monthIndex=$.inArray(month, monthIndexArray);
				var monthIndex = $.inArray(monthName, monthArray);
				
				if((today.getTime() < startDate.getTime()) && (app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay').getTime() < startDate.getTime())){
					monthIndex = 0;
				}
				
				// console.log("monthIndex="+monthIndex);
				var selectedFirstMonthIndex = getMonthIndex(monthIndex);
				//  console.log("selectedFirstMonthIndex="+selectedFirstMonthIndex);
				// console.log("****************************");
				if (view.firstMonthIndex != selectedFirstMonthIndex) {
					view.firstMonthIndex = selectedFirstMonthIndex;
					view.isDirty = true;
				}

			}, updateCalendar = function() {
				view.mapCalendar();
				view.isDirty = true;
				view.render();
			};

			view._scrollManager = new diary.service.ScrollManager();
			view._scrollManager.maintainPosition = true;

			//console.log('DiaryGlanceView.initialize()');
			//console.log(this);
			//app.model.get(ModelName.APPLICATIONSTATE).on("change:" + 'selectedDay', updateMonth, this);
			app.listenTo(app.model.get(ModelName.APPLICATIONSTATE), "change:" + 'selectedDay', updateMonth);
			//app.model.on("change:" + ModelName.CALENDAR, updateCalendar, this);
			app.listenTo(app.model, "change:" + ModelName.CALENDAR, updateCalendar);

			// listen to the event so that we would know when to refresh the list of diary items that are being displayed
			// NOTE - The Diary View (the base class of all calendar views) is listening to the three different events that
			// are triggered by the diary item collection
			app.context.on(EventName.REFRESHDIARYITEMS, updateCalendar, this);

			view.isAlwaysDirty = true;
			updateMonth();
			view.mapCalendar();

			// render the screen
			this.render();
		},

		parseAdditionalParams : function(options) {
			if ( typeof (options.el) !== "undefined") {
				this.el = options.el;
			}
		},

		setCurrentMonth : function(monthIndex, dayIndex) {
			console.log("yearIndex="+monthIndex+"monthIndexArray : "+monthIndexArray);
			var yearIndex = $.inArray(parseInt(monthIndex), monthIndexArray);
			console.log("yearIndex="+yearIndex);
			var year = yearArray[yearIndex];
			var yeartoshow = yearArray[monthIndex];
			//  console.log("setCurrentMonth monthIndex="+monthIndex);
			// console.log("date="+new Rocketboots.date.Day(year, monthIndex, dayIndex ? dayIndex : 1));
			app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(year, monthIndex, dayIndex ? dayIndex : 1));
		},
		
		accessStudCalendar : function(event) {
			
			openCalendarAccess();
		},

		mapCalendar : function() {

			//console.log("mapping calendar");

			// get the calendar from the application model
			var calendarModel = app.model.get(ModelName.CALENDAR);
			// get all the diary items for the calendar
			var diaryItemsModel = app.model.get(ModelName.DIARYITEMS).getDiaryItems();
			//console.log("glance calendarModel=");
			//console.log(calendarModel);
			// replace the global calendar with the generated one
			calendar = diary.view.DiaryCalendarViewUtil.mapYear(calendarModel, diaryItemsModel);
			//console.log("MAP CALENDAER calendar=");
			//console.log(calendar);
			// include place-holder days (so all months have 31 rows)
			_.forEach(calendar, function(month) {
				// Explicitly disable dragging of all diary items in Glance view
				_.forEach(month.days, function(day) {
					_.forEach(day.events, function(diaryItem) {
						diaryItem.moveable = false;
					});

					_.forEach(day.tasks, function(diaryItem) {
						diaryItem.moveable = false;
					});

					_.forEach(day.notes, function(diaryItem) {
						diaryItem.moveable = false;
					});

					//console.log('mapCalendar events '+ day.events.length);
					//Assuming dayofnotes will come from event table
					//while(day.dayOfNotes.length + day.events.length > 3) {
					while (day.events.length > 3) {
						if (day.events.length > 0) {
							day.events.pop();
						} /* else if (day.dayOfNotes.length > 0) {
						 day.dayOfNotes.pop();
						 } */
					}
				});

				// if we don't have 31 days we will include empty days
				// so that the calendar will still display correctly
				while (month.days.length < 31) {
					month.days.push({
						'date' : ''
					});
				}
			});

		},

		render : function() {
			var updateGlanceCalendar = function() {
				view.isDirty = true;
				view.render();
			};
			app.model.get(ModelName.DIARYITEMS).off("change");
			app.model.get(ModelName.DIARYITEMS).off("remove");
			app.model.get(ModelName.DIARYITEMS).off("add");
			app.model.get(ModelName.DIARYITEMS).on("change", updateGlanceCalendar, this);
			app.model.get(ModelName.DIARYITEMS).on("remove", updateGlanceCalendar, this);
			app.model.get(ModelName.DIARYITEMS).on("add", updateGlanceCalendar, this);

			this.mapCalendar();
			//alert("glance render")
			//this.renderDiaryItemForm();
			//console.log("RENDER GLANCE VIEW");
			if (!this.isVisible)
				return;
			//console.log("diary.view.DiaryCalendarViewUtil.months=");
			//console.log(diary.view.DiaryCalendarViewUtil.months);
			//console.log("this.firstMonthIndex=");
			//console.log(this.firstMonthIndex);
			var view = this, today = new Rocketboots.date.Day(), isYear = today.getFullYear() == SystemSettings.HARDCODDEDYEAR, dateLabel = getCurrentDate(), calendarModel = app.model.get(ModelName.CALENDAR), dateRange = glanceViewDateRange(this.firstMonthIndex, this.firstMonthIndex + 3);
			//alert("calendar");
			// Mustache is "logic-less".  Therefore the view must work out all the conditional logic before Mustache is invoked
			var viewParams = {
				'disableToday' : !isYear,
				'dateNow' : dateLabel,
				'dateRange' : dateRange,
				'isTeacher'	: app.model.get(ModelName.STUDENTPROFILE).get("isTeacher"),
				'calendar' : {
					'months' : [calendar[this.firstMonthIndex], calendar[this.firstMonthIndex + 1], calendar[this.firstMonthIndex + 2], calendar[this.firstMonthIndex + 3]]
				}
			};
			
			var content = app.render('diaryGlance', viewParams);
			//$(".cal_filter_task").show();

			$(view.el).html(content);

			$("#glancePopupMonthList", view.el).on("click", function(event) {
				var monthNumber = $(event.target).index();
				//console.log("monthNumber="+monthNumber);
				/*var index=monthNumber;
				 var arr=diary.view.DiaryCalendarViewUtil.months;
				 var current=arr[index];
				 // console.log("current="+current);
				 var index2= $.inArray(current, monthNames)
				 // console.log("index2="+index2);
				 //monthNumber=index2;
				 // console.log("monthNumber="+monthNumber);*/
				if (!isSingleYear) {
					monthNumber = monthIndexArray[monthNumber];
					//console.log("monthNumber="+monthNumber);
					/*if(monthNumber<6)
					 {
					 monthNumber=monthNumber+6;
					 }
					 else if(monthNumber==6)
					 {
					 monthNumber=0;

					 }
					 else if(monthNumber>6)
					 {
					 monthNumber=monthNumber%6;
					 }*/
				}
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "GLANCE", monthNumber);

				view.setCurrentMonth(monthNumber);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			});

			view.selectMonth();
			_.defer(_.bind(function() {
				view._scrollManager.setScrollContainer($(".glanceColumnsWrapper", view.$el));
				view._scrollManager.resetScrollPosition();
			}));
			//console.log("selectedMonthGlanceView : "+selectedMonthGlanceView);
			if(selectedMonthGlanceView != ""){
						selectedDivPosition = 0;
						//console.log("$(#diaryGlanceDatePicker).find(.active) 111111111: "+$("#diaryMonthDatePicker").find(".active").attr("class"));
						$("#diaryGlanceDatePicker").find(".active").removeClass("active");
						//console.log("$(#diaryGlanceDatePicker).find(.active)2222222 : "+$("#diaryMonthDatePicker").find(".active").attr("class"));
					
						$(".weekfromto").each(function(){
							selectedDivPosition = selectedDivPosition + 80;
							if($(this).attr("id") == selectedMonthGlanceView){
								$(this).parent().addClass("active");
		      					$("#containerMonthList").scrollLeft(selectedDivPosition);
							}
							
						});
			}
			
			

		},

		renderDiaryItemForm : function() {
			var view = this, viewParams = {};

			viewParams.el = $('#glanceDiaryItemFormEditPopup', this.el);
			viewParams.editorref = "editor4";
			viewParams.classes = "glanceDiaryItemFormEditPopup";
			viewParams.onChange = function() {
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			};

			view.diaryItemForm = new diary.view.DiaryItemView(viewParams);
		},

		showTasksInPopup : function(event) {

			var view = this,
			// get the diary items
			diaryItemsModel = app.model.get(ModelName.DIARYITEMS),
			// get the identifier of the diary item that we need to display
			diaryItemIds = $(event.currentTarget).attr('diaryItemIds'), dayKey = $(event.currentTarget).closest('.daycell').attr('daykey');

			if (diaryItemIds) {

				// render the content of the popup
				var popupContent = app.render('diaryItemsSummary', {
					'items' : diary.view.DiaryCalendarViewUtil.mapTasksForPopup(diaryItemsModel, diaryItemIds, Rocketboots.date.Day.parseDayKey(dayKey))
				});
			
				if(!(popupOnDay == dayKey) || !(isOpenPopup)){
					$("#glancePopup>div.popupContent").html(popupContent);
					app.popup.show($("#glancePopup"), $(event.currentTarget), "right", null, event, $(".glanceColumnsWrapper", view.el));
			
					isOpenPopup = true;
					popupOnDay = dayKey;
				}	
				else{
					app.popup.hide($("#glancePopup"));
					isOpenPopup = false;
					popupOnDay = "";
					event.stopPropagation();
					
				}
				
					
				$("#glancePopup .diaryItemSummary .infolink", this.el).on("click", function(event) {
				
					view.showDiaryItemForm(event);
				});
				
				$(".webLinkDiaryItem", this.el).on("click", function(event) {	
					
					var link = prepareWeblinkURL($(event.currentTarget).attr('webLinkURL')); 
					var urlReq = new air.URLRequest(link); 
					air.navigateToURL(urlReq);
				});
				
				$("#glancePopup .diaryItemSummary .noteEntry .save", this.el).on("click", function(event) {
						
					// get the note that was entered
					var noteEntryElement = $(event.currentTarget).closest("#glancePopup .diaryItemSummary .noteEntry").find(".noteEntry"),
						noteId = parseInt(noteEntryElement.attr("noteId"), 10),
						noteText = noteEntryElement.val().trim();
					
					// append the note
					diary.view.DiaryView.appendNote(noteId, noteText);
					app.popup.hide($("#glancePopup"));
					openMessageBox("Diary item updated successfully.");
				});
				
				$("#glancePopup .diaryItemSummary .noteEntry .delete", this.el).on("click", function(event) {
				
	            // get the note that was entered
	            var hel = confirm("Are you sure to delete this note ?");
			
					if(hel)
					{
	           			var noteEntryElement = $(event.currentTarget).closest("#glancePopup .diaryItemSummary .noteEntry").find(".noteEntry"),
	                	noteId = parseInt(noteEntryElement.attr("noteId"), 10),
	                	noteText = noteEntryElement.val().trim();
	            		DeleteNoteEntryMethod(noteId);
	           			app.popup.hide($("#glancePopup"));
	            	}
	            	else
	            		return;
	            
	            
	        	});

				$("#glancePopup .diaryItemSummary ul.attachments > li.attachment").on("dblclick", view.openDiaryItemAttachment);

			}

		},

		selectMonth : function(event) {
			var popupParams = {
				months : []
			}, now = new Date();
			var index = parseInt(now.getMonth())
			var month = monthNames[index];
			// console.log("month="+month);
			var arr = diary.view.DiaryCalendarViewUtil.months;
			var monthIndex = $.inArray(month, arr);
			// console.log("indexofvalue="+$.inArray(month, arr));

			for (var i = 0; i < 12; i++) {
				popupParams.months.push({
					month : diary.view.DiaryCalendarViewUtil.months[i],
					index : i,
					thisMonth : (monthIndex == i && now.getFullYear() == SystemSettings.HARDCODDEDYEAR)
				});
			}

			var popupContent = app.render('monthList', popupParams);
			console.log("popupContent : " + popupContent);
			//app.popup.hide($("#glancePopupMonthList"));
			//$("#glancePopupMonthList>div.popupContent").html(popupContent);
			$("#diaryGlanceDatePicker", this.el).html(popupContent);
			//app.popup.show( $("#glancePopupMonthList"), $(event.currentTarget), "right", null, event );
		},

		changeMonth : function(event) {
			var $element = $("div", event.currentTarget);
			if ($element.hasClass("navLeft")) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_PREVIOUS, "GLANCE");
				this.previousMonth();
			} else if ($element.hasClass("navRight")) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_NEXT, "GLANCE");
				this.nextMonth();
			} else if ($element.hasClass("navToday") && !$element.hasClass('disabled')) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "GLANCE", "TODAY");
				this.todaysMonth();
			}
		},

		previousMonth : function() {
			//console.log("previousMonth DiaryGlanceView="+app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'));
			var view = this;
			//console.log("previousMonth view.firstMonthIndex="+view.firstMonthIndex)
			var monthName = monthNames[view.firstMonthIndex];
			var index = monthIndexArray[view.firstMonthIndex];
			//console.log("previousMonth index="+index);
			if (view.firstMonthIndex > 0) {
				view.setCurrentMonth(index);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
		},

		nextMonth : function() {
			//console.log("nextMonth DiaryGlanceView="+app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'));
			var view = this;
			//console.log("nextMonth view.firstMonthIndex="+view.firstMonthIndex)
			var monthName = monthNames[view.firstMonthIndex];
			//console.log("monthName="+monthName);
			var index = monthIndexArray[view.firstMonthIndex];
			//index=getMonthIndex(index);
			//console.log("nextMonth index="+index);
			//console.log(monthIndexArray[0]);
			if (view.firstMonthIndex < 8) {
				//console.log("INSIDE IF")
				index = index + 2;
				if (!isSingleYear) {
					if ((index) > 11) {
						index = (index) % 6;
					}
				}
				//console.log("nextMonth index2="+index);
				view.setCurrentMonth(index);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
		},

		todaysMonth : function() {
			var view = this, now = new Date();
			var index = parseInt(now.getMonth())
			var month = monthNames[index];
			// console.log("month="+month);
			var arr = diary.view.DiaryCalendarViewUtil.months;
			var monthIndex = $.inArray(month, arr);
			now = new Date();
			var dateIndex = now.getDate();
			var index = parseInt(now.getMonth())
			var month = monthNames[index];
			var monthIndex = $.inArray(month, monthArray);
			app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(yearArray[monthIndex], index, dateIndex));
			//view.setCurrentMonth(monthIndex, now.getDate());
			// call the refresh calendar so that the calendar will be refreshed
			app.context.trigger(EventName.REFRESHCALENDAR);
		},

		navigateToMonth : function(event) {
			app.model.get(ModelName.APPLICATIONSTATE).set('setToday', true);
			var monthToView = $(event.currentTarget).attr('data-month');
			app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(app.model.get(ModelName.CALENDAR).get('year'), monthToView - 1, 1));
			app.context.trigger(EventName.VIEWMONTH);
		},

		selectDay : function(event) {
			diary.view.DiaryView.selectDay(event);
		},

		navigateToDay : function(event) {
			app.model.get(ModelName.APPLICATIONSTATE).set('setToday', true);
			diary.view.DiaryView.selectDay(event);
			diary.view.DiaryView.navigateToDay(event);
		},

		showDiaryItemForm : function(event) {
			
			var diaryItemId = $(event.currentTarget).attr('diaryItemId');

			if (diaryItemId) {
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"SELECT UserID,createdBy,type FROM diaryItem WHERE id = :diaryItemId",

				// params
				{
					'diaryItemId' : diaryItemId
				},
				// success
				function(statement) {

					var detail = _.map(statement.getResult().data, function(item) {
						return item;
					});

					if (detail.length) {
						var userID = detail[0].UserID;
						var createdBy = detail[0].createdBy;
						var type = detail[0].type;
						diaryItemId && diaryItemForm.showDialog(diaryItemId, '', '', '', '', createdBy);
						if (createdBy != null && userID == createdBy) {
							$(".editButtonContainer .save").show();
							$(".ui-dialog-titlebar").hide();
							$(".editButtonContainer .lockItem").hide();
							$(".editButtonContainer .delete").show();
						} else {
							disablediaryitemformDay();
							if (type == 'EVENT') {
								$(".editButtonContainer .save").hide();
							} else {
								$(".editButtonContainer .save").show();
							}
							$(".editButtonContainer .delete").hide();
							$(".ui-dialog-titlebar").hide();
							$(".editButtonContainer .lockItem").show();
						}
					}
				},

				// error
				function error() {
					//console.log("Error while trying to create ClassUser item");
				});
			}

			event.stopPropagation();
		},

		openDiaryItemAttachment : function(event) {
		
			var target = $(event.target), diaryItemId = target.closest("article").find(".infolink").attr('diaryItemId'), attachmentId = target.closest(".attachment").attr('attachmentId'), diaryItem = null, attachment = null;

			if (diaryItemId && attachmentId) {
				diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId);
				attachment = diaryItem.get('attachments').get(attachmentId);
				if (diaryItem && attachment) {
					app.context.trigger(EventName.OPENATTACHMENT, diaryItem, attachment);
				}
			}
			event.stopPropagation();
		},

		stopDoubleClickSummary : function(event) {
			event.stopPropagation();
		},
		monthSelect : function(event) {
			var view = this;
			console.log("month select glance" + $(event.target).attr("id") + " class : " + $(event.target).attr("class"));
			var monthNumber = $(event.target).attr("id");
			//console.log("monthNumber="+monthNumber);
			selectedMonthGlanceView = monthNumber;
			if (!isSingleYear) {
				monthNumber = monthIndexArray[monthNumber];
				//console.log("monthNumber="+monthNumber);
				
			}
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "GLANCE", monthNumber);

			view.setCurrentMonth(monthNumber);
			// call the refresh calendar so that the calendar will be refreshed
			app.context.trigger(EventName.REFRESHCALENDAR);
		},
		renderGlanceView : function() {
			$("#tS2").thumbnailScroller({
				scrollerType : "hoverPrecise",
				scrollerOrientation : "horizontal",
				scrollSpeed : 2,
				scrollEasing : "easeOutCirc",
				scrollEasingAmount : 600,
				acceleration : 4,
				scrollSpeed : 800,
				noScrollCenterSpace : 5,
				autoScrolling : 0,
				autoScrollingSpeed : 2000,
				autoScrollingEasing : "easeInOutQuad",
				autoScrollingDelay : 500
			});

		}
	});

	return view;

})(); 