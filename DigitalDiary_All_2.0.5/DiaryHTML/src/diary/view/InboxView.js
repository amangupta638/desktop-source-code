diary = diary || {};
diary.view = diary.view || {};
var itemId = 0;
var itemType;
var gArrSelectedStudent = [];
var recvrType="";
var Subject = "";

var tempHtml = "";

diary.view.InboxView = (function() {

	var inboxView = Rocketboots.view.View.extend({

		events: {
			"click .inboxItemContainer .inboxItemTable .inboxItemRow" 		: "renderInboxItem",
			"click .inbox_content .left_ib .sequence .dt_icon"				: "createOrReplyMessage",
			"click .richbox .send"											: "sendOrReplyMessage",
			"click .right_ib .replyToMessage"								: "createOrReplyMessage",
			"click .right_ib .deleteMessage"								: "deleteMessage",
			"click .modalTableInbox img.attachment"							: "openAttachment",
			"click .sort_dt"												: "sortInbox",
			"click #studentSelect"											: "sendToStudents",
			"click #parentSelect"											: "sendToParents",
			"click #classSelect"											: "sendToClass",
			"click #allSelect"												: "sendToAll",
			"click #teacherSelect"											: "sendToTeacher",
			"click #labelSpecificStudent"									: "showSpecificStudent",
			"click #cancelPopupBtn"											: "cancelPopup",
			"click #doneBtn"												: "sendOrReplyMessage",
			"click #doneBtnStudentSelection"								: "doneStudentSelection",
			"click #divStudentList .MSDList_ListCheckBox"					: "updateStudentSelection",
			"click #labelChangeSelection"									: "changeSelection",
			"click #labelDiscardChange"										: "discardChange",
			"change .class #toList"											: "setSelectedStudentOnClassChange",
			"click .inbox_content"											: "hideToList",
			"click .MSDList_List"											: "preventHide",
			"blur .messageSubject"											: "setDirtyHtml",
			"click #inbox_filter_announcement"								: "inboxFilterClicked",
			"click #inbox_filter_news"										: "inboxFilterClicked",
			"click #inbox_filter_message"									: "inboxFilterClicked"
			
			
			
		},
		
		initialize: function(options) {
			//console.log("init : "+modeCreateMsg);
			var view = this;
			var selectedItemId = app.model.get(ModelName.APPLICATIONSTATE).get('inboxItemId');
			if(selectedItemId){
				view.itemId = selectedItemId;
				view.itemType = app.model.get(ModelName.APPLICATIONSTATE).get('inboxItemType');
			} else {
				view.itemId = 0;
			}
			
			app.context.trigger(EventName.FETCHINBOX); 
			view.isAlwaysDirty = true;
			this.render();
			
		},	
					
		render: function() {
			//console.log("render : "+modeCreateMsg);
			if(modeCreateMsg){
			
			}
			//console.log("render");
			var calcHeight = true;
			var count = 0;
			var trLen = 0;
			var trObj = null;
			var height = 0;
			var view = this;
			
			if(fromSync){
				this.inboxFilterClicked();		
				fromSync = false;
			} else {
				
				app.context.trigger(EventName.FETCHINBOX); 
				
				var selectedItemId = app.model.get(ModelName.APPLICATIONSTATE).get('inboxItemId');
				if(selectedItemId){
					view.itemId = selectedItemId;
					view.itemType = app.model.get(ModelName.APPLICATIONSTATE).get('inboxItemType');
				} else {
					view.itemId = 0;
				}
				var inboxItems = app.model.get(ModelName.INBOXITEMS).getFilterredInboxItems();
				var allInboxItems = [];
				for(var i = 0 ; i < inboxItems.length; i++){
					var inboxItem = inboxItems.at(i);
					var childsCount = (inboxItem.get('type') == 'News' || inboxItem.get('type') == 'Announcement') ? 0 : this.getChildMessages(inboxItem.get('UniqueID')).length;
					allInboxItems.push(this.mapMailThread(inboxItem, null, childsCount));
					
				}
				
				var sortInbox = app.model.get(ModelName.APPLICATIONSTATE).get('sortOrder');
				if(sortInbox == 'desc'){
					allInboxItems = allInboxItems.reverse();
				} 
				
				var mailThread = [];
				var message;
				
				if(view.itemId != undefined && view.itemId != 0){
					message = app.model.get(ModelName.INBOXITEMS).getInboxItem(view.itemType,view.itemId);
					mailThread.push(this.mapMailThread(message));
				} else {
					if(sortInbox == 'desc'){
						message =  inboxItems.at(allInboxItems.length-1); 
					} else {
						message =  inboxItems.at(0); 
					}
					
					var mailThread = [];
					
					if(message){
						var uniqueId = message.get('UniqueID');
						
						var childMessages = (message.get('type') == 'News' || message.get('type') == 'Announcement') ? [] : this.getChildMessages(uniqueId);
						mailThread.push(this.mapMailThread(message,uniqueId));
						$.each(childMessages,function(index,record){
							var recordId = record.Id;
							
							var item = app.model.get(ModelName.INBOXITEMS).getInboxItem("Message",recordId);
							mailThread.push(view.mapMailThread(item,uniqueId));   
			            });
				
					}
				}
				
				//console.log("allInboxItems : "+JSON.stringify(allInboxItems))
				var inboxItemList = {
					"inboxItems":allInboxItems,
					"mailThread":mailThread
				}
				
				var content = app.render('inboxView',inboxItemList);
				
				
				$(this.el).html(content);	
				
				if(sortInbox == 'desc'){
					allInboxItems = allInboxItems.reverse();
					$(".sequence .sort_dt",this.el).removeClass("sorted");
				} else {
					$(".sequence .sort_dt",this.el).addClass("sorted");
				}
				app.model.get(ModelName.APPLICATIONSTATE).set('inboxItemId',null);
				
				setTimeout(function() {
					
					if (view.itemId != 0)
					{
						trLen = $(".inboxItemRow", this.el).length;
						for (count = 0; count <= trLen; count++)
						{
							trObj = $(".inboxItemRow", this.el)[count];
							
							if ($(trObj).attr('itemId') == view.itemId && $(trObj).attr('itemType') == view.itemType)
							{
								$(trObj).parent().addClass('highlightMessage');
								height = height + $(trObj).parent().height();
								calcHeight = false;
							}
							else
							{
								if(calcHeight)
								{	
									height = height + $(trObj).parent().height();
								}
							}
						}
						
						$(".inboxItemContainer", this.el).scrollTop(((height / 2) - 150));
					}
					else
					{
						trObj = $(".inboxItemTable tr:first", this.el);
						$(trObj).parent().addClass('highlightMessage');
						$(".inboxItemContainer", this.el).scrollTop(0);
					}
					
				}, 200);
			}
			
			//console.log("tempHtml : "+tempHtml);
			if(tempHtml != "" && modeCreateMsg){
				//console.log("create msg");
				//$(".right_ib").html(tempHtml);
				$(".inbox_content .left_ib .sequence .dt_icon").trigger('click');
			}
			
		},
		openAttachment : function (event) {
			var link = $(event.currentTarget).attr('attachmentURL');
			var urlReq = new air.URLRequest(link); 
			air.navigateToURL(urlReq);
		},
		
		sortInbox : function (event) {
			if($(".sequence .sort_dt").hasClass("sorted")){
				app.model.get(ModelName.APPLICATIONSTATE).set('sortOrder', 'desc');
			} else {
				app.model.get(ModelName.APPLICATIONSTATE).set('sortOrder', 'ascending');
			}
			this.inboxFilterClicked();			
		},
		
		inboxFilterClicked : function (event) {
			//this.render();
			
			var view = this;
			app.context.trigger(EventName.FETCHINBOX); 
			
			var selectedItemId = app.model.get(ModelName.APPLICATIONSTATE).get('inboxItemId');
			if(selectedItemId){
				view.itemId = selectedItemId;
				view.itemType = app.model.get(ModelName.APPLICATIONSTATE).get('inboxItemType');
			} else {
				view.itemId = 0;
			}
			var inboxItems = app.model.get(ModelName.INBOXITEMS).getFilterredInboxItems();
			var allInboxItems = [];
			for(var i = 0 ; i < inboxItems.length; i++){
				var inboxItem = inboxItems.at(i);
				var childsCount = (inboxItem.get('type') == 'News' || inboxItem.get('type') == 'Announcement') ? 0 : this.getChildMessages(inboxItem.get('UniqueID')).length;
				allInboxItems.push(this.mapMailThread(inboxItem, null, childsCount));
				
			}
			
			var sortInbox = app.model.get(ModelName.APPLICATIONSTATE).get('sortOrder');
			if(sortInbox == 'desc'){
				allInboxItems = allInboxItems.reverse();
			} 
			
			var mailThread = [];
			var message;
			
			if(view.itemId != undefined && view.itemId != 0){
				message = app.model.get(ModelName.INBOXITEMS).getInboxItem(view.itemType,view.itemId);
				mailThread.push(this.mapMailThread(message));
			} else {
				if(sortInbox == 'desc'){
					message =  inboxItems.at(allInboxItems.length-1); 
				} else {
					message =  inboxItems.at(0); 
				}
				
				var mailThread = [];
				
				if(message){
					var uniqueId = message.get('UniqueID');
					
					var childMessages = (message.get('type') == 'News' || message.get('type') == 'Announcement') ? [] : this.getChildMessages(uniqueId);
					mailThread.push(this.mapMailThread(message,uniqueId));
					$.each(childMessages,function(index,record){
						var recordId = record.Id;
						
						var item = app.model.get(ModelName.INBOXITEMS).getInboxItem("Message",recordId);
						mailThread.push(view.mapMailThread(item,uniqueId));   
		            });
			
				}
			}
			
			var inboxItemList = {
				"inboxItems":allInboxItems,
				"mailThread":mailThread
			}
			
			var leftContent = app.render('inboxItemsList',inboxItemList);
			var rightContent = app.render('inboxItem',inboxItemList);
			$(".inboxItemsList").html(leftContent);
			$(".right_ib").html(rightContent);
			
			if(sortInbox == 'desc'){
				allInboxItems = allInboxItems.reverse();
				$(".sequence .sort_dt",this.el).removeClass("sorted");
			} else {
				$(".sequence .sort_dt",this.el).addClass("sorted");
			}
		},
		
		renderInboxItem : function (event) {
			modeCreateMsg = false;
			//alert('renderInboxItem : ');
			
			var id = null;
			var type = null;
			//var UniqueID = null;
			var uniqueId = null;
			
			var view = this;
			
			if (event != null)
			{	
				id = $(event.currentTarget).attr('itemId');
				type = $(event.currentTarget).attr('itemType');
				//UniqueID = $(event.currentTarget).attr('UniqueID');
				
				view.itemId = id;
				view.itemType = type;
				
				$('.selectMessage').removeClass('highlightMessage');
				$(event.currentTarget).parent().addClass('highlightMessage');
			}
			
			//alert('in renderInboxItem :: view.itemId = ' + view.itemId)
			
			
			
			if(type != 'News' && type != 'Announcement'){
				type = 'Message';
			}
			
			//var items = app.model.get(ModelName.INBOXITEMS).getInboxItem(type,id);
			var items = app.model.get(ModelName.INBOXITEMS).getInboxItem(view.itemType, view.itemId);
			
			
			uniqueId = items.get('UniqueID');
			
			view.uniqueId = uniqueId;
			
			//alert('another unique id = ' + uniqueId)
			
			//alert('this.getChildMessages = ' + this.getChildMessages(view.uniqueId).length)
			
			var childMessages = (items.get('type') == 'News' || items.get('type') == 'Announcement') ? [] : this.getChildMessages(uniqueId);
				
			var mailThread = [];
			
			
			if(items){
				mailThread.push(this.mapMailThread(items,uniqueId));
				$.each(childMessages,function(index,record){
					var recordId = record.Id;
					
					var item = app.model.get(ModelName.INBOXITEMS).getInboxItem("Message",recordId);
					mailThread.push(view.mapMailThread(item,uniqueId));  
	            });
				
			}

			var inboxItem = {
				"mailThread":mailThread
			} 
			
			var content = app.render('inboxItem',inboxItem);
			$(".right_ib").html(content);
			
		},
		
		createOrReplyMessage : function (event) {
			//console.log("createOrReplyMessage");
			modeCreateMsg = true;
			var receiverId = $(event.currentTarget).attr('receiverId');
			var receiverName = $(event.currentTarget).attr('receiverName');
			var subject = $(event.currentTarget).attr('subject');
			var parentId = $(event.currentTarget).attr('parentId');
			
			
			var studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
			var isTeacher = false;
			if(studentProfile.get("isTeacher")){
				isTeacher = true;
			}
			var isNewMsg = true;
			console.log("receiverId : "+receiverId);
			if(receiverId){
				isNewMsg = false;
			}
				
			
			//console.log("isNewMsg : "+isNewMsg)
			var viewParams = {
				//'toList' : toList,
				'isTeacher' : isTeacher,
				'isNewMsg' : isNewMsg,
				'allowParents' : app.model.get(ModelName.SCHOOLPROFILE).get("allowParents") == 1 ? true : false
				
			};
			console.log("viewParams : "+JSON.stringify(viewParams));
			var content = app.render('createReplyMessage',viewParams);
			$(".right_ib").html(content);
			
			var selectUserType = ""
			// console.log("receiverId : "+receiverId);
			var toList = [];
			if(isNewMsg){
				if(!(isTeacher)){
					//console.log("student inbox"+$("#teacherSelect"));
					$("#teacherSelect").attr("checked",true);
					selectUserType = "teacher"
				}
				else{
					//console.log("teacher inbox"+$("#studentSelect"));
					$("#studentSelect").attr("checked",true);
					selectUserType = "student"
				}
				toList = this.getToList();
			}
			
			
			//console.log("receiverId : "+receiverId+"receiverName : "+receiverName);
			if(!(this.containsSender(toList,receiverId)) && receiverId != undefined){
				var obj1 = new Object();
				obj1.UserName = receiverName;
			  	obj1.UserId = receiverId;
			  	obj1.type = selectUserType;
			  	
			  	toList.push(obj1); 
			}
			//console.log("toList : "+JSON.stringify(toList));
			
			viewParams = {
					'toList' : toList,
					'type' : selectUserType
			};
			content = app.render('inboxViewToList',viewParams);
			$("#divTolist").html(content);
			var receiverlist = new MSDList("toList");
			receiverlist.render();
			receiverlist.unselectAllItems();
			
			//$(".toList").attr("multiselect", true); 
			if(!isNewMsg){
				
				$("#divTolist input:checkbox[value=" + receiverId + "]").prop('checked', true).triggerHandler('click');
				$(".messageSubject").val(subject);
				$(".hiddenParentId").val(parentId);
				$("#selectAllDiv").hide();
				
			} else {
				$("select.toList option").filter(function() {
				    return $(this).val() == ""; 
				}).attr('selected', true);
				
				$(".messageSubject").val("");
				$(".hiddenParentId").val(0);
			}
			tempHtml = $(".right_ib").html();
		},
		
		containsSender : function (toList,receiverId) {
			var flag = false;
			for (var i = 0; i < toList.length; i++) {
		        if (toList[i].UserId == receiverId) {
		        	flag = true;
		            return flag;
		        }
		        
		    }
			return flag;
		},
		sendOrReplyMessage : function (event) {
			console.log('sendOrReplyMessage : ');
			//alert('sendOrReplyMessage : ');
			var view = this;
			var inboxItems = new diary.model.InboxItem();
			var parentId = $(event.currentTarget).attr('parentId');
			
			var attrs = this.getAttributesForSave(parentId);
			var	result = inboxItems.set(attrs);
			
			console.log("inboxItem : "+JSON.stringify(inboxItems));
			if(attrs && result){
				var arrReceiverId = inboxItems.get("receiverId").split(",");
				var arrReceiverName = inboxItems.get("receiverName").split(",");
				var receiverIds = "";
				var receiverNames = "";
				//console.log("arrReceiverId.length : "+arrReceiverId.length);
				//console.log("arrReceiverName.length : "+arrReceiverName.length);
				for(var index = 0; index < arrReceiverId.length ; index++){
					
					if(index < (arrReceiverId.length - 1)){
						receiverIds = receiverIds + arrReceiverId[index] + ",";
						receiverNames = receiverNames + arrReceiverName[index]+ ",";
					}
					else{
						receiverIds = receiverIds + arrReceiverId[index];
						receiverNames = receiverNames + arrReceiverName[index];
					}
				}
				
				inboxItems.set("receiverId", receiverIds);
				inboxItems.set("receiverName", receiverNames);
				
				app.context.trigger(EventName.SENDMESSAGE, inboxItems);
				function clearCache() {
					app.context.trigger(EventName.CLEARSEARCHCACHE);
				}
				app.listenTo(app.model, "change:" + ModelName.INBOXITEMS, clearCache);
				app.listenTo(app.model.get(ModelName.INBOXITEMS), "change", clearCache);
				GetLastSync("inboxItem");
				triggerEventNewsAnncManager();
				fromSync = true;
				
				// if new mail
				parentId = $(".hiddenParentId").val();
				//console.log("parentId : "+parentId);
				if( parentId == 0){
					this.render();
				}
				else{
					setTimeout(function() {
						view.renderInboxItem();
						$('#msgCount_' + view.uniqueId).html((view.getChildMessages(view.uniqueId).length + 1));
					}, 2000);
				}
				
			}
			modeCreateMsg = false;
		},
		
		getChildMessages : function (uniqueId, inboxitemArr) {
			var view = this;
			var inboxItems = [];
			if(inboxitemArr){
				inboxItems = inboxitemArr;
			}
				
			var inboxitemsTemp;
			
			
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "SELECT Id, UniqueID  FROM Messages WHERE ParentId = :UniqueID AND UniqueID != 0 AND DeletedStatus = 0",
	
	            // params
	            {
	            	'UniqueID' :uniqueId
	            },
				// success
				function(statement) {
	            	inboxitemsTemp =_.map(statement.getResult().data, function(item) {
                        return item;
                    });
	            	
            		$.each(inboxitemsTemp,function(index,record){
            			inboxItems.push(record);
            		});
	            	
					if(inboxitemsTemp.length > 0){
						$.each(inboxitemsTemp,function(index,record){
							view.getChildMessages(record.UniqueID, inboxItems);
						});
					}
	    			
					
				},
				
				// error
				function error()
				{
					
				}
			);
			
			
				return inboxItems
			
			
		},
		getChildMessages1 : function(strUniqueId){
			var inboxitems;
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "SELECT Id, UniqueID  FROM Messages WHERE ParentId in (:UniqueID) AND DeletedStatus = 0",
	
	            // params
	            {
	            	'UniqueID' :strUniqueId
	            },
				// success
				function(statement) {
					inboxitems =_.map(statement.getResult().data, function(item) {
                        return item;
                    });
				},
				
				// error
				function error()
				{
					
				}
			);
			
			return inboxitems;
			
		},
		
		deleteMessage : function (event) {
			var view = this;
			var Id = $(event.currentTarget).attr('itemId');
			
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
				
	            // query
	            "UPDATE Messages SET DeletedStatus = 1, isSync = 0 WHERE Id = :Id",
	
	            // params
	            {
	            	'Id' :Id
	            },
				// success
				function(statement) {
	            	
	            	function clearCache() {
	    				app.context.trigger(EventName.CLEARSEARCHCACHE);
	    			}
	    			app.listenTo(app.model, "change:" + ModelName.INBOXITEMS, clearCache);
	    			app.listenTo(app.model.get(ModelName.INBOXITEMS), "change", clearCache);
	    			
	    			//console.log("Id : "+Id+"app.model.get(ModelName.INBOXITEMS) : "+JSON.stringify(app.model.get(ModelName.INBOXITEMS).get(Id)));
					
					var inboxItems = app.model.get(ModelName.INBOXITEMS);
					var inboxItem = null;
					for(var len = 0 ; len < inboxItems.length; len++){
						if(inboxItems.at(len).get("itemId") == Id && inboxItems.at(len).get("type") != "News" && inboxItems.at(len).get("type") != "Announcement"){
							inboxItems.remove(inboxItems.at(len));
							inboxItems.at(len).set("deletedStatus", 1);
							
							var eventParameters = getMessageEventParameters(inboxItems.at(len));
			      			//alert("eventParameters : " + JSON.stringify(eventParameters));
			      			FlurryAgent.logEvent("Message Deleted", eventParameters);
						}
					}
					
					view.render();
					//view.renderInboxItem();
					//$('#msgCount_' + view.uniqueId).html((view.getChildMessages(view.uniqueId).length + 1));
					
					openMessageBox("Deleted successfully");
				},
				
				// error
				function error()
				{
					FlurryAgent.logError("Message Error","Error in deleting Message", 1);
				}
			);
			
			
			
		},
		
		getAttributesForSave : function(ParentId) {
			var attrs = {};
			var studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
			
			var now = new Date(); 
			var date = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
			
			var monthnow = parseInt(new String(((date.getMonth())+1)));
			monthnow = monthnow == 0 ? "00" : (monthnow < 10 ? "0" : "") + monthnow;
			var minuteString = date.getMinutes();
			minuteString = minuteString == 0 ? "00" : (minuteString < 10 ? "0" : "") + minuteString;
			var secondString = date.getSeconds();
			secondString = secondString == 0 ? "00" : (secondString < 10 ? "0" : "") + secondString;
			var hourString = date.getHours();
			hourString = hourString == 0 ? "00" : (hourString < 10 ? "0" : "") + hourString;
			var dayString = date.getDate();
			dayString = dayString == 0 ? "00" : (dayString < 10 ? "0" : "") + dayString;
			
			var createdDate = date.getFullYear() +"-"+monthnow+"-"+ dayString +" "+hourString+":"+minuteString+":"+secondString;
			
			if($("#toListContainer").attr("class") == "class"){
				
				if($("#StudentIdList").val() == ""){
					openMessageBox("Please specify at least one recipient");
					return;
				}
			}
			else{
				if($(".toList").val() == ""){
					openMessageBox("Please specify at least one recipient");
					return;
				}
			}
			
			
			if($(".messageSubject").val() == ""){
				openMessageBox("Please Enter Subject");
				return;
			}
			
			attrs.title = $(".messageSubject").val();
			attrs.type = $(".messageSubject").val();
			attrs.parentId = $(".hiddenParentId").val();
			attrs.userId = UserId;
			attrs.userName = studentProfile.get('name')+ ' '+ studentProfile.get('lastName');
			if($("#toListContainer").attr("class") == "class"){
				attrs.receiverId = $("#StudentIdList").val();
				attrs.receiverName = $('#StudentNameList').val();
			}
			else{
				attrs.receiverId = $(".toList").val();
				attrs.receiverName = $('.MSDList_Label').html();
			}
			attrs.description = $(".messageDescription").val();
			attrs.IpAddress =  app.model.get(ModelName.SYSTEMCONFIGURATION).get('macAddresses');
			attrs.sentDate = createdDate;
			//if parent then send email id
			var arrEmailId = $('#parentEmailList').text().split(",");
			var emailIdList = "";
			for(var len = 0; len < arrEmailId.length; len++){
				console.log("arrEmailId[len] != : "+arrEmailId[len]);
				if(arrEmailId[len] != "noMail" && arrEmailId[len] != " noMail"){
					if(len < (arrEmailId.length - 1))
						emailIdList = emailIdList + arrEmailId[len] + ",";
					else
						emailIdList = emailIdList + arrEmailId[len];
				}
			}
			console.log("emailIdList : "+emailIdList);
			attrs.emailId = emailIdList;
			return attrs;
		},
		
		mapMailThread : function (inboxItem, uniqueId, mailThreadCount) {
			var isNews=false, isAnnouncement = false, isMessage = false, showReply = false, showDelete = false;
			var messageType = 'Message';
			var type = inboxItem.get('type');
			var studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
			
			if(type == 'News'){
				messageType = 'News';
				isNews = true;
			} else if(type == 'Announcement'){
				messageType = 'Announcement';
				isAnnouncement = true;
			} else {
				isMessage = true;
			}
			
			var tags = inboxItem.get('tags');
			var tagList = [];
			if(type == 'News' && tags != ''){
				var allTags = tags.split(",");
				for (var count in allTags) {
				  	var obj1 = new Object();
				  	obj1.tag = allTags[count];
				  	tagList.push(obj1);
				}
				
			}
			
			var description = inboxItem.get('description');
			var tablePresent = (description == null || description == undefined) ? 0 : description.indexOf("<tr");
			var showHTML = true;
			if(tablePresent > 0)
				showHTML = false;
				
			
			var receiverId;
			var receiverName;
			var userId = inboxItem.get('userId');
			
			if(UserId == userId){
				receiverId = inboxItem.get('receiverId');
				receiverName = inboxItem.get('receiverName');
				//console.log("sender : "+userId+" current user : "+UserId+" : sender and user is same hide reply button");
				showReply = false;
				showDelete = true;
			} else {
				receiverId = inboxItem.get('userId');
				receiverName = inboxItem.get('userName');
				showReply = true;
				if(studentProfile.get("isTeacher")){
					showDelete = true;
				}
				else{
					showDelete = false;
				}
			}
			
			return {
				'id' 			: inboxItem.get('itemId'),
				'title' 		: inboxItem.get('title'),
				'description' 	: inboxItem.get('description'),
				'sentDate' 		: this.convertDate(inboxItem.get('sentDate')),
				'sentTime' 		: this.convertTime(inboxItem.get('sentDate')),
				'type' 			: inboxItem.get('type'),
				'userName'		: inboxItem.get('userName'),
				'receiverName'	: receiverName,
				'actualReceiverName'	: inboxItem.get('receiverName'),
				'isNews'		: isNews,
				'isAnnouncement' : isAnnouncement,
				'isMessage'		: isMessage,
				'tags'			: tagList,
				'hasTags'		: tagList.length > 0 ? true : false,
				'author'		: inboxItem.get('author'),
				'campusName'	: inboxItem.get('campusName'),
				'iconType'		: messageType,
				'parentId'		: uniqueId ? uniqueId : inboxItem.get('parentId'),
				'itemId'		: inboxItem.get('itemId'),
				'receiverId'	: receiverId,
				'userId'		: inboxItem.get('userId'),
				'UniqueID'		: inboxItem.get('UniqueID'),
				'attachmentURL'	: inboxItem.get('attachmentURL'),
				'showHTML'		: showHTML,
				'mailThreadCount' : mailThreadCount == 0 ? null : mailThreadCount + 1,
				'showReply' 	: showReply,
				'showDelete'	: showDelete,
			};
		},
		
		sendToStudents : function(){
			
			var receiverId = $(event.currentTarget).attr('receiverId');
			var receiverName = $(event.currentTarget).attr('receiverName');
			console.log("sendToStudents  receiverId : "+receiverId);
			var toList = this.getToList();
			
			if(!(this.containsSender(toList,receiverId)) && receiverId != undefined){
				var obj1 = new Object();
				obj1.UserName = receiverName;
			  	obj1.UserId = receiverId;
			  	obj1.type = 'student';
			  	toList.push(obj1); 
			}
			
			viewParams = {
					'toList' : toList,
					'type' : 'student'
					
			};
			content = app.render('inboxViewToList',viewParams);
			console.log("content : of tolist : "+content);
			$("#divTolist").html(content);
			$("#selectAllDiv").show();
			var receiverlist = new MSDList("toList");
			receiverlist.render();
			receiverlist.unselectAllItems();
		},
		sendToParents : function(){
			
			var receiverId = $(event.currentTarget).attr('receiverId');
			var receiverName = $(event.currentTarget).attr('receiverName');
			console.log("sendToParents");
			var toList = this.getParentsList();
			console.log("&& receiverId != undefined : "+receiverId);
			if(!(this.containsSender(toList,receiverId)) && receiverId != undefined){
				var obj1 = new Object();
				obj1.UserName = receiverName
			  	obj1.UserId = receiverId
			  	obj1.type = 'parent';
			  	toList.push(obj1); 
			}
			
			viewParams = {
					'toList' : toList,
					'type' : "parent"
			};
			content = app.render('inboxViewToList',viewParams);
			console.log("content : of tolist : "+content);
			$("#divTolist").html(content);
			$("#selectAllDiv").show();
			var receiverlist = new MSDList("toList");
			receiverlist.render();
			receiverlist.unselectAllItems();
		},
		sendToClass : function(){
			
			
			
			var receiverId = $(event.currentTarget).attr('receiverId');
			var receiverName = $(event.currentTarget).attr('receiverName');
			console.log("sendToClass");
			var toList = this.getClassStudentList();
			console.log("classes list"+JSON.stringify(toList));
			if(!(this.containsSender(toList,receiverId)) && receiverId != undefined){
				var obj1 = new Object();
				obj1.UserName = receiverName;
			  	obj1.UserId = receiverId;
			  	obj1.type = 'class';
			  	toList.push(obj1); 
			}
			
			viewParams = {
					'toList' : toList,
					'type' : "class"
			};
			content = app.render('inboxViewToList',viewParams);
			console.log("content : of tolist : "+content);
			$("#divTolist").html(content);
			$("#labelSpecificStudent").show();
			$("#selectAllDiv").hide();
			//var receiverlist = new MSDList("toList");
			//receiverlist.render();
			//receiverlist.unselectAllItems();

		},
		sendToAll : function(){
			console.log("sendToAll");
			
			var receiverId = $(event.currentTarget).attr('receiverId');
			var receiverName = $(event.currentTarget).attr('receiverName');
			
			var toList = this.getStudentParentList();
			console.log("getStudentParentList list"+JSON.stringify(toList));
			if(!(this.containsSender(toList,receiverId)) && receiverId != undefined){
				var obj1 = new Object();
				obj1.UserName = receiverName;
			  	obj1.UserId = receiverId;
			  	obj1.type = 'all';
			  	toList.push(obj1); 
			}
			
			viewParams = {
					'toList' : toList,
					'type' : "all"
			};
			content = app.render('inboxViewToList',viewParams);
			console.log("content : of tolist : "+content);
			$("#divTolist").html(content);
			var receiverlist = new MSDList("toList");
			receiverlist.render();
			receiverlist.unselectAllItems();
		},
		sendToTeacher : function(){
			console.log("sendToTeacher");
		},
		getToList : function () {
			var studentProfile  = app.model.get(ModelName.STUDENTPROFILE);
			var fetchListFrom;
			var field;
			//if teacher is logged in then fetch list according to selected option
			if(studentProfile.get("isTeacher")){
				
				//if student then fetch student list
				console.log("$(#studentSelect)"+$("#studentSelect"));
				console.log("$(#studentSelect).attr(selected)"+$("#studentSelect").is(':checked'));
				console.log("$(#parentSelect).attr(selected)"+$("#parentSelect").is(':checked'));
				console.log("$(#classSelect).attr(selected)"+$("#classSelect").is(':checked'));
				
				if($("#studentSelect").is(':checked')){
					fetchListFrom = 'student where status = 1';
					field = 'student_id as user_id, firstName, lastName';
				}
				//if parents then fetch parents
				if($("#parentSelect").is(':checked')){
					console.log("parentSelect : ");
					fetchListFrom = 'myParent mp, student s where mp.status = 1 and  mp.isDeleted = 0 group by parent_id,studentId';
					//select mp.*, s.firstName|| " " || s.lastName from ;
					field = 'mp.*, s.firstName|| " " || s.lastName';
					// get parents
				}
				//if class then fetch classes
				if($("#classSelect").is(':checked')){
					console.log("classSelect : ");
					fetchListFrom = 'student  where status = 1';
					field = 'student_id as user_id, firstName, lastName';
					//get class from model
				}
			} 
			//if student is logged in then fetch list of my teachers
			else {
				fetchListFrom = 'myTeacher where status = 1 and isDeleted = 0';
				field = 'teacher_id as user_id, name';
			}
			
			var queryString = "SELECT Distinct "+ field +"  FROM "+fetchListFrom;
			
			var allusers;
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            queryString ,
	
	            // params
	            {
				
	            },
				// success
				function(statement) {
					allusers =_.map(statement.getResult().data, function(item) {
                        return item;
                    });
				},
				
				// error
				function error()
				{
					
				}
			);
			
			var users = [];
			$.each(allusers,function(index,record){
				var obj1 = new Object();
				
				if(studentProfile.get("isTeacher")){
					obj1.UserName = record.firstName + ' ' + record.lastName
					obj1.type = 'student';
				}else{
					obj1.UserName = record.name
					obj1.type = 'teacher';
				}
			  	obj1.UserId = record.user_id
			  			
			  	users.push(obj1);     
            });
            return users;
		},
		getParentsList : function () {
			
			var queryString = "select mp.*, s.firstName|| ' ' || s.lastName as studentName from myParent mp, student s where s.student_id = mp.studentId and s.status = 1 and mp.status = 1 and mp.isDeleted = 0 group by mp.parent_id,studentId";
			
			var allusers;
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            queryString ,
	            // params
	            {
	            },
				// success
				function(statement) {
					allusers =_.map(statement.getResult().data, function(item) {
                        return item;
                    });
				},
				
				// error
				function error()
				{
					
				}
			);
			
			var users = [];
			$.each(allusers,function(index,record){
				var obj1 = new Object();
				obj1.UserName = record.name
				obj1.UserDetails = ' (p/o ' +  record.studentName  + ')'
			    obj1.UserId = record.parent_id;
				obj1.email = record.email;								// if parent then insert email id into object
				obj1.type = 'parent'
			  	users.push(obj1);     
            });
			console.log("getParentsList : "+JSON.stringify(users));
            return users;
		},
		getClassStudentList : function(){
			var classUsers = app.model.get(ModelName.CLASSUSERS);
			console.log("classUser : "+JSON.stringify(classUsers));
			//getClassUsersLocal();
			var users = [];
			var classes = app.model.get(ModelName.CLASSES);
			for(var len = 0  ; len < classUsers.length; len++){
			//	console.log("classId : "+classUsers.at(len).get("class"));
				var obj1 = new Object();
				//console.log("classes :  : "+JSON.stringify(classes));
				classDetails = getObjectByValue(classes, classUsers.at(len).get("class"), 'uniqueId');
				//console.log("noOfUsers : "+JSON.stringify(classDetails));
				if(classDetails != null){
					noOfUsers = classUsers.at(len).get("users").length;
					//console.log("noOfUsers : "+noOfUsers);
					obj1.UserName = classDetails.get("name") 
					obj1.UserDetails = ' (' +  noOfUsers + ' students)'
				  	obj1.UserId = classUsers.at(len).get("class");
					obj1.type = 'class'
				  //	console.log("obj1 : "+JSON.stringify(obj1));
				  	users.push(obj1);   
				}
				  
            }
            return users;
		},
		getStudentParentList : function(){
			
			// get student list 
			var users = [];
			var queryString = "SELECT Distinct  student_id as user_id, firstName, lastName FROM student where status = 1";
				var allStudent;
				var allParent;
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
		            queryString ,
		            // params
		            {
		            },
					// success
					function(statement) {
		            	allStudent =_.map(statement.getResult().data, function(item) {
	                        return item;
	                    });
						
						$.each(allStudent,function(index,record){
							// push student into object
							var obj1 = new Object();
							obj1.UserName = record.firstName + ' ' + record.lastName
						  	obj1.UserId = record.user_id
						  	obj1.type = 'student'		
						  	users.push(obj1);     
				           
							if(app.model.get(ModelName.SCHOOLPROFILE).get("allowParents") == 1){
								// get parent list of specific student
							  	console.log(" student id to fetch parent id : "+ record.user_id);
								var queryString = "select mp.*, s.firstName|| ' ' || s.lastName as studentName from myParent mp, student s where mp.status = 1 and mp.isDeleted = 0 and s.student_id = mp.studentId and s.student_id = :studentId group by mp.parent_id,studentId";
								app.locator.getService(ServiceName.APPDATABASE).runQuery(
						            // query
						            queryString ,
						            // params
						            {
						            	'studentId' :  record.user_id
						            },
									// success
									function(statement) {
						            	allParent =_.map(statement.getResult().data, function(item) {
					                        return item;
					                    });
										$.each(allParent,function(index,record){
											var obj1 = new Object();
											obj1.UserName = record.name
											obj1.UserDetails = ' (p/o ' +  record.studentName  + ')'
										    obj1.UserId = record.parent_id;
											obj1.email = record.email;								// if parent insert email id into object
											obj1.type = 'parent'
										  	users.push(obj1);     
							            });
									},
									
									// error
									function error()
									{
										
									}
								);
							}
								
			            });
					},
					
					// error
					function error()
					{
						
					}
				);
			
				
				console.log("getStudentParentsList : "+JSON.stringify(users));
	            return users;
		},
		showSpecificStudent : function(){
			var selectedOpt = $("#toList option:selected").val();
			console.log("selectedOpt : "+selectedOpt);
			if(selectedOpt == "0"){
				openMessageBox("please select class");
			}
			else{
				//get class - student list
				var student;
				var classUsers = app.model.get(ModelName.CLASSUSERS);
				
			
				var classUsers = getObjectByValue(classUsers, selectedOpt, 'class');
				var classUserList = {
					'students' : classUsers.get("users").toJSON()
				}
				//console.log("classUserList : "+JSON.stringify(classUserList));
				$('#popupDiv', this.el).html(app.render('popupStudentList',classUserList));
				
				$('#divStudentList').show();
				$('#inboxFade').show();	
				
			}
		
		},
		cancelPopup : function(){
			$('#divStudentList').hide();
		    $('#inboxFade').hide();	
		   // console.log("gArrSelectedStudent : "+gArrSelectedStudent);
		    for(var len = 0; len < gArrSelectedStudent.length; len++){
		    	 console.log("gArrSelectedStudent[len] : "+gArrSelectedStudent[len]);
		    	$("#"+gArrSelectedStudent[len]).attr("value", gArrSelectedStudent[len]).attr("checked", true);
		    }
		},
		doneStudentSelection : function(){
			var totalStudent = 0;
			var selectedStudent = 0;
			gArrSelectedStudent = [];
			$("#divStudentList input:checkbox").each(function(){
				totalStudent++;
			    var $this = $(this);
			    if($this.is(":checked")){
			    	//insert into global array of selected student
			    	gArrSelectedStudent.push($this.val());
			    	selectedStudent++;
			    }
			});
			if(selectedStudent == 0){
				openMessageBox("Please specify at least one recipient");
			}
			else{
				if(totalStudent > selectedStudent){
					var originalText = $("#toList option:selected").text();
					var modifiedText = originalText.split("(");
					$("#toList option:selected").text(modifiedText[0] + " (modified list: " + selectedStudent + " Students)");
				}
				$('#divStudentList').hide();
			    $('#inboxFade').hide();
			    $('#labelSpecificStudent').hide();
			    $('#labelDiscardChange').show();
			    $('#labelChangeSelection').show();
			    
			   // console.log("gArrSelectedStudent in done selection : "+gArrSelectedStudent);
			}
			
			
		},
		updateStudentSelection : function(event){
			var StudentNameList  = "";
			var StudentIdList  = "";
			var student={
				selectedStudId : [],
				selectedName : []
			};
			//student.selectedStudId=[];
			$("#divStudentList input:checkbox").each(function(){
			    var $this = $(this);
			    console.log("checked stud id : "+$this.is(":checked"));
			    if($this.is(":checked")){
			    	student.selectedStudId.push($this.val());
			    	student.selectedName.push($this.attr("label"));
			    }
			});
			//console.log("student.selectedStudId : "+student.selectedStudId);
			$("#StudentNameList").val(student.selectedName);
			$("#StudentIdList").val(student.selectedStudId);
			//gArrSelectedStudent = student.selectedStudId;
		},
		changeSelection : function(){
			$('#divStudentList').show();
		    $('#inboxFade').show();
		},
		discardChange : function(){
			this.sendToClass();
		
		},
		setSelectedStudentOnClassChange : function(){
			var selectedOpt = $("#toList option:selected").val();
			//console.log("selectedOpt : "+selectedOpt);
			var studentNameList = [];
			var StudentIdList = [];
			gArrSelectedStudent = [];
			if(selectedOpt == "0"){
				openMessageBox("please select class");
			}
			else{
				var classUsers = app.model.get(ModelName.CLASSUSERS);
				classUsers = getObjectByValue(classUsers, selectedOpt, 'class');
				var students = classUsers.get("users");
				
				for(var len = 0; len < students.length; len++){
					StudentIdList.push(students.at(len).get("UniqueId"));
					studentNameList.push(students.at(len).get("name") + " " + students.at(len).get("lastName"));
				}
				$("#StudentIdList").val(StudentIdList);
				$("#StudentNameList").val(studentNameList);
				gArrSelectedStudent = StudentIdList;
			}
			  $('#labelSpecificStudent').show();
			  $('#labelDiscardChange').hide();
			  $('#labelChangeSelection').hide();
			console.log("gArrSelectedStudent on change class: "+gArrSelectedStudent);
		},
		
		
		convertDate : function(date){
			if(date != null || date != undefined || date != '')
			{
			    dateTime = date.split(" ");
			
			    var date = dateTime[0].split("-");
			    var yyyy = date[0];
			    var mm = date[1];
			    var dd = date[2];
			    
			    var schoolFormat = app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat');
			    if (schoolFormat == "USA")
					return mm + '-' + dd + '-'+ yyyy;
				else
					return dd + '-' + mm + '-'+ yyyy;
					
		    } else {
		    	return "";
		    }
		},
		hideToList : function(e){
			//setTimeout($(".MSDList_ButtonSection").trigger("click"),1);
			var currentTarget = e.target.className;
			console.log("currentTarget : "+currentTarget);
			//MSDList_ListTableHeaderFilterCell
			//MSDList_ListLabelCell
			//MSDList_ListCheckBoxCell
			//MSDList
			//MSDList_ListFilterBox
			if(currentTarget == "MSDList_ListTableHeaderFilterCell" || 
					currentTarget == "MSDList_ListLabelCell" || 
					currentTarget == "MSDList_ListCheckBoxCell" ||
					currentTarget == "MSDList_ListCheckBox" ||
					currentTarget == "MSDList_ListCheckBoxCell" ||
					currentTarget == "MSDList" ||
					currentTarget == "MSDList_ListFilterBox" || 
					currentTarget == "MSDList_Label" ||
					currentTarget == "MSDList_ButtonSection"){
					
					
			}
			else{
				
				$(".MSDList_List").hide();
				$(".MSDList_ListFilterBox").val("");
				/*
				var msdList = new MSDList();
				msdList.hideMsdList.call();
				*/
			}
			
			
		},
		
		convertTime : function(date){
			var includeMinutes = true;
			if(date != null || date != undefined || date != '')
			{
			    var time = dateTime[1].split(":");
			    var hour = time[0];
			    var minute = time[1];
			    //var minuteString = (minute == 0 ? (includeMinutes ? ":00" : "") : ":" + minute);
			    var minuteString = (minute == 0 ? (includeMinutes ? ":00" : "") : ":" + (minute < 10 ? "0" : "") + minute);
			
			    return (hour < 12 ?
					(hour == 0 ? 12 : hour) + minuteString + " AM" : hour - (hour == 12 ? 0 : 12) + minuteString + " PM" );
		    } else {
		    	return "";
		    }
		}

	});
	
	return inboxView;

})();