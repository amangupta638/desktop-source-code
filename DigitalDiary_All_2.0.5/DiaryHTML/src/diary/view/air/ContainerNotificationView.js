diary = diary || {};
diary.view = diary.view || {};
diary.view.air = diary.view.air || {};

diary.view.air.ContainerNotificationView = (function() {
	var view = Backbone.View.extend({

		viewShown: false,
		notificationHtmlLoader: null,
		notifications: [],
		updateInterval: null,

		_maxWindowHeight: function() {
			return Math.min(33 + (this.notifications.length * 48) + 6, 550);
		},

		initialize: function() {
			// Listen for new notifications	
			app.context.on(EventName.NEWNOTIFICATION, this.receiveNotification, this);

			diary.view.ThemeUtil.registerThemeListener(view);

			//console.log('diary.view.air.ContainerNotificationView ready. Waiting for events');
		},
	
		receiveNotification: function(notification) {
			var view = this;
			
			// Map to a simple object that can be understood by the template engine
			var displayNotification = {
				type: 		notification.get('type'),
				title: 		notification.get('title'),
				message: 	notification.get('message'),
				colour:		notification.get('colour') || "noColor",
				created: 	new Date(),
				expires: 	new Date(new Date().getTime() + SystemSettings.NOTIFICATIONDELAY)
			};

            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.SHOW_REMINDER, displayNotification);
			view.notifications.push(displayNotification);
			
			if (!view.viewShown) {
				view.showNotification();
			}

//			console.log("ContainerNotificationView.receiveNotification()", displayNotification);
		},
		
		showNotification: function() {
			var view = this;

			if (view.notificationHtmlLoader != null) return;
			 
			//console.log('showNotification() - Creating Notification NativeWindow ' + view.viewShown);
						
			var windowInitOptions = new air.NativeWindowInitOptions();
			windowInitOptions.systemChrome = air.NativeWindowSystemChrome.NONE;
			windowInitOptions.transparent = true;
			windowInitOptions.type = air.NativeWindowType.LIGHTWEIGHT;
		
			var notificationWidth = 250,
				notificationX = air.Capabilities.screenResolutionX - notificationWidth - 50,
				notificationY = 50,
				bounds = new air.Rectangle(notificationX, notificationY, notificationWidth, view._maxWindowHeight()),
				initialUrl = new air.URLRequest("notification.html");
		
			view.notificationHtmlLoader = air.HTMLLoader.createRootWindow(
				false,
				windowInitOptions,
				false,
				bounds
			);
			
			view.clippedMask = new au.com.productdynamics.view.ClippedRoundedWindow(view.notificationHtmlLoader);
		
			view.notificationHtmlLoader.window.nativeWindow.alwaysInFront = true;
			
			function notificationHtmlLoaderComplete(event) {
				view.notificationHtmlLoader.removeEventListener("complete", notificationHtmlLoaderComplete);
	
				view.notificationHtmlLoader.window.addEventListener('startDrag', function(event) {
					view.dragView.apply(view, [event]);
				});

				// Required for ThemeUtil
				view.document = view.notificationHtmlLoader.window.document;
				view.viewShown = true;			
				view.render();
		
				view.notificationHtmlLoader.window.nativeWindow.visible = true;	
			}
			
			view.notificationHtmlLoader.addEventListener("complete", notificationHtmlLoaderComplete);
			view.notificationHtmlLoader.load(initialUrl);
			
		},
		
		hideNotification: function() {
			var view = this;

			if (view.notificationHtmlLoader == null) return;

			view.notificationHtmlLoader.window.nativeWindow.close();
			view.notificationHtmlLoader = null;
			view.clippedMask = null;
			view.viewShown = false;
		},
		
		render: function() {
			var view = this;

			if (!view.viewShown) return;

			// Adjust the height of the window according to the number of notifications
			view.notificationHtmlLoader.stage.nativeWindow.height = view._maxWindowHeight();

			var notificationParams = {
				notifications:		view.notifications
			};
			
			var notificationClicked = function(index) {
				if (typeof(index) == "undefined") {
					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.DISMISS_NOTIFICATION);
					
					view.hideNotification();
					view.clearCheckForRenderUpdates();
				}
				else {
					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.DISMISS_INDIVIDUAL_NOTIFICATION, index);
					
					// Remove notification
					if (index < view.notifications.length) {
						view.notifications[index].expires = new Date();
					}
					
					view.checkForRenderUpdates(true);
				}
			};
						
			var notificationContent = app.render('notification', notificationParams);
			view.notificationHtmlLoader.window.notificationClicked = notificationClicked;
			view.notificationHtmlLoader.window.injectContent(notificationContent);

			// Notifications have a time limited life, kick of a polling check to keep the notifications visible for a limited time
			if (view.updateInterval == null) {
				view.updateInterval = setInterval(function() {
					view.checkForRenderUpdates.apply(view);
				}, 250);
			}

			diary.view.ThemeUtil.renderThemeFromSchoolProfile(view);
		},
	
		checkForRenderUpdates: function(forceUpdate) {
			var view = this,
				changes = false,
				now = new Date();
			
			if (forceUpdate == true) {
				changes = true;
			}
			
			// Check to see if any of the display notifications should be removed
			for(var i=0; i<view.notifications.length; i++) {
				if (now > view.notifications[i].expires) {
					changes = true;
					view.notifications.splice(i, 1);
					i--;		
				}
			}
			
			// If the notifications to display have changed
			if (changes) {
				if (view.notifications.length > 0) {
					// Rerender the remaining notifications
					view.render();
				}
				else {
					// Hide, if nothing more to display
					view.hideNotification();
				}
			}

			// No more notifications to display, clear the interval timer
			if (view.notifications.length == 0) {
				view.clearCheckForRenderUpdates();
			}
			
		},
		
		clearCheckForRenderUpdates: function() {
			var view = this;
			
			if (view.updateInterval == null) return;
			
			clearInterval(view.updateInterval);
			view.updateInterval = null;
		},
		
		dragView: function(event) {
			var view = this;
			view.notificationHtmlLoader.window.nativeWindow.startMove();
		}
		
	});

	return view;

})();
