diary = diary || {};
diary.view = diary.view || {};
diary.view.air = diary.view.air || {};

diary.view.air.ContainerTokenView = (function() {
	
	var view = Backbone.View.extend({

		viewShown: false,
		tokenHtmlLoader: null,
		internetAvailable: false,
		isUpdateRequest: false,

		initialize: function() {
			//this.model.bind('change', this.render, this);
			//alert("init token view");
			diary.view.ThemeUtil.registerThemeListener(this);
			this.render();
		},
	
		showToken: function(requestType) {
			//console.log('ContainerTokenView.showToken()');
			//alert("requestType : "+requestType);
			var view = this;

			view.handledClose = false;
			
            requestType = requestType || diary.view.air.ContainerTokenView.REQUESTTYPE.REGISTER;
			view.isUpdateRequest = requestType == diary.view.air.ContainerTokenView.REQUESTTYPE.UPDATE;
 			
			if (view.tokenHtmlLoader != null) {
				return;
			}
			
			var windowInitOptions = new air.NativeWindowInitOptions();
			windowInitOptions.type = air.NativeWindowType.LIGHTWEIGHT;
			windowInitOptions.systemChrome = air.NativeWindowSystemChrome.NONE;
			windowInitOptions.transparent = true;
			//alert("view._appState.get('windowHeight') : "+view._appState.get('windowHeight'));
			//alert("air.Capabilities.screenResolutionX : "+air.Capabilities.screenResolutionX)
			var tokenWidth = 500,
				tokenHeight = 500,
				tokenX = (air.Capabilities.screenResolutionX - tokenWidth) / 2,
				tokenY = (air.Capabilities.screenResolutionY - tokenHeight) / 2,
				bounds = new air.Rectangle(tokenX, tokenY, tokenWidth, tokenHeight),
				initialUrl = new air.URLRequest("token.html");
		
			view.tokenHtmlLoader = air.HTMLLoader.createRootWindow(
				false,
				windowInitOptions,
				false,
				bounds
			);
			
			view.clippedMask = new au.com.productdynamics.view.ClippedRoundedWindow(view.tokenHtmlLoader);

			view.tokenHtmlLoader.window.nativeWindow.alwaysInFront = true;
			
			function tokenHtmlLoaderComplete(event) {
				view.tokenHtmlLoader.removeEventListener("complete", tokenHtmlLoaderComplete);

				// Required for ThemeUtil
				view.document = view.tokenHtmlLoader.window.document;
				view.viewShown = true;			
				view.render();

				view.tokenHtmlLoader.window.nativeWindow.visible = true;
				view.tokenHtmlLoader.window.addEventListener('processToken', function(event) {
					view.processToken.apply(view, [event]);
				});

				view.tokenHtmlLoader.window.addEventListener('startDrag', function(event) {
					view.dragView.apply(view, [event]);
				});
				
				view.tokenHtmlLoader.window.addEventListener('closeRegistration', function(event) {
					view.hideToken();
					if (!view.isUpdateRequest) {
						view.handledClose = true;
						window.nativeWindow.close();						
					}
				});

			}
			
			function tokenHtmlLoaderClose(event) {
				if (!view.handledClose) {	// If this isn't an explicitly handled close
					if (!view.isUpdateRequest) {	// and if it isn't an Update Token request
						air.NativeApplication.nativeApplication.exit();
					}
				}
			}
			
			view.tokenHtmlLoader.addEventListener("complete", tokenHtmlLoaderComplete);
			view.tokenHtmlLoader.parent.stage.nativeWindow.addEventListener("close", tokenHtmlLoaderClose);
			view.tokenHtmlLoader.load(initialUrl);
			
		},
		
		hideToken: function() {
			var view = this;

			if (view.tokenHtmlLoader == null) return;

            app.context.off(EventName.NETWORKSTATUSCHANGE, view.onNetworkStatusChange);

            view.handledClose = true;
			
			view.tokenHtmlLoader.window.nativeWindow.close();
			view.tokenHtmlLoader = null;
			view.viewShown = false;
		},
		
		render: function() {
			//console.log('ContainerTokenView.render()');
			
			var view = this;

			if (!view.viewShown) return;
			
			var tokenParams = {
				'registered' : view.isUpdateRequest
			};
			
			var tokenContent = app.render('tokenRegister', tokenParams);
			//alert("view : "+view);
			
			//alert("view.tokenHtmlLoader : "+view.tokenHtmlLoader);
			
			//alert("view.tokenHtmlLoader.window : "+view.tokenHtmlLoader.window);
			
			//alert("view.tokenHtmlLoader.window.injectContent : "+view.tokenHtmlLoader.window.injectContent);
			view.tokenHtmlLoader.window.injectContent(tokenContent);
			
			app.context.on(EventName.NETWORKSTATUSCHANGE, view.onNetworkStatusChange, this);
            _.defer(function() {
                view.onNetworkStatusChange(window.monitors.network.available);
            }, this);

			diary.view.ThemeUtil.renderThemeFromSchoolProfile(view);
		},
		
		processToken: function(event) {
			//alert("in process token");
			var view = this;
			
			var token = event.token;
			
			view.tokenHtmlLoader.window.displayMessage('');
			view.tokenHtmlLoader.window.disableForm();
		
			function processTokenSuccess() {
				//alert("processTokenSuccess");
				app.context.off(EventName.TOKENREGISTERED, processTokenSuccess);
				app.context.off(EventName.TOKENREGISTRATIONFAILED, processTokenFailed);
			
				view.tokenHtmlLoader.window.enableForm(view.internetAvailable);
				view.hideToken();
				
				app.context.off(EventName.NETWORKSTATUSCHANGE, view.onNetworkStatusChange);
				//alert("view.isUpdateRequest :"+view.isUpdateRequest);
                if (view.isUpdateRequest) {
                    app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.UPDATE_TOKEN, token);
                    app.context.trigger(EventName.RESETAPPLICATION);
                } else {
                    app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.REGISTER_TOKEN, token);
                    app.context.trigger(EventName.STARTAPPLICATION);
                }

            }
			
			function processTokenFailed(message) {
				//alert("processTokenFailed");
				app.context.off(EventName.TOKENREGISTERED, processTokenSuccess);
				app.context.off(EventName.TOKENREGISTRATIONFAILED, processTokenFailed);
			
				view.tokenHtmlLoader.window.displayMessage(message, true);
				view.tokenHtmlLoader.window.enableForm(view.internetAvailable);
			}
			//alert("processTokenSuccess : "+processTokenSuccess);
			app.context.on(EventName.TOKENREGISTERED, processTokenSuccess);
			app.context.on(EventName.TOKENREGISTRATIONFAILED, processTokenFailed);
			
			app.context.trigger(EventName.REGISTERTOKEN, event.token); 
		},
		
		dragView: function(event) {
			var view = this;
			view.tokenHtmlLoader.window.nativeWindow.startMove();
		},

		onNetworkStatusChange : function(available) {
			var view = this;
			view.internetAvailable = true;
			if (view.tokenHtmlLoader.window.displayMessage) {
				if (view.internetAvailable) {
					view.tokenHtmlLoader.window.displayMessage("", false);
				} else {
					view.tokenHtmlLoader.window.displayMessage("Please connect to the internet to continue registration", true);
				}
				view.tokenHtmlLoader.window.enableForm(view.internetAvailable);
			}
		}
	});

    view.REQUESTTYPE = {
        REGISTER: 0,
        UPDATE: 1,
        INVALID: 2
    };

	return view;

})();
