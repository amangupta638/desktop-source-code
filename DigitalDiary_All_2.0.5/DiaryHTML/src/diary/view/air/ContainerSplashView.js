diary = diary || {};
diary.view = diary.view || {};
diary.view.air = diary.view.air || {};

diary.view.air.ContainerSplashView = (function() {
	
	var view = Backbone.View.extend({

		viewShown: false,
		splashHtmlLoader: null,

		initialize: function() {
			//console.log('ContainerSplashView.initialize()');
			
			// Create a convenience view model as we are drawing information from two separate models
			this.model = new Backbone.Model({
				school: app.model.get(ModelName.SCHOOLPROFILE),
				student: app.model.get(ModelName.STUDENTPROFILE)
			});
			
			this.model.bind('change', this.render, this);
			diary.view.ThemeUtil.registerThemeListener(this);

			this.render();
		},
	
		showSplash: function(studentProfile) {
			
			var profileImage = null;
			var profileImageToDisplay = null;
			
			var isImg = false;
			
			//alert("showSplash");
			//console.log('ContainerSplashView.showSplash()');
			var view = this;
			view.handledClose = false;
			
			isImg = checkUserProfileImage();
			
			profileImage = studentProfile.get('profileImage');
			
			if (profileImage != null && profileImage != '' && isImg)
			{
				profileImageToDisplay = 'app-storage:/' + UserProfileImage.USERPROFILEIMAGEDIR + '/' + profileImage;
			}
			else
			{
				profileImageToDisplay = 'assets/splash/images/img005.png';
			}
			
			studentProfile.set('profileImageToDisplay', profileImageToDisplay);
			
			this.model.set("student", studentProfile)
			this.model.student = studentProfile;
			if (view.splashHtmlLoader != null) return;
			
			var windowInitOptions = new air.NativeWindowInitOptions();
			windowInitOptions.type = air.NativeWindowType.LIGHTWEIGHT;
			windowInitOptions.systemChrome = air.NativeWindowSystemChrome.NONE;
			windowInitOptions.transparent = true;
			
			var splashWindowWidth = air.Capabilities.screenResolutionX-430,
				splashWindowHeight = air.Capabilities.screenResolutionY-200,
				windowX = (air.Capabilities.screenResolutionX - splashWindowWidth) / 2,
				windowY = (air.Capabilities.screenResolutionY - splashWindowHeight) / 2,
				bounds = new air.Rectangle(windowX, windowY, splashWindowWidth, splashWindowHeight),
				initialUrl = new air.URLRequest("splash.html");
			
			view.splashHtmlLoader = air.HTMLLoader.createRootWindow(
				false,
				windowInitOptions,
				false,
				bounds
			);
			
			view.clippedMask = new au.com.productdynamics.view.ClippedRoundedWindow(view.splashHtmlLoader);

			view.splashHtmlLoader.window.nativeWindow.alwaysInFront = true;
			
			function splashHtmlLoaderComplete(event) {
				view.splashHtmlLoader.removeEventListener("complete", splashHtmlLoaderComplete);

				// Required for ThemeUtil
				view.document = view.splashHtmlLoader.window.document;
	
				view.viewShown = true;			
				view.render();
		
				view.splashHtmlLoader.window.nativeWindow.visible = true;	
				view.splashHtmlLoader.window.addEventListener('closeSplash', function(event) {
				
					view.hideSplash();
					if (!view.isUpdateRequest) {
						view.handledClose = true;
						window.nativeWindow.close();						
					}
				});
				
				view.splashHtmlLoader.window.addEventListener('startDrag', function(event) {
					//alert("drag");
					view.dragView.apply(view, [event]);
				});
				
			}
			
			view.splashHtmlLoader.addEventListener("complete", splashHtmlLoaderComplete);
			//alert("splash html loader"+initialUrl);
			view.splashHtmlLoader.load(initialUrl);
			
			
		},
		
		
		hideSplash: function() {
			var view = this;

			if (view.splashHtmlLoader == null) return;

			view.splashHtmlLoader.window.nativeWindow.close();
			view.splashHtmlLoader = null;
			view.viewShown = false;
		},
		
		render: function() {
			var view = this;
			if (!view.viewShown) return;
			
			var splashParams = {
				'school' : this.model.get('school').toJSON(),
				'student' : this.model.student.toJSON()
			};
			var splashContent = app.render('splash', splashParams);
			//alert("view.splashHtmlLoader : "+view.splashHtmlLoader);
			
			view.splashHtmlLoader.window.injectContent(splashContent);
			//alert("this.model.get('school').get('schoolImage') : "+this.model.get('school').get('schoolImage'));
            //view.splashHtmlLoader.window.injectLogo(this.model.get('school').get('schoolImage'), this.model.get('school').get('theme'));
			
			//console.log('School Background image = ' + this.model.get('school').get('schoolBgImage'));		// do not remove this console.log
			//console.log ('school image file is available = ' +  checkSchoolBgImage(this.model.get('school').get('schoolBgImage')));
			
			var isBgAvailable = false;
			
			var tryToCheckBgImage = function() {
				
				//console.log('IN tryToCheckBgImage ' +  isBgAvailable);
				
				isBgAvailable = checkSchoolBgImage(view.model.get('school').get('schoolBgImage'));
				//console.log('isBgAvailable = ' +  isBgAvailable);
				if (!isBgAvailable) 
				{
					//console.log ('checking background image again' );
					setTimeout(tryToCheckBgImage, 100);
				}
				else
				{
					view.splashHtmlLoader.window.injectLogo(view.model.get('school').get('schoolBgImage'), view.model.get('school').get('theme'));
		            //alert("view : "+view);
					diary.view.ThemeUtil.renderThemeFromSchoolProfile(view);
				}
			};
			
			setTimeout(tryToCheckBgImage, 100);
		},
		showprogress: function(element)
		{
		  var view = this;
            if (!view.viewShown) return;
            //view.splashHtmlLoader.window.injectProgress(element);
		},
		dragView: function(event) {
			var view = this;
			view.splashHtmlLoader.window.nativeWindow.startMove();
		},
		
	});

	return view;

})();
