diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};
var editor,process;

diary.view.assignDiaryItemView = (function() {

	var _MODE = {
			'UPDATE' : 1, 
			'CREATE' : 2, 
			'READONLY' : 3
	};

	var view = Rocketboots.view.View.extend({

		events: {
		"change .tableFieldLayout .type"			: "updateDiaryItemType",
		"change .tableFieldLayout .subject"			: "resetDefaultTimeFields",
		"change .tableFieldLayout .allDay"			: "updateVisibleTimeFields",
		"change .startTimeRow select"				: "updateOrDefaultEndTime",
		"change .attachmentsRow .attachment"		: "addAttachment",
		"click .attachmentList .attachmentRemove"	: "removeAttachment",
		"click .editButtonContainer .cancel"		: "closeDialog",
		"click .editButtonContainer .delete"		: "deleteDiaryItem",
		"click .editButtonContainer .save"			: "saveDiaryItem",
		"dblclick ul.attachments > li.attachment"	: "openAttachment",
		"blur .taskTable input"						: "updateSliderValue",
		"click .radio"								: "assignTask"
		
		
	},

	initialize: function(options, view) {
		
		var view = options;
		
		this.render(view);
	},

	parseAdditionalParams: function(options) {
		
	},

	render: function(viewPopUp) {
	

		var view = this;

		var minutes = [];
		var i = 0;
		var minute;
		while(i < 60){
			minute = {'minute' : i};
			minutes.push(minute);
			i++;
		}

		var hours = [];
		var j = 0;
		var hour;
		while(j < 12){
			hour = { 'hour' : j};
			hours.push(hour);
			j++;
		}
		var viewParams = {
				"classes" : app.model.get(ModelName.CLASSES).toJSON()
		}

		
		
		var content = app.render('assignDiaryItemView', viewParams);
		
		$(viewPopUp.el).html(content);


	}, 

	

	});

	return view;

})();