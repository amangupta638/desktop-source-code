diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};
var isOpenPopup = false;
var popupOnDay = "";
var selectedDivPosition = 0;
diary.view.DiaryMonthView = (function() {

	var MAX_EVENTS_NO_TASKS = 3;
	var MAX_EVENTS_WITH_TASKS = 1; // hide 2 more events, leaving room for task widgets
	var MAX_TASKS = 2;
	
	var view = Rocketboots.view.View.extend({

		events: {
			"click .monthView .monthEvents li"			: "showDiaryItemForm",
			"click .monthView .summarymultiplewidget"		: "showTasksInPopup",		// pop-up for multiple subjects
			"dblclick .monthView .summarymultiplewidget"	: "stopDoubleClickSummary",	// stop the double click of the summary
			"click .monthView .subjectwidget"				: "showTasksInPopup",		// pop-up for single subject
			"dblclick .monthView .subjectwidget"			: "showDiaryItemForm",		// pop-up the edit for single subject
			"click .monthView .calendarDateSelect"			: "selectMonth",			// month selector
			"click .monthView .calendarNav li"				: "changeMonth",			// previous/next month
			//"click .monthView .monthViewDay .daycell"	 	: "selectDay",				
			"click .monthView .monthViewDay .daycell" 		: "navigateToDay",			// navigate to a particular day
			"dragstart .monthView .subjectWidget"			: "startDragWidget",			// start dragging a diary item
			"onload"										: "renderMonthView",
			"click .jTscroller .ts2Ancher"					: "monthSelect",
			"click .studCalendarAccess"						: "accessStudCalendar"
		},
		
		initialize: function() {
			//console.log("INITIALIZE MONTH VIEW");
			GetStartDate();
			Rocketboots.view.View.prototype.initialize.call(this);
			
			var view = this,
				updateMonth = function() {
				//console.log("DiaryMonthView="+app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'));
					var selectedMonth = app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay').getMonth();
					//console.log("view.currentMonth="+view.currentMonth);
					if (view.currentMonth != selectedMonth) {
						view.currentMonth = selectedMonth;
						view.isDirty = true;
					}
					view.mapMonth();
				},
				updateCalendar = function() {
					////console.log('DiaryMonthView updateCalendar()');
					view.mapMonth();
					view.isDirty = true;
					view.render();
				};
			
			////console.log('DiaryMonthView.initialize()');
			
			//app.model.get(ModelName.APPLICATIONSTATE).on("change:" + 'selectedDay', updateMonth, this);
			app.listenTo(app.model.get(ModelName.APPLICATIONSTATE), "change:" + 'selectedDay', updateMonth);
			//app.model.on("change:" + ModelName.CALENDAR, updateCalendar, this);
			app.listenTo(app.model, "change:" + ModelName.CALENDAR, updateCalendar);
			
			// listen to the event so that we would know when to refresh the list of diary items that are being displayed
			// NOTE - The Diary View (the base class of all calendar views) is listening to the three different events that
			// are triggered by the diary item collection
			app.context.on(EventName.REFRESHDIARYITEMS, updateCalendar, this);
			
			view.isAlwaysDirty = true;
			this.dropTargetHtml = null;
			
			this.month = {};
			updateMonth();
			
			// render the screen
			this.render();
	
			this.dropTarget = new diary.view.DropTarget({
				el: $(".monthViewCalendar", this.$el)
			}).register(
				this.dragEnterMonthView, 
				this.dragLeaveMonthView, 
				this.dropOnMonthView,
				null,
				this
			);
						
			view.dragItem = {
				'item': null, 
				'fromDay': null,
                'isDue' : false
			};

			this.popupDropTarget = new diary.view.DropTarget({
				el: $("#monthPopup", this.$el)
			}).register(
				this.dragEnterMonthPopup, 
				this.dragLeaveMonthPopup, 
				this.dropOnMonthPopup,
				null, 
				this
			);
		},

		parseAdditionalParams: function(params) {
			if (typeof(params.el) !== "undefined") {
				this.el = params.el;
			}
			/*
			if (typeof(params.monthToView) != "undefined") {
				this.options.monthToView = params.monthToView;
				this.currentMonth = this.options.monthToView - 1;
				////console.log('setting currentMonth=' + this.currentMonth);
				
				this.mapMonth();
			}
			*/
		},
		
		setCurrentMonth: function(monthIndex, dayIndex, year) {
		//alert("app.model.get(ModelName.CALENDAR).get('year')="+app.model.get(ModelName.CALENDAR).get('year'));
		var yeartoshow=yearArray[monthIndex];
		//console.log("setCurrentMonth monthIndex="+monthIndex);
			app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(parseInt(year), monthIndex, dayIndex ? dayIndex : 1));
		},
		
		accessStudCalendar : function(event) {
			
			openCalendarAccess();
		},
		
		mapMonth: function() {
			
			
			var startDate = new Rocketboots.date.Day(app.model.get(ModelName.APPLICATIONSTATE).get('startDate'));
			var selectedDay = new Rocketboots.date.Day(app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'));
			var today = new Rocketboots.date.Day();
			
			if((today.getTime() < startDate.getTime()) && (selectedDay.getTime() < startDate.getTime())){
				this.currentMonth = startDate.getMonth();
			}
			
			////console.log("mapping calendar");
			
			// get the calendar from the application model
			////console.log("mapping calendar");
            
            // get the calendar from the application model
            var calendarModel = app.model.get(ModelName.CALENDAR);
           // console.log("calendarModel");
            calendarModel.attributes.year=selectedDay.getFullYear();//SystemSettings.HARDCODDEDYEAR;
            //console.log(calendarModel);
            // get all the diary items for the calendar
            var diaryItemsModel = app.model.get(ModelName.DIARYITEMS).getDiaryItems();
            var month=this.currentMonth;
           // console.log("month="+month);
            var monthname=monthArray[month];
           // console.log("monthname="+monthname);
           var monthIndex=$.inArray(monthname, monthNames)+1;
          // console.log("monthIndex="+monthIndex);
           this.currentMonth=month;
            // update the generated month
            this.month = diary.view.DiaryCalendarViewUtil.mapMonth(calendarModel, diaryItemsModel, month+1, true);
			
			
			// for each day, ensure no more than 2 tasks are in the task list. All remaining tasks should go in the summary.
			_.forEach(this.month.days, function (day) {
				
				var extraTasks;
				
				if (day.tasks.length >0) {
					
					// remove all but the first task from the list
					extraTasks = day.visibleitems;
					
					// construct a summary object
					day.summary = {
						'count' : extraTasks.length,
						'extraTasks' : extraTasks,
						'extraTaskIds' : ""
					};
					
					// create list of extra task ids
					_.forEach(day.summary.extraTasks, function (task) {
						day.summary.extraTaskIds += task.id + ",";
					});
					
					// override original task ids
//					day.diaryItemIds = "";
//					_.forEach(day.tasks, function (task) {
//						day.diaryItemIds += task.id + ",";
//					});
					day.diaryItemIds = day.summary.extraTaskIds;
					
				} else {
					// remove summary
					day.summary = false;
				}

				// limit the number of events displayed
				var limitEventsTo = function (day, maxEvents) {
					while ((day.events ? day.events.length : 0) > maxEvents) {
						if (day.events.length > 0) {
							day.events.pop();
						} /* else if (day.dayOfNotes.length > 0) {
							day.dayOfNotes.pop();
						} */
					}
				};
				// when displaying any tasks, only allow 1 event/dayOfnote; otherwise allow 3
				//limitEventsTo(day, day.tasks.length > 0 ? MAX_EVENTS_WITH_TASKS : MAX_EVENTS_NO_TASKS);
				limitEventsTo(day, MAX_EVENTS_NO_TASKS);

			});

		},
		
		render: function() {
			//alert("MONTH render")
			var updateMonthCalendar = function() {
				view.isDirty = true;
				view.render();
			};
			app.model.get(ModelName.DIARYITEMS).off("change");
			app.model.get(ModelName.DIARYITEMS).off("remove");
			app.model.get(ModelName.DIARYITEMS).off("add");
			app.model.get(ModelName.DIARYITEMS).on("change", updateMonthCalendar, this);
			app.model.get(ModelName.DIARYITEMS).on("remove", updateMonthCalendar, this);
			app.model.get(ModelName.DIARYITEMS).on("add", updateMonthCalendar, this);
			
			this.mapMonth();
				//this.renderDiaryItemForm();
			//console.log("RENDER MONTH VIEW");
			//alert("SystemSettings.HARDCODDEDYEAR="+SystemSettings.HARDCODDEDYEAR)
			var now=new Date();
			//console.log("this.currentMonth="+this.currentMonth);
			/*var startdate=app.model.get(ModelName.APPLICATIONSTATE).get('startDate');
					var startDay2=new Rocketboots.date.Day(startdate);
					//console.log("startDay2="+startDay2.getMonth());
					if(this.currentMonth<startDay2.getMonth())
					{
						this.currentMonth=startDay2.getMonth()
					}*/
			var view = this,
				calendarModel = app.model.get(ModelName.CALENDAR),
				today = new Rocketboots.date.Day(),
				isYear = today.getFullYear() == SystemSettings.HARDCODDEDYEAR,
				dateLabel = getCurrentDate(),
				dateRange =getDateRange(this.currentMonth);
			//alert("isYear="+isYear);
			// Mustache is "logic-less".  Therefore the view must work out all the conditional logic before Mustache is invoked
			var viewParams = {
				'disableToday': !isYear,
				'dateNow': dateLabel,
				'dateRange': dateRange,
				'isTeacher'	: app.model.get(ModelName.STUDENTPROFILE).get("isTeacher"),
				'month': this.month
			};
			
			
			
			var startTime = new Date().getTime();
			var content = app.render('diaryMonth', viewParams);
			//$(".cal_filter_task").show();
			var duration = new Date().getTime() - startTime;
			////console.log('Month View Mustache Render Time: ', duration, ' length: ', content.length);
			//console.log("MONTH VIEW PARAMS");
			//console.log(viewParams);
			$(view.el).html(
				content
			);
						
			//$("#monthPopupMonthList", view.el).on("click", function(event) {
			$(".jTscroller .ts2Ancher").on("click", function(event) {
				console.log("jTscroller click : "+ $(event.target).index());
				var monthNumber = $(event.target).index();
				//console.log("monthPopupMonthList MONTHNUMBER="+monthNumber);
				//console.log(monthNames[monthNumber]);
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "MONTH", monthNumber);
				if(!isSingleYear)
				{
					monthNumber=monthIndexArray[monthNumber];
					//console.log("monthNumber="+monthNumber);
					
					/*if(monthNumber<=6)
					{
						monthNumber=monthNumber+7;
					}
					else if(monthNumber==7)
					{
						monthNumber=0;
						
					}
					else if(monthNumber>6)
					{
						monthNumber=monthNumber%7;
					}*/
				}
				view.setCurrentMonth(monthNumber);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			});
			
			//$(".cal_tbw_right .cal_filter_task").show();
			
			if (view.dropTarget) {
				view.dropTarget.setElement( $(".monthViewCalendar", view.$el) );
			}
			
			if (view.popupDropTarget) {
				view.popupDropTarget.setElement( $("#monthPopup"), this.$el);
			}
			
			view.selectMonth();
			if(selectedMonthView != ""){
				selectedDivPosition = 0;
				$("#diaryMonthDatePicker").find(".active").removeClass("active");
				$(".weekfromto").each(function(){
					selectedDivPosition = selectedDivPosition + 80;
					if($(this).attr("id") == selectedMonthView){
						$(this).parent().addClass("active");
      					$("#containerMonthList").scrollLeft(selectedDivPosition);
					}
					
				});
			}
		},
		
		renderDiaryItemForm: function() {
			var view = this, 
				viewParams = {};		
			
			viewParams.el = $('#monthDiaryItemFormEditPopup', this.el);
			viewParams.editorref="editor5";
			viewParams.classes="monthDiaryItemFormEditPopup";
			viewParams.onChange = function() {
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			};
			
			
			view.diaryItemForm = new diary.view.DiaryItemView(viewParams);
		},
		
		showTasksInPopup: function(event) {
			//console.log("showTasksInPopup");
			
			var view = this,
				// get the identifier of the diary item that we need to display
				diaryItemIds = $(event.currentTarget).attr('diaryItemIds');
			
			if (diaryItemIds) {
				view.showDiaryItemPopup(diaryItemIds, $(event.currentTarget));
				event.stopPropagation();
			}
			
		},
		
		showDiaryItemPopup: function(diaryItemIds, $anchor) {
			var view = this, 
				// get the diary items
				diaryItemsModel = app.model.get(ModelName.DIARYITEMS),
                dayKey = $anchor.closest('.daycell').attr('daykey');
			
			
			// render the content of the popup
			var popupContent = app.render('diaryItemsSummary',
                                          {'items' : diary.view.DiaryCalendarViewUtil.mapTasksForPopup(diaryItemsModel,
                                                                                                       diaryItemIds,
                                                                                                       Rocketboots.date.Day.parseDayKey(dayKey)) });
					
			app.popup.hide($("#monthPopup"));	
		
			if(!(popupOnDay == dayKey) || !(isOpenPopup)){
				$("#monthPopup>div.popupContent").html(popupContent);
				app.popup.show( $("#monthPopup"), $anchor, "right", null, event, $(".monthViewCalendarBodyWrapper", view.el));
				isOpenPopup = true;
				popupOnDay = dayKey;
			}	
			else{
				app.popup.hide($("#monthPopup"));	
				isOpenPopup = false;
				popupOnDay = "";
			}
			
			
			
			$("#monthPopup .diaryItemSummary .infolink", this.el).on("click", function(event) {	
							
				view.showDiaryItemForm(event);
			});
			
			$(".webLinkDiaryItem", this.el).on("click", function(event) {				
				var link = prepareWeblinkURL($(event.currentTarget).attr('webLinkURL'));
				var urlReq = new air.URLRequest(link); 
				air.navigateToURL(urlReq);
			});
			
			$("#monthPopup .diaryItemSummary .noteEntry .save", this.el).on("click", function(event) {
				// get the note that was entered
				var noteEntryElement = $(event.currentTarget).closest("#monthPopup .diaryItemSummary .noteEntry").find(".noteEntry"),
					noteId = parseInt(noteEntryElement.attr("noteId"), 10),
					noteText = noteEntryElement.val().trim();
				
				// append the note
				diary.view.DiaryView.appendNote(noteId, noteText);
				app.popup.hide($("#monthPopup"));
				openMessageBox("Diary item updated successfully.");
			});
			
			$("#monthPopup .diaryItemSummary .noteEntry .delete", this.el).on("click", function(event) {
            // get the note that was entered
            var hel = confirm("Are you sure to delete this note ?");
		
				if(hel)
				{
           			var noteEntryElement = $(event.currentTarget).closest("#monthPopup .diaryItemSummary .noteEntry").find(".noteEntry"),
                	noteId = parseInt(noteEntryElement.attr("noteId"), 10),
                	noteText = noteEntryElement.val().trim();
            		DeleteNoteEntryMethod(noteId);
           			app.popup.hide($("#monthPopup"));
            	}
            	else
            		return;
            
            
        	});
			
			$("#monthPopup .diaryItemSummary ul.attachments > li.attachment").on("dblclick", view.openDiaryItemAttachment);
			
			if (view.popupDropTarget) {
				view.popupDropTarget.setElement( $("#monthPopup"), view.$el);					
			}
			$("#monthPopup .diaryItemSummary .summaryWidgetItem").on("dragstart", function(event) {
				view.startDragWidget(event);
			});

		},
		
		hideDiaryItemPopup: function() {
			app.popup.hide($("#monthPopup"));			
		},

				
		
		selectMonth: function(event) {
			var popupParams = {
					'months': []
				},
			now = new Date();
            var index=parseInt(now.getMonth())
            var month=monthNames[index];
           // //console.log("month="+month);
            var arr=diary.view.DiaryCalendarViewUtil.months;
            var monthIndex=$.inArray(month, arr);
           // //console.log("indexofvalue="+$.inArray(month, arr));
            
            var startDate = new Rocketboots.date.Day(app.model.get(ModelName.APPLICATIONSTATE).get('startDate'));
            var year = startDate.getDate().getFullYear();
            for(var i=0; i<12; i++) {
                popupParams.months.push(
                    {
                        month: diary.view.DiaryCalendarViewUtil.months[i],
                        index: i,
                        year:year,
                        thisMonth: (monthIndex == i && now.getFullYear() == SystemSettings.HARDCODDEDYEAR)
                    }
                );
                
                if(diary.view.DiaryCalendarViewUtil.months[i].toLowerCase() == 'december'){
            		year = year+1;
            	}
            }
			
			
			var popupContent = app.render('monthList', popupParams);
			
			//app.popup.hide($("#monthPopupMonthList"));			
			//$("#monthPopupMonthList>div.popupContent").html(popupContent);
			
			$("#diaryMonthDatePicker", this.el).html(popupContent);
			
			//app.popup.show( $("#monthPopupMonthList"), $(event.currentTarget), "right", null, event );
			
		},

		changeMonth: function(event) {
			var $element = $("div", event.currentTarget);
			if ( $element.hasClass("navLeft") ) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_PREVIOUS, "MONTH");
				this.previousMonth();
			}
			else if ( $element.hasClass("navRight") ) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_NEXT, "MONTH");
				this.nextMonth();
			}
			else if ( $element.hasClass("navToday") && !$element.hasClass('disabled')) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "MONTH", "TODAY");
				this.todaysMonth();
			}
		},
		
		previousMonth: function() {
		
		   var view = this;
		   //console.log("previousMonth view.currentMonth="+view.currentMonth)
			if(!isSingleYear)
			{
			if (view.currentMonth > 0) {
				
				if (view.currentMonth==monthIndexArray[0])
				    return;
				console.log(view.currentMonth - 1);
				view.setCurrentMonth(view.currentMonth - 1);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
			else if ( view.currentMonth==0)
			{   view.setCurrentMonth(11);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
				
			}
			}
			else
			{
				if (view.currentMonth > 0) {
				view.setCurrentMonth(view.currentMonth - 1);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
			}
			view=null;
			
	
		},
		
		nextMonth: function() {
			
			
			var view = this;
			//console.log("nextMonth view.currentMonth="+view.currentMonth)
			if(!isSingleYear)
			{
			
			if (view.currentMonth < 11) {
					if(view.currentMonth == monthIndexArray[monthIndexArray.length-1]) 
					return;
				view.setCurrentMonth(view.currentMonth + 1);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
			
			else if(view.currentMonth == 11) {
				view.setCurrentMonth(0);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
			}
			else
			{
				if (view.currentMonth < 11) {
				view.setCurrentMonth(view.currentMonth + 1);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
			}
			view=null;
		},
		
		todaysMonth: function() {
			var view = this, 
				now = new Date();
                var index=parseInt(now.getMonth())
                var month=monthNames[index];
               // //console.log("month="+month);
                var arr=diary.view.DiaryCalendarViewUtil.months;
                var monthIndex=$.inArray(month, arr);
                var year=yearArray[monthIndex];
			now = new Date();
		    var dateIndex=now.getDate();
            var index=parseInt(now.getMonth())
            var month=monthNames[index];
            var monthIndex=$.inArray(month, monthArray);
			app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(yearArray[monthIndex], index, dateIndex));
			//view.setCurrentMonth(monthIndex, now.getDate());
			// call the refresh calendar so that the calendar will be refreshed
			app.context.trigger(EventName.REFRESHCALENDAR);
			view=null;
		},
		
		selectDay: function(event) {
			diary.view.DiaryView.selectDay(event);
		},
		
		navigateToDay: function(event) {
			app.model.get(ModelName.APPLICATIONSTATE).set('setToday', true);
			diary.view.DiaryView.selectDay(event);
			diary.view.DiaryView.navigateToDay(event);
		},
		
		showDiaryItemForm: function(event) {
			console.log("showDiaryItemForm");
			var diaryItemId = $(event.currentTarget).attr('diaryItemId');
			
			if(diaryItemId)
			{
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "SELECT UserID,createdBy,type FROM diaryItem WHERE id = :diaryItemId",
	
	            // params
	            {
				'diaryItemId' :diaryItemId
	            },
				// success
				function(statement) {
				
					var detail=_.map(statement.getResult().data, function(item) {
	                        return item;
	                    });
	                    
	                 if(detail.length)
	                 {
	                 		var userID=detail[0].UserID;
	                 		var createdBy=detail[0].createdBy;
	                 		var type=detail[0].type;
	                 		diaryItemId && diaryItemForm.showDialog(diaryItemId,'','','','',createdBy);
	                 		if(createdBy!=null && userID==createdBy)
	                 		{
	                                   $(".editButtonContainer .save").show();
	                                   $(".ui-dialog-titlebar").hide();
	                                   $(".editButtonContainer .lockItem").hide();
	                                   $(".editButtonContainer .delete").show();
	                 		}
	                 		else
	                 		{
	                           	disablediaryitemformDay();
	                           	if(type == 'EVENT'){
	                           		$(".editButtonContainer .save").hide();
	                            } else {
	                            	$(".editButtonContainer .save").show();
	                            }
	                 			$(".editButtonContainer .delete").hide();
	                 			$(".ui-dialog-titlebar").hide();
	                 			$(".editButtonContainer .lockItem").show();
	                 		}
	                 }
	            },
				
				// error
				function error()
				{
					//console.log("Error while trying to create ClassUser item");
				}
				);
			}
			event.stopPropagation();
		},
		
		openDiaryItemAttachment: function (event) {
			var target = $(event.target),
				diaryItemId = target.closest("article").find(".infolink").attr('diaryItemId'),
				attachmentId = target.closest(".attachment").attr('attachmentId'),
				diaryItem = null,
				attachment = null;
			
			if (diaryItemId && attachmentId) {
				diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId);
				attachment = diaryItem.get('attachments').get(attachmentId);
				if (diaryItem && attachment) {
					app.context.trigger(EventName.OPENATTACHMENT, diaryItem, attachment);
				}
			}
			event.stopPropagation();
		},
		
		stopDoubleClickSummary: function(event) {
			event.stopPropagation();
		},
		
		/*
		 * Drag and drop
		 */
		
		clearDropTarget: function() {
			var view = this,
				$target = null;

			if (view.dropTarget.target != null) {
				if (view.dropTarget.type == "day") {
					$target = $(".monthViewDay div.daycell[dayKey=\"" + view.dropTarget.target + "\"]", view.$el);
				} else if (view.dropTarget.type == "diaryitem") {
					$target = $(".subjectwidget[diaryItemId=\"" + view.dropTarget.target + "\"]", view.$el);
				}
				$target && $target.removeClass("dropTarget");
				
				// At some point, AIR decides to start dispatch DragLeave events prior to the Drop. So we need to clear the selected day,
				// but retain a reference to it so it can be used by the Drop that immediately follows
				view.dropTarget.active = false; 
			}			
		},
		
		setDropTarget: function($target, target, type, active) {
			var view = this;
			
			view.dropTarget.setTarget(target, type, active);
			$target.addClass('dropTarget');
			
		},
		
		setDragItem: function(diaryItem, isDue, fromDay) {
			// TODO this is not the cleanest solution for dragging diary items,
            // take a look at the day view for a much neater solution. The
            // drag code for this view will eventually have to be updated
            // to make it cleaner
            var view = this;
			view.dragItem.item = diaryItem;
			view.dragItem.fromDay = fromDay;
            view.dragItem.isDue = isDue;
		},		

		/*
		 * Dragging 
		 */
		dragEnterMonthView: function(event) {
			
			var view = this,
				$target = $(event.target),
				isDiaryItemDrop = event.dataTransfer.types == null,
				isFileDrop = view.dropTarget.isFileDrop(event),		
				$diaryItem = $target.closest('.subjectwidget, .summarymultiplewidget'),
				$monthViewDayCell = $target.closest('.monthViewDay'),
				$dayCell = $monthViewDayCell.find('.daycell');

			var overTarget = false;
			
			if ($diaryItem.length > 0 && isFileDrop) { // we are dragging over a diary item
				overTarget = view.dragEnterDiaryItem(event, $diaryItem, isDiaryItemDrop, isFileDrop);			
			} else if ($dayCell.length > 0) { // Dragging over a day
				view.hideDiaryItemPopup();
				overTarget = view.dragEnterMonthDay(event, $dayCell, isDiaryItemDrop, isFileDrop);
			}
						
			if (!overTarget && view.dropTarget.target) {	
				view.clearDropTarget();
				view.dropTarget.reset();
				event.dataTransfer.dropEffect = 'none';
			}
		},
		
		dragEnterDiaryItem: function(event, $diaryItem, isDiaryItemDrop, isFileDrop) {
			var view = this, 
				overTarget = false,
				diaryItemId  = $diaryItem.attr('diaryItemId'),
				diaryItemIds = $diaryItem.attr('diaryItemIds') || "";
			
			if (isFileDrop) {
				if (diaryItemIds.split(',').length > 1) {
					// Dragging over a multiple diary item
					// open the popup
					view.showDiaryItemPopup(diaryItemIds, $diaryItem);
					view.clearDropTarget();
					overTarget = false;					
				} else {
					// Dragging over a single diary item
					view.hideDiaryItemPopup();
					if (view.dropTarget.type != "diaryitem" || view.dropTarget.target != diaryItemId) {
						// Dragging over this diary item for the first time						
						view.clearDropTarget();
						view.setDropTarget($diaryItem, diaryItemId, "diaryitem", true, null);
						overTarget = true;
					} else if (view.dropTarget.type == "diaryitem" && view.dropTarget.target == diaryItemId) {
						// Dragging over this already highlighted diary item 
						overTarget = true;
					}
				}
			} // else if isDiaryItemDrop, do nothing as you can't drop a diary item over a diary item
			
			return overTarget;
		},
		
		dragEnterMonthDay: function(event, $dayCell, isDiaryItemDrop, isFileDrop) {
			var view = this, 
				overTarget = false,
				dayKey = $dayCell.attr("dayKey");
		
			if (typeof(dayKey) != "undefined") {
			
				if (isDiaryItemDrop && dayKey == view.dragItem.fromDay.getKey()) {
					// Can't drop over the same day
					if (view.dropTarget.target) {
						view.clearDropTarget();
						view.dropTarget.reset();						
					}
					overTarget = false;
				} else if ((isDiaryItemDrop && dayKey != view.dragItem.fromDay.getKey()) || isFileDrop) {
					if (dayKey != view.dropTarget.target || !view.dropTarget.active) {
						view.clearDropTarget();
						view.setDropTarget($dayCell, dayKey, "day", true);					
						event.dataTransfer.dropEffect = 'copy';
						overTarget = true;
					} else {
						// Already over the target
						////console.log("we are already over the target");
						overTarget = true;
					}
				}
			}
			return overTarget;
		},
		
		dragLeaveMonthView: function(event) {
			var view = this;
			
			// Check if the mouse is still within the container
			var container =  $(".monthViewCalendar", this.$el),
				position = $(event.currentTarget).offset();
					
			var isStillOverContainer = event.x >= position.left && event.y >= position.top && event.x <= (position.left + container.outerWidth()) && event.y <= (position.top + container.outerHeight());
			if (isStillOverContainer) return;
			
			// If outside the container, then clear any current drop target
			view.clearDropTarget();	
			view.dropTarget.reset();
		},
		
		
		/* 
		 * Dropping
		 */
		
		dropOnMonthView: function(event) {
			var view = this,
				isDiaryItemDrop = event.dataTransfer.types == null,
				isFileDrop = view.dropTarget.isFileDrop(event);				

			////console.log(view.dropTarget.target);
			
			if (isDiaryItemDrop && view.dropTarget.target) {
				////console.log('Month View: Drop targetDay=' + view.dropTarget.target + ' originalDay:' + view.dragItem.fromDay.getKey());
				if (view.dropTarget.target && view.dragItem.fromDay.getKey()) {
					view.dropOnMonthDay(event, isFileDrop);
				}
			} else if (isFileDrop) {
				////console.log('Month View: Drop target=' + view.dropTarget.target + ' from File');			
				if (view.dropTarget.target && view.dropTarget.type == "diaryitem") {
					view.dropOnDiaryItem(event, isFileDrop);
				} else if (view.dropTarget.target && view.dropTarget.type == "day") {
					view.dropOnMonthDay(event, isFileDrop);
				}
			}
			
			view.clearDropTarget();	
			view.dropTarget.reset();
		},
				
		dropOnMonthDay: function(event, isFileDrop) {
			var view = this;
						
			////console.log('Dropping onto month day ' + view.dropTarget.target);
			
			if (isFileDrop) {
				var files = event.dataTransfer.getData('application/x-vnd.adobe.air.file-list'),
					attachmentsToAdd = _.map(
						_.filter(files, function(file) { return !file.isDirectory; }), 
						function(file) {
							return file.nativePath;
						}
					),
                    calendarDay,
					defaultValues = {};
				
				if (attachmentsToAdd.length == 0) return;

                calendarDay = Rocketboots.date.Day.parseDayKey(view.dropTarget.target);
									
				// If there is a single attachment, derive the title from it
				if (attachmentsToAdd.length == 1) {
					var fileProperties = diary.view.air.ViewFilesystemUtil.getFileProperties(attachmentsToAdd[0]);
							
					if (fileProperties.title) {
                        defaultValues.title = fileProperties.title;
					}
					if (fileProperties.description) {
                        defaultValues.description = fileProperties.description;
					}				
				}
					
				diaryItemForm.showDialog(null,
                                              calendarDay,
                                              null,
                                              null,
                                              defaultValues,
                                              attachmentsToAdd);
			} else {
				app.context.trigger(EventName.UPDATEDIARYITEMONDROP,
                                    view.dragItem.item,
                                    view.dragItem.isDue,
						            Rocketboots.date.Day.parseDayKey(view.dropTarget.target));
			}
					
		},
		
		dropOnDiaryItem: function(event, isFileDrop) {
			var view = this;
			
			if (!isFileDrop) return;
			
			var files = event.dataTransfer.getData('application/x-vnd.adobe.air.file-list'),
				attachmentsToAdd = _.map(
					_.filter(files, function(file) { return !file.isDirectory; }), 
					function(file) {
						return file.nativePath;
					}
				);
			
			if (attachmentsToAdd.length == 0) return;

			diaryItemForm.showDialog(view.dropTarget.target,
                                          null,
                                          null,
                                          null,
                                          null,
                                          attachmentsToAdd);

		},
		
		startDragWidget: function(event) {
			var view = this;
			////console.log('MonthView: startDragWidget');
			
			// TODO: Handle multiple diary item widget
			
			////console.log(event.target.outerHTML);
			
			var isDue = $(event.originalEvent.target).hasClass('due'),
                diaryItemId = $(event.originalEvent.target).attr('diaryItemId');
			
			if (typeof(diaryItemId) == "undefined" || diaryItemId == null || diaryItemId == "") {
				event.originalEvent.dataTransfer.effectAllowed = "none";
				return;
			}
			
			var diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId),
				fromDay = null;

            if (diaryItem.get('locked')) { // although only events will ever be locked and you can't drag events
                event.originalEvent.dataTransfer.effectAllowed = "none";
                return;
            }


            if (diaryItem instanceof diary.model.Note || diaryItem instanceof diary.model.Message) {

                fromDay = diaryItem.get('atDay');

            } else if (diaryItem instanceof diary.model.Task) {

                if (isDue) {
                    fromDay = diaryItem.get('dueDay');
                } else {
                    fromDay = diaryItem.get('assignedDay');
                }

            } else if (diaryItem instanceof diary.model.Event) {

                fromDay = diaryItem.get('startDay');

            } else {

                throw new Error ("Diary Item is not supported");

            }

			////console.log(JSON.stringify(diaryItem), fromDay);
			this.setDragItem(diaryItem, isDue, fromDay);
			
			event.originalEvent.dataTransfer.effectAllowed = "move";

			////console.log('MonthView: startDragWidget id:' + diaryItem.get('id') + ' fromDay:' + fromDay + ' moveable:' + diaryItem.get('moveable'));

			// setData serialises the value rather than passing by reference.  So we need to pass simple values around
			/*
			event.originalEvent.dataTransfer.setData("diary/diary-item-id", diaryItemId);
			event.originalEvent.dataTransfer.setData("diary/diary-item-date-key", dateKey.toString());
			event.originalEvent.dataTransfer.setData("text/plain", "diaryitem://" + diaryItemId + "/" + dateKey);
 			*/
		}, 
		
		/*
		 * Drag and drop over popup
		 */
		
		clearPopupDropTarget: function() {
			var view = this,
			$target = null;

			if (view.popupDropTarget.target != null) {
				$target = $("#monthPopup .diaryItemSummary[diaryItemId=\"" + view.popupDropTarget.target + "\"]", view.$el);
				$target && $target.removeClass("dropTarget");
				
				// At some point, AIR decides to start dispatch DragLeave events prior to the Drop. So we need to clear the selected day,
				// but retain a reference to it so it can be used by the Drop that immediately follows
				view.popupDropTarget.active = false; 
			}			
		},
		
		setPopupDropTarget: function($target, target, type, active) {
			var view = this;
			
			view.popupDropTarget.setTarget(target, type, active);
			$target.addClass('dropTarget');
		},
		
		dragEnterMonthPopup: function(event) {
			var view = this,
			$target = $(event.target),
			isFileDrop = view.popupDropTarget.isFileDrop(event),		
			$diaryItem = $target.closest('.diaryItemSummary');
						
			var overTarget = false;
			
			if ($diaryItem.length > 0 && isFileDrop) { // we are dragging over a diary item
				var diaryItemId  = $diaryItem.attr('diaryItemId');
				
				if (isFileDrop) {
					if (view.popupDropTarget.type != "diaryitem" || view.popupDropTarget.target != diaryItemId) {
						// Dragging over this diary item for the first time						
						view.clearPopupDropTarget();
						view.setPopupDropTarget($diaryItem, diaryItemId, "diaryitem", true, null);
						overTarget = true;
					} else if (view.popupDropTarget.type == "diaryitem" && view.popupDropTarget.target == diaryItemId) {
						// Dragging over this already highlighted diary item 
						overTarget = true;
					}
				}
			} // else if isDiaryItemDrop, do nothing as you can't drop a diary item over a diary item
			
			if (!overTarget && view.popupDropTarget.target) {	
				view.clearPopupDropTarget();
				view.popupDropTarget.reset();
				event.dataTransfer.dropEffect = 'none';
			}
		},
		
		dragLeaveMonthPopup: function(event) {
			var view = this;
						
			// Check if the mouse is still within the container
			var container =  $("#monthPopup", this.$el),
				position = $(event.currentTarget).offset();
					
			var isStillOverContainer = event.x >= position.left && event.y >= position.top && event.x <= (position.left + container.outerWidth()) && event.y <= (position.top + container.outerHeight());
			if (isStillOverContainer) return;
			
			// If outside the container, then clear any current drop target
			////console.log("leaving month popup");
			view.clearPopupDropTarget();	
			view.popupDropTarget.reset();
			view.hideDiaryItemPopup();
		},
		
		dropOnMonthPopup: function(event) {
			var view = this,
				isFileDrop = view.popupDropTarget.isFileDrop(event);				

			if (isFileDrop) {
				////console.log('Month View: Drop target=' + view.popupDropTarget.target + ' from File');			
				if (view.popupDropTarget.target && view.popupDropTarget.type == "diaryitem") {
					
					var files = event.dataTransfer.getData('application/x-vnd.adobe.air.file-list'),
					attachmentsToAdd = _.map(
						_.filter(files, function(file) { return !file.isDirectory; }), 
						function(file) {
							return file.nativePath;
						}
					);
				
					if (attachmentsToAdd.length > 0) {
						diaryItemForm.showDialog(view.popupDropTarget.target,
                                                      null,
                                                      null,
                                                      null,
                                                      null,
                                                      attachmentsToAdd);
					}
				} 
			}
			
			view.clearPopupDropTarget();	
			view.popupDropTarget.reset();
		}, 
		monthSelect : function(event){
			console.log("month select month view");
			var view = this;
			var monthNumber = $(event.target).attr("id");
			var year = $(event.target).attr("year");
			
			selectedMonthView = monthNumber;
			//console.log("monthPopupMonthList MONTHNUMBER="+monthNumber);
			//console.log(monthNames[monthNumber]);
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "MONTH", monthNumber);
			if(!isSingleYear)
			{
				monthNumber=monthIndexArray[monthNumber];
			}
			view.setCurrentMonth(monthNumber,null,year);
			// call the refresh calendar so that the calendar will be refreshed
			app.context.trigger(EventName.REFRESHCALENDAR);
		
		},
		
		renderMonthView: function () {
			$("#tS2").thumbnailScroller({
				scrollerType:"hoverPrecise",
				scrollerOrientation:"horizontal",
				scrollSpeed:2,
				scrollEasing:"easeOutCirc",
				scrollEasingAmount:600,
				acceleration:4,
				scrollSpeed:800,
				noScrollCenterSpace:5,
				autoScrolling:0,
				autoScrollingSpeed:2000,
				autoScrollingEasing:"easeInOutQuad",
				autoScrollingDelay:500
			});
		}
				
	});

	return view;

})();