diary = diary || {};
diary.view = diary.view || {};

diary.view.MainView = (function() {

	// Map navigation linkTag to view classes
	var viewMap = {
		day: {
			viewClass: diary.view.DiaryView,
			viewCacheName: "diary.view.DiaryView",
			params: {
				diaryViewClass: diary.view.DiaryDayView,
				viewName: "diary.view.DiaryDayView"
			},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_DAY
		},
		week: {
			viewClass: diary.view.DiaryView,
			viewCacheName: "diary.view.DiaryView",
			params: {
				diaryViewClass: diary.view.DiaryWeekView,
				viewName: "diary.view.DiaryWeekView"
			},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_WEEK
		},
		month: {
			viewClass: diary.view.DiaryView,
			viewCacheName: "diary.view.DiaryView",
			params: {
				diaryViewClass: diary.view.DiaryMonthView,
				viewName: "diary.view.DiaryMonthView"
			},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_MONTH
		},
		glance: {
			viewClass: diary.view.DiaryView,
			viewCacheName: "diary.view.DiaryView",
			params: {
				diaryViewClass: diary.view.DiaryGlanceView,
				viewName: "diary.view.DiaryGlanceView"
			},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_GLANCE
		},
		subject: {
			viewClass: diary.view.SubjectView,
			viewCacheName: "diary.view.SubjectView",
			params: {},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_SUBJECT
		},
		inbox: {
			viewClass: diary.view.InboxView,
			viewCacheName: "diary.view.InboxView",
			params: {},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_INBOX
		},
		myclasses: {
			viewClass: diary.view.MyClassesView,
			viewCacheName: "diary.view.MyClassesView",
			params: {},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_MYCLASSES
		},
		myprofile: {
			viewClass: diary.view.MyProfileView,
			viewCacheName: "diary.view.MyProfileView",
			params: {},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_MYPROFILE
		},
		home: {
			viewClass: diary.view.HomeView,
			viewCacheName: "diary.view.HomeView",
			params: {},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_HOME
		},
		report: {
			viewClass: diary.view.ReportView,
			viewCacheName: "diary.view.ReportView",
			params: {},
			logEvent: diary.model.UsageLogEvent.TYPE.VIEW_REPORT
		},
		
	};
	
	// builds the mapping details for the footer tabs
	function buildFooterTabs() {
		
		getEmpowerDetails();
		var empowerType = app.model.get(ModelName.SCHOOLPROFILE).get("empowerType");
		//alert('empowerType : ' + empowerType);
		
		var contentType = 'Educational Content';
		if (empowerType)
		{
			if (empowerType.toUpperCase() == ContentSetting.EMPOWERSENIOR.toUpperCase())
			{
				contentType = 'Educational Content Sr';
			} 
			else if (empowerType.toUpperCase() == ContentSetting.EMPOWERJUNIOR.toUpperCase())
			{
				contentType = 'Educational Content Jr';
			} else if(empowerType.toUpperCase() == ContentSetting.EMPOWERELEM.toUpperCase())
			{
				contentType = 'Educational Content Elem';
			}
		}
		
		var jsonCnt = 0;
		var jsonLen = 0;
		var jsonObj = null;
		
		// holds flag if user is teacher
		var isTeacher = null; 
				
		// holds flag if user is student
		var isStudent = null;

		// holds student's profile i.e. user information
		var studentProfile = null;

		var shortcutsArr = null;

		
		
		// build the model from the content mapping model
		var schoolProfile = app.model.get(ModelName.SCHOOLPROFILE),
			footerItems = [];
		
		// if we have any footer links (shortcuts) mapping we will use it to generate
		// the footer tabs
		if (schoolProfile.get("contentMapping")) {
			
			// get the collection of all the shortcuts
			var shortcuts = schoolProfile.get("contentMapping").get("shortcuts"),
				linkIndex = 0;
			
			if (shortcuts != null)
			{
				studentProfile = app.model.get(ModelName.STUDENTPROFILE);
				isTeacher = studentProfile.get("isTeacher");
				isStudent = studentProfile.get("isStudent");
				//console.log("isTeacher : "+isTeacher);
				//console.log("isStudnet: "+isStudent);
				
				if (isTeacher != null && isTeacher == 1)
				{
					//console.log("isTeacher 2: "+ContentSetting.USERROLETEACHER);
					roleToFilter = ContentSetting.USERROLETEACHER;
				}

				if (isStudent != null && isStudent == 1)
				{
					//console.log("isStudnet: 2"+ContentSetting.USERROLESTUDENT);
					roleToFilter = ContentSetting.USERROLESTUDENT;
				}
				
				shortcutsArr = shortcuts.toArray();
				
				jsonLen = shortcutsArr.length;
				
				for (jsonCnt = 0; jsonCnt < jsonLen; jsonCnt++)
				{
					jsonObj = shortcutsArr[jsonCnt];
					
					if (shortcutsArr[jsonCnt].get('packageId') == ContentSetting.ABOUTDAISYPACKAGEID)
					{
						//console.log("roleToFilter : "+roleToFilter);
						//console.log("ContentSetting.USERROLESTUDENT : "+ContentSetting.USERROLESTUDENT);
						if (roleToFilter == ContentSetting.USERROLESTUDENT)
						{	
							shortcutsArr[jsonCnt].set('article', ContentSetting.ABOUTDAISYHOMEPAGESTUDENT);
							shortcutsArr[jsonCnt].set('documentId', (ContentSetting.ABOUTDAISYPACKAGEID + "/" + ContentSetting.ABOUTDAISYHOMEPAGESTUDENT));
							shortcutsArr[jsonCnt].set('id', ("app:/" + ContentSetting.ABOUTDAISYDIR + "/" + ContentSetting.ABOUTDAISYHOMEPAGESTUDENT));
							shortcutsArr[jsonCnt].set('url', ("app:/" + ContentSetting.ABOUTDAISYDIR + "/" + ContentSetting.ABOUTDAISYHOMEPAGESTUDENT));
						}
						else if (roleToFilter == ContentSetting.USERROLETEACHER)
						{	
							shortcutsArr[jsonCnt].set('article', ContentSetting.ABOUTDAISYHOMEPAGETEACHER);
							shortcutsArr[jsonCnt].set('documentId', (ContentSetting.ABOUTDAISYPACKAGEID + "/" + ContentSetting.ABOUTDAISYHOMEPAGETEACHER));
							shortcutsArr[jsonCnt].set('id', ("app:/" + ContentSetting.ABOUTDAISYDIR + "/" + ContentSetting.ABOUTDAISYHOMEPAGETEACHER));
							shortcutsArr[jsonCnt].set('url', ("app:/" + ContentSetting.ABOUTDAISYDIR + "/" + ContentSetting.ABOUTDAISYHOMEPAGETEACHER));
						}
					}
				}
				
				shortcuts = shortcutsArr;
			}
			
			// go through each of the shortcut items and create the
			// footer tabs and the footer mapping
			if (shortcuts) {
				//_.forEach(shortcuts.toArray(), function(shortcut) {
				_.forEach(shortcuts, function(shortcut) {
					
					var linkTag = "link" + linkIndex++,
						linkMapping = {},
						linkParams = {};
					// map the content item
					linkParams.contentViewClass = diary.view.ContentView;
					linkParams.viewName = "ContentView";
					linkParams.article = shortcut;
					linkParams.name=shortcut.get('title');
					linkMapping.viewClass = diary.view.ContentNavigationView;
					linkMapping.viewCacheName = "diary.view.ContentNavigationView";
					linkMapping.params = linkParams;
					linkMapping.logEvent = diary.model.UsageLogEvent.TYPE.VIEW_SHORTCUT;
					
					if(shortcut.get('title') == 'School Information'){
						linkTag = 'schoolInformation';
					} else if(shortcut.get('title') == contentType){
						linkTag = 'educationalContent';
					} else if(shortcut.get('title') == 'About Daisy'){
						linkTag = 'about';
					}
					
					// add the mapping to the global view mapping
					viewMap[linkTag] = linkMapping;
					// add the mapping to the footer link mapping
					footerItems.push({ 'linkTag' : linkTag,
									   'name' : shortcut.get('title') });
					
				});
			}
			
			// PD-520 Add Feedback button
			footerItems.push({ 'linkTag' : 'feedback',
							   'name' : 'Feedback' });
			
		}
		
		// return the created view
		return footerItems;
		
	}
	
	var mainView = Rocketboots.view.View.extend({

		events: {
			"click .frame .headerNavigation li"	: "navigationClick",
			"click .frame .footerNavigation a"	: "navigationClick",
			"click .cal_tbw_left .cal_tbw_top ul li.col.week a"		: "navigationClick",
			"click .cal_tbw_left .cal_tbw_top ul li.col.day a"		: "navigationClick",
			"click .cal_tbw_left .cal_tbw_top ul li.col.glance a"	: "navigationClick",
			"click .cal_tbw_left .cal_tbw_top ul li.col.month a"	: "navigationClick",
			
			//"click .cal_tbw_right .cal_filter_task ul li.col .tasks"	: "filterClick",
			//"click .cal_tbw_right .cal_filter_task ul li.col .event"	: "filterClick",
			//"click .cal_tbw_right .cal_filter_task ul li.col .note"		: "filterClick",
			
			"click .frame .wrap .navbar ul li.due"					: "dueCallout",
			"click .frame .wrap .navbar ul li.assigned"				: "assignedCallout",
			"click .frame .wrap .navbar ul li.notification"			: "notificationCallout",
			"click .frame .apps_btn table tr td img.search_icon"	: "searchCallout",
			"click .frame .apps_btn table tr td img.settings_icon"	: "redirectToProfile",
			"click .frame .apps_btn table tr td img.synch_icon"		: "callSynchAPI",
			"click .frame .apps_btn table tr td img.feedbackLink"	: "callFeedbackAPI",
			"click .cdacw_btn_cancel"								: "closeFeedbackForm",
			"click .dialog-message-cancel"							: "closeDialogMessage",
			"click .cdacw_btn_submit"								: "submitFeedbackForm",
			
			"click .calloutarea_due .callout_click table div"			: "dueItemSelected",
			"click .calloutarea_assigned .callout_click table div"		: "assignedItemSelected",
			"click .calloutarea_notification .callout_click table div"	: "notificationSelected",
			
			"click .calloutarea_due .callout_click table div img"		: "removeDueItem",
			"click .calloutarea_assigned .callout_click table div img"	: "removeAssignedItem",
			"click .calloutarea_notification .callout_click table div img"	: "removeNotificationItem",
			
			"click .frame .task_ic"										: "showDiaryItemForm",
			"click .closeCallout"										: "closeCallout",
			"click .closeCalendarAccessPopup"							: "closeStudCalendar",		// close Student Calendar
			
			"mousedown .frame .footerNavigation a"	: "preventDrag",
			"mousedown .frame header"			: "frameStartDrag",
			"mousedown .frame .search input"	: "onSearchDrag",
			"click .frame .search input"		: "onSearchClick",
			"keydown .frame .search input"		: "onSearchKeydown",
			"keyup .frame .search input"		: "onSearchChange"
		},
			
		initialize: function() {
			
			var currentUserId = ""+app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
			//FlurryAgent.setUserId(currentUserId);
			FlurryAgent.setAppVersion("2.0.5");
			
			Rocketboots.view.View.prototype.initialize.call(this);

			app.context.on(EventName.VIEWMONTH, this.switchToMonthView, this);
			app.context.on(EventName.VIEWDAY, this.switchToDayView, this);
			
			app.context.on(EventName.VIEWPROFILE, this.inboxView, this);
			
			app.context.on(EventName.REFRESHSUBJECTS, this.refreshSubjectView, this);

			app.context.on(EventName.SEARCHCOMPLETE, this.showSearchResults, this);
			
			app.context.on(EventName.CONTENTITEMSRELOADED, this.renderFooterNav, this);
			app.context.trigger(EventName.FETCHNEWSANNOUNCEMENT);
			app.context.trigger(EventName.INBOXITEMS);
			//diary.view.ThemeUtil.registerThemeListener(this);

			this.render();
		},
		
		render: function() {
			Rocketboots.view.View.prototype.render();

			this.headerItems = [
					{ linkTag: 'day',		name: 'DAY' },
					{ linkTag: 'week',		name: 'WEEK' },
					{ linkTag: 'month',		name: 'MONTH' },
					{ linkTag: 'glance',	name: 'GLANCE' }
				];
				
			this.actualHeaderItems = [
					{ linkTag: 'home',		name: 'Home' },
					{ linkTag: 'day',		name: 'Calendar' },
					{ linkTag: 'subject',	name: 'Timetable' },
					{ linkTag: 'inbox',		name: 'Inbox' },
					{ linkTag: 'schoolInfo',name: 'School Information' },
					{ linkTag: 'myclasses',	name: 'My Classes' },
					{ linkTag: 'myprofile',	name: 'My Profile' },
					{ linkTag: 'about',		name: 'About' }
				];
			
			setTimeout(function(){
				this.footerItems = buildFooterTabs();
			},7000);
			
			//this.triggerFetchDiaryItems();
			fetchDairyItems();
			
			//Fetching due items in callout.
			var overDueTasks = app.model.get(ModelName.DIARYITEMS).getRequiredOverdueTasks();
			
			//Fetching assigned items in callout.
			var assignedTasks = app.model.get(ModelName.DIARYITEMS).getRequiredAssignedTasks();
			
			//Fetching notifications in callout.
			var notifications = app.model.get(ModelName.DIARYITEMS).getNotifications();
         	var params = {
				headerNav: this.actualHeaderItems,
				footerNav: this.footerItems,
				versionNumber: "Version " + app.model.get(ModelName.SYSTEMCONFIGURATION).get('versionLabel'),
				overDueTaskCount:overDueTasks.length,
				assignedTaskCount:assignedTasks.length,
				notificationCount:notifications.length,
				'isTeacher' : true
			};
			var content = app.render('frame', params);

			$(this.el).html(content);
			console.log("MainContent", content);
			
			this.searchInput = $(".search > input", this.el);

			// force the theme to render when the view is rendered
		
			
			diary.view.ThemeUtil.renderThemeFromSchoolProfile(this);
		},

        renderSchoolLogo: function() {
            var schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);
			console.log("schoolProfile.get('schoolLogo')"+schoolProfile.get('schoolLogo'));
            if (_.isEmpty(schoolProfile.get('schoolLogo'))) {
                $("header div.logo img", this.el).hide();
            } else {
                $("header div.logo img", this.el).attr('src', ("app-storage:/" + schoolProfile.get('schoolLogo')));
                $("header .logo_text").text(schoolProfile.get('name'));
                
            }
        },
        
        setTempDeletionFlag: function(event, hideDiaryItem){
        	var id = $(event.currentTarget).attr('id');
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "UPDATE diaryItem SET "+hideDiaryItem+" = 1  WHERE id = :diaryItemId",
	
	            // params
	            {
				'diaryItemId' :id
	            },
				// success
				function(statement) {
				
				},
				
				// error
				function error()
				{
					
				}
			);
			
			//this.triggerFetchDiaryItems();
			fetchDairyItems();
        },
        
        markItemAsRead: function(event) {
			var id = $(event.currentTarget).attr('itemId');
			var table = $(event.currentTarget).attr('itemType');
			var tableName = '';
			if(table == 'News'){
				tableName = 'News';
			} else if(table == 'Announcement'){
				tableName = 'Announcements';
			}
			
			if(tableName != ''){
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
		            // query
		            "UPDATE "+ tableName + " SET readStatus = 1  WHERE id = :itemId",
		
		            // params
		            {
					'itemId' :id
		            },
					// success
					function(statement) {
					
					},
					
					// error
					function error()
					{
						
					}
				);
			}
			app.context.trigger(EventName.INBOXITEMS);
			app.context.trigger(EventName.FETCHNEWSANNOUNCEMENT);
		},
        
        triggerFetchDiaryItems: function(){
        
        	var year = SystemSettings.HARDCODDEDYEAR;
	        var startDate = new Rocketboots.date.Day(year, 0, 1);
	        var endDate = new Rocketboots.date.Day(year, 11, 31);
	        
			app.context.trigger(EventName.FETCHDIARYITEMS, startDate, endDate, false, false, false);
        },
        
        closeStudCalendar : function(event) {
			
			//alert('closeStudCalendar called');
        	$(".black_overlay_callout_calandar").hide();
    	    $("#calendarAccessPopup").hide();
		},
        
        removeDueItem: function(event){
			this.setTempDeletionFlag(event, "hideFromDue");
			this.renderDueCallout(event);
			event.stopPropagation();
		},
		
		removeAssignedItem: function(event){
			this.setTempDeletionFlag(event, "hideFromAssigned");
			this.renderAssignedCallout(event);
			event.stopPropagation();
		},
		
		removeNotificationItem: function(event){
			this.setTempDeletionFlag(event, "hideFromNotifications");
			this.renderNotificationCallout(event);
			event.stopPropagation();
		},
		
		showDiaryItemForm : function(event){
			once=true;
			var date = new Date();
			app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(date.getFullYear(), date.getMonth(), date.getDate()));
			var view = this,
				params = {},
				target = $(event.currentTarget),
				diaryItemId = target.attr('diaryItemId') || null;

			if (diaryItemId == null) {
				params = view.getDiaryItemAttributesFromElement(target);
			}

			diaryItemForm.showDialog(diaryItemId,
	                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
	                                          params.startTime,
	                                          params.endTime,
	                                          { 'subject' : params.subject },'',null, params.addTask);
	                                          
            $(".editButtonContainer .save").show();
            $(".editButtonContainer .lockItem").hide();
            $(".editButtonContainer .delete").hide();
            $(".ui-dialog-titlebar").hide();
		    event.stopPropagation();
		},
		
		getDiaryItemAttributesFromElement: function (element) {
			var view = this,
				params = {},
				target = $(element);

			// try to get the dayKey
			var dayKey = target.attr("dayKey");
			if (dayKey) {
				params.date = Rocketboots.date.Day.parseDayKey(dayKey).getDate();
			} else {
				var currentDate= new Date();
				var date = new Rocketboots.date.Day(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
				params.date = date.getDate();
			}

			// try to get period
			var period = target.attr("period");
			if (period) {
				params.period = parseInt(period, 10);
			}

			// try to get start/end times
			{
			    var startTimeString = target.attr("startTime"),
					endTimeString = target.attr("endTime"),
					startTime = startTimeString ? Rocketboots.date.Time.parseTime(startTimeString) : null,
					endTime = endTimeString ? Rocketboots.date.Time.parseTime(endTimeString) : null,
					isMidnight = function (time) {
						return time.getHour() == 0 && time.getMinute() == 0;
					};

				if (startTime && endTime) {
					// don't use midnight times, instead taking a 1 hour segment closest to the following/previous period
					if (isMidnight(startTime)) {
						startTime = new Rocketboots.date.Time(endTime.getHour() - 1, endTime.getMinute());
					}
					if (isMidnight(endTime)) {
						endTime = new Rocketboots.date.Time(startTime.getHour() + 1, startTime.getMinute());
					}
				}

				if (startTime) params.startTime = startTime;
				if (endTime) params.endTime = endTime;
			}

			// try to get subject
			var subjectId = target.attr("subjectId");
			var subject = subjectId && app.model.get(ModelName.SUBJECTS).get(subjectId);

			if (subject) {
				params.subject = subject;
			}
			
			params.addTask = true;
			

			return params;
		},
		
		renderFooterNav: function() {
			var view = this, 
				footerContent, 
				selectedFooterNavItem, 
				previousSelectedLinkName = null,
				previousSelectedArticle = null,
				link = null;
			view.footerItems = buildFooterTabs();
			
			// check if there is currently a tab selected
			selectedFooterNavItem = $(".footerNavigation .navigationItem.selected:first", view.el);
			
			if (selectedFooterNavItem.length > 0) {
				previousSelectedLinkName = selectedFooterNavItem.html();
				
				// try to figure out the currently viewed article
				var contentNavView = view.childViews["diary.view.ContentNavigationView"];
				previousSelectedArticle = contentNavView.tabDetails.selectedArticle;
			}
			
			footerContent = app.render('frameFooterNav', { 'footerNav' : view.footerItems });
			$('footer', view.el)
				.find('nav').remove().end()
				.prepend(footerContent);

			if (previousSelectedLinkName) {
				link = view.getFooterLinkTagByName(previousSelectedLinkName);
				if (link && link.linkTag) {
					// switch to content view
					view.switchToView(link.linkTag, {
							'articleToShow' : previousSelectedArticle, 
							'mustRender' : true, 
							'forceRefresh': true
					});
				} else {
					view.footerItems.length > 0 && view.switchToView(view.footerItems[0].linkTag);
				}
			}
			
		},
		
		betaFeedback: function() {
			var view = this;
			$("#feedbackContent", view.el).val("");
			
			var feedbackDialog = $( "#feedbackDialog", view.el ),
				feedbackCancel = function() {
					feedbackClose();
				},
				feedbackSave = function() {
					var content = $("#feedbackContent", view.el).val();
					
					if (content && content != '') {
						
						var segmentIndex = 1, thisSegment;
						
						// Clip really long content to 1000 characters
						if (content.length > 1000) {
							content = content.substr(0, 1000);
						}
						
						// Break the feedback up into 200 character pieces (underlying DB limitation of 255 - plus room for JSON encoding)
						while (content.length > 200) {
							thisSegment = '[' + (segmentIndex++) + '] ' + content.substr(0, 200);
							content = content.substr(200);
							
							app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.FEEDBACK, thisSegment);
						}
						
						thisSegment =  '[' + (segmentIndex) + '] ' + content;
						app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.FEEDBACK, thisSegment);
					}
					
					feedbackClose();
				},
				feedbackClose = function() {
					$("#feedbackSave", view.el).off("click", feedbackSave);
					$("#feedbackCancel", view.el).off("click", feedbackCancel);
					
					feedbackDialog.dialog('close');
				};
			
			$("#feedbackSave", view.el).on("click", feedbackSave);
			$("#feedbackCancel", view.el).on("click", feedbackCancel);
			
			feedbackDialog.dialog({ autoOpen: false, modal: true });
			feedbackDialog.dialog('open');
			
			
			//var feedback = prompt('We appreciate any feedback about your diary', 'Feedback', 'Title goes here', 'something else');
			//alert(feedback);

		},
		
		switchToView: function(viewName, additionalParams) {
			
			air.System.gc();
			
			selectedWeekView = "";
			selectedMonthView = "";
			selectedMonthGlanceView = "";
			if(viewName == 'subject'){
				 //console.log("subject view");
				 if (app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")) {
					getClassForTeacher(app.model.get(ModelName.STUDENTPROFILE).get("UniqueID"));
				 } else {
					getAllClasses();
				 }
		         app.context.trigger(EventName.GENERATECALENDAR, SystemSettings.HARDCODDEDYEAR);
		         //app.context.trigger(EventName.GENERATETIMETABLE, SystemSettings.HARDCODDEDYEAR);
			          
			}
			
			var eventParameters = getNavigationEventParameters();
			FlurryAgent.logEvent(""+ toTitleCase(viewName) +" View Clicked",eventParameters);
			
			if(viewName == 'home'){
				if(app.model.get(ModelName.APPLICATIONSTATE).get('isDueSelected')){
					app.model.get(ModelName.APPLICATIONSTATE).set('isDueSelected',true);
				}
				$("#containerDiv").css("border-radius","0px 6px 6px 6px");
			} else {
				app.model.get(ModelName.APPLICATIONSTATE).set('isDueSelected',true);
				$("#containerDiv").css("border-radius","6px 6px 6px 6px");
			}
			
			console.log("after GENERATECALENDAR");
			var view = this;
			if(viewName == "link0" && currentEditingState == true) return;
			if (viewName == 'feedback') {
				view.betaFeedback();				
				return;
			}
			
			var viewDef = viewMap[viewName];
			
			if (typeof(viewDef) === "undefined" || viewDef == null) {
				return;
			}
	
			var usageLogEvent = viewDef.logEvent;
			if (typeof(usageLogEvent) != 'undefined') {
				if (typeof(viewDef.params.article) != "undefined") {
					app.context.trigger(EventName.ADDUSAGELOG, usageLogEvent, viewDef.params.article);
				}
				else {
					app.context.trigger(EventName.ADDUSAGELOG, usageLogEvent);
				}
			}
	
			var	viewClass = viewDef.viewClass, 
				viewParams = viewDef.params,
				viewCacheName = viewDef.viewCacheName;
				viewDef.params.name=passname;

			if (typeof(viewClass) === "undefined" || viewClass == null) {
				return;
			}

			if (typeof(viewParams) === "undefined" || viewParams == null) {
				viewParams = {};
			}

			if (typeof(viewCacheName) === "undefined" || viewCacheName == null) {
				viewCacheName = viewName;
			}

			
			var $link = $(".navigationItem[data-link=" + viewName + "]", view.el);
			var linkName = $link.text();
			if ($link.hasClass("selected")) {
				if (typeof(additionalParams) != "undefined" && additionalParams.forceRefresh) {
					// continue					
				} /* else if(linkName != 'CalendarDay'){
					return;					
				} */
			}
			else {
				$(".navigationItem", view.el).removeClass("selected");
				if(linkName == 'Week' || linkName == 'Month' || linkName == 'Glance'){
					$(".calender_li").addClass("selected");
				} else {
					$link.addClass("selected");
				}
			}
			
			var setToday = app.model.get(ModelName.APPLICATIONSTATE).get('setToday');
			if(!setToday){
				var today = new Rocketboots.date.Day();
			  	app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', today);
			} else {
				app.model.get(ModelName.APPLICATIONSTATE).set('setToday', false);
			}
			
			if(viewName == 'inbox'){
				
			  	app.model.get(ModelName.APPLICATIONSTATE).set('sortOrder', 'desc');			
			}
			
			if(viewName == 'day'){
			  	app.model.get(ModelName.APPLICATIONSTATE).set('hideFilters', true);			
			} else {
				app.model.get(ModelName.APPLICATIONSTATE).set('hideFilters', false);	
				$(".cal_tbw_right .cal_filter_task").show();
			}

			$(".content > div", view.el).hide();
		
 			var childView = view.childViews[viewCacheName];
			if (typeof(childView) === "undefined") {
				// Initial creation of the child view
				viewParams.el = $("<div></div>");
				if (typeof(additionalParams) != "undefined") {
					// do a copy operation so the additional parameters aren't cached
					viewParams = _.extend({}, viewParams, additionalParams);
				}
				childView = view.childViews[viewCacheName] = new viewClass(viewParams);
			}
			else {
				if (typeof(childView.parseAdditionalParams) != "undefined") {
					if (typeof(additionalParams) != "undefined") {
						// do a copy operation so the additional parameters aren't cached
						viewParams = _.extend({}, viewParams, additionalParams);
					}

					childView.parseAdditionalParams(viewParams);
				}

				//console.log("rendering", viewCacheName);
				childView.render();
			}
			$(".content", view.el).append(childView.el);
			childView.$el.show();
		},
		
		navigationClick: function(event) {
			
			modeCreateMsg = tempModeCreate;
			tempModeCreate = false;
			$("div[id*='classFormEditPopup']").parents(".ui-dialog").remove();
			currentEditingState = false;
			if(emailExits)
			{
				var $link = $(event.target),
					thisView = $link.attr('data-link');
					passname=$link.text();
				
				if(thisView == 'day'){
					//Clearing all check box values. 
					$('.cal_tbw_right .cal_filter_task ul li.col .tasks').removeAttr('checked');
					$('.cal_tbw_right .cal_filter_task ul li.col .event').removeAttr('checked');
					$('.cal_tbw_right .cal_filter_task ul li.col .note').removeAttr('checked');
				}
				
				if(thisView == 'inbox' && !fromSync){
					//Clearing all check box values. 
					$('#inbox_filter_message').removeAttr('checked');
					$('#inbox_filter_news').removeAttr('checked');
					$('#inbox_filter_announcement').removeAttr('checked');
				}
				
				if (typeof(thisView) === "undefined" || thisView == null) {
					return;
				}
			
				
				this.switchToView(thisView);
			}
			
			air.System.gc();
		},
		
		filterClick: function(event) {
			
			var isNote = false;
			var isTask = false;
			var isEvent = false;
			
			if($('#cal_filter_notes').attr('checked')) {
				isNote = true;
			}
			
			if($('#cal_filter_events').attr('checked')) {
				isEvent = true;
			}
			
			if($('#cal_filter_tasks').attr('checked')) {
				isTask = true;
			}
			
			var startDate = app.model.get(ModelName.APPLICATIONSTATE).get('startDate');
           	var endDate = app.model.get(ModelName.APPLICATIONSTATE).get('endDate');
           	
			app.context.trigger(EventName.FETCHDIARYITEMS, startDate, endDate, isNote, isTask, isEvent);
			app.context.trigger(EventName.REFRESHCALENDAR);
				
		},
		
		dueCallout: function(event) {
		
			if($('.calloutarea_due').hasClass("due")){
				$(".calloutarea_due").hide();
				$(".black_overlay_callout").css("display","none");
				$(".calloutarea_due").removeClass("due");
			} else {
				$(".black_overlay_callout").show();
		        $(".calloutarea_due").show();
		        $(".calloutarea_due").addClass("due");
		        $(".calloutarea_notification").removeClass("notification");
		        $(".calloutarea_assigned").removeClass("assigned");
		        $(".calloutarea_search").removeClass("search");
		        $(".calloutarea_assigned").hide();
		        $(".calloutarea_notification").hide();
		        $(".calloutarea_search").hide();
		        
		        this.renderDueCallout(event);
			}
		},
		
		renderDueCallout: function(event) {
			
			var eventParameters = getNavigationEventParameters();
  			FlurryAgent.logEvent("Due Callout Clicked", eventParameters);
  			
			var overDueTasks = app.model.get(ModelName.DIARYITEMS).getRequiredOverdueTasks();
			var diaryItemOverDue = [];
		
			for(var i = 0 ; i < overDueTasks.length; i++){
				var task = overDueTasks.at(i);
				diaryItemOverDue.push(diary.view.DiaryCalendarViewUtil.mapDiaryItem(task));
			}
			
			var noResult = null;
			if(diaryItemOverDue && diaryItemOverDue.length == 0){
				noResult = "Due tasks not available.";
			}
			var overDueTaskList = {
					"overDueTasks":diaryItemOverDue,
					"noResult":noResult
			}
			
			//$("#dueCount").html(overDueTasks.length);
			var popupContent = app.render('dueItemsView', overDueTaskList);
			$(".calloutarea_due .callout_click .callout_click_container").html(popupContent);
		},
		
		assignedCallout: function(event) {
			if($('.calloutarea_assigned').hasClass("assigned")){
				$(".calloutarea_assigned").hide();
				$(".black_overlay_callout").css("display","none");
				$(".calloutarea_assigned").removeClass("assigned");
			} else {
				$(".black_overlay_callout").show();
		        $(".calloutarea_due").hide();
		        $(".calloutarea_search").hide();
		        $(".calloutarea_assigned").show();
		        $(".calloutarea_assigned").addClass("assigned");
		        $(".calloutarea_due").removeClass("due");
		        $(".calloutarea_notification").removeClass("notification");
		        $(".calloutarea_search").removeClass("search");
		        $(".calloutarea_notification").hide();
		        
		        this.renderAssignedCallout(event);
		    }
		},
		
		renderAssignedCallout: function(event) {
			
			var eventParameters = getNavigationEventParameters();
  			FlurryAgent.logEvent("Assigned Callout Clicked", eventParameters);
  			
			var assignedTasks = app.model.get(ModelName.DIARYITEMS).getRequiredAssignedTasks();
			var diaryItemAssigned = [];
			
			for(var i = 0 ; i < assignedTasks.length; i++){
				var task = assignedTasks.at(i);
				diaryItemAssigned.push(diary.view.DiaryCalendarViewUtil.mapDiaryItem(task));
			}
			
			var noResult = null;
			if(diaryItemAssigned && diaryItemAssigned.length == 0){
				noResult = "Assigned tasks not available.";
			}
			
			var assignedTaskList = {
					"assignedTasks":diaryItemAssigned,
					"noResult":noResult
			}

			//$("#assignedCount").html(assignedTasks.length);
			var popupContent = app.render('assignedItemsView', assignedTaskList);
			$(".calloutarea_assigned .callout_click .callout_click_container").html(popupContent);
		},
		
		notificationCallout: function(event) {
			if($('.calloutarea_notification').hasClass("notification")){
				$(".calloutarea_notification").hide();
				$(".black_overlay_callout").css("display","none");
				$(".calloutarea_notification").removeClass("notification");
			} else {
				$(".black_overlay_callout").show();
				$(".calloutarea_due").hide();
		        $(".calloutarea_assigned").hide();
		        $(".calloutarea_search").hide();
		        $(".calloutarea_notification").show();
		        $(".calloutarea_notification").addClass("notification");
		        $(".calloutarea_assigned").removeClass("assigned");
		        $(".calloutarea_due").removeClass("due");
		        $(".calloutarea_search").removeClass("search");
		        
		        this.renderNotificationCallout(event);
		    }
		},
		
		renderNotificationCallout: function(event) {
			
			var eventParameters = getNavigationEventParameters();
  			FlurryAgent.logEvent("Notifications Callout Clicked", eventParameters);
  			
			var notifications = app.model.get(ModelName.DIARYITEMS).getNotifications();
			var newsAndAnnouncments = app.model.get(ModelName.NEWSANNOUNCEMENTS);
			var newsAnnouncmentsForToday = this.mapDay(newsAndAnnouncments);
			  
			var allNotifications = [];
			
			for(var i = 0 ; i < notifications.length; i++){
				var task = notifications.at(i);
				allNotifications.push(diary.view.DiaryCalendarViewUtil.mapDiaryItem(task));
			}
			
			var noResult = null;
			if(allNotifications && allNotifications.length == 0 && newsAnnouncmentsForToday && newsAnnouncmentsForToday.newsAnnouncements.length == 0){
				noResult = "Notifications not available.";
			}
			var notificationsList = {
					"notifications":allNotifications,
					"newsAndAnnouncments":newsAnnouncmentsForToday,
					"noResult":noResult
			}

			//$("#notificationCount").html(notifications.length);
			var popupContent = app.render('notificationsView', notificationsList);
			$(".calloutarea_notification .callout_click .callout_click_container").html(popupContent);
		},
		
		//group news and announcements daywise
		'mapDay' : function(newsAnnouncements){
			var daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
			var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			
			var view = this;
			var newsAnnouncementsModel = {
					'newsAnnouncements':[],
			};
		 
		 	var now=new Date();
		    var month=now.getMonth();
		    var day= now.getDate();
		 	var calendar = app.model.get(ModelName.CALENDAR);
		 	//take calendardays of 1 month to take news of 1 month span
		 	
		 	var dayObject;
		 	var days;
		 	var newsAnnouncementObject;
		 	var calendarDay = calendar.getDay(month+1,day);
		 		dayObject = {
						'newsAnnouncementsForDay': [],
					};
				newsAnnouncementObject={
						'newsAnnouncement':[],
				};
		 		var calDay = new Rocketboots.date.Day(calendarDay.get('day'));
		 		var arrNewsAnnouncement = [];
				for(var i = 0; i < newsAnnouncements.length;i++){
					var newsAnnouncement =  newsAnnouncements[i];
					var dayKey = newsAnnouncement.get('Updated');
					var year=dayKey.toString().substring(0,4);
					var month=dayKey.toString().substring(5,7);
					var day=dayKey.toString().substring(8,10);
					var newDate=new Date(year,month-1,day);
					var calendarDate = new Date(calDay.getDate());
					newsAnnouncement.createdTime = (dayKey.split(" "))[1];
					
					var itemTitle = newsAnnouncement.get('title');
					
					newsAnnouncement.set('notificationTitle', itemTitle);
				    
					var tablePresent = (itemTitle == null || itemTitle == undefined) ? 0 : itemTitle.indexOf("<tr");
				     if(tablePresent > 0){
				      
				    	 var eventTitle = null;
				    	 var eventDesc = null;
				    	 
				    	 var tableObj = null;
				    	 
				    	 tableObj = $('<div/>').html(itemTitle).contents();
				    	 
				    	 eventTitle = $(tableObj).children().find("tr:eq(0)").find("td:nth-child(1)").text();
				    	 eventDesc = $(tableObj).children().find("tr:eq(0)").find("td:nth-child(2)").text();
				    	 
				    	 newsAnnouncement.set('notificationTitle', (eventTitle + " " + eventDesc));
				      }
				      
				    if ((newDate-calendarDate == 0) && newsAnnouncement.get('readStatus') == 0) {
						newsAnnouncementObject.newsAnnouncement.push(newsAnnouncement.toJSON());
					}	
				}
				if(newsAnnouncementObject.newsAnnouncement.length > 0){
					dayObject.newsAnnouncementsForDay.push(newsAnnouncementObject);
				}
				
				if(dayObject.newsAnnouncementsForDay.length > 0){
					var date = calendarDay.get('dayOfMonth')  + " " + months[calendarDay.get('day').getMonth()].toString().substring(0,3) + " " + now.getFullYear();
					
					dayObject.createdDate = date;
					dayObject.createdDay = daysOfWeek[calendarDay.get('day').getDay()];
					
					var dayOfWeekForGlance = '';
					dayOfWeekForGlance = diary.view.DiaryCalendarViewUtil.formatShortDateInString(new Rocketboots.date.Day(calendarDay.get('day')));
					if(dayOfWeekForGlance != ''){
						dayObject.createdDay = dayOfWeekForGlance;
					}
				
					newsAnnouncementsModel.newsAnnouncements.push(dayObject);
				}
				
				
		 	return newsAnnouncementsModel;
		},
		
		searchCallout: function(event) {
			if($('.calloutarea_search').hasClass("search")){
				$(".calloutarea_search").hide();
				$(".black_overlay_callout").css("display","none");
				$(".calloutarea_search").removeClass("search");
			} else {
				$(".black_overlay_callout").show();
				$(".calloutarea_due").hide();
		        $(".calloutarea_assigned").hide();
		        $(".calloutarea_notification").hide();
		        $(".calloutarea_search").show();
		        $(".calloutarea_search").addClass("search");
		        $(".calloutarea_assigned").removeClass("assigned");
		        $(".calloutarea_due").removeClass("due");
		        $(".calloutarea_notification").removeClass("notification");
		    }
		},
		
		hideCallouts : function() { 
			
			$(".calloutarea_due").hide();
	        $(".calloutarea_assigned").hide();
	        $(".calloutarea_notification").hide();
	        $(".calloutarea_search").hide();
	        $(".black_overlay_callout").css("display","none");
	        $(".calloutarea_search").removeClass("search");
	        $(".calloutarea_notification").removeClass("notification");
	        $(".calloutarea_assigned").removeClass("assigned");
	        $(".calloutarea_due").removeClass("due");
	        
		},
		
		redirectToProfile: function(event) { 
			
			this.hideCallouts();
			this.switchToView("myprofile");
			$(".profile_li .navigationItem").addClass("selected");
		},
		
		callSynchAPI: function(event) { 
			
			this.hideCallouts();
	        
			app.model.get(ModelName.DIARYITEMS).off("change");
			//var activeSubTab = $("#subTabProfileView .tabs_container").find(".active");
			//console.log("calling to timeout current active tab is"+$(".headerNavigation").find(".selected").attr("data-link"));
			timeout();
			var eventParameters = getNavigationEventParameters();
  			FlurryAgent.logEvent("Sync Called", eventParameters);
				
			//$(".headerNavigation").find(".selected").click()
			//console.log("$(.tabs_container).find(.active) : "+activeSubTab.attr("class"));
			//$(".tabs_container .tabMenu_btn ul li.announcements").click()
			//ctiveSubTab.click();
		},
		
		callFeedbackAPI: function(event) { 
			
			var view = this;
			if($('.feedbackDiv').hasClass("feedback")){
				 view.closeFeedbackForm();
			} else {
				view.hideCallouts();
				$(".black_overlay_callout").css("height","100%");
				$(".black_overlay_callout").css("top","0%");
				$(".black_overlay_callout").show();
		        $(".feedbackDiv").show();
		        $(".feedbackDiv").addClass("feedback");
		    }
		},
		
		closeFeedbackForm : function(event) { 
			$(".feedbackDiv").hide();
			$(".black_overlay_callout").css("height","93%");
			$(".black_overlay_callout").css("top","7%");
			$(".black_overlay_callout").css("display","none");
			$(".feedbackDiv").removeClass("feedback");
		},
		
		closeDialogMessage : function(event) { 
			$(".dialog-message").hide();
			$(".black_overlay_callout_message").hide();
			event.stopPropagation();
		},
		
		submitFeedbackForm : function(event) { 
			var view = this;
			var email = app.model.get(ModelName.STUDENTPROFILE).get('email');
			var macAddress = app.model.get(ModelName.SYSTEMCONFIGURATION).get('macAddresses');
			var message = $(".feedbackMessage").val().trim();
			var url = SystemSettings.SERVERURL + "/feedback/";
			
			if(message == null || message == undefined || message == ''){
				openMessageBox("Please enter feedback");
				return;
			}
			
			var newData = '{"device_type":"desktop","email_id":"'+email+'","user_id":"'+UserId+'","message":"'+message+'","ipadd":"'+ macAddress +'"}';
			console.log("submitFeedbackForm called : " + JSON.stringify(newData));
			 
			 $.ajax({
		            'url' : url,
		            'type': ajaxType,
		            'data': JSON.stringify(newData),
		            'dataType' : "json",
		            'success': function(result) {
			           	console.log("success submitFeedbackForm = "+JSON.stringify(result));
			            $(".feedbackMessage").val("");
			            view.closeFeedbackForm();
			            openMessageBox("Feedback submitted successfully");
		           	}, 
		            'error': function(response) {
		                console.log("TheCloud.error submitFeedbackForm: ", JSON.stringify(response));
		                FlurryAgent.logError("Feedback Error", "Error in submitting feedback",1);
		                if(!(window.monitors.network.available))
		    			{	
		                	openMessageBox("Internet is not available, Please submit feedback later.");
		    			} else { 
		    				openMessageBox("Error in submitting feedback");
		    			}
		                
		            }
		        });
		},
		
		dueItemSelected: function(event){
			var view = this;
			
			$(".calloutarea_due").hide();
			$(".black_overlay_callout").css("display","none");
			$(".calloutarea_due").removeClass("due");
			var id = $(event.currentTarget).attr('itemId');
			
			var diaryItem = app.model.get(ModelName.DIARYITEMS).get(id);
			if (diaryItem) {										
				view.showDiaryItem(diaryItem);
			}
			
		},
		
		closeCallout: function(event){
			hideCallouts();
		},
		
		assignedItemSelected: function(event){
			var view = this;
			
			$(".calloutarea_assigned").hide();
			$(".black_overlay_callout").css("display","none");
			$(".calloutarea_assigned").removeClass("assigned");
			var id = $(event.currentTarget).attr('itemId');
			
			var diaryItem = app.model.get(ModelName.DIARYITEMS).get(id);
			if (diaryItem) {										
				view.showDiaryItem(diaryItem,true);
			}
			
		},
		
		notificationSelected: function(event){
			var view = this;
			
			$(".calloutarea_notification").hide();
			$(".black_overlay_callout").css("display","none");
			$(".calloutarea_assigned").removeClass("notification");
			var id = $(event.currentTarget).attr('itemId');
			var itemType = $(event.currentTarget).attr("itemType")
			
			if (itemType == 'assignment' || itemType == 'homework' || itemType == 'note' || itemType == 'event') {
				var diaryItem = app.model.get(ModelName.DIARYITEMS).get(id);
				
				if (diaryItem) {										
					view.showDiaryItem(diaryItem);
				}
			} else {
				this.markItemAsRead(event);
				
				app.model.get(ModelName.APPLICATIONSTATE).set('inboxItemId',id);
				app.model.get(ModelName.APPLICATIONSTATE).set('inboxItemType',itemType);
				view.switchToView("inbox",{
					id: id,
					type: itemType,
					forceRefresh: true
				});
			}
			
		},
		
		preventDrag: function(event) {
			//console.log(event);
			event.preventDefault();
		},

		/**
		 * Indicate to the container chrome that here lies an element that can drag the container
		 */
		frameStartDrag: function(event) {
			app.trigger(EventName.CHROMEDRAGGABLE, event);
		},
		
		switchToMonthView: function(monthToView) {
			this.switchToView('month', {monthToView: monthToView});
		},
		
		switchToDayView: function(dayToView) {
			this.switchToView('day', {dayToView: dayToView});
		},
		
		switchToProfileView: function(inboxView) {
			this.switchToView('inbox', {inboxView: inboxView});
		}, 
		
		refreshSubjectView: function() {
			
 			var view = this.childViews[viewMap.subject.viewCacheName];
 			console.log("refreshSubjectView : view : "+view);
 			view.render();
		}, 
		
		/*
		 * on click inside the search input
		 */
		onSearchClick: function(event) {
			var $input = $(event.target);
			$.createSelection($input, 0, $input.val().length, true);
			this.onSearchChange(event);
		},
		
		/*
		 * on click inside the search input
		 */
		onSearchDrag: function(event) {
			event.stopPropagation(); // so we don't end up dragging the whole window
		},
		
		
		/*
		 * On keydown in search input
		 */
		onSearchKeydown: function(event) {
			var view = this, 
			input = $(event.target);
			
			if (event.keyCode == 9) { // tab
				input.blur();
				view.hideSearchResults();
				event.preventDefault();
			}
		},
		
		/* 
		 * On keyup in search input 
		 */
		onSearchChange: function(event) {
			var view = this, 
				input = $(event.target);
			
			clearTimeout(view.triggerSearchTimeout);
			
			if (event.keyCode == 27) { // escape
				input.blur();
				view.hideSearchResults();
			} else if (view.searchInput.val().length > 0) {
				view.triggerSearchTimeout = setTimeout(function() {
					view.triggerSearch();
				}, 300);				
			} else {
				view.hideSearchResults();
			}			
			
		},
		
		
		/**
		 * Trigger search
		 */
		triggerSearch: function() {
		
			if(!emailExits)
 					{
 						return;
 					}
			var criteria = this.searchInput.val();
			
			this.searchInput.parents('.search:first').addClass('loading');
			app.context.trigger(EventName.SEARCH, criteria);
		},
		
		/**
		 * Show search results popup when search results come back
		 */
		showSearchResults: function(searchResults) {
			var view = this;
			
			if (view.searchInput && view.searchInput.val().toLowerCase() == searchResults.get('criteria')) {
				// show search results
				var searchResultsParams = diary.view.DiarySearchUtil.mapSearchResults(searchResults);
				
				// render the content of the popup		
				view.searchInput.parents(".search:first").removeClass('loading');
				
				var popupContent = app.render('searchResults', searchResultsParams);
				
				//console.log("showing search results popup");
				
				view.hideSearchResults();

				$("div.popupContent").html(popupContent);

				app.popup.show( $("#searchPopup"), view.searchInput, "bottom", "left-top");

				$(".searchResults .resultItem", view.el).on("click", function(event) {
					var result = $(event.target).closest(".resultItem"),
						resultsType = result.attr("resultsType"),
						itemType = result.attr("itemType"),
						id = result.attr("itemId"); 
					//console.log("itemType 1: "+itemType);
					if (resultsType == diary.model.SearchResultSection.RESULTSTYPE.DIARYITEM) {
						var diaryItem = app.model.get(ModelName.DIARYITEMS).get(id);
						
						if (diaryItem) {										
							view.showDiaryItem(diaryItem);
						}
					} else if (resultsType == diary.model.SearchResultSection.RESULTSTYPE.CONTENTITEM) {
						var contentItem = app.model.get(ModelName.SCHOOLPROFILE).get('contentMapping').getArticles().get(id);
						if (contentItem) {
							view.showContentItem(contentItem, itemType);
						}
					}
					else if (resultsType == diary.model.SearchResultSection.RESULTSTYPE.CLASSES) {
						//console.log("class clicked itemType 2"+itemType);
						view.showContentItem(id, itemType);
						
					}
					 else if (resultsType == diary.model.SearchResultSection.RESULTSTYPE.SUBJECTS) {
						view.showContentItem(id, itemType);
					}
					view.hideSearchResults();
				});
			
			}
			
		}, 
		
		/*
		 * close search results box and stop any searches that might be happening
		 */
		hideSearchResults : function() {
			app.popup.hide($(".searchResults"));						
		}, 
		
		showDiaryItem: function(diaryItem,fromAssigned) {
			var view = this,
				selectedDate = null;

            if (diaryItem instanceof diary.model.Note || diaryItem instanceof diary.model.Message) {

                selectedDate = diaryItem.get('atDay');

            } else if (diaryItem instanceof diary.model.Task) {

				if(fromAssigned){
					selectedDate = diaryItem.get('assignedDay');
				} else {
					selectedDate = diaryItem.get('dueDay');
				}

            } else if (diaryItem instanceof diary.model.Event) {

                selectedDate = diaryItem.get('startDay');

            } else {

                throw new Error ("Diary Item is not supported");

            }

			view.showDiaryItemOrContentDayView(selectedDate, diaryItem);						
		}, 
		
		showContentItem: function(contentItem, itemType) {
		    //console.log("showContentItem"+itemType+"diary.model.SearchResultSection.ITEMTYPE.CLASS : "+diary.model.SearchResultSection.ITEMTYPE.CLASS);
			var view = this;
			
			$(".calloutarea_search").hide();
			$(".black_overlay_callout").css("display","none");
			$(".calloutarea_search").removeClass("search");
			
			if (itemType == diary.model.SearchResultSection.ITEMTYPE.SCHOOL || 
				itemType == diary.model.SearchResultSection.ITEMTYPE.EMPOWER) {
				
				var shortcut = app.model.get(ModelName.SCHOOLPROFILE).get('contentMapping').getShortcut(contentItem);

                // try to get the default shortcut if non found, the shortcut is being used to
                // determine what tab to display
                if (!shortcut) {
                    shortcut = app.model.get(ModelName.SCHOOLPROFILE).get('contentMapping').getDefaultShortcut();
                }

				if (shortcut) {

                   // console.log("switching to view");
					
					var footerLinkTag = view.getFooterLinkTagByName(shortcut.get('title')).linkTag;
					//console.log(footerLinkTag);
					
					// switch to content view
					view.switchToView(footerLinkTag, {
							'article' : shortcut,
							'articleToShow' : contentItem, 
							'mustRender' : true, 
							'forceRefresh': true
					});

				} else {

                    //console.log("Could not find shortcut for article", JSON.stringify(contentItem));

				}			
				
				
			} else if (itemType == diary.model.SearchResultSection.ITEMTYPE.BANNER) {

				view.showDiaryItemOrContentDayView(contentItem.get('fromDate'));

			}			
			else if(itemType == diary.model.SearchResultSection.ITEMTYPE.CLASS || itemType == diary.model.SearchResultSection.ITEMTYPE.SUBJECT){
			 		//console.log("class or subject"+contentItem);
			 		view.switchToView('subject');
			 		if(itemType == diary.model.SearchResultSection.ITEMTYPE.CLASS){
			 			var classes = app.model.get(ModelName.CLASSES);
			 			var classObj = getObjectByValue(classes, contentItem, "classId")
			 			var timeTableIdForClass = classObj.get("timeTableID");
						var view = new diary.view.SubjectView,
							timetableId = timeTableIdForClass,
							timetable = app.model.get(ModelName.CALENDAR).get('timetables').get(timetableId);
						view.timetableId = timetableId;
						
						if (timetableId && timetable) {
							view.setTimetable(timetable);
						}
			 		
			 		}
			 		else{
			 			var subjects = app.model.get(ModelName.SUBJECTS);
			 			var classes = app.model.get(ModelName.CLASSES);
			 			var objSubject = subjects.get(contentItem);
			 			var classObj = getObjectByValue(classes, objSubject.get("classUniqueID"), "classId")
			 		}
			 		
			}
		}, 
		
		showDiaryItemOrContentDayView : function(selectedDate, diaryItem) {

			$(".calloutarea_search").hide();
			$(".black_overlay_callout").css("display","none");
			$(".calloutarea_search").removeClass("search");
				
			app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', selectedDate);
			app.model.get(ModelName.APPLICATIONSTATE).set('setToday', true);
			// switch to day view and make sure it renders
			this.switchToView("day", {
				dayToView: selectedDate,
				forceRefresh: true
			}); // this will re-render the view						

		}, 
		
		getFooterLinkTagByName: function(name) {
			return _.find(this.footerItems, function(footerItem) {
				return footerItem.name == name;
			});			
		}
		
	});

	return mainView;

})();