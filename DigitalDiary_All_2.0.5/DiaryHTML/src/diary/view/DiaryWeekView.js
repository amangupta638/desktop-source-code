diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};
var rendered = false;
var tempHtml = "";
var selectedDivPosition = 0;
diary.view.DiaryWeekView = (function() {
	
	var view = Rocketboots.view.View.extend({

		events: {
			"click .weekView .weekDayAdd"		    : "showDiaryItemForm", 		// pop-up for add new diary item
			"click .weekView .weekEvents li"	    : "showDiaryItemForm",
			"click .weekView .diaryItem"	        : "showTasksInPopup",		// pop-up for tasks
			"dblclick .weekView .diaryItem"	        : "showDiaryItemForm",		// pop-up the edit for single subject
			"click .weekView .calendarDateSelect"   : "selectWeek",				// week selector
			"click .weekView .calendarNav li"	    : "changeWeek",				// previous/next week
			//"click .weekView .weekViewDay"		: "selectDay",				
			"click .weekView .weekViewDay"	    	: "navigateToDay",			// navigate to a particular day
			"dragstart .weekView .diaryItem"	    : "startDragWidget",			// start dragging a diary item
			"onload"								: "renderWeekView",
			"click .jTscroller .ts2Ancher"		    : "weekSelected",
			"click .studCalendarAccess"				: "accessStudCalendar"
		},

		initialize: function() {	
			//console.log("INITIALIZE WEEK VIEW");
			Rocketboots.view.View.prototype.initialize.call(this);
			//this.todaysWeek();
			var view = this,
				updateWeek = function () {
				//console.log("DiaryWeekView="+app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'));
					var selectedWeek = app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay').getWeek();
					//console.log("selectedWeek= "+selectedWeek);
					//console.log("view.currentWeek="+view.currentWeek);
					if (view.currentWeek != selectedWeek) {
						view.currentWeek = selectedWeek;
						view.isDirty = true;
					}
					view.mapWeek();
				},
				updateCalendar = function() {
					view.mapWeek();
					view.isDirty = true;
					view.render();
				};

			//console.log('DiaryWeekView.initialize()');
			//app.model.get(ModelName.APPLICATIONSTATE).on("change:" + 'selectedDay', updateWeek, this);
		
			app.listenTo(app.model.get(ModelName.APPLICATIONSTATE),"change:" + 'selectedDay', updateWeek);
			//app.model.on("change:" + ModelName.CALENDAR, updateCalendar, this);
			app.listenTo(app.model, "change:" + ModelName.CALENDAR, updateCalendar);
			
			// listen to the event so that we would know when to refresh the list of diary items that are being displayed
			// NOTE - The Diary View (the base class of all calendar views) is listening to the three different events that
			// are triggered by the diary item collection
			app.context.on(EventName.REFRESHDIARYITEMS, updateCalendar, this);

			view.isAlwaysDirty = true;
			view.week = {};
			updateWeek();

			// render the screen
			view.render();

			view.dropTarget = new diary.view.DropTarget({
				el: $(".weekViewCalendar", this.$el)
			}).register(
				this.dragEnterWeekView,
				this.dragLeaveWeekView,
				this.dropOnWeekView,
				null,
				this
			);

			view.dragItem = {
				'item': null,
				'fromDay': null,
                'isDue' : false
			};

		},

		parseAdditionalParams: function(options) {
			if (typeof(options.el) !== "undefined") {
				this.el = options.el;
			}
		},

		mapWeek: function() {
			var calendarModel = app.model.get(ModelName.CALENDAR),
				diaryItemsModel = app.model.get(ModelName.DIARYITEMS).getDiaryItems(),
				day = null;
            //console.log("this.currentWeek="+this.currentWeek);
			// update the generated week
			//console.log("calendarModel");
			console.log("calendarModel : " + calendarModel)
			console.log("diaryItemsModel : " + diaryItemsModel);
			//console.log(diaryItemsModel);
			this.week = diary.view.DiaryCalendarViewUtil.mapWeek(calendarModel, diaryItemsModel, this.currentWeek);

			_.each(this.week.days, function(d) {
				d.enableAddButton = true;
			});

			// Append dummy days (if not enough days in the week)
			while (this.week.days.length < 7) {
				if (this.currentWeek == 0) {
					day = new Rocketboots.date.Day(this.week.days[0].dateObject);
					day.addDays(-1);
					this.week.days.splice(0, 0, {
						'date' : day.getDate().getDate(),
						'dayOfWeek' : diary.view.DiaryCalendarViewUtil.daysOfWeek[day.getDate().getDay()],
						'othermonth' : true,
						'dateObject' : day.getDate()
					});
				} else { // week 52 or 53
					day = new Rocketboots.date.Day(this.week.days[this.week.days.length-1].dateObject);
					day.addDays(1);
					this.week.days.push({
						'date' : day.getDate().getDate(),
						'dayOfWeek' : diary.view.DiaryCalendarViewUtil.daysOfWeek[day.getDate().getDay()],
						'othermonth' : true,
						'dateObject' : day.getDate()
					});
				}
			}

			// If we needed to append dummy days, update the week description.
			if (day != null) {
				this.week.dateStart = this.week.days[0].dateObject;
				this.week.dateEnd = this.week.days[this.week.days.length-1].dateObject;
				this.week.week = diary.view.DiaryCalendarViewUtil.dateRangeDescription(this.week.dateStart, this.week.dateEnd);
			}
			//console.log("this.week");
			//console.log(this.week);
		},
		
		accessStudCalendar : function(event) {
			
			openCalendarAccess();
		},

		render: function() {
			
			var updateWeekCalendar = function() {
				view.isDirty = true;
				view.render();
			};
			app.model.get(ModelName.DIARYITEMS).off("change");
			app.model.get(ModelName.DIARYITEMS).off("remove");
			app.model.get(ModelName.DIARYITEMS).off("add");
			app.model.get(ModelName.DIARYITEMS).on("change", updateWeekCalendar, this);
			app.model.get(ModelName.DIARYITEMS).on("remove", updateWeekCalendar, this);
			app.model.get(ModelName.DIARYITEMS).on("add", updateWeekCalendar, this);
			
			this.mapWeek();
			//this.renderDiaryItemForm();
		//console.log("RENDER WEEK VIEW");
			var view = this,
				today = new Rocketboots.date.Day(),
				isYear = today.getFullYear() == SystemSettings.HARDCODDEDYEAR;
			//console.log("this.week.week=");
			//console.log(view.week);
			
			var viewParams = {
				'disableToday' : !isYear,
				'dateNow' : getCurrentDate(),
				'dateRange' : this.week.week,
				'isTeacher'	: app.model.get(ModelName.STUDENTPROFILE).get("isTeacher"),
				'week' : this.week
			};
			
			$(view.el).html(app.render('diaryWeek', viewParams));
			//$(".cal_filter_task").show();
			//console.log("WEEK VIEW");
			//console.log(viewParams);
			//console.log("this.week");
			//console.log(this.week);
			
			$(".jTscroller a", view.el).on("click", function(event) {
				alert('jTscroller called : ');
				var jumpToDay = new Rocketboots.date.Day(new Date(1.0 * $(event.target).attr('timestamp')));

				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "WEEK", jumpToDay.getKey());

				app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', jumpToDay);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			});

			//$(".cal_tbw_right .cal_filter_task").show();

			if (view.dropTarget) {
				view.dropTarget.setElement( $(".weekViewCalendar", view.$el) );
			}
			
			view.selectWeek();
			
			if(selectedWeekView != ""){
				selectedDivPosition = 0;
				$("#diaryWeekDatePicker").find(".active").removeClass("active");
				$(".weekfromto").each(function(){
					selectedDivPosition = selectedDivPosition + 80;
					if($(this).attr("timestamp") == selectedWeekView){
						$(this).parent().addClass("active");
						$("#containerWeekList").scrollLeft(selectedDivPosition);
      						//$("#containerWeekList").css("scrollLeft", selectedDivPosition);
					}
					
				});
			}
			
		},

		renderDiaryItemForm: function() {
			var view = this,
				viewParams = {};

			viewParams.el = $('#weekDiaryItemFormEditPopup', this.el);
			
			viewParams.editorref="editor2";
			viewParams.classes="weekDiaryItemFormEditPopup";
			view.diaryItemForm = new diary.view.DiaryItemView(viewParams);
			
		},

		showTasksInPopup: function(event) {

			var view = this,
				// get the diary items
				diaryItemsModel = app.model.get(ModelName.DIARYITEMS),
				// get the identifier of the diary item that we need to display
				diaryItemIds = $(event.currentTarget).attr('diaryItemIds'),
                dayKey = $(event.currentTarget).closest('.weekViewDay').attr('dayKey');

			if (diaryItemIds) {

				// render the content of the popup
				var popupContent = app.render('diaryItemsSummary', {'items' : diary.view.DiaryCalendarViewUtil.mapTasksForPopup(diaryItemsModel,
                                                                                                                                diaryItemIds,
                                                                                                                                Rocketboots.date.Day.parseDayKey(dayKey)) });

				app.popup.hide($("#weekPopup"));
				$("#weekPopup>div.popupContent").html(popupContent);

				app.popup.show( $("#weekPopup"), $(event.currentTarget), "right", null, event, $(".weekViewCalendar", view.el) );

				$("#weekPopup .diaryItemSummary .infolink", this.el).on("click", function(event) {
					view.showDiaryItemForm(event);
				});
				
				$(".webLinkDiaryItem", this.el).on("click", function(event) {				
					var link = prepareWeblinkURL($(event.currentTarget).attr('webLinkURL'));
					var urlReq = new air.URLRequest(link); 
					air.navigateToURL(urlReq);
				});

				$("#weekPopup .diaryItemSummary .noteEntry .save", this.el).on("click", function(event) {
					// get the note that was entered
					var noteEntryElement = $("#weekPopup .diaryItemSummary .noteEntry .noteEntry"),
						noteId = parseInt(noteEntryElement.attr("noteId"), 10),
						noteText = noteEntryElement.val().trim();

					// append the note
					diary.view.DiaryView.appendNote(noteId, noteText);
					app.popup.hide($("#weekPopup"));
					openMessageBox("Diary item updated successfully.");
				});
				
				$("#weekPopup .diaryItemSummary .noteEntry .delete", this.el).on("click", function(event) {
                // get the note that was entered
                var hel = confirm("Are you sure to delete this note ?");
			
					if(hel)
					{
               			var noteEntryElement = $("#weekPopup .diaryItemSummary .noteEntry .noteEntry"),
                    	noteId = parseInt(noteEntryElement.attr("noteId"), 10),
                    	noteText = noteEntryElement.val().trim();
                		DeleteNoteEntryMethod(noteId);
               			app.popup.hide($("#weekPopup"));
                	}
                	else
                		return;
                
                
            	});

				$("#weekPopup .diaryItemSummary ul.attachments > li.attachment").on("dblclick", view.openDiaryItemAttachment);

				$("#weekPopup .diaryItemSummary .summaryWidgetItem").on("dragstart", function(event) {
					view.startDragWidget(event);
				});
			}

		},

		selectWeek: function(event) {

			var popupParams = {
					'weeks': []
				},
				now = new Rocketboots.date.Day(),
				startDate = app.model.get(ModelName.APPLICATIONSTATE).get('startDate'),
				endDate = app.model.get(ModelName.APPLICATIONSTATE).get('endDate'),
				startYear = startDate.getFullYear(),
				endYear = endDate.getFullYear();
			
			var d = new Date(startDate);
			var day = d.getDay(),
			diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
			var result = new Date(d.setDate(diff));
			this.week.dateStart = new Rocketboots.date.Day(result);
			this.week.dateEnd = new Rocketboots.date.Day(this.week.dateStart).addDays(6);
			
			//console.log("now="+now);
			//console.log('year',year);
			//for(var i=-4; i<=4; i++) {
			for(var i=0; i<=52; i++) {
				// Calculate the start and end of the
				var dateStart, dateEnd;
				dateStart = new Rocketboots.date.Day(this.week.dateStart);
				dateEnd = new Rocketboots.date.Day(this.week.dateEnd);
				dateStart.addDays(7*i);
				dateEnd.addDays(7*i);
				//console.log("dateStart="+dateStart+" dateEnd="+dateEnd);
				// Ensure the date range has overlap with the current year
				if (dateStart.getFullYear() == startYear || dateEnd.getFullYear() == startYear || dateStart.getFullYear() == endYear || dateEnd.getFullYear() == endYear) {
					popupParams.weeks.push(
						{
							weekText: diary.view.DiaryCalendarViewUtil.dateRangeDescription(dateStart.getDate(), dateEnd.getDate()),
							weekTimestamp: (dateStart.getFullYear() == startYear ? dateStart : dateEnd).getTime(),
							isThisWeek: (now.getWeek() == this.currentWeek + i && now.getFullYear() == startYear),
							weekNumber: i
						}
					);
				}
			}
			
		
			popupContent = app.render('weekList', popupParams);
	
			$("#diaryWeekDatePicker", this.el).html(popupContent);
			if(selectedWeekView != ""){
				selectedDivPosition = 0;
				
				$(".weekfromto").each(function(){
					selectedDivPosition = selectedDivPosition + 80;
					if($(this).attr("timestamp") == selectedWeekView){
						console.log("change scrolllist"+selectedDivPosition);
						//$(this).parent().addClass("active");
						$("#containerWeekList").scrollLeft(selectedDivPosition);
      					
					}
					
				});
			}
		},

		changeWeek: function(event) {
			var $element = $("div", event.currentTarget);
			if ( $element.hasClass("navLeft") ) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_PREVIOUS, "WEEK");
				this.previousWeek();
			}
			else if ( $element.hasClass("navRight") ) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_NEXT, "WEEK");
				//console.log("calling next week");
				this.nextWeek();
			}
			else if ( $element.hasClass("navToday") && !$element.hasClass('disabled')) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "WEEK", "TODAY");
				this.todaysWeek();
			}
		},

		previousWeek: function() {
			var view = this,
				newEndDate = new Rocketboots.date.Day(view.week.dateEnd),
				calendarModel = app.model.get(ModelName.CALENDAR);

			newEndDate.addDays(-7); // go back 1 week
			if(!isSingleYear)
			{
				var count=view.week.dateEnd.getDate();
				//console.log("count="+count);
				var monthname=monthNames[newEndDate.getMonth()];
				var month=$.inArray(monthname,monthArray);
				//console.log("nextWeek month="+month);
				//console.log("nextWeek newStartDay="+newStartDay.getDate().getDate());
				//if (newEndDate.getFullYear() == calendarModel.get('year')) {
				
				if(month==11)
				{
					if(count-7 <=0)
						return;
				}
			
				app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', newEndDate);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
			else
			{	
				if (newEndDate.getFullYear() == calendarModel.get('year')) {
				app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', newEndDate);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
			}
		},

		nextWeek: function() {
		
			var view = this,
				newStartDay = new Rocketboots.date.Day(view.week.dateStart),
				calendarModel = app.model.get(ModelName.CALENDAR);
		  //  console.log("nextWeek view.week.dateStart="+view.week.dateStart);
			newStartDay.addDays(7); // go forward 1 week
			if(!isSingleYear)
			{
			var count=view.week.dateStart.getDate();
			//console.log("count="+count);
			var monthname=monthNames[newStartDay.getMonth()];
			var month=$.inArray(monthname,monthArray);
			//console.log("nextWeek month="+month);
			//console.log("nextWeek newStartDay="+newStartDay.getDate().getDate());
			
			//if (newStartDay.getFullYear() == calendarModel.get('year')) {
	
			if (month<=11) 
			{
			
					if(month ==0)
					{
						if((month%2)==0)
						{
						if((count+7) >=31)
					      return;
					    }
					      else
					      {
					      	if((count+7) >=30)
					        return;
					      }
					}
			
					{
						app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', newStartDay);
						// call the refresh calendar so that the calendar will be refreshed
						app.context.trigger(EventName.REFRESHCALENDAR);
					}
			}
		}
		else
		{
				if (newStartDay.getFullYear() == calendarModel.get('year')) {
				app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', newStartDay);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
		}
				
			
		},

		todaysWeek: function() {
			
			//var fromDate = new Date('01/17/2012');
			var now=new Date();
			var monthname=monthArray[0];
			var monthIndex=$.inArray(monthname, monthNames);
			//console.log("monthIndex="+monthIndex);
			var currentIndex=now.getMonth();
		//	console.log("currentIndex="+currentIndex);
			var i=currentIndex+monthIndex;
			if(i>11)
			{
			i=i-11;
			}
            var toDate = new Date(new Date().setMonth(i));
           // console.log("toDate="+toDate);
            var today = new Rocketboots.date.Day(toDate);
          //  console.log("today= "+today);
            now = new Date();
		    var dateIndex=now.getDate();
            var index=parseInt(now.getMonth())
            var month=monthNames[index];
            var monthIndex=$.inArray(month, monthArray);
			app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(yearArray[monthIndex], index, dateIndex));
			//app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', today);
			// call the refresh calendar so that the calendar will be refreshed
			app.context.trigger(EventName.REFRESHCALENDAR);
		},

		selectDay: function(event) {
			diary.view.DiaryView.selectDay(event);
		},

		navigateToDay: function(event) {
			app.model.get(ModelName.APPLICATIONSTATE).set('setToday', true);
			diary.view.DiaryView.selectDay(event);
			diary.view.DiaryView.navigateToDay(event);
		},

		showDiaryItemForm: function(event) {
	
	
			//console.log("showDiaryItemForm : ");
			var view = this;
			
			var diaryItemId = $(event.currentTarget).attr('diaryItemId') || null;
            var  calendarDay;

			if (diaryItemId == null) {
				// try to get the dayKey
				var dayKey = $(event.currentTarget).parents(".weekViewDay") && $(event.currentTarget).parents(".weekViewDay:first").attr("dayKey");
				if (dayKey) {
                    calendarDay = Rocketboots.date.Day.parseDayKey(dayKey);
				} else {
                    // this should never happen as the day in the week view should contain the
                    // date of the day
                    calendarDay = app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay');
                }
			}
			//console.log("showDiaryItemForm : calendarDay : "+calendarDay);
			if(diaryItemId)
			{
			//console.log("inside if");
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT UserID,createdBy,type FROM diaryItem WHERE id = :diaryItemId",

            // params
            {
			'diaryItemId' :diaryItemId
            },
			// success
			function(statement) {
			
				var detail=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    
                 if(detail.length)
                 {
                 		var userID=detail[0].UserID;
                 		var createdBy=detail[0].createdBy;
                 		var type=detail[0].type;
                 		if(createdBy!=null && userID==createdBy)
                 		{
                 			//console.log("createdBy diary Item Match");
						
                 		 diaryItemForm.showDialog(diaryItemId,
                                          calendarDay,'','','',createdBy);
                                   $(".editButtonContainer .save").show();
                                   $(".editButtonContainer .delete").show();
                                   $(".editButtonContainer .lockItem").hide();
                                   $(".ui-dialog-titlebar").hide();
                 		}
                 		else
                 		{
                 			//console.log("createdBy Match FALSE");
                 			diaryItemForm.showDialog(diaryItemId,
                                          calendarDay,'','','',createdBy);
                 			disablediaryitemformWeek();
                 			if(type == 'EVENT'){
                           		$(".editButtonContainer .save").hide();
                            } else {
                            	$(".editButtonContainer .save").show();
                            }
                 			$(".editButtonContainer .delete").hide();
                 			$(".editButtonContainer .lockItem").show();
                 			$(".ui-dialog-titlebar").hide();
                 		}
                 }
			
			},
			
			// error
			function error()
			{
				console.log("Error while trying to create ClassUser item");
			}
	);
	}
	else
	{
	
		diaryItemForm.showDialog(diaryItemId, calendarDay, '','','','',null,true,true);
                                          
         $(".editButtonContainer .save").show();
         $(".editButtonContainer .delete").hide();
         $(".editButtonContainer .lockItem").hide();
         $(".ui-dialog-titlebar").hide();
	}

			
			event.stopPropagation();
		},

		openDiaryItemAttachment: function (event) {
			var target = $(event.target),
				diaryItemId = target.closest("article").find(".infolink").attr('diaryItemId'),
				attachmentId = target.closest(".attachment").attr('attachmentId'),
				diaryItem = null,
				attachment = null;

			if (diaryItemId && attachmentId) {
				diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId);
				attachment = diaryItem.get('attachments').get(attachmentId);
				if (diaryItem && attachment) {
					app.context.trigger(EventName.OPENATTACHMENT, diaryItem, attachment);
				}
			}
			event.stopPropagation();
		},

		/*
		 * Drag and drop
		 */

		clearDropTarget: function() {
			var view = this,
				$target = null;

			if (view.dropTarget.target != null) {
				if (view.dropTarget.type == "day") {
					$target = $(".weekViewDay[dayKey=\"" + view.dropTarget.target + "\"]", view.$el);
				} else if (view.dropTarget.type == "diaryitem") {
					$target = $(".weekDiaryItemWidget[diaryItemId=\"" + view.dropTarget.target + "\"]", view.$el);
				}
				$target && $target.removeClass("dropTarget");

				// At some point, AIR decides to start dispatch DragLeave events prior to the Drop. So we need to clear the selected day,
				// but retain a reference to it so it can be used by the Drop that immediately follows
				view.dropTarget.active = false;
			}
		},

		setDropTarget: function($target, target, type, active) {
			var view = this;

			view.dropTarget.setTarget(target, type, active);
			$target.addClass('dropTarget');

		},

		/*
		 * Dragging
		 */

		dragEnterWeekView: function(event) {

			var view = this,
				$target = $(event.target),
				isDiaryItemDrop = event.dataTransfer.types == null,
				isFileDrop = view.dropTarget.isFileDrop(event),
				$diaryItem = $target.closest('.diaryItem'),
				$dayCell = $target.closest('.weekViewDay');

			var overTarget = false;

			if ($diaryItem.length > 0 && isFileDrop && ($diaryItem.hasClass('assignment') || $diaryItem.hasClass('homework'))) { // we are dragging over a diary item
				overTarget = view.dragEnterDiaryItem(event, $diaryItem, isDiaryItemDrop, isFileDrop);
			} else if ($dayCell.length > 0) { // Dragging over a day
				overTarget = view.dragEnterWeekDay(event, $dayCell, isDiaryItemDrop, isFileDrop);
			}

			if (!overTarget && view.dropTarget.target) {
				view.clearDropTarget();
				view.dropTarget.reset();
				event.dataTransfer.dropEffect = 'none';
			}
		},

		dragEnterDiaryItem: function(event, $diaryItem, isDiaryItemDrop, isFileDrop) {
			var view = this,
				overTarget = false,
				diaryItemId  = $diaryItem.attr('diaryItemId'),
				diaryItemIds = $diaryItem.attr('diaryItemIds');

			if (isFileDrop) {
				if (diaryItemIds.split(',').length > 1) {
					// Dragging over a multiple diary item
					//console.log('Dragging over a multiple summary widget');

				} else {
					// Dragging over a single diary item

					if (view.dropTarget.type != "diaryitem" || view.dropTarget.target != diaryItemId) {
						// Dragging over this diary item for the first time
						view.clearDropTarget();
						view.setDropTarget($diaryItem, diaryItemId, "diaryitem", true, null);
						overTarget = true;
					} else if (view.dropTarget.type == "diaryitem" && view.dropTarget.target == diaryItemId) {
						// Dragging over this already highlighted diary item
						overTarget = true;
					}
				}
			} // else if isDiaryItemDrop, do nothing as you can't drop a diary item over a diary item

			return overTarget;
		},

		dragEnterWeekDay: function(event, $dayCell, isDiaryItemDrop, isFileDrop) {
			var view = this,
				overTarget = false,
				dayKey = $dayCell.attr("dayKey");

			if (typeof(dayKey) != "undefined") {

				if (isDiaryItemDrop && dayKey == view.dragItem.fromDay.getKey()) {
					// Can't drop over the same day
					if (view.dropTarget.target) {
						view.clearDropTarget();
						view.dropTarget.reset();
					}
					overTarget = false;
				} else if ((isDiaryItemDrop && dayKey != view.dragItem.fromDay.getKey()) || isFileDrop) {
					if (dayKey != view.dropTarget.target || !view.dropTarget.active) {
						view.clearDropTarget();
						view.setDropTarget($dayCell, dayKey, "day", true);
						event.dataTransfer.dropEffect = 'copy';
						overTarget = true;
					} else {
						// Already over the target
						//console.log("we are already over the target");
						overTarget = true;
					}
				}
			}
			return overTarget;
		},

		dragLeaveWeekView: function(event) {
			var view = this;

			// Check if the mouse is still within the container
			var container =  $(".weekViewCalendar", this.$el),
				position = $(event.currentTarget).offset();

			var isStillOverContainer = event.x >= position.left && event.y >= position.top && event.x <= (position.left + container.outerWidth()) && event.y <= (position.top + container.outerHeight());
			if (isStillOverContainer) return;

			// If outside the container, then clear any current drop target
			view.clearDropTarget();
			view.dropTarget.reset();

		},

		/*
		 * Dropping
		 */

		dropOnWeekView: function(event) {
			var view = this,
				isDiaryItemDrop = event.dataTransfer.types == null,
				isFileDrop = view.dropTarget.isFileDrop(event);

			if (isDiaryItemDrop && view.dropTarget.target) {
				//console.log('Week View: Drop targetDay=' + view.dropTarget.target + ' originalDay:' + view.dragItem.fromDay.getKey());
				if (view.dropTarget.target && view.dragItem.fromDay.getKey()) {
					view.dropOnWeekDay(event, isFileDrop);
				}
			} else if (isFileDrop) {
				//console.log('Week View: Drop target=' + view.dropTarget.target + ' from File');
				if (view.dropTarget.target && view.dropTarget.type == "diaryitem") {
					view.dropOnDiaryItem(event, isFileDrop);
				} else if (view.dropTarget.target && view.dropTarget.type == "day") {
					view.dropOnWeekDay(event, isFileDrop);
				}
			}

			view.clearDropTarget();
			view.dropTarget.reset();
		},

		dropOnWeekDay: function(event, isFileDrop) {
			var view = this;

			//console.log('Dropping onto week day ' + view.dropTarget.target);

			if (isFileDrop) {
				var files = event.dataTransfer.getData('application/x-vnd.adobe.air.file-list'),
					attachmentsToAdd = _.map(
						_.filter(files, function(file) { return !file.isDirectory; }),
						function(file) {
							return file.nativePath;
						}
					),
					calendarDay,
                    defaultValues = {};

                calendarDay = Rocketboots.date.Day.parseDayKey(view.dropTarget.target);

				// If there is a single attachment, derive the title from it
				if (attachmentsToAdd.length == 1) {
					var fileProperties = diary.view.air.ViewFilesystemUtil.getFileProperties(attachmentsToAdd[0]);

					if (fileProperties.title) {
                        defaultValues.title = fileProperties.title;
					}
					if (fileProperties.description) {
                        defaultValues.description = fileProperties.description;
					}
				}

				if (attachmentsToAdd.length == 0) return;
				diaryItemForm.showDialog(null,
                                              calendarDay,
                                              null,
                                              null,
                                              defaultValues,
                                              attachmentsToAdd);
			} else {

				app.context.trigger(EventName.UPDATEDIARYITEMONDROP,
                                    view.dragItem.item,
                                    view.dragItem.isDue,
						            Rocketboots.date.Day.parseDayKey(view.dropTarget.target));

			}

		},

		dropOnDiaryItem: function(event, isFileDrop) {
			var view = this;

			if (!isFileDrop) return;

			var files = event.dataTransfer.getData('application/x-vnd.adobe.air.file-list'),
				attachmentsToAdd = _.map(
					_.filter(files, function(file) { return !file.isDirectory; }),
					function(file) {
						return file.nativePath;
					}
				);

			if (attachmentsToAdd.length == 0) return;
			diaryItemForm.showDialog(view.dropTarget.target,
                                          null,
                                          null,
                                          null,
                                          null,
                                          attachmentsToAdd);

		},

		startDragWidget: function(event) {
			//console.log('WeekView: startDragWidget');

			// TODO: Handle multiple diary item widget

			var view = this,
                isDue = $(event.originalEvent.target).hasClass('due'),
				diaryItemId = $(event.originalEvent.target).attr('diaryItemId');

			if (typeof(diaryItemId) == "undefined" || diaryItemId == null || diaryItemId == "") {
				event.originalEvent.dataTransfer.effectAllowed = "none";
				return;
			}

			var diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId),
                fromDay = null;

            // although only events will ever be locked and you can't drag events
            if (diaryItem.get('locked')) {
                event.originalEvent.dataTransfer.effectAllowed = "none";
                return;
            }

            if (diaryItem instanceof diary.model.Note || diaryItem instanceof diary.model.Message) {

                fromDay = diaryItem.get('atDay');

            } else if (diaryItem instanceof diary.model.Task) {

                if (isDue) {
                    fromDay = diaryItem.get('dueDay');
                } else {
                    fromDay = diaryItem.get('assignedDay');
                }

            } else if (diaryItem instanceof diary.model.Event) {

                fromDay = diaryItem.get('startDay');

            } else {

                throw new Error ("Diary Item is not supported");

            }

			view.setDragItem(diaryItem, isDue, fromDay);

			event.originalEvent.dataTransfer.effectAllowed = "move";

			//console.log('WeekView: startDragWidget id:' + diaryItem.get('id') + ' fromDay:' + fromDay + ' moveable:' + diaryItem.get('moveable'));

			// setData serialises the value rather than passing by reference.  So we need to pass simple values around
			/*
			event.originalEvent.dataTransfer.setData("diary/diary-item-id", diaryItemId);
			event.originalEvent.dataTransfer.setData("diary/diary-item-date-key", dateKey.toString());
			event.originalEvent.dataTransfer.setData("text/plain", "diaryitem://" + diaryItemId + "/" + dateKey);
 			*/
		},

		setDragItem: function(diaryItem, isDue, fromDay) {
            // TODO this is not the cleanest solution for dragging diary items,
            // take a look at the day view for a much neater solution. The
            // drag code for this view will eventually have to be updated
            // to make it cleaner
            var view = this;
			view.dragItem.item = diaryItem;
			view.dragItem.fromDay = fromDay;
            view.dragItem.isDue = isDue;
		}, 
		weekSelected : function(event){
				selectedWeekView = $(event.target).attr('timestamp');
				var jumpToDay = new Rocketboots.date.Day(new Date(1.0 * $(event.target).attr('timestamp')));
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "WEEK", jumpToDay.getKey());
				app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', jumpToDay);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
				
		},
		
		renderWeekView: function () {
			$("#tS2").thumbnailScroller({
				scrollerType:"hoverPrecise",
				scrollerOrientation:"horizontal",
				scrollSpeed:2,
				scrollEasing:"easeOutCirc",
				scrollEasingAmount:600,
				acceleration:4,
				scrollSpeed:800,
				noScrollCenterSpace:5,
				autoScrolling:0,
				autoScrollingSpeed:2000,
				autoScrollingEasing:"easeInOutQuad",
				autoScrollingDelay:500
			});
		
		
		}

	});

	return view;

})();
