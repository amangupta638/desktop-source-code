diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.FetchNextUpdatedContentPackageCommand = (function() {

	var _command = function() {
		//console.log('FetchNextUpdatedContentPackageCommand()');

		this._contentManager = app.locator.getService(ServiceName.CONTENTMANAGER);
		this._documentManager = app.locator.getService(ServiceName.DOCUMENTMANAGER);
	};

	/**
	 * @param retryTimeout {?Number} Optional. The number of milliseconds between retries (upon failure).
	 * 			If not specified the value will be read from system settings.
	 */
	_command.prototype.execute = function(retryTimeout) {

		// holds the command itself
		var command = this,
				contentPackageQueue = app.model.get(ModelName.CONTENTPACKAGEQUEUE);

		// defaults
		if (!retryTimeout) retryTimeout = SystemSettings.CONTENTDOWNLOADRETRYINTERVAL;

		//console.log('FetchNextUpdatedContentPackageCommand.execute()');
		if (contentPackageQueue.size() == 0) {
			return;
		}

		var packageToDownload = contentPackageQueue.at(0);
		//console.log("packageToDownload=");
		//console.log(packageToDownload);
		command._contentManager.getContentPackageFromRemoteLocation(packageToDownload.get('package'),
				function onSuccess() {
					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.FETCHED_REMOTE_CONTENT, packageToDownload);
					// remove the package from the queue, only do this now as we use the queue as a record
					// of which packages are currently downloading
					contentPackageQueue.remove(packageToDownload);

					// tell the views that we have a new package
					app.context.trigger(EventName.CONTENTITEMSRELOADED);

					// start indexing this package in the background
					command._documentManager.indexEntirePackage(packageToDownload.get('package'),
							// success
							function () {
								// Invalidate the search cache
								app.context.trigger(EventName.CLEARSEARCHCACHE);
								// finally mark the package as updated and save it in the db
								// if any step fails before we get to this point, the next time
								// the diary launches, the package will be downloaded again
								packageToDownload.set('dateLastUpdated', packageToDownload.get('dateUpdated'));
								command._contentManager.addStoredContentPackage(packageToDownload, $.noop, console.log);
							},
							// error
							console.log);

					// we can now start downloading the next package (reset the timeout since this download was successful)
					app.context.trigger(EventName.FETCHNEXTCONTENTPACKAGE);
				},
				function onError(message) {
					//console.log(message);

					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchNextUpdatedContentPackageCommand", "getContentPackageFromRemoteLocation", packageToDownload.get('package'));

					// take this package off the front of the collection and move it to the back
					contentPackageQueue.remove(packageToDownload);
					contentPackageQueue.add(packageToDownload);

					//console.log(new Date(), "Waiting " + retryTimeout + "ms before fetching next content package");
					setTimeout(function() {
						app.context.trigger(EventName.FETCHNEXTCONTENTPACKAGE, retryTimeout*2);
					}, retryTimeout);

				}
		);
	};

	return _command;

})();
