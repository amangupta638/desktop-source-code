/**
 * The command that is to be executed at the start of the application.  This command will be responsible to create
 * the basic blocks required for the application to function and will fire off the initial data requirements
 * 
 *  @author Brian Bason
 */
diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.InitApplicationCommand = (function() {
				
	/**
	 * Construct the Startup Command
	 * 
	 * @constructor
	 */
	 var Demo=false;
	var _command = function() {
		//console.log('InitApplicationCommand()');
		
		// get all the required managers from the application context
		this._appState = app.model.get(ModelName.APPLICATIONSTATE);
		this._database = app.locator.getService(ServiceName.APPDATABASE);
		this._schemaManager = app.locator.getService(ServiceName.SCHEMAMANAGER);
		this._dataManager = app.locator.getService(ServiceName.DEFAULTDATAMANAGER);
		this._demoDataManager = app.locator.getService(ServiceName.DEMODATAMANAGER);
		this._contentManager = app.locator.getService(ServiceName.CONTENTMANAGER);
	};
	
	/**
	 * Executes the command
	 */
	_command.prototype.execute = function() {
		// holds the command that is being executed
		var command;

		try {
			
			// keep track of the command since we need it later on
			command = this;
			//alert("args.ManifestLocation != null"+args.ManifestLocation != null);	
			//alert("args.ArchiveLocation != null===="+args.ArchiveLocation != null);	
			//alert("args.DemoArchiveLocation != null=="+args.DemoArchiveLocation != null);
			// 0 - determine mode the application has been launched in 			
			// determine the 'mode' this application has been launched in
			{
				if (args.ManifestLocation != null) {
					// we are in preview mode
					alert("PREVIEW MODE - reading local manifest file");
					command._appState.set('mode', diary.model.ApplicationState.MODE.PREVIEW);
					command._appState.set('manifestLocation', args.ManifestLocation);
					command._database.setDatabase('temp');

				} else if (args.ArchiveLocation != null) { // in proof mode
					alert("PROOF MODE - using local archive"); 
					command._appState.set('mode', diary.model.ApplicationState.MODE.PROOF);
					command._appState.set('archiveLocation', args.ArchiveLocation);
					command._database.setDatabase('temp');			
				} else if (args.DemoArchiveLocation != null) {
					alert("DEMO MODE - using local archive"); 
					command._appState.set('mode', diary.model.ApplicationState.MODE.DEMO);
					command._appState.set('archiveLocation', args.DemoArchiveLocation);
					command._database.setDatabase('temp');

					// Override the splash delay
					SystemSettings.SPLASHDELAY = SystemSettings.SPLASHDELAY_DEMOMODE;
				
				} else {
					// default is production mode
					command._appState.set('mode', diary.model.ApplicationState.MODE.PRODUCTION);
				}						
			}
			
			// 1 - setup any database stuff that is required
			{
				if (command._appState.get('mode') == diary.model.ApplicationState.MODE.PRODUCTION) {

					// Guard for old versions of the application
					if (command._database.databaseExists() && !command._schemaManager.isSchemaVersionSupported()) {

						// Display explanation of failure to user
						var shouldContinue = window.confirm("Your application has been upgraded from an unsupported version. This occurs when upgrading the 2012 diary to 2013 or later.\n\n" +
							"To continue you must DELETE ALL 2012 DATA.\n" +
							"Are you sure you want to continue?");

						if (shouldContinue) {

							//console.log("InitApplicationCommand - upgrading from 2012 to 2013 - deleting all user data!");

							// drop the database for a clean install
							command._schemaManager.dropDatabase();

							// delete the content directory
							command._contentManager.deleteAllDirectories();

						} else {
							// Close application
							app.context.trigger(EventName.EXITAPPLICATION);

							// Prevent the application initialising
							throw new Error("Attempted to run the application with an unsupported version of the database schema. The user chose 'cancel' and the app was closed.");
						}
					}

					// create/update the schema
					command._schemaManager.installSchemaUpdates();

					// install the database scripts for default data
					command._dataManager.installDefaultData();

				} else {
					// drop the database for a clean install
					command._schemaManager.dropDatabase();
					
					// create/update the schema
					command._schemaManager.installSchemaUpdates();

					// install the database scripts for default data
					command._dataManager.installDefaultData();

					if (command._appState.get('mode') == diary.model.ApplicationState.MODE.DEMO) {
						// install the database scripts
						command._demoDataManager.installDemoData();
					}
				}
				
			}
			
			// 2 - get system configuration and save macAddresses
			app.context.trigger(EventName.FETCHSYSTEMCONFIGURATION, function () {
				// update system configuration with values from container
				// TODO: move this AIR-specific code into a container package
				var descriptor = new DOMParser().parseFromString(air.NativeApplication.nativeApplication.applicationDescriptor + "","text/xml");
				var versionLabel = descriptor.getElementsByTagNameNS("*", "versionLabel")[0].textContent;
				var versionNumber = descriptor.getElementsByTagNameNS("*", "versionNumber")[0].textContent;
				app.model.get(ModelName.SYSTEMCONFIGURATION).set('versionNumber', versionNumber);
				app.model.get(ModelName.SYSTEMCONFIGURATION).set('versionLabel', versionLabel);
				app.model.get(ModelName.SYSTEMCONFIGURATION).set('macAddresses', args.MacAddresses);
				var SchoolUniqueId=app.model.get(ModelName.SCHOOLPROFILE).get("UniqueID");
				//console.log("Check="+args.Check)
				
				var year = SystemSettings.HARDCODDEDYEAR;
		        // holds the start date for the scholastic year
		        var startDate = app.model.get(ModelName.APPLICATIONSTATE).get('startDate');
		        // holds the end date for the scholastic year
		        var endDate = app.model.get(ModelName.APPLICATIONSTATE).get('endDate');
		        
		        
//		        app.locator.getService(ServiceName.APPDATABASE).runQuery(
//		            // query
//		            "UPDATE diaryItem SET hideFromDue = 0, hideFromAssigned = 0, hideFromNotifications = 0 ",
//		
//		            // params
//		            {
//					
//		            },
//					// success
//					function(statement) {
//					
//					},
//					
//					// error
//					function error()
//					{
//						
//					}
//				);
				app.context.trigger(EventName.FETCHSUBJECTS);
				app.context.trigger(EventName.FETCHDIARYITEMS, startDate, endDate, false, false, false);
				//get student profile to check if teacher or student
				app.context.trigger(EventName.FETCHSTUDENTPROFILE);
				// 3 - instantiate all the UI so it's ready whenever we need it
				if (typeof(app.view.tokenView) == 'undefined') {
					app.view.tokenView = new diary.view.air.ContainerTokenView();
				}
				if (typeof(app.view.loginView) == 'undefined') {
					app.view.loginView = new diary.view.air.ContainerLoginView();
				}
					
				if (typeof(app.view.themeView) == 'undefined') {
					app.view.themeView = new diary.view.air.ContainerThemeView();
				}

				if (typeof(app.view.dialogView) == 'undefined') {
					app.view.dialogView = new diary.view.air.ContainerDialogView();
				}


				if (typeof(app.view.mainView) == 'undefined') {
					// establish the main view
					app.view.mainView = new diary.view.MainView({
						el: 'body',
						model: app.model
					});
				}

				if (typeof(app.view.containerChromeView) == 'undefined') {
					// create the Container specific chrome
					app.view.containerChromeView = new diary.view.air.ContainerChromeView({
						el: 'body'
					});
				}


				/* if (typeof(app.view.notificationView) == 'undefined') {
					// generate the notification view for when it is required
					app.view.notificationView = new diary.view.air.ContainerNotificationView();
				} */

				var resizeIntervalId = -1;

				if (window.nativeWindow)
					window.nativeWindow.addEventListener("resize", function(event) {

						if (resizeIntervalId != -1) {
							clearInterval(resizeIntervalId);
							resizeIntervalId = -1;
						}

						resizeIntervalId = setInterval(function() {
							clearInterval(resizeIntervalId);
							app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.RESIZE_WINDOW, event.afterBounds.width, event.afterBounds.height);
							command._appState.set("windowHeight", event.afterBounds.height);
							command._appState.set("windowWidth", event.afterBounds.width);
						}, 1000);

					}, false);

				// 4 - Start application
				// 4 - Start application
				app.context.trigger(EventName.STARTAPPLICATION);
			});

		} catch (error) {
			
			// log the error so that we can keep track of what is going on
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "InitApplicationCommand", error);

			/**
			 * Re-throw the error to preserve logic flow when an exception is thrown.
			 * This is done because the above line doesn't actually handle the exception, it only logs it.
			 * By re-throwing, we preserve the concept of handled and unhandled exceptions.
			 */
			throw error;
		}
		
	};
	
	// return the created command
	return _command;
	
})();
