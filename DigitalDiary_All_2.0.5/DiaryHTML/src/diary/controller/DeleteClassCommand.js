diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.DeleteClassCommand = (function() {

	var _command = function() {
		//console.log('DeleteClassCommand()');

		this._manager = app.locator.getService(ServiceName.TIMETABLEMANAGER);
	};

	/**
	 * Deletes a class from the system
	 * 
	 * @param {!diary.model.Timetable} timetable The timetable to which the class is to be added to
	 * @param {!number} classId The identifier of the class that is to be deleted
	 */
	_command.prototype.execute = function(timetable, classId) {
		//console.log('DeleteClassCommand.execute()');

		var command = this,
			classModel = null;
		
		// we need to check that all the required details have been passed
		if (!timetable) {
			throw new Error("Timetable must be specified, timetable is not optional");
		}
		
		if (!classId) {
			throw new Error("Class Identifier must be specified, class is not optional");
		}
		
		function error(message) {
			//console.log("Error while trying to delete a Class - " + message);
			FlurryAgent.logError("Class Error","Error in deleting Class", 1);
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR,
                "DeleteClassCommand", message);

        }
		
		// we need to get the class that is being deleted from the
		// timetable
		classModel = timetable.get('classes').get(classId);
		
		// check that the class model was found
		if (classModel == null) {
			throw new Error("Class Identifier is not valid, class was not found in timetable");
		}
		
		// remove the class from the timetable as it is not required any longer
		command._manager.removeClassFromTimetable(timetable, classModel, 
				
			// the success operation of the remove class from timetable
			function(modifiedTimetable) {
			
				// holds the subject that is being referenced by the class
				//console.log("timetable delete=");
				//console.log(timetable);
				//console.log("classmodel delete");
				//console.log(classModel);
				var subject = app.model.get(ModelName.SUBJECTS).get(classModel.get('subject').get('id'));
				deleteClassUser();
				// we can now remove the class from the time table
				timetable.get('classes').remove(classId);
				
				// trigger the class removed event
				app.context.trigger(EventName.CLASSDELETED, classId);
                app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CLASS_DELETED, classId);
                
                
				
				// check if we need to delete the subject from the list of subjects
				// if it is not being used any more
				command._manager.deleteSubject(subject,
					
					// the success operation for deleting the subject
					function(wasDeleted) {
						
						// if the subject was deleted we need to remove it from the
						// collection of subjects available to the application
						if (wasDeleted) {
							// remove the subject from the collection since this is no longer
							// being used.
							// NOTE - this will fire off an event that the subject collection was changed
							app.model.get(ModelName.SUBJECTS).remove(subject);
						}
					
					}, 
				
					// the error operation for deleting the subject
					error
				
				);
				
			}, 
				
			// the error operation of the remove class from timetable
			error
		);

	};

	return _command;

})();
