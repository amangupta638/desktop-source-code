diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.FetchInboxItemsCommand = (function() {

	var _command = function() {
		this._newsAnnouncementManager = app.locator.getService(ServiceName.NEWSANNOUNCEMENTMANAGER);
	};

	//function to fetch messages, news and announcements.
	_command.prototype.execute = function (doneCallback) {
		//alert('FetchInboxItemsCommand');
		doneCallback = doneCallback || $.noop;
	    this._newsAnnouncementManager.getInboxItems(success, error);

		// the function that is to be executed on successful operation
		function success(inboxItems) {
			//console.log('FetchInboxItemsCommand '+JSON.stringify(inboxItems));
			app.model.get(ModelName.INBOXITEMS).reset(inboxItems.toArray());
			var notifications = app.model.get(ModelName.INBOXITEMS).getUnreadCount();
			var notificationForToday = app.model.get(ModelName.INBOXITEMS).getUnreadNotifications();
			var taskList = app.model.get(ModelName.DIARYITEMS).getNotifications();
			
			$(".notification_num").html(notifications);
			$("#notificationCount").html(taskList.length + notificationForToday);
			
			taskList = null;
			notificationForToday = null;
			notifications = null;
			
			doneCallback && doneCallback();
		}

		// the function that is to be executed on failure of the operation
		function error(message) {
			//FlurryAgent.logError("Inbox Item Error","Error in fetching Inbox Items", 1);
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchInboxItemsCommand", message);
			doneCallback && doneCallback();
		}
		
	};

	return _command;

})();
