diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.SendMessageCommand = (function() {

	var _command = function() {
		this._manager = app.locator.getService(ServiceName.NEWSANNOUNCEMENTMANAGER);
	};

	_command.prototype.execute = function(inboxItem) {
		this._manager.sendMessage(inboxItem, success, error);
		
		function success(obj) {
			
			console.log('success sendMessage'+obj);
			console.log('success sendMessage'+JSON.stringify(obj));
			app.model.get(ModelName.INBOXITEMS).add(inboxItem);
			console.log("app.model.get(ModelName.INBOXITEMS) : "+app.model.get(ModelName.INBOXITEMS));
			app.context.trigger(EventName.FETCHINBOX); 
		}

		function error(message) {
			openMessageBox('Error in sending Message');
			FlurryAgent.logError("Message Error","Error in sending Message", 1);
			console.log('error sendMessage');
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR,
                "SendMessageCommand", message);
		}
		
	};

	return _command;

})();
