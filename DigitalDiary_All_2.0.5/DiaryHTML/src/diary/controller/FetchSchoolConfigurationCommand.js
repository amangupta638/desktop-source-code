diary = diary || {};
diary.controller = diary.controller || {};

//  We need to work out where our manifest file is and if it's up to date
//  PRODUCTION - if not present, download from server. also update if out of date
//  PREVIEW - use the .ddm file passed on the command line
//  PROOF - extract from the .dda file that launched the app
//  DEMO - extract from the .ddd file that launched the app
//  note: PROOF and DEMO are the same, the different file extensions are for easy identification of what mode we are in
diary.controller.FetchSchoolConfigurationCommand = (function() {

    var _command = function() {
        //console.log('FetchSchoolConfigurationCommand()');

        this._appState = app.model.get(ModelName.APPLICATIONSTATE);
        this._contentManager = app.locator.getService(ServiceName.CONTENTMANAGER);
    };

    _command.prototype.execute = function(doneCallback) {
        //console.log('FetchSchoolConfigurationCommand.execute()');
    	//alert("fetch school profile..");
    	var schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);
    	console.log("in fetch school configuration  :" +JSON.stringify(schoolProfile));
    	
        var needToDownloadNewSchoolConfiguration = function() {
            var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION),
                lastSchoolProfileCheck = systemConfig.get('lastSchoolProfileCheck'),
                needToCheck = true;
            //alert("systemConfig : "+JSON.stringify(systemConfig));

            if (lastSchoolProfileCheck) {
                var lastCheckDay = new Rocketboots.date.Day(lastSchoolProfileCheck).getKey(),
                    today = new Rocketboots.date.Day().getKey();

                if (lastCheckDay == today) {
                    // If we're already checked today, then we don't need to check again.
                    needToCheck = false;
                }
            }

            // We always need to check if the manifest file is missing
            // Removed, Manifest file will be read from build directory
            //var manifestFile = air.File.applicationStorageDirectory.resolvePath("config/manifest.ddm");	 
            var manifestFile = air.File.applicationDirectory.resolvePath("config/manifest.ddm");
            
            if (!manifestFile.exists) {
                needToCheck = true;
            }

            return needToCheck;
        };

        var updateComplete = function() {
            //console.log('update completed');
            var systemConfiguration = app.model.get(ModelName.SYSTEMCONFIGURATION),
                systemConfigManager = app.locator.getService(ServiceName.SYSTEMCONFIGURATIONMANAGER);

            command._appState.set("manifestUpdated", true);
            systemConfiguration.set({
                'lastSchoolProfileCheck': new Date()
            });

            var onSaveConfigSuccess = function() {
            		//alert("onSaveConfigSuccess : hide");
            		app.view.themeView.hideTheme();
                    //app.view.dialogView.hideWindow();
                    onCommandSuccess();
                },
                onSaveConfigError = function() {
                	//alert("onSaveConfigError : hide");
                  //  console.log('Saving system configuration after update check failure');
                	app.view.themeView.hideTheme();
                   // app.view.dialogView.hideWindow();
                    onCommandFailure("Fatal Error - unable to save system configuration");
                };

           // console.log('Saving system configuration after update check');
            systemConfigManager.setSystemConfiguration(systemConfiguration, onSaveConfigSuccess, onSaveConfigError);

        };

        var onCommandSuccess = function() {
            doneCallback && doneCallback();
            
        };


        var onCommandFailure = function(errorMessage) {
        	//TODO : error message
        	console.log("errorMessage : "+errorMessage);
        	
        	openMessageBox("Error Starting Digital Diary");
//            app.view.dialogView.showWindow({
//                title: "Error Starting Digital Diary",
//                message: " " + errorMessage,
//                isError: true,
//                actions: { retry: true, close: true },
//                retryCallback: function() {
//                    app.view.dialogView.hideWindow();
//                    _.defer(function() {
//                        app.context.trigger(EventName.FETCHSCHOOLCONFIGURATION, doneCallback);
//                    });
//                }
//            }, true);
        };


        try {
            var command = this,
                applicationState = app.model.get(ModelName.APPLICATIONSTATE),
                applicationMode = applicationState.get('mode');

            switch(applicationMode) {

                case diary.model.ApplicationState.MODE.PREVIEW:
                    command._appState.set("manifestUpdated", true);
                    onCommandSuccess();
                    break;

                case diary.model.ApplicationState.MODE.PROOF:
                case diary.model.ApplicationState.MODE.DEMO:

                    // we need to extract the .dda / .ddd to the sandbox location
                    command._contentManager.getContentFromArchive(applicationState.get('archiveLocation'),
                        onCommandSuccess,
                        function onError(message) {
                            var file = new air.File(applicationState.get('archiveLocation'));
                            throw new Error("Unable to extract archive: " + file.name + " - " + message);
                        });
                    command._appState.set("manifestUpdated", true);
                    break;

                case diary.model.ApplicationState.MODE.PRODUCTION:
                    if (needToDownloadNewSchoolConfiguration()) {
                    	//TODO : remove updating window and show theme window
                    	 var schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);
                    	//alert("in fetch school info :" +JSON.stringify(schoolProfile));
                		
                    	
//                       app.view.dialogView.showWindow({
//                            title : "Updating Digital Diary",
//                            message : "Updating...",
//                            isError : false,
//                            disableClose: true
//                        });

                        var getRemoteContentSuccess = function() {
                        	   //alert(" getRemoteContentSuccess ");
                                app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.FETCHED_REMOTE_MANIFEST);
                                updateComplete();

                            },
                            getRemoteContentError = function(message) {

                                app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchSchoolConfigurationCommand", "getRemoteContentError", message);
                                //alert(" getRemoteContentError : hidetheme");
                               app.view.themeView.hideTheme();
                                //app.view.dialogView.hideWindow();

                                switch(message) {
                                    case diary.service.TheCloudServiceDelegate.ERRORS.AUTHENTICATIONERROR:
                                        // kill this token
                                        //app.view.tokenView.showToken(diary.view.air.ContainerTokenView.REQUESTTYPE.INVALID);
                                        app.view.loginView.showLogin();
                                        break;
                                    default:
                                        // is this a fatal error?
                                        // Manifest file will be read from build directory
                                    	//var manifestFile = air.File.applicationStorageDirectory.resolvePath("config/manifest.ddm");				
                                        var manifestFile = air.File.applicationDirectory.resolvePath("config/manifest.ddm");		
                                         if (!manifestFile.exists) {
                                            // no way we can proceed
                                            if (window.monitors.network.available) {
                                                // it must be a server error
                                                onCommandFailure("Could not retrieve school configuration - " + message);
                                            } else {
                                                onCommandFailure("Please check your internet connection and try again");
                                            }
                                        } else { 
                                            // let's proceed anyway, we can always update next time
                                            onCommandSuccess();
                                        }
                                        break;
                                }

                            };

                        //command._contentManager.getSchoolConfigurationFromRemoteLocation(getRemoteContentSuccess, getRemoteContentError);
						updateComplete();
                    } else {
                        //console.log("School Configuration is up to date");
                        onCommandSuccess();
                    }
                    break;

                default:
                    throw new Error("Unhandled application mode " + applicationMode);

            }


        } catch (errorMessage) {
            
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchSchoolConfigurationCommand", errorMessage);
            
            onCommandFailure("Fatal " + errorMessage);
        }
    };


    return _command;

})();