diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.UpdateDiaryItemOnDropCommand = (function() {

	var _command = function() {
		//console.log('UpdateDiaryItemOnDropCommand()');

		this._manager = app.locator.getService(ServiceName.DIARYITEMMANAGER);
	};

	/**
	 * Updates a diary item that has been dropped on a new day / time slot
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem The task that is to be updated (with values unchanged)
     * @param {!boolean} isDue Determines if the task to be updated is either due or assigned
	 * @param {?Rocketboots.date.Day} day The day the task was dropped on
	 * @param {?Rocketboots.date.Time} startTime The start time of the time slot the task was dropped on
	 */
	_command.prototype.execute = function(diaryItem, isDue, day, startTime) {

        var attrs = {},
            newDiaryItem;

       // console.log('UpdateDiaryItemOnDropCommand.execute()');

		if (!diaryItem) {
			throw new Error("Diary item must be specified");			
		}
		
		if (diaryItem.get('locked') == true) {
			throw new Error("Can't update a locked diary item");
		}

        // for the time being ONLY a task can be updated on drag and drop
        if (!diaryItem instanceof diary.model.Task) {
            throw new Error("Diary Item is invalid, only Tasks can be updated by drag and drop");
        }

		if (isSpecified(day) && !(day instanceof Rocketboots.date.Day)) {
			throw new Error("Date must be of type Rocketboots.date.Day");
		}

		if (isSpecified(startTime) && !(startTime instanceof Rocketboots.date.Time)) {
			throw new Error("Start time must be a Rocketboots.date.Time");
		}

        // if a day was specified, we will check if we can find a
        // period (time slot) for the subject that is related to the
        // diary item, if so we will override the start time if one was not
        // specified (i.e. undefined - NULL means that it is an all day time)
        if (isSpecified(day) && startTime == undefined) {

            // update day
            // if the existing diary item has a subject, move its period
            if (diaryItem.get('subject') instanceof diary.model.Subject) {
                // get timetable for target day
                var calendarModel = app.model.get(ModelName.CALENDAR),
                    calendarDay = calendarModel.getDay(day.getMonth() + 1, day.getDayInMonth());

                if (calendarDay != null) {

                    var timetable = calendarDay.get('timetable');

                    if (timetable != null) {
                        var period = timetable.getFirstPeriodWithSubjectOnCycleDay(
                            diaryItem.get('subject').get('id'), calendarDay.get('cycleDay'));

                        if (period != null) {
                            startTime = period.get('startTime');
                        }
                    }

                }
            }

        }

        // determine if we need to update the due or assigned values
        if (isDue) {

            // update the due fields
            if (isSpecified(day)) {
                attrs.dueDay = day;
            }

            // update the due time only if it was passed
            // the due time can be NULL which means that the
            // task is due for the day and not a particular time slot
            if (startTime !== undefined) {
                 attrs.dueTime = startTime;
            }

        } else {

            //update the assigned fields
            if (isSpecified(day)) {
                attrs.assignedDay = day;
            }

            // update the assigned time only if it was passed
            if (startTime !== undefined) {

                // if the start time was specified as all day (NULL) then
                // we will default the assigned time to midnight otherwise
                // we will update it to the specified time
                if (startTime == null) {
                    attrs.assignedTime = new Rocketboots.date.Time(0,0);
                } else {
                    attrs.assignedTime = startTime;
                }

            }

        }

		// save
        newDiaryItem = diaryItem.clone();

		if (newDiaryItem.set(attrs, { 'error' : error })) {
			this._manager.updateDiaryItem(newDiaryItem, null, null, success, error);			
		}
		
		function success(diaryItem) {

			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.UPDATE_DIARY_ITEM_ON_DROP, diaryItem);

			// replace item in app model
			var items = app.model.get(ModelName.DIARYITEMS);
			var oldItem = items.get(diaryItem.get("id"));

			oldItem.set(diaryItem.attributes);
			
			app.context.trigger(EventName.CLEARSEARCHCACHE);
		}

		function error(object, message) {

           // alert(message);
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "UpdateDiaryItemOnDropCommand", message);
	
		}
	};

	return _command;

})();
