/**
 * The command that is to be executed at the start of the application.  This command will be responsible to create
 * the basic blocks required for the application to function and will fire off the initial data requirements
 *
 *  @author Brian Bason
 */
diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.StartApplicationCommand = (function() {

	/**
	 * Construct the Startup Command
	 *
	 * @constructor
	 */
	var _command = function() {
		//console.log('StartApplicationCommand()');

		this._appState = app.model.get(ModelName.APPLICATIONSTATE);
		this._notificationManager = app.locator.getService(ServiceName.NOTIFICATIONMANAGER);
		this._registrationManager = app.locator.getService(ServiceName.REGISTRATIONMANAGER);
		this._manager = app.locator.getService(ServiceName.DIARYITEMMANAGER);
	};

	/**
	 * Executes the command
	 */
	_command.prototype.execute = function() {

		//alert('Executing StartApplicationCommand()');

		// holds the command that is being executed
		var command = this;
		// holds the year for the scholastic year PD-773 For release 1.0, our Calendar is 2013.
		var year = SystemSettings.HARDCODDEDYEAR;
		// holds the start date for the scholastic year
		var startDate = new Rocketboots.date.Day(year, 0, 1);
		// holds the end date for the scholastic year
		var endDate = new Rocketboots.date.Day(year, 11, 31);
		// Build up the list of async events to queue.
		var operationRunner, events;
		
		getUserDetail();
		
		// flag to hold if applicatio to be displayed or not
		var displayApplication = false;
		getAppInstallStatus();
		var appInstallStatus = app.model.get(ModelName.APPLICATIONSTATE).get('appInstallStatus');
		updateDiaryItemTypes();
		//if(appInstallStatus && app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
			//getClassUsers();
		//}
		
		// hide the window while we're starting up
		if (window.nativeWindow)
			window.nativeWindow.visible = false;

		if (!this._registrationManager.applicationIsRegistered()) {
			//alert("in app not registered ");
			app.context.trigger(EventName.REGISTERAPPLICATION);
			return;
		} else {
			app.view.loginView.showLogin(true);
			setTimeout(function() {
				app.view.loginView.hideLogin();
			}, 3000);
		}
		var studentProfile = app.model.get(ModelName.STUDENTPROFILE);

		// Store the calendar dates to allow the calendar to be refreshed later
		command._appState.set('year', year);
		command._appState.set('startDate', startDate);
		command._appState.set('endDate', endDate);

		events = [
		// read the School Profile from the DB, used for themeing the update screen (may be shown during 'fetch school configuration')
		EventName.FETCHSCHOOLPROFILE,

		// Work out where our school configuration is and whether it's up to date
		EventName.FETCHSCHOOLCONFIGURATION,

		// load just school profile so we can display the splash screen
		EventName.LOADSCHOOLPROFILE,

		// get the school details (must be done again, as it could have been modified upon 'fetch school configuration')
		EventName.FETCHSCHOOLPROFILE, EventName.CHECKSCHOOLID,
		// get the student details
		EventName.FETCHSTUDENTPROFILE,

		// show the splash screen
		function(doneCallback) {
			//alert("alert : doneCallback");
			if (studentProfile.get("name") == null) {
				studentProfile = app.model.get(ModelName.STUDENTPROFILE);
			}
			//alert("alert : showing splash");
			app.view.splashView = new diary.view.air.ContainerSplashView();
			app.view.splashView.showSplash(studentProfile);
			var i = 1;
			// set the timeout for the splash screen so that we can eventually
			// show the application
			//downloadLogoUrl();
			//alert("alert : after downloadLogoUrl");
			GetStartDate();
			//alert("alert : GetStartDate");
			function syncSuccess() {
				//alert("alert : syncSuccess");
				//console.log("syncusagelog Success");
			}

			function syncError() {
				//alert("alert : syncError");
				//console.log("syncusagelog Error");
			}

			exportdiaryitems();
			updateSubjectRecords();
			setTimeout(function() {
				exportClass();
			}, 5000);
			updateNoteEntryRecord();
			//alert("alert : updateNoteEntry");
				GetStartDate();
			var tryToShowApplication = function() {
				//alert("alert : tryToShowApplication hideSplash "+hideSplash);
				
				
				
				if (hideSplash) {//
					//alert("alert : showapp : "+showapp +" imagesaved : "+imagesaved);
					//alert("alert : showapp"+showapp +" imagesaved : "+imagesaved);
					if (showapp == true || imagesaved == true) {
						//console.log("operationRunner.hasCompleted() : "+operationRunner.hasCompleted() +"insertUpdateComplete :"+insertUpdateComplete+"addClassSubject : "+addClassSubject);
						
						if (!appInstallStatus && window.monitors.network.available) 
						{
							displayApplication = (operationRunner.hasCompleted() && insertUpdateComplete == true && addClassSubject == true);
						}
						else if(appInstallStatus || !(window.monitors.network.available))
						{
							displayApplication = (operationRunner.hasCompleted());
						}
						
						// check flag for insertUpdateSync and addClassSubject to check if they complete or not.
						//if (operationRunner.hasCompleted() && insertUpdateComplete == true && addClassSubject == true) {
						if (displayApplication) {
							//console.log("operationRunner.hasCompleted() : "+operationRunner.hasCompleted() +"insertUpdateComplete :"+insertUpdateComplete+"addClassSubject : "+addClassSubject);
							app.view.splashView.hideSplash();
							app.view.splashView = null;

							app.context.trigger(EventName.RENDERCONTENT);
								// hide the splash screen
								// switch to default view
								app.view.mainView.renderSchoolLogo();
								app.view.mainView.renderFooterNav();
								checkUserProfile();
								window.nativeWindow.visible = true;
								// generate any pending notifications
								// NOTE - The manager will fire off NEWNOTIFICATION events for each generated
								// notification
								command._notificationManager.generatePendingNotifications();
								saveAppInstallStatus();
						} else {
							// wait again until the app is ready
							//console.log("calling tryToShowApplication");
							setTimeout(tryToShowApplication, 1500);
						}
					} else {
						if (imagesaved) {
							// wait again until the app is ready
							setTimeout(tryToShowApplication, 200);
						} else {
							// wait again until the app is ready
							setTimeout(tryToShowApplication, 200);
						}
					}
				}
			};
			setTimeout(tryToShowApplication, SystemSettings.SPLASHDELAY);
			//GetStartDate();
			doneCallback();
			// allow continuing straight away
		}, EventName.DATACONTENT,
		// load the rest of the manifest file
		EventName.LOADMANIFEST,

		// get all the required subjects for the diary
		// NOTE - the subjects need to be fetched first before anything else as
		// there are some other model which make use of the subjects
		EventName.FETCHSUBJECTS,

		// figure out what mode the app is running in and get all the required content items for the application
		function(doneCallback) {
			console.log("doneCallback : ");
			// allow this to be done in the background
			app.context.trigger(EventName.FETCHCONTENTITEMS);
			//app.context.trigger(EventName.TIMETABLESYNC);
			//app.context.trigger(EventName.PERIODSYNC);
			
			/* var execSequentially = $.Deferred();
			execSequentially.done(triggerEventTimeTableSync, triggerEventGetDiaryItemManager);
			
			execSequentially.resolve(); */
			
			//if user is teacher then fetch class user data
			//alert("studentProfile.get(isTeacher) :"+studentProfile.get("isTeacher"));
			if (studentProfile.get("isTeacher")) {
				console.log("user is teacher : ");
				getClassUsersLocal();
				//getClassUsers();
				getClassForTeacher(studentProfile.get("UniqueID"));
			} else {
				console.log("user is student : ");
				insertMyTeachers();
				getParents();
				getAllClasses();
			}
			
			//getGrades();
			//getState();
			//getAllClasses();
			doneCallback();

		},
	
	
		// diary items for the year
		{
			exec : app.context.trigger,
			context : app.context,
			args : [EventName.FETCHDIARYITEMS, command._appState.get('startDate'), command._appState.get('endDate'), false, false, false]
		},

		// generate the calendar for the current scholastic year
		{
			exec : app.context.trigger,
			context : app.context,
			args : [EventName.GENERATECALENDAR, year]
		},

		// hook up to the model to keep track of any changes to the diary items
		// and content items so that we can fire of an event to indicate a search result
		// cache refresh
		function(doneCallback) {
			//alert("alert : doneCallback 2");
			//app.model.on("change:" + ModelName.DIARYITEMS, clearCache, this);
			app.listenTo(app.model, "change:" + ModelName.DIARYITEMS, clearCache);
			//app.model.get(ModelName.DIARYITEMS).on("change", clearCache, this);
			app.listenTo(app.model.get(ModelName.DIARYITEMS), "change", clearCache);

			function clearCache() {
				app.context.trigger(EventName.CLEARSEARCHCACHE);
			}
			// signal done instantly
			doneCallback();
		}];

		try {
			operationRunner = new Rocketboots.util.AsyncOperationRunner(_.map(events, function(event) {
				//alert("event : "+event);
				// convert plain events into objects appropriate for the async runner
				if (typeof event === "string") {
					return {
						"exec" : app.context.trigger,
						"context" : app.context,
						"args" : [event]
					};
				}
				// by default, just use the event object defined
				return event;
			}));

			operationRunner.run();

		} catch (error) {
			// report error
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "StartApplicationCommand:", error);

			/**
			 * Re-throw the error to preserve logic flow when an exception is thrown.
			 * This is done because the above line doesn't actually handle the exception, it only logs it.
			 * By re-throwing, we preserve the concept of handled and unhandled exceptions.
			 */
			throw error;
		}

		app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.START_APPLICATION);
	};

	// return the created command
	return _command;

})();
