/**
 * $Id$
 */

/** @namespace */
diary = diary || {};

/** @namespace */
diary.controller = diary.controller || {};

/**
 * The command that is invoked to create and add a new usage log event
 *
 * @constructor
 */
diary.controller.AddUsageLogCommand = (function() {

    var _command = function() {
        this._manager = app.locator.getService(ServiceName.USAGELOGMANAGER);
    };

    _command.prototype.execute = function(eventType) {

		// Only log a sub-set of events
		{

			var t = diary.model.UsageLogEvent.TYPE,
				productionTypes = [
					t.REGISTER_TOKEN,
					t.UPDATE_TOKEN,
					t.UPDATE_STUDENT_PROFILE,
					t.DIARY_ITEM_CREATED,
					t.DIARY_ITEM_DELETED,
					t.UPDATE_DIARY_ITEM,
					t.UPDATE_DIARY_ITEM_ON_DROP,
					
					
					t.TASK_COMPLETED,
					t.NOTE_ENTRY_ADDED,
					t.CLASS_CREATED,
					t.CLASS_DELETED,
					t.UPDATE_CLASS,
					t.UPDATE_SUBJECT,
					t.FEEDBACK,
					t.WARNING,
					t.ERROR
				];
			if (!_.contains(productionTypes, eventType)) {
				//console.log("UsageLog: ignoring event of type " + eventType);
				return;
			}
		}

        var eventDetail = _.reduce(Array.prototype.slice.call( arguments, 1 ), function(detailsString, arg) {

			var argDetails;

			if (detailsString != "") detailsString += "; ";

			// if the arg is a diary item we ONLY need the type of it
			// and not anything else
			if (arg instanceof diary.model.DiaryItem) {
				argDetails = arg.get('type');
			} else {
				argDetails = JSON.stringify( arg.attributes || arg );
			}

			// Support both backbone models and regular javascript objects		
			return detailsString + argDetails;
        }, "");

       // console.log('UsageLog: eventType=' + eventType + ' eventDetail=' + eventDetail);

        if (app.model.get(ModelName.APPLICATIONSTATE).get('mode') != diary.model.ApplicationState.MODE.PRODUCTION) {
            return;
        }

        if (_.isUndefined(eventType) || _.isUndefined(diary.model.UsageLogEvent.TYPE[eventType])) {
           // console.log("AddUsageLogCommand: Error - eventType is invalid or undefined", eventType);
            return;
        }

        var usageLogEvent = new diary.model.UsageLogEvent(); // time is defaulted to the current time
        var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);

        usageLogEvent.set('type', eventType);
        usageLogEvent.set('detail', eventDetail);
        usageLogEvent.set('token', systemConfig.get('token'));
		usageLogEvent.set('macAddresses', systemConfig.get('macAddresses'));
		usageLogEvent.set('schoolConfiguration', systemConfig.get('schoolConfigTimestamp') || "");
        usageLogEvent.set('version', diary.model.UsageLogEvent.VERSION[eventType]);
		usageLogEvent.set('clientVersion', systemConfig.get('versionNumber'));

		this._manager.addUsageLog(usageLogEvent,
            function success(eventId) {
                usageLogEvent.set('id', eventId);
                //console.log("Successfully added usage log event");
                var appState = app.model.get(ModelName.APPLICATIONSTATE),
                    lastSyncTime = appState.get('lastUsageLogSyncTime'),
                    now = new Date();

                if (!appState.get('syncingUsageLog') && (lastSyncTime == null ||
                    (lastSyncTime != null &&
                        (now.getTime() - lastSyncTime.getTime()) > SystemSettings.USAGELOGSYNCINTERVAL))) {
                    //app.context.trigger(EventName.SYNCUSAGELOG, SystemSettings.USAGELOGSYNCINTERVAL);
                }
			
            },
            function error(msg) {
                //console.log("Error adding usage log event", msg);
            }
        );
    };

    return _command;

})();
