diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.ExitApplicationCommand = (function() {

	var _command = function() {
		// empty constructor
	};

	/**
	 * Exits the application.
	 * This has the same effect as if the user had selected Apple-Q on Mac,
	 * or selected the close window icon.
	 */
	_command.prototype.execute = function() {
		//console.log('ExitApplicationCommand.execute()');

		// TODO: move this AIR code into an AIR package
		window.nativeWindow.close();

	};

	return _command;

})();
