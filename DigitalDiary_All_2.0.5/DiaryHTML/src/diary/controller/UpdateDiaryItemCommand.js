diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.UpdateDiaryItemCommand = (function() {

	var _command = function() {
		//console.log('UpdateDiaryItemCommand()');

		this._manager = app.locator.getService(ServiceName.DIARYITEMMANAGER);
	};

	/**
	 * Updates a diary item
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem The diary item that is to be updated
	 * @param {?Array<string>} attachmentsToAdd A list of file paths pointing to files to be added as attachments to the diary item.
	 * @param {?diary.collection.AttachmentCollection} attachmentsToRemove A collection of attachment objects to be removed from the diary item.
	 */
	_command.prototype.execute = function(diaryItem, attachmentsToAdd, attachmentsToRemove) {
		//console.log('UpdateDiaryItemCommand.execute()');
		
		this._manager.updateDiaryItem(diaryItem, attachmentsToAdd, attachmentsToRemove, success, error);
		console.log("diaryItem : "+JSON.stringify(diaryItem));
		function success(diaryItem) {
			var userId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.UPDATE_DIARY_ITEM, diaryItem);

			// replace item in app model
			var items = app.model.get(ModelName.DIARYITEMS);
			var oldItem = items.get(diaryItem.get("id"));
			oldItem.set(diaryItem.attributes);
			
			app.context.trigger(EventName.CLEARSEARCHCACHE);
			console.log("diaryItem : "+JSON.stringify(diaryItem));
			console.log("diaryItem.assignedtoMe == 1"+(diaryItem.assignedtoMe == "1")+"diaryItem.assignedtoMe : "+diaryItem.get("assignedtoMe"));
			//console.log("!(diaryItem.assignedtoMe) : "+(!(diaryItem.get("assignedtoMe"))));
			if(!(diaryItem.get("assignedtoMe"))){
				if(diaryItem.get("type") == "EVENT"){
					if(diaryItem.get("uniqueId") == null){
						setEvent(diaryItem.toJSON());
					}
					else{
						editEvent(diaryItem.toJSON());
					}
					
				}
				else{
					//console.log("if assigned task ... set");
					if(diaryItem.get("uniqueId") == null){
						setDiaryItem(diaryItem.toJSON());
					}
					else{
						editDiaryItem(diaryItem.toJSON());	
					}
				}
			}
			openMessageBox("Diary item updated successfully.");
			fetchDairyItems();
		}

		function error(message) {
			//var eventParameters = getNavigationEventParameters();
			FlurryAgent.logError("Diary Item Error","Error in updating Diary Item", 1);
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "UpdateDiaryItemCommand", message);
		}
	};
		

	return _command;

})();
