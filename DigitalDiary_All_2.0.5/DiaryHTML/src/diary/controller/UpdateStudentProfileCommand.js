diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.UpdateStudentProfileCommand = (function() {

	var _command = function() {
		//console.log('UpdateStudentProfileCommand()');

		this._studentManager = app.locator
				.getService(ServiceName.STUDENTMANAGER);
	};

	_command.prototype.execute = function(replacementStudentProfile) {
		
		this._studentManager.setStudentProfile(replacementStudentProfile, success, error);
		//app.model.set(ModelName.STUDENTPROFILE, replacementStudentProfile);
		
		function success(replacementStudentProfile) {
			app.model.set(ModelName.STUDENTPROFILE, replacementStudentProfile);
			//getUserID();
			//app.context.trigger(EventName.CLEARSEARCHCACHE);
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.UPDATE_STUDENT_PROFILE, replacementStudentProfile);
			
		}
		function error(message) {
			openMessageBox("insert student profile error"+message);
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "UpdateStudentProfileCommand", message);
		}
	};
	return _command;
})();
