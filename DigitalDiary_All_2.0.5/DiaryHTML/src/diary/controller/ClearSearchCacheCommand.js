/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: ClearSearchCacheCommand 6171 2012-04-23 03:19:57Z brian.bason $
 */

/** @namespace */
diary = diary || {};

/** @namespace */
diary.controller = diary.controller || {};

/**
 * The command that is invoked to clear the search result cache in the application
 * 
 * @constructor
 */
diary.controller.ClearSearchCacheCommand = (function() {

	var _command = function() {
		//console.log('ClearSearchCacheCommand()');

		this._manager = app.locator.getService(ServiceName.SEARCHMANAGER);
	};

	/**
	 * Clears the cache of the search results
	 */
	_command.prototype.execute = function() {
		//console.log('ClearSearchCacheCommand::execute()');

		// clear the cache, that is all that we are going to do
		// buh yah!!!!
		this._manager.clearSearchResultsCache();
	};

	return _command;

})();
