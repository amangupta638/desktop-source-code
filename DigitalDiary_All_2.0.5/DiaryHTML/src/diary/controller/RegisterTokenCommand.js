diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.RegisterTokenCommand = (function() {

	var _command = function() {
		//console.log('RegisterTokenCommand()');

		this._manager = app.locator.getService(ServiceName.REGISTRATIONMANAGER);
	};

	/**
	 * Registers a token with the cloud service
	 * 
	 * @param {token} String The token to register
	 */
	_command.prototype.execute = function(token) {
		//alert('RegisterTokenCommand.execute()');

		this._manager.registerApp(token, success, error);

		function success(result) {
			//console.log('register token success');
			//console.log(result.schoolid);
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.REGISTER_TOKEN, result);
			app.context.trigger(EventName.TOKENREGISTERED);
		
		}

		function error(message) {

			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "RegisterTokenCommand", message);
			app.context.trigger(EventName.TOKENREGISTRATIONFAILED, message);

		}
	};

	return _command;

})();
