diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.FetchTeacherDetailsCommand = (function() {

	var _command = function() {
		//alert("FetchTeacherDetailsCommand 1");
		this._teacherManager = app.locator.getService(ServiceName.TEACHERMANAGER);
	};

	_command.prototype.execute = function (doneCallback) {
		//alert("FetchTeacherDetailsCommand 2");
		doneCallback = doneCallback || $.noop;

		this._teacherManager.getTeacherDetails(success, error);

		// the function that is to be executed on successful operation
		function success(teacher) {
			
			app.model.set(ModelName.TEACHER, teacher);
			doneCallback();
		}

		// the function that is to be executed on failure of the operation
		function error(message) {
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchTeacherDetailsCommand", message);
			doneCallback();
			
		}
	};

	return _command;

})();
