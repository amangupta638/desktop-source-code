diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.FetchSubjectsCommand = (function() {

	var _command = function() {
		//console.log('FetchSubjectsCommand()');

		this._timeTableManager = app.locator.getService(ServiceName.TIMETABLEMANAGER);
	};

	_command.prototype.execute = function (doneCallback) {
		//console.log('FetchSubjectsCommand.execute()');

		doneCallback = doneCallback || $.noop;

		// the function that is to be executed on success retrieval of the subjects
		function success(subjects) {
			app.model.get(ModelName.SUBJECTS).reset(subjects.toArray());
			doneCallback();
		}
		
		// the function that is to be executed on failing to retrieve the subjects
		function error(message) {
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchSubjectsCommand", message);
			doneCallback();
		}
		
		// get the manager to get all the diary items for the date range
		this._timeTableManager.getSubjects(success, error);
	};

	return _command;

})();
