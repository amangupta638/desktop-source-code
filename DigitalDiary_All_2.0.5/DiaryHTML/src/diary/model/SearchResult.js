/**
 * The model that represents a result of a search operation that was performed by a user
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.SearchResult = (function() {
	
	/**
	 * The constructor
	 * @constructor
	 */
	var searchResult = Backbone.Model.extend({
		
		defaults : function() {
			// return the default attributes
			return { 'criteria' : null,			// {!string} The criteria that was used for searching
					 'results' : null };		// {?diary.model.SearchResultSectionCollection} The collection of subjects of results related to the criteria
		},
		
		initialize : function() {
			
			// if the results was not set we will create a default empty collection
			if (!isSpecified(this.get("results"))) {
				this.set("results", new diary.collection.SearchResultSectionCollection());
			}
			
		}
		
	});
	
	return searchResult;
})();