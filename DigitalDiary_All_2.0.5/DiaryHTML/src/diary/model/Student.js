/**
 * The model that represents a STUDENT details
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.Student = (function() {

	var student = Backbone.Model.extend({

		// init the student with the default values
		defaults : function() {
			
				 
				 
			// define the attributes of the student profile
			var attrs = { 
						  'id' : null,
						  'classId' : null,
						  'schoolId' : null,
						  'myClass'	: null,
						  'updateTime' : null,
						  'email' : null,
						  'name' : null,	// the name of the student
						  'lastName': null,	
						  'imageUrl' : null,
						  'telephone' : null,
						  'studentId':null,	
						  'teacherId' : null,	
						  'image' : null,
						  'profileImageToDisplay' : null,
						 
						  };	
			
			// return the attributes of the class
			return attrs;
		},
	
			
		/*
		 * gets the attribute of a student profile
		 * 
		 * @param {string} attribute The attribute name that is required for the Student Profile
		 *
		 * @return {object} The value of the requested attribute
		 */ 
		get : function (attribute) {
			
			value = Backbone.Model.prototype.get.call(this, attribute);
			
			// return the attribute of the value
			return value;
		}
		
	});
	
	return studentProfile;
	
})();

