/**
 * The model that represents a manifest (ddm) file.  The app should parse a ddm file into this model once and then just use this model
 *
 * author - Phil Haeusler
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.Manifest = (function() {

    /**
     * The constructor for the Manifest
     *
     * @constructor
     */
    var manifest = Backbone.Model.extend({

        // return the default attributes
        defaults : function() {

            // return all of the properties for the model
            return {
                'contentMapping' : null,	// the defined content mappings (diary.model.ContentMapping)
                'contentPackages' : new diary.collection.ContentPackageCollection(),    // the defined content packages (diary.collection.ContentPackageCollection)
				'daysOfNote' : new diary.collection.DayOfNoteCollection(),	// the days of notes defined
				'events' : new diary.collection.DiaryItemCollection(),	// the school event diary items defined
                'timetables' : null,
                'schoolProfile' : null,
                'timestamp' : null
            };
        }

    });

    // return the created manifest
    return manifest;

})();

