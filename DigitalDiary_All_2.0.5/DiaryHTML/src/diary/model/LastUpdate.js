/**
 * The model that represents the system configuration for the entire application.
 *  
 * author - Appstudioz
 */

diary = diary || {};
diary.model = diary.model || {};

diary.model.LastUpdate = (function() {
	
	/**
	 * Creates the system configuration base object
	 * 
	 * @constructor
	 */
	var lastUpdate = Backbone.Model.extend({
		
		// init the application state with the default values
		defaults : function() {
			
			// define the default attributes
			var attrs = { 
				'TableName' : null, 
				'lastupdated' : null
			};
			
			// return the default attributes
			return attrs;
		}
		
		
	});
	
	
	// return the created system configuration
	return lastUpdate;
	
})();
