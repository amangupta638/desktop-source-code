/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: Event.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.model = diary.model || {};

(function() {
	"use strict";

	diary.model.Event = diary.model.DiaryItem.extend(
		/** @lends diary.model.Event.prototype */
		{
			/**
			 * Sets the model attribute defaults.
			 *
			 * @returns {Object} The model attribute defaults
			 */
			defaults : function() {
				var attrs = {
					"type" : diary.model.DiaryItem.TYPE.EVENT,
					"startDay" : new Rocketboots.date.Day(),
					"startTime" : null,
					"atTime" : null,
					"endTime" : null,
					"locked" : false,
					"classId" : null,
					"assignedtoMe":1,
					"uniqueId":null,
					"priority" : 0
				};

				return _.extend(diary.model.DiaryItem.prototype.defaults(), attrs);
			},

			/**
			 * Validates the model attributes.
			 *
			 * @param {!Object} attrs The model attributes to be set
			 *
			 * @returns {String} Validation error, if any
			 */
			validate : function(attrs) {
				var result = null;

				// validate base class
				result = diary.model.DiaryItem.prototype.validate.call(this, attrs);
				if (result != null) {
					return result;
				}

				// 'startDay' is required
				if (!isSpecified(attrs.startDay)) {
					return "Start Day must be specified and cannot be NULL";
				}

				// 'startDay' must be a valid Rocketboots.date.Day, if specified
				if (!(attrs.startDay instanceof Rocketboots.date.Day)) {
					return "Start Day must be a valid Day";
				}

				// 'startTime' must be a valid Rocketboots.date.Time, if specified
				if (isSpecified(attrs.startTime) && !(attrs.startTime instanceof Rocketboots.date.Time)) {
					return "Start Time must be a valid Time";
				}

				// 'endTime' must be a valid Rocketboots.date.Time, if specified
				if (isSpecified(attrs.endTime) && !(attrs.endTime instanceof Rocketboots.date.Time)) {
					return "End Time must be a valid Time";
				}

				// 'locked' is required
				if (!isSpecified(attrs.locked)) {
					return "Locked must be specified and cannot be NULL";
				}

				// 'locked' must be boolean
				if (attrs.locked !== true && attrs.locked !== false) {
					return "Locked must be either 'true' or 'false'";
				}

				// 'startTime' must be < 'endTime'
				if (isSpecified(attrs.startTime) && isSpecified(attrs.endTime)
						&& attrs.startTime.toInteger() > attrs.endTime.toInteger()) {
					return "End Time must be after Start Time";
				}
			},

			/**
			 * Determines if the Diary Item falls on the specified day.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day
			 */
			isWithinDay : function(day) {
				return this.get('startDay').getKey() == day.getKey();
			},
			
			isAssignedToday : function() {
				var day = new Rocketboots.date.Day();
				return this.get('startDay').getKey() == day.getKey();
			},

			/**
			 * Determines if the Diary Item falls within the specified time frame. An Event is
			 * considered to fall within a time frame if the Start Day matches the specified day and
			 * the Start Time falls within the specified time frame. If no Start Time is specified
			 * for an Event, it will be considered NOT falling within the specified time frame. 
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 * @param {!Rocketboots.date.Time} startTime The start time from which to check
			 * @param {!Rocketboots.date.Time} endTime The end time from which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day and within the specified time frame
			 */
			isWithinTimeFrame : function(day, startTime, endTime) {
				//alert("alert : isWithinTimeFrame")
				return this.get('startDay').getKey() == day.getKey()
					&& isSpecified(this.get('startTime'))
					&& this.get('startTime').toInteger() >= startTime.toInteger()
					&& this.get('startTime').toInteger() < endTime.toInteger();
			},
			
			isOnTheDay : function(day, startTime, endTime) {
				return this.get('startDay').getKey() == day.getKey() 
						&& !isSpecified(startTime) 
						&& !isSpecified(endTime);
			}
		}
	);

})();
