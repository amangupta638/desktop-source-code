/**
 * The model that represents a SCHOOL PROFILE in a diary.
 * A school profile will contain details about the school to which the diary belongs to.  Such information will
 * be the school image, logos, name, title, etc...
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.renderContentModel = (function() {
	
	/**
	 * Creates a School Profile by extending the Backbone model class
	 * 
	 * @constructor
	 */
	var RenderContentModel = Backbone.Model.extend({

		// init the schoolProfile with the default values
		defaults : function() {
			
			// define the attributes of the school profile
			var attrs = { 'ContentType' : null,					
						  'Title' : null,				
						  'Content' : null,				
						  'uniqueID' : null,				
						  'respectedPanel' : null,		
						  'StartDate' : null,				
						  'EndDate' : null			
						  };		
						  
			// return the attributes
			return attrs;
			
		},
		
		// the initialisation function
		initialize : function() {
			
		
			
		}
		
	});
	
	return RenderContentModel;
	
})();

