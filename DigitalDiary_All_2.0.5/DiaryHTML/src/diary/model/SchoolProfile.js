/**
 * The model that represents a SCHOOL PROFILE in a diary.
 * A school profile will contain details about the school to which the diary belongs to.  Such information will
 * be the school image, logos, name, title, etc...
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.SchoolProfile = (function() {
	
	/**
	 * Creates a School Profile by extending the Backbone model class
	 * 
	 * @constructor
	 */
	var schoolProfile = Backbone.Model.extend({

		// init the schoolProfile with the default values
		defaults : function() {
			
			// define the attributes of the school profile
			var attrs = { 'name' : null,					// the name of the school
						  'schoolLogo' : null,				// the path to the logo of the school
						  'schoolImage' : null,				// the path to the image of the school
						  'schoolBgImage' : null,			// the path to the background image of the school		 
						  'schoolMotto' : null,				// the school motto
						  'missionStatement' : null,		// the school mission statement
						  'diaryTitle' : null,				// the title for the diary
						  'includeEmpower' : false,			// determines if the empower content is included for the school
						  'theme' : null,                   // override the default colour scheme of the application
						  'AllowAutoUpdates': null,
						  'RegionalFormat': null,
						  'UniqueID': null,
						  'domainName': null,
						  'AllowSynch':null,	
						  'empowerType' : null,
						  'includeEmpowerContent' : null,
						  'allowParents' : 1,		
						  'contentMapping' : null };		// the content item mapping for the school profile
						  
			// return the attributes
			return attrs;
			
		}, 
		
		// the initialisation function
		initialize : function() {
			// if no content menu was specified we will create one 
			if (!this.get('contentMapping')) {
				this.set('contentMapping', new diary.model.ContentMapping());
			}
			
		}
		
	});
	
	return schoolProfile;
	
})();

