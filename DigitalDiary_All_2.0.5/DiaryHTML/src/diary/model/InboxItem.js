diary = diary || {};
diary.model = diary.model || {};

diary.model.InboxItem = (function() {
	
	var inboxItem = Backbone.Model.extend({
		
		defaults : function() {
			
			var attrs = { 
				'title' : null, 
				'itemId' : null,
				'UniqueID' : null,
				'type' : null, 
				'iconType' : null,
				'sentDate' : null,
				'sentTime' : null,
				'description' : null,
				'from' : null,
				'to' : null,
				'showReply' : false,
				'showDelete' : false,
				'parentId' : 0,
				'userId' : null,
				'receiverId' : null,
				'IpAddress' : null,
				'campusName' : null,
				'author' : null,
				'tags' : null,
				'receiverName':null,
				'userName':null,
				'attachmentURL':null,
				'deletedStatus':0,
				'showHTML':true,
				'mailThreadCount' : 0,
				'readStatus' : 0,
				'emailId' : null
			};
			
			return attrs;
		},
		
		isAssignedToday : function(date) {
			var day = new Rocketboots.date.Day();
			var sentDate = this.convertDate(date);
			return sentDate == day.getKey();
		},
		
		getUnreadNotificationCount: function(date,day) {
			if(day == null){
				return false;
			}
			
			var sentDate = this.convertDate(date);
			return sentDate >= day.getKey();
		},
		
		convertDate : function(date){
			if(date != null || date != undefined || date != '')
			{
			    dateTime = date.split(" ");
			
			    var date = dateTime[0].split("-");
			    var yyyy = date[0];
			    var mm = date[1];
			    var dd = date[2];
			    
			    return yyyy + mm + dd;
			
		    } else {
		    	return "";
		    }
		}
		
	});
	
	return inboxItem;
	
})();
