/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: Message.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.model = diary.model || {};

(function() {

	diary.model.Message = diary.model.DiaryItem.extend(
		/** @lends diary.model.Message.prototype */
		{
			/**
			 * Sets the model attribute defaults.
			 *
			 * @returns {Object} The model attribute defaults
			 */
			defaults : function() {
				var attrs = {
					// use 'title' for 'attention'
					"type" : diary.model.DiaryItem.TYPE.MESSAGE,
					"description" : "(unknown)",		// use 'description' for 'message'
					"atDay" : new Rocketboots.date.Day(),
					"atTime" : null,
					"messageFrom" : diary.model.Message.FROM.TEACHER,
					"messageFromText" : "(unknown)",
					"responseRequired" : false
				};

				return _.extend(diary.model.DiaryItem.prototype.defaults(), attrs);
			},

			/**
			 * Validates the model attributes.
			 *
			 * @param {!Object} attrs The model attributes to be set
			 *
			 * @returns {String} Validation error, if any
			 */
			validate : function(attrs) {
				var validMessageFromValues = [];

				// override base class validation to provide Message-specific error messages
				// e.g. 'title' is called "Attention" and 'description' is called "Message"

				// 'title' is required
				if (!isSpecified(attrs.title)) {
					return "Attention must be specified and cannot be NULL";
				}

				// 'title' must be a string
				if (typeof(attrs.title) != 'string') {
					return "Attention must be a valid string";
				}

				// 'title' cannot be empty
				if (attrs.title == "") {
					return "Attention must be specified and cannot be empty";
				}

				// 'description' is required
				if (!isSpecified(attrs.description)) {
					return "Message must be specified and cannot be NULL";
				}

				// 'description' must be a string
				if (typeof(attrs.description) != 'string') {
					return "Message must be a valid string";
				}

				// 'description' cannot be empty
				if (attrs.description == "") {
					return "Message must be specified and cannot be empty";
				}

				// 'atDay' is required
				if (!isSpecified(attrs.atDay)) {
					return "Day must be specified and cannot be NULL";
				}

				// 'atDay' must be Rocketboots.date.Day
				if (!(attrs.atDay instanceof Rocketboots.date.Day)) {
					return "Day must be a valid Day";
				}

				// 'atTime' must be Rocketboots.date.Time, if specified
				if (isSpecified(attrs.atTime) && !(attrs.atTime instanceof Rocketboots.date.Time)) {
					return "Time must be a valid Time";
				}

				// 'messageFrom' is required
				if (!isSpecified(attrs.messageFrom)) {
					return "Message From must be specified and cannot be NULL";
				}

				// 'messageFrom' must be a valid value
				for (var type in diary.model.Message.FROM) {
					validMessageFromValues.push(type);
				}

				if (_.indexOf(validMessageFromValues, attrs.messageFrom) == -1) {
					return "Message From must be one of: " + validMessageFromValues;
				}

				// 'messageFromText' is required
				if (!isSpecified(attrs.messageFromText)) {
					return "Message From Text must be specified and cannot be NULL";
				}

				// 'messageFromText' must be a string
				if (typeof(attrs.messageFromText) != 'string') {
					return "Message From Text must be a valid string";
				}

				// 'messageFromText' cannot be empty
				if (attrs.messageFromText == "") {
					return "Message From Text must be specified and cannot be empty";
				}

				// 'responseRequired' is required
				if (!isSpecified(attrs.responseRequired)) {
					return "Response Required must be specified and cannot be NULL";
				}

				// 'responseRequired' must be boolean
				if (attrs.responseRequired !== true && attrs.responseRequired !== false) {
					return "Response Required must be either 'true' or 'false'";
				}
			},

			/**
			 * Determines if the Diary Item falls on the specified day.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day
			 */
			isWithinDay : function(day) {
				return this.get('atDay').getKey() == day.getKey();
			},

			/**
			 * Determines if the Diary Item falls within the specified time frame. A Message is
			 * considered to fall within a time frame if the At Day matches the specified day and
			 * the At Time falls within the specified time frame. If no At Time is specified for a
			 * Message, it will be considered NOT falling within the specified time frame.
			 *
			 * @param {!Rocketboots.date.Day} day The day on which to check
			 * @param {!Rocketboots.date.Time} startTime The start time from which to check
			 * @param {!Rocketboots.date.Time} endTime The end time from which to check
			 *
			 * @returns {Boolean} True if the Diary Item falls on the specified day and within the specified time frame
			 */
			isWithinTimeFrame : function(day, startTime, endTime) {
				return this.get('atDay').getKey() == day.getKey()
					&& isSpecified(this.get('atTime'))
					&& this.get('atTime').toInteger() >= startTime.toInteger()
					&& this.get('atTime').toInteger() < endTime.toInteger();
			}
			
		}, {
			/**
			 * Defines the different possible values for the 'messageFrom' field.
			 * @enum {string}
			 */
			FROM : {
				PARENT : "PARENT",
				TEACHER : "TEACHER"
			}
		}
	);

})();
