diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.FetchContentItemsCommand = (function() {
    
    var _command = function() {
        //console.log('FetchContentItemsCommand()');

        this._contentManager = app.locator.getService(ServiceName.CONTENTMANAGER);
        this._documentManager = app.locator.getService(ServiceName.DOCUMENTMANAGER);
    };

    _command.prototype.execute = function() {
        
        // holds the command itself
        var command = this;
        
        //console.log('FetchContentItemsCommand.execute()');

        var manifest = app.model.get(ModelName.MANIFEST),
            contentMapping = manifest.get('contentMapping'),
            calendarBanners = contentMapping && contentMapping.get('calendarBanners'),
            includeSidebarContent = (calendarBanners != null && calendarBanners.length > 0),
            applicationMode = app.model.get(ModelName.APPLICATIONSTATE).get('mode');
        
        // inject the details into the school profile
        app.model.get(ModelName.SCHOOLPROFILE).set( { 'contentMapping' : contentMapping });
        if (app.model.get(ModelName.SCHOOLPROFILE).get('includeEmpower') == true) {
            app.model.get(ModelName.SCHOOLPROFILE).set('includeEmpower', includeSidebarContent);
        }

        // figure out which content packages are old or missing and start processing the queue
        if (applicationMode == diary.model.ApplicationState.MODE.PRODUCTION) {

            command._contentManager.getStoredContentPackages(
                function onSuccess(contentPackagesFromDB) {
                    app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.FETCHED_STORED_PACKAGES, contentPackagesFromDB.size());

                    var packagesToUpdate = [], packagesToKeep = [];
                    manifest.get('contentPackages').each(function(contentPackageFromManifest) {
                                var contentPackageFromDB = contentPackagesFromDB.find(function(p) {
                                    return p.get('package') == contentPackageFromManifest.get('package');
                                });
                                var existsOnDisk = air.File.applicationStorageDirectory.resolvePath("content/" + contentPackageFromManifest.get('package')).exists;

                                // If we don't have a record of the contentPackage in the db or on disk or
                                // the dateLastUpdated is old, then we want to update the contentPackage
                                if (!existsOnDisk || !contentPackageFromDB || contentPackageFromManifest.get('dateUpdated') > contentPackageFromDB.get('dateLastUpdated')) {
                                  //  console.log("Package [", contentPackageFromManifest.get('package'), "] needs updating");
                                    packagesToUpdate.push(contentPackageFromManifest);
                                } else {
                                    packagesToKeep.push(contentPackageFromManifest);
                                   // console.log("Package [", contentPackageFromManifest.get('package'), "] is up to date");
                                }
                            }
                    );

                    app.model.get(ModelName.CONTENTPACKAGEQUEUE).reset(packagesToUpdate);
                    // delete the indexes for all documents that we don't want to keep, this will include
                    // any old packages that have been indexed, but are no longer in the manifest
                    command._documentManager.deleteDocumentsNotInPackages(packagesToKeep,
                            // success
                            function() {
                                // Invalidate the search cache
                                app.context.trigger(EventName.CLEARSEARCHCACHE);

                                // if we have any packages to update, then start downloading them
                                if (packagesToUpdate.length > 0) {
                                    // delete any db records of the packages we need to update
                                    // so we don't need to check if we need to update an existing record, we just
                                    // add a new one when the download and unzipping is complete
                                    command._contentManager.deleteStoredContentPackages(
                                            // success
                                            function() {
                                                // now start processing the download queue
                                                app.context.trigger(EventName.FETCHNEXTCONTENTPACKAGE);
                                            },
                                            // error
                                            console.log,
                                            // packages to delete from the db
                                            packagesToUpdate
                                    );
                                }
                            },

                            // error
                            console.log
                    );

                },
                function onError(message) {
                    app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchContentItemsCommand", "getStoredContentPackages.onError", message);
                }
            );

        } else if (applicationMode == diary.model.ApplicationState.MODE.PREVIEW ||
                 applicationMode == diary.model.ApplicationState.MODE.PROOF ||
                 applicationMode == diary.model.ApplicationState.MODE.DEMO) {
            
            command._documentManager.indexOnStartup(function() {
                // Invalidate the search cache
                app.context.trigger(EventName.CLEARSEARCHCACHE);
                //console.log("Document indexing for " + applicationMode + " mode completed successfully");
            });
            
        }
    };

    return _command;

})();