diary = diary || {};
diary.controller = diary.controller || {};

/**
 * Open a diary item's attachment using the native system editor.
 * @author John Pearce
 */
diary.controller.OpenDiaryItemAttachmentCommand = (function() {

	var _command = function() {
		//console.log('OpenDiaryItemAttachmentCommand()');

		this._manager = app.locator.getService(ServiceName.DIARYITEMMANAGER);
	};

	/**
	 * @public
	 * Open a diary item's attachment using the systems native editor.
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem The diary item with the attachment to be opened. 
	 * @param {!diary.model.Attachment} attachment The attachment to be opened.
	 */
	_command.prototype.execute = function(diaryItem, attachment) {
		//console.log('OpenDiaryItemAttachmentCommand.execute()');
		
		if (!this._manager.openAttachmentOnDiaryItem(diaryItem, attachment)) {
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "OpenDiaryItemAttachmentCommand", attachment.get('displayName'));

		}
		
	};

	return _command;

})();
