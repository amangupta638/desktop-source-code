diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.FetchStudentProfileCommand = (function() {

	var _command = function() {
		//console.log('FetchStudentProfileCommand()');

		this._studentManager = app.locator.getService(ServiceName.STUDENTMANAGER);
	};

	_command.prototype.execute = function (doneCallback) {
		//console.log('FetchStudentProfileCommand.execute()');

		// NOTE - The manager will update the details of the passed student
		// profile
		this._studentManager.getStudentProfile(success, error);

		// the function that is to be executed on success retrieval of the student
		// profile
		function success(student) {
			
			//alert("FetchStudentProfileCommand : student : after fetching info : "+JSON.stringify(student))
			app.model.set(ModelName.STUDENTPROFILE, student);
			//getUserID();
			doneCallback && doneCallback();
		}
		
		// the function that is to be executed on failing to retrieve the student
		// profile
		function error(message) {
			
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "FetchStudentProfileCommand", message);
			doneCallback && doneCallback();
			
		}
	};

	return _command;

})();
