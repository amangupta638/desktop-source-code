diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.CreateDiaryItemCommand = (function() {

	var _command = function() {
		//console.log('CreateDiaryItemCommand()');

		this._manager = app.locator.getService(ServiceName.DIARYITEMMANAGER);
	};

	/**
	 * Creates a diary item
	 * 
	 * @param {!diary.model.DiaryItem} diaryItem The diary item that is to be created
	 * @param {?Array<string>} attachments The file paths that are to be linked as attachments to the diary item being created
	 */
	_command.prototype.execute = function(diaryItem, attachments) {
		console.log("(diaryItem) in create diaryItemCommand: "+JSON.stringify(diaryItem));
	    var objId="";
		this._manager.createDiaryItem(diaryItem, attachments, success, error);
		
		console.log("!(diaryItem.assignedtoMe) : "+(diaryItem.get("assignedtoMe")));
		
			
		function success(obj) {
			console.log("!(diaryItem.assignedtoMe) : "+(diaryItem.get("assignedtoMe")));
			// add diary item to app model
			if(!(diaryItem.get("assignedtoMe"))){
				if(diaryItem.get("type") == "EVENT"){
					setEvent(diaryItem.toJSON());
				}
				else{
					setDiaryItem(diaryItem.toJSON());
				}
			}
			else{
				console.log(" else " );
				app.model.get(ModelName.DIARYITEMS).add(obj.diaryItem);
			}
			
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.DIARY_ITEM_CREATED, diaryItem);
            app.context.trigger(EventName.CLEARSEARCHCACHE);
           // app.locator.getService(ServiceName.DIARYITEMMANAGER).getDiaryitems("", onsuccess, onerror);
		}

		function error(message) {
			openMessageBox("Error in creating Diary Item");
			FlurryAgent.logError("Diary Item Error","Error in creating Diary Item", 1);
			
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR,
                "CreateDiaryItemCommand", message);
		}
		function onupdateSuccess(values) 
		{
		//console.log("onupdateSuccess");
		}
		function onupdateError(values) 
        {
       // console.log("onupdateError");
        }
		function onsuccess(values) 
		{
		if(values.length>=10)
		{
		      var dataArray=[];
              var obj={};
              var i;
              var fileList={};
               var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
               var token=systemConfig.get('token');
             for (i=0;i<values.length;i++)
             {
               var obj1=new Object();
               obj1.diaryid=values[i].diaryid
               obj1.student_id=token
               obj1.type=values[i].type
               obj1.title=values[i].title
               obj1.description=values[i].description
               obj1.created=values[i].created
               obj1.assignedday=values[i].assignedDay
               obj1.assignedtime=values[i].assignedTime
               obj1.dueday=values[i].dueDay
               obj1.duetime=values[i].dueTime
               obj1.completed=values[i].completed
               obj.subjectid=values[i].subjectId
               obj1.startday=values[i].startDay
               obj1.starttime=values[i].startTime
               obj1.endtime=values[i].endTime
               obj1.messagefrom=values[i].messageFrom
               obj1.messagefromtext=values[i].messageFromText
               obj1.lastupdated=values[i].lastupdated
               obj1.is_sync_desktop=1
               obj1.originalFilePath=values[i].originalFilePath
               obj1.displayName=values[i].displayName
               obj1.fileType=values[i].fileType
               obj1.localFileName=values[i].localFileName
               obj1.dateAttached=values[i].dateAttached
               obj1.attachmentid=values[i].id
               obj1.created_time=values[i].createdDate
              // console.log(obj1);
                dataArray.push(obj1);
             }
            /*===================*/
            var url =SystemSettings.SERVERURL+"/insertupdate";
            data =  {
            "param":dataArray        
            };
        
            ajaxType = 'POST'; 
           // console.log("TheCloud.callService: url=" + url + ", type=" + ajaxType + " data=" + JSON.stringify(data));
            $.ajax({
            'url' : url,
            'type': ajaxType,
            'data': data,
            'dataType' : "json",
            'success': function(result) {
           // console.log("single insert="+JSON.stringify(result));
            var resposne=result.response;
            var IdArray=[];
            $.each(resposne,function(i,j)
            {
                var objId={};
                $.each(j,function(key,value)
                {
                  // alert(key + ': ' + value);
                   
                   if(key=="itemid")
                   {
                   objId.id=value;
                   }
                   else if(key=="universalid")
                   {
                  // objId.Universal_ID=value;
                   }
                  
                })
                 IdArray.push(objId)
               
            });
                app.locator.getService(ServiceName.DIARYITEMMANAGER).updateUniversalIDs(IdArray, onupdateSuccess, onupdateError);
                app.locator.getService(ServiceName.ATTACHMENTDBDELEGATE).GetAttachments("", onGetAttachmentSuccess, onAttachmentError);
                function onGetAttachmentSuccess()
                {
                }
                function onAttachmentError()
                {
                }
                
                function onupdateSuccess()
                {
                    //alert("");
                }
                function onupdateError()
                {
                    
                }
                
            
                
           }, 
            'error': function(response) {
               //console.log(JSON.stringify(response));
                // console.log("insert TheCloud.error: ", JSON.stringify(response));
            }
        });
	}
		
   
          
        }
        
        function onerror(message) {
         //  alert("createDiary Item :here on error");
        }
        function callmethod(objId)
        {
         
        }
	};

	return _command;

})();
