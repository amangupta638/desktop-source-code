diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.GenerateTimeTableCommand = (function() {
	
	var _command = function() {
		console.log('GenerateTimeTableCommand()');

		this._timetableManager = app.locator.getService(ServiceName.TIMETABLEMANAGER);
	};

	_command.prototype.execute = function(year, doneCallback) {
        console.log('GenerateTimeTableCommand.execute(', year, ')');

        // get the calendar for the current year.
        // NOTE - for the time being it is going to be assumed that ONLY one calendar
        // exists for a scholastic year
        console.log("in geanrate calendar command : ");
        if (!year) {
			throw new Error("Year must be defined, year is not optional");
		}
		
		var manager = this, subjects,
			appState = app.model.get( ModelName.APPLICATIONSTATE ),
			startDate = appState.get('startDate'),
			endDate = appState.get('endDate');
		
		manager._timetableManager.getTimetables(new Rocketboots.date.Day(startDate), new Rocketboots.date.Day(endDate), subjects,
				
				// successfully got timetables
				function(timetables) {
					console.log("success timetables"+JSON.stringify(timetables));
					// get all the timetables for the year
					manager._dayOfNoteManager.getAllDaysOfNote(

							// successfully get the days of note
							function(daysOfNote) {
								var calendar = new diary.model.Calendar({ 'year' : year, 'timetables' : timetables });

								successCallback(calendar);
							},

							// error
							errorCallback || $.noop);
				},

				// error
				errorCallback || $.noop
		);
        
// the function that is to be executed on success retrieval of the calendar
        function success(calendar) {
        
        	console.log("success calendar"+JSON.stringify(calendar));
        	
            var cal = app.model.get(ModelName.CALENDAR);
            
            //console.log("success calendar1");
            cal.set('year', calendar.get('year'));
          
           // console.log("success calendar2");
            cal.get('timetables').reset(calendar.get('timetables').toArray());
          
           // console.log("success calendar3");
            cal.get('calendarDays').reset(calendar.get('calendarDays').toArray());
          
           // console.log("success calendar4");
            doneCallback && doneCallback();
           // console.log("success calendar11");
        }
		
		// the function that is to be executed on failing to retrieve the calendar
		function error(message) {
			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "GenerateTimeTableCommand", message);
			doneCallback && doneCallback();
		}
	};

	return _command;

})();
