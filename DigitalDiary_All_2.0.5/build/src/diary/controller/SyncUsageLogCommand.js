/**
 * $Id$
 */

/** @namespace */
diary = diary || {};

/** @namespace */
diary.controller = diary.controller || {};

/**
 * The command that is invoked to sync logged usage log events with the cloud
 *
 * @constructor
 */
diary.controller.SyncUsageLogCommand = (function() {

    var _command = function() {
       // console.log('SyncUsageLogCommand()');

        this._manager = app.locator.getService(ServiceName.USAGELOGMANAGER);
        this._theCloudManager = app.locator.getService(ServiceName.THECLOUDSERVICEMANAGER);
    };

    var syncUsageLog = function(usageLogEventArray, successCallback, errorCallback) {
        var command = this,
            logEventsToSync = usageLogEventArray.slice(0, SystemSettings.MAXUSAGELOGLENGTH);

        if (usageLogEventArray.length == 0) {
            successCallback();
            return;
        }

       command._theCloudManager.sendUsageLogs(
            new diary.collection.UsageLogEventCollection(logEventsToSync),
            function success(result) {
            //alert(result)
                var syncedEventIds = _.pluck(logEventsToSync, "id");
                switch(result.success) {
                    case "TRUE" :
                        // all were synced
                        break;
                    case "WARNING":
                        // all were synced, except those in DETAILS (which were duplicates)
                        // in which case, we can just mark them as synced again
                        break;
                    case "ERROR":
                        // all were synced except those in details
                        syncedEventIds = _.difference(syncedEventIds, result["DETAILS"]);
                        break;
                    default:
                       // console.log("Invalid response");
                        syncedEventIds = [];
                }

                if (syncedEventIds.length > 0) {
                    command._manager.markUsageLogAsSynced(syncedEventIds, successCallback, errorCallback);
                } else {
                    errorCallback("The Cloud responded with Errors");
                }
            },
            errorCallback
        );
    };

    var syncError = function(retrySyncInterval) {
        //console.log(new Date(), "Waiting " + retrySyncInterval + "ms before trying to sync usage log again");
        setTimeout(function() {
            app.context.trigger(EventName.SYNCUSAGELOG, retrySyncInterval * 2, true);
        }, retrySyncInterval);
    };

    _command.prototype.execute = function(retrySyncInterval, forceSync) {
       // console.log(': SyncUsageLogCommand::execute()', new Date());

        var command = this,
            appState = app.model.get(ModelName.APPLICATIONSTATE),
            currentlySyncing = appState.get('syncingUsageLog'),
            forceSync = forceSync || false;

		// forceSync==true indicates an automated sync after completing a previous, or after error timeout retry - seems it can be dropped
		
        if (currentlySyncing) {
            return; // when the current sync finishes, it will check the length of the queue and continue syncing if required
        }
		
		// If app has not been registered, then we can't sync logs.
		var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
		if (systemConfig.get('token') == null) {
			//console.log('SyncUsageLogCommand - Application not regsitered. Will not sync');
			return;
		}

        appState.set('syncingUsageLog', true);
        command._manager.getUnsyncedUsageLog(
            function success(usageLogEventCollection) {
                if (usageLogEventCollection.size() > 0) {
                    syncUsageLog.call(command, usageLogEventCollection.toArray(),
                        function onSuccess() {
                          //  console.log("SyncUsageLogCommand: Successfully synced usage log with the cloud");
                            appState.set('syncingUsageLog', false);
                            appState.set('lastUsageLogSyncTime', new Date());
                            // trigger the command again so that it can sync the next batch, if required
                            app.context.trigger(EventName.SYNCUSAGELOG, SystemSettings.USAGELOGSYNCINTERVAL, true);
                        },
                        function onError(msg) {
                          //  console.log("SyncUsageLogCommand: Error syncing usage log with the Cloud", msg);
                            appState.set('syncingUsageLog', false);
                            syncError(retrySyncInterval);
                        }
                    );
                } else {
                    appState.set('syncingUsageLog', false);
                   // console.log("SyncUsageLogCommand: Usage log is empty");
                }
            },
            function error(msg) {
                //console.log("SyncUsageLogCommand: Error getting unsynced usage log", msg);
                appState.set('syncingUsageLog', false);
                syncError(retrySyncInterval);
            }
        );

    };

    return _command;

})();
