diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.periodSyncData = (function () {
    // updated by aayshi


    var _command = function () {

        this._schoolManager = app.locator.getService(ServiceName.SCHOOLPROFILEDELEGATE);

    };


    _command.prototype.execute = function (doneCallback) {
		var resultsMain = "";
        doneCallback = doneCallback || $.noop;
		//alert('periodSyncData');
        this._schoolManager.getschoolid(success, error);



        var timetablids = [];


        function checkcontent(scid, lastsyncing) {


            var url = SystemSettings.SERVERURL + "/periodsync";
            var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
            
            var ajaxType = 'POST';
            var user_type = "Student";
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				user_type = "Teacher";
			} 
			//alert("lastsyncing : "+lastsyncing);
            var dataSend = {
                "schoolId": scid,
                "lastSynch": lastsyncing,
                "type": user_type,
                "user_id": currentUserId
            }

            dataSend = JSON.stringify(dataSend);
            //console.log("period sync url="+url+"data="+JSON.stringify(dataSend));

            $.ajax({
                'url': url,
                'type': ajaxType,
                'timeout' : 0,
                'data': dataSend,
                'dataType': "json",
                'success': function (data) {
            	var data = data
            	
            		console.log("periodsync details : "+JSON.stringify(data));
                    //console.log("success in retrievig period data");
                    resultsMain = data.details; 
                    dataSend = null;
                    
                    var appInstallStatusVal = app.model.get(ModelName.APPLICATIONSTATE).get('appInstallStatusVal');
                    if(appInstallStatusVal == 2){
                    	if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
                    		getClassUsers();
                    	}
                    	timeout();
                    }
                    
                    var deletedPeriods = data.deleted;
    	            $.each(deletedPeriods,function(index,serverrecord){
    					deletePeriodRecord(serverrecord.Id);
    	            }); 
    	            
    	            
                    //console.log("period results " + JSON.stringify(data))
                    $.each(resultsMain, function (index, element) { 
                       

                        var semname = element.semester_name;
                        var unID = element.timetableId;


                        app.locator.getService(ServiceName.APPDATABASE).runQuery(
                            // query
                            "SELECT * FROM  timetable WHERE (name) = :name ",
                            // params
                            {
                                'name': semname
                            },

                            // success

                            function (statement) {
                                var datas = statement.getResult().data;
								
								if(datas && datas != null && datas[0]){
	                                var ob = new Object();
	
	                                ob.tid = datas[0].id;
	                                ob.semname = datas[0].name;
	                                timetablids.push(ob);
	                                if (datas[0].UniqueID == null && datas[0].UniqueID != unID || datas[0].UniqueID == "") {
	
	                                    app.locator.getService(ServiceName.APPDATABASE).runQuery(
	                                        // query
	                                        "UPDATE timetable " +
	                                        "SET UniqueID = :unID " +
	                                        "WHERE name = :name",
	
	                                        // params
	                                        {
	                                            'name': semname,
	                                            'unID': unID
	
	                                        },
	                                        // success
	
	                                        function (statement) {},
	
	                                        // error
	
	                                        function error(statement) {
	                                            //console.log(statement + "   error in storing content to db")
	                                        }
	                                    );
	
	                                }
								}
								datas = null;
                            },

                            // error

                            function error() {
                               
                            }
                        );

                    });
                    

                },
                'complete': function (jqXHR, textStatus) {
                	
                //console.log("period sync complete");
                    var icr = 0;
                    $.each(resultsMain, function (index, element) {
                        var titles = element.title;
						
                        var stTime = new String(element.startTime);
                        stTime = stTime.split(":");
                        var newtime = parseFloat(stTime[0]) + "" +stTime[1]
                        newtime = parseInt(newtime);
                        //console.log(newtime)

                        var endTime = element.endTime;
                        endTime = endTime.split(":");
                        var newendtime = parseFloat(endTime[0]) + "" + endTime[1]
                        newendtime = parseInt(newendtime);

                        var cycleDays = (element.cycleDays == null || element.cycleDays == 0) ? null : element.cycleDays;
                        var hasBreak = element.hasBreak;
                        var unid = element.timetableId;
                        var semnames = element.semester_name;
                        var pid = element.id;
                        var forTeacher = element.ForTeacher;

                        app.locator.getService(ServiceName.APPDATABASE).runQuery(
                            // query
                            "SELECT id FROM  period WHERE (UniqueID) = :UniqueID ",

                            // params
                            {
                                'UniqueID': pid
                            },

                            // success
                            function (statement) {


                                var d = statement.getResult().data;
								
                                if (d != null) {
									
									//console.log("Update period with response id "+d[0].id)
                                    updatePeriod(titles, newtime, newendtime, cycleDays, hasBreak, unid, semnames, pid,d[0].id,forTeacher);
                                } else {

                                    var tids = "";
                                    for (var j = 0; j < timetablids.length; j++) {
                                        var checksin = timetablids[j];
                                        //alert(checksin.semname+"  "+checksin.tid+" "+titles)
                                        if (checksin.semname == semnames) {
                                            tids = checksin.tid

                                        }
                                    }
                                    var Query="";
                                    var params={};
                                    if(cycleDays!=null)
                                    {
										Query="SELECT  id FROM period WHERE title=:title AND timetable=:timetable AND cycleDay=:cycleDay";
										params={
										'title': titles,
                                        'timetable': tids,
									    'cycleDay':cycleDays
										}
									}
									else
									{
										Query="SELECT  id FROM period WHERE title=:title AND timetable=:timetable AND ifnull (period.cycleDay,'')=''";
										params={
										'title': titles,
                                        'timetable': tids
										}
									}
									//console.log("Query="+Query);
									//console.log("params="+params);

                                    app.locator.getService(ServiceName.APPDATABASE).runQuery(
                                        Query,


                                        // params
                                        params,

                                        // success

                                        function (statement) {

                                            var d = statement.getResult().data;

											//console.log("Query Result=");
											//console.log(d);
                                            if (d != null) {
												//console.log("Update period with its own  id "+d[0].id)
                                                updatePeriod(titles, newtime, newendtime, cycleDays, hasBreak, unid, semnames, pid,d[0].id,forTeacher)



                                            } else {
												//console.log("insert  period withnew data ")
                                                insertPeriod(titles, newtime, newendtime, cycleDays, hasBreak, unid, semnames, pid, tids,forTeacher)

                                            }

                                        },

                                        // error

                                        function error(statement) {
                                            console.log("error in upadting periodsync   " + statement)
                                        }
                                    );
                                }
                            },

                            // error

                            function error() {
                                console.log("error in periodsync");
                            }
                        );

                        icr++;

                        if (icr > resultsMain.length - 1) {
                            var datnow = new String(new Date().getDate());
                            var yearnow = new String(new Date().getFullYear());
                            var monthnow = new String(((new Date().getMonth()) + 1));
                            
                            var hoursnow = new String(new Date().getHours());
                            var minssnow = new String(new Date().getMinutes());
                            var secssnow = new String(new Date().getSeconds());
                            if (datnow.length == 1)
                                if (datnow.length == 1) {
                                    datnow = "0" + datnow
                                }

                            if (monthnow.length == 1) {
                                monthnow = "0" + monthnow
                            }

                            if (hoursnow.length == 1) {
                                hoursnow = "0" + hoursnow
                            }
                            if (minssnow.length == 1) {
                                minssnow = "0" + minssnow
                            }
                            if (secssnow.length == 1) {
                                secssnow = "0" + secssnow
                            }



                            timenow = yearnow + "-" + monthnow + "-" + datnow + " " + hoursnow + ":" + minssnow + ":" + secssnow;
							var periodSyncTime = parseInt(getTimestampmilliseconds());

                            //alert("timenow : "+timenow);
                            app.locator.getService(ServiceName.APPDATABASE).runQuery(
                                // query


                                "UPDATE lastSynch " +
                                "SET contentsync = :contentsync, lastSynch = :periodSyncTime " +
                                "WHERE tableName = :tableName",

                                // params
                                {
                                    'contentsync': timenow,
                                    'periodSyncTime' : periodSyncTime,
                                    'tableName': "periodSyncing"

                                },

                                // success

                                function (statement) {
									timenow = null;
									datnow = null;
									yearnow = null;
									monthnow = null;
									hoursnow = null;
									minssnow = null;
									secssnow = null;
                                    doneCallback();

                                    //console.log("last sync time stored in db"+timenow)
                                },

                                // error

                                function error() {
                                    //console.log("last sync time not stored in db"+timenow)
                                }
                            );
                        }

                    });

					timetablids = null;
					resultsMain = null;
					
                },
                'error': function (response) {

                    console.log("error in fetching periodsync");
                    FlurryAgent.logError("Period Sync Error", "Error in fetching data from periodsync web service",1);
                    dataDownloadError();
                }
            });


        }

        function updatePeriod(titles, newtime, newendtime, cycleDays, hasBreak, unid, semnames, pid,onid,forTeacher) {

            app.locator.getService(ServiceName.APPDATABASE).runQuery(
			
			
			  "UPDATE period " +
				"SET startTime = :startTime , endTime = :endTime, cycleDay = :cycleDay , title = :title , UniqueID = :UniqueID, isBreak = :isBreak, " +
				
				" forTeacher = :forTeacher WHERE id = :ids",
			
			

                // params
                {
                    'startTime': newtime,
                    'endTime': newendtime,
                    'cycleDay': cycleDays,
                    'title': titles,
                    'UniqueID': pid,
					'ids':onid,
					'isBreak': hasBreak,
					'forTeacher' : forTeacher
                },

                // success

                function (statement) {

                    //console.log("suc in updating periodsync" + statement)

                },

                // error

                function error(statement) {
                    console.log("error in upadting periodsync   " + statement)
                }
            );
        }


        function insertPeriod(titles, newtime, newendtime, cycleDays, hasBreak, unid, semnames, pid, tids,forTeacher) {

            app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "INSERT INTO period (startTime, endTime, cycleDay, title,isBreak,timetable, UniqueID, forTeacher)" +
                " VALUES (:startTime, :endTime, :cycleDay, :name , :isBreak, :timetable, :UniqueID, :forTeacher)",

                // params
                {
                    'startTime': newtime,
                    'endTime': newendtime,
                    'cycleDay': cycleDays,
                    'name': titles,
                    'isBreak': hasBreak,
                    'timetable': tids,
                    'UniqueID': pid,
                    'forTeacher': forTeacher
                },

                // success

                function (statement) {

                   // console.log(statement + "inserting periodsync")
                },

                // error

                function error(statement) {
                    console.log(statement + "error in inserting periodsync")
                }
            );
        }

        // the function that is to be executed on successful operation

        function success(scid) {

            if (scid != null) {
                app.locator.getService(ServiceName.APPDATABASE).runQuery(

                    // query
                    "SELECT * FROM lastSynch WHERE (tableName) = :tablename ",

                    // params
                    {
                        'tablename': "periodSyncing"
                    },

                    // success

                    function (statement) {
                        var result = statement.getResult().data;




                        lastsyncing = result[0].lastSynch


                        checkcontent(scid, lastsyncing)
						result = null;




                    },

                    // error

                    function (statement) {
                        //alert("error")
                    }




                );
            } else {}
        }

        // the function that is to be executed on failure of the operation

        function error(message) {


        }


    };




    return _command;

})();