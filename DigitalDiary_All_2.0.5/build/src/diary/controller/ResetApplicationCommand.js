diary = diary || {};
diary.controller = diary.controller || {};

diary.controller.ResetApplicationCommand = (function() {

    var _command = function() {
       // console.log('ResetApplicationCommand()');

        this._contentManager = app.locator.getService(ServiceName.CONTENTMANAGER);
		this._systemConfigurationDelegate = app.locator.getService(ServiceName.SYSTEMCONFIGURATIONDELEGATE);
    };    
    
    _command.prototype.execute = function() {

        // holds the command itself
        var command = this,
        	commandComplete = function() { // function to be called when sequence is done
        		app.context.trigger(EventName.STARTAPPLICATION);
        	}, 
        	deleteDirectoryFromLocalStore = function(folderName) {
            try {
                var configFolder = air.File.applicationStorageDirectory.resolvePath(folderName);
                if (configFolder.exists) {
                	configFolder.deleteDirectory(true);
                }        	
            } catch (e) {
				
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "ResetApplicationCommand", "deleteDirectoryFromLocalStore", e);
	
            }
        };
        
       // console.log('ResetApplicationCommand.execute()');

        // we need to clear away any system config data that is associated with the current token
        // so that when we trigger a restart we will be in a clean state

        /*
         * Reset School configuration 
         */

        // 1 - delete config directory from disk
        deleteDirectoryFromLocalStore("config");

        // 2 - set lastSchoolProfile check to null (done on successful registration by TheCloudServiceManager)  		


        /*
         * Reset Content packages
         */

        // 3 - delete /content directory from disk        
        deleteDirectoryFromLocalStore("content");
        
        // 4 - clear stored content packages table in db      
        command._contentManager.deleteStoredContentPackages(function onSuccess() {
            //console.log("Successfully cleared stored content packages");
            cleanUpDatabase();
        }, function onError(message) {

			app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR, "ResetApplicationCommand", "deleteStoredContentPackages", message);
			cleanUpDatabase();           
        });
        
		function cleanUpDatabase() {
			 // delete timetable, locked events and days of note, etc
			command._systemConfigurationDelegate.prepareForTokenRefresh(
				function(success) {
					app.context.trigger(EventName.REFRESHDIARYITEMS);
					commandComplete();
				}
			);
		}

    };

    return _command;

})();
