diary = diary || {};
diary.controller = diary.controller || {};
var checker = false;
var url;
var ajaxType;
var results, lastsyncing;
var syncer1 = false;
var syncer2 = false;
var indexer = 0;
var timenow;

diary.controller.datacontent = (function() {
	// updated by aayshi

	var schoolids;
	var pageno = 1;
	var counts = 0;
	var counter = 0;
	var limit;
	var limitadd = 0;
	var storedItems = 0;
	var len;

	var _command = function() {

		this._schoolManager = app.locator.getService(ServiceName.SCHOOLPROFILEDELEGATE);

	};

	_command.prototype.execute = function(doneCallback) {

		doneCallback = doneCallback || $.noop;

		this._schoolManager.getschoolid(success, error);
		
		function callupdate(lastsyncing, scid, pageno) {
			var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
			url = SystemSettings.SERVERURL + "/getschoolallcontents";
			var userType = "Student";
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				userType = "Teacher";
			}
			
			ajaxType = 'POST';
			
			lastsyncing = parseInt(lastsyncing);
			var dataSend = {
				"SchoolId" : scid,
				"user_id":userId,
				"lastsynch" : lastsyncing,
				"pageNO" : pageno,
				"userType" : userType
			}
			//   alert(url+""+JSON.stringify(dataSend))
			dataSend = JSON.stringify(dataSend);
			//console.log(url+""+JSON.stringify(dataSend)+"     updation")
			$.ajax({
				'url' : url,
				'type' : ajaxType,
				'data' : dataSend,
				'dataType' : "json",
				'success' : function(data) {
					//console.log(JSON.stringify(data))
					//console.log("success in fetching fromservice in update")

					results = data.result.result;
					
					var deletedContent = data.deleted;
					$.each(deletedContent,function(index,serverrecord){
						deleteContentRecord(serverrecord);            
		            });
					
					counts = data.result.count;
					if (counts == 0) {
						syncer1 = true;

					}

					if (syncer2 == true && syncer1 == true) {
						doneCallback();
					}

					limit = data.result.limit;
					limitadd += limit

					$.each(results, function(index, element) {

						// console.log("updateCounter"+counter)

						var d = element.Id;

						app.locator.getService(ServiceName.APPDATABASE).runQuery(

						// query
						"SELECT * FROM contentStorage WHERE (uniqueID) = :uniqueID ",

						// params
						{
							'uniqueID' : d
						},

						// success
						function(statement) {
							var result = statement.getResult().data;

							if (result != null) {

								//if (result[0].Content == element.Content) {
									//alert("tr")
									//console.log("no updating require")
								//} else {
									//alert(false)
									app.locator.getService(ServiceName.APPDATABASE).runQuery(
									// query
									"UPDATE contentStorage " + "SET Content = :Content, allStudents = :allStudents, allTeachers = :allTeachers, Title = :Title " + "WHERE uniqueID = :uniqueID",

									// params
									{
										'Content' : element.Content,
										'allStudents' : element.All_Students,
										'allTeachers' : element.All_Teachers,
										'Title' : element.Title,
										'uniqueID' : d

									},

									// success
									function(statement) {
										//console.log("updated contentStorage")

									},

									// error
									function(statement) {
										//console.log("error in updating contentStorage")

									})
								//}

							} else {

								app.locator.getService(ServiceName.APPDATABASE).runQuery(
								// query
								"INSERT INTO contentStorage (ContentType, Title, Content, uniqueID,respectedPanel,StartDate, EndDate, allStudents, allTeachers)" + 
								" VALUES (:ContentType, :Title, :Content, :uniqueID , :respectedPanel, :StartDate, :EndDate, :allStudents, :allTeachers)",

								// params
								{
									'ContentType' : element.ContentType,
									'Title' : element.Title,
									'Content' : element.Content,
									'uniqueID' : element.Id,
									'respectedPanel' : element.ContentType,
									'StartDate' : element.StartDate,
									'EndDate' : element.EndDate,
									'allStudents' : element.All_Students,
									'allTeachers' : element.All_Teachers
								},

								// success
								function(statement) {

									//console.log("success in storing new  content to db")
								},

								// error
								function error() {
									//console.log("error in storing new  content to db")
								});

							}

						},

						// error
						function(statement) {
							alert("error")
						});
						counter++;
						if (counter >= counts) {

							showapp = true;
							//alert(showapp)
							var datnow = new String(new Date().getDate());
							var yearnow = new String(new Date().getFullYear());
							var monthnow = new String(((new Date().getMonth()) + 1));

							var hoursnow = new String(new Date().getHours());
							var minssnow = new String(new Date().getMinutes());
							var secssnow = new String(new Date().getSeconds());
							if (datnow.length == 1)
								if (datnow.length == 1) {
									datnow = "0" + datnow
								}

							if (monthnow.length == 1) {
								monthnow = "0" + monthnow
							}

							if (hoursnow.length == 1) {
								hoursnow = "0" + hoursnow
							}
							if (minssnow.length == 1) {
								minssnow = "0" + minssnow
							}
							if (secssnow.length == 1) {
								secssnow = "0" + secssnow
							}

							timenow = yearnow + "-" + monthnow + "-" + datnow + " " + hoursnow + ":" + minssnow + ":" + secssnow;
							
							var contentSyncTime = getTimestampmilliseconds();
							
							//alert("alert : update lastSynch callupdate ");
							app.locator.getService(ServiceName.APPDATABASE).runQuery(
							// query
							
							"UPDATE lastSynch " + "SET contentsync = :contentsync " + "WHERE tableName = :tableName",

							// params
							{
								'contentsync' : contentSyncTime,
								'tableName' : "contentsyncData"

							},

							// success
							function(statement) {

								doneCallback();

								//console.log("last sync time stored in db"+timenow)
							},

							// error
							function error() {
								//console.log("last sync time not stored in db"+timenow)
							});

						}

						if (counter > limitadd - 1 && counter <= counts && counter < limitadd + 1) {
							pageno++;
							//  alert(counter+"sdsddsd")
							callupdate(lastsyncing, scid, pageno)
						}
					});

				},
				'error' : function(response) {
					//alert("alert : getschoolallcontents : ");
					FlurryAgent.logError("School Contents Sync Error", "Error in fetching data from getschoolallcontents web service",1);
					showapp = true;
					imagesaved = true;
					doneCallback && doneCallback();

					//console.log("calling update again")
					//   callupdate(lastsyncing,scid,pageno)
					// downloadFile(response.responseText);
				}
			});

		}

		function saveimage(imagesyncing, scid) {

			//console.log("savimage")

			var dataFolderMain, configMain, schoolMain, parentalMain, imagesfolder, localFile
			var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
			var url = SystemSettings.SERVERURL + "/getimages";

			var ajaxType = 'POST';

			imagesyncing = parseInt(imagesyncing);
			var data = {
				"SchoolId" : +scid,
				"user_id" : userId,
				"lastSynch" : imagesyncing
			}
			data = JSON.stringify(data);
			//console.log(url+""+data)
			$.ajax({
				'url' : url,
				'type' : ajaxType,
				'data' : data,
				'dataType' : "json",
				'success' : function(data) {
					console.log("get images  : "+JSON.stringify(data))
					var result = data.images;

					if (result.length == 0) {
						syncer2 = true;

					}

					if (syncer2 == true && syncer1 == true) {
						doneCallback();
					}
					function pathim() {
						//	$.each(result, function(index, element) {

						var element = result[indexer]

						var path = element.url;
						var stream = new air.URLStream();
						stream.load(new air.URLRequest(path));

						var filename = element.image;
						var pathname = path.split("/")
						console.log("imagescounter" + indexer)
						console.log(path)
						/*"http:\/\/54.253.121.67\/data\/config\/school_9\/AboutDaisy\/images\/aboutDaisy_flower.png
						 var patharray=new Array();*/

						dataFolderMain = air.File.applicationStorageDirectory.resolvePath(pathname[pathname.length - 6]);

						if (!dataFolderMain.exists) {
							dataFolderMain.createDirectory();
						}

						configMain = air.File.applicationStorageDirectory.resolvePath(pathname[pathname.length - 6] + "/" + pathname[pathname.length - 5]);

						if (!configMain.exists) {
							configMain.createDirectory();
						}

						schoolMain = air.File.applicationStorageDirectory.resolvePath(pathname[pathname.length - 6] + "/" + pathname[pathname.length - 5] + "/" + pathname[pathname.length - 4]);

						if (!schoolMain.exists) {
							schoolMain.createDirectory();
						}

						parentalMain = air.File.applicationStorageDirectory.resolvePath(pathname[pathname.length - 6] + "/" + pathname[pathname.length - 5] + "/" + pathname[pathname.length - 4] + "/" + pathname[pathname.length - 3]);

						if (!parentalMain.exists) {
							parentalMain.createDirectory();
						}

						imagesfolder = air.File.applicationStorageDirectory.resolvePath(pathname[pathname.length - 6] + "/" + pathname[pathname.length - 5] + "/" + pathname[pathname.length - 4] + "/" + pathname[pathname.length - 3] + "/" + pathname[pathname.length - 2]);

						if (!imagesfolder.exists) {
							imagesfolder.createDirectory();
						}

						localFile = imagesfolder.resolvePath(filename);

						stream.addEventListener(air.Event.COMPLETE, function(e) {
							//alert("download complete");
							var fileData = new air.ByteArray();
							stream.readBytes(fileData, 0, stream.bytesAvailable);
							var fileStream = new air.FileStream();
							fileStream.openAsync(localFile, air.FileMode.WRITE);
							fileStream.writeBytes(fileData, 0);
							fileStream.close();
							if (indexer < result.length - 1) {
								console.log("sssssss" + indexer)
								if (indexer == result.length - 2) {
									//alert("alert : imagesaved saveimage : getImages service");	
									imagesaved = true;
									var imgtimenow = parseInt(getTimestampmilliseconds());
									//alert("alert : update  lastSynch");		
									app.locator.getService(ServiceName.APPDATABASE).runQuery(
									// query
											
									"UPDATE lastSynch " + "SET contentsync = :contentsync " + "WHERE tableName = :tableName",

									// params
									{
										'contentsync' : imgtimenow,
										'tableName' : "imagesyncing"

									},

									// success
									function(statement) {
										if (imagesaved == true && showapp == true) {
											doneCallback();
										}

										//console.log("last sync time stored in db"+imgtimenow)
									},

									// error
									function error() {
										//console.log("last sync time not stored in db"+imgtimenow)
									});

								}
								indexer++;
								pathim();
							}

						});
						stream.addEventListener("ioError", function (error) {
							showapp = true;
							doneCallback();
						});

					}

					if (result.length > 0) {
						pathim();
					} else {
						showapp = true;
						//imagesaved = true;
					}
					//	});
					//

				},
				'error' : function(response) {
					FlurryAgent.logError("School Content Images Sync Error", "Error in fetching data from getimages web service",1);
					// console.log("error in updating content and passing again")
					// saveimage(imagesyncing,scid);
					// downloadFile(response.responseText);
				}
			});

		}

		function insertdata(lastsyncing, scid, pageno) {
			//console.log("insertdata")
			//inserting content in db
			var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
			var url = SystemSettings.SERVERURL + "/getschoolallcontents";
			var userType = "Student";
			if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
				userType = "Teacher";
			}
			
			var ajaxType = 'POST';
			lastsyncing = parseInt(lastsyncing);
			var dataSend = {
				"SchoolId" : scid,
				"user_id":userId,
				"lastsynch" : lastsyncing,
				"pageNO" : pageno,
				"userType" : userType
			}

			dataSend = JSON.stringify(dataSend);
			//console.log(url+""+JSON.stringify(dataSend))
			//	alert(url+""+JSON.stringify(dataSend))
			$.ajax({
				'url' : url,
				'type' : ajaxType,
				'data' : dataSend,
				'dataType' : "json",
				'success' : function(data) {//alert(JSON.stringify(data))
					//console.log("success in retrievig")

					var results = data.result.result;
					
					/* var deletedContent = data.result.deleted;
					$.each(deletedContent,function(index,serverrecord){
						deleteContentRecord(serverrecord);            
		            }); */

					counts = data.result.count;
					limit = data.result.limit;
					limitadd += limit

					$.each(results, function(index, element) {//alert(counter)
						// console.log("insertCounter"+counter)

						app.locator.getService(ServiceName.APPDATABASE).runQuery(
						// query
						"INSERT INTO contentStorage (ContentType, Title, Content, uniqueID,respectedPanel,StartDate, EndDate, allStudents, allTeachers)" + 
						" VALUES ( :ContentType, :Title, :Content, :uniqueID , :respectedPanel, :StartDate, :EndDate, :allStudents, :allTeachers)",

						// params
						{
							'ContentType' : element.ContentType,
							'Title' : element.Title,
							'Content' : element.Content,
							'uniqueID' : element.Id,
							'respectedPanel' : element.ContentType,
							'StartDate' : element.StartDate,
							'EndDate' : element.EndDate,
							'allStudents' : element.All_Students,
							'allTeachers' : element.All_Teachers
						},

						// success
						function(statement) {

							//console.log("success in storing content to db")
						},

						// error
						function error() {
							//console.log("error in storing content to db")
						});
						//alert(counter)

						counter++;
						//showapp = true;
						//if (counter >= counts) {

							

						//}

						if (counter > limitadd - 1 && counter <= counts && counter < limitadd + 1) {
							pageno++;

							insertdata(lastsyncing, scid, pageno);
						}

						//successCallback(result);
					});
					
					showapp = true;
					//alert(showapp)
					var datnow = new String(new Date().getDate());
					var yearnow = new String(new Date().getFullYear());
					var monthnow = new String(((new Date().getMonth()) + 1));

					var hoursnow = new String(new Date().getHours());
					var minssnow = new String(new Date().getHours());
					var secssnow = new String(new Date().getHours());
					if (datnow.length == 1)
						if (datnow.length == 1) {
							datnow = "0" + datnow
						}

					if (monthnow.length == 1) {
						monthnow = "0" + monthnow
					}

					if (hoursnow.length == 1) {
						hoursnow = "0" + hoursnow
					}
					if (minssnow.length == 1) {
						minssnow = "0" + minssnow
					}
					if (secssnow.length == 1) {
						secssnow = "0" + secssnow
					}

					timenow = yearnow + "-" + monthnow + "-" + datnow + " " + hoursnow + ":" + minssnow + ":" + secssnow;
					var contentSyncTime = getTimestampmilliseconds();
					
					//alert("alert : update  lastSynch insertdata");
					app.locator.getService(ServiceName.APPDATABASE).runQuery(
					// query
							
					"UPDATE lastSynch " + "SET contentsync = :contentsync " + "WHERE tableName = :tableName",

					// params
					{
						'contentsync' : contentSyncTime,
						'tableName' : "contentsyncData"

					},

					// success
					function(statement) {
						//alert('UPDATE success');
						if (imagesaved == true || showapp == true) {
							doneCallback();
						}

						//console.log("last sync time stored in db"+timenow)
					},

					// error
					function error() {
						//console.log("last sync time not stored in db"+timenow)
					});

				},
				'error' : function(response) {
					FlurryAgent.logError("School Contents Sync Error", "Error in fetching data from getschoolallcontents web service",1);
					//console.log("error in fetching content and passing again")
					//  insertdata(lastsyncing,scid,pageno)
					// downloadFile(response.responseText);
				}
			});

		}

		// the function that is to be executed on successful operation
		function success(scid) {
			
			if (scid != null) {
				app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"SELECT * FROM lastSynch WHERE (tableName) = :tablename ",
				// params
				{
					'tablename' : "imagesyncing"
				},

				// success
				function(statement) {
					//alert("alert : imagesaved function success 1: ");
					var result = statement.getResult().data;

					if (result != null) {

						imagesyncing = result[0].contentsync;

						app.locator.getService(ServiceName.APPDATABASE).runQuery(

						// query
						"SELECT * FROM lastSynch WHERE (tableName) = :tablename ",
						// params
						{
							'tablename' : "contentsyncData"
						},

						// success
						function(statement) {
							var result = statement.getResult().data;
								//alert("alert : imagesaved function success 2 :"+result);	
							if (result != null) {
								//alert("alert : imagesaved function success 2.1 :"+result[0].contentsync);
								lastsyncing = result[0].contentsync

								if (lastsyncing != null) {
									//alert("alert : imagesaved function success 3 :");	
									showapp = true;
									imagesaved = true;

									callupdate(lastsyncing, scid, pageno)
									saveimage(imagesyncing, scid)
								} else {

									saveimage(imagesyncing, scid)

									insertdata(lastsyncing, scid, pageno);

								}

							} else {
							}

						},

						// error
						function(statement) {
							alert("function success error")
						});

					} else {
					}

				},

				// error
				function(statement) {
					//alert("error")
				});

			} else {
				showapp = true;
			}
		}

		// the function that is to be executed on failure of the operation
		function error(message) {

		}

	};

	return _command;

})();
