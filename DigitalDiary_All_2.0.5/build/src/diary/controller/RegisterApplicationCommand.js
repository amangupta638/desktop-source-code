 /**
 * The command that is to be executed to register the application
 * 
 *  @author Brian Bason
 */
diary = diary || {};
diary.controller = diary.controller || {};
 
diary.controller.RegisterApplicationCommand = (function() {
			
	/**
	 * Construct the Register Command
	 * 
	 * @constructor
	 */
	var _command = function() {
		//console.log('RegisterApplicationCommand()');
		
		this._registrationManager = app.locator.getService(ServiceName.REGISTRATIONMANAGER);
	};
	
	
	/**
	 * Executes the command
	 */
	_command.prototype.execute = function() {

		// holds the command that is being executed
		var command;
		try {
			
			// keep track of the command since we need it later on
			command = this;
			//TODO : remove showToken()
			//app.view.tokenView.showToken();
			//alert("call to login view");
			app.view.loginView.showLogin(false);
			
			
		} catch (error) {
			// log the error so that we can keep track of what is going on
//			console.log("Error while executing RegisterApplication Command");
//			console.log(error);
            app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.ERROR,
                "RegisterApplicationCommand: " + error);
		}
		
	};
	
	// return the created command
	return _command;
	
})();
