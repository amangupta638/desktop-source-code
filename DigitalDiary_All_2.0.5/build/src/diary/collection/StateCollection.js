/**
 * The model that represents a collection of state
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.StateCollection = (function() {

	/**
	 * Creates a collection of States
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({
		model : diary.model.State,
		getStatesForCountry : function(countryId) {
			
			var states = _.filter(this.toArray(), function(state) {
				return state instanceof diary.model.State
				 && (state.get('countryId') == countryId)
				
			});
			console.log("states : "+JSON.stringify(states));
			return new diary.collection.StateCollection(states);
		}
	});
	
})();