/**
 * The model that represents a collection of Content Items in a diary.
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.ContentItemCollection = (function() {

	/**
	 * Creates a collection of Content Items
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({
		model: diary.model.ContentItem
	});
	
})();