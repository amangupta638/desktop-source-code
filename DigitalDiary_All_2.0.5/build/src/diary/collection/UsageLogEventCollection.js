/**
 * The model that represents a collection of usage log events in a application.
 *
 * author - Jane Sivieng
 */
diary = diary || {};
diary.collection = diary.collection || {};

diary.collection.UsageLogEventCollection = (function() {

    /**
     * Creates a collection of usage logs
     *
     * @constructor
     */
    return Backbone.Collection.extend({
        model : diary.model.UsageLogEvent
    });

})();