/**
 * The model that represents a collection of Search Result Section which section is related to a search result
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.SearchResultSectionCollection = (function() {

	/**
	 * Creates a collection of Search Result Sections
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({
		
		model: diary.model.SearchResultSection
		
	});
	
})();
