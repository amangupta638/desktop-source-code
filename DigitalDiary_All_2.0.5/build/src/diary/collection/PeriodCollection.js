/**
 * The model that represents a collection of PERIODs in a diary.
 * 
 * author - John Pearce
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.PeriodCollection = (function() {

	/**
	 * Creates a collection
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

		initialize: function () {
			this.on("change:startTime", function () {this.sort();}, this);
		},
		
		model: diary.model.Period,
		
		/**
		 * The function that is used to keep the collection sorted.  The collection
		 * will be sorted by the start time of the periods so that we will have the list
		 * sorted in the natural order
		 * 
		 * @param {diary.model.Period} period The period that is to be sorted in the collection
		 * 
		 * @return {integer} The timestamp of the start time that represents the period
		 */
		comparator : function(period) {
			return (period.get('startTime') ? period.get('startTime').toInteger() : 0);
		}
		
	});
	
})();