/**
 * The model that represents a collection of students
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.ParentCollection = (function() {

	/**
	 * Creates a collection of parent
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

	
		
		model : diary.model.Student,
		
	
	});
	
})();