new diary.collection.CalendarDayCollection()/**
 * The model that represents a collection of Subjects in a application.
 * 
 * author - Brian Bason/Jane Sivieng
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.SubjectCollection = (function() {

	/**
	 * Creates a collection of Timetables
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

		initialize: function () {
			this.on("change:title", function () {this.sort();}, this);
		},
		
		model : diary.model.Subject,
		
		/**
		 * The function that is used to keep the collection sorted.  The subject collection
		 * will be sorted by title in ascending alphabetical order.
		 * 
		 * @param {diary.model.Subject} subject The subject that is to be sorted in the collection
		 * 
		 * @return {integer} The string title of the subject
		 */
		comparator : function(subject) {
			// the sort field is the start date of the timetable
			return subject.get('title');
		}
	
	});
	
})();