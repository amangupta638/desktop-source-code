/**
 * The model that represents a collection of NOTE Entries, within a Note diary item.
 * 
 * author - John Pearce
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.NoteEntryCollection = (function() {

	/**
	 * Creates a collection of Note Entries
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

		initialize: function () {
			this.on("change:timestamp", function () {this.sort();}, this);
		},
		
		model: diary.model.NoteEntry,
		
		/**
		 * The function that is used to keep the collection sorted.  The note entry collection
		 * will be sorted by the timestamp of the note entry.
		 * 
		 * @param {diary.model.NoteEntry} noteEntry The note entry that is to be sorted in the collection
		 * 
		 * @return {integer} The timestamp of the date that represents the note entry
		 */
		comparator : function(noteEntry) {
			// the sort field is the date of the diary item
			return noteEntry.get('timestamp').getTime();
		}
	
	});
	
})();