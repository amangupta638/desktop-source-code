/**
 * The model that represents a collection of CLASSes in a calendar.
 * 
 * author - John Pearce
 */
diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.ClassCollection = (function() {

	/**
	 * Creates a collection of Classes
	 * 
	 * @constructor
	 */
	return Backbone.Collection.extend({

		initialize: function () {
			this.on("change:cycleDay change:periodId", function () {this.sort();}, this);
		},
		
		model : diary.model.Class,
		
		getCurrentSemClasses : function(timetableId) {
			var classes = _.filter(this.toArray(), function(currentClass) {
				return currentClass && currentClass.get('timeTableID') == timetableId
			});
			return new diary.collection.ClassCollection(classes);
		},
		
		/**
		 * The function that is used to keep the collection sorted.  The collection
		 * will be sorted by the cycle day then period of the classes so that we will have the list
		 * sorted in the natural order
		 * 
		 * @param {diary.model.Class} classObjectA  
		 * @param {diary.model.Class} classObjectB 
		 * 
		 * @return {integer} sort result
		 */
		comparator : function(classObjectA, classObjectB) {
			var cycleDayDiff = classObjectA.get('cycleDay') - classObjectB.get('cycleDay'); 
			if (cycleDayDiff != 0) {
				return cycleDayDiff;
			}
			// else cycleDay is equal so compare the period id
			return classObjectA.get('periodId') - classObjectB.get('periodId');
		}
	
	});
	
})();