/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id$
 */

/** @namespace */
var diary = diary || {};
diary.collection = diary.collection || {};
 
diary.collection.DiaryItemCollection = (function() {

	/**
	 * Constructor.
	 */
	return Backbone.Collection.extend({

		model: diary.model.DiaryItem,

		/**
		 * Gets the collection of Tasks assigned within a date range from the current collection.
		 * 
		 * @param {!Rocketboots.date.Day} fromDay The date from when to start filtering the required events and events
		 * @param {!Rocketboots.date.Day} toDay The date to when to stop filtering the required events and events
		 * 
		 * @return {diary.collection.DiaryItemCollection} The collection of Tasks assigned within the specified time period
		 */
		getTasksAssignedInDateRange : function(fromDay, toDay, includeCompleted) {
			if (!fromDay) {
				throw new Error("From Day must be specified");
			}

			if (!toDay) {
				throw new Error("To Day must be specified");
			}

			var filteredDiaryItems = _.filter(this.toArray(), function(diaryItem) {
				return diaryItem instanceof diary.model.Task
					&& (includeCompleted || !diaryItem.isCompleted())
					&& diaryItem.get('assignedDay').getKey() >= fromDay.getKey()
					&& diaryItem.get('assignedDay').getKey() <= toDay.getKey();
			});

			return new diary.collection.DiaryItemCollection(filteredDiaryItems);
		},

		/**
		 * Gets the collection of Tasks due within a date range from the current collection and are not overdue
		 * 
		 * @param {!Rocketboots.date.Day} fromDay The date from when to start filtering the required events and events
		 * @param {!Rocketboots.date.Time} fromTime The time from when to start filtering the required events and events
		 * @param {!Rocketboots.date.Day} toDay The date to when to stop filtering the required events and events
		 * 
		 * @return {diary.collection.DiaryItemCollection} The collection of Tasks due within the specified time period
		 */
		getTasksDueInDateRange : function(fromDay, fromTime, toDay, includeCompleted) {
			if (!fromDay) {
				throw new Error("From Day must be specified");
			}

			if (!fromTime) {
				throw new Error("From Time must be specified");
			}

			if (!toDay) {
				throw new Error("To Day must be specified");
			}

			var filteredDiaryItems = _.filter(this.toArray(), function(diaryItem) {
				return diaryItem instanceof diary.model.Task
					&& (includeCompleted || !diaryItem.isCompleted())
					&& diaryItem.get('dueDay').getKey() >= fromDay.getKey()
					&& diaryItem.get('dueDay').getKey() <= toDay.getKey()
					&& !diaryItem.isOverdue(fromDay, fromTime);
			});

			return new diary.collection.DiaryItemCollection(filteredDiaryItems);
		},

		/**
		 * Gets the collection of non-overdue, incomplete Tasks from the current collection.
		 * 
		 * @return {diary.collection.DiaryItemCollection} The collection of non-overdue Tasks
		 */
		getNonOverdueTasks: function() {
			var tasks = _.filter(this.toArray(), function(diaryItem) {
				return diaryItem instanceof diary.model.Task
					&& !diaryItem.isCompleted()
					&& !diaryItem.isOverdue();
			});
			return new diary.collection.DiaryItemCollection(tasks);
		},

		/**
		 * Gets the collection of overdue, incomplete Tasks from the current collection.
		 * 
		 * @return {diary.collection.DiaryItemCollection} The collection of overdue Tasks
		 */
		getOverdueTasks: function() {
			var tasks = _.filter(this.toArray(), function(diaryItem) {
				return diaryItem instanceof diary.model.Task && diaryItem.isOverdue();
			});

			return new diary.collection.DiaryItemCollection(tasks);
		},
		
		getRequiredOverdueTasks: function() {
			
			var rangeEndDay = new Rocketboots.date.Day().addDays(15);
			var rangeStartDay = new Rocketboots.date.Day().addDays(-15);
			
			var tasks = _.sortBy(this.toArray(), function(diaryItem) {
				return diaryItem instanceof diary.model.Task && diaryItem.get('dueDay').getKey();
			});
			
			tasks = _.filter(tasks, function(diaryItem) {
				return diaryItem instanceof diary.model.Task 
				&& !diaryItem.isCompleted()
				&& diaryItem.get('dueDay').getKey() <= rangeEndDay.getKey() 
				&& diaryItem.get('dueDay').getKey() >= rangeStartDay.getKey()
				&& diaryItem.get('showDue') == true;
			});
			//alert("getRequiredOverdueTasks "+tasks.length);
			return new diary.collection.DiaryItemCollection(tasks);
		},
		
		getRequiredAssignedTasks : function() {
			
			var rangeEndDay = new Rocketboots.date.Day().addDays(15);
			var rangeStartDay = new Rocketboots.date.Day().addDays(-15);
			
			var assignedTasks = _.sortBy(this.toArray(), function(diaryItem) {
				return (diaryItem instanceof diary.model.Task && diaryItem.get('assignedDay').getKey())
						|| (diaryItem instanceof diary.model.Note && diaryItem.get('atDay').getKey());
			});
			
			assignedTasks = _.filter(assignedTasks, function(diaryItem) {
					return (diaryItem instanceof diary.model.Task
							&& diaryItem.get('showAssigned') == true
							&& diaryItem.get('assignedDay').getKey() <= rangeEndDay.getKey() 
							&& diaryItem.get('assignedDay').getKey() >= rangeStartDay.getKey()
							&& !diaryItem.isCompleted())
							|| (diaryItem instanceof diary.model.Note
							&& diaryItem.get('showAssigned') == true
							&& diaryItem.get('atDay').getKey() <= rangeEndDay.getKey() 
							&& diaryItem.get('atDay').getKey() >= rangeStartDay.getKey());
					//If assigned to students. Removed this check on 18 Jan 2014
					//&& diaryItem.get('assignedtoMe') == 0; 
			});

			if(assignedTasks && assignedTasks.length > 1){
				assignedTasks = assignedTasks.reverse();
			}
			return new diary.collection.DiaryItemCollection(assignedTasks);
		},
		
		getNotifications: function() {
			var currentUserId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
		
//			var notifications = _.filter(this.toArray(), function(diaryItem) {
//				
//				return diaryItem instanceof diary.model.Task
//					&& diaryItem.get('showNotifications') == true
//					&& diaryItem.isAssignedToday()
//					&& !diaryItem.isCompleted()
//					&& diaryItem.get('createdBy') != currentUserId; 
//			});
			
			
			var notifications = _.filter(this.toArray(), function(diaryItem) {
				
				return (diaryItem instanceof diary.model.Task
							&& diaryItem.get('showNotifications') == true
							&& diaryItem.isAssignedToday()
							&& !diaryItem.isCompleted()
							&& diaryItem.get('createdBy') != currentUserId) 
					|| (diaryItem instanceof diary.model.Note
							&& diaryItem.get('showNotifications') == true
							&& diaryItem.isAssignedToday()
							&& diaryItem.get('createdBy') != currentUserId)
					|| (diaryItem instanceof diary.model.Event
							&& diaryItem.get('showNotifications') == true
							&& diaryItem.isAssignedToday()
							&& diaryItem.get('createdBy') != currentUserId); 
			});

			return new diary.collection.DiaryItemCollection(notifications);
			
		},
		
		getDiaryItemsForToday : function() {
			var today = new Rocketboots.date.Day();
			
			var diaryItems = _.filter(this.toArray(), function(diaryItem) {
				return (diaryItem instanceof diary.model.Event && diaryItem.get('startDay').getKey() == today.getKey())
					|| (diaryItem instanceof diary.model.Note && diaryItem.get('atDay').getKey() == today.getKey())
					|| (diaryItem instanceof diary.model.Task && diaryItem.get('assignedDay').getKey() == today.getKey())
					|| (diaryItem instanceof diary.model.Task && diaryItem.get('dueDay').getKey() == today.getKey());
			});

			return new diary.collection.DiaryItemCollection(diaryItems);
		},
		
		getUpcomingDiaryItems : function() {
			var today = new Rocketboots.date.Day();
			//var lastDay = new Rocketboots.date.Day(today.getFullYear(), today.getMonth() + 1,31).getKey();
			var lastDay = new Rocketboots.date.Day().addDays(15);
			
			var diaryItems = _.filter(this.toArray(), function(diaryItem) {
				return (diaryItem instanceof diary.model.Event && diaryItem.get('startDay').getKey() > today.getKey() && diaryItem.get('startDay').getKey() <= lastDay.getKey())
					|| (diaryItem instanceof diary.model.Task && diaryItem.get('dueDay').getKey() > today.getKey() && diaryItem.get('dueDay').getKey() <= lastDay.getKey());
			});

			return new diary.collection.DiaryItemCollection(diaryItems);
		},
		
		getPreviousDiaryItems : function() {
			var today = new Rocketboots.date.Day();
			//var lastDay = new Rocketboots.date.Day(today.getFullYear(), today.getMonth() - 1,1).getKey();
			var lastDay = new Rocketboots.date.Day().addDays(-15);
			
			var diaryItems = _.filter(this.toArray(), function(diaryItem) {
				return (diaryItem instanceof diary.model.Note && diaryItem.get('atDay').getKey() < today.getKey() && lastDay.getKey() <= diaryItem.get('atDay').getKey())
					|| (diaryItem instanceof diary.model.Task && !diaryItem.isCompleted() && diaryItem.get('assignedDay').getKey() < today.getKey() && lastDay.getKey() <= diaryItem.get('assignedDay').getKey())
					|| (diaryItem instanceof diary.model.Task && !diaryItem.isCompleted() && diaryItem.get('dueDay').getKey() < today.getKey() && lastDay.getKey() <= diaryItem.get('dueDay').getKey());
			});

			return new diary.collection.DiaryItemCollection(diaryItems);
		},
		
		getDiaryItems: function() {
			
			var isNote = false;
			var isTask = false;
			var isEvent = false;
			
			if($('#cal_filter_notes').attr('checked')) {
				isNote = true;
			}
			
			if($('#cal_filter_events').attr('checked')) {
				isEvent = true;
			}
			
			if($('#cal_filter_tasks').attr('checked')) {
				isTask = true;
			}
			
			var diaryItems;
			
			if(isNote || isTask || isEvent){
				diaryItems = _.filter(this.toArray(), function(diaryItem) {
					
					if(isNote && isTask && isEvent){
						return diaryItem; 
					} else if(isTask && isEvent){
						return (diaryItem instanceof diary.model.Task) || (diaryItem instanceof diary.model.Event); 
					} else if(isEvent && isNote){
						return (diaryItem instanceof diary.model.Event) || (diaryItem instanceof diary.model.Note); 
					} else if(isNote && isTask){
						return (diaryItem instanceof diary.model.Note) || (diaryItem instanceof diary.model.Task); 
					} else if(isTask){
						return diaryItem instanceof diary.model.Task; 
					} else if(isEvent){
						return diaryItem instanceof diary.model.Event; 
					} else if(isNote){
						return diaryItem instanceof diary.model.Note; 
					} 
				});
			} else {
				diaryItems = _.filter(this.toArray(), function(diaryItem) {
					return diaryItem; 
				});
			}
			return new diary.collection.DiaryItemCollection(diaryItems);
		},
		getAssignmentHomework : function() {
			var assignedTasks = _.filter(this.toArray(), function(diaryItem) {
				return (diaryItem.get('type') == "ASSIGNMENT" || diaryItem.get('type') == "HOMEWORK")
					
			});

			return new diary.collection.DiaryItemCollection(assignedTasks);
		},
		getAssignment : function(selectedTypeId) {
			
			var assignedTasks = _.filter(this.toArray(), function(diaryItem) {
				return (diaryItem.get('localDairyItemTypeId') == selectedTypeId)
				
			});
			console.log("assignment : "+JSON.stringify(assignedTasks));
			return new diary.collection.DiaryItemCollection(assignedTasks);
		},
		getHomework : function(selectedTypeId) {
			var assignedTasks = _.filter(this.toArray(), function(diaryItem) {
				return diaryItem instanceof diary.model.Task
					&& (diaryItem.get('localDairyItemTypeId') == selectedTypeId)
					
			});
			console.log("hw : "+JSON.stringify(assignedTasks));
			return new diary.collection.DiaryItemCollection(assignedTasks);
		},
		
		getEventsOnTheDay: function(day) {
			var currentUserId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
			var events = _.filter(this.toArray(), function(diaryItem) {
				return diaryItem instanceof diary.model.Event 
				&& diaryItem.isWithinDay(day)
				&& diaryItem.get('createdBy') != currentUserId;
			});

			return new diary.collection.DiaryItemCollection(events);
		}
		
	
	});
	
})();
