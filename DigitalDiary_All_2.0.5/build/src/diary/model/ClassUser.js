// model which keeps mapping of class and its user for my class

diary = diary || {};
diary.model = diary.model || {};
 
diary.model.ClassUser = (function() {
	
	/**
	 * @constructor
	 */
	return Backbone.Model.extend({
			
			
		// init the calendar with the default values
		defaults : function() {
			var attrs = { 
							'class': null,											//classId
						  	'users': new diary.collection.StudentCollection()		// users of class
						};
			return attrs;
		}
	});
})();