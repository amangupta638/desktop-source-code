/**
 * The model that represents a NOTIFICATION in a diary.
 * A Notification is a message that is to be shown to the student either as a reminder.
 * 
 * author - Brian Bason
 * @constructor
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.Notification = (function() {
	
	// we will extend the Model for the Notification and keep a reference to it so that
	// we can late on add some static functions to the model.
	var notification = Backbone.Model.extend({

		// init the notification with the default values
		defaults : function() {
			
			// define the notification attributes
			var attrs = { "title" : null,					// the title that is to be used for the notification
						  "message" : null,					// the message that is to be shown in the notification
						  "type" : null,					// the type of notification that is being created
						  "colour" : null };				// the requested colour/theme of the notification, no validation of this done within the model (optional)
		
			// return the attributes of the model
			return attrs;
		},
		
		/*
		 * the validation function for the Notification
		 * NOTE - This is only required to make sure that the notification keeps to a particular structure
		 * 
		 * @param {object} attrs The attributes that are set for the Notification
		 *
		 * @return {string} The error, if any, that is generated by the validation method
		 */ 
		validate : function(attrs) {
			
			// holds if the type is a valid type or not
			var typeValid = false;
			// holds the different types of tasks
			var types = "";
			
			// determine that all the attributes have been given
			if (!isSpecified(attrs.title)) {
				return "Title must be specified, title cannot be NULL";
			}
			
			if (typeof(attrs.title) != 'string') {
				return "Title must be of type string";
			}
			
			if (!isSpecified(attrs.message)) {
				return "Message must be specified, message cannot be NULL";
			}
			
			if (typeof(attrs.message) != 'string') {
				return "Message must be of type string";
			}
			
			if (!isSpecified(attrs.type)) {
				return "Type must be specified, type cannot be NULL";
			}
			
			// check if the given type is one of the valid ones
			for (typeKey in diary.model.Notification.NotificationType) {
				// populate the types
				types += diary.model.Notification.NotificationType[typeKey] + ",";
				
				if (diary.model.Notification.NotificationType[typeKey] == attrs.type) {
					typeValid = true;
					break;
				}
			}
			
			if (!typeValid) {
				return "Type is not valid, type must be one of [" + types.substring(0, types.length - 1) + "]";
			}
			
		}
		
	});
	
	/**
	 * Defines the different types of Notification Types that can exist in the system
	 * NOTE - This will be used to determine the different styles / icons that are to be used
	 * for the notification
	 * @enum {string}
	 */
	notification.NotificationType = {
			REMINDER : "REMINDER",
			INFO : "INFO",
			HOMEWORK: "HOMEWORK",
			ASSIGNMENT: "ASSIGNMENT"
	};
	
	return notification;
	
})();

