/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: Document.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
diary = diary || {};

/** @namespace */
diary.model = diary.model || {};

/**
 * The Document class represents a single, searchable document or page of
 * content that is typically downloaded and available in the School Information
 * or Empower section. This also includes the Empower sidebar content.
 * 
 * @constructor
 */
diary.model.Document = (function() {

	var document = Backbone.Model.extend({

		defaults : function() {
			return {
				'type' : null,
				'title' : null,

				// path to document on file system
				'documentId' : null
			};
		},

		validate : function(attrs) {

			if (!isSpecified(attrs.type)) {
				return "No Type specified";
			}

			if (!isSpecified(attrs.title)) {
				return "No Title specified";
			}

			if (!isSpecified(attrs.documentId)) {
				return "No Document Identifier specified";
			}

			if (!diary.model.Document.Types.hasOwnProperty(attrs.type)) {
				return "Type is invalid";
			}

			if (typeof attrs.title !== "string") {
				return "Title must be of type string";
			}

			if (typeof attrs.documentId !== "string") {
				return "Document Identifier must be of type string";
			}

		}

	});

	/**
	 * Enumerated list of valid Document types
	 * 
	 * @static
	 */
	document.Types = {
		SCHOOL : "SCHOOL",
		EMPOWER : "EMPOWER",
		EMPOWER_BANNER : "EMPOWER_BANNER"
	};

	return document;

})();
