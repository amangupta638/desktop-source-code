/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: NoteEntry.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

/** @namespace */
var diary = diary || {};
diary.model = diary.model || {};

(function() {

	diary.model.NoteEntry = Backbone.Model.extend(
		/** @lends diary.model.NoteEntry.prototype */
		{
			/**
			 * Sets the model attribute defaults.
			 *
			 * @returns {Object} The model attribute defaults
			 */
			defaults : function() {
				var attrs = {
					"timestamp" : new Date(),
					"text" : "(no entry)"
				};

				return attrs;
			},

			/**
			 * Validates the model attributes.
			 *
			 * @param {!Object} attrs The model attributes to be set
			 *
			 * @returns {String} Validation error, if any
			 */
			validate : function(attrs) {
				// 'timestamp' is required
				if (!isSpecified(attrs.timestamp)) {
					return "Timestamp must be specified and cannot be NULL";
				}

				// 'timestamp' must be a timestamp
				if (!(attrs.timestamp instanceof Date)) {
					return "Timestamp must be a valid timestamp";
				}
				// 'text' is required
				if (!isSpecified(attrs.text)) {
					return "Text must be specified and cannot be NULL";
				}

				// 'text' must be a string
				if (typeof attrs.text != 'string') {
					return "Text must be a valid string";
				}

				// 'text' cannot be empty
				if (attrs.text == "") {
					return "Text must be specified and cannot be empty";
				}
			}
		}
	);

})();
