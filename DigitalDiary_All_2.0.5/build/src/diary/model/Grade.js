/**
 * The model that represents a STUDENT details
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.Grade = (function() {

	var grade = Backbone.Model.extend({

		// init the student with the default values
		defaults : function() {
			// define the attributes 
			var attrs = { 
							  'uniqueId' : null,
							  'name' : null
						  };	
			
			// return the attributes of the grades
			return attrs;
		},
	
	
		get : function (attribute) {
			
			value = Backbone.Model.prototype.get.call(this, attribute);
			
			// return the attribute of the value
			return value;
		}
		
	});
	
	return grade;
	
})();

