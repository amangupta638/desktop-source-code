/**
 * The model that represents a content package.
 *
 * author - Jane Sivieng
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.ContentPackage = (function() {

    /**
     * The constructor for the Content Package.
     *
     * @constructor
     */
    var contentPackage = Backbone.Model.extend({

        // init the default properties for the content item
        defaults : function() {

            // return all of the properties for the model
            return {
                'package' : null,			// The package id
                'dateUpdated' : null,		// The date last updated according to the manifest file
                'dateLastUpdated' : null	// The date the package was downloaded and stored on the local computer
            };
        },

        /*
         * the initialization function of the model
         * this is executed on the construction of the model class
         */
        initialize : function() {

        }

    });


    // return the created content package
    return contentPackage;

})();