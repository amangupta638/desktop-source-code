/**
 * The model that represents a TIMETABLE in a diary.
 * A Timetable is a per-student schedule in the form of a table. 
 * The columns represent the days of a Cycle and the rows represent Periods. 
 * Each cell in the table represents a possible location in the schedule for a Class.
 *
 * A Timetable can change over the course of the year as a student changes subjects. 
 * When a Timetable is updated we normally do not want to change the Timetable for dates already past, 
 * unless there was a mistake in writing down the previous Timetable. 
 * To support this, a diary supports a series of Timetables, each with an starting date on which it 
 * replaces the previous Timetable in the series. 
 * For any date there is either zero or one Current Timetable.
 * 
 * author - John Pearce
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.Timetable = (function() {
	
	/**
	 * The constructor for the Timetable.
	 * @constructor
	 */
	var timetable = Backbone.Model.extend({
		
		// init the timetable with the default values
		// just in case no parameters are passed upon creation
		defaults : function() {
			// return the default attributes
			return {
				// {?string} The human-readable name for the timetable, e.g. "Semester 1"
				'name' : null,
				// {!Rocketboots.date.Day} Day from which the Timetable takes effect, overriding the previous Timetable if there was one
				'startDay' : null,
				'endDay' : null,
				// {!diary.collection.PeriodCollection} A sequence of Periods attached to the Timetable
				'periods' : new diary.collection.PeriodCollection(),
				// {!diary.collection.ClassCollection} A list of Classes that appear on the Timetable
				'classes' : new diary.collection.ClassCollection(),
				// {!number} The number of days after which the Cycle repeats, corresponding to the number of columns in the Timetable
				'cycleLength' : 0,
				// {?String} The label to display describing the cycle number. e.g. in "Semester 1 >> Week 2 >> Monday", the cycle label is "Week"
				'cycleLabel' : null,
				/**
				 * {?Array} A description for each cycle day, including 'label' and 'shortLabel'.
				 * The short label is used in Month and Glance views.
				 * The full label is used in Day and Week views.
				 * e.g. in "Semester 1 >> Week 2 >> Monday", the cycle day label is "Monday".
				 */
				'cycleDays' : null
			};
		},
		
		/**
		 * Find the subject associated with a cycle day and period combination
		 * @param {!number} cycleDay The cycle day in which the subject occurs
		 * @param {!diary.model.Period} period The period in which the subject occurs
		 * @returns {?diary.model.Subject} The matches subject, or null if no subject matches.
		 */
		subjectAt: function (cycleDay, period) {
			
			var classObj = this.classAt(cycleDay, period);
			
			if (classObj) {
				return classObj.get('subject');
			}
			
			return null;
			
		},
		
		/**
		 * Find the class associated with a cycle day and period combination
		 * @param {!number} cycleDay The cycle day in which the class occurs
		 * @param {!diary.model.Period} period The period in which the class occurs
		 * @returns {?diary.model.Class} The matching class, or null if no class matches.
		 */
		classAt: function (cycleDay, period) {
			
			var classes = [];
			
			// Check params for validity
			if (cycleDay <= 0 || cycleDay > this.get('cycleLength')) {
				throw "Cycle day must be positive and within cycle length (" + cycleDay + "," + this.get('cycleLength') + ")";
			}
			
			if (isSpecified(period.get('cycleDay')) && period.get('cycleDay') != cycleDay) {
				// Period must occur within the cycle day
				return null;
			}
			
			// Find class
			classes = _.filter(this.get('classes').toArray(), function (classObject) {
				return (classObject.get('cycleDay') == cycleDay || classObject.get('cycleDay') == 0) && classObject.get('periodId') == period.get('id');
			});
			
			if (classes.length > 0) {
				return classes[0];
			}
			
			return null;
			
		},

        getClass: function(classId) {
            return _.find(this.get('classes').toArray(), function(classObj) {
                return classObj.get('id') == classId;
            });
        },

        getPeriod: function(periodId) {
            return _.find(this.get('periods').toArray(), function(period) {
                return period.get('id') == periodId;
            });
        },

		getFirstPeriodWithSubjectOnCycleDay: function(subjectId, cycleDay) {
			var timetable = this;
			var periods = this.periodsForCycleDay(cycleDay);
			var period = _.find(periods.toArray(), function(period) {
				var classObject = timetable.classAt(cycleDay, period);
				return (classObject != null && classObject.get('subject').get('id') == subjectId);
			}) || null;

			return period;
		},


        hasClassesOfSubject: function(subject) {
            var timetable = this;
            var classes = _.filter(timetable.get('classes').toArray(), function(classObj) {
                return classObj.get('subject').get('id') == subject.get('id');
            });

            return classes.length;
        },

		/**
		 * Find the classes for a particular day in the timetables cycle
		 * @param {!number} cycleDay The cycle day in which the class occurs
		 * @returns {diary.collection.ClassCollection} The classes for a particular day, in period order
		 */
		classesForCycleDay: function (cycleDay) {
			
			var matchedClasses = new diary.collection.ClassCollection();
			
			// Check params for validity
			if (cycleDay <= 0 || cycleDay > this.get('cycleLength')) {
				throw "Cycle day must be positive and within cycle length";
			}
						
			matchedClasses.add(_.filter(this.get('classes').toArray(), function (classObject) {
				return classObject.get('cycleDay') == cycleDay;
			}));
			
			return matchedClasses;
			
		},
		
		
		/**
		 * Get the list of periods for a particular day in the timetables cycle.
		 * And derives the endTime for any period, if required
		 * 
		 * @public
		 * @param {number} cycleDay The day of the cycle in which the resulting periods should occur
		 * @return {diary.collection.PeriodCollection} The periods for a particular day, in start-time order
		 */
		periodsForCycleDay: function (cycleDay) {
			
			var matchedPeriods = new diary.collection.PeriodCollection();
			
			matchedPeriods.add(_.filter(this.get('periods').toArray(), function (period) {
				return period.get('cycleDay') == cycleDay || period.get('cycleDay') == 0;
			}));

            if (matchedPeriods.size() == 0) {
                // get default set instead
                matchedPeriods.add(_.filter(this.get('periods').toArray(), function (period) {
                    return !isSpecified(period.get('cycleDay'));
                }));
            }
			
			_.forEach(matchedPeriods, function(period, periodIndex) {
				var endTime = period.get('endTime');
				if (!endTime) {
					console.log('period endtime is null ' + period.get('title') + ' ' + endTime);
					
					if (periodIndex < matchedPeriods.length) {
						endTime = matchedPeriods[periodIndex + 1].get('startTime');
					} else {
						// default to just before midnight
						endTime = new Rocketboots.date.Time(23, 59);
						console.log("WARNING: period " + period.id + " has no end time.");
					}
					period.set('endTime', endTime);
				}				
			});

			return matchedPeriods;
		},

		/**
		 * Get the cycle day label for a given cycle day.
		 * @param cycleDay The day of cycle, beginning at 1
		 * @return {String} The cycle day label.
		 */
		getCycleDayLabel: function (cycleDay) {
			return this.get('cycleDays') && this.get('cycleDays').length > 0 && cycleDay > 0 ? this.get('cycleDays')[cycleDay-1].shortLabel : null;
		},

		/**
		 * Get the cycle day short label for a given cycle day.
		 * @param cycleDay The day of the cycle, beginning at 1
		 * @return {String} The cycle day short label
		 */
		getCycleDayShortLabel: function (cycleDay) {
			return this.get('cycleDays') && this.get('cycleDays').length > 0 && cycleDay > 0 ? this.get('cycleDays')[cycleDay-1].shortLabel : null;
		},
		
		/*
		 * the validation function for the Timetable
		 * 
		 * @param {object} attrs The attributes that are set for the Timetable
		 *
		 * @return {string} The error, if any, that is generated by the validation method
		 */ 
		validate : function(attrs) {
			
			// start date
			if (!isSpecified(attrs.startDay)) {
				return "No start day specified";
			}
			
			if (!(attrs.startDay instanceof Rocketboots.date.Day)) {
				return "Start day must be a valid Day";
			}
			
			// periods
			
			if (!isSpecified(attrs.periods)) {
				return "Periods must be specified";
			}
			
			if (isSpecified(attrs.periods) && !(attrs.periods instanceof diary.collection.PeriodCollection)) {
				return "Periods must be a collection of periods";
			}
			
			// classes
			
			if (!isSpecified(attrs.classes)) {
				return "Classes must be specified";
			}
			
			if (isSpecified(attrs.classes) && !(attrs.classes instanceof diary.collection.ClassCollection)) {
				return "Classes must be a collection of class objects";
			}
			
			// cycle length
			if (!isSpecified(attrs.cycleLength)) {
				return "No cycle length specified";
			}
			
			if (attrs.cycleLength <= 0) {
				return "Cycle length must be positive";
			}

			// labels
			if (isSpecified(attrs.cycleLabel) && typeof attrs.cycleLabel !== "string") {
				return "Cycle label must be a string";
			}
			if (isSpecified(attrs.cycleDays)) {
				if (!_.isArray(attrs.cycleDays)) {
					return "Cycle days must be an array";
				}
				if (attrs.cycleDays.length != attrs.cycleLength) {
					return "There must be the same number of cycle days as the cycle length";
				}

				var validationError = null;

				_.each(attrs.cycleDays, function (cycleDay, cycleDayIndex) {
					if (validationError) return;

					if (!cycleDay.label || cycleDay.label.length == 0 || typeof cycleDay.label !== "string") {
						validationError = "Cycle day label for day " + (1 + cycleDayIndex) + " must be a non-empty string";
					}
					if (!cycleDay.shortLabel || cycleDay.shortLabel.length == 0 || typeof cycleDay.shortLabel !== "string") {
						validationError = "Cycle day short label for day " + (1 + cycleDayIndex) + " must be a non-empty string";
					}
				});

				if (validationError) {
					return validationError;
				}
			}
			
			/* Business logic */
			
			// For each cycle day...
			for (var cycleDay = 1; cycleDay <= attrs.cycleLength; ++cycleDay) {
				
				var result = null,
					setResult = function (newResult) {
						if (result == null) {
							result = newResult;
						}
					},
					periods = [],
					lastPeriodStartTime = null,
					lastPeriodEndTime = null;
				
				// Get the list of periods relevant for the current cycle day
				periods = this.periodsForCycleDay(cycleDay).toArray();

				// Last period must have an end time 
				if (periods.length > 0) {
					if (!isSpecified(periods[periods.length-1].get('endTime'))) {
						setResult("Last period in a given day must have an end time");
					}
				}
				
				_.forEach(periods, function (period) {
					
					// Check the validity of this period
					if (lastPeriodStartTime != null && period.get('startTime').toInteger() <= lastPeriodStartTime.toInteger()) {
						setResult("Periods must be in ascending start time order, without duplicate start times");
					}
					if (lastPeriodEndTime != null && lastPeriodEndTime.toInteger() > period.get('startTime').toInteger()) {
						setResult("Periods must not overlap");
					}
					
					// Check the validity of the classes
					{
						// Get the classes for the cycle day and period
						var classes = _.filter(attrs.classes.toArray(), function (classObject) {
							return classObject.get('periodId') == period.id && classObject.get('cycleDay') == cycleDay;
						});
						
						if (classes.length > 1) {
							setResult("No cycle day and period combination may contain more than 1 subject");
						}
					}
					
					// Continue
					lastPeriodStartTime = period.get('startTime');
					lastPeriodEndTime = period.get('endTime');
					
				});
				
				if (result) {
					return result;
				}
			}
			
		},

		/**
		 * Defined for debugging purposes.
		 * @return {String} A string representation of the object.
		 */
		toString: function () {
			//alert("alert : toString timetable");
			return "Timetable " + (this.get('name') ? this.get('name') : "unnamed") + " from " + this.get('startDay').getKey();
		}
		
	});
	
	return timetable;
})();