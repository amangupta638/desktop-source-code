/**
 * The model that represents a section of results related to a particular search that was performed by the user
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.SearchResultSection = (function() {
	
	/**
	 * The constructor
	 * @constructor
	 */
	var searchResultSection = Backbone.Model.extend({
		
		defaults : function() {
			// return the default attributes
			return { 'heading' : null,				// {!string} The heading for the result section
					 'itemType' : null,				// {!diary.model.SearchResultSection.ITEMTYPE} The specific type of the result items
					 'resultsType' : null,			// {!diary.model.SearchResultSection.RESULTSTYPE} The type of the results
				 	 'sectionResults' : null };		// {!Array} The results related to this particular section
		},
		
		initialize : function() {
			
			// if the results was not set we will create a default empty array
			if (!isSpecified(this.get("sectionResults"))) {
				this.set("sectionResults", new Array());
			}
			
		}
		
	});	

	/**
	 * The different types of results that can be returned
	 */
	searchResultSection.RESULTSTYPE = {
		DIARYITEM : 'diaryItem',
		CONTENTITEM : 'contentItem',
		SUBJECTS : 'subject',
		CLASSES : 'classes'
	};
	
	/**
	 * The different item types that can be returned in the result
	 */
	searchResultSection.ITEMTYPE = {
		TASK : 'task',
		EVENT : 'event',
		MESSAGE : 'message',
		NOTE : 'note',
		SCHOOL : 'school',
		EMPOWER : 'empower',
		BANNER : 'banner',
		SUBJECT : 'subject',
		CLASS : 'class'
	};
	
	return searchResultSection;
})();