/**
 * The model that represents a STUDENT details
 */
diary = diary || {};
diary.model = diary.model || {};

diary.model.State = (function() {

	var state = Backbone.Model.extend({

		// init the state with the default values
		defaults : function() {
				 
			// define the attributes 
			var attrs = { 
						  'uniqueId' : null,
						  'countryId' : null,
						  'name'	: null
						 };	
			
			// return the attributes of the state
			return attrs;
		},
		
		get : function (attribute) {
			value = Backbone.Model.prototype.get.call(this, attribute);
			// return the attribute of the value
			return value;
		}
		
	});
	
	return state;
	
})();

