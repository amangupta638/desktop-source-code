diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};
var clicksave=false;   var checkedInput=false;

diary.view.DiaryDayView = (function() {

	var debug = false;
	
	var notePanelPadding = 8,
		noteItemHeight = 38;
	
	var view = Rocketboots.view.View.extend({
	
		// keeps track of the current note that is being displayed
		// NOTE - if the value is NULL it means that no note is current being shown
		currentDisplayedNoteId: null,
		
		events: {
			"click .dayView .addDiaryItemButton"						: "showDiaryItemForm", 		// pop-up for add new diary item
			"dblclick .dayView .diaryItem"								: "showDiaryItemForm",		// pop-up the edit for a diary item
			"click .dayView .eventContainer > div"						: "showDiaryItemForm",		// pop-up the edit for a diary item
			"click .sidebar .tasks .link"								: "showDiaryItemForm",		// pop-up the edit for a diary item
			"click .sidebar > .tasks"									: "panelTabClicked",		// toggle the tasks panel
			"click .sidebar > .notes"									: "panelTabClicked",		// toggle the notes panel
			"click .sidebar .contents .topTab"							: "sidebarInternalTabSelected",	// toggle the tasks due view
			"click .sidebar .contents .tabContent .header"				: "sidebarToggleTaskList",	// toggle the expand/collapse for a task list
			"click .dayView .diaryItem"					    			: "showTasksInPopup",		// pop-up for tasks
			"click .studCalendarAccess"									: "accessStudCalendar",		// access Student Calendar
			"click .dayView .calendarNavigation > div"					: "changeDay",				// previous/next day
			"click .sidebar .contents .note .link"						: "renderNote",				// show the note details
			"click .sidebar .contents .noteDetail .noteBack"			: "renderNotes",			// show the notes list
			"click .sidebar .contents .noteDetail .save"				: "appendNoteEntry",		// append a note entry
			"click .sidebar .contents .noteDetail .delete"              : "DeleteNoteEntry",         // append a note entry
			"change .taskListItem .title input"              			: "savCompletion",
			"click .hideCalendarNavigation"								: "selectDay",
			"click #toppanel a.open"									: "pullUpSlider",
			"click #toppanel a.close"									: "pushDownSlider"
			
		},
		
		initialize: function() {
		GetStartDate();	
			//console.log("INITIALIZE DAY VIEW");
			Rocketboots.view.View.prototype.initialize.call(this);

			var view = this, 
				updateDay = function () {
					var selectedDay = app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay');
					//console.log("initialize selectedDay= "+selectedDay);
					//console.log("view.currentDay="+view.currentDay);
					view.currentDay = view.currentDay || null;
					if (view.currentDay == null || view.currentDay.getKey() != selectedDay.getKey()) {
						//console.log("in if view.currentDay == null || view.currentDay.getKey() != selectedDay.getKey() ");
						view.currentDay = selectedDay;
						view.mapDay();
						view.isDirty = true;
						//console.log("ashish");
					}
					//console.log("ashish1"+view.currentDay);
				}, 
				updateCalendar = function() {
					console.log("updateCalendar : render diaryDay");
					
					var name = $(".headerNavigation").find(".selected").attr('data-link'); 
					if(name == 'home'){
						console.log("Calling home view");
						$(".headerNavigation").find(".selected").click();
					} else {
						view.mapDay();
						view.isDirty = true;
						view.render();
					}
				},
				updateTimetable = function() {
					console.log("updateTimetable : render diaryDay");
					view.mapDay();
					view.isDirty = true;
					//view.render();
				},
				noteEntryAppended = function(noteId) {
					// if we have a note which is currently showing we will refresh the notes screen too
					if (this.currentDisplayedNoteId != null && this.currentDisplayedNoteId === noteId) {
						this.showNoteDetails(noteId);
					}
				},
				refreshCalendar = function() {
					app.context.trigger(EventName.REFRESHCALENDAR);
				};	
						
			//debug && console.log('DiaryDayView.initialize()');
			
			this._appState = app.model.get(ModelName.APPLICATIONSTATE);
			
			//app.model.on("change:" + ModelName.CALENDAR, updateCalendar, this);
			//app.model.get(ModelName.APPLICATIONSTATE).on("change:" + 'selectedDay', updateDay, this);
			app.listenTo(app.model, "change:" + ModelName.CALENDAR, updateCalendar);
			app.listenTo(app.model.get(ModelName.APPLICATIONSTATE), "change:" + 'selectedDay', updateDay);
			
			// listen to the event so that we would know when to refresh the list of diary items that are being displayed
			// NOTE - The Diary View (the base class of all calendar views) is listening to the three different events that
			// are triggered by the diary item collection
			console.log("set app.context.on(EventName.REFRESHDIARYITEMS)");
			app.context.off(EventName.REFRESHDIARYITEMS);
			app.context.on(EventName.REFRESHDIARYITEMS, updateCalendar, this);
			// listen to the event of a note entry appended so that we can re-render the note screen
			app.context.on(EventName.NOTEENTRYAPPENDED, noteEntryAppended, this);
			
			//app.model.get(ModelName.DIARYITEMS).on("reset", updateCalendar, this);
			//app.model.get(ModelName.CALENDAR).get('timetables').on("reset", updateCalendar, this);
			app.listenTo(app.model.get(ModelName.DIARYITEMS), "reset", updateCalendar);
			app.listenTo(app.model.get(ModelName.CALENDAR).get('timetables'), "reset", updateTimetable);

			// listen to the resize event
			//app.model.get(ModelName.APPLICATIONSTATE).on("change:windowHeight change:windowWidth", view.onResize, this);
			app.listenTo(app.model.get(ModelName.APPLICATIONSTATE), "change:windowHeight change:windowWidth", view.onResize);
						
			view._scrollManager = new diary.service.ScrollManager();
			view.isAlwaysDirty = true;
			view.day = {};
			updateDay();
			
			// render the screen
			view.render();
		},

		parseAdditionalParams: function(options) {
			if (typeof(options.el) !== "undefined") {
				this.el = options.el;
			}
		},
		
		updateTimetableForDay: function() {
			var view = this;
			view.mapPeriodsInDay();
			view.isDirty = true;
		},
		
		hideCalendarNavigation : function() {
			
			if($(".daySelection").hasClass('hidden')){
				//alert('if');
				$(".daySelection", this.el).hide();
				$(".eventsOnDay", this.el).show();
				$(".cal_tbw_right .cal_filter_task").hide();
				$(".daySelection").removeClass('hidden');
			} else {
				//alert('else');
				$(".daySelection", this.el).show();
				//$(".cal_tbw_right .cal_filter_task").show();
				$(".eventsOnDay", this.el).hide();
				$(".daySelection").addClass('hidden');
			}
			
		},
		
		mapDay: function () {
			var view = this;
			
//			debug && console.log("mapping calendar day");
			
			// get the calendar from the application model
			var calendarModel = app.model.get(ModelName.CALENDAR);
			
			if (view.calendarDay && view.calendarDay.get('timetable')) {
				view.calendarDay.get('timetable').get('classes').off('add', view.updateTimetableForDay, view);
				view.calendarDay.get('timetable').get('classes').off('change', view.updateTimetableForDay, view);
				view.calendarDay.get('timetable').get('classes').off('remove', view.updateTimetableForDay, view);
			} else {
				//debug && console.log("this.calendarDay is undefined");
			}
			//alert("view.currentDay.getMonth()="+view.currentDay.getMonth());
			var monthname=monthArray[view.currentDay.getMonth()];
			//alert(monthname);
			var index=$.inArray(monthname,monthNames);
			//alert("index="+index);
			view.calendarDay = calendarModel.getDay(view.currentDay.getMonth() + 1, view.currentDay.getDayInMonth());
			
			if (view.calendarDay && view.calendarDay.get('timetable')) {
				view.calendarDay.get('timetable').get('classes').on('add', view.updateTimetableForDay, view);
				view.calendarDay.get('timetable').get('classes').on('change', view.updateTimetableForDay, view);
				view.calendarDay.get('timetable').get('classes').on('remove', view.updateTimetableForDay, view);
			}
			
			view.mapPeriodsInDay();
			
		},
		
		mapPeriodsInDay: function() {
			// get all the diary items for the calendar
			var diaryItemsModel = app.model.get(ModelName.DIARYITEMS).getDiaryItems();

			//debug && console.log("mapping periods in calendar day");
			
			// update the generated day
			this.day = diary.view.DiaryCalendarViewUtil.mapPeriodsInDay(diaryItemsModel, 
					                                                    this.calendarDay);

		},
		
		render: function() {
			var updateDayCalendar = function() {
				console.log("updateDayCalendar : render diaryDay");
				
				var name = $(".headerNavigation").find(".selected").attr('data-link'); 
				if(name == 'home'){
					console.log("Calling home view");
					$(".headerNavigation").find(".selected").click();
				} else {
					view.mapDay();
					view.isDirty = true;
					view.render();
				}
			};
			app.model.get(ModelName.DIARYITEMS).off("change");
			app.model.get(ModelName.DIARYITEMS).off("remove");
			app.model.get(ModelName.DIARYITEMS).off("add");
			app.model.get(ModelName.DIARYITEMS).on("change", updateDayCalendar, this);
			app.model.get(ModelName.DIARYITEMS).on("remove", updateDayCalendar, this);
			app.model.get(ModelName.DIARYITEMS).on("add", updateDayCalendar, this);
			
			this.mapPeriodsInDay();
			var view = this,
				today = new Rocketboots.date.Day(),
				isYear = today.getFullYear() == SystemSettings.HARDCODDEDYEAR,
 				isToday = view.currentDay.getKey() == today.getKey();
           // console.log("view.currentDay render="+view.currentDay);
           var dayOfNotes = view.currentDay;
           //  console.log("view.day=");
           //  console.log(view.day);
           
           	var calendarDay = view.calendarDay;
           	var cycleInfo;
           	if (calendarDay.get('cycle') != null) {
           		cycleInfo = (calendarDay.get('cycleLabel') ? calendarDay.get('cycleLabel').toUpperCase() : 'CYCLE') + ' ' + calendarDay.get('cycle') + ', '+calendarDay.get('cycleDayShortLabel');
			}
			
			var viewParams = {
				'day'					: view.day,
				'dateNow'				: getCurrentDate(),
				'dateLabel'				: view.currentDay.getDayInMonth(),
				'isTeacher'				: app.model.get(ModelName.STUDENTPROFILE).get("isTeacher"),
				'cycleInfo'				: cycleInfo,
				'dayLabel'				: diary.view.DiaryCalendarViewUtil.daysOfWeek[view.currentDay.getDate().getDay()],
				'dateString'			: diary.view.DiaryCalendarViewUtil.formatDateString(view.currentDay),
				'disableToday'			: isToday || !isYear,
				'sidebarMinimised'	 	: !app.model.get(ModelName.APPLICATIONSTATE).get('dayViewPanelVisible'),
				'tasksTabSelected'		: app.model.get(ModelName.APPLICATIONSTATE).get('dayViewActivePanel') == 'tasks',
				'notesTabSelected'		: app.model.get(ModelName.APPLICATIONSTATE).get('dayViewActivePanel') == 'notes',
				'tasksDueTabSelected'	: app.model.get(ModelName.APPLICATIONSTATE).get('dayViewTasksPanelView') != 'assigned',
				'tasksAssignedTabSelected': app.model.get(ModelName.APPLICATIONSTATE).get('dayViewTasksPanelView') == 'assigned'
			};

			var content = app.render('diaryDay', viewParams);
			$(this.el).html(content);
			//$('.dayView', this.el).after(app.render('sidebar', viewParams));

			// render the notes too
			this.renderNotes();
			// and the tasks
			this.renderTasks();

            // if the notes tab is selected and the side bar is not minimised we
            // will mark the notes tab as selected
            if (!viewParams.sidebarMinimised && viewParams.notesTabSelected) {

                // switch to the notes panel but do not log the switch as
                // this is not being invoked by the user
                this.switchToPanel($(".sidebar > .notes", this.el), false);

            }
			
			//alert('view.calendarDay just before' + dayOfNotes);
			this.renderEventsOnDay(dayOfNotes);
			this.renderBanner();		// Render pullup bar
			
			// render the form
			//this.renderDiaryItemForm();

			_.defer(_.bind(function() {
				view.initDatePicker();
				view._scrollManager.setScrollContainer($(".periodContainer ul", view.$el));

				if (view._scrollManager.maintainPosition) {
					view._scrollManager.resetScrollPosition();
				}
				//view._scrollManager.maintainPosition = false;

				// size correctly initially
				view.onResize();
				
				
				// setup drag-and-drop
				view.dragDropHelper = new diary.view.DragDropHelper({
					container: $(".calendarContainer", view.el),
					scrollManager: view._scrollManager,
					dragGroups: [
						// diary items
						{
							draggables: ".diaryItem",
							dropTargets: ".periodContainer li, .allDayContainer",
							onDropOverTarget: _.bind(view.dropDiaryItem, view)
						},
                        // pop-ups
                        {
                            draggables: "#dayPopup .summaryWidgetItem",
                            dropTargets: ".periodContainer li, .allDayContainer",
                            onDropOverTarget: _.bind(view.dropDiaryItem, view)
                        },
						// files
						{
							canDropFiles: true,
							dropTargets: ".diaryItem[draggable], .periodContainer li, .allDayContainer",
							onDropOverTarget: _.bind(view.dropFile, view)
						}
					]
				});
                view.dragDropHelper.assignEventHandlers();

			}, view));
			
		//performance fix
		app.model.destroy();
			
		},
		
		mapEvents : function (event) {
			return {
				'title' 		: event.get('title'),
				'description' 	: event.get('description'),
			};
		},
		
		renderEventsOnDay : function (calendarDay) {
			var linkName = app.model.get(ModelName.APPLICATIONSTATE).get('hideFilters');
			if (linkName) {
				$(".cal_tbw_right .cal_filter_task").hide();
			}
			
			var today = new Rocketboots.date.Day();
			var eventsOnDay = app.model.get(ModelName.DIARYITEMS).getEventsOnTheDay(today);
			//var eventOnDayLabel = 'On this day';
			var calendarDayKey = today.getKey();
			//if(today.getKey() == calendarDay.getKey()){
			var eventOnDayLabel = null; 
			
			var schoolFormat = app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat');
		    if (schoolFormat == "USA"){
		    	eventOnDayLabel = diary.model.CalendarDay.WEEKDAY[today.getDay()]+', '+monthNames[parseInt(calendarDayKey.substring(4,6))-1]+' '+calendarDayKey.substring(6,8)+', '+calendarDayKey.substring(0,4)+'';
			} else {
				eventOnDayLabel = diary.model.CalendarDay.WEEKDAY[today.getDay()]+', '+calendarDayKey.substring(6,8)+' '+monthNames[parseInt(calendarDayKey.substring(4,6))-1]+' '+calendarDayKey.substring(0,4)+'';
			}
		    
			//}
			
			var allEventsOnDay = [];
			for(var i = 0 ; i < eventsOnDay.length; i++){
				var event = eventsOnDay.at(i);
				allEventsOnDay.push(this.mapEvents(event));
			}
			var viewParams = {
				'allEventsOnDay' : allEventsOnDay,
				'totalEvents' : eventsOnDay.length,
				'eventOnDayLabel' : eventOnDayLabel
			};
			
			$('.eventsOnDay', this.el).html(app.render('eventOnDay', viewParams));
		},

		onResize: function () {
			//debug && console.log("DiaryDayView.onResize");

			var view = this;

			$(".periodContainer ul").css("top", $(".allDayContainer", view.$el).outerHeight() + "px");
		},
		
		renderDiaryItemForm: function() {
			var view = this, 
			viewParams = {};		
			viewParams.el = $('#dayDiaryItemFormEditPopup', this.el);
			viewParams.editorref="editor1";
			viewParams.classes="dayDiaryItemFormEditPopup";
			view.diaryItemForm = new diary.view.DiaryItemView(viewParams);
			
		},
		
		
		renderTasks : function() {
			//alert('renderTasks called');
			var diaryItems = app.model.get(ModelName.DIARYITEMS).getDiaryItems();
           // console.log("rendertask===");
            var task=diary.view.DiaryCalendarViewUtil.mapTasksForTasksAssignedPanel(diaryItems);
            //console.log(diary.view.DiaryCalendarViewUtil.mapTasksForTasksAssignedPanel(diaryItems));
            //console.log("task.taskGroups");
           // console.log(task.taskGroups);
            var taskGroups=task.taskGroups;
           for(var i=0;i<taskGroups.length;i++)
            {
                var tasks=taskGroups[i].tasks;
                if(tasks.length)
                {
                   // console.log("tasks.length="+tasks.length);
                    for(var j=0;j<tasks.length;j++)
                    {
                        //console.log("completed="+tasks[j].completed);
                        var completedTask=tasks[j].completed;
                        if(completedTask)
                        {
                            tasks[j]["class"]="completed";
                            //console.log(tasks[j]);
                        }
                        else
                        {
                            tasks[j]["class"]="notcompleted";
                            //console.log(tasks[j]);
                        }
                    }
                }
                
            }
            //console.log(task);
            
			// replace the tasks panel with the retrieved task details
			$(".sidebar .contents .tabContent.due", this.el).html(
					app.render('sidebarTaskList', diary.view.DiaryCalendarViewUtil.mapTasksForTasksDuePanel(diaryItems))
			);
			//console.log(task)
			$(".sidebar .contents .tabContent.assigned", this.el).html(
					app.render('sidebarTaskList', diary.view.DiaryCalendarViewUtil.mapTasksForTasksAssignedPanel(diaryItems))
			);
			
			
			var taskitesms=$(".taskListItem");
			//alert(taskitesms.length)
			taskitesms.each(function(){
				var checkers=$(this).find(".dot").attr("class");
				
				if(checkers.indexOf("completed")>-1)
				{$(this).find("input").attr("checked","true")
					
				}else{
					
					$(this).find("input").removeAttr("checked");
					
					}
				
				
				
				})

		},
		
		renderBanner: function(forceRefresh) {
			getEmpowerDetails();
			this.schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);
			var empowerContentEnabled = this.schoolProfile.get("includeEmpowerContent");
			
			var empowerType = this.schoolProfile.get("empowerType");
			
			forceRefresh = forceRefresh || false;
			
			var articleContent;
			//console.log("DiaryView.renderBanner()", empowerContentEnabled);
			
			var article = null;
			var packageId = null;
			var juniorOrSenior = null;
			
			
			if (empowerType && empowerContentEnabled == ContentSetting.ENABLEEMPOWER)
			{
				if (empowerType.toUpperCase() == ContentSetting.EMPOWERSENIOR.toUpperCase())
				{
					packageId = ContentSetting.EMPOWERSRPULLBARPACKAGEID;
				}
				else if (empowerType.toUpperCase() == ContentSetting.EMPOWERJUNIOR.toUpperCase())
				{
					packageId = ContentSetting.EMPOWERJRPULLBARPACKAGEID;
				} else if(empowerType.toUpperCase() == ContentSetting.EMPOWERELEM.toUpperCase())
				{
					packageId = ContentSetting.EMPOWERELEMPULLBARPACKAGEID;
				}
			}
			else if (empowerContentEnabled == ContentSetting.ENABLECATHOLIC)
			{
				packageId = ContentSetting.CATHOLICPULLBARPACKAGEID;
			}
			
			//alert('DiaryDayView.js :: packageId =  ' + packageId);
			
			article = this.getEmpowerBannerArticle(app.model.get(ModelName.APPLICATIONSTATE).get("selectedDay"), packageId);
			//alert('renderBanner :: Article = ' + JSON.stringify(article))
			
			// render the empower content if the school is
			// subscribed to it
			if (empowerContentEnabled == 2) {
				$('#toppanel', this.el).css("display","none");
				
			} else {
				
				// get the article that we are going to render
				// NOTE - It is being assumed that the date from and to of the article are going to be
				// specified for the article banners

				/* Comment starts ----
				
				var dates =app.model.get(ModelName.APPLICATIONSTATE).get("selectedDay");
	
				var banners=app.model.get(ModelName.RENDERCONTENTMODEL);
				if(banners && banners!= "undefined" && banners.length>0 )
				{
					//alert('came here banners 1 : ' + banners.length );
					banners.each(function(banner) {
						//alert('came here banners 2 : ' + banners.length );
						if(banner.get("ContentType") == "Panel")
						{
							//alert('came here inside');
							var startdate=Rocketboots.date.Day.parseDateString(banner.get("StartDate"))
							var enddate=Rocketboots.date.Day.parseDateString(banner.get("EndDate"))
							//alert(startdate)
							if(dates.getTime() >= startdate.getTime() && dates.getTime() <= enddate.getTime())
							{//alert(1111)
								articleContent=banner.get("Content");
								//alert('came here articleContent : '+ articleContent);
							}
						}
					});
				}
				Comment Ends -----
				*/
				$('#toppanel', this.el).css("display","block");
				if (article != null)
				{
					articleContent = readFile(article.get('url'));
				}
				else
				{
					articleContent = 'Content is not available';
				}
				
				imageURL = "/content/" + packageId + "/images/";
				articleContent = articleContent.replace(/images\//g, imageURL);
				diary.view.ArticleViewUtil.renderArticle(articleContent, $('.panelContent', this.el));
				
			}
			
		},
		
		renderNotes : function() {
			
			// holds the list of notes that need to be rendered
			var view = this,
				viewParams = { notes : [] };

			// clear the notes identifier to indicate that no note is being displayed
			view.currentDisplayedNoteId = null;

			// get all the notes
			var notes = new diary.collection.DiaryItemCollection(app.model.get(ModelName.DIARYITEMS).filter(function(item) {
				return item instanceof diary.model.Note;
			}));
			//console.log(notes);

			// sort them by atDay and atTime
			notes.comparator = function(note1, note2) {

				// note if the note is an all day note we will assume the time is at the end of the day so that the all day notes will be
				// sorted before the other notes on the same day
				var sort1 = parseInt(note1.get('atDay').getKey() + ((note1.get('atTime')) ? note1.get('atTime').toString() : "2359"), 10),
					sort2 = parseInt(note2.get('atDay').getKey() + ((note2.get('atTime')) ? note2.get('atTime').toString() : "2359"), 10);

				return ((sort1 < sort2) ? 1 : (sort1 > sort2 ? -1 : 0));

			};
			notes.sort();

			// map them to the model for rendering the template
			notes.forEach(function(note) {
				viewParams.notes.push(diary.view.DiaryCalendarViewUtil.mapDiaryItem(note));
			});

			// replace the notes panel with the retrieved details
			$(".sidebar .contents .notes", view.el).html(
				app.render('sidebarNoteList', viewParams)
			);

			_.defer(
				function() {
					var startOfToday = new Rocketboots.date.Day().getDate().getTime(),
						focusedNoteIndex = -1;
					
					var focusedNote = notes.find(function(note) {
						focusedNoteIndex++;
						return (note.get('atDay').getTime() >= startOfToday);
					});
					
					if (focusedNote != null) {
						// TODO : adjust this selector to be the scrollable element
						var notesPanel = $(".sidebar .contents .notes", view.$el),
							availableHeight = notesPanel.outerHeight(),
							minimumScrollPosition = 0,
							maximumScrollPosition = (notes.size * noteItemHeight) - availableHeight + (notePanelPadding * 2);

						var notesPanelPosition = (focusedNoteIndex * noteItemHeight);
						notesPanelPosition = Math.max(Math.min(notesPanelPosition, maximumScrollPosition), minimumScrollPosition);
						if(notesPanel.get(0))
							notesPanel.get(0).scrollTop = notesPanelPosition;
					}
				}
			);
			
		},
		
		'renderNote' : function(event) {
			
			// holds the identifier of the note that is to be rendered
			var noteId;
			
			// get the note Id from the element that was clicked
			noteId = parseInt($(event.currentTarget).attr('noteId'), 10);
			this.showNoteDetails(noteId);
			
		},
		
		showNoteDetails : function(noteId) {
			
			debug && console.log("showing note " + noteId);
			
			// holds the note that is to be rendered
			var note;
			
			// map the note to an object which is understood by the template
			note = app.model.get(ModelName.DIARYITEMS).get(noteId);
			
			// map the note
			note = diary.view.DiaryCalendarViewUtil.mapDiaryItem(note);
				
			// replace the notes panel with the note details
			$(".sidebar .contents .notes .noteDetail", this.el).remove();
			$(".sidebar .contents .notes .noteList", this.el).hide().after(
				app.render('sidebarNoteDetail', note)
			);

			// keep track of which note is currently on display
			this.currentDisplayedNoteId = noteId;
			
		},
		
		showTasksInPopup: function(event) {
			
			var view = this,
				// get the identifier of the diary item(s) that we need to display
				diaryItemIds = $(event.currentTarget).attr('diaryItemIds') || $(event.currentTarget).attr('diaryItemId');
			
			if (diaryItemIds) {
				view.showDiaryItemPopup(diaryItemIds, $(event.currentTarget));
				event.stopPropagation();
			}
			
		},
		
		showDiaryItemPopup: function(diaryItemIds, $anchor) {
			var view = this, 
				// get the diary items
				diaryItemsModel = app.model.get(ModelName.DIARYITEMS),
                periodRow = $anchor.closest('li'),
                startTime,
                endTime;

            // if the start and end time of the
            if (periodRow.attr('startTime') && periodRow.attr('endTime')) {
                startTime = Rocketboots.date.Time.parseTime(periodRow.attr('startTime'));
                endTime = Rocketboots.date.Time.parseTime(periodRow.attr('endTime'));
            }

            // render the content of the popup
			var popupContent = app.render('diaryItemsSummary', {'items' : diary.view.DiaryCalendarViewUtil.mapTasksForPopup(diaryItemsModel,
                                                                                                                            diaryItemIds,
                                                                                                                            app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
                                                                                                                            startTime,
                                                                                                                            endTime) });
			
			app.popup.hide($("#dayPopup"));			
			$("#dayPopup>div.popupContent").html(popupContent);
			
			app.popup.show( $("#dayPopup"), $anchor, "right", null, null, $(".cal_content", view.el));
			
			$("#dayPopup .diaryItemSummary .infolink", this.el).on("click", function(event) {				
				view.showDiaryItemForm(event);
			});
			
			$(".webLinkDiaryItem", this.el).on("click", function(event) {				
				var link = prepareWeblinkURL($(event.currentTarget).attr('webLinkURL'));
				var urlReq = new air.URLRequest(link); 
				air.navigateToURL(urlReq);
			});
			
			$("#dayPopup .diaryItemSummary .noteEntry .save", this.el).on("click", function(event) {
				// get the note that was entered
				var noteEntryElement = $("#dayPopup .diaryItemSummary .noteEntry .noteEntry"),
					noteId = parseInt(noteEntryElement.attr("noteId"), 10),
					noteText = noteEntryElement.val().trim();
					//setnoteisSyncZero(noteId);
				// append the note
				diary.view.DiaryView.appendNote(noteId, noteText);
				app.popup.hide($("#dayPopup"));
				openMessageBox("Diary item updated successfully.");
				
			});
			$("#dayPopup .diaryItemSummary .noteEntry .delete", this.el).on("click", function(event) {
                // get the note that was entered
                var hel = confirm("Are you sure to delete this note ?");
			
					if(hel)
					{
               			 var noteEntryElement = $("#dayPopup .diaryItemSummary .noteEntry .noteEntry"),
                    	noteId = parseInt(noteEntryElement.attr("noteId"), 10),
                    	noteText = noteEntryElement.val().trim();
                		DeleteNoteEntryMethod(noteId);
               			 // append the note
                		//diary.view.DiaryView.appendNote(noteId, noteText);
               			 app.popup.hide($("#dayPopup"));
                		/*var noteEntryElement = $(".noteDetail #newNoteEntry .noteEntry"),
               			 noteId = parseInt(noteEntryElement.attr("noteId"), 10);*/
                	}
                	else
                		return;
                
                
            });
			
			$("#dayPopup .diaryItemSummary ul.attachments > li.attachment").on("dblclick", view.openDiaryItemAttachment);

            // re-assign the event handlers as the pop-up was rendered
            view.dragDropHelper.assignEventHandlers();

		},
		
		/* 
		 * Init date picker
		 */
		initDatePicker: function() {
			var view = this;
			// init datepicker for 'go to day'
			view.dayPicker = $(".hideCalendarNavigation .dateSelector", view.$el);
			var calendarDates = view._appState.getCalendarDates(new Date());
			//console.log("calendarDates.startDate= "+calendarDates.startDate);
			//console.log("calendarDates.endDate= "+calendarDates.endDate);
			//console.log("calendarDates.currentDate= "+calendarDates.currentDate);
          //  console.log("view.currentDay.getDate()= "+view.currentDay.getDate());
            var cDate=new Rocketboots.date.Day(new Date())
          //  console.log("cDate="+cDate);
			view.dayPicker.datepicker({
				'showOn' : 'focus', 
				'minDate' : calendarDates.startDate,
				'maxDate' : calendarDates.endDate,
				'defaultDate' : cDate,
				'changeMonth' : true,
				'duration' : 0,
				'showOptions': {direction: 'down' },
				'onSelect' : function(dateText, inst) {
					var selectedDate = $(this).datepicker("getDate"),
						jumpToDay = new Rocketboots.date.Day(selectedDate);
					//console.log("shruti");
					//console.log(jumpToDay);
					
					app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "DAY", jumpToDay.getKey());
					
					app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', jumpToDay);
					// call the refresh calendar so that the calendar will be refreshed
					app.context.trigger(EventName.REFRESHCALENDAR);
				}
			});
			view.dayPicker.datepicker("setDate", view.currentDay.getDate());

		},
		
		selectDay: function(event) {
			var view = this;

			//debug && console.log("DiaryDayView.selectDay");

			view.dayPicker.focus();
		},
		
		changeDay: function(event) {
			var $element = $(event.currentTarget);

			//debug && console.log("DiaryDayView.changeDay - " + $element.attr("class"));

			if ( $element.hasClass("prev") ) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_PREVIOUS, "DAY");
				this.previousDay();
			}
			else if ( $element.hasClass("next") ) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_NEXT, "DAY");
				this.nextDay();
			}
			else if ( $element.hasClass("today") && !$element.hasClass('disabled')) {
				app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.CALENDAR_JUMP, "DAY", "TODAY");
				this.todaysDay();
			}
		},
		
		previousDay: function() {
			var view = this,
				newDate = new Rocketboots.date.Day(view.currentDay),
				calendarModel = app.model.get(ModelName.CALENDAR),
				startDate = view._appState.get('startDate');
			
			newDate.addDays(-1); // go back 1 day
			view.needToMaintainCalendarScroll = true;
			
			if (newDate.getTime() >= startDate.getTime()) {
				app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', newDate);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
		},
		
		nextDay: function() {
			var view = this,
				newDate = new Rocketboots.date.Day(view.currentDay),
				calendarModel = app.model.get(ModelName.CALENDAR),
				endDate = view._appState.get('endDate');
			
			newDate.addDays(1); // go forward 1 day
			view.needToMaintainCalendarScroll = true;
	
			if (newDate.getTime() <= endDate.getTime()) {
				app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', newDate);
				// call the refresh calendar so that the calendar will be refreshed
				app.context.trigger(EventName.REFRESHCALENDAR);
			}
		},
		
		todaysDay: function() {
			var today = new Rocketboots.date.Day();
			//console.log("today="+today);	
		    now = new Date();
		    var dateIndex=now.getDate();
            var index=parseInt(now.getMonth())
            var month=monthNames[index];
            var monthIndex=$.inArray(month, monthArray);
			 app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(yearArray[monthIndex], index, dateIndex));			
			//app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', today);
			// call the refresh calendar so that the calendar will be refreshed
			app.context.trigger(EventName.REFRESHCALENDAR);

		},


        /**
         * Switch to the specified panel
         *
         * @param panel The panel that the user is switching to
         * @param logSwitch Determines if the switch should be logged or not
         */
        switchToPanel: function(panel, logSwitch) {
            //console.log("switchToPanel");
            var result,
                panelType;

            // determine which panel the user is switching to
            panelType = panel.hasClass('tasks') ? "TASKS" : "NOTES";
            result = diary.view.DiaryCalendarViewUtil.togglePanel(panel);

            app.model.get(ModelName.APPLICATIONSTATE).set({ 'dayViewPanelVisible' : result.dayViewPanelVisible });
            app.model.get(ModelName.APPLICATIONSTATE).set({ 'dayViewActivePanel' : result.dayViewActivePanel });

            if (result.dayViewPanelVisible && logSwitch) {

                if (panelType == 'TASKS') {
                    var tasksTypeDue = app.model.get(ModelName.APPLICATIONSTATE).get('dayViewTasksPanelView') != 'assigned';

                    app.context.trigger(EventName.ADDUSAGELOG, tasksTypeDue ? diary.model.UsageLogEvent.TYPE.VIEW_TASKS_DUE
                                                                            : diary.model.UsageLogEvent.TYPE.VIEW_TASKS_ASSIGNED);

                } else {

                    app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.VIEW_NOTES);

                }

            }
			
			var taskitesms=$(".taskListItem");
			//alert(taskitesms.length)
			taskitesms.each(function(){
				var checkers=$(this).find(".dot").attr("class");
				
				if(checkers.indexOf("completed")>-1)
				{$(this).find("input").attr("checked","true")
					
				}else{
					
					$(this).find("input").removeAttr("checked");
					
					}
				
				
				
				})	
        },

		/**
		 * Toggling of Notes and Tasks Panel 
		 */
		panelTabClicked: function(event) {
			this.switchToPanel($(event.currentTarget), true);
		},

		sidebarInternalTabSelected: function (event) {
			var target = $(event.currentTarget);

			//debug && console.log("DiaryDayView.sidebarInternalTabSelected - " + target.attr("class"));

			if (target.hasClass("selected")) return;

			// De-select all tabs
			target.parent().find(".topTab").removeClass("selected");
			// Re-select clicked tab
			target.addClass("selected");

			app.context.trigger(EventName.ADDUSAGELOG,
				target.hasClass("due") ? diary.model.UsageLogEvent.TYPE.VIEW_TASKS_DUE : diary.model.UsageLogEvent.TYPE.VIEW_TASKS_ASSIGNED);

			app.model.get(ModelName.APPLICATIONSTATE).set({ 'dayViewTasksPanelView' : target.hasClass("due") ? "due" : "assigned" });
		},

		sidebarToggleTaskList: function (event) {
			var target = $(event.currentTarget);

//			debug && console.log("DiaryDayView.sidebarToggleTaskList - " + target.attr("class"));

			target.toggleClass("expanded");
		},
		
		

		/*
		 * Diary Item View/Edit Form Dialog
		 */
		
		showDiaryItemForm: function(event) {
			once=true;
			var view = this,
				params = {},
				target = $(event.currentTarget),
				diaryItemId = target.attr('diaryItemId') || null;

			// Do nothing for days of note
			if (target.hasClass("dayOfNote")) return;

			if (diaryItemId == null) {
				params = view.getDiaryItemAttributesFromElement(target);
			}
			//console.log("showDiaryItemForm"+diaryItemId)
			if(diaryItemId)
			{
			//console.log("inside if");
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT UserID,createdBy,type FROM diaryItem WHERE id = :diaryItemId",

            // params
            {
			'diaryItemId' :diaryItemId
            },
			// success
			function(statement) {
			
				var detail=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                    
                 if(detail.length)
                 {
                 		var userID=detail[0].UserID;
                 		var createdBy=detail[0].createdBy;
                 		var type=detail[0].type;
                 		console.log("showDiaryItemForm : "+createdBy);
                 		if(createdBy!=null && userID==createdBy)
                 		{
                 			console.log("createdBy diary Item Match");
                 		 	diaryItemForm.showDialog(diaryItemId,
                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
                                          params.startTime,
                                          params.endTime,
                                          { 'subject' : params.subject }, createdBy);
                                   $(".editButtonContainer .save").show();
                                   $(".ui-dialog-titlebar").hide();
                                   $(".editButtonContainer .lockItem").hide();
                                   $(".editButtonContainer .delete").show();
                 		}
                 		else
                 		{
                 			console.log("createdBy Match FALSE");
                 			diaryItemForm.showDialog(diaryItemId,
                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
                                          params.startTime,
                                          params.endTime,
                                          { 'subject' : params.subject }, createdBy);
                            disablediaryitemformDay();
                            if(type == 'EVENT'){
                           		$(".editButtonContainer .save").hide();
                            } else {
                            	$(".editButtonContainer .save").show();
                            }
                 			$(".editButtonContainer .delete").hide();
                 			$(".ui-dialog-titlebar").hide();
                 			$(".editButtonContainer .lockItem").show();
                 		}
                 }
			
			},
			
			// error
			function error()
			{
				//console.log("Error while trying to create ClassUser item");
			}
	);
	}
	else
	{
	console.log("inside else");
		diaryItemForm.showDialog(diaryItemId,
                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
                                          params.startTime,
                                          params.endTime,
                                          { 'subject' : params.subject });
         $(".editButtonContainer .save").show();
         $(".ui-dialog-titlebar").hide();
         $(".editButtonContainer .lockItem").hide();
	}
			
			event.stopPropagation();
		},

		getDiaryItemAttributesFromElement: function (element) {
			var view = this,
				params = {},
				target = $(element),
				periodId = target.attr('period'),
				cycleDay = target.attr('cycleDay');
			
			// try to get the dayKey
			var dayKey = target.attr("dayKey");
			if (dayKey) {
				params.date = Rocketboots.date.Day.parseDayKey(dayKey).getDate();
			} else {
				params.date = view.currentDay.getDate();
			}

			// try to get period
			var period = target.attr("period");
			if (period) {
				params.period = parseInt(period, 10);
			}

			// try to get start/end times
			{
			    var startTimeString = target.attr("startTime"),
					endTimeString = target.attr("endTime"),
					startTime = startTimeString ? Rocketboots.date.Time.parseTime(startTimeString) : null,
					endTime = endTimeString ? Rocketboots.date.Time.parseTime(endTimeString) : null,
					isMidnight = function (time) {
						return time.getHour() == 0 && time.getMinute() == 0;
					};
				
				//To fix issue EZTEST-298.
				/* if (startTime && endTime) {
					// don't use midnight times, instead taking a 1 hour segment closest to the following/previous period
					if (isMidnight(startTime)) {
						startTime = new Rocketboots.date.Time(endTime.getHour() - 1, endTime.getMinute());
					}
					if (isMidnight(endTime)) {
						endTime = new Rocketboots.date.Time(startTime.getHour() + 1, startTime.getMinute());
					}
				} */

				if (startTime) params.startTime = startTime;
				if (endTime) params.endTime = endTime;
			}

			// try to get subject
			var subjectId = target.attr("subjectId");
			var className = null;
            if(subjectId && periodId && cycleDay){
            	className = getObjectByValues(app.model.get(ModelName.ALLCLASSES), subjectId, 'subjectId',cycleDay,'cycleDay', periodId, 'periodId');
            }
            
			var subject;
			if(className){
				subject = className;
			} else {
			 	subject = subjectId && app.model.get(ModelName.SUBJECTS).get(subjectId);
			}

			if (subject) {
				params.subject = subject;
			}

			return params;
		},
		
		/* 
		 * Diary Item Attachments
		 */
		
		openDiaryItemAttachment: function (event) {
		//alert("openDiaryItem attachment");
			var target = $(event.target),
				diaryItemId = target.closest("article").find(".infolink").attr('diaryItemId'),
				attachmentId = target.closest(".attachment").attr('attachmentId'),
				diaryItem = null,
				attachment = null;
			
			if (diaryItemId && attachmentId) {
				diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId);
				attachment = diaryItem.get('attachments').get(attachmentId);
				if (diaryItem && attachment) {
					app.context.trigger(EventName.OPENATTACHMENT, diaryItem, attachment);
				}
			}
			event.stopPropagation();
		},
		
		/*
		 * Note Entries
		 */
		
		appendNoteEntry: function(event) {
			// get the note that was entered
			var noteEntryElement = $(".noteDetail #newNoteEntry .noteEntry"),
				noteId = parseInt(noteEntryElement.attr("noteId"), 10),
				noteText = noteEntryElement.val().trim();
			
			// append the note
			diary.view.DiaryView.appendNote(noteId, noteText);
		},
		DeleteNoteEntry: function(event) {
           var hel = confirm("Are you sure to delete this note ?");
			
					if(hel)
					{    var noteEntryElement = $(".noteDetail #newNoteEntry .noteEntry"),
                noteId = parseInt(noteEntryElement.attr("noteId"), 10);
                DeleteNoteEntryMethod(noteId);}
                	else
                		return;
                
        },
		
		savCompletion: function(event) {
	
	
			clicksave=true;
			
     checkedInput=$(event.target).is(":checked");
		
		
			
			
			
			$(event.target).parents(".taskListItem").find(".link").trigger("click");
			
		
	
                
        },
		
		pullUpSlider : function(event) {
			
			$("#toppanel #panel").css("display","block");
			$("#toppanel #panel").css("height","400px");
			$("#toppanel a.open").css("display","none");
			$("#toppanel a.close").css("display","block");
			
		},
		
		pushDownSlider : function(event) {
			$("#toppanel #panel").css("display","block");
			$("#toppanel #panel").css("height","45px");
			$("#toppanel a.open").css("display","block");
			$("#toppanel a.close").css("display","none");
			
		},
		
		accessStudCalendar : function(event) {
			
			openCalendarAccess();
		},
		
		/*
		 * Drag and drop
		 */
		dropDiaryItem: function (type, $diaryItem, $dropTarget, event) {
			var view = this,
				diaryItemId = $diaryItem.attr("diaryItemId") ? parseInt($diaryItem.attr("diaryItemId"), 10) : null,
				diaryItem = app.model.get(ModelName.DIARYITEMS).get(diaryItemId),
                isDue = $diaryItem.hasClass('due'),
				diaryItemAttrsFromPeriod = view.getDiaryItemAttributesFromElement($dropTarget);

			view._scrollManager.maintainPosition = true;

			if (diaryItem) {

				// TODO update command to handle meta-periods
				app.context.trigger(EventName.UPDATEDIARYITEMONDROP,
                                    diaryItem,
                                    isDue,
                                    null,
                                    diaryItemAttrsFromPeriod.startTime ? diaryItemAttrsFromPeriod.startTime : null);

				//debug && console.log("DiaryDayView.dropDiaryItem", diaryItemAttrsFromPeriod.startTime);

			} else {

                //debug && console.log("DiaryDayView.dropDiaryItem - failed to update diary item with ID " + diaryItemId);

			}
		},

		dropFile: function (type, fileList, $dropTarget, event) {
			var view = this,
				// diary item id will be null when it's a file drop on a period
				diaryItemId = $dropTarget.attr("diaryItemId") ? parseInt($dropTarget.attr("diaryItemId"), 10) : null,
				params = view.getDiaryItemAttributesFromElement($dropTarget),
                defaultValues = {};

			// If there is a single attachment, derive the title from it
			if (fileList.length == 1) {
				var fileProperties = diary.view.air.ViewFilesystemUtil.getFileProperties(fileList[0]);

				if (fileProperties.title) defaultValues.title = fileProperties.title;
				if (fileProperties.description) defaultValues.description = fileProperties.description;
			}

            // add the subject to the default values
            defaultValues.subject = params.subject;

			diaryItemForm.showDialog(diaryItemId,
                                          app.model.get(ModelName.APPLICATIONSTATE).get('selectedDay'),
                                          params.startTime,
                                          params.endTime,
                                          defaultValues,
                                          fileList);
		},
		
		getEmpowerBannerArticle: function(forDate, packageId) {
			
			// get the list of banner articles that we can display
			var bannerArticles, article;
			
			if (this.schoolProfile.get("contentMapping") && this.schoolProfile.get("contentMapping").get("calendarBanners")) {
				
				// get the list of all the banner articles
				bannerArticles = this.schoolProfile.get("contentMapping").get("calendarBanners").toArray();
				
				// we will try to find an article which is enabled within the specified
				// date
				article = _.find(bannerArticles, function(article) {
					// check if the article is enabled within the specified date
					return (forDate.getTime() >= article.get("fromDate").getTime() && forDate.getTime() <= article.get("toDate").getTime() && article.get("packageId") == packageId);
				});
			
			} else {
				
				// no article could be determined
				article = null;
				
			}
			
			return article;
		} 	
		
	});

	return view;

})();
