diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};

/**
 * Provide the hover states and drop-callbacks for drag-and-drop elements.
 * Supports HTML element dragging and file dragging.
 *
 * Please note that this class only supports ONE file-drop handler.
 * It does support any number of HTML element drag-and-drop handlers.
 * However, it must be ensured that the "draggables" selector does not have overlap between groups.
 */
diary.view.DragDropHelper = (function() {

	var debug = false,
		defaults = {
			container: null,
			scrollManager: null,
			dropTargetClass: "dropTarget",
			dragGroups: []
		},
		defaultGroup = {
			draggables: null,
			dropTargets: null,
			canDropFiles: false,
			onDropOverTarget: function (type, dragItemOrFileList, dropTarget, event) {
				debug && console.log("DragDropHelper.onDropOverTarget - unimplemented drop handler, no operation performed.", type);
			}
		},
		// for debugging
		stringifyElement = function (element) {
			return _.reduce($(element).toArray(), function (memo, el) {
				var $el = $(el),
					text = $el.text().trim();

				return memo + "<" + el.tagName +
					_.reduce(el.attributes, function (memo, attr) {
						return memo + " " + attr.name + "=\"" + attr.value + "\"";
					}, "") + (!text ? "/>" : ">" + text + "</" + el.tagName + ">") + "\n";
			}, "");
		},
		emptyDragState = { group:{} },
		DRAG_TYPE = {
			FILE : "FILE",
			ELEMENT : "ELEMENT"
		};

	/**
	 * Construct the helper. It will not work until you additionally call .assignEventHandlers()
	 * @throws {Error} If you attempt to specify more than 1 file-drop handler.
	 * @constructor
	 */
	var dragDropHelper = function (options) {
		var helper = this,
			fileGroupCount = 0;

		helper.settings = _.extend({}, defaults, options);
		helper.dragState = _.extend({}, emptyDragState);

		if (helper.settings.dragGroups.length > 0) {
			helper.settings.dragGroups = [];
			_.each(options.dragGroups, function (dragGroup) {
				helper.settings.dragGroups.push(_.extend({}, defaultGroup, dragGroup));
				if (dragGroup.canDropFiles) ++fileGroupCount;
			});
		}

		if (fileGroupCount > 1) throw new Error("Multiple file-drop handlers not supported at this time.");
	};

	/**
	 * Add the necessary event handlers (used internally in the DragDropHelper) to the container and draggable elements.
	 * This must be called at least once to allow drag-drop to work.
	 *
	 * Calling this subsequent times will remove any drag handlers from items which are still matched,
	 * and re-assign the handlers. This is useful because the helper can be constructed once
	 * (for example in a views initialize function) but have the event handlers assigned multiple times
	 * when the HTML elements change (for example in a views render function).
	 */
	dragDropHelper.prototype.assignEventHandlers = function () {
		var helper = this;

		// Container
		helper._getContainer()
			.off("dragover dragenter dragleave drop")
			.on("dragover", _.bind(helper._dragOver, helper))
			.on("dragenter", _.bind(helper._dragEnter, helper))
			.on("dragleave", _.bind(helper._dragLeave, helper))
			.on("drop", _.bind(helper._drop, helper));

		// Draggables
		_.each(helper.settings.dragGroups, function (dragGroup) {
			helper._getElementsForSelector(dragGroup.draggables).off("dragstart").on("dragstart", function (event) {
				helper._dragStartWithElement(event, dragGroup);
			});
		});
	};

	/**
	 * Determine if a drag event represents dragging-and-dropping of files.
	 * Note that the event passed in here must be the "original event", thus to determine if the event
	 * is a file drag event in a "dragEnter" (for example) you would use:
	 * <code>dragDropHelper._isFileDrop(dragEnterEventObject.originalEvent)</code>
	 * @param event The original drag event
	 * @return {Boolean} Whether the event represents a file-drag event.
	 * @private
	 */
	dragDropHelper.prototype._isFileDrop = function(event) {
		// TODO : move file reading to air package
		return event.dataTransfer != null && event.dataTransfer.types != null &&
			(event.dataTransfer.types.indexOf('text/uri-list') != -1 || event.dataTransfer.types.indexOf('application/x-vnd.adobe.air.file-list') != -1);
	};

	/**
	 * Get the HTML element representing the drag-and-drop region controlled by this helper class.
	 * @return {jQuery} The container
	 * @private
	 */
	dragDropHelper.prototype._getContainer = function () {
		return $(this.settings.container);
	};

	/**
	 * Get the HTML element(s) represented by a selector, ensuring it is a child of the container.
	 * @param elementSelector {String} The jQuery selector
	 * @return {jQuery} The matched elements
	 * @private
	 */
	dragDropHelper.prototype._getElementsForSelector = function (elementSelector) {
		if (!elementSelector) return $([]); // return empty jQuery set
		return $(elementSelector, this._getContainer());
	};

	dragDropHelper.prototype._setDropTarget = function ($dropTarget) {
		var helper = this;

		// handle empty drop targets
		if (!$dropTarget || !$dropTarget.length) return;

		// don't allow drag over original container
		if ($dropTarget.is(helper.dragState.originalDropTarget)) return;

		// store new target, add CSS class
		helper.dragState.dropTarget = $dropTarget;
		$dropTarget.addClass(helper.settings.dropTargetClass);
	};

	/**
	 * This will only be called in instances where dragging has begun from within the application,
	 * via dragging a HTML element. It will never be called for file drops.
	 * It will be called once when a HTML element begins drag.
	 * @param event
	 * @param dragGroup The group that the dragged element belongs to.
	 * @private
	 */
	dragDropHelper.prototype._dragStartWithElement = function (event, dragGroup) {
		var helper = this,
			$target = $(event.target),
			$dropTarget = $(event.target).closest(dragGroup.dropTargets);

		helper.dragState = {
			type : DRAG_TYPE.ELEMENT,
			group : dragGroup,
			draggable : $target,
			originalDropTarget : $dropTarget,
			dropTarget : null
		};

		debug && console.log("DragDropHelper._dragStart", dragGroup, stringifyElement(helper.dragState.draggable), stringifyElement(helper.dragState.originalDropTarget));
	};

	/**
	 * This will be called whenever a dragEnter event is fired for file-drag events.
	 * @param event
	 * @private
	 */
	dragDropHelper.prototype._dragStartWithFiles = function (event) {
		var helper = this,
			// find the group which supports file-drop
			group = _.find(helper.settings.dragGroups, function (dragGroup) { return dragGroup.canDropFiles; });

		helper.dragState = {
			type : DRAG_TYPE.FILE,
			group : group,
			dropTarget : null
		};

		debug && console.log("DragDropHelper._dragStartWithFiles", group);
	};

	/**
	 * Called constantly during dragging when the mouse is over the drag/drop container
	 * (even when the mouse isn't moving).
	 * @param event
	 * @private
	 */
	dragDropHelper.prototype._dragOver = function (event) {
		var helper = this,
			$target = $(event.target),
			$dropTarget = helper.dragState.group ? $target.closest(helper.dragState.group.dropTargets) : $([]);

		// without preventing the default dragOver action, drop will never be called
		event.originalEvent.preventDefault();

		debug && console.log("DragDropHelper._dragOver", helper.dragState.type, stringifyElement($dropTarget));

		// auto-scroll
		if (helper.settings.scrollManager) {
			helper.settings.scrollManager.drag(event.originalEvent);
		}

		// to handle false dragLeave events, re-assign drop targets on dragOver
		helper._setDropTarget($dropTarget);
	};

	/**
	 * Called every time a new DOM element is entered.
	 * @param event
	 * @private
	 */
	dragDropHelper.prototype._dragEnter = function (event) {
		var helper = this,
			$target = $(event.target),
			$dropTarget;

		event.originalEvent.preventDefault();

		// check if we're starting a new file drag
		if (helper._isFileDrop(event.originalEvent)) {
			// reset the file drag every time a new HTML element is entered, since we don't get a dragStart event for file drags
			helper._dragStartWithFiles(event);
		}

		$dropTarget = $target.closest(helper.dragState.group.dropTargets);

		debug && console.log("DragDropHelper._dragEnter", helper.dragState.type, stringifyElement($dropTarget));

		// store new target, add CSS class
		helper._setDropTarget($dropTarget);
	};

	/**
	 * Called every time a DOM element is exited. Also called when the dragging stops (via mouse release).
	 * @param event
	 * @private
	 */
	dragDropHelper.prototype._dragLeave = function (event) {
		var helper = this,
			$target = $(event.target),
			$dropTarget = $target.closest(helper.dragState.group.dropTargets);

		debug && console.log("DragDropHelper._dragLeave", helper.dragState.type, stringifyElement($dropTarget));

		// clear target, remove CSS class (opposite of _setDropTarget)
		helper.dragState.dropTarget = null;
		$dropTarget.removeClass(helper.settings.dropTargetClass);
	};

	dragDropHelper.prototype._drop = function (event) {
		var helper = this;

		debug && console.log("DragDropHelper._drop", helper.dragState.type);

		// trigger drop callback, but only if the item is dropped on a matched item
		if (helper.dragState.dropTarget && helper.dragState.dropTarget.length) {
			var dragItemOrFileList = helper.dragState.draggable;
			if (helper.dragState.type == DRAG_TYPE.FILE) {
				// TODO : move file reading to air package
				var files = event.originalEvent.dataTransfer.getData('application/x-vnd.adobe.air.file-list');
				dragItemOrFileList = _.map(
						_.filter(files, function(file) { return !file.isDirectory; }),
						function(file) {
							return file.nativePath;
						}
					);

				// don't trigger on empty file lists
				if (dragItemOrFileList.length == 0) dragItemOrFileList = null;
			}

			// don't trigger if there isn't a valid drop item
			if (dragItemOrFileList) {
				helper.dragState.group.onDropOverTarget(helper.dragState.type, dragItemOrFileList, helper.dragState.dropTarget, event);

				debug && console.log("DragDropHelper._drop",
					helper.dragState.type,
					(helper.dragState.type == DRAG_TYPE.FILE ?
						_.reduce(dragItemOrFileList, function (memo, file, index) {
							return memo + file + (index + 1 == dragItemOrFileList.length ? "" : ", ");
						}, "[") + "]" :
						stringifyElement(dragItemOrFileList)),
					stringifyElement(helper.dragState.dropTarget));
			}
		}

		// clear drag state
		helper._dragLeave(event);
		helper.dragState = _.extend({}, emptyDragState);
	};

	dragDropHelper.DRAG_TYPE = DRAG_TYPE;

	return dragDropHelper;

})();