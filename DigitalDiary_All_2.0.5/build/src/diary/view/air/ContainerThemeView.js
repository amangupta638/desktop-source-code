diary = diary || {};
diary.view = diary.view || {};
diary.view.air = diary.view.air || {};

diary.view.air.ContainerThemeView = (function() {
	
	var view = Backbone.View.extend({

		viewShown: false,
		updateHtmlLoader: null,
        isAppError: false,
        settings : {},
        retrying: false,

		initialize: function() {
        	this.student = app.model.get(ModelName.STUDENTPROFILE);
        	this.school = app.model.get(ModelName.SCHOOLPROFILE);
			diary.view.ThemeUtil.registerThemeListener(view);
			this.render();
		},
	
		showTheme: function(school, student) {
			//alert("showTheme");
			this.school = school;
			this.student = student;
			var view = this;
            view.isAppError = view.settings.isError;
            view.retrying = false;

			if (view.dialogHtmlLoader != null) return;
			
			var windowInitOptions = new air.NativeWindowInitOptions();
			windowInitOptions.type = air.NativeWindowType.LIGHTWEIGHT;
			windowInitOptions.systemChrome = air.NativeWindowSystemChrome.NONE;
			windowInitOptions.transparent = true;

            var themeWindowWidth = air.Capabilities.screenResolutionX-430,
			themeWindowHeight = air.Capabilities.screenResolutionY-200,
				windowX = (air.Capabilities.screenResolutionX - themeWindowWidth) / 2,
				windowY = (air.Capabilities.screenResolutionY - themeWindowHeight) / 2,
				bounds = new air.Rectangle(windowX, windowY, themeWindowWidth, themeWindowHeight),
				initialUrl = new air.URLRequest("windowTheme.html");
				view.dialogHtmlLoader = air.HTMLLoader.createRootWindow(
				false,
				windowInitOptions,
				false,
				bounds
			);
			
			view.clippedMask = new au.com.productdynamics.view.ClippedRoundedWindow(view.dialogHtmlLoader);

			view.dialogHtmlLoader.window.nativeWindow.alwaysInFront = true;

            var updateHtmlLoaderComplete = function(event) {
                try {
                   // console.log('ContainerDialogView.updateHtmlLoaderComplete()');

					if (!view.dialogHtmlLoader) {
						//console.log('ContainerDialogView has already been closed');
						return;
					}
					
                    view.dialogHtmlLoader.removeEventListener("complete", updateHtmlLoaderComplete);
					// Required for ThemeUtil
					view.document = view.dialogHtmlLoader.window.document;
                    view.viewShown = true;
                    view.render();

                    _.defer(function() {
                        view.dialogHtmlLoader.window.displayMessage(view.settings.message, view.settings.isError);
                    });

                    view.dialogHtmlLoader.window.nativeWindow.visible = true;

                    view.dialogHtmlLoader.window.addEventListener('startDrag', function(event) {
                        view.dragView.apply(view, [event]);
                    });

                    view.dialogHtmlLoader.window.addEventListener('closeWindow', function(event) {
                        if (view.settings.disableClose) return;
                        view.hideTheme();
                        if (view.isAppError) {
                            window.nativeWindow.close();
                        }
                    });

                    view.dialogHtmlLoader.window.addEventListener('retry', function(event) {
                        if (view.retrying) return;

                        if (typeof(view.settings.retryCallback) === "function") {
                            view.settings.retryCallback();
                            view.retrying = true;
                        }
                    });

                } catch (e) {
                    //console.log(new Date(), e);
                }
            };

            //console.log("Adding onComplete event listener", new Date());

            view.dialogHtmlLoader.addEventListener("complete", updateHtmlLoaderComplete);

            view.dialogHtmlLoader.load(initialUrl);

        },
		
		hideTheme: function() {

			var view = this;

			if (view.dialogHtmlLoader == null) return;

            view.dialogHtmlLoader.window.nativeWindow.close();
			view.dialogHtmlLoader = null;
			view.viewShown = false;
		},
		
		showBusy: function() {
			view.dialogHtmlLoader.window.showBusy();
		},
		
		hideBusy: function() {
			view.dialogHtmlLoader.window.hideBusy();
		},
		
		render: function() {
			var view = this;
			if (!view.viewShown) return;
			var params = {
				'school' : view.school.toJSON(),
				'student' : view.student.toJSON()
            };
			
			//alert("view.school : "+JSON.stringify(view.school));
            //console.log("view.school theme: "+view.school.get("theme"));
			var content = app.render('windowTheme', params);
			view.dialogHtmlLoader.window.injectContent(content,view.school.get("theme"));
			//diary.view.ThemeUtil.renderThemeFromSchoolProfile(view);
		},
		
		dragView: function(event) {
			var view = this;
			view.dialogHtmlLoader.window.nativeWindow.startMove();
		}
	});

	return view;

})();
