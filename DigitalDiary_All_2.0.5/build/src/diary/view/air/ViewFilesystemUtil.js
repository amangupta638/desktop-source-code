diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};
diary.view.air = diary.view.air || {};

/**
 * A collection of utility methods to be used by the diary views to render
 * files.
 * @author John Pearce
 * 
 * @type {Object}
 */
diary.view.air.ViewFilesystemUtil = {
	
	/**
	 * @public
	 * Load the appropriate file icon for a given file and desired icon size.
	 * 
	 * The icon returned will be either the largest icon available, or the smallest
	 * icon larger or equal to the size provided in the parameters.
	 * 
	 * @param {!string} nativeFilePath The native filesystem-dependent path, pointing to the file for which an icon is required.
	 * @param {!Object} size The icon size in the format of { width: number, height: number }
	 * 
	 * @return {!Object} The results, in the format { 'source': ?string, 'size': ?Object, 'error': ?string|Object },
	 * 		The first argument (icon source) is data appropriate to set on an HTML IMG element. (either a URL or Base64-encoded data).
	 * 		The second argument (icon size) is the width and height of the resultant icon (not necessarily the same as the size requested).
	 * 		The third argument (error) is a string or error object representing anything that went wrong
	 */
	'loadFileIcon' : function (nativeFilePath, size) {
		
		var sourceFile, bestIcon = null;
		
		try {
			
			// load file and check
			sourceFile = new air.File(nativeFilePath);
			if (!sourceFile.exists || sourceFile.isDirectory) {
				console.log("Failed to get icon for file \"" + nativeFilePath + "\" - file is a directory or does not exist");
				return { 'error': "Source file does not exist, or is a directory" };
			}
			
			// check if there are icons
			if (!sourceFile.icon || !sourceFile.icon.bitmaps || sourceFile.icon.bitmaps.length == 0) {
				console.log("Failed to get icon for file \"" + nativeFilePath + "\" - no icons");
				return { 'error': "The file has no icons associated with it" };
			}
			
			// find the most appropriate icon
			_.forEach(sourceFile.icon.bitmaps, function (bitmap) {
				
				if (bestIcon == null || !isSpecified(size) || (bitmap.height >= size.height && bitmap.width >= size.width && bitmap.height < bestIcon.height && bitmap.width < bestIcon.width)) {
					bestIcon = bitmap;
				}
			});
			
			// encode the icon
			{
				var
					pngEncoder = new mx.graphics.codec.PNGEncoder(),
					base64Encoder = new mx.utils.Base64Encoder(),
					iconEncoded = null;
				
				// convert to PNG image
				iconEncoded = pngEncoder.encode(bestIcon);
				
				// convert from PNG to Base64 encoded string
				base64Encoder.encodeBytes(iconEncoded);
				
				// return the result
				return {
					'source': "data:image/png;base64," + base64Encoder.toString(),
					'size': {
						'width': bestIcon.width, 
						'height': bestIcon.height
					}
				};
			}
			
		} catch (ex) {
			console.log("Failed to get icon for file \"" + nativeFilePath + "\"", ex);
			return { 'error': ex };
		}
		
	},
	
	/**
	 * @public
	 * Obtain a human-readable file name from a native file path.
	 * 
	 * @param {string} nativeFilePath The full file path.
	 * @param {?boolean} includeExtension Whether the file extension should be included. Defaults to <code>true</code>. 
	 * 
	 * @return {string} A human-readable string representing the file (the file name).
	 */
	'displayNameForFilePath': function (nativeFilePath, includeExtension) {
		var file;
		
		// Defaults
		if (!isSpecified(includeExtension)) {
			includeExtension = true;
		}
		
		try {
			
			file = new air.File(nativeFilePath);
			
			// optionally remove file extension
			if (!includeExtension && file.extension && file.extension.length > 0) {
				return file.name.substring(0, file.name.length - 1 - file.extension.length);
			}
			
			return file.name;
			
		} catch (ex) {
			console.log("Error converting file path \"" + nativeFilePath + "\" to file name", ex);
			return nativeFilePath;
		}
	},
	
	/**
	 * @public
	 * For a document in OpenXML format (such as .docx files), get the documents
	 * properties (such as the title and description).
	 * 
	 * @param {!string} nativeFilePath The file to be processed.
	 * 
	 * @return {Object} The documents properties, such as title and description.
	 */
	'getDocumentProperties': function (nativeFilePath) {
		
		// holds the contents as a zip file
		var file, fileStream, zipFile, docProps = {};
		
		try {
			
			// read the file from disk
			file = new air.File(nativeFilePath);
			
			fileStream = new air.FileStream();
			try {
				fileStream.open(file, "read");				
				// contain the downloaded contents in a memory zip file
				zipFile = new window.nochump.util.zip.ZipFile(fileStream);
			} catch (e) {
				console.log(e);
			} finally {
				// close file stream
				fileStream.close();				
			}
			
			
			
			// for each item that was found in the zip file unzip it and
			// save it to the local path
			for(var i = 0; i < zipFile.entries.length; i++) {
				
				// get the name of the zip file
				var entry = zipFile.entries[i],
					fileDataByteArray,
					fileAsString,
					xmlDoc;
				
				// search for meta data XML file
				// 		"docProps/core.xml" is Microsoft Office's metadata file.
				//		"meta.xml" is OpenOffice's metadata file.
				if (entry.name != "docProps/core.xml" && entry.name != "meta.xml") continue;
				
				// extract the entry's data from the zip
				fileDataByteArray = zipFile.getInput(entry);
				fileAsString = fileDataByteArray.readUTFBytes(fileDataByteArray.length);
				
				// parse the XML using jQuery
				xmlDoc = $($.parseXML(fileAsString));
				docProps.title = xmlDoc.find("title").text();
				docProps.description = xmlDoc.find("description").text();
				
			}
		
			// return the data
			return docProps;
			
		} catch (error) {
			// log error
			console.log("Error while trying to read document properties", error);
			
			// return error
			return error;
		}
		
	},
	
	/**
	 * @public
	 * Derive the properties of a file.
	 * For documents, these properties are embedded in the document itself.
	 * For unrecognised files, the file name is used as the title and the description is empty.
	 * 
	 * @param {!string} nativeFilePath The file to be processed.
	 * 
	 * @return {Object} Document properties, such as title and description.
	 */
	'getFileProperties': function (nativeFilePath) {
		
		var documentExtensions = [
				// Microsoft Office XML file formats
				'docx', 'xlsx', 'pptx', 
				// Open Office formats
				'odt', 'ods', 'odp', 'odg'
            ],
			file,
			fileProperties = {};
		
		try {
			
			file = new air.File(nativeFilePath);
			
			// documents (add title and description if present)
			if (_.find(documentExtensions, function (extension) { return extension == file.extension; })) {
				fileProperties = _.extend({}, fileProperties, diary.view.air.ViewFilesystemUtil.getDocumentProperties(nativeFilePath));
			}
			
			// title defaults to the filename
			if (!fileProperties.title || fileProperties.title.length == 0) {
				fileProperties.title = diary.view.air.ViewFilesystemUtil.displayNameForFilePath(nativeFilePath, false);
			}
			
			return fileProperties;
			
		} catch (error) {
			
			console.log("Error whilst attempting to get file properties:", nativeFilePath, error);
			return error;
		}
	}
	
};