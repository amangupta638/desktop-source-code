diary = diary || {};
diary.view = diary.view || {};

/**
 * A set of utility methods to assist views which require themeing
 * based on the school profile.
 * @static
 */
diary.view.ThemeUtil = (function() {

	var util = {};

	/**
	 * Register a view to automatically update its theme whenever the theme property
	 * on the school profile changes.
	 * @param {diary.view.*} view The reference to the view which wishes to control themeing (used for registering 1 handler per view).
	 */
	util.registerThemeListener = function (view) {
		var event = "change:" + ModelName.SCHOOLPROFILE,
			schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);

		if (!view) {
			throw new Error("ThemeUtil.registerThemeListener must have a view specified");
		}

		// remove any prior listener (for the given view only)
		app.model.off(event, util._themeDidChange, view);
		schoolProfile && schoolProfile.off("change:theme", util._themeDidChange, view);
		// add new listener
		//app.model.on(event, util._themeDidChange, view);
		app.listenTo(app.model, event, util._themeDidChange);
		schoolProfile && schoolProfile.on("change:theme", util._themeDidChange, view);
	};

	util._themeDidChange = function () {
		util.renderThemeFromSchoolProfile.call(this);
	};

	/**
	 * Retrieve the theme from the school profile,
	 * and update the CSS in the current page according to that theme.
	 *
	 * If the view does not use the global window, the view must have
	 * the "document" property set to the appropriate document. This will
	 * apply to any separate native windows.
	 *
	 * @param {diary.view.*} view The view in which the theme should be rendered.
	 * @return {String} The theme used for rendering (typically a hex colour value)
	 */
	util.renderThemeFromSchoolProfile = function (view) {
		var schoolProfile = app.model.get(ModelName.SCHOOLPROFILE),
			theme = schoolProfile ? schoolProfile.get('theme') : null,
			stylesheet = "";

		if (theme) {

			stylesheet = "<style id=\"theme\">" +
				"body.widgetWindow {" +
					"background-color: " + Color(theme).alpha(0.8).rgbaString() +
				"}" +

				".frame { " +
					"background-color: " + theme + "; " +
				"}" +

				".frame > header > nav > ul > li > a.selected, " +
				".frame > footer > nav > ul > li a.selected, " +
				".frame > footer > nav > ul > li a:hover.selected {" +
					"color: " + theme + ";" +
				"}" +

				".ui-dialog .ui-dialog-titlebar {" +
					"background: " + theme + ";" +
				"}" +
				"</style>";

			$("head", view ? view.document : null)
				.find("style#theme").remove().end()
				.append(stylesheet);
		}

		return theme;
	};

	return util;

})();
