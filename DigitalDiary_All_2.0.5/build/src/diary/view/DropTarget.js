diary = diary || {};
diary.view = diary.view || {};

diary.view.DropTarget = (function() {

	return Backbone.View.extend({
	
		events: {
			"dragover":		"dragOver",
			"dragenter":	"dragEnter",
			"dragleave":	"dragLeave",
			"drop":			"drop"
		},
		
		initialize: function() {
			
			this.dragManager = app.locator.getService("dragManager");
			
			if (typeof(this.dropIdentifier) === "undefined") {
				this.dropIdentifier = this.dragManager.newDropIdentifier();
			}
			
			this.dragManager.on("change", this.change, this);
		
			this.reset();
	
			/*
			if (typeof(this.handlers) !== "undefined") {
				if (typeof(this.handlerScope) === "undefined") {
					this.handlerScope = this;
				}
				
				this.register(this.handlers[0], this.handlers[1], this.handlers[2], this.handlerScope);
			}
			*/
			
//			console.log('My drop id = ' + this.dropIdentifier);
			
			return this;
		},
		
		register: function(dragEnterHandler, dragLeaveHandler, dropHandler, dragOverHandler, scope) {

			if (typeof(dragEnterHandler) == "function") {
				this.on("dragEnter", dragEnterHandler, scope);
			}
			
			if (typeof(dragLeaveHandler) == "function") {
				this.on("dragLeave", dragLeaveHandler, scope);
			}
				
			if (typeof(dropHandler) == "function") {
				this.on("drop", dropHandler, scope);
			}
			
			if (typeof(dragOverHandler) == "function") {
				this.on("dragOver", dragOverHandler, scope);
			}
			
			return this;
		},
		
		reset: function() {
			this.active = false;	
			this.type = null;
			this.target = null;
			this.subjectId = null;
		}, 
		
		setTarget: function(target, type, active, subjectId) {
			this.target = target;
			this.type = type; 
			this.active = active;
			this.subjectId = subjectId;
		},
		
		change: function(identifier) {
			if (identifier == this.dropIdentifier) return;
			if (!this.active) return;
			
//			console.log('TODO: catch-all disable drop target - shouldn\'t be active anyway...');
		},
		
		dragOver: function(event) {
			event.originalEvent.preventDefault();
	
			var dropTarget = this.dragManager.getDropTarget();		
			if (dropTarget == this.dropIdentifier) {
				this.trigger("dragOver", event.originalEvent);
				return;
			}
			if (dropTarget != null) return;
	
			// Reclaim drop target focus
			active = true;
			this.dragManager.setDropTarget(this.dropIdentifier);
			
			this.trigger("dragEnter", event.originalEvent);
			event.originalEvent.stopImmediatePropagation();
		},
		
		dragEnter: function(event) {
			event.originalEvent.preventDefault();

			if (this.active) return;
			active = true;
			this.dragManager.setDropTarget(this.dropIdentifier);
			
			this.trigger("dragEnter", event.originalEvent);
		},
		
		dragLeave: function(event) {
			this.active = false;
			this.dragManager.clearDropTarget(this.dropIdentifier);
			this.trigger("dragLeave", event.originalEvent);
		},
		
		drop: function(event) {
			event.originalEvent.stopImmediatePropagation();
			
			this.dragManager.clearDropTarget(this.dropIdentifier);
			
			this.trigger("dragLeave", event.originalEvent);
			this.trigger("drop", event.originalEvent);
			
		}, 
		
		isFileDrop: function(event) {
			return (event.dataTransfer.types != null) && (event.dataTransfer.types.indexOf('text/uri-list') != -1 || event.dataTransfer.types.indexOf('application/x-vnd.adobe.air.file-list') != -1);
		},
		
		getFileDropFileCount: function(event) {
			if (!this.isFileDrop(event)) return -1;
			
			var files = event.dataTransfer.getData('application/x-vnd.adobe.air.file-list'),
				fileCount = 0;
				
			_.each(files, function(file) {
				if (!file.isDirectory) fileCount++;
			});
			
			return fileCount;
		}
		
	});
	
})();
