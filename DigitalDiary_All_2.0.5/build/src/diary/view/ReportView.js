diary = diary || {};
diary.view = diary.view || {};
diary.view.ReportView = (function() {
	var reportView = Rocketboots.view.View.extend({
		notifications: [],
		
		events: {
			"change .reportType"				: "selectReport"
		},

		initialize: function(options) {
			var view = this;
			view.render();
		},	
		
		mapReports : function (report,currentUserId,schoolId) {
			return {
				'UniqueID' 		: report.get('UniqueID'),
				'isStudent' 	: report.get('isStudent'),
				'isTeacher' 	: report.get('isTeacher'),
				'isReportActive' : report.get('isReportActive'),
				'isSchoolActive' : report.get('isSchoolActive'),
				'ReportDescription' : report.get('ReportDescription'),
				'ReportURL' 	: report.get('ReportURL') + '&SchoolID='+schoolId+'&TeacherID='+currentUserId,
				'ReportName' 	: report.get('ReportName')
			};
		},
		
		selectReport : function (event) {
			var view = this;
			var reportId = $(".reportType").val();
			var reports = app.model.get(ModelName.REPORTS).getActiveReports();
			var currReport = getObjectByValue(reports, reportId, 'UniqueID');
			
			var currentUserId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
			var schoolId = app.model.get(ModelName.STUDENTPROFILE).get('schoolId');
			
			var params = {
				"selectedReport": currReport.get('ReportURL')+ '&SchoolID='+schoolId+'&TeacherID='+currentUserId
			}
			
			var reportContent = app.render('reportDetails',params);
			$(".rightpanelDetails").html(reportContent);
		},
		
		render: function() {
			var view = this;
			var reports = app.model.get(ModelName.REPORTS).getActiveReports();
			var currentUserId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
			var schoolId = app.model.get(ModelName.STUDENTPROFILE).get('schoolId');
			
			var allReports = [];
			for(var i = 0 ; i < reports.length; i++){
				var report = reports.at(i);
				allReports.push(this.mapReports(report,currentUserId,schoolId));
			}
			var selectedReport = allReports[0].ReportURL;
			
			var params = {
				"selectedReport": selectedReport,
				"allReports" : allReports
			}
			var c = app.render('reportView', params);
			$(this.el).html(c);	
			
		}
		
	});
	
	return reportView;

})();