diary = typeof(diary) !== "undefined" ? diary : {};
diary.view = diary.view || {};
 var  passname="";
/**
 * A collection of utility methods to be used by the article views.
 * created by Brian Bason
 * 
 * @type {Object}
 */
diary.view.ArticleViewUtil = {
	
	
	/**
	 * Renders an article into the given view and place holder.
	 * 
	 * @param {!diary.model.ContentItem} article The article that is to be rendered on the specified view
	 * @param {!jQuery} element The jQuery html element which is to be used to render the article
	 */
	'renderArticle' : function (article, element) {
		
		element.html(article);
		//console.log("AIR memory not in use and allocated to AIR->air.System.freeMemory: " + air.System.freeMemory);
		//console.log("Memory used by application->air.System.privateMemory: " + air.System.privateMemory);
		//console.log("Memory currently in use and clloacted by AIR->air.System.totalMemory: " + air.System.totalMemory);	
		setTimeout(function(){		

		var imgs=$("img");
		imgs.each(function(){
			if($(this).hasClass('ui-datepicker-trigger'))
			{
			}
			else
			{
				var srcs=$(this).attr("src");
				
				//alert('srcs = ' + srcs);
			
				if(srcs.indexOf("app-storage")<0)
				{
					//srcs="app-storage:/"+srcs
				}
				
				if(srcs.indexOf("app")<0)
				{
					//srcs="app:/"+srcs
				}
				
				//alert('srcs = ' + srcs)
				
				$(this).attr("src",srcs)
				
			}
			
			
			
			})
			
		},10)
		
		/*
		var _dataManager = app.locator.getService(ServiceName.CONTENTITEMFORMMANAGER);

		// we will only render the article if we have otherwise we will ignore it
		// as we cannot do anything about it
		if (article) {

			//console.log("article1", article);

            var contentUrlToLoad = article.get('url'),
					baseContentUrl = contentUrlToLoad.substring(0, contentUrlToLoad.lastIndexOf('/') + 1),
					file = new air.File(),
					fileStream = new air.FileStream(),
					content = "",
					downloadingThisPackage = app.model.get(ModelName.CONTENTPACKAGEQUEUE).find(function(p) {return false;
						//return p.get('package') == article.get('packageId');
					});

			try {
				if(downloadingThisPackage) {
					content = "<h3>Downloading Content, this may take a few minutes...</h3>";
				} else {
					file.url = contentUrlToLoad;
					if (!file.exists) {
						//console.log("Can not find article", file.url);
						content = "<h3>Article not found.</h3>";
					} else {
						fileStream.open(file, "read");
						content = fileStream.readUTFBytes(fileStream.bytesAvailable);
					}
				}
			} catch (e) {
				//console.log("trying to open file", file.url);
				content = "<h3>Could not load Article.</h3>";
			} finally {
				fileStream.close();
				// clear the element if there is no content
				if(!content) {
					element.html("");
					return;
				}

				// We need to replace any src="./ references with the absolute path of where this content is stored.
				// TODO: regex this.  Is there any other types of external refs we need to rewrite?

				// ASSUMING ALL CONTENT URLS ARE RELATIVE
				content = content.replace(/src=\"(\.\/)?/ig, "src=\"" + baseContentUrl);

				// make images not draggable
				content = content.replace(/<img\s/ig, "<img draggable='false' ");

				element.html(content);

				_.defer(function() {
					_dataManager.getDataForArticle(article, function success(data) {
						$("input[type=text], textarea", element).each(function() {
							var $input = $(this);
							var key = $input.attr("name");
							if (key) {
								$input.val(data[key] || "");
								$input.on("blur", function() {
									data[key] = $input.val();
									_dataManager.setDataForArticle(data, article);
								});
							}
						});
					});

				});
			};
		}
	*/
	}
};