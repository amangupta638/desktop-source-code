/**
 * The manager for System Configuration.  The manager is used to take care of all the business
 * logic related to a system configuration.
 * 
 * author - Jane Sivieng
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SystemConfigurationManager = (function() {
	
	/**
	 * Constructs a System Configuration Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		this._systemConfigurationDelegate = app.locator.getService(ServiceName.SYSTEMCONFIGURATIONDELEGATE);
	};
	
	/**
	 * Gets the system configuration for the application.  The details of the passed model will be updated with the details that
	 * were read from the data store.
	 * 
	 * @param {?function(diary.model.SystemConfiguration)} successCallback The function that is to be called on successful system configuration update [OPTIONAL]
	 * @param {?function(message)} errorCallback The function that is to be called on failure of the system configuration update [OPTIONAL]
	 * @param {?diary.model.SystemConfiguration} systemConfiguration The system configuration to be updated if any [OPTIONAL]
	 */
	_manager.prototype.getSystemConfiguration = function(successCallback, errorCallback, systemConfiguration) {
		
		// check that a valid type was specified
		if (systemConfiguration && !(systemConfiguration instanceof diary.model.SystemConfiguration)) {
			throw new Error("System Configuration is not valid, profile must be of type diary.model.SystemConfiguration");
		}

		systemConfiguration = systemConfiguration || new diary.model.SystemConfiguration();
		this._systemConfigurationDelegate.getSystemConfiguration(
				function(readSystemConfiguration) {
					if(readSystemConfiguration) {
						systemConfiguration.set(readSystemConfiguration.attributes);
						successCallback(systemConfiguration);
					} else {
						errorCallback && errorCallback("No system configuration was found, system configuration should be defined");
					}
				}, errorCallback || $.noop
		);
	};
	
	/**
	 * Sets the system configuration of the application.  
	 * 
	 * @param {?function(diary.model.SystemConfiguration)} successCallback The function that is to be called on successful system configuration update [OPTIONAL]
	 * @param {?function(message)} errorCallback The function that is to be called on failure of the system configuration update [OPTIONAL]
	 * @param {?diary.model.SystemConfiguration} systemConfiguration The system configuration to be updated if any [OPTIONAL]
	 */
	_manager.prototype.setSystemConfiguration = function(systemConfiguration, successCallback, errorCallback) {
		this._systemConfigurationDelegate.updateSystemConfiguration(systemConfiguration, successCallback, errorCallback);
	};
	
	// return the generated system configuration manager
	return _manager;
		
})();

