diary = diary || {};
diary.service = diary.service || {};

diary.service.NewsAnnouncementSQLDelegate = (function() {
	
	/**
	 * Constructs a News and Announcements Delegate.
	 * 
	 * @constructor
	 */
	var _delegate = function() {	
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};
	
	/*
	 * Maps the news and announcements from the database to the model in the application
	 */
	function newsAnnouncementsMapper(statement) {
		var result = statement.getResult().data;
		var newsAnnouncementsList = [];
		if (result && result.length > 0) {
		
			for (var newsAnnouncementsCount = 0; newsAnnouncementsCount < result.length; newsAnnouncementsCount++)
			{ 
				var newsAnnouncements =  new diary.model.NewsAnnouncements({
				  'id' : result[newsAnnouncementsCount].Id,					
				  'title' : result[newsAnnouncementsCount].Title,				
				  'schoolId' : result[newsAnnouncementsCount].SchoolId,				
				  'body' : result[newsAnnouncementsCount].Body,				
				  'tags' : result[newsAnnouncementsCount].Tags,		
				  'All_Administrators' : result[newsAnnouncementsCount].All_Administrators,				
				  'All_Teachers' : result[newsAnnouncementsCount].All_Teachers,			
				  'All_Students' : result[newsAnnouncementsCount].All_Students,                   
				  'All_Parents': result[newsAnnouncementsCount].All_Parents,
				  'CampusIDs': result[newsAnnouncementsCount].CampusIDs,
				  'Isdeleted': result[newsAnnouncementsCount].Isdeleted,
				  'Created':convertToLocalDate(result[newsAnnouncementsCount].Created),		
				  'CreatedTime':diary.view.DiaryCalendarViewUtil.convertDateTime(convertToLocalDate(result[newsAnnouncementsCount].Updated)),	
				  'CreatedBy' : result[newsAnnouncementsCount].CreatedBy,
				  'Updated': convertToLocalDate(result[newsAnnouncementsCount].Updated),
				  'UpdatedBy': result[newsAnnouncementsCount].UpdatedBy,
				  'Status':result[newsAnnouncementsCount].Status,			
				  'type' : result[newsAnnouncementsCount].Type,
				  'readStatus' : result[newsAnnouncementsCount].readStatus
				});
			
				newsAnnouncementsList.push(newsAnnouncements);
			}
			
			return newsAnnouncementsList;
			
		}
		return null;
	}
	
	/**
	 * Gets the News And Announcements
	 */
	_delegate.prototype.getNewsAndAnnouncements = function(successCallback, errorCallback) {
		
		var user_type = "All_Students";
		if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
			user_type = "All_Teachers";
		}
		
		this._database.runQuery(
				// query
				"SELECT Id, Title, SchoolId, Body, Tags, All_Administrators, All_Teachers, All_Students, All_Parents, CampusIDs, Isdeleted, Created, CreatedBy, Updated, UpdatedBy, Status, \'News\' Type, readStatus from news where "+user_type+" = 1 UNION SELECT Id, Details, SchoolId,Details, \'\', \'\', All_Teachers, All_Students, All_Parents, CampusIDs, Isdeleted, Created, CreatedBy, Updated, UpdatedBy, 1, \'Announcement\' Type, readStatus from announcements where "+user_type+" = 1",

				// params
				{},

				// success
				function(statement) {
					successCallback(newsAnnouncementsMapper(statement));
				},

				// error
				errorCallback, "Error while trying to read news and aanouncements"
		);
	};
	
	/**
	 * Gets the Inbox items
	 */
	_delegate.prototype.getInboxItems = function(successCallback, errorCallback) {
		console.log("fetching inbox items.....");
		
		var user_type = "All_Students";
		if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
			user_type = "All_Teachers";
		}
		
		this._database.runQuery(
				// query
				"SELECT UniqueID, n.Id Id, Title, SchoolId, Body, Tags, All_Administrators, All_Teachers, "+
				"All_Students, All_Parents, CampusIDs, Isdeleted DeletedStatus, n.Updated Created, n.CreatedBy CreatedBy, n.Updated Updated, "+
				"n.UpdatedBy UpdatedBy, Status, \'News\' Type, readStatus, \'\' UserName, \'\' ReceiverName, 0 ParentId, 0 receiverId, 0 UserId, na.attachments attachments from news n left outer join news_attachments na on n.UniqueID = na.newsID where " +user_type+ " = 1 UNION SELECT UniqueID, Id, Details, "+
				"SchoolId,Details, \'\', \'\', All_Teachers, All_Students, All_Parents, CampusIDs, "+
				"Isdeleted DeletedStatus, Updated, CreatedBy, Updated, UpdatedBy, 1, \'Announcement\' Type, "+
				"readStatus, \'\', \'\', 0 ParentId, 0 receiverId, 0 UserId, \'\' from announcements where " +user_type+ " = 1 UNION SELECT UniqueID, Id, Subject, 0, Description, \'\', 0,0,0,0, "+
				"Description,DeletedStatus,Created, CreatedBy, Updated, UpdatedBy,0,Subject,0, UserName, ReceiverName, ParentId, receiverId, UserId, \'\' from messages where DeletedStatus = 0 AND UniqueID != 0",

				// params
				{},

				// success
				function(statement) {
					
					var inboxItemCollection = new diary.collection.InboxItemCollection();
					var inboxItem;
					_.forEach(statement.getResult().data, function(dbInboxItem) {
							//console.log("dbInboxItem.Id : "+dbInboxItem.UniqueID);
							//console.log("dbInboxItem.Title : "+dbInboxItem.Title);
							inboxItem = new diary.model.InboxItem(_.extend({
								'UniqueID'  : dbInboxItem.UniqueID,
								'itemId'  : dbInboxItem.Id,
								'title' : dbInboxItem.Title,
								'type' : dbInboxItem.Type,
								'description': dbInboxItem.Body,
								'sentDate': convertToLocalDate(dbInboxItem.Created),
								'showReply' : false,
								'showDelete' : false,
								'campusName' : dbInboxItem.CampusIDs,
								'author' : dbInboxItem.CreatedBy,
								'tags' : dbInboxItem.Tags,
								'userName' : dbInboxItem.UserName,
								'receiverName' : dbInboxItem.ReceiverName,
								'parentId' : dbInboxItem.ParentId,
								'userId' : dbInboxItem.UserId,
								'receiverId' : dbInboxItem.receiverId,
								'attachmentURL' : dbInboxItem.attachments,
								'deletedStatus'	: dbInboxItem.DeletedStatus,
								'readStatus'	: dbInboxItem.readStatus
								
							}));
							
							inboxItemCollection.add(inboxItem);
							inboxItem = null;
					});
					//console.log("inboxItemCollection  : "+JSON.stringify(inboxItemCollection));
					successCallback(inboxItemCollection);
					
					inboxItemCollection = null;
				},

				// error
				errorCallback, "Error while trying to read messages"
		);
	};
	
	_delegate.prototype.sendMessage = function(inboxItem, successCallback, errorCallback) {
	
			this._database.runQuery(
		                // query
		                "INSERT INTO Messages( ParentId, UserId, ReceiverId, Subject, Description, IpAddress, created, isSync, UniqueID, UserName, ReceiverName, ParentEmails) "+
						" VALUES (:ParentId, :UserId, :ReceiverId, :Subject, :Description, :IpAddress, :created, :isSync, :UniqueID, :userName, :receiverName, :parentEmails)",
		                // params
		                {
		                    'ParentId' 	: inboxItem.get('parentId'),
		                    'UserId' 	: inboxItem.get('userId'),
		                    'ReceiverId' : inboxItem.get('receiverId'),
		                    'Subject' 	: inboxItem.get('title'),
		                    'Description' : inboxItem.get('description'),
		                    'IpAddress' : inboxItem.get('IpAddress'),
		                    'created' 	: inboxItem.get('sentDate'),
		                    'isSync'	: 0,
		                    'UniqueID'	: 0,
		                    'userName' 		: inboxItem.get('userName'),
		                    'receiverName'	: inboxItem.get('receiverName'),
		                    'parentEmails' : inboxItem.get("emailId")
		                },
		
						// success
						function(statement) {
		                	var isParent = inboxItem.get('parentId');
							var eventParameters = getMessageEventParameters(inboxItem);
			      			//alert("eventParameters : " + JSON.stringify(eventParameters));
			      			if(isParent == "0")
			      			{
			      				FlurryAgent.logEvent("Message Created", eventParameters);
			      			} else {
			      				FlurryAgent.logEvent("Reply to Message", eventParameters);
			      			}
			      			
		                    successCallback(statement.getResult().lastInsertRowID);
						},
		
						// error
						errorCallback, "Error while sending Message"
				);
		
	};
	
	_delegate.prototype.getInboxitems = function(id, successCallback, errorCallback) {
        var params = {};
        var delegate = this,
        this._database.runQuery(
            // query
            "SELECT * FROM messages m WHERE m.isSync = 0 OR m.DeletedStatus = 1",
                // params
                {},

                // success
                function(statement) {
                    //console.log("success");
                     var inboxitem=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
                   successCallback(inboxitem);
                                
                },

                // error
                errorCallback, "Error while fetching inbox item"
        );
    };
	
	return _delegate;
})();
