/**
 * The manager for the generation of a calendar.  The manager will be mainly responsible to generate a calendar
 * for a particular year.
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.CalendarManager = (function() {

	/**
	 * Populates a calendar with the specified details
	 * 
	 * @param {!Rocketboots.date.Day} startDate The date from when the calendar should be populated
	 * @param {!Rocketboots.date.Day} endDate The date to when the calendar should be populated
	 * @param {!diary.model.Calendar} calendar The calendar that is to be populated
	 * @param {!diary.collection.DayOfNoteCollection} dayOfNotes The day of notes related to the year for which the calendar is being generated
	 * 
	 * @return {diary.model.Calendar} The worked out calendar
	 */
	function populateCalendar(startDate, endDate, calendar, dayOfNotes) {

		// holds the current timetable that is being used
		var timetable = null;
		// holds the index of the next time table to use from the given list of time tables
		var nextTimetableIndx = 0;
		// holds the index of the next day of note to use from the given list of day of notes
		var nextDayOfNoteIndx = 0;
		// holds the cycle length, by default it will be set to 5 (ONE WEEK)
		var cycleLength = 5;
		// The time tables related to the year for which the calendar is being generated
		var timetables = calendar.get('timetables');
		
		// holds the cycle number, this starts from 1 of course
		var cycle = 1;
		// holds the cycle day, this also starts at 1
		var cycleDay = 1;
		
		var thisDay = new Rocketboots.date.Day(startDate),
			endDay = new Rocketboots.date.Day(endDate);
		
		// for each day in the calendar generate a Calendar Day model
		while(thisDay.getKey() <= endDay.getKey()) {
			//console.log(thisDay.getKey());
			var day,									// holds the calendar day that is being generated
				dayDate = thisDay.getDate(),			// holds the date of the day
				dayDateKey = thisDay.getKey(),			// Because people shouldn't do date comparisons when time is involved
				dayCycle = null,						// holds the cycle number of the day or NULL if it is not part of the cycle
				dayCycleDay = null,						// holds the cycle day number of the day or NULL if it is not part of the cycle
				isWeekend = false,						// stores the temporary value to determine if the day is a weekend
				isGreyedOut = true,					    // determines if the day should be greyed out or not
				isCycleDay = false,						// determines if the day is a cycle day or not
				title = null;							// determines the title of the day if any
			
			var currentDayOfNotes = new diary.collection.DayOfNoteCollection(); 
			
			// first determine the timetable that is going to be used for the calendar day.
			// NOTE - a time table will be used for a period of time thus all that is required
			// to be done is to determine if a new timetable is to be used from the current date
			// that we are in
			if (nextTimetableIndx < timetables.length && 
				timetables.at(nextTimetableIndx).get('startDay') && 
				thisDay.getKey() == timetables.at(nextTimetableIndx).get('startDay').getKey()) {
				//console.log("startdate="+timetables.at(nextTimetableIndx).get('startDay').getKey())
				// keep track of the timetable that we will be using for this period
				timetable = timetables.at(nextTimetableIndx++);
				cycleLength = timetable.get('cycleLength');
				
				// reset the cycles of the timetable
				cycle = 1;
				cycleDay = 1;
							
			}

			isWeekend = (dayDate.getDay() == 6 || dayDate.getDay() == 0);
			
			// we will only consider the day as part of the cycle if we have a time table for the period
			// otherwise we will ignore everything
			if (timetable) {
				// determine if it is a cycle day.  By default a cycle day is a weekday
				// so the weekend will be considered as a non cycle day.
				isCycleDay = !isWeekend;
				// also by default if the day is a not a cycle day the day will be greyed out
				isGreyedOut = !isCycleDay;
			}
		
			// Set some flags, so if a day has multiple DayOfNotes then only set each property once			
			var setCycleDay = false,
				setIsCycleDay = false,
				setIsGreyedOut = false,
				setCycle = false,
				setTitle = false;

			// determine if we have a day of note for the day that is being generated
			// handle multiple days of note for the same day
			while (nextDayOfNoteIndx < dayOfNotes.length &&
				   dayDateKey == dayOfNotes.at(nextDayOfNoteIndx).get('date').getKey()) {

				// we have a day of note for the day so we need to process it and override
				// any default assumptions that we made before
				var dayOfNote = dayOfNotes.at(nextDayOfNoteIndx++);
				
				if (timetable) {
					// Cycles only exist inside timetables

					// determine if we need to reset the cycle day from the found day of note
                    if (dayOfNote.get('overrideCycleDay') != null
                        && (dayOfNote.get('overrideCycleDay') > 0 && dayOfNote.get('overrideCycleDay') <= cycleLength)
                        && !setCycleDay) {
						//console.log("1");
						cycleDay = dayOfNote.get('overrideCycleDay');
						setCycleDay = true;
					}

					// determine if we need to reset the cycle (i.e. week) from the found day of note
					if (dayOfNote.get('overrideCycle') != false && dayOfNote.get('overrideCycle') > 0 && !setCycle) {
					//console.log("2");
						cycle = dayOfNote.get('overrideCycle');
						setCycle = true;
						if (!setCycleDay) {
							cycleDay = 1; // reset to cycleDay 1
						}
					}
					
					// determine if we need to include the current day in the cycle or not
					if (dayOfNote.get('includeInCycle') != null && !setIsCycleDay) {
					//console.log("3");
						//As it was skipping current day information of period and subject.
						//Again reverted code.
                        isCycleDay = dayOfNote.get('includeInCycle');
						setIsCycleDay = true;
					}
				}
				
				// determine if we should grey out the day since it is not required in the calendar
				if (dayOfNote.get('isGreyedOut') != null && !setIsGreyedOut) {
					//console.log('dayDateKey : ' + dayDateKey + ' isGreyedOut : '+dayOfNote.get('isGreyedOut'));
					if(dayOfNote.get('isGreyedOut') == '1'){
						isCycleDay = false;
					} else {
						isGreyedOut = dayOfNote.get('isGreyedOut');
						setIsGreyedOut = false;
					}
				}
				
				// if we have a title we will use it for the day.  If multiple titles for the one day, then comma separate them as there's no space in the model to support multiple titles yet
				if (dayOfNote.get('title') != null) {
					if (!setTitle) {
						title = "";
					}
					var dayOfNoteTitle = dayOfNote.get('title').trim(); 
					if (!_.isEmpty(dayOfNoteTitle)) {
						title += (_.isEmpty(title) ? "" : ", ") + dayOfNoteTitle;
					}
					setTitle = true;
					currentDayOfNotes.add(dayOfNote);
				}

			}
			
			// if it is a cycle day we need to populate the cycle day details for the day
			// otherwise we will ignore them and move on
			if (isCycleDay) {
				
				dayCycle = cycle;
				dayCycleDay = cycleDay;
				//console.log("dayCycleDay="+dayCycleDay);
			}

			// finally we will generate the day
			day = new diary.model.CalendarDay({ 'day' : new Rocketboots.date.Day(thisDay),
												'cycle' : dayCycle,
												'cycleLabel' : timetable ? timetable.get('cycleLabel') : null,
												'cycleDay' : dayCycleDay,
												'cycleDayLabel' : timetable ? timetable.getCycleDayLabel(dayCycleDay) : null,
												'cycleDayShortLabel' : timetable ? timetable.getCycleDayShortLabel(dayCycleDay) : null,
												'title' : title,
												'dayOfNotes' : currentDayOfNotes,
												'isGreyedOut' : isGreyedOut,
												'timetable' : isCycleDay ? timetable : null });

			// add then add it to the calendar
			calendar.add(day);

			// Increment the cycle and day-of-cycle numbers (doesn't affect current day)
			if (!isWeekend) {
				++cycleDay;
				if (cycleDay > cycleLength) { // if the cycle reached the end, reset it
					cycle++;
					cycleDay = 1;
				}
			}

			thisDay.addDays(1);
			day = null;
		}
		timetables = null;
	}
	
	/**
	 * Constructs a Calendar Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {
		this._dayOfNoteManager = app.locator.getService(ServiceName.DAYOFNOTEMANAGER);
		this._timetableManager = app.locator.getService(ServiceName.TIMETABLEMANAGER);
	};
	
	/**
	 * Gets the calendar for a particular year
	 * 
	 * @param {!integer} year The year for which the diary is required to be generated
	 * @param {!function(diary.model.Calendar)} successCallback The function that is to be called if the calendar was generated
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_manager.prototype.getCalendar = function(year, subjects, successCallback, errorCallback) {
		//console.log('CalendarManager::getCalendar(, year, )'+year);

		// check the given parameters
		if (!year) {
			throw new Error("Year must be defined, year is not optional");
		}
		//console.log("cal manager app.model.get(ModelName.APPLICATIONSTATE) : "+JSON.stringify(app.model.get(ModelName.APPLICATIONSTATE)));
		
		var manager = this,
			appState = app.model.get(ModelName.APPLICATIONSTATE),
			startDate = appState.get('startDate'),
			endDate = appState.get('endDate');
		//console.log("cal manager new Rocketboots.date.Day(startDate) : "+new Rocketboots.date.Day(startDate)+" new Rocketboots.date.Day(endDate) : "+new Rocketboots.date.Day(endDate));
		manager._timetableManager.getTimetables(new Rocketboots.date.Day(startDate), new Rocketboots.date.Day(endDate), subjects,

				// successfully got timetables
				function(timetables) {
					//console.log('CalendarManager.getCalendar:successGetTimetable()');

					// get all the timetables for the year
					manager._dayOfNoteManager.getAllDaysOfNote(

							// successfully get the days of note
							function(daysOfNote) {
								//console.log('CalendarManager.getCalendar:successGetDayOfNotes()');

								var calendar = new diary.model.Calendar({ 'year' : year, 'timetables' : timetables });
								populateCalendar(startDate, endDate, calendar, daysOfNote);
								//console.log("console.log(calendar);");
								//console.log(calendar);
								successCallback(calendar);
								calendar = null;
							},

							// error
							errorCallback || $.noop);
				},

				// error
				errorCallback || $.noop
		);
	};
	
	// return the generated manager
	return _manager;
})();
