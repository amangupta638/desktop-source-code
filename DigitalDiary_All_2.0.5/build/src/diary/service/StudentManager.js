/**
 * The manager for Student Manager.  The manager is used to take care of all the business
 * logic related to a student profile.
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.StudentManager = (function() {
	
	/**
	 * Constructs a Student Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		this._studentProfileDelegate = app.locator.getService(ServiceName.STUDENTPROFILEDELEGATE);
	};
	
	/**
	 * Gets the profile of the student.  The details of the passed model will be updated with the details that
	 * were read from the data store.
	 * 
	 * @param {?function(diary.model.StudentProfile)} successCallback The function that is to be called on successful profile update [OPTIONAL]
	 * @param {?function(message)} errorCallback The function that is to be called on failure of the profile update [OPTIONAL]
	 * @param {?diary.model.StudentProfile} studentProfile The profile for the student to be updated if any [OPTIONAL]
	 */
	_manager.prototype.getStudentProfile = function(successCallback, errorCallback, studentProfile) {
		
		// check that a valid type was specified
		if (studentProfile && !(studentProfile instanceof diary.model.StudentProfile)) {
			throw new Error("Student Profile is not valid, profile must be of type diary.model.StudentProfile");
		}

		studentProfile = studentProfile || new diary.model.StudentProfile();
		this._studentProfileDelegate.getStudentProfile(
				function(readStudentProfile) {
					if(readStudentProfile) {
						studentProfile.set(readStudentProfile.attributes);
						successCallback(studentProfile);
					} else {
						errorCallback && errorCallback("No student profile was found, profile should be defined");
					}
				}, errorCallback || $.noop
		)
	};
	
	/**
	 * Sets the profile of the student.  The details of the passed model will be updated with the details that
	 * were read from the data store.
	 * 
	 * @param {!diary.model.StudentProfile} studentProfile The profile for the student to be updated
	 * @param {!function(diary.model.StudentProfile)} successCallback The function that is to be called on successful profile update
	 * @param {?function(message)} errorCallback The function that is to be called on failure of the profile update [OPTIONAL]
	 */
	_manager.prototype.setStudentProfile = function(studentProfile, successCallback, errorCallback) {
		// NOTE - by default the identifier of the student profile will always be 1
		//console.log("setStudentProfile mangr"+JSON.stringify(studentProfile));
		this._studentProfileDelegate.updateStudentProfile(studentProfile, successCallback, errorCallback);
		//console.log("setStudentProfile mangr2"+JSON.stringify(studentProfile));
		
	};
	
	// return the generated student manager
	return _manager;
		
})();

