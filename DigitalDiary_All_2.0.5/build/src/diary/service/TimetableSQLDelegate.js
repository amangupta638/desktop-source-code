/**
 * The local SQL delegate for Timetables.  The delegate will use SQL queries to actually
 * get data from a local database.  In future there might be another or replacement of this
 * delegate to get data from a remote service 
 * 
 * author - John Pearce
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.TimetableSQLDelegate = (function() {
	
	/**
	 * Constructs a Timetable SQL Delegate.  
	 * 
	 * @constructor
	 */
	var _delegate = function() {	
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};
	
	/*
	 * Maps the timetable from the db record set to the model in the application
	 * 
	 * @param {Object} dbTimetable The record that was read from the database
	 * 
	 * @return {!diary.model.Timetable} The model object in the application
	 */
	function timetableMapper(dbTimetable) {
		//alert("parseDayKey timetableMapper");
		// return the mapped task
		return new diary.model.Timetable({ 
									'id' : dbTimetable.timetableId,
									'name' : dbTimetable.name,
									'startDay' : Rocketboots.date.Day.parseDayKey(dbTimetable.startDate),
									'endDay' : Rocketboots.date.Day.parseDayKey(dbTimetable.EndDate),
									'cycleLength' : dbTimetable.cycleLength,
									'cycleLabel' : dbTimetable.cycleLabel,
									'cycleDays' : dbTimetable.cycleDays ? JSON.parse(dbTimetable.cycleDays) : null
								});
	}
	
	/**
	 * Maps the period from the DB record set to the model in the application.
	 * @param {Object} dbPeriod The DB record
	 * @return {!diary.model.Period} The model object
	 */
	function periodMapper(dbPeriod) {
		
		return new diary.model.Period({
			'id' : dbPeriod.periodId,
			'startTime' : (dbPeriod.startTime != null ? Rocketboots.date.Time.parseTime(dbPeriod.startTime) : null),
			'endTime' : (dbPeriod.endTime != null ? Rocketboots.date.Time.parseTime(dbPeriod.endTime) : null),
			'title' : dbPeriod.title,
			'cycleDay' : dbPeriod.cycleDay, 
			'isBreak' : dbPeriod.isBreak == 1 ? true : false
		});
	};
	
	/**
	 * Maps the class/subject combination from a DB record to the model in the application.
	 * 
	 * @param {!Object} dbClass The DB record
	 * @param {diary.collection.SubjectCollection} subjects The list of subjects that are to be used to link to the class
	 * 
	 * @return {!diary.model.Class} The model object
	 */
	function classMapper(dbClass, subjects) {
		//console.log("classMapper");
		//console.log(dbClass);
		//console.log(subjects);
		// get the subject
		var subject = subjects.get(dbClass.subjectId);
		
		// check that the subject was found
		if (subject == null) {
			throw new Error("Subject [" + dbClass.subjectId + "] was not found in specified subject collection");
		}
		//alert(dbClass.RoomName)
		return new diary.model.Class({
			'id': dbClass.id,
			'cycleDay': dbClass.cycleDay,
			'periodId': dbClass.periodId,
			'subject': subject,
			'room': dbClass.RoomName
		});
	};
	
	/**
	 * Gets the timetable which is current at a particular date.
	 * 
	 * @param {!Rocketboots.date.Day} day The day at which the returned timetable will be current
	 * @param {!diary.collection.SubjectCollection} subjects The list of subjects that are to be related to the loaded classes if necessary
	 * @param {function(?diary.model.Timetable)} successCallback The function that is to be called if all overdue tasks are fetched
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_delegate.prototype.getCurrentTimetable = function(day, subjects, successCallback, errorCallback) {
		if (!day || !(day instanceof Rocketboots.date.Day)) {
			throw new Error("day must be defined, date is not optional");
		}
		
		if (!subjects) {
			throw new Error("subjects must be defined, subjects is not optional");
		}

		var delegate = this;
		delegate._database.runQuery(
				// query
				"SELECT timetable.id AS timetableId, timetable.*, period.id AS periodId, period.* " +
						"FROM timetable LEFT OUTER JOIN period ON (timetable.id = period.timetable) " +
						"WHERE timetable.id = (" +
						"SELECT timetable.id " +
						"FROM timetable " +
						"WHERE timetable.startDate <= " + day.getKey() + " " +
						"ORDER BY timetable.startDate DESC " +
						"LIMIT 1" +
						")",

				// params
				{},

				// success
				function(statement) {
					var timetable = null, dbTimetables = statement.getResult().data;

					// if we found a timetable we will convert it to a timetable model
					if (dbTimetables != null && dbTimetables.length > 0) {

						// use the first result to construct a timetable
						timetable = timetableMapper(dbTimetables[0]);

						// go through each of the found results and convert it to a period model,
						// adding them to the timetable object
						_.forEach(dbTimetables, function(dbPeriod) {
							// store
							var period = periodMapper(dbPeriod);
							if (period !== null) {
								timetable.get('periods').add(period);
							}
						});

						// Get the list of classes
						delegate.getClassesForTimetable(timetable.id, subjects, function (classes) {

							// update timetable with the classes
							timetable.set({'classes': classes}, {'silent': true});
							// return
							successCallback(timetable);

						}, function (errorMessage) {
							errorCallback && errorCallback(errorMessage);
						});

					} else {
						// invoke the call back function with the (empty) result
						successCallback(timetable);
					}
				},

				// error
				errorCallback, "Error while trying to read the current timetable for day " + day.getKey()
		);
	};
	
	/**
	 * Gets the timetables for a given range of dates.
	 * 
	 * @param {!Rocketboots.date.Day} startDay The day from where we need to get the timetables
	 * @param {!Rocketboots.date.Day} endDay The day to where we need to get the timetables
	 * @param {!diary.collection.SubjectCollection} subjects The list of subjects that are to be related to the loaded classes if necessary
	 * @param {function(?diary.collection.TimetableCollection)} successCallback The function that is to be called if all overdue tasks are fetched
	 * @param {?function(error)} errorCallback The function that is to be called on error [OPTIONAL]
	 */
	_delegate.prototype.getTimetables = function(startDay, endDay, subjects, successCallback, errorCallback) {
		console.log("getTimetables startDay"+startDay);
		//console.log(startDay);
		console.log("getTimetables endDay"+endDay);
		//console.log(endDay);
		var delegate = this;
		
		// check that the dates were specified
		if (!startDay || !(startDay instanceof Rocketboots.date.Day)) {
			throw new Error("Start Day is required");
		}

		if (!endDay || !(endDay instanceof Rocketboots.date.Day)) {
			throw new Error("End Day is required");
		}
		
		var studentQuery = "";
		if (!(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher"))) {
			studentQuery = " period.forTeacher = 0 and ";
		}

		delegate._database.runQuery(
				// query
				"SELECT timetable.id AS timetableId, timetable.*, period.id AS periodId, period.* " +
						"FROM timetable LEFT OUTER JOIN period ON (timetable.id = period.timetable) " +
						"WHERE "+studentQuery+" timetable.id IN (" +
						"SELECT timetable.id " +
						"FROM timetable " +
					     "WHERE timetable.startDate >= " + startDay.getKey() + " " +
					     "AND timetable.startDate <= " + endDay.getKey() +
      
						") " +
						"ORDER BY timetable.id",

				// params
				{},

				// success
				function(statement) {
					var dbTimetables = statement.getResult().data,
							timetables = new diary.collection.TimetableCollection(),
							currentTimetableId = null, timetable = null;

					// go through all the records that we read from the DB
					_.forEach(dbTimetables, function(record) {

						// if we don't have an identifier for the timetable then
						// it is a new timetable record
						if (record.timetableId != currentTimetableId) {
							// create the time table
							timetable = timetableMapper(record);
							// add the created time table to the list of timetables
							timetables.add(timetable);

							// keep track of the timetable identifier
							currentTimetableId = record.timetableId;
						}

						// this is a period so will add that to the list
						var period = periodMapper(record);

						if (record.periodId !== null && period !== null) {
							timetable.get('periods').add(period);
						}

					});

					// build a list of timetable IDs
					var timetableIds = _.map(timetables.toArray(), function (timetable) {
						return timetable.id;
					}).join(",");

					delegate._database.runQuery(
							// query
							"SELECT class .* ,Room.Name as RoomName FROM class INNER JOIN Room ON class.RoomId=Room.id"+
							" JOIN ClassUser ON ClassUser.classId=class.id WHERE class.timetable IN (" + timetableIds + ")"+
							" AND ifnull (ClassUser.isDelete,'')=''",

							// params
							{},

							// success
							function (statement) {
								
								_.forEach(statement.getResult().data, function (dbClass) {
									var classMapped = classMapper(dbClass, subjects);
									if (classMapped != null) {
										timetables.get(dbClass.timetable).get('classes').add(classMapped);
									}
								});

								// return
								successCallback(timetables);

							},

							// error
							errorCallback, "Error while trying to read the timetable for period between start day [" + startDay.getKey() + "] and end day [" + endDay.getKey() + "]"
					);
				},

				// error
				errorCallback, "Error while trying to read the timetable for period between start day [" + startDay.getKey() + "] and end day [" + endDay.getKey() + "]"
		);
	};

	/**
	 * Gets the list of unique rooms in the diary
	 * 
	 * @param {!function(Array)} successCallback The function that will be executed on a successful read of the rooms
	 * @param {?function(message)} errorCallback The function that will be executed on failure of reading the rooms [OPTIONAL]
	 */
	_delegate.prototype.getUniqueRooms = function(successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"SELECT DISTINCT RoomId FROM class",

				// params
				{},

				// success
				function(statement) {
					successCallback(_.map(statement.getResult().data, function(item) {
						return item.RoomId;
					}));
				},

				// error
				errorCallback, "Error while trying to read unique rooms"
		);
	};
	
	/**
	 * Get the collection of classes assigned to a particular timetable.
	 * The class collection will be returned in the success handler.
	 * 
	 * @param {!number} timetable The ID of the timetable the classes are associated with.
	 * @param {!function(diary.collection.ClassCollection)} successCallback Called upon success, with the new class collection.
	 * @param {?function(string)} errorCallback Called upon failure, with an error message.
	 */
	_delegate.prototype.getClassesForTimetable = function (timetableId, subjects, successCallback, errorCallback) {
		
		var db = this._database;
		
		// check the parameters
		if (typeof timetableId !== "number") {
			throw new Error("TimetableId must be a number");
		}

		this._database.runQuery(
				// query
				"SELECT class .* ,Room.Name as RoomName FROM class INNER JOIN Room ON class.RoomId=Room.id WHERE class.timetable = :timetableId",

				// params
				{ 'timetableId': timetableId },

				// success
				function (statement) {
				//console.log("getClassesForTimetable");
				//console.log(statement)
					successCallback(new diary.collection.ClassCollection(_.map(statement.getResult().data, function(item){
						return classMapper(item, subjects);
					})));
				},

				// error
				errorCallback, "Error while trying to read the classes"
		);
	};
	
	/**
	 * Add a class to a timetable. The identifier of the new class is returned in the success callback.
	 * 
	 * @param {!number} timetableId Timetable id of the timetable to which the class should be added.
	 * @param {!diary.model.Class} classModel The class to be added to the timetable.
	 * @param {!function(number)} successCallback Called upon success, with the new classes ID.
	 * @param {?function(string)} errorCallback Called upon failure, with an error message.
	 */
	_delegate.prototype.addClassToTimetable = function (timetableId, classModel, successCallback, errorCallback) {
	//alert("classModel.get('Code')="+classModel.get('Code'));
		// check the parameters
		if (typeof timetableId !== "number") {
			throw new Error("Timetable must be an identifier");
		}
		console.log("classModel : "+JSON.stringify(classModel));
		console.log("classModel.get('subject').title : "+classModel.get('subject').get("title"));
		this._database.runQuery(
				// query
				"INSERT INTO class (timetable, cycleDay, periodId, subjectId, RoomId, Code, UniqueID, isSync, Name) " +
				"VALUES (:timetableId, :cycleDay, :periodId, :subjectId, :RoomId, :Code, :UniqueID, :isSync, :Name)", 

				// params
				{
					'timetableId': timetableId,
					'cycleDay': classModel.get('cycleDay'),
					'periodId': classModel.get('periodId'),
					'subjectId': classModel.get('subject').id,
					'RoomId': classModel.get('room'),
					'Code' : classModel.get('Code'),
					'UniqueID'	: classModel.get('uniqueId'),
					'isSync':'0',
					'Name'	: classModel.get('subject').get("title")
				}, 
				
				// success
				function (statement) {
					successCallback(statement.getResult().lastInsertRowID);
				}, 
				
				// error
				errorCallback, "Failed to add new class"
		);
	};
	
	/**
	 * Remove a class from the timetable to which it is associated.
	 * 
	 * @param {!number} classId The identifier of the class to remove.
	 * @param {!function()} successCallback Called upon success.
	 * @param {?function(string)} errorCallback Called upon failure with an error message.
	 */
	_delegate.prototype.removeClassFromTimetable = function (classId, successCallback, errorCallback) {
		this._database.runQuery(
				// query
				"UPDATE ClassUser SET isDelete=1 WHERE classId = :classId",

				// params
				{ 'classId' : classId },
				
				// success
				successCallback || $.noop,
				

				// error
				errorCallback, "Failed to remove class"
		);
	};
	
	/**
	 * Update a class.
	 * 
	 * @param {?number} timetableId Optionally change the timetable the class is assocaited with. If not specified, it will remain unchanged.
	 * @param {!diary.model.Class} classModel The class which will be used to update the DB.
	 * @param {!function(diary.model.Class)} successCallback Called upon success, with the class model matching the DB.
	 * @param {?function(string)} errorCallback Called upon failure with an error message.
	 */
	_delegate.prototype.updateClass = function (timetableId, classModel, successCallback, errorCallback) {
	//console.log("classModel");
	//console.log(classModel);
		
		var colourCode = classModel.get('subject').get('colour');
		
		this._database.runQuery(
				// query
				"UPDATE class " +
				"SET " + (timetableId ? "timetable = :timetableId, " : "") + 
					"cycleDay = :cycleDay, periodId = :periodId, subjectId = :subjectId, Name = :subject, RoomId = :RoomId, isSync = :isSync, Code = :Code " +
				" WHERE id = :classId",

				// params
				{
					'classId': classModel.id,
					'cycleDay': classModel.get('cycleDay'),
					'periodId': classModel.get('periodId'),
					'subjectId': classModel.get('subject').get('id'),
					'subject': classModel.get('subject').get('title'),
					'RoomId': classModel.get('room'),
					'timetableId' : timetableId,
					'isSync' : 1,
					'Code' : colourCode
				},
				
				// success
				successCallback || $.noop,
				
				
				// error
				errorCallback, "Failed to update class"
		);
	};
	
	_delegate.prototype.updateAdminClass = function (timetableId, classModel, classUniqueId, successCallback, errorCallback) {
	//console.log("classModel");
	//console.log(classModel);
		var colourCode = classModel.get('subject').get('colour');
		this._database.runQuery(
				// query
				"UPDATE class " +
				"SET " + (timetableId ? "timetable = :timetableId, " : "") + 
					"cycleDay = :cycleDay, periodId = :periodId, subjectId = :subjectId, RoomId = :RoomId, isSync = :isSync, Code = :Code " +
				" WHERE id = :classId",

				// params
				{
					'classId': classModel.id,
					'cycleDay': classModel.get('cycleDay'),
					'periodId': classModel.get('periodId'),
					'subjectId': classModel.get('subject').get('id'),
					'RoomId': classModel.get('room'),
					'timetableId' : timetableId,
					'isSync' : 1,
					'Code' : colourCode
				},
				
				// success
				successCallback || $.noop,
				
				
				// error
				errorCallback, "Failed to update class"
		);
		
		if(classUniqueId && colourCode && (classUniqueId != null) && (classUniqueId != undefined) && (colourCode != null)){
			updateAllClasses(classUniqueId,colourCode);
		}
	};


    /**
     * Gets all the (base) timetable records from the database, without mapping the periods and classese
     *
     * @param successCallback
     * @param errorCallback
     */
    _delegate.prototype.getAllTimetables = function(successCallback, errorCallback) {
        this._database.runQuery(
				// query
				"SELECT timetable.id AS timetableId, timetable.* " +
						"FROM timetable " +
						"ORDER BY timetable.id",

				// params
				{},

				// success
				function success(statement) {
					successCallback(new diary.collection.TimetableCollection(_.map(statement.getResult().data, function(item) {
						return timetableMapper(item);
					})));
				},

				// error
				errorCallback, "Failed to retrieve all timetables"
		);
    };

	/**
		Add room to room table and return roomid
	**/
	_delegate.prototype.addRoomtoTimetable = function(room, successCallback, errorCallback)
	{
		var roomName=room.toString();
		roomName=roomName.toUpperCase();
		var delegate = this;
		/** check if room exists**/
        delegate._database.runQuery(
				// query
				"SELECT * FROM Room where Name = :name",

				// params
				{
					'name' : roomName
				},

				// success
				function (statement) {
                    var values=_.map(statement.getResult().data, function(item) {
                        return item;
                    });
					//console.log("addRoomtoTimetable"+values.length);
					if(values.length>0)
					{
						successCallback(values[0].id)
					}
					else
					{
				        delegate._database.runQuery(
								// query
								"INSERT INTO Room (SchoolId, CampusId, Name, Code, UniqueID, lastupdated) " +
										"VALUES (:SchoolId, :CampusId, :Name, :Code, :UniqueID, :lastupdated )",

								// params
								{
									'SchoolId' : 44,
									'CampusId' : 2,
									'Name' : roomName,
									'Code' : "#0000",
									'UniqueID' : 12,
									'lastupdated' : getTimestampmilliseconds()
								},

								// success
								function (statement) {
									//console.log("Room inserted sucess");
									successCallback(statement.getResult().lastInsertRowID);
									//delegate.addTimetablePeriods(timetable, successCallback, errorCallback);
								},

								// error
								errorCallback, "Error adding room"
				        );
					}
					
					//delegate.addTimetablePeriods(timetable, successCallback, errorCallback);
				},

				// error
				errorCallback, "Error adding room"
        );
		/*****************/
        
	}
    /**
     * Add a timetable model to the database.
	 *
	 * Note that an ID will be automatically assigned (any ID on the model passed in will be ignored),
	 * and the timetable model passed in will have its ID adjusted to the new value.
     *
     * @param timetable The timetable to be added.
     * @param successCallback Called upon success, with the adjusted timetable model passed as argument.
     * @param errorCallback Called upon failure, with a generic error message passed as argument.
	 *
	 * @throws {Error} Errors if the arguments are not of the expected types.
     */
    _delegate.prototype.addTimetable = function(timetable, successCallback, errorCallback) {
   // console.log("addTimetable==");
   // console.log(timetable);
        if (!(timetable instanceof diary.model.Timetable)) {
            throw Error("timetable must be of type diary.model.Timetable");
        }

		var delegate = this;
        delegate._database.runQuery(
				// query
				"INSERT INTO timetable (name, startDate, cycleLength, cycleLabel, cycleDays) " +
						"VALUES (:name, :startDate, :cycleLength, :cycleLabel, :cycleDays)",

				// params
				{
					'name' : timetable.get('name'),
					'startDate' : timetable.get('startDay').getKey(),
					'cycleLength' : timetable.get('cycleLength'),
					'cycleLabel' : timetable.get('cycleLabel'),
					'cycleDays' : timetable.get('cycleDays') ? JSON.stringify(timetable.get('cycleDays')) : null
				},

				// success
				function (statement) {
					timetable.set('id', statement.getResult().lastInsertRowID);
					delegate.addTimetablePeriods(timetable, successCallback, errorCallback);
				},

				// error
				errorCallback, "Error adding timetable"
        );
    };

    /**
     * Add the periods of a timetable model to the database
     *
     * @param timetable
     * @param successCallback
     * @param errorCallback
     */
    _delegate.prototype.addTimetablePeriods = function(timetable, successCallback, errorCallback) {
        if (!(timetable instanceof diary.model.Timetable)) {
            throw Error("timetable must be of type diary.model.Timetable");
        }
        if (!(timetable.get("periods") instanceof diary.collection.PeriodCollection)) {
            throw Error("timetable.get('periods') must be of type diary.collection.PeriodCollection");
        }

		var delegate = this,
				callbacks = new Rocketboots.util.CallbackAggregator(
						// success
						function(){
							successCallback(timetable);
						},

						// error
						function() {
							errorCallback && errorCallback("Error adding timetable periods");
						});

		_.each(timetable.get('periods').toArray(), function(period) {
			callbacks.addCall();
			var successFunction = callbacks.getSuccessFunction();
			var errorFunction = callbacks.getErrorFunction();
			delegate._database.runQuery(
					// query
					"INSERT INTO period (timetable, startTime, endTime, title, cycleDay, isBreak) " +
					"VALUES (:timetable, :startTime, :endTime, :title, :cycleDay, :isBreak)",

					// params
					{
						'timetable' : timetable.get('id'),
						'startTime' : period.get('startTime').toInteger(),
						'endTime' : period.get('endTime') == null ? null : period.get('endTime').toInteger(),
						'title' : period.get("title"),
						'cycleDay' : period.get('cycleDay'),
						'isBreak' : period.get('isBreak')
					},

					// success
					function(statement) {
						period.set('id', statement.getResult().lastInsertRowID);
						successFunction();
					},

					// error
					errorFunction, "Error adding timetable period"
			);
		});
		callbacks.done();
    };

	// return the generated delegate
	return _delegate;
})();
