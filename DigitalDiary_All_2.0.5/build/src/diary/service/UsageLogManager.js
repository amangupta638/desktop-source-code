/**
 * The manager for the usage log
 *
 * author - Jane Sivieng
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.UsageLogManager = (function() {

    /**
     * Constructs a Usage Log Manager.
     *
     * @constructor
     */
    var _manager = function() {

        this._delegate = app.locator.getService(ServiceName.USAGELOGDELEGATE);
    };

    /**
     * Add a new log event
     *
     * @param usageLogEvent
     * @param successCallback
     * @param errorCallback
     */
    _manager.prototype.addUsageLog = function(usageLogEvent, successCallback, errorCallback) {
        if (!(usageLogEvent instanceof diary.model.UsageLogEvent)) {
            throw new Error("usageLogEvent must be of type diary.model.UsageLogEvent");
        }

        if (typeof(successCallback) != "function") {
            throw new Error("successCallback must be specified");
        }

        this._delegate.addUsageLogEvent(usageLogEvent, successCallback, errorCallback);

    };

    /**
     * Get the unsynced log events
     *
     * @param successCallback
     * @param errorCallback
     */
    _manager.prototype.getUnsyncedUsageLog = function(successCallback, errorCallback) {
        if (typeof(successCallback) != "function") {
            throw new Error("successCallback must be specified");
        }

        this._delegate.getUnsyncedLogEvents(successCallback, errorCallback);
    };

    /**
     * Mark the log events with the given ids as synced
     *
     * @param syncedLogEventIds
     * @param successCallback
     * @param errorCallback
     */
    _manager.prototype.markUsageLogAsSynced = function(syncedLogEventIds, successCallback, errorCallback) {

        if (!_.isArray(syncedLogEventIds)) {
            throw new Error("syncedLogEventIds must be an array");
        }

        if (syncedLogEventIds.length == 0) {
            //console.log("Empty array passed to UsageLogManager.markUsageLogAsSynced()");
            successCallback && successCallback();
        } else {
			var ids = [];
			_.forEach(syncedLogEventIds, function(eventId) {
				if (_.isNumber(eventId)) {
					ids.push(eventId);
				}
			});
			this._delegate.markUsageLogEventsAsSynced(ids, successCallback, errorCallback);
		}
    };

	/**
	 * Get any derived properties for a usage log, additional to those stored in the DB.
	 * @param usageLog The usage log entry which will be sent to the server.
	 * @return {Object} Return any additional properties in a key-value object.
	 */
	_manager.prototype.getAdditionalFields = function (usageLog) {
		if (usageLog.get('type') == diary.model.UsageLogEvent.TYPE.FEEDBACK) {
			// inject the name and email now so we have the most up to date entries from the student profile
			return {
				'studentName' : app.model.get(ModelName.STUDENTPROFILE).get('name') || undefined,
				'studentEmail' : app.model.get(ModelName.STUDENTPROFILE).get('email') || undefined
			};
		}
		return {};
	};

    // return the generated manager
    return _manager;

})();
