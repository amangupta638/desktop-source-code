/**
 * The manager for caching search results.
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SearchResultsCacheManager = (function() {

	var MAX_CACHE_SIZE = 10,
		resultsCache = new Array();
	
	/**
	 * Constructs a Search Results Cache Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		
	};
	
	/**
	 * Gets the search results if one exists for the criteria otherwise will return NULL
	 * 
	 * @param {!string} criteria The search criteria that is to be used to search the diary items
	 */
	_manager.prototype.search = function(criteria) {
		
		var result = null;
		
		// check that the required inputs where given
		if (!criteria || criteria.length == 0) {
			return (new diary.model.SearchResult({'criteria' : " "}));
		}
	
		// search the cache for the specified criteria 
		for (var x = 0 ; x < resultsCache.length ; x++) {
			if (resultsCache[x].get('criteria') == criteria) {
				// move the result to the end of the cache so that it will not be removed
				// since it is still being used
				result = resultsCache.splice(x, 1)[0];
				resultsCache.push(result);
				break;
			}
		}
		
		return result;
		
	};
	
	/**
	 * Adds a search result to the cache
	 * 
	 * @param {!diary.model.SearchResult} searchResult The search result that is to be included in the cache
	 */
	_manager.prototype.add = function(searchResult) {
		
		// check that the required inputs where given
		if (!searchResult) {
			throw new Error("Search Result must be defined, result is not optional");
		}
	
		// if the cache reached the max allowed size remove the first entry
		if (resultsCache.length == MAX_CACHE_SIZE) {
			resultsCache = _.rest(resultsCache);
		}
		
		// add the result to the cache
		resultsCache.push(searchResult);
		
	};
	
	/**
	 * Clear the search cache
	 */
	_manager.prototype.clear = function() {
		// nice and simple, create new to empty cache
		resultsCache = new Array();
		
	};
	
	// return the generated manager
	return _manager;
		
})();

