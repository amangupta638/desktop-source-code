/**
 * The manager that will install the default data
 * 
 * @author Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.DefaultDataManager = (function() {

	/**
	 * Constructs a service to manage the default data
	 * 
	 * @constructor
	 */
	var _service = function() {

		// get the database that is being used for the application
		this._database = app.locator.getService(ServiceName.APPDATABASE);

	};

	/**
	 * Will install the demo data
	 */
	_service.prototype.installDefaultData = function() {

		// the queries that is to be executed
		var queries, 
			query, 
			result;

		// write a log entry to indicate the start of the
		// installation of the default data
		//console.log("Installing Default Data");

		try {
			
			// ------------------------
			//   System Configuration
			// ------------------------

			queries = new Array();
			
			// get the system configuration
			query = "SELECT * FROM systemConfig WHERE id = 1";
			result = this._database.runQuery(query).getResult().data;
			
			if (result == null) {
				queries.push({
					'sql' : "INSERT INTO systemConfig (id) VALUES (1)",
					'rollback' : "DELETE FROM systemConfig"
				});
			}

            // ------------------------
            //   School Profile
            // ------------------------

            // get the system configuration
            query = "SELECT * FROM schoolProfile WHERE id = 1";
            result = this._database.runQuery(query).getResult().data;

            if (result == null) {
                queries.push({
                    'sql' : "INSERT INTO schoolProfile (id) VALUES (1)",
                    'rollback' : "DELETE FROM schoolProfile"
                });
            }

            // ------------------------
            //   Student Profile
            // ------------------------

            // get the system configuration
            query = "SELECT * FROM User WHERE id = 1";
            result = this._database.runQuery(query).getResult().data;

            if (result == null) {
                queries.push({
                    'sql' : "INSERT INTO User (id) VALUES (1)",
                    'rollback' : "DELETE FROM studentProfile"
                });
            }

            // execute all the queries
			this._database.runScripts(queries);
			//console.log("Successful installation of default system configuration data");

		} catch (error) {
			//console.log("Error while trying to load default data into application");
		//	console.log(error);

			// throw an error so that we can stop the loading process
			throw new Error("Error while installing default data");
		}

	};

	// return the generated service
	return _service;

})();