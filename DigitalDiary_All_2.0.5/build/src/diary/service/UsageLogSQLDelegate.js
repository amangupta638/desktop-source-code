/**
 * The local SQL delegate for Usage Log Events.
 *
 * author - Jane Sivieng
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.UsageLogSQLDelegate = (function() {

    /**
     * Constructs a Usage Log SQL Delegate.
     *
     * @constructor
     */
    var _delegate = function() {
        this._database = app.locator.getService(ServiceName.APPDATABASE);
    };

    /**
     * Insert a new usage log event into the db
     */
    _delegate.prototype.addUsageLogEvent = function(usageLogEvent, successCallback, errorCallback) {
        if(!(usageLogEvent instanceof diary.model.UsageLogEvent)) {
            throw Error("usageLogEvent must be of type diary.model.UsageLogEvent");
        }
		console.log("USSAGELOG MAC ADDRESS="+usageLogEvent.get('macAddresses'));
		this._database.runQuery(
				// query
				"INSERT INTO usageLog (time, type, detail, token, macAddresses, schoolConfiguration, version, clientVersion, synced, createdDate) " +
	            "VALUES (:time, :type, :detail, :token, :macAddresses, :schoolConfiguration, :version, :clientVersion, :synced, :createdDate)",

				// params
				{
					'time' : usageLogEvent.get('time').getTime(),
					'type' : usageLogEvent.get('type'),
					'detail' : usageLogEvent.get('detail'),
					'token' : usageLogEvent.get('token') || 'notoken',
					'macAddresses' : usageLogEvent.get('macAddresses'),
					'schoolConfiguration' : usageLogEvent.get('schoolConfiguration'),
					'version' : usageLogEvent.get('version'),
					'clientVersion' : usageLogEvent.get('clientVersion'),
					'synced' : false,
					'createdDate': getUTCTime()
				},

				// success
				function success(statement) {
					successCallback(statement.getResult().lastInsertRowID);
				},

				// error
				errorCallback, "Error inserting usageLog in db"
		);
    };
    Date.prototype.toUTCArray= function()
{
    var D= this;
    var month=D.getUTCMonth()+1
    if(month<10)
    {
        month="0"+month;
    }
    var date=D.getUTCDate();
    if(date<10)
    {
        date="0"+date;
    }
    var hours=D.getUTCHours();
    if(hours<10)
    {
        hours="0"+hours;
    }
    var mins=D.getUTCMinutes();
    if(mins<10)
    {
        mins="0"+mins;
    }
    var sec=D.getUTCSeconds();
    if(sec<10)
    {
        sec="0"+sec;
    }
    return (D.getUTCFullYear()+"-"+month+"-"+date);  
   
}
function getUTCTime()
{
    var d=new Date();
    return(d.toUTCArray());
}
function SyncUsageLogFirst(time)
{
//console.log("time="+time);
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT createdDate FROM usageLog WHERE (synced = 0 AND createdDate='"+time+"') ORDER BY createdDate DESC LIMIT 1",

                // params
                {},

                // success
                function(statement) {
                   var date= _.map(statement.getResult().data, function(item) {
                        return item.createdDate;
                    })
                   // console.log("createdDate=");
                   // console.log(date);
                    if(date.length)
                    {
                        getRecordforDate(date[0]);
                    }
                    
                },

                // error
                function(statement) {
                  // console.log("error");
                }
        );
}
function SyncUsageLog(time)
{
//console.log("time="+time);
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT createdDate FROM usageLog WHERE (synced = 0 AND createdDate>'"+time+"') ORDER BY createdDate DESC LIMIT 1",

                // params
                {},

                // success
                function(statement) {
                   var date= _.map(statement.getResult().data, function(item) {
                        return item.createdDate;
                    })
                   // console.log("createdDate=");
                   // console.log(date);
                    if(date.length)
                    {
                        getRecordforDate(date[0]);
                    }
                    
                },

                // error
                function(statement) {
                  // console.log("error");
                }
        );
}
function getRecordforDate(date)
{
var createdTime=date;
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT * FROM usageLog WHERE (synced = :synced AND createdDate='"+date+"')ORDER BY usageLog.id",

                // params
                { 'synced' : false },

                function success(statement)
                {
                     var date= _.map(statement.getResult().data, function(item) {
                        return item;
                    })
                   // console.log("getRecordforDate===");
                   // console.log(date);
                    var usagelogobj=[];
                    for(var i=0;i<date.length;i++)
                    {
                        var obj={};
                        obj.clientVersion=date[i].clientVersion;
                        obj.createdDate=date[i].createdDate;
                        obj.detail=date[i].detail;
                        obj.id=date[i].id;
                        obj.macAddresses=date[i].macAddresses;
                        obj.schoolConfiguration=date[i].schoolConfiguration;
                        obj.synced=date[i].synced;
                        obj.time=date[i].time;
                        obj.token=date[i].token;
                        obj.type=date[i].type;
                        obj.version=date[i].version;
                        
                        usagelogobj.push(obj);
                        
                    }
                    //console.log(usagelogobj);
                    var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
                     var url = SystemSettings.SERVERURL + "/addusagelogs?"; 
        data = {
        'token' :  systemConfig.get('token'), 
        'macAddresses' : systemConfig.get('macAddresses'),
        'usageLogs' : JSON.stringify(usagelogobj),
        'log_created_time':createdTime,
        'clientVersion' : systemConfig.get('versionNumber'),
		'userid' : app.model.get(ModelName.STUDENTPROFILE).get('UniqueID')
        }
        ajaxType = 'POST'; 
        
        
        //console.log("TheCloud.callService: addusagelogs url=" + url + ", type=" + ajaxType + " data=");
        //console.log(data);
                
        $.ajax({
            'url' : url,
            'type': ajaxType,
            'data': data,
            'dataType' : "json",
            'success': function(result) {
               // console.log("TheCloud.response addusagelogs: ", JSON.stringify(result));
                //successCallback(result);
                setsync(usagelogobj,createdTime);     
            }, 
            'error': function(response) {
               // console.log("addusagelogs TheCloud.error: ", JSON.stringify(response));
                //errorCallback("Error contacting server");
            }
        });   
                    
                },

                // error
                function(statement) {
                  // console.log("error");
                }
        );
}
function setsync(usagelogobj,createdTime)
{
   // console.log("usagelogobj");
   // console.log(usagelogobj)
    for(var i=0;i<usagelogobj.length;i++)
    {
        var id=usagelogobj[i].id;
        app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "UPDATE usageLog SET synced = 1 WHERE id="+id,

                // params
                {},

                // success
                function(statement) {
                  // console.log("synced updated")
                    
                },

                // error
                function(statement) {
                 //  console.log("error");
                }
        );
        
    }
    updateDateSynced(createdTime);
    
}
function updateDateSynced(time)
{
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "UPDATE UsageLogSync SET dateLastSynced = '"+time+"' WHERE id=1",

                // params
                {},

                // success
                function(statement) {
                  // console.log("UsageLogSync updated");
                   SyncUsageLog(time);
                    
                },

                // error
                function(statement) {
                  // console.log("error");
                }
        );
}

    /**
     * Get a collection of unsynced log events from the db
     */
    _delegate.prototype.getUnsyncedLogEvents = function(successCallback, errorCallback) {
    
    
    this._database.runQuery(
                // query
                "SELECT * FROM UsageLogSync",

                // params
                {},

                // success
                function(statement) {
                   var date= _.map(statement.getResult().data, function(item) {
                        return item.dateLastSynced;
                    })
                   // console.log("date===");
                   // console.log(date);
                    if(date.length)
                    {
                       SyncUsageLogFirst(date[0]);
                    }
                    else
                    {
                        app.locator.getService(ServiceName.APPDATABASE).runQuery(
               			 // query
                		"SELECT * FROM usageLog WHERE synced = :synced ORDER BY usageLog.id",

                		// params
                		{ 'synced' : false },

               		 	// success
                		function(statement) {
                		var date=getUTCTime();
                    	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                			// query
                			"INSERT INTO UsageLogSync (dateLastSynced, id) VALUES ('"+date+"',1) ",

               			 // params
                		 { },

               		  // success
               	   	   function(statement) {
                	   getRecordforDate(date);
                    
                		},

                		// error
                	  errorCallback, "Failed to get usage logs"
        			);
                    
                },

                // error
                errorCallback, "Failed to get usage logs"
        );
                    }
                },

                // error
                errorCallback, "Failed to get usage logs"
        );
		/**/
    };

    /**
     * Mark the usage log events with the given ids as synced in the db.
     */
    _delegate.prototype.markUsageLogEventsAsSynced = function(usageLogEventIds, successCallback, errorCallback) {
        if (!_.isArray(usageLogEventIds)) {
            throw Error("usageLogEventIds must be an array");
        }

        if (usageLogEventIds.length == 0) {
            successCallback();
            return;
        }

        this._database.runQuery(
				// query
				"UPDATE usageLog SET synced = 1 WHERE id IN (" + usageLogEventIds.join(',') + ")",

				// params
				{},

				// success
				successCallback || $.noop,

				// error
				errorCallback, "Failed to update usage log"
		);
    };

    // return the generated delegate
    return _delegate;

})();
