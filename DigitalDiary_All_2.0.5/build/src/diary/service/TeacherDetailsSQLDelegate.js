/**
 * The local SQL delegate for Teacher details.  The delegate will use SQL queries to actually
 * get data from a local database or from server.  In future there might be another or replacement of this
 * delegate to get data from a remote service 
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.TeacherDetailsSQLDelegate = (function() {
	
	/**
	 * Constructs a School Delegate.  
	 * 
	 * @constructor
	 */
	var _delegate = function() {	
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};
	
	// get teacher details from cloud 
	
	_delegate.prototype.getTeacherDetailsServer = function(data, successCallback, errorCallback) {
		alert("sql delegate getTeacher details cloud");
		
		if (typeof(successCallback) !== "function") {
			throw new Error("Success callback must be defined.");
		}
				
		if (typeof(errorCallback) !== "function") {
			throw new Error("Error callback must be defined.");
		}
		var newData = "";
		if(method == 'login'){
			url = "http://172.51.30.102/daisy/html/index.php/desktop/teacher?"; 
			newData = '{"user_id":"6512bd43d9caa6e02c990b0a82652dca","LastSync":0,"user_type":"Teacher"}';
			//newData = '{"email_id":"'+data.email_id+'","userId":"'+data.userId+'"}';
		}
		
		data = data || {};
		
		var ajaxType = ajaxType || 'POST'; 
		
		alert("@@@@@@@URL : "+method+" url :"+url);
	
		$.ajax({
			'url' : url,
			'type': ajaxType,
			'data': JSON.stringify(newData),
			'dataType' : "json",
			'success': function(result) {
				alert("result cloud service in success:"+JSON.stringify(result))	
			}, 
			'error': function(response) {
			//console.log("method="+method);
				errorCallback("Error contacting server");
			}
		});
	};
	
	// get teacher details from local database 
	
	_delegate.prototype.getTeacherDetails = function(successCallback, errorCallback) {
		/*
		this._database.runQuery(
				// query
				//TODO: query to get details  form local db
				// params
				{},

				// success
				function(result) {
					successCallback(result);
				},

				// error
				errorCallback, "Error while trying to read school profile"
		);
		*/
	};

    /**
     * Sets the school profile for the diary
     */
	_delegate.prototype.updateSchoolProfile = function(schoolProfile, successCallback, errorCallback) {
	
			//TODO: write query to update teacher profile
    };
	
	
	
	// return the generated school profile delegate
	return _delegate;
})();
