/**
 * The manager for handling content page form data.
 *
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.sideArticle = (function() {
	
	/**
	 * Constructs a School Delegate.  
	 * 
	 * @constructor
	 */
	var _delegate = function() {	
		this._database = app.locator.getService(ServiceName.APPDATABASE);
	};

	
	
	_delegate.prototype.getDataForArticle = function(successCallback, errorCallback) {
	
		var user_type = "allStudents";
		if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
			user_type = "allTeachers";
		}
		
		this._database.runQuery(
				// query
				"SELECT * FROM contentStorage WHERE " + user_type + " = 1",

				// params
				{},

				// success
				function(statement) {
							
					
					successCallback(new diary.collection.renderContentCollection(_.map(statement.getResult().data, function(item) {
						
                            return new diary.model.renderContentModel({
				'ContentType' : item.ContentType,
				'Title' : item.Title,
				'Content' : item.Content,
				'uniqueID' : item.uniqueID,
				'respectedPanel' :item.respectedPanel,
				'StartDate' : item.StartDate,
				'EndDate' : item.EndDate
				
			});
						})
					));
				
				},

				// error
				errorCallback, "Error while trying to read school profile"
		);
	};

    /**
     * Sets the school profile for the diary
     */

	// return the generated school profile delegate
	return _delegate;
})();

