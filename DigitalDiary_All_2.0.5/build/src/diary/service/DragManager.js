diary = diary || {};
diary.service = diary.service || {};

diary.service.DragManager = (function() {
	
	var service = function() {
		//console.log('DragManager()');
		
		_.extend(this, Backbone.Events);
		this.currentDropTarget = null;
		this.dragIdIndex = 0;
	};
	
	/*
	 * Determine who is the current Drop Target
	 */
	service.prototype.getDropTarget = function() {
		return this.currentDropTarget;
	};
	
	/*
	 * Allows a DropTarget to claim the current Drop Target
	 */
	service.prototype.setDropTarget = function(target) {
		if (this.currentDropTarget == target) return;
		this.currentDropTarget = target;

		this.trigger("change", target);
	};
	
	/*
	 * Allows a DropTarget to relinquish it's hold as the current Drop Target
	 */
	service.prototype.clearDropTarget = function(oldTarget) {
		if (this.currentDropTarget == null || this.currentDropTarget == "") return;
		if (typeof(oldTarget) !== "undefined" && this.currentDropTarget != oldTarget) return;
		
		this.currentDropTarget = null;
		this.trigger("change", null);
	};

	/*
	 * Convenience method to generate a unique dropIdentifier for a DropTarget 
	 */
	service.prototype.newDropIdentifier = function() {
		this.dragIdIndex++;
		return "drop" + this.dragIdIndex;
	};
	
	return service;
	
})();
