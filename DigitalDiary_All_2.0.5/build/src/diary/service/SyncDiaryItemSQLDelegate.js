/**
 * The local SQL delegate for Diary Items. The delegate will use SQL queries to
 * actually get data from a local database. In future there might be another or
 * replacement of this delegate to get data from a remote service
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SyncDiaryItemSQLDelegate = (function() {

    /**
     * Constructs a Diary Item SQL Delegate.
     * 
     * @constructor
     */
    var _delegate = function() {
        this._database = app.locator.getService(ServiceName.APPDATABASE);
    };

    /**
     * Searches the diary items with the given search criteria. The search is performed on the following fields
     * Title, Description, Subject, Note Entry
     * 
     * The search function will return a bunch of identifiers related to diary items so that processing of diary items
     * is done in a quick way.
     * 
     * @param {!string} criteria The search criteria that is to be used to search the diary items
     * @param {function(diaryItemIds)} successCallback
     * @param {function(message)} errorCallback [OPTIONAL]
     */
    _delegate.prototype.SyncDiaryItem = function( successCallback, errorCallback) {
        // check that the required inputs where given
       
        this._database.runQuery(
                // query
                "SELECT * FROM diaryItem",

                // params
                {  },

                // success
                function (statement) {
                    // we just want the ids from the diary items
                    successCallback(_.map(statement.getResult().data, function(item) {
                        return item.id;
                    }));
                },

                // error
                errorCallback, "Error while trying to search diary items for student"
        );
    };

   

    // return the generated delegate
    return _delegate;

})();
