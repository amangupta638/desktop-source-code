/**
 * The manager for School Management.  The manager is used to take care of all the business
 * logic related to a school profile.
 * 
 * author - Brian Bason
 */
diary = diary || {};
diary.service = diary.service || {};

diary.service.SchoolManager = (function() {
	
	/**
	 * Constructs a School Manager.  
	 * 
	 * @constructor
	 */
	var _manager = function() {	
		this._schoolProfileDelegate = app.locator.getService(ServiceName.SCHOOLPROFILEDELEGATE);
	};
	
	/**
	 * Gets the profile of the school.  The profile that is passed will be updated with the details from the data source
	 * 
	 * @param {function(diary.model.SchoolProfile)} successCallback The function that is to be called on successful profile update [OPTIONAL]
	 * @param {function(message)} errorCallback The function that is to be called on failure of the profile update [OPTIONAL]
	 * @param {diary.model.SchoolProfile} schoolProfile The profile for the school to be updated if any [OPTIONAL]
	 */
	_manager.prototype.getSchoolProfile = function(successCallback, errorCallback, schoolProfile) {
		
		// check that a valid type was specified
		//alert("check school pro instance : "+schoolProfile instanceof diary.model.SchoolProfile);
		if (schoolProfile && !(schoolProfile instanceof diary.model.SchoolProfile)) {
			throw new Error("School Profile is not valid, profile must be of type diary.model.SchoolProfile");
		}
		
		// if a school profile object was not specified
		// we will create one...
		schoolProfile = schoolProfile || new diary.model.SchoolProfile();

		// update the profile for the data store
		this._schoolProfileDelegate.getSchoolProfile(
				function success(readSchoolProfile) {
				//alert("alert : SchoolManager success: ");
					if(readSchoolProfile) {
						schoolProfile.set(readSchoolProfile.attributes);
						successCallback(schoolProfile);
					} else {
						errorCallback && errorCallback("No school profile was found, profile should be defined");
					}
				}, errorCallback || $.noop
		);
	};

    /**
     * Sets the school profile
     *
     * @param schoolProfile
     * @param successCallback
     * @param errorCallback
     */
    _manager.prototype.setSchoolProfile = function(schoolProfile, successCallback, errorCallback) {
        // check that a valid type was specified
        if (!(schoolProfile instanceof diary.model.SchoolProfile)) {
            throw new Error("School Profile is not valid, profile must be of type diary.model.SchoolProfile");
        }

        this._schoolProfileDelegate.updateSchoolProfile(schoolProfile, successCallback, errorCallback);
    };
	
	// return the generated school manager
	return _manager;
})();

