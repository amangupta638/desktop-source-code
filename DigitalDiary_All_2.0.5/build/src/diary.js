var root = this;

diary = typeof(diary) !== "undefined" ? diary : {};

app = new Rocketboots.Application({

    modelClass: diary.model.DiaryModel,
    
    popupManager: Rocketboots.popup.PopupManager,

    // loads the environment variables according to the specified
    // Environment.js file
    _loadEnvironmentVariables : function() {

        // reads in the variables from the specified
        // an application settings variable. If the variable
        // is found to be an object each key in that object will
        // also be mapped
        var loadVariables = function(variables) {

            // load all known environment variables
            for (var variableKey in variables) {

                var variableValue = variables[variableKey];

                // if the variable is an object read each
                // key too
                if (variableValue instanceof Object) {
                    // read the child variables
                    loadVariables(variableValue);
                } else {
                    // overwrite the value
                    SystemSettings[variableKey.toUpperCase()] = variableValue;
                }

            }

        };

        // if we have an environment variable object we
        // will overwrite the application settings
        if (Environment) {

            // remove the name of the environment being loaded as it is not required
            // This is a bit of a hack but it is not too bad
            if (Environment.debug) {
                //console.log("Found Environment Settings, loading variables for " + Environment.name);
            }
            delete Environment.name;

            // load all the variables for the environment
            loadVariables(Environment);

        }

    },

    mapEvents: function() {
        this.mapEvent(EventName.INITAPPLICATION, diary.controller.InitApplicationCommand);
        this.mapEvent(EventName.REGISTERAPPLICATION, diary.controller.RegisterApplicationCommand);
        this.mapEvent(EventName.STARTAPPLICATION, diary.controller.StartApplicationCommand);
        this.mapEvent(EventName.RESETAPPLICATION, diary.controller.ResetApplicationCommand);
        this.mapEvent(EventName.LOADMANIFEST, diary.controller.LoadManifestCommand);
        this.mapEvent(EventName.LOADSCHOOLPROFILE, diary.controller.LoadSchoolProfileCommand);

        this.mapEvent(EventName.ADDUSAGELOG, diary.controller.AddUsageLogCommand);
        this.mapEvent(EventName.SYNCUSAGELOG, diary.controller.SyncUsageLogCommand);

        this.mapEvent(EventName.GENERATECALENDAR, diary.controller.GenerateCalendarCommand);
        this.mapEvent(EventName.GENERATETIMETABLE, diary.controller.GenerateTimeTableCommand);
        this.mapEvent(EventName.FETCHCONTENTITEMS, diary.controller.FetchContentItemsCommand);
        this.mapEvent(EventName.RELOADMANIFEST, diary.controller.ReloadManifestCommand);
        this.mapEvent(EventName.EXITAPPLICATION, diary.controller.ExitApplicationCommand);
        this.mapEvent(EventName.FETCHDIARYITEMS, diary.controller.FetchDiaryItemsCommand);
        this.mapEvent(EventName.FETCHNEXTCONTENTPACKAGE, diary.controller.FetchNextUpdatedContentPackageCommand);
        this.mapEvent(EventName.FETCHSCHOOLPROFILE, diary.controller.FetchSchoolProfileCommand);
        this.mapEvent(EventName.FETCHSTUDENTPROFILE, diary.controller.FetchStudentProfileCommand);
        this.mapEvent(EventName.FETCHSUBJECTS, diary.controller.FetchSubjectsCommand);
        this.mapEvent(EventName.FETCHSCHOOLCONFIGURATION, diary.controller.FetchSchoolConfigurationCommand);
        this.mapEvent(EventName.FETCHSYSTEMCONFIGURATION, diary.controller.FetchSystemConfigurationCommand);
        this.mapEvent(EventName.FETCHTEACHERDETAILS, diary.controller.FetchTeacherDetailsCommand);
        //aayshu
		
		 this.mapEvent(EventName.DATACONTENT, diary.controller.datacontent);
		 this.mapEvent(EventName.RENDERCONTENT, diary.controller.renderContent);
		 this.mapEvent(EventName.PERIODSYNC, diary.controller.periodSyncData);
		 this.mapEvent(EventName.TIMETABLESYNC, diary.controller.timetableSyncData);
		
		/////
		////shruti
		this.mapEvent(EventName.CHECKSCHOOLID, diary.controller.CheckSchoolId);
		////
        
        this.mapEvent(EventName.UPDATESTUDENTPROFILE, diary.controller.UpdateStudentProfileCommand);
        this.mapEvent(EventName.CREATEDIARYITEM, diary.controller.CreateDiaryItemCommand);
        this.mapEvent(EventName.UPDATEDIARYITEM, diary.controller.UpdateDiaryItemCommand);
        this.mapEvent(EventName.DELETEDIARYITEM, diary.controller.DeleteDiaryItemCommand);
        this.mapEvent(EventName.UPDATEDIARYITEMONDROP, diary.controller.UpdateDiaryItemOnDropCommand);
        this.mapEvent(EventName.OPENATTACHMENT, diary.controller.OpenDiaryItemAttachmentCommand);
        this.mapEvent(EventName.APPENDNOTEENTRY, diary.controller.AppendNoteEntryToNoteCommand);
        this.mapEvent(EventName.UPDATESUBJECT, diary.controller.UpdateSubjectCommand);
        this.mapEvent(EventName.UPDATECLASS, diary.controller.UpdateClassCommand);
        this.mapEvent(EventName.CREATECLASS, diary.controller.CreateClassCommand);
        this.mapEvent(EventName.DELETECLASS, diary.controller.DeleteClassCommand);
        this.mapEvent(EventName.SEARCH, diary.controller.SearchCommand);
        this.mapEvent(EventName.CLEARSEARCHCACHE, diary.controller.ClearSearchCacheCommand);
        this.mapEvent(EventName.REGISTERTOKEN, diary.controller.RegisterTokenCommand);
        this.mapEvent(EventName.REGISTERUSER, diary.controller.RegisterUserCommand);
        this.mapEvent(EventName.FETCHNEWSANNOUNCEMENT, diary.controller.FetchNewsAnnouncementCommand);
        this.mapEvent(EventName.INBOXITEMS, diary.controller.FetchInboxItemsCommand);
        this.mapEvent(EventName.SENDMESSAGE, diary.controller.SendMessageCommand);
    },

    mapServices: function() {
        this.mapService("localDatabase", Rocketboots.data.air.LocalDatabase);
        this.mapService(ServiceName.APPDATABASE, diary.service.DiaryDatabase);

		this.mapService(ServiceName.SIDEARTICLE, diary.service.sideArticle);
        this.mapService(ServiceName.SCHEMAMANAGER, diary.service.DiarySchemaManager);
        this.mapService(ServiceName.DEFAULTDATAMANAGER, diary.service.DefaultDataManager);
        this.mapService(ServiceName.DEMODATAMANAGER, diary.service.DemoDataManager);
        this.mapService(ServiceName.MANIFESTMANAGER, diary.service.ManifestManager);

        this.mapService(ServiceName.CONTENTITEMFORMDELEGATE, diary.service.ContentItemFormSQLDelegate);
        this.mapService(ServiceName.DAYOFNOTEDELEGATE, diary.service.DayOfNoteSQLDelegate);
        this.mapService(ServiceName.DIARYITEMDELEGATE, diary.service.DiaryItemSQLDelegate);
        this.mapService(ServiceName.ATTACHMENTFILEDELEGATE, diary.service.AttachmentFilesystemDelegate);
        this.mapService(ServiceName.ATTACHMENTDBDELEGATE, diary.service.AttachmentSQLDelegate);
        this.mapService(ServiceName.SCHOOLPROFILEDELEGATE, diary.service.SchoolProfileSQLDelegate);
        this.mapService(ServiceName.STUDENTPROFILEDELEGATE, diary.service.StudentProfileSQLDelegate);
        this.mapService(ServiceName.NEWSANNOUNCEMENTDELEGATE, diary.service.NewsAnnouncementSQLDelegate);
        this.mapService(ServiceName.SUBJECTDELEGATE, diary.service.SubjectSQLDelegate);
        this.mapService(ServiceName.CLASSDELEGATE, diary.service.ClassSQLDelegate);
        this.mapService(ServiceName.SYSTEMCONFIGURATIONDELEGATE, diary.service.SystemConfigurationSQLDelegate);
        this.mapService(ServiceName.THECLOUDSERVICEDELEGATE, diary.service.TheCloudServiceDelegate);
        this.mapService(ServiceName.TIMETABLEDELEGATE, diary.service.TimetableSQLDelegate);
        this.mapService(ServiceName.DOWNLOADCONTENTDELEGATE, diary.service.DownloadContentDelegate, false);
        this.mapService(ServiceName.DOCUMENTDELEGATE, diary.service.DocumentSQLDelegate);
        this.mapService(ServiceName.CONTENTPACKAGEDELEGATE, diary.service.ContentPackageSQLDelegate);
        this.mapService(ServiceName.USAGELOGDELEGATE, diary.service.UsageLogSQLDelegate);
        this.mapService(ServiceName.TEACHERDETAILSDELEGATE, diary.service.TeacherDetailsSQLDelegate);
        
        this.mapService(ServiceName.CALENDARMANAGER, diary.service.CalendarManager);
        this.mapService(ServiceName.CONTENTITEMFORMMANAGER, diary.service.ContentItemFormManager);
        this.mapService(ServiceName.DAYOFNOTEMANAGER, diary.service.DayOfNoteManager);
        this.mapService(ServiceName.DIARYITEMMANAGER, diary.service.DiaryItemManager);
        this.mapService(ServiceName.CLASSMANAGER, diary.service.ClassManager);
        this.mapService(ServiceName.SUBJECTMANAGER, diary.service.SubjectManager);
        this.mapService(ServiceName.DOCUMENTMANAGER, diary.service.DocumentManager);
        this.mapService(ServiceName.CONTENTMANAGER, diary.service.DownloadableContentManager);
        this.mapService(ServiceName.NOTIFICATIONMANAGER, diary.service.NotificationManager);
        this.mapService(ServiceName.REGISTRATIONMANAGER, diary.service.RegistrationManager);        
        this.mapService(ServiceName.SCHOOLMANAGER, diary.service.SchoolManager);
        this.mapService(ServiceName.SEARCHMANAGER, diary.service.SearchManager);
        this.mapService(ServiceName.STUDENTMANAGER, diary.service.StudentManager);
        this.mapService(ServiceName.NEWSANNOUNCEMENTMANAGER, diary.service.NewsAnnouncementManager);
        this.mapService(ServiceName.SYSTEMCONFIGURATIONMANAGER, diary.service.SystemConfigurationManager);
        this.mapService(ServiceName.THECLOUDSERVICEMANAGER, diary.service.TheCloudServiceManager);      
        this.mapService(ServiceName.TIMETABLEMANAGER, diary.service.TimetableManager);
        this.mapService(ServiceName.USAGELOGMANAGER, diary.service.UsageLogManager);
        this.mapService(ServiceName.TEACHERMANAGER, diary.service.TeacherManager);

        
        this.mapService(ServiceName.DRAGMANAGER, diary.service.DragManager);
        this.mapService(ServiceName.CSVPARSER, Rocketboots.parser.CSVParser);
    },

    registerTemplates: {
        chrome:                         'templates/chrome.mustache',
        classFormEdit:                  'templates/classFormEdit.mustache',
        containerDialog:                'templates/containerDialog.mustache',
        contentNavigation:              'templates/contentNavigation.mustache',
        contentNavigationFrame:         'templates/contentNavigationFrame.mustache',
        diaryDay:                       'templates/diaryDay.mustache',
        diaryDayCellGlance:             'templates/diaryDayCellGlance.mustache',
        diaryDayCellMonth:              'templates/diaryDayCellMonth.mustache',
        diaryDayCellWeek:               'templates/diaryDayCellWeek.mustache',
        diaryDayDiaryItems:             'templates/diaryDayDiaryItems.mustache',
        diaryDaySubjectWidget:          'templates/diaryDaySubjectWidget.mustache',
        diaryDaySummaryWidget:          'templates/diaryDaySummaryWidget.mustache',
        diaryDayNoteSummaryWidget:      'templates/diaryDayNoteSummaryWidget.mustache',
        diaryGlance:                    'templates/diaryGlance.mustache',
        diaryGlanceCalendar:            'templates/diaryGlanceCalendar.mustache',
        diaryItem:                      'templates/diaryItem.mustache',
        diaryItemFormEdit:              'templates/diaryItemFormEdit.mustache',
        diaryItemFormEditAttachment:    'templates/diaryItemFormEditAttachment.mustache',
        diaryItemFormEditTypes:         'templates/diaryItemFormEditTypes.mustache',
        diaryItemsSummary:              'templates/diaryItemsSummary.mustache',
        diaryItemSummary:               'templates/diaryItemSummary.mustache',
        diaryMonth:                     'templates/diaryMonth.mustache',
        diaryMonthCalendar:             'templates/diaryMonthCalendar.mustache',
        diaryView:                      'templates/diaryView.mustache',
        diaryWeek:                      'templates/diaryWeek.mustache',
        diaryWeekCalendar:              'templates/diaryWeekCalendar.mustache',
        empowerPanel:                   'templates/empowerPanel.mustache',
        frame:                          'templates/frame.mustache',
        frameFooterNav:                 'templates/frameFooterNav.mustache',
        frontPage:                      'templates/frontPage.mustache',
        monthList:                      'templates/monthList.mustache',
        navigationItem:                 'templates/navigationItem.mustache',
        notification:                   'templates/notification.mustache',
        searchResults:                  'templates/searchResults.mustache',
        sidebar:                        'templates/sidebar.mustache',
        sidebarNoteDetail:              'templates/sidebarNoteDetail.mustache',
        sidebarNoteList:                'templates/sidebarNoteList.mustache',
        sidebarTaskList:                'templates/sidebarTaskList.mustache',
        splash:                         'templates/splash.mustache',
        studentProfileFormEdit:         'templates/studentProfileFormEdit.mustache',
        studentProfileFormView:         'templates/studentProfileFormView.mustache',
        subjectView:                    'templates/subjectView.mustache',
        subjectViewSubjectPanel:        'templates/subjectViewSubjectPanel.mustache',
        subjectViewTimetable:           'templates/subjectViewTimetable.mustache',
        subjectViewTimetableList:       'templates/subjectViewTimetableList.mustache',
        subjectViewTimetablePeriod:     'templates/subjectViewTimetablePeriod.mustache',
        tokenRegister:                  'templates/tokenRegister.mustache',
        weekList:                       'templates/weekList.mustache',
        newProfileFormEdit:				'templates/newProfileFormEdit.mustache',
        newProfileFormView:				'templates/newProfileFormView.mustache',
        
        inboxView:                  	'templates/inboxView.mustache',
        myClassesView:                  'templates/myClassesView.mustache',
        myProfileView:                  'templates/myProfileView.mustache',
        homeView:                  		'templates/homeView.mustache',
        reportView:                  	'templates/reportView.mustache',
        reportDetails:                  'templates/reportDetails.mustache',
        calendarAccessDetails:          'templates/calendarAccessDetails.mustache',
        userProfileView:				'templates/userProfileView.mustache',
        tabContainerHomeView:           'templates/tabContainerHomeView.mustache',
        glanceHomeView:					'templates/glanceHomeView.mustache',
        glanceSectionsView:				'templates/glanceSectionsView.mustache',
        glanceSectionsAssignedView:		'templates/glanceSectionsAssignedView.mustache',
        overdueItemHomeView:			'templates/overdueItemHomeView.mustache',
        overdueItemListHomeView:		'templates/overdueItemListHomeView.mustache',
        newsAnnouncementsHomeView:		'templates/newsAnnouncementsHomeView.mustache',
        loginView:						'templates/loginView.mustache',
        windowTheme:					'templates/windowTheme.mustache',
        overdueItemView:				'templates/overdueItemView.mustache',
        dueItemsView:					'templates/dueItemsView.mustache',
        assignedItemsView:				'templates/assignedItemsView.mustache',
        notificationsView:				'templates/notificationsView.mustache',
        myClassesSubjectList:			'templates/myClassesSubjectList.mustache',
       	myClassesTaskList:				'templates/myClassesTaskList.mustache',
        myClassesStudentList:			'templates/myClassesStudentList.mustache',
        popupReminder:					'templates/popupReminder.mustache',
        inboxItem:                      'templates/inboxItem.mustache',
        inboxItemsList:                  'templates/inboxItemsList.mustache',
        createReplyMessage:             'templates/createReplyMessage.mustache',
        tabContainerProfileView:		'templates/tabContainerProfileView.mustache',
        myProfileUserProfileView :      'templates/myProfileUserProfile.mustache',
        myProfileTeacherProfileView :	'templates/myProfileTeacherProfile.mustache',
        myProfileTeacherDetails :		'templates/myProfileTeacherDetails.mustache',
        myProfileParentsAndGuardianView:'templates/myProfileParentsAndGuardian.mustache',
		myProfileParentsAndGuardianDetailsView:'templates/myProfileParentsAndGuardianDetails.mustache',
        assignDiaryItemView:			'templates/assignDiaryItemView.mustache',
        assignToStudentView:			'templates/assignToStudentView.mustache',
        assignTaskDetailsView:			'templates/assignTaskDetailsView.mustache',
        eventOnDay:                     'templates/eventOnDay.mustache',
        popupSendMessage:               'templates/popupSendMessage.mustache',
        inboxViewToList:               	'templates/inboxViewToList.mustache',
        popupStudentList:               'templates/popupStudentList.mustache',
        highlightDateStyle:				'templates/highlightDateStyle.mustache',
        diaryFiltersView:				'templates/diaryFiltersView.mustache'
        
        
        
        
    },

    contextCreated: function(context) {
        // Make global reference to context
        root.context = context;
    },

    preAppInitialisation: function(app) {
        // load the settings of the application according to the
        // specified environment
        app._loadEnvironmentVariables();
    },

    appInitialized: function(app) {

        // Include the AIR Introspector, if required
        if (SystemSettings.DEBUG && SystemSettings.DEBUGCONSOLE && air.Introspector) {
            air.Introspector.register();
        }

        // trigger the application startup
        app.context.trigger(EventName.INITAPPLICATION);
    }

});