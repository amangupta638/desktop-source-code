Environment = {
    "name": "Production",
    "debug": true,
	"debugConsole": true,
	"debugConsoleLogAll": true,
    "remoting": {
		"serverUrl": "https://prime-planner.com/desktopv4",
		"serverImageUrl": "https://prime-planner.com",
		"updateUrl": "https://prime-planner.com/client/update.xml"
    },
    "splashDelay": 1000
};