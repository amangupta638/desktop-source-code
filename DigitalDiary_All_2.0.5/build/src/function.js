
var monthNames = [ "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];
var shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],

  var monthIndexArray=[];
    var yearArray=[];
  var monthArray=[];  
  var startYear="";
  var startMonth="";
  var endMonth="11";
  var endYear="";
  var isSingleYear=true;
  var emailExits=false;
  var getUserIdSuccess;
  var list;
  var versionnumber="1.4.0";
  var colorCodes = {
		
  		MAROON : "#800000",
  		RED : "#FF0000",
  		PINK : "#ff33ff",
  		PURPLE : "#800080",
  		DARKBLUE : "#000066",
  		SKYBLUE : "#87CEEB",
  		CYAN : "#1f9bd5",
  		AQUA : "#0f9392",
  		DARKGREEN : "#006400",
  		GRASSGREEN : "#5eb902",
    	DARKBROWN : "#562b01",
    	LIGHTBROWN : "#ac8152",
   		DEEPORANGE : "#ff6600",	
  		ORANGE : "#FFA500",
  		GREY : "#808080",
  		DARKGREY : "#A9A9A9",
  		WHITE : "#ffffff",
  		BLACK : "#000000"
		
  	};
	
    
    function downloadLogoUrl()
    {
    var url=SystemSettings.SERVERURL +"/downloadapplogo/?SchoolId="+schoolId;
    //var url="http://product-dynamics-dev.expensetrackingapplication.com/web/school/downloadapplogo"
    //var schoolID=app.model.get(ModelName.SCHOOLPROFILE).get('UniqueID');
    //alert("schoolID="+schoolId);
    //alert("url for school logo : "+url);
    var data={
    		"SchoolId":schoolId
    }
    //console.log("downloadLogoUrl="+url);
    if(schoolId)
    {
        $.ajax({
            'url' : url,
            'type': 'GET',
            'data': {},
            'dataType' : "json",
            'success': function(result) {
                //successCallback(result);    
                downloadFile(result.url);     
                console.log("in download url success : "+result.url);
            }, 
            'error': function(response) {
               // console.log("downloadLogoUrl TheCloud.error: ", JSON.stringify(response));
               // downloadFile(response.);  
            	// alert("in download url error : "+JSON.stringify(response));
            }
        });
       }
    }
    function downloadFile(remoteFile) 
    {
        
         // get just the filename from the remote file
         var fn = remoteFile.match(/[a-z0-9-+.]+?$/i);
            // desktop directory or app storage directory?
         var storage = air.File.applicationStorageDirectory.resolvePath("AppLogo");
         console.log("storage : "+storage);
         var localFile = storage.resolvePath("footer_icon.png");
         var stream = new air.URLStream();
         stream.addEventListener(air.Event.COMPLETE, function(e){
         var fileData = new air.ByteArray();
         stream.readBytes(fileData,0,stream.bytesAvailable);
         var fileStream = new air.FileStream();
         fileStream.openAsync(localFile, air.FileMode.WRITE);
         fileStream.writeBytes(fileData,0);
         fileStream.close();
         var contentFolder = air.File.applicationStorageDirectory.resolvePath("AppLogo");
         var fileurl="file://"+contentFolder.nativePath + "/footer_icon.png";
         $("footer .icon").html("<img src='"+fileurl+"'/>");
        });
        
        stream.addEventListener("ioError", function (error) {
         	
     	});
         
    stream.load(new air.URLRequest(remoteFile));
}

    
/* This function is to download school logo */
function downloadSchoolLogoFile() 
{    
	var schoolLogoFile = null;
	var serverURL = null;
	var remoteFile = null;
	
	var localFile = null;
	
	var storage = null;
	
	var directoryName = null;
	var fileName = null;
	
	var stream = null;
	var fileData = null;
	var fileStream = null;
	
	// Download school logo
	schoolLogoFile = app.model.get(ModelName.SCHOOLPROFILE).get('schoolLogo');
	serverURL = SystemSettings.SERVERURL.substring(0, SystemSettings.SERVERURL.lastIndexOf("/"));
	remoteFile = SystemSettings.SERVERIMAGEURL + "/"  + schoolLogoFile;
	
	directoryName = schoolLogoFile.substring(0, schoolLogoFile.lastIndexOf("/"));
	
	fileName = schoolLogoFile.substr((schoolLogoFile.lastIndexOf("/") + 1));
	
    // store file in Application Storage
    storage = air.File.applicationStorageDirectory.resolvePath(directoryName);
    //console.log("storage : "+storage);
    
    localFile = storage.resolvePath(fileName);
    stream = new air.URLStream();
    
    stream.addEventListener(air.Event.COMPLETE, function(e) {
	    fileData = new air.ByteArray();
	    stream.readBytes(fileData, 0, stream.bytesAvailable);
	    fileStream = new air.FileStream();
	    fileStream.openAsync(localFile, air.FileMode.WRITE);
	    fileStream.writeBytes(fileData, 0);
	    fileStream.close();
	    //var contentFolder = air.File.applicationStorageDirectory.resolvePath(schoolLogoFile);
	   
    });
    stream.addEventListener("ioError", function (error) {
    	
	});
    
    stream.load(new air.URLRequest(remoteFile));

}


/* This function is to download school banner */
function downloadSchoolImageFile() 
{    
	var schoolImgFile = null;
	var serverURL = null;
	var remoteFile = null;
	
	var localFile = null;
	
	var storage = null;
	
	var directoryName = null;
	var fileName = null;
	
	var stream = null;
	var fileData = null;
	var fileStream = null;
	
	// Download school logo
	schoolImgFile = app.model.get(ModelName.SCHOOLPROFILE).get('schoolImage');
	serverURL = SystemSettings.SERVERURL.substring(0, SystemSettings.SERVERURL.lastIndexOf("/"));
	
	remoteFile = SystemSettings.SERVERIMAGEURL + "/"  + schoolImgFile;
	
	directoryName = schoolImgFile.substring(0, schoolImgFile.lastIndexOf("/"));
	
	fileName = schoolImgFile.substr((schoolImgFile.lastIndexOf("/") + 1));
	
    // store file in Application Storage
    storage = air.File.applicationStorageDirectory.resolvePath(directoryName);
    //console.log("storage : "+storage);
    
    localFile = storage.resolvePath(fileName);
    stream = new air.URLStream();
    
    stream.addEventListener(air.Event.COMPLETE, function(e) {
	    fileData = new air.ByteArray();
	    stream.readBytes(fileData, 0, stream.bytesAvailable);
	    fileStream = new air.FileStream();
	    fileStream.openAsync(localFile, air.FileMode.WRITE);
	    fileStream.writeBytes(fileData, 0);
	    fileStream.close();
	    //var contentFolder = air.File.applicationStorageDirectory.resolvePath(schoolLogoFile);
	   
    });
    stream.addEventListener("ioError", function (error) {
    	
	});
    stream.load(new air.URLRequest(remoteFile));
}


/* This function is to download school background image */
function downloadSchoolBgImageFile() 
{    
	var schoolBgImgFile = null;
	var serverURL = null;
	var remoteFile = null;
	
	var localFile = null;
	
	var storage = null;
	
	var directoryName = null;
	var fileName = null;
	
	var stream = null;
	var fileData = null;
	var fileStream = null;
	
	// Download school logo
	schoolBgImgFile = app.model.get(ModelName.SCHOOLPROFILE).get('schoolBgImage');
	serverURL = SystemSettings.SERVERURL.substring(0, SystemSettings.SERVERURL.lastIndexOf("/"));
	
	remoteFile = SystemSettings.SERVERIMAGEURL + "/"  + schoolBgImgFile;
	
	directoryName = schoolBgImgFile.substring(0, schoolBgImgFile.lastIndexOf("/"));
	
	fileName = schoolBgImgFile.substr((schoolBgImgFile.lastIndexOf("/") + 1));
	
    // store file in Application Storage
    storage = air.File.applicationStorageDirectory.resolvePath(directoryName);
    //console.log("storage : "+storage);
    
    localFile = storage.resolvePath(fileName);
    stream = new air.URLStream();
    
    stream.addEventListener(air.Event.COMPLETE, function(e) {
	    fileData = new air.ByteArray();
	    stream.readBytes(fileData, 0, stream.bytesAvailable);
	    fileStream = new air.FileStream();
	    fileStream.openAsync(localFile, air.FileMode.WRITE);
	    fileStream.writeBytes(fileData, 0);
	    fileStream.close();
	    //var contentFolder = air.File.applicationStorageDirectory.resolvePath(schoolLogoFile);
	   
    });
    stream.addEventListener("ioError", function (error) {
    	
	});
    stream.load(new air.URLRequest(remoteFile));
}


function downloadUserProfileImage(imageName) 
{ 
	var fileExists = false;
	var userProfileImgFile = null;
	var serverURL = null;
	var remoteFile = null;
	
	var localFile = null;
	
	var storage = null;
	
	var directoryName = null;
	var fileName = null;
	
	var stream = null;
	var fileData = null;
	var fileStream = null;
	
	// Download user profile image
	if (imageName != null)
	{
		userProfileImgFile = imageName;
	}
	else
	{
		userProfileImgFile = app.model.get(ModelName.STUDENTPROFILE).get('profileImage');
	}
	
	if (userProfileImgFile != null)
	{
		serverURL = SystemSettings.SERVERURL.substring(0, SystemSettings.SERVERURL.lastIndexOf("/"));
		
		remoteFile = SystemSettings.SERVERIMAGEURL + "/" + UserProfileImage.USERPROFILEIMAGEDIR + "/" + userProfileImgFile;
		
		directoryName = UserProfileImage.USERPROFILEIMAGEDIR;
		
		fileName = userProfileImgFile;
		
		// store file in Application Storage
	    storage = air.File.applicationStorageDirectory.resolvePath(directoryName);
	    localFile = storage.resolvePath(fileName);
	    fileExists =  localFile ? localFile.exists : false;
	    
	    if(!fileExists){
		    stream = new air.URLStream();
		    
		    stream.addEventListener(air.Event.COMPLETE, function(e) {
			    fileData = new air.ByteArray();
			    stream.readBytes(fileData, 0, stream.bytesAvailable);
			    fileStream = new air.FileStream();
			    fileStream.openAsync(localFile, air.FileMode.WRITE);
			    fileStream.writeBytes(fileData, 0);
			    fileStream.close();
			    //var contentFolder = air.File.applicationStorageDirectory.resolvePath(schoolLogoFile);
			   
		    });
		    
		    stream.addEventListener("ioError", function (error) {
		    	//console.log("error : "+error);
		    	//app.model.get(ModelName.STUDENTPROFILE).set('profileImage', null);
		    	//updateUserProfileImage(app.model.get(ModelName.STUDENTPROFILE).get('UniqueID'));
			});
		    console.log("remoteFile : "+ JSON.stringify(remoteFile));
		    stream.load(new air.URLRequest(remoteFile));
	    }
	
	}
}

function uploadUserProfileImage(imageName) 
{ 
	
	var userProfileImgFile = null;
	var serverURL = null;
	var remoteFile = null;
	
	var localFile = null;
	
	var storage = null;
	
	var directoryName = null;
	var fileName = null;
	
	var stream = null;
	var fileData = null;
	var fileStream = null;
	
	// Download user profile image
	if (imageName != null)
	{
		userProfileImgFile = imageName;
	}
	else
	{
		userProfileImgFile = app.model.get(ModelName.STUDENTPROFILE).get('profileImage');
	}
	
	if (userProfileImgFile != null)
	{
		serverURL = SystemSettings.SERVERURL.substring(0, SystemSettings.SERVERURL.lastIndexOf("/"));
		
		remoteFile = userProfileImgFile;
		
		directoryName = UserProfileImage.USERPROFILEIMAGEDIR;
		
		var userProfileImg = userProfileImgFile.split("\\");
		imgName = userProfileImg[userProfileImg.length-1];
	
		fileName = imgName;
		
		// store file in Application Storage
	    storage = air.File.applicationStorageDirectory.resolvePath(directoryName);
	    
	    localFile = storage.resolvePath(fileName);
	    console.log("storage : "+localFile);
	    stream = new air.URLStream();
	    
	    stream.addEventListener(air.Event.COMPLETE, function(e) {
		    fileData = new air.ByteArray();
		    stream.readBytes(fileData, 0, stream.bytesAvailable);
		    fileStream = new air.FileStream();
		    fileStream.openAsync(localFile, air.FileMode.WRITE);
		    fileStream.writeBytes(fileData, 0);
		    fileStream.close();
		   // console.log("fileData : "+fileData);
		    //var contentFolder = air.File.applicationStorageDirectory.resolvePath(schoolLogoFile);
			
			app.model.get(ModelName.STUDENTPROFILE).set('profileImageToDisplay', imgName);
	    });
	    
	    stream.addEventListener("ioError", function (error) {
	    	console.log("error : "+error);
	    	app.model.get(ModelName.STUDENTPROFILE).set('profileImage', null);
	    	//updateUserProfileImage(app.model.get(ModelName.STUDENTPROFILE).get('UniqueID'));
		});
	    console.log("remoteFile 2: "+remoteFile);
	    stream.load(new air.URLRequest(remoteFile));
	
	}
}

function downloadAttachmentFile(remoteFile,displayName,tempFileName) 
{
     // get just get the filename from the remote file
     var fn = remoteFile.match(/[a-z0-9-+.]+?$/i);
     // desktop directory or app storage directory?
     var storage = air.File.applicationStorageDirectory.resolvePath("attachments");
     var timstamp = getTimestampmilliseconds();
     var localFileName = tempFileName ? tempFileName : timstamp+displayName;
     var localFile = storage.resolvePath(localFileName);
     var stream = new air.URLStream();
     stream.addEventListener(air.Event.COMPLETE, function(e){
	     var fileData = new air.ByteArray();
	     stream.readBytes(fileData,0,stream.bytesAvailable);
	     var fileStream = new air.FileStream();
	     fileStream.openAsync(localFile, air.FileMode.WRITE);
	     fileStream.writeBytes(fileData,0);
	     
	     //Releasing resources
	     fileStream.close();
	     stream.close();
	     fileData = null;
	     
	     var contentFolder = air.File.applicationStorageDirectory.resolvePath("attachments");
	     var fileurl="file://"+contentFolder.nativePath + "/"+localFileName;
	 });
     stream.addEventListener("ioError", function (error) {
     	
 	 });    
     
     try {
    	 stream.load(new air.URLRequest(remoteFile));
     }
     catch(e){
         //alert("downloadAttachmentFile error : "+JSON.stringify(e));
     }
     
     return localFileName;
}

function downloadNewsImage(remoteFile) 
{ 
	var fileExists = false;
	var stream = new air.URLStream();
	
	if (remoteFile != null)
	{
		console.log("downloadNewsImage remoteFile : " + remoteFile);
		var newsFolderMain = air.File.applicationStorageDirectory.resolvePath("NewsImages");

		if (!newsFolderMain.exists) {
			newsFolderMain.createDirectory();
		}

		var desktopMain = air.File.applicationStorageDirectory.resolvePath("NewsImages/Desktop");

		if (!desktopMain.exists) {
			desktopMain.createDirectory();
		}
		
		var fileName = remoteFile.substring(remoteFile.lastIndexOf("/") + 1);
	    var localFile = desktopMain.resolvePath(fileName);
	    fileExists =  localFile ? localFile.exists : false;
	    
	    if(!fileExists){
		    stream = new air.URLStream();
		    stream.addEventListener(air.Event.COMPLETE, function(e) {
		    	 
		    	 var fileData = new air.ByteArray();
			     stream.readBytes(fileData,0,stream.bytesAvailable);
			     var fileStream = new air.FileStream();
			     fileStream.openAsync(localFile, air.FileMode.WRITE);
			     fileStream.writeBytes(fileData,0);
			     fileStream.close();
			   
		    });
		    stream.addEventListener("ioError", function (error) {
		    	//alert("ioError ");
		    	console.log("downloadNewsImage error : ");
			});
		    try {
		    	stream.load(new air.URLRequest(remoteFile));
            }
            catch(e){
                //alert("downloadNewsImage error : "+JSON.stringify(e));
            	console.log("downloadNewsImage catch : ");
            }
	    }
	
	}
}
    
    
function getkey(value)
{
//alert("here");
var test="";
//alert("value="+value)
	$.each(colorCodes,function(key,colorvalue)
	{
	if(colorvalue==value)
	{
		test=key;
		//return key;
	}
	
	});
	return test;
} 

function getColourValue(key)
{
	var test;
	$.each(colorCodes,function(colorkey,colorvalue)
	{
		if(colorkey== key.toUpperCase())
		{
			test=colorvalue;
			return colorvalue;
		}
	});
	return test;
}

function nill(){}
function getHexCode(value)
{
	var test="";
	//alert("value="+value)
	$.each(colorCodes,function(key,colorvalue)
	{
		//console.log("colorvalue = " + colorvalue);
		if(key==value.toUpperCase())
		{
			test=colorvalue;
			//return key;
			
		}
	
	});
	return test;
} 

function isHexCode(value)
{
	if(value && value.search("#") >=0)
	 {
	   return 0;
	 }
	 else 
	 {
	   return 1;
	 }
}

function checkHexCode(value)
{
	 if(value && value.search("#") >=0)
	 {
	   return 1;
	 }
	 else 
	 {
	   return 0;
	 }
}

function GetStartDate()
{
    monthArray=[];
    yearArray=[];
    monthIndexArray=[];
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
    		"SELECT * FROM timetable ORDER BY startDate ASC LIMIT 1",

            // params
            {},

            function success(statement)
            {
              // console.log("GetStartDate success");
              // console.log(statement);
                     var diaryitem=_.map(statement.getResult().data, function(item) {
                        return item.startDate;
                    });
                  // console.log("diaryitem.length " + diaryitem.length);
                   if(diaryitem.length)
                   {
                   // console.log("GetStartDate ="+diaryitem[0]);
                    var month=diaryitem[0];
                    month=month.toString();
                    var year=month.substring(0,4);
                    startYear=year;
                    year=parseInt(year);
                    month=month.substring(4,6);
                   //console.log("GetStartDate month : "+month);
                    month=parseFloat(month)-1;
                   // console.log("month="+month)
                    startMonth=month;
                    var uperLimit=11;
                    //console.log("year="+year);
                    //year = "2014";
                    for(var i=month;i<=uperLimit;i++)
                    {
                    	
                       //	console.log(i);
                        monthArray.push(monthNames[i]);
                        monthIndexArray.push(i);
                        yearArray.push(year);
                       // console.log("The current month is " + monthNames[i]);
                        if(i==11)
                        {
                            i=-1;
                            uperLimit=12-monthArray.length-1;
                           // console.log("new uperLimit="+uperLimit);
                            year=year+1;
                        }
                    }
                   // console.log("monthArray=");
                   // console.log(monthArray);
                   // console.log("yearArray");
                   // console.log(yearArray);
                   // console.log("monthIndexArray");
                   //console.log(monthIndexArray);
                   endYear=yearArray[11];
                    now = new Date();
                    var index=parseInt(now.getMonth())
                    var dateIndex=now.getDate();
                    var month=monthNames[index];
                    var endmonthIndex=monthArray[11];
                    var endMonth=$.inArray(endmonthIndex, monthNames);
                   // console.log("index="+index);
                //console.log("month1="+month);
                var arr=diary.view.DiaryCalendarViewUtil.months;
                var monthIndex=$.inArray(month, monthArray);
               // console.log("monthindex="+monthIndex);
                diary.view.DiaryCalendarViewUtil.addMonth(monthArray);
                //alert(new Rocketboots.date.Day(yearArray[monthIndex], index, dateIndex));
                
                //Commenting below line in order to avoid issue if day view is initialized and date is something else
                //As calender will be opened from search and callout items and date might be different than today.
                //app.model.get(ModelName.APPLICATIONSTATE).set('selectedDay', new Rocketboots.date.Day(yearArray[monthIndex], index, dateIndex));
               // console.log("startYear="+startYear);
                //console.log("startMonth="+startMonth);
                var lastDayOfMonth = new Date(endYear, endMonth+1, 0);
                SystemSettings.HARDCODDEDYEAR=endYear;
                //alert("SystemSettings.HARDCODDEDYEAR="+SystemSettings.HARDCODDEDYEAR);
                var lastDay=lastDayOfMonth.getDate();
               // alert("lastDayOfMonth="+lastDayOfMonth);
               if(startYear!=endYear)
               {
               	isSingleYear=false;
               }
              //console.log("isSingleYear="+isSingleYear);
               var startDate=new Rocketboots.date.Day(parseInt(startYear), parseInt(startMonth), 1);
              var endDate=new Rocketboots.date.Day(endYear, endMonth, lastDay);
          
              	 app.model.get(ModelName.APPLICATIONSTATE).set('startDate', startDate); 
                 app.model.get(ModelName.APPLICATIONSTATE).set('endDate', endDate);
                 app.model.get(ModelName.APPLICATIONSTATE).set('year', endYear)
                // console.log("app.model.get(ModelName.APPLICATIONSTATE) : "+JSON.stringify(app.model.get(ModelName.APPLICATIONSTATE)));
                 //app.model.get(ModelName.CALENDAR).set('year', endYear);
                    }
                    
                    
            },

            // error
            function error()
            {
                
            }
        );
        return monthArray;
}

function getCurrentDate()
{
    var now=new Date();
   // alert("date="+now)
    var Day=now.getDate();
    var month=now.getMonth();
    month=monthNames[month];
    var year=now.getFullYear();
   // alert("year="+year);
    var currentdate=Day+" "+month+" "+year;
    //console.log("currentdate="+currentdate);
    return currentdate;
}


function DeleteNoteEntryMethod(noteID)
{
   // console.log("DeleteNote noteID="+noteID);
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT UniqueID  FROM diaryItem WHERE id="+noteID,

            // params
            {},

            function success(statement)
            {
              //  console.log("DeleteNote success");
                     var diaryitem=_.map(statement.getResult().data, function(item) {
                        return item.UniqueID;
                    });
                  //  console.log("noteItem=");
                    // console.log(diaryitem);
                     if(diaryitem.length)
                     {
                        DeleteNoteRecord(noteID);
                     }
                     else
                     {
                        // console.log("DeleteNote noteID="+noteID);
    						app.locator.getService(ServiceName.APPDATABASE).runQuery(
           				 // query
            					"DELETE FROM diaryItem WHERE id	= "+noteID,

            					// params
            					{},

           					 function success(statement)
          					 {
            					  // console.log("DeleteNote success");
                    			
           					 },

           				 // error
           				 function error()
          			 	{
                
           				 }
       				 );
                   }
                  
            },

            // error
            function error()
            {
                
            }
        );
        
    
}
function DeleteNoteRecord(noteID)
{
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE diaryItem SET isDelete = 1 WHERE id="+noteID,

            // params
            {},

            function success(statement)
            {
             //   console.log("DeleteNoteRecord success");
                var items = app.model.get(ModelName.DIARYITEMS);
                var item = items.get(noteID);
                items.remove(item);
                app.context.trigger(EventName.ADDUSAGELOG, diary.model.UsageLogEvent.TYPE.DIARY_ITEM_DELETED, "Note Deleted");
                app.context.trigger(EventName.CLEARSEARCHCACHE);
                //diary.view.DiaryDayView.renderNotes();
                    
            },

            // error
            function error()
            {
                
            }
        );
}
function deleteNoteEntryRecord(id)
{
     app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "DELETE FROM noteEntry WHERE id="+id,

            // params
            {},

            function success(statement)
            {
               // console.log("deleteNoteEntryRecord success");
                    
            },

            // error
            function error()
            {
                
            }
        );
}
function getDateRange(currentMonth)
{
	//console.log("getDateRange currentMonth="+currentMonth);
    var month=diary.view.DiaryCalendarViewUtil.months[currentMonth];
    //console.log("getDateRange month="+month);
    var indexOfMonth=$.inArray(month, monthNames);
  // console.log("getDateRange indexOfMonth="+indexOfMonth);
   var yearIndex=$.inArray(currentMonth, monthIndexArray);
  // console.log("yearIndex="+yearIndex);
    var year=yearArray[yearIndex];
   // console.log("getDateRange year="+year);
    
     var datetoshow=monthNames[currentMonth].substring(0,3) + ' ' + year;
    // console.log("datetoshow="+datetoshow);
     return datetoshow;
}

function glanceViewDateRange(startMonth,endMonth)
{
       /// console.log("year");
       // console.log(yearArray)
        if(endMonth>11)
        {
             endMonth=endMonth-12;
        }
       // console.log("startMonth="+startMonth+" endMonth="+endMonth);
        var startMonthName=monthArray[startMonth];
       // console.log("startMonthName="+startMonthName);
        var indexOfStartMonth=$.inArray(startMonthName, monthArray);
       // console.log("indexOfStartMonth="+indexOfStartMonth);
        
        var month=monthNames[startMonth];
        var i=$.inArray(month, monthArray);
        var startYear=yearArray[startMonth];
        
        /*=================*/
        
        var EndMonthName=diary.view.DiaryCalendarViewUtil.months[endMonth];
       // console.log("EndMonthName="+EndMonthName);
        var indexOfEndMonth=$.inArray(EndMonthName, monthArray);
        //console.log("indexOfEndMonth="+indexOfEndMonth);
        
        var month=monthNames[endMonth];
        var i=$.inArray(month, monthArray)
        var EndYear=yearArray[indexOfEndMonth];
        
        /*=======================*/
        var display=startMonthName.substring(0,3) + ' ' + startYear + ' to ' + EndMonthName.substring(0,3) + ' ' + EndYear;

        return display;
       
}
function checkUserProfile()
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT UniqueID FROM User WHERE id=1",

            // params
            {},

            function success(statement)
            {
            	var setDayView=true;
              //  console.log("checkUserProfile success");
                var values=_.map(statement.getResult().data, function(item) {
                        return item.UniqueID;
                    });
                    if(values[0])
                    {
                    	emailExits=true;
                    	app.view.mainView.switchToView("home");
                    	$("#errormessege").hide();
                    }
                    else
                    {
                    	app.view.mainView.switchToView("home");
                   		/* 	
                    	app.view.mainView.switchToView("link0");
                    	$(".studentProfileOverview .editButtonContainer .editStudentProfileLaunch").trigger("click");
		   				$(".studentProfileOverview1 .editButtonContainer .editNewStudentProfileLaunch").trigger("click");
		   				*/
                    }
                    //alert(setDayView);
                    //return setDayView;
            },

            // error
            function error()
            {
                
            }
        );
}
function showError()
{
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT UniqueID FROM User WHERE id=1",

            // params
            {},

            function success(statement)
            {
            	var setDayView=true;
               // console.log("checkUserProfile success");
                var values=_.map(statement.getResult().data, function(item) {
                        return item.UniqueID;
                    });
                    if(values[0])
                    {
                    	emailExits=true;
                    }
                    else
                    {
                    	
                    	$(".studentProfileOverview .editButtonContainer .editStudentProfileLaunch").trigger("click");
                    	$("#errormessege").hide();
                    	
                    }
                    //alert(setDayView);
                    //return setDayView;
            },

            // error
            function error()
            {
                
            }
        );

}

function saveAppInstallStatus(status)
{
	  var installStatus = 1;
	  if(status){
		  installStatus = status;
	  }
	  app.locator.getService(ServiceName.APPDATABASE).runQuery(
		     // query
		     "UPDATE schoolProfile SET appInstallStatus = :installStatus WHERE id = 1 ",
		
		     // params
		     {
		    	 'installStatus' : installStatus
		     },
		
		     // success
		     function(statement) {
		     	
		     },
		
		     // error
		     function error()
		     {
		     	
		     }
	   );
}

function getAppInstallStatus()
{
	  app.locator.getService(ServiceName.APPDATABASE).runQuery(
		     // query
		     "Select appInstallStatus From schoolProfile WHERE id = 1",
		
		     // params
		     {
		     
		     },
		
		     // success
		     function(statement) {
		     	var values=_.map(statement.getResult().data, function(item) {
                    return item;
                });
                if(values[0]){
                	app.model.get(ModelName.APPLICATIONSTATE).set('appInstallStatus', values[0].appInstallStatus == 1 ? true : false); 
                	app.model.get(ModelName.APPLICATIONSTATE).set('appInstallStatusVal', values[0].appInstallStatus);
                	
                }
		     },
		
		     // error
		     function error()
		     {
		     	
		     }
	   );
}

function deleteAttachmentRecord(Value)
{
	  app.locator.getService(ServiceName.APPDATABASE).runQuery(
		     // query
		     "DELETE FROM attachment WHERE UniqueID = :Value",
		
		     // params
		     {
		     	'Value':Value
		     },
		
		     // success
		     function(statement) {
		     	
		     },
		
		     // error
		     function error()
		     {
		     	
		     }
	   );
}
 
 
function getUserID(userinfo)
{
	var userdetail=new Object();
    userdetail=userinfo;
	//console.log("getUserID userdetail");
	//console.log(userdetail);
	var email=userinfo.email;
	var firstname=userinfo.firstname;
	var token=app.model.get(ModelName.SYSTEMCONFIGURATION).get('token');
	var pass=userinfo.password;
	var add1=userinfo.add1;
	var address2=userinfo.add2;
	var city="";
	var state=userinfo.state;
	
	//console.log("address2 = "+address2);
	//console.log("add1 = "+add1);
	//console.log("pass = "+pass);
	//console.log("firstname = "+firstname);
	//console.log("email = "+email);
	//console.log("state = "+state);
	
	var url=SystemSettings.SERVERURL +"/getuserid/?email="+email+"&token="+token+"&firstname="+firstname+"&password="+pass+"&address1="+add1+"&address2="+address2+"&city="+city+"&state="+state;
	//console.log("url="+url);
	$.ajax({
            'url' : url,
            'type': 'POST',
            'data': {           
            },
            'dataType' : "json",
            'success': function(result) {
            console.log("getUserID result=");
                console.log(result);
				var status=result.status;
				//alert("status="+status);
				if(status=="SUCCESS")
				{
                //console.log("getuserid");
            	//console.log(result);
            	var userid=result.userid;
            	var schoolid=result.schoolid;
            	//console.log("userid="+userid);
				getUserIdSuccess = true;
				
            	if(userid)
            	{
            		emailExits=true;
					saveUserID(userid);
				}
					
				if(schoolid)
            		saveschoolId(schoolid);
				
				}
				else
				{
				if(result.userid)
				{
					saveUserID(result.userid);
					saveschoolId(result.schoolid);
					
				}
				else
				{
					//alert(result.error);
				}
					getUserIdSuccess= false;
				}
            		
            	
            }, 
            'error': function(response) {
                console.log("getuserid TheCloud.error: ", JSON.stringify(response));
            }
        });
}
function saveSchoolid(schoolid)
 {
 
  app.locator.getService(ServiceName.APPDATABASE).runQuery(
     // query
     "UPDATE schoolProfile SET UniqueID=:UniqueID WHERE id = 1 ",

     // params
     {
     'UniqueID':schoolid
     },

     // success
     function(statement) {
     //console.log("school id is set");
     schoolId=schoolid;
     
     },

     // error
     function error()
     {
     console.log("Error while trying to read school profile");
     }
   );
 }

function getindex(index)
{
	var monthname=monthNames[index];
	var aIndex=$.inArray(monthname,monthArray);
	return aIndex;
}
function saveschoolId(id)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE schoolProfile SET UniqueID=:USERID WHERE id=1",

            // params
            {
            USERID:id
            },

            function success(statement)
            {
            	//console.log("School Id Set");
            	app.model.get(ModelName.SCHOOLPROFILE).set('UniqueID',id);
				if(!(app.model.get(ModelName.APPLICATIONSTATE).get('appInstallStatus'))){
					timeout();
				}
            },

            // error
            function error()
            {
               // console.log("update User id error");
            }
        );
}

function getEmpowerDetails()
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "Select Empower_Type, includeEmpower, Allow_Parent, domainName from schoolProfile WHERE id=1",

            // params
            {
            
            },

            function success(statement)
            {
            	 var values=_.map(statement.getResult().data, function(item) {
                     return item;
                 });
            	 
            	//console.log("School Id Set");
            	app.model.get(ModelName.SCHOOLPROFILE).set('includeEmpowerContent',values[0].includeEmpower);
            	app.model.get(ModelName.SCHOOLPROFILE).set('empowerType',values[0].Empower_Type);
            	app.model.get(ModelName.SCHOOLPROFILE).set('allowParents',values[0].Allow_Parent);
            	app.model.get(ModelName.SCHOOLPROFILE).set('domainName',values[0].domainName);
            	
            },

            // error
            function error()
            {
               // console.log("update User id error");
            }
        );
}


function saveUserID(userid)
{
	console.log("saveUserID="+userid);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE User SET UniqueID=:USERID WHERE id=1",

            // params
            {
            	USERID:userid
            },

            function success(statement)
            {
            	//console.log("User Id Set");
            	app.model.get(ModelName.STUDENTPROFILE).set('UniqueID',userid);
            },

            // error
            function error()
            {
               // console.log("update User id error");
            }
        );
}
function getip()
{
	
}



function parseInsertResposne(record)
{

		//console.log("parseInsertResposne"+JSON.stringify(record));
		
	    //console.log(record);
	//change for duplicate entry
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT diaryItem.id FROM diaryItem WHERE UniqueID = :uniqueid and lower(type) = :type",

            // params
            {
            'uniqueid':record.uniqueid,
            'type' : record.type.toLowerCase()
            },

            function success(statement)
            {
            //console.log("statement");
            //console.log(statement);
            	//console.log("User Id Set");
            	var id=_.map(statement.getResult().data, function(item) {
                        return item.id;
                    });
                   // console.log("parseInsertResposne");
                   // console.log("")
                   
                // console.log(id);
            	//console.log("id[0] : "+id[0]);
            	
                if(id[0] != undefined && id[0] != "")
                {
                	//console.log("id[0] :ifff "+id[0]);
	                if(record.subjectName != "null")
	                	GetSubjectId(record.subjectName,record,1);
	                else
	                	updateDiaryItemTwo(record);
	            }
                else
                {
                  //console.log("id[0] :else "+id[0]);
                  checkDesktop(record);
                }
            },

            // error
            function error()
            {
               // console.log("update User id error");
            }
        );
	record = null;
}
function GetSubjectId(subjectname,record,chkv)
{
	//console.log("GetSubjectId");
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT UniqueID FROM subject WHERE name = :name",

            // params
            
            {
            'name':subjectname
             },
            

            function success(statement)
            {
            	//console.log("GetSubjectId success");
            	var id=_.map(statement.getResult().data, function(item) {
                        return item.UniqueID;
                    });
                 //console.log("GetSubjectId : id[0]"+id[0]);
                if(id[0] != undefined && id[0] != "")
                {
                	//console.log("GetSubjectId : if ");
                	record.subjectId=id[0];
                	if (chkv){
                		//console.log("GetSubjectId : if : if");
                		updateDiaryItemTwo(record);
                	}
                	else{
                		//console.log("GetSubjectId : if : else");
                		updateDiaryItem(record);
                	}
                		
                }
                else
                {
                	//console.log("GetSubjectId : else ");
                	if (chkv){
                		//console.log("GetSubjectId : else  : if");
                		updateDiaryItemTwo(record);
                	}
                		
                	else{
                		//console.log("GetSubjectId : else  : else");
                		updateDiaryItem(record);
                	}
                		
                }
                
                id = null;
            },

            // error
            function error()
            {
                console.log("GetSubjectId error");
            }
        );
}	
function checkDesktop(record)
{
    var macAddress=app.model.get(ModelName.SYSTEMCONFIGURATION).get('macAddresses');
   // alert(record.appId);
    console.log(record.appId);
	console.log("check Desktop:"+record.appType);
	//console.log(record)
	if(record.appType=="desktop")
	{
		console.log("record.ipadd==macAddress="+(record.ipadd==macAddress));
		if(record.ipadd==macAddress)
		{
			//console.log("macaddress equals");
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id FROM diaryItem WHERE id = :appId AND appType = :appType",

            // params
            { 
            	'appId' 	: record.appId,
            	'appType' 	:record.appType
            },

            function success(statement)
            {
            	var id=_.map(statement.getResult().data, function(item) {
                        return item.id;
                    });
                //This block was replacing some wrong diary items beacuase of appID
            	console.log("id[0]="+id[0]+"record.appId : "+record.appId)
            	console.log("id[0]==record.appId "+id[0]==record.appId)
                if(id[0]==record.appId)
                {
                	console.log("in update="+id[0]);
                	GetSubjectId(record.subjectName,record,0);
                }
                else
                { 
                	console.log("insertDiaryItem=");
                	insertDiaryItem(record);
                }
            },

            // error
            function error()
            {
                //console.log("update User id error");
            }
        );
		}
		else
		{
		insertDiaryItem(record)
		}
	}
	else
	{
	//console.log("insert equals");
		insertDiaryItem(record)
	}
}
function updateDiaryItemTwo(record)
{

	var completed=record.completed;
	var assignedDate=record.assignedDate;
	if(assignedDate==null || assignedDate==0)
	{
		
	}
	else
	{
		assignedDate=new Rocketboots.date.Day(record.assignedDate).getKey();
		assignedDate=parseInt(assignedDate);
	}
	
	
	var startDay=record.startDate;
	if(startDay==null || startDay==0)
	{
		
	}
	else
	{
		startDay=new Rocketboots.date.Day(record.startDate).getKey();
		startDay=parseInt(startDay);
	}
	
	
	var dueDate=record.dueDate;
	if(dueDate==null || dueDate==0)
	{
		
	}
	else
	{
		dueDate=new Rocketboots.date.Day(record.dueDate).getKey();
		dueDate=parseInt(dueDate);
	}
	
	
	var assignedTime=record.assignedTime;
	//console.log("record.assignedTime="+record.assignedTime);
		assignedTime=formateTimes(record.assignedTime);
	//console.log("assignedTime="+assignedTime);
		
	var startTime=record.startTime;
		startTime=formateTimes(record.startTime);
	//console.log("startTime="+startTime);
	
	var endTime=record.endTime;
	//console.log("record.endTime="+record.endTime);
		endTime=formateTimes(record.endTime);
	console.log("update endTime="+endTime);
	
	var dueTime=record.dueTime;
		dueTime=formateTimes(record.dueTime);
	//console.log("assignedDate=="+assignedDate);
	//console.log("startDay=="+startDay);
	//console.log("assignedTime=="+assignedTime);
	var subjectid=record.subjectId;
	if(subjectid==0)
	{
		subjectid=null;
	}
	var itemcompleted=record.completed;
	if(itemcompleted==0)
	{
		itemcompleted=null;
	}
if(completed.toString() == "0")
{
	completed=null;
}
console.log("updateDiaryItem2");
//console.log(record.appId);
	if(record.type=="NOTE")
	{
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE diaryItem " +
            "SET hideFromDue = 0, hideFromAssigned = 0, hideFromNotifications = 0, type = :type, title = :title, description = :description, completed = :completed, progress = :progress, EstimatedHours = :EstimatedHours, "+
                "EstimatedMins = :EstimatedMins, priority = :priority, subjectId = :subjectId, ClassId = :ClassId, SchoolId = :SchoolId, UserID = :UserID, isSync = :isSync, startDay = :startDay, dueDay = :dueDay, "+
                " assignedDay = :assignedDay, created = :created " +
               // "dueTime = :dueTime, startTime = :startTime, assignedTime = :assignedTime, endTime = :endTime "+
                 " WHERE diaryItem.UniqueID  = :UniqueID and lower(type) = :typeCheck",

            // params
            {
                	'type' : record.type,
                    'title' : record.title,
                    'description' :null,
                    'completed' : null,
                    'progress' : null,
                    'EstimatedHours' : null,
                    'EstimatedMins' : null,
                    'priority' : null,
                    'subjectId' : null,
                    'ClassId' :record.classId,
                    'SchoolId' : record.schoolId,
                    'UserID' : record.userid,
                    'isSync' : 1,
                    'startDay' :assignedDate,
                    'dueDay' :null,
                    'assignedDay' : null,
                    'created' : record.created != ""?record.created:null,
                   // 'dueTime' : null,
	 				//'startTime':startTime!=0?startTime:null,
	 				//'assignedTime' :null,
	 				//'endTime':null,
	 				'UniqueID' : record.uniqueid,
	 				'typeCheck' : record.type.toLowerCase()
	 				
            },

			// success
			function success(statement)
			{
				record = null;
				//console.log("update diary Item success");
			},

			function error(error)
			{
				record = null;
				//console.log("update error");
				//console.log(error);
			}
		);
	}
	else
	{
	var completed=record.completed;
	if(completed.toString()=="0")
	{
		completed=null;
	}
	//console.log("update progress : "+record.progress);
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE diaryItem " +
            "SET hideFromDue = 0, hideFromAssigned = 0, hideFromNotifications = 0, type = :type, title = :title, description = :description, completed = :completed, progress = :progress, EstimatedHours = :EstimatedHours, "+
                "EstimatedMins = :EstimatedMins, priority = :priority, subjectId = :subjectId, ClassId = :ClassId, SchoolId = :SchoolId, UserID = :UserID, isSync = :isSync, startDay = :startDay, dueDay = :dueDay, "+
                " assignedDay = :assignedDay, created = :created, dueTime = :dueTime, startTime = :startTime, assignedTime = :assignedTime, endTime = :endTime, weight = :weight, "+
                 " onTheDay = :onTheDay, assignedtoMe = :assignedToMe, webLink = :WebLink WHERE diaryItem.UniqueID  = :UniqueID and lower(type) = :typeCheck",

            // params
            {
                	'type' : record.type,
                    'title' : record.title,
                    'description' : record.description,
                    'completed' : completed,
                    'progress' : record.progress,
                    'EstimatedHours' : record.estimatedHours,
                    'EstimatedMins' : record.estimatedMins,
                    'priority' : record.priority,
                    'subjectId' : subjectid,
                    'ClassId' : record.classId,
                    'SchoolId' : record.schoolId,
                    'UserID' : record.userid,
                    'isSync' : 1,
                    'startDay' : startDay,
                    'dueDay' : dueDate,
                    'assignedDay' : assignedDate,
                    'created' : record.created != ""?record.created:null,
                    'dueTime' : dueTime!=0? dueTime:dueTime,
	 				'startTime':startTime!=0?startTime:null,
	 				'assignedTime' :assignedTime!=0?assignedTime:assignedTime,
	 				'endTime':endTime,
	 				'UniqueID' : record.uniqueid,
	 				'weight' : record.weight,
	 				'onTheDay' : record.onTheDay,
	 				'assignedToMe' : record.assignedtoMe,
	 				'WebLink' : record.WebLink,
	 				'typeCheck' : record.type.toLowerCase()

	 				
            },

			// success
			function success(statement)
			{
				var serverattachments = record.attachmentData;
			    var diaryItemId = record.uniqueid;
	            $.each(serverattachments,function(index,serverrecord){
	            	var localDiaryItemId = getDiaryItemId(record);
					parseAttachmentRecord(serverrecord,localDiaryItemId,diaryItemId);   
	            });
	            
	            var deletedAttachments=record.deletedAttachmentdata;
	            $.each(deletedAttachments,function(index,serverrecord){
					deleteAttachmentRecord(serverrecord.Id);
	            });
	            
	            serverattachments = null;
	            diaryItemId = null;
	            deletedAttachments = null;
				record = null;
				//console.log("update diary Item success");
			},

			function error(error)
			{
				record = null;
				//console.log("update error");
				//console.log(error);
			}
		);
	}	
}

function getDiaryItemId(record)
{
	var localDiaryItemId = 0;
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id From diaryItem WHERE type = :type AND UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.uniqueid,
                'type' : record.type
            },

			// success
			function(statement) {
			    var diaryItems = _.map(statement.getResult().data, function(item) {
                    return item.id;
               	});  
               	
               	if(diaryItems.length != 0){
               		localDiaryItemId = diaryItems[0];
               	} 
               	
			},

			// error
			function error()
			{
				//alert("attachment insert Error");
			}
	);
	
	return localDiaryItemId;
}

function updateDiaryItem(record)
{

var completed=record.completed;
	var assignedDate=record.assignedDate;
	if(assignedDate==null || assignedDate==0)
	{
		
	}
	else
	{
		assignedDate=new Rocketboots.date.Day(record.assignedDate).getKey();
		assignedDate=parseInt(assignedDate);
	}
	
	
	var startDay=record.startDate;
	if(startDay==null || startDay==0)
	{
		
	}
	else
	{
		startDay=new Rocketboots.date.Day(record.startDate).getKey();
		startDay=parseInt(startDay);
	}
	
	
	var dueDate=record.dueDate;
	if(dueDate==null || dueDate==0)
	{
		
	}
	else
	{
		dueDate=new Rocketboots.date.Day(record.dueDate).getKey();
		dueDate=parseInt(dueDate);
	}
	
	
	var assignedTime=record.assignedTime;
	//console.log("record.assignedTime="+record.assignedTime);
		assignedTime=formateTimes(record.assignedTime);
	//console.log("assignedTime="+assignedTime);
		
	var startTime=record.startTime;
		startTime=formateTimes(record.startTime);
	//console.log("startTime="+startTime);
	
	var endTime=record.endTime;
	//console.log("record.endTime="+record.endTime);
		endTime=formateTimes(record.endTime);
	//console.log("update endTime="+endTime);
	
	var dueTime=record.dueTime;
		dueTime=formateTimes(record.dueTime);
	//console.log("assignedDate=="+assignedDate);
	//console.log("startDay=="+startDay);
	//console.log("assignedTime=="+assignedTime);
	var subjectid=record.subjectId;
	if(subjectid==0)
	{
		subjectid=null;
	}
	var itemcompleted=record.completed;
	if(itemcompleted==0)
	{
		itemcompleted=null;
	}
if(completed.toString() == "0")
{
	completed=null;
}
//console.log("updateDiaryItem");
//console.log(record.appId);
	if(record.type=="NOTE")
	{
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE diaryItem " +
            "SET hideFromDue = 0, hideFromAssigned = 0, hideFromNotifications = 0, type = :type, title = :title, description = :description, UniqueID = :UniqueID, completed = :completed, progress = :progress, EstimatedHours = :EstimatedHours, "+
                "EstimatedMins = :EstimatedMins, priority = :priority, subjectId = :subjectId, ClassId = :ClassId, SchoolId = :SchoolId, UserID = :UserID, isSync = :isSync, startDay = :startDay, dueDay = :dueDay, "+
                " assignedDay = :assignedDay, created = :created " +
               // "dueTime = :dueTime, startTime = :startTime, assignedTime = :assignedTime, endTime = :endTime "+
                 " WHERE id = :diaryItemId and lower(type) = :typeCheck",

            // params
            {
                	'diaryItemId' : record.appId,
                	'type' : record.type,
                    'title' : record.title,
                    'description' :null,
                    'UniqueID' : record.uniqueid,
                    'completed' : null,
                    'progress' : null,
                    'EstimatedHours' : null,
                    'EstimatedMins' : null,
                    'priority' : null,
                    'subjectId' : null,
                    'ClassId' :record.classId,
                    'SchoolId' : record.schoolId,
                    'UserID' : record.userid,
                    'isSync' : 1,
                    'startDay' :startDay,
                    'dueDay' :null,
                    'assignedDay' : null,
                    'created' : record.created != ""?record.created:null,
                   // 'dueTime' : null,
	 				//'startTime':startTime!=0?startTime:null,
	 				//'assignedTime' :null,
	 			//	'endTime':null,
	 				'typeCheck' : record.type.toLowerCase()

            },

			// success
			function success(statement)
			{
				record = null;
				//console.log("update diary Item success");
			},

			function error(error)
			{
				record = null;
				//console.log("update error");
				//console.log(error);
			}
		);
	}
	else
	{
	var completed=record.completed;
	if(completed.toString()=="0")
	{
		completed=null;
	}
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE diaryItem " +
            "SET hideFromDue = 0, hideFromAssigned = 0, hideFromNotifications = 0, type = :type, title = :title, description = :description, UniqueID = :UniqueID, completed = :completed, progress = :progress, EstimatedHours = :EstimatedHours, "+
                "EstimatedMins = :EstimatedMins, priority = :priority, subjectId = :subjectId, ClassId = :ClassId, SchoolId = :SchoolId, UserID = :UserID, isSync = :isSync, startDay = :startDay, dueDay = :dueDay, "+
                " assignedDay = :assignedDay, created = :created, dueTime = :dueTime, startTime = :startTime, assignedTime = :assignedTime, endTime = :endTime, weight = :weight, "+
                 " onTheDay = :onTheDay, assignedtoMe = :assignedtoMe, webLink = :WebLink WHERE id = :diaryItemId AND appType = :appType and lower(type) = :typeCheck",

            // params
            {
                	'diaryItemId' : record.appId,
                	'appType' : record.appType,
                	'type' : record.type,
                    'title' : record.title,
                    'description' : record.description,
                    'UniqueID' : record.uniqueid,
                    'completed' : completed,
                    'progress' : record.progress,
                    'EstimatedHours' : record.estimatedHours,
                    'EstimatedMins' : record.estimatedMins,
                    'priority' : record.priority,
                    'subjectId' : subjectid,
                    'ClassId' : record.classId,
                    'SchoolId' : record.schoolId,
                    'UserID' : record.userid,
                    'isSync' : 1,
                    'startDay' : startDay,
                    'dueDay' : dueDate,
                    'assignedDay' : assignedDate,
                    'created' : record.created != ""?record.created:null,
                    'dueTime' : dueTime!=0? dueTime:dueTime,
	 				'startTime':startTime!=0?startTime:null,
	 				'assignedTime' :assignedTime!=0?assignedTime:assignedTime,
	 				'endTime':endTime,
	 				'weight' : record.weight,
	 				'onTheDay' : record.onTheDay,
	 				'assignedtoMe' : record.assignedtoMe,
	 				'WebLink' : record.WebLink,
	 				'typeCheck' : record.type.toLowerCase()

            },

			// success
			function success(statement)
			{
				var serverattachments = record.attachmentData;
			    var diaryItemId = record.uniqueid;
	            $.each(serverattachments,function(index,serverrecord){
	            	var localDiaryItemId = getDiaryItemId(record);
					parseAttachmentRecord(serverrecord,localDiaryItemId,diaryItemId);   
	            });
	            
	            var deletedAttachments=record.deletedAttachmentdata;
	            $.each(deletedAttachments,function(index,serverrecord){
					deleteAttachmentRecord(serverrecord.Id);
	            });
				record = null;
				//console.log("update diary Item success");
			},

			function error(error)
			{
				record = null;
				//console.log("update error");
				//console.log(error);
			}
		);
	}	
}
function insertDiaryItem(record)
{
	console.log("insertDiaryItem record.subjectId")
	if(record.subjectId==0||record.subjectId=="null")
	{
		insertitemDiary(record);
	}
	else
	{
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "SELECT UniqueID FROM subject WHERE name = :name",

	            // params
            
	            {
	            'name':record.subjectName
	             },
            

	            function success(statement)
	            {
	            	//console.log("GetSubjectId");
	            	var id=_.map(statement.getResult().data, function(item) {
	                        return item.UniqueID;
	                    });
	                // console.log("subjectId="+id[0]);
	                if(id[0] != undefined && id[0] != "")
	                {
	                	record.subjectId=id[0];
	                	insertitemDiary(record);
	                }
	                else
	                {
	                	//console.log("subjectId else");
						insertitemDiary(record);
	                }
	            },

	            // error
	            function error()
	            {
	                //console.log("update User id error");
	            }
	        );
		
	}
		
	
}

function insertitemDiary(record)
{
	console.log("insertDiaryItem called"+record.uniqueid);
	//console.log(record);
	//console.log("record.type===="+record.type);
	var completed=record.completed;
	var assignedDate=record.assignedDate;
	if(assignedDate==null || assignedDate==0)
	{
		
	}
	else
	{
		//console.log("assignedDate here")
		assignedDate=new Rocketboots.date.Day(record.assignedDate).getKey();
		assignedDate=parseInt(assignedDate);
	}
	//.log("assignedDate=="+assignedDate);
	
	var startDay=record.startDate;
	if(startDay==null || startDay==0)
	{
		
	}
	else
	{
		//console.log("startDay here")
		startDay=new Rocketboots.date.Day(record.startDate).getKey();
		startDay=parseInt(startDay);
	}
	
	//console.log("startDay=="+startDay);
	
	var dueDate=record.dueDate;
	if(dueDate==null || dueDate==0)
	{
		
	}
	else
	{
	//console.log("dueDate here");
		dueDate=new Rocketboots.date.Day(record.dueDate).getKey();
		dueDate=parseInt(dueDate);
	}
	
	
	var assignedTime=record.assignedTime;
	//console.log("record.assignedTime="+record.assignedTime);
	assignedTime=formateTimes(record.assignedTime);
	//console.log("assignedTime="+assignedTime);
		
	var startTime=record.startTime;
		startTime=formateTimes(record.startTime);
	//console.log("startTime="+startTime);
	
	var endTime=record.endTime;
	//console.log("record.endTime="+record.endTime);
		endTime=formateTimes(record.endTime);
	//console.log("insert endTime="+endTime);
	
	var dueTime=record.dueTime;
		dueTime=formateTimes(record.dueTime);
	
	
	//console.log("assignedTime=="+assignedTime);
	var subjectid=record.subjectId;
	if(subjectid==0)
	{
		subjectid=null;
	}
	var itemcompleted=record.completed;
	if(itemcompleted==0)
	{
		itemcompleted=null;
	}
	var locked=null;
	if(record.type=="EVENT")
	{
		if(record.createdBy!=UserId)
		{
			locked=1;
		}
	}
	
if(completed.toString() == "0")
{
	completed=null;
}
var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	if(record.type=="NOTE")
	{
		console.log("locked 1"+locked);
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO diaryItem (type, title, description, UniqueID, completed, progress, EstimatedHours, creatorUserName, "+
            "EstimatedMins, priority, subjectId, ClassId, SchoolId, UserID, isSync, startDay, startTime, endTime, dueDay, "+
            "assignedDay, created, assignedTime, dueTime, lastupdated, isDelete, createdBy, locked, appType, type_id, type_name)" +
            " VALUES (:type, :title, :description, :UniqueID, :completed, :progress, :EstimatedHours, :creatorUserName, :EstimatedMins, "+
            ":priority, :subjectId, :ClassId, :SchoolId, :UserID, :isSync, :startDay, :startTime, :endTime, :dueDay, "+
            ":assignedDay, :created, :assignedTime, :dueTime, :lastupdated, :isDelete, :createdBy, :locked, :appType, :type_id, :type_name)",
		
            // params
            {
                'type' : record.type,
                'title' : record.title,
                'description' : "",
                'UniqueID' : record.uniqueid,
                'completed' : null,
                'progress' : null,
                'EstimatedHours' :null,
                'EstimatedMins' : null,
                'priority' : null,
                'subjectId' : null,
                'ClassId' :record.classId,
                'SchoolId' : record.schoolId,
                'UserID' : record.userid,
                'isSync' : 1,
                'startDay' :assignedDate,
                'startTime': record.OnTheDay == 1 ? null : assignedTime,
                'endTime':null,
                'dueDay' :null,
                'assignedDay':null,
                'created' : record.created != '' ? record.created:null,
                'assignedTime' :null,
                'dueTime' : null,
				'lastupdated':record.lastupdate != '' ? record.lastupdate:null,
				'isDelete':0,
				'createdBy':record.createdBy,
				'locked':locked,
                'appType' : record.appType,
                'type_id' : record.type_id,
                'type_name' : record.type_name,
                'creatorUserName' : record.creatorUserName
            },

			// success
			function(statement) {
							record = null;
			                //console.log(statement);
			               // console.log(statement.getResult().lastInsertRowID);
			              
			},

			// error
			function error()
			{
				record = null;
				//console.log("Error while trying to create diary item");
			}
	);
	}
	else
	{
	/*console.log("else");
	console.log("record.title="+record.title);
	console.log(" record.uniqueid=="+ record.uniqueid);
	console.log("record.description="+record.description);
	console.log("completed="+completed);
	console.log("record.progress="+record.progress);
	console.log("record.estimatedHours="+record.estimatedHours);
	console.log("record.estimatedMins="+record.estimatedMins);
	console.log("parseInt(record.priority)="+ record.priority!="null"?parseInt(record.priority):null);
	console.log("record.subjectId="+record.subjectId);
	console.log("record.classId="+record.classId);
	console.log("record.userid="+record.userid);
	console.log("record.startDate="+record.startDate);
	console.log("new Rocketboots.date.Day(record.startDate).getKey()="+new Rocketboots.date.Day(record.startDate).getKey());
	console.log("record.dueDate="+record.dueDate);
	console.log("new Rocketboots.date.Day(record.dueDate).getKey()="+new Rocketboots.date.Day(record.dueDate).getKey());
	console.log("record.assignedDate="+record.assignedDate);
	console.log("record.created="+record.created);
	console.log("record.assignedTime="+record.assignedTime);
	console.log("record.dueTime="+record.dueTime);
	console.log("record.startTime="+record.startTime);*/
		console.log("insertDiaryItem called else "+record.uniqueid);
		console.log("locked 2"+locked);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO diaryItem (type, title, description, UniqueID, completed, progress, EstimatedHours, webLink,"+
            "EstimatedMins, priority, subjectId, ClassId, SchoolId, UserID, isSync, startDay, startTime, endTime, dueDay, "+
            "assignedDay, created, assignedTime, dueTime, lastupdated, isDelete, createdBy, locked, appType, assignedtoMe, weight, onTheDay, type_id, type_name)" +
            " VALUES (:type, :title, :description, :UniqueID, :completed, :progress, :EstimatedHours, :WebLink, :EstimatedMins, "+
            ":priority, :subjectId, :ClassId, :SchoolId, :UserID, :isSync, :startDay, :startTime, :endTime, :dueDay, "+
            ":assignedDay, :created, :assignedTime, :dueTime, :lastupdated, :isDelete, :createdBy, :locked, :appType, :assignedtoMe, :Weight, :onTheDay, :type_id, :type_name)",
		
            // params
            {
                'type' : record.type,
                'title' : record.title,
                'description' : record.description,
                'UniqueID' : record.uniqueid,
                'completed' : itemcompleted,
                'progress' : record.progress,
                'EstimatedHours' : record.estimatedHours,
                'EstimatedMins' : record.estimatedMins,
                'priority' : record.priority != null ? record.priority : null,
                'subjectId' : subjectid,
                'ClassId' : record.classId,
                'SchoolId' : record.schoolId,
                'UserID' : record.userid,
                'isSync' : 1,
                'startDay' :startDay,
                'startTime':startTime!=0?startTime:null,
                'endTime':endTime,
                'dueDay' :dueDate,
                'assignedDay':assignedDate,
                'created' : record.created != '' ? record.created:null,
                'assignedTime' :assignedTime!=0?assignedTime:assignedTime,
                'dueTime' : dueTime!=0? dueTime:dueTime,
				'lastupdated':record.lastupdate != '' ? record.lastupdate:null,
				'isDelete':0,
				'createdBy':record.createdBy,
				'locked':locked,
                'appType' : record.appType,
                'assignedtoMe' : record.assignedtoMe,
                'Weight' : record.weight,
                'onTheDay' : record.onTheDay,
                'WebLink' : record.WebLink,
                'type_id' : record.type_id,
                'type_name' : record.type_name
            },

			// success
			function(statement) {
				
				app.locator.getService(ServiceName.ATTACHMENTDBDELEGATE).GetAttachments(record.uniqueid);
			    var serverattachments = record.attachmentData;
			    var localDiaryItemId = statement.getResult().lastInsertRowID;
			    var diaryItemId = record.uniqueid;
	            $.each(serverattachments,function(index,serverrecord){
					parseAttachmentRecord(serverrecord,localDiaryItemId,diaryItemId);   
	            });
			    record = null;     
			},

			// error
			function error()
			{
				record = null;
				//console.log("Error while trying to create diary item");
			}
	);
	}
	
}

function parseAttachmentRecord(record,localDiaryItemId,diaryItemId)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id From attachment WHERE serverPath = :serverPath AND diaryItemId = :localDiaryItemId",
		
            // params
            {
                'serverPath' : record.FilePath,
                'localDiaryItemId' : localDiaryItemId
            },

			// success
			function(statement) {
			    var attachments = _.map(statement.getResult().data, function(item) {
                    return item.id;
               	});  
               	
               	if(attachments.length == 0){
               		insertDiaryAttachmentRecord(record,localDiaryItemId,diaryItemId);
               	} 
               	
			},

			// error
			function error()
			{
				//alert("attachment insert Error");
			}
	);
}

function insertDiaryAttachmentRecord(record,localDiaryItemId,diaryItemId)
{
	var localFileName = "";
	if(record.FileType.toUpperCase() == ".GDOC"){
		localFileName = getTimestampmilliseconds() + record.DisplayName;
	} else {
		localFileName = downloadAttachmentFile(record.FilePath,record.DisplayName);
	}
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            
            "INSERT INTO attachment (UniqueID, diaryItemId, originalFilePath, displayName, fileType, localFileName, serverPath, isSync, fileSize) " +
			"VALUES (:UniqueID, :diaryItemId, :originalFilePath, :displayName, :fileType, :localFileName, :serverPath, :isSync, :fileSize)",

			// params
			{
				'UniqueID' : record.Id,
				'diaryItemId': localDiaryItemId,
				'originalFilePath': 'path',
				'displayName': record.DisplayName,
				'fileType': record.FileType,
				'localFileName': localFileName,
				'serverPath' : record.FilePath,
				'isSync': 1,
				'fileSize' : record.fileSize ? parseInt(record.fileSize) : 0
			},

			// success
			function(statement) {
				//alert("attachment inserted successfully");
			},

			// error
			function error()
			{
				//alert("attachment insert Error");
			}
	);
	
	
}

/* 
function updateDiaryAttachmentRecord(record,localDiaryItemId,diaryItemId)
{
	//alert('updateDiaryAttachmentRecord called');
	//var localFileName = downloadAttachmentFile(record.FilePath,record.DisplayName);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE attachment SET isSync = :isSync, serverPath = :serverPath, " + 
            " WHERE diaryItemId = :diaryItemId",
		
            // params
            {
                'isSync' : 1,
                'serverPath' : record.FilePath,
                'diaryItemId' : localDiaryItemId
            },

			// success
			function(statement) {
				//alert("attachment updated successfully");
			              
			},

			// error
			function error()
			{
				//alert("attachment update Error");
			}
	);
	
} */

function updatenoteentry()
{
 	hideSplash=1;
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT di.id as diaryid,di.type,di.title,di.description,di.created,di.assignedday,"+
            "di.assignedtime,di.dueday,di.duetime,di.completed,di.subjectid,di.startday,di.starttime,"+
            "di.endtime,di.messagefrom,di.messagefromtext,di.lastupdated,di.UniqueID, nt.id,"+
            "nt.text, nt.noteId, nt.lastupdated, nt.UniqueID as NoteEntryUnique, nt.timestamp "+
            "FROM diaryItem as di INNER JOIN noteEntry nt ON nt.noteId = di.id WHERE di.isSync=0",

                // params
                { },

				// success
				function(statement) {
				                //console.log(statement);
				                var note=_.map(statement.getResult().data, function(item) {
                        			return item;
                   				 });
                   				 //console.log("note");
                   				 SyncNoteEntry(note);
                   				 note=null;
				               
				},

				// error
				function error()
				{
					console.log("Error while trying to create diary item");
				}
		);
}
function SyncNoteEntry(note)
{
	var values=note;
	var noteArray=[];
	var dataArray=[];
    var macAddress=app.model.get(ModelName.SYSTEMCONFIGURATION).get('macAddresses');
    var UserId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
   // var schoolID=app.model.get(ModelName.SCHOOLPROFILE).get('UniqueID');
    //var lastupdated=GetLastSync("noteEntry");
    var timstamp=getTimestampmilliseconds();
	for (i=0;i<values.length;i++)
             {
             //console.log("======"+i+"=========");
            // console.log(values[i].UniqueID);
             //console.log("values[i].id="+values[i].id);
            // console.log("values[i].noteId="+values[i].noteId);
             //console.log("values[i].text="+values[i].text);
            // console.log("values[i].created="+values[i].created);
            // console.log("values[i].lastupdated="+values[i].lastupdated);
             
             
             /*=== Note Obj===*/
             var note=new Object();
                note.appId=values[i].diaryid
                note.type=values[i].type
                note.userid=UserId
                note.schoolid=schoolId
                note.startday=ChangeDate(values[i].startDay);
                note.starttime=changeTime(values[i].startTime)
                note.endtime=changeTime(values[i].endTime)
                note.subjectid=values[i].subjectId
                note.title=values[i].title
                note.description=values[i].description
                note.assignedDay=ChangeDate(values[i].assignedDay);
                note.assignedtime=changeTime(values[i].assignedTime)
                note.dueDay=ChangeDate(values[i].dueDay);
                note.completed=values[i].completed
                note.appType="desktop"
                note.ipadd=macAddress
                note.created=values[i].created
                note.lastupdate=values[i].lastupdated
                note.uniqueid=values[i].UniqueID
                note.endday=values[i].endDay
                note.duetime=values[i].dueTime
             /*=====*/
                var obj1=new Object();
                obj1.uniqueid=values[i].NoteEntryUnique;
                obj1.ipadd=macAddress
                obj1.userid=UserId;
                obj1.appId=values[i].id;
                obj1.appType="desktop";
                obj1.noteId="";
                obj1.text=values[i].text;
                obj1.createddate=values[i].timestamp;
                obj1.lastupdate=values[i].lastupdated
                obj1.localnoteid=values[i].diaryid
                dataArray.push(obj1);
                noteArray.push(note);
                
            }
            getNoteForSync(noteArray,dataArray,timstamp);
            noteArray=null;
            dataArray=null;
            note=null;
            values=null;
            timstamp=null;
}

function getNoteForSync(noteArray,dataArray,timstamp)
{
	
	var macAddress=app.model.get(ModelName.SYSTEMCONFIGURATION).get('macAddresses');
    var UserId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
			
               // "SELECT noteEntry.*,diaryItem.created,diaryItem.id AS noteLocal from noteEntry INNER JOIN diaryItem ON diaryItem.UniqueID=noteEntry.noteUniqueId WHERE noteEntry.isSynch=0 ",
				"SELECT noteEntry.* ,diaryItem.id AS noteLocal from noteEntry INNER JOIN diaryItem ON diaryItem.UniqueID=noteEntry.noteUniqueId AND upper(diaryItem.type) = 'NOTE' WHERE noteEntry.isSynch=0 ",

                // params
                { },

				// success
				function(statement) {
				               // console.log(statement);
				                var noteEntry=_.map(statement.getResult().data, function(item) {
                        			return item;
                   				 });
                   				// console.log("noteEntry");
                   				 
                   				 $.each(noteEntry,function(i,noteEntryv)
                   				 {
                   				 	console.log("noteEntry creted date : "+noteEntryv.timestamp+" for entry : "+noteEntryv.text);
                   				 	var obj1=new Object();
                					obj1.uniqueid=noteEntryv.UniqueID;
               						 obj1.ipadd=macAddress
                					 obj1.userid=UserId;
                					 obj1.appId=noteEntryv.id;
               						 obj1.appType="desktop";
                					 obj1.noteId=noteEntryv.noteUniqueId;
               						 obj1.text=noteEntryv.text;
               						 obj1.createddate=noteEntryv.timestamp;
               						 obj1.lastupdate=noteEntryv.lastupdated
               						 obj1.localnoteid=noteEntryv.noteLocal
               						 dataArray.push(obj1);
                   				 });
                   				 
                   				 
                var url =SystemSettings.SERVERURL+"/insertupdatenotentry";
            // alert("NoteEntryUpdate = "+NoteEntryUpdate);
             data =  {
	           "lastSynch":NoteEntryUpdate,
	           "userId":UserId,
	           "notes":noteArray,
	           "details":dataArray     
            };
			if(!UserId)
			{
				return;
			}
			console.log("TheCloud.callService insertupdatenotentry: url=" + url + " data=" + JSON.stringify(data)+"readyStateChanged : "+readyStateChanged);
			var xhr = new XMLHttpRequest();
			xhr.open("POST",url, true);
			xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');
			xhr.setRequestHeader('Accept', 'text/javascript, text/html, application/xml, text/xml, */*');
			xhr.onreadystatechange = readyStateChanged;
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhr.send(JSON.stringify(data));	
			function readyStateChanged(){
				
				if(xhr.readyState === 4)
				{
						//console.log("readyStateChanged");
						//console.log(xhr);
						var result=xhr.responseText;
						if(result)
						{
								data = null;
								url = null;
								noteEntry = null;
								dataArray = null;
								
								result=JSON.parse(result);
								//console.log("insertupdatenotentry="+JSON.stringify(result));
								 var details=result.details;
		           				 var notes=result.notes;
		           				 var deleted=result.deleted_notes;
		           				 var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
		           				 $.each(notes,function(i,j)
					            {
					            	parseInsertResposne(j);
					            	//to insert note users in diaryItemUser Table 
					            	
					            	var assignedUsers = j.assignUserId;
					            	//console.log("insertupdatenotentry j.assignUserId : "+JSON.stringify(j.assignUserId));
					            	//console.log("insertupdatenotentry assignedUsers : "+JSON.stringify(assignedUsers));
					 	            $.each(assignedUsers, function(index, assignedUser) {
					 						insertDiaryItemUsers(assignedUser, currentUserId);
					 				});
					 				
					            });
		           				 
            
					            $.each(details,function(i,j)
					            {
					            	ParseNoteEntry(j);
					            });
					            $.each(deleted,function(i,j)
					            {
					            	DeleteNote(j);
					            });
					            updateLastUpdate("noteEntry",timstamp);
					            
					            if(deleted.length > 0 || details.length > 0 || notes.length > 0){
					            	fetchDairyItems();
					           	 	
					           	 	details=null;
					           	 	notes=null;
					           	 	deleted=null;
					            }
			           	}
						
						result = null;
						xhr.onreadystatechange = null;
						xhr.abort();
				}
			}
				               
				},

				// error
				function error()
				{
					console.log("Error while trying to create diary item");
				}
		);
		noteArray=null;
		dataArray=null;
		
}



function ParseNoteEntry(record)
{
	//console.log("ParseNoteEntry");
	//console.log(record);
	CheckId(record);
}

function CheckId(record)
{
	console.log("CheckId");
    //console.log(record);
    if(record.appId!="0")
    {
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id FROM noteEntry WHERE UniqueID="+record.uniqueid,
            // params
            { },

            function success(statement)
            {
            	//console.log("User Id Set");
            	var id=_.map(statement.getResult().data, function(item) {
                        return item.id;
                    });
               //console.log("id==");
               //console.log("id[0] : "+id[0])
                if(id[0])
                {
                	updateNoteEntry(record);
                }
                else
                {
                	CheckNoteEntryType(record)
                }
             },

            // error
            function error()
            {
                console.log("update User id error");
            }
        );
    }
    else
    {
    		app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id FROM noteEntry WHERE UniqueID="+record.uniqueid,

            // params
            { },

            function success(statement)
            {
            	//console.log("User Id Set");
            	var id=_.map(statement.getResult().data, function(item) {
                        return item.id;
                    });
               // console.log("id==");
               // console.log(id[0]==record.appId)
                if(id[0])
                {
                	updateNoteEntry(record);
                }
                else
                {
                	CheckNoteEntryType(record)
                }
             },

            // error
            function error()
            {
                console.log("update User id error");
            }
        );
    }
}
function CheckNoteEntryType(record)
{
	var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
	 var macAddress=systemConfig.get('macAddresses');
	if(record.appType=="desktop")
	{
		
		if(record.ipadd==macAddress)
		{
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
			"SELECT id FROM noteEntry WHERE isSynch = 0 AND id="+record.appId,
            //"SELECT id FROM noteEntry WHERE UniqueID="+record.uniqueid,

            // params
            { },

            function success(statement)
            {
            	var id=_.map(statement.getResult().data, function(item) {
                        return item.id;
                    });
                if(id[0])
                {
                	updateNoteEntry(record);
                }
                else
                {
                	insertNoteEntry(record)
                }
            },

            // error
            function error()
            {
                //console.log("update User id error");
            }
        );
		}
		else
		{
			insertNoteEntry(record);
		}
	}
	else
	{
		insertNoteEntry(record)
	}
}
function updateNoteEntry(record)
{
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE noteEntry " +
            "SET noteUniqueId = :noteUniqueId, text = :text, UniqueID = :UniqueID, lastupdated = :lastupdated, isSynch=:isSynch  WHERE id = :noteItemId",

            // params
            {
                'noteItemId' : record.appId,
                'noteUniqueId' : record.noteId,
                'text' : record.text,
                'UniqueID' : record.uniqueid,
                'lastupdated' : parseInt(record.lastupdate)*1000,
                'isSynch':1               
            },

			// success
			function success(statement)
			{
				//console.log("update Note Entry Item success");
			},

			function error(error)
			{
				//console.log("update error");
				//console.log(error);
			}
		);
}

function insertNoteEntry(record)
{
	var localNoteId; 
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id FROM diaryItem WHERE type== 'NOTE' AND UniqueID = "+record.noteId,

            // params
            { },

            function success(statement)
            {
            	localNoteId =_.map(statement.getResult().data, function(item) {
                        return item.id;
                    });
            	
            },

            // error
            function error()
            {
                //console.log("update User id error");
            }
    );	
//    console.log("record : "+JSON.stringify(record));
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "INSERT INTO noteEntry (noteId, text, timestamp, UniqueID, lastupdated, noteUniqueId, isSynch) " +
                "VALUES (:noteId, :text, :timestamp, :UniqueID, :lastupdated, :noteUniqueId, :isSynch)",

                // params
                {
                	'noteId' : localNoteId ? parseInt(localNoteId) : parseInt(record.noteId),
                	'text' : record.text,
                	//'timestamp' : new Date().getTime(),
                	'timestamp' : parseInt(record.createddate),
                	'UniqueID' : parseInt(record.uniqueid),
                	'lastupdated' : parseInt(record.lastupdate)*1000,
                	'noteUniqueId' : parseInt(record.noteId),
                	'isSynch' : 1
                },

				// success
				function(statement) {
				           //console.log(statement);
				           //console.log(statement.getResult().lastInsertRowID);
				},

				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}
function ChangeDate(date)
{
	if(date!=null)
	{
	//console.log("date="+date);
	var currentDate=date.toString();
	var year=currentDate.substring(0,4);
	//console.log("year="+year);
	var month=currentDate.substring(4,6);
	//console.log("month="+month);
	var day=currentDate.substring(6,8);
	//console.log("day="+day);
	var newDate=year+"-"+month+"-"+day;
	//console.log("newDate="+newDate);
	return newDate;
	}
}

function changeTime(time)
{
//alert("time="+time);
if(time!=null)
	{
	var now=time.toString();
	var newTime="";
	
	if(now.length==3)
	{
		var hour=now.substring(0,1);
		var min=now.substring(1,3);
		newTime=hour+":"+min;
		
	}
	else
	{
		var hour=now.substring(0,2);
		var min=now.substring(2,4);
		newTime=hour+":"+min;
	}
	//console.log("newTime="+newTime);
	return newTime;
	}
}
function updateLastUpdate(tableName,timestamp)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "UPDATE lastSynch SET " +
                "lastSynch  = :lastupdated WHERE tableName = :TableName",

                // params
                {
                	'lastupdated' : timestamp,
                	'TableName' : tableName
                },

				// success
				function(statement) {
				           //console.log("table name "+tableName +"timestamp updated="+timestamp);
				           //console.log(statement.getResult().lastInsertRowID);
				},

				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}

function GetLastSync(tableName)
{
	//console.log("tableName="+tableName);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT lastSynch FROM lastSynch WHERE tableName = :TableName",

                // params
                {
                	'TableName' : tableName
                },

				// success
				function(statement) {
				           var lastupdated=_.map(statement.getResult().data, function(item) {
                        return item.lastSynch;
                    });
                    // alert("table name="+tableName );
                    //console.log(lastupdated);
                    if(lastupdated!=null)
                    {
                    	//alert(lastupdated[0]);
                    	if(tableName=="diaryItem")
                    	{
                    		diaryLastUpdate=lastupdated[0];
                    		//alert("diaryLastUpdate="+diaryLastUpdate);
                    	}
                    	else if (tableName=="noteEntry")
                    	{
                    		NoteEntryUpdate=lastupdated[0];
                    		//alert("NoteEntryUpdate="+NoteEntryUpdate);
                    	}
                    	else if(tableName=="dayOfNote")
                    	{
                    		dayOfNote=lastupdated[0];
                    	}
                    	else if(tableName=="inboxItem")
                    	{
                    		inboxItemUpdated=lastupdated[0];
                    	}
                    	else if(tableName=="getClassUserSyncing")
						{
                    		usersUpdate=lastupdated[0];
						}
						else
						{
							ClassUserUpdate=lastupdated[0];
						}
                    	
                    	
						
                   	 	//return lastupdated[0];
                    }
                    else
                    {
                    	return null;
                    }
				},

				// error
				function error()
				{
					console.log("Error while trying to create noteEntry item");
				}
		);
}

function getUserUniqueId()
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT UniqueID FROM User WHERE id=1",

                // params
                {
                	
                },

				// success
				function(statement) {
				var UniqueID=_.map(statement.getResult().data, function(item) {
                        return item.UniqueID;
                    });
                    if(UniqueID[0] != undefined && UniqueID[0] != "")
				           UserId=UniqueID[0];
				           //alert("UserId="+UserId);
				           
				},

				// error
				function error()
				{
					console.log("Error while trying to create noteEntry item");
				}
		);
}
function getUserEncryptedId()
{
	var encryptedUserId = null;
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
        // query
        "SELECT EncrptedUserId FROM User WHERE id=1",

        // params
        {
        	
        },

		// success
		function(statement) {
        	var EncrptedUserId=_.map(statement.getResult().data, function(item) {
                return item.EncrptedUserId;
            });
            if(EncrptedUserId[0] != undefined && EncrptedUserId[0] != ""){
            	(app.model.get(ModelName.STUDENTPROFILE)).set('EncrptedUserId',EncrptedUserId[0]);
            	encryptedUserId = EncrptedUserId[0];
            	//alert('EncrptedUserId[0] : ' + EncrptedUserId[0]);                    
            }
		           
		},

		// error
		function error()
		{
		}
	);
	return encryptedUserId;
}
function getSchoolUniqueId()
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT UniqueID FROM schoolProfile WHERE id=1",

                // params
                {
                	
                },

				// success
				function(statement) {
				var UniqueID=_.map(statement.getResult().data, function(item) {
                        return item.UniqueID;
                    });
                    if(UniqueID[0] != undefined && UniqueID[0] != "")
				           schoolId=UniqueID[0];
				           //alert("UserId="+UserId);
				           
				},

				// error
				function error()
				{
					console.log("Error while trying to create noteEntry item");
				}
		);
}

function GetUniqueId_Id()
{
	//console.log("insidedelte unique id=");
		 
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id,UniqueID,type,UserID FROM diaryItem WHERE isDelete = :Value",

            // params
            { 
               'Value': "1"
            },

            function success(statement)
            {
           // console.log("insidedelete + success");
            	
            	var id=_.map(statement.getResult().data, function(item) {
					return item;
					});
					//console.log("GetUniqueId_Id");
					//console.log(id);
            	if(id.length >0 )
                {	
                	var idarr = [];
                	var uidarr = [];
                	
                	for( i = 0 ;i< id.length ;i++)
                	{
                	   idarr.push(id[i].id);
                	   uidarr.push({"uniqueid":id[i].UniqueID,"type":id[i].type});
                	}
                	
                	DeleteItemFromServer(uidarr,idarr);
                	
                }
              
            },

            // error
            function error()
            {
               // console.log("insidedelete update User id error");
            }
        );
}
function DeleteItemFromServer(uidArr,idArr) { 


 var UserId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	var url =SystemSettings.SERVERURL+"/deletedairyitem"; 
	data ={
	"userId":UserId,
	"details":uidArr 
	};
	if(!UserId)
	{
		return;
	}
	console.log("TheCloud.callService DeleteItemFromServer: url=" + url + " data=" + JSON.stringify(data)+"readyStateChanged : "+readyStateChanged);
			var xhr = new XMLHttpRequest();
			xhr.open("POST",url, true);
			xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');
			xhr.setRequestHeader('Accept', 'text/javascript, text/html, application/xml, text/xml, */*');
			xhr.onreadystatechange = readyStateChanged;
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhr.send(JSON.stringify(data));	
			function readyStateChanged(){
				
				if(xhr.readyState === 4)
				{
						//console.log("readyStateChanged");
						//console.log(xhr.responseText);
						var result=xhr.responseText;
						//console.log("result");
						//console.log(result)
						result=JSON.parse(result);
						var details=result.details;
						for(i = 0; i <idArr.length; i++)
						{
							DeleteItem('diaryItem', "id", idArr[i]);
						} 
						xhr.onreadystatechange = nill;
						xhr.abort();
				}
			}
/*$.ajax({
'url' : url,
'type': 'POST',
'timeout' : 120000,
'data': JSON.stringify(data),
processData: false,
'dataType' : "json",
'success': function(result) {

// console.log("deletedairyitem");
 //console.log(result);
var details=result.details;
//console.log(details);
for(i = 0; i <idArr.length; i++)
{
DeleteItem('diaryItem', "id", idArr[i]);
} 
 
}, 
'error': function(response) {
	//console.log("DeleteItemFromServer=");
	//console.log(JSON.stringify(response));
 
}
});*/
}
function DeleteItem(tableName, fieldName, Value)
{
 
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "DELETE FROM "+tableName+" WHERE "+fieldName+" = :Value",

                // params
                {
                	'Value' : Value
                },

				// success
				function(statement) {
					//console.log("DELETE success");
				},

				// error
				function error()
				{
					//console.log("Error while trying to delete entry from database");
				}
		);
}


//================


function GetUniqueId_NoteId()
{
	//console.log("insidedelte unique id=");
		 
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id,UniqueID FROM noteEntry WHERE isDelete = :Value",

            // params
            { 
               'Value': "1"
            },

            function success(statement)
            {
            	//console.log("GetUniqueId_NoteId");
            	
            	var id=_.map(statement.getResult().data, function(item) {
					return item;
					});
				//console.log(id);
            	if(id.length >0 )
                {	
                	var idarr = [];
                	var uidarr = [];
                	
                	for( i = 0 ;i< id.length ;i++)
                	{
                	   idarr.push(id[i].id);
                	   uidarr.push({"uniqueid":id[i].UniqueID});
                	}
                	
                	DeleteNoteFromServer(uidarr,idarr);
                	
                }
              
            },

            // error
            function error()
            {
                ///console.log("insidedelete update User id error");
            }
        );
}
function DeleteNoteFromServer(uidArr,idArr) { 
 
	var url =SystemSettings.SERVERURL+"/deletenoteentry"; 
	//console.log("DeleteNoteFromServer="+uidArr);
	//console.log(uidArr);
	data ={
		"details":uidArr 
		};
			var xhr = new XMLHttpRequest();
			xhr.open("POST",url, true);
			xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');
			xhr.setRequestHeader('Accept', 'text/javascript, text/html, application/xml, text/xml, */*');
			xhr.onreadystatechange = readyStateChanged;
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhr.send(JSON.stringify(data));	
			function readyStateChanged(){
				
				if(xhr.readyState === 4)
				{
						//console.log("readyStateChanged");
						//console.log(xhr.responseText);
						var result=xhr.responseText;
						//console.log("result");
						//console.log(result)
						result=JSON.parse(result);
						var details=result.details;
						for(i = 0; i <idArr.length; i++)
						{
							DeleteItem('noteEntry', "id", idArr[i]);
						} 
						xhr.onreadystatechange = nill;
						xhr.abort();
				}
			}
	/*$.ajax({
		'url' : url,
		'type': 'POST',
		'data': JSON.stringify(data),
		'timeout' : 120000,
		processData: false,
		'dataType' : "json",
		'success': function(result) {
			var details=result.details;
			for(i = 0; i <idArr.length; i++)
			{
				//alert("here");
				DeleteItem('noteEntry', "id", idArr[i]);
			} 
 
		}, 
		'error': function(response) {
 
		}
		});*/
}
function DeleteNote(record)
{
 	var localNoteId; 
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id FROM diaryItem WHERE type = 'NOTE' AND UniqueID = "+record.uniqueid,

            // params
            { },

            function success(statement)
            {
            	localNoteId =_.map(statement.getResult().data, function(item) {
                        return item.id;
                    });
            	
            },

            // error
            function error()
            {
                //console.log("update User id error");
            }
    );	
    
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "DELETE FROM diaryItem WHERE type = 'NOTE' AND UniqueID = :Value",

                // params
                {
                	'Value' : record.uniqueid
                },

				// success
				function(statement) {
					DeleteNoteEntries(localNoteId[0]);
				},
				
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}

function DeleteNoteEntries(localNoteId)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "DELETE FROM noteEntry WHERE noteId = :Value",

                // params
                {
                	'Value' : localNoteId
                },

				// success
				function(statement) {
				
				},
				
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}


function CheckLastUpdate()
{

	//alert("CheckLastUpdate");
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT * FROM lastSynch",

                // params
                {},

				// success
				function(statement) {
				
				var values=_.map(statement.getResult().data, function(item) {
					return item;
					});
					//console.log("CheckLastUpdate");
				//console.log(values);
				for(var i=0;i<values.length;i++)
				{
					app.model.get(ModelName.LASTUPDATE).set({TableName : values[i].TableName, lastupdated :values[i].lastupdated });
				}
					
				var items=app.model.get(ModelName.LASTUPDATE);
				//console.log("items=")
				//console.log(items);
				},
				
				// error
				function error()
				{
					console.log("Error while trying to create noteEntry item");
				}
		);
}

function formateTimes(times)
{	
	//console.log("formateTimes="+times);
	//alert("formateTimes="+times);
	if(!times)
	{
		//alert("inside  if");
		return null;
	}
	else
	{
		//alert("inside else");
				var h=times;
				var newFormatedTime = h;
				var timeArray = newFormatedTime.split(":");
				//var showtime=timeArray[0]+timeArray[1];
				var timeSplit=timeArray[0].substring(0)[0];
				var showtime="";
				if(timeSplit==0)
				{
					showtime=timeArray[0].substring(0)[1]+timeArray[1];
					//alert("showtime="+showtime);
				}
				else
				{
					showtime=timeArray[0]+timeArray[1];
				}
				
				if(showtime=="000")
					return 0000;
				else
					return parseInt(showtime);
 	}
		
}

function updateClass()
{
	//alert("updateClass 1");
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT c.UniqueID as classUniqueID, p.title, s.name, cu.code, cu.cycleDay, cu.classId, t.name as timetableName, Room.Name as roomName FROM ClassUser cu INNER JOIN class c ON cu.classId = c.id"+
					" INNER JOIN subject s ON c.subjectId = s.id"+
					" INNER JOIN period p ON c.periodId = p.id"+
					" INNER JOIN timetable t ON c.timetable = t.id"+
					" INNER JOIN Room  ON c.RoomId = Room.id"+
					" WHERE cu.isSync = 0",
                // params
                {},

				// success
				function(statement) {
                	//console.log("addClassSubject finished")
                	
                	var values=_.map(statement.getResult().data, function(item) {
					return item;
					});
					
					var userid=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
					var dataArray=[];
					
					for(var i=0;i<values.length;i++)
					{
						var obj=new Object();
						obj.created=getTimestampmilliseconds();
						obj.CycleDays=values[i].cycleDay;
						obj.code=values[i].code;
						obj.room_name=values[i].roomName;
						obj.period_name=values[i].title;
						obj.subject_name=values[i].name;
						obj.timetableName=values[i].timetableName;
						obj.classUniqueID=values[i].classUniqueID;
						dataArray.push(obj);
					}
					
					var user_type = "Student";
					getAppInstallStatus();
					var appInstallStatus = app.model.get(ModelName.APPLICATIONSTATE).get('appInstallStatus');
					if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
						user_type = "Teacher";
						//if(!appInstallStatus){
							getClassUsers();
						//}
					} else {
						insertMyTeachers();
						getParents();
					}
					
					data={
						"lastSynch":ClassUserUpdate,
						"userid":userid,
						"SchoolId":schoolId,
						"user_type":user_type,
						"details":dataArray
					}
					if(!UserId)
					{
						return;
					}
					
					values = null;
					//alert("calling addclasssubject");
					var url=SystemSettings.SERVERURL +"/addclasssubject";
					console.log('addclasssubject data : '+JSON.stringify(data));
					var xhr = new XMLHttpRequest();
					xhr.open("POST",url, true);
					xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');
					xhr.setRequestHeader('Accept', 'text/javascript, text/html, application/xml, text/xml, */*');
					xhr.onreadystatechange = readyStateChanged;
					xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
					xhr.send(JSON.stringify(data));	
					function readyStateChanged(){
		
						if(xhr.readyState == 4)
						{
								var result=xhr.responseText;
								console.log("addclasssubject result"+result)
								if(result)
								{
										data = null;
										dataArray = null;
										
										result=JSON.parse(result);
										var classUserLastupdated=getTimestampmilliseconds();
									
										if(result.status=="ERROR")
										{
											hideSplash=1;
											 //console.log("updateClass error"+hideSplash);
										}
										
										var details=result.details;
										var deleteArray=result.deleted;
										if(deleteArray.length)
										{
											for(var i=0;i<deleteArray.length;i++)
											{
												deleteclass(deleteArray[i]);
											}
										}
										if(details.length)
										{
											for(var i=0;i<details.length;i++)
											{
												//console.log("====="+i+"=======");
												processClassUserDetail(details[i]);
											}
											
											var timetables = app.model.get(ModelName.CALENDAR).get('timetables');
											var hasPeriods = false;
											for ( var i = 0; i < timetables.length; i++) {
												timetable = timetables.at(i);
												var periodsCount = timetable.get('periods').length;
												if(periodsCount > 0){
													hasPeriods = true;
												}
											}
											//alert('periodsCount : ' + hasPeriods);
											if(hasPeriods){
												updateLastUpdate("ClassUser",classUserLastupdated);
											}
									 	}
										else{
											addClassSubject = true;
										}
										
										hideSplash=1;
										
										if(details.length > 0 || deleteArray.length > 0){
										
											if (app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")) {
												getClassForTeacher(app.model.get(ModelName.STUDENTPROFILE).get("UniqueID"));
											} else {
												getAllClasses();
											}
					
											app.context.trigger(EventName.FETCHSUBJECTS);
											app.context.on(EventName.REFRESHSUBJECTS, diary.view.MainView.refreshSubjectView, diary.view.MainView);
											app.context.trigger(EventName.GENERATECALENDAR, SystemSettings.HARDCODDEDYEAR);
										}
										
										$("#updateMsg").hide();
										
										var activeSubTab = $("#subTabProfileView .tabs_container").find(".active");
										
										var name = $(".headerNavigation").find(".selected").attr('data-link'); 
										if(name == 'home' || name == 'inbox'){
											tempModeCreate = modeCreateMsg;
											fromSync = true;
											$(".headerNavigation").find(".selected").click();
											
										}
										
										if(name == 'myprofile'){
											$(".myProfileNavigation").find(".active").click();
										}
										
										//console.log("addclasssubject readyStateChanged 22"+readyStateChanged);
										//console.log("addClassSubject flag : "+addClassSubject);
										
										details = null;
										deleteArray = null;
										result = null;
										classUserLastupdated = null;
								}
								xhr.onreadystatechange = null;
								xhr.abort();
						}
					}
					
					url = null;
					user_type = null;
					userid = null;
				},
				// error
				function error()
				{
					console.log("Error while trying to updateClass");
					FlurryAgent.logError("Class Sync Error", "Error in fetching data from addclasssubject web service",1);
					dataDownloadError();
				}
		);
}
function deleteclass(record)
{
var DataObj=new Object();
app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT period.timetable, period.id FROM period JOIN timetable ON period.timetable = timetable.id WHERE period.title=:recordTitle AND timetable.name=:timetableName AND cycleDay= :CycleDays",

                // params
                {
					'recordTitle':record.periodTitle,
					'timetableName':record.timetable_name,
					'CycleDays':record.cycleDays
					
                },

				// success
				function(statement) {
				
	            	var value=_.map(statement.getResult().data, function(item) {
						return item;
						});
					if(value.length)
					{
						DataObj.periodId=value[0].id;
						DataObj.timetableId=value[0].timetable;
						getclassSubjectId(DataObj,record);
					}
					else
					{
						app.locator.getService(ServiceName.APPDATABASE).runQuery(
						                // query
						                "SELECT period.timetable, period.id FROM period JOIN timetable ON period.timetable = timetable.id WHERE period.title=:recordTitle AND timetable.name=:timetableName AND ifnull (period.cycleDay,'')=''",

						                // params
						                {
											'recordTitle':record.periodTitle,
											'timetableName':record.timetable_name
					
						                },

										// success
										function(statement) {
				
							            	var value=_.map(statement.getResult().data, function(item) {
												return item;
												});
											if(value.length)
											{
												DataObj.periodId=value[0].id;
												DataObj.timetableId=value[0].timetable;
											}
											getclassSubjectId(DataObj,record);
				
										},
										// error
										function error()
										{
											//console.log("Error while trying to create noteEntry item");
										}
								);
					}
				
				},
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
	
}
function getclassSubjectId(dataObj,record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT id FROM subject WHERE name=:SubjectName AND classId = :classId",

                // params
                {
					'SubjectName':record.subject_name.toUpperCase(),
					'classId': record.classId
                },

				// success
				function(statement) {
				
	            	var value=_.map(statement.getResult().data, function(item) {
						return item.id;
						});
						if(value.length)
						{
							dataObj.subjectId=value[0];
						}
						app.locator.getService(ServiceName.APPDATABASE).runQuery(
					                // query
					                "SELECT id FROM Room WHERE Name=:Name",

					                // params
					                {
										'Name':record.room_name.toUpperCase(),
					                },

									// success
									function(statement) {
				
						            	var value=_.map(statement.getResult().data, function(item) {
											return item.id;
											});
											if(value.length)
											{
												dataObj.roomId=value[0];
											}
											getclassId(dataObj,record);
				
									},
				
									// error
									function error()
									{
										console.log("Error while trying to create noteEntry item");
									}
							);
				    
				
				},
				
				// error
				function error()
				{
					console.log("Error while trying to create noteEntry item");
				}
		);
	
}
function getclassId(dataObj,record)
{
	//alert('getclassId is called');
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT class.id FROM class WHERE cycleDay=:cycleDay AND timetable=:timetable AND periodId=:periodId AND subjectId=:subjectId AND RoomId=:RoomId",

                // params
                {
					'cycleDay':record.cycleDays,
					'timetable':dataObj.timetableId,
					'periodId':dataObj.periodId,
					'subjectId':dataObj.subjectId,
					'RoomId':dataObj.roomId
                },

				// success
				function(statement) {

	            	var value=_.map(statement.getResult().data, function(item) {
						return item.id;
						});
						if(value.length)
						{
							app.locator.getService(ServiceName.APPDATABASE).runQuery(
						                // query
						                "DELETE FROM ClassUser WHERE classId=:classId AND cycleDay=:cycleDay",

						                // params
						                {
											'cycleDay':record.cycleDays,
											'classId':value[0]
											
						                },

										// success
										function(statement) {

							            	console.log("DELETE CLASS USER SUCCESS");
											deleteClass(value[0]);
										},

										// error
										function error()
										{
											console.log("Error while trying to create noteEntry item");
										}
								);
						}

				},

				// error
				function error()
				{
					console.log("Error while trying to create noteEntry item");
				}
		);
}

function deleteClass(classId)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "DELETE FROM Class WHERE id = :classId",

                // params
                {
					'classId':classId
                },

				// success
				function(statement) {
	            	console.log("DELETE CLASS SUCCESS");
				},

				// error
				function error()
				{
					console.log("DELETE CLASS FAILURE");
				}
		);
}

function processClassUserDetail(record)
{
	//console.log("processClassUserDetail");
	var recordInfo={
			'periodId':"",
			'subjectId':"",
			'roomId':""
	}
	var classid="";
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT class.id, class.cycleDay, class.periodId, class.subjectId, class.room FROM class JOIN period ON class.periodId = period.id JOIN timetable ON timetable.id = period.timetable JOIN subject ON class.subjectId = subject.id "+
				"JOIN Room ON Room.id=class.RoomId  WHERE timetable.name=:timetableName AND subject.name=:SubjectName  AND period.title=:recordTitle AND Room.Name=:Name AND class.cycleDay=:cycleDay",

                // params
                {
					'recordTitle':record.periodTitle,
					'SubjectName':record.subject_name.toUpperCase(),
					'Name':record.room_name.toUpperCase(),
					'timetableName':record.timetable_name,
					'cycleDay':record.cycleDays
                },

				// success
				function(statement) {
				
	            	var value = _.map(statement.getResult().data, function(item) {
						return item;
					});
				    //console.log("checkClassId value");
					//console.log(value);
					if(value.length)
					{
						classid=value[0].id;
						console.log(classid);
						console.log("value.length : "+value.length);
						recordInfo={
								'periodId':value[0].periodId,
								'subjectId':value[0].subjectId,
								'roomId':value[0].RoomId
						}
						
						//console.log("inside if");
						DeleteClassUser(record,classid);
						/***********UPDATE SUBJECT******/
									app.locator.getService(ServiceName.APPDATABASE).runQuery(
				                // query
				                "UPDATE subject SET colour=:color, UniqueID=:UniqueID, classId=:classIdVal WHERE (name=:subjectName AND uniqueid is null) OR (name=:subjectName AND classId =:classId AND uniqueid is not null)",

				                // params
				                {
				                	'color':record.code,
									'subjectName':record.subject_name.toUpperCase(),
									'UniqueID': record.subject_id,
									'classId': record.classId,
									'classIdVal': record.classId
				                },

								// success
								function(statement) {
				                	addClassSubject = true;
					            	//console.log("SUBJECT COLOR UPDATED now update class");
				                	insertintoClass(record,recordInfo);
				
								},
				
								// error
								function error()
								{
									//console.log("Error while trying to create noteEntry item");
								}
						);
									
									//GetrecordInfo(record,record.cycleDays);
									
									
					}
					else
					{
						//console.log("inside else");
						addClassSubject = true;
						GetrecordInfo(record,record.cycleDays);
					}
				
				},
				
				// error
				function error()
				{
					console.log("Error while trying to create noteEntry item");
				}
		);
	//eckClassId(record);	
	
}
function DeleteClassUser(record,classid)
{
	//console.log("DeleteClassUser classid="+classid);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "DELETE FROM ClassUser WHERE classId=:classId",

                // params
                {
					'classId':classid
                },

				// success
				function(statement) {
				
	            	//console.log("DELETE CLASSUSER SUCCESS");
					insertNewClassUser(record,classid);
				
				},
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}
function insertNewClassUser(record,classid)
{
	var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	//alert("userId="+userId);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO ClassUser (classId, userId, cycleDay, code, lastupdated, isSync, UniqueID, createdBy, classUniqueId) VALUES (:classId, :userId, :cycleDay, :code, :lastupdated, :isSync, :UniqueID, :createdBy, :classUniqueId)",

            // params
            {
			'classId' :classid,
			'userId':userId,
			'cycleDay':record.cycleDays,
			'code':record.code,
			'lastupdated':getTimestampmilliseconds(),
			'isSync':1,
	    	'UniqueID':record.uniqueId,
	    	'createdBy':record.createdBy,
	    	'classUniqueId' : record.classId
            },
			// success
			function(statement) {
				//console.log("addClassuserEntry id="+statement.getResult().lastInsertRowID);
				updateClassUniqueId(record,classid);
			},
			
			// error
			function error()
			{
				console.log("Error while trying to create ClassUser item");
			}
	);
}

function GetRoomId(record)
{
	var roomId = null;
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
        // query
        "SELECT id FROM Room WHERE Room.Name=:roomName ",

        // params
        {
			'roomName':record.room_name.toUpperCase(),

        },

		// success
		function(statement) {

        	var value=_.map(statement.getResult().data, function(item) {
				return item.id;
			});
			if(value.length > 0)
			{
				roomId = value[0];
			}
		},

		// error
		function error()
		{
			
		}
	);
	
	return roomId;
}

function updateClassUniqueId(record,classid){
	var roomId = GetRoomId(record);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE class SET UniqueID = :UniqueID, lastupdated = :lastupdated, RoomId = :RoomId, isSync = :isSync "+
			" WHERE id = :classid ",

            // paramssubjectName
            {
				'isSync':1,
				'RoomId' : roomId,
				'classid':classid,
				'UniqueID':record.classId,
				'lastupdated':getTimestampmilliseconds()
				
            },

			// success
			function(statement) {
			
            	//alert("insertintoClass update success");
			},
			
			// error
			function error()
			{
				//console.log("Error while trying to create noteEntry item");
			}
	);
}

function GetrecordInfo(record, CycleDay)
{
//console.log("CycleDay="+CycleDay);
	//console.log("GetrecordInfo");
	
	var recordInfo={
		'periodId':"",
		'subjectId':"",
		'roomId':""
	}
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT period.id FROM period JOIN timetable ON period.timetable = timetable.id WHERE period.title=:recordTitle AND timetable.name=:timetableName AND cycleDay= :CycleDays",

                // params
                {
					'recordTitle':record.periodTitle,
					'timetableName':record.timetable_name,
					'CycleDays':CycleDay
					
                },

				// success
				function(statement) {
				
	            	var value=_.map(statement.getResult().data, function(item) {
						return item.id;
						});
						//console.log("GetrecordInfo");
						//console.log(value);
						if(value.length)
						{
								recordInfo.periodId=value[0];
					          app.locator.getService(ServiceName.APPDATABASE).runQuery(
				                // query
				                "SELECT id FROM subject WHERE subject.name=:subjectName AND classId = :classId",

				                // params
				                {
									'subjectName':record.subject_name.toUpperCase(),
									'classId': record.classId
					
				                },

								// success
								function(statement) {
				
					            	var value=_.map(statement.getResult().data, function(item) {
										return item.id;
									});
									
									if(value.length)
									{
									recordInfo.subjectId=value[0];
									/***********UPDATE SUBJECT******/
									app.locator.getService(ServiceName.APPDATABASE).runQuery(
				                // query
				                "UPDATE subject SET colour=:color WHERE name=:subjectName AND classId = :classId",

				                // params
				                {
				                	'color':record.code,
									'subjectName':record.subject_name.toUpperCase(),
									'classId': record.classId
					
				                },

								// success
								function(statement) {
				
					            	//console.log("SUBJECT COLOR UPDATED");
					
				
								},
				
								// error
								function error()
								{
									//console.log("Error while trying to create noteEntry item");
								}
						);
									/*******************************/
									SetRoomInfo(record,recordInfo);
									insertintoClass(record,recordInfo);
									}
									else
									{
										insertSubject(record,recordInfo)
									}
									
					
				
								},
				
								// error
								function error()
								{
									//console.log("Error while trying to create noteEntry item");
								}
						);
					}
					else
					{
						//insertintoperiod
						//console.log("I am in else");
						
						//change for issue EZTEST-920
						GetrecordInfo2(record);
					}
					
				
				},
				
				// error
				function error()
				{
					console.log("Error while trying to create noteEntry item");
				}
		);
}
function SetRoomInfo(record,recordInfo)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
				                // query
				                "SELECT id FROM Room WHERE Room.Name=:roomName ",

				                // params
				                {
									'roomName':record.room_name.toUpperCase(),
					
				                },

								// success
								function(statement) {
				
					            	var value=_.map(statement.getResult().data, function(item) {
										return item.id;
									});
									if(value.length)
									{
									recordInfo.roomId=value[0];
									//SetRoomInfo(record,recordInfo);
									//insertintoClass(record,recordInfo);
									}
									else
									{
										insertRoom(record,recordInfo)
									}
									
					
				
								},
				
								// error
								function error()
								{
									console.log("Error while trying to create noteEntry item");
								}
						);
}
function insertRoom(record,recordInfo)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "INSERT INTO Room (SchoolId, CampusId, Name, Code, UniqueID, lastupdated) " +
										"VALUES (:SchoolId, :CampusId, :Name, :Code, :UniqueID, :lastupdated )",

								// params
								{
									'SchoolId' : 10,
									'CampusId' : 2,
									'Name' : record.room_name.toUpperCase(),
									'Code' : "#0000",
									'UniqueID' : 12,
									'lastupdated' : getTimestampmilliseconds()
								},

				// success
				function(statement) {
				
	            	//alert("classUser delete success");
					recordInfo.roomId=statement.getResult().lastInsertRowID;
					//2 entries were going in the tables
					//insertintoClass(record,recordInfo);
					
				
				},
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}
function GetrecordInfo2(record)
{
	//console.log("CycleDay="+CycleDay);
	//console.log("GetrecordInfo2");
	
	var recordInfo={
		'periodId':"",
		'subjectId':"",
		'subjectColor':"",
		'roomId':""
	}
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT period.id FROM period JOIN timetable ON period.timetable = timetable.id WHERE period.title=:recordTitle AND timetable.name=:timetableName AND ifnull (period.cycleDay,'')=''",

                // params
                {
					'recordTitle':record.periodTitle,
					'timetableName':record.timetable_name
					
                },

				// success
				function(statement) {
				
	            	var value=_.map(statement.getResult().data, function(item) {
						return item.id;
						});
						//console.log("GetrecordInfo2");
						//console.log(value);
						if(value.length)
						{
								recordInfo.periodId=value[0];
					          app.locator.getService(ServiceName.APPDATABASE).runQuery(
				                // query
				                "SELECT id,colour FROM subject WHERE subject.name=:subjectName AND classId = :classId",

				                // params
				                {
									'subjectName':record.subject_name.toUpperCase(),
									'classId': record.classId
					
				                },

								// success
								function(statement) {
				
					            	var value=_.map(statement.getResult().data, function(item) {
										return item;
									});
									//console.log("get subject details");
									//console.log(value);
									
									if(value.length)
									{
									/***********UPDATE SUBJECT******/
									app.locator.getService(ServiceName.APPDATABASE).runQuery(
				                // query
				                "UPDATE subject SET colour=:color WHERE name=:subjectName AND classId = :classId",

				                // params
				                {
				                	'color':record.code,
									'subjectName':record.subject_name.toUpperCase(),
									'classId': record.classId
					
				                },

								// success
								function(statement) {
				
					            	//console.log("SUBJECT COLOR UPDATED");
					
				
								},
				
								// error
								function error()
								{
									//console.log("Error while trying to create noteEntry item");
								}
						);
									/*******************************/
									recordInfo.subjectId=value[0].id;
									recordInfo.subjectColor=value[0].colour;
									SetRoomInfo(record,recordInfo);
									insertintoClass(record,recordInfo);
									}
									else
									{
										insertSubject(record,recordInfo)
									}
									
					
				
								},
				
								// error
								function error()
								{
									//console.log("Error while trying to create noteEntry item");
								}
						);
					}
					else
					{
						//console.log("I am in else");
						
					}
					
				
				},
				
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}

function insertSubject(record,recordInfo)
{
	//alert("here");
	//alert("record.subject_name.toUpperCase()=="+record.subject_name.toUpperCase());
	var subjectName=record.subject_name.toUpperCase();
	var code=record.code;	
	//alert("code="+code);
	if(code=="undefined"||code=="")
	{
		code=record.code;	
	}
	//alert("code="+code);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "INSERT INTO subject (name, colour, UniqueID, classId) VALUES (:name, :colour, :UniqueID, :classId)",

                // params
                {
					'name':subjectName,
					'colour':code,
					'UniqueID':record.subject_id,
					'classId':record.classId
                },

				// success
				function(statement) {
				
	            	//alert("classUser delete success");
	            	
	            	var newSubject = new diary.model.Subject({
						'title' : subjectName,
						'colour' : code
					});
					console.log("newSubject00");
					console.log(newSubject);
					app.model.get(ModelName.SUBJECTS).add(newSubject);
					recordInfo.subjectId=statement.getResult().lastInsertRowID;
					SetRoomInfo(record,recordInfo);
					insertintoClass(record,recordInfo);
				},
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
	
}
function insertintoClass(record,recordInfo)
{
	//console.log("insertintoClass");
	//console.log(record);
	//console.log(recordInfo);
	//console.log("before timetableid")
	var timetableid= 1; //app.model.get(ModelName.CALENDAR).get('timetables').getSemesterAt(record.timetable_name).id;
	if(record.timetable_name == 'Semester 2'){
		timetableid = 2;
	}
	//alert(timetableid);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT id FROM class where cycleDay = :cycleDay AND periodId = :periodId AND UniqueID = :UniqueID",

                // paramssubjectName
                {
					'cycleDay':record.cycleDays,
					'periodId':recordInfo.periodId,
					'UniqueID':record.classId,
                },

				// success
				function(statement) {
					
					var values=_.map(statement.getResult().data, function(item) {
						return item.id;
					});
					if(values[0])
					{
						updateExistingClassRecord(record,recordInfo);
					} else {
						insertClassRecord(record,recordInfo);
					}
					
				},
				
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}

function updateExistingClassRecord(record,recordInfo){

	var subjectId;
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id FROM subject WHERE name = :name AND classId = :classId",

            // params
            
            {
            	'name':	record.subject_name.toUpperCase(),
            	'classId': record.classId
            },
            

            function success(statement)
            {
            	var id =_.map(statement.getResult().data, function(item) {
                        return item.id;
                });
                
                subjectId = id[0];
            },

            // error
            function error()
            {
               // console.log("update User id error");
            }
    );
	
	var roomId = GetRoomId(record);
        
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "UPDATE class SET subjectId = :subjectId, Code = :Code, lastupdated = :lastupdated, RoomId = :RoomId, Name = :ClassName "+
				" WHERE cycleDay = :cycleDay AND periodId = :periodId AND UniqueID = :UniqueID",

                // paramssubjectName
                {
					'cycleDay':record.cycleDays,
					'periodId':recordInfo.periodId,
					'UniqueID':record.classId,
					'Code':record.code,
					'lastupdated':getTimestampmilliseconds(),
					'RoomId':parseInt(roomId),
					'ClassName' :record.className,
					'subjectId' : subjectId
                },

				// success
				function(statement) {
				
	            	//alert("insertintoClass update success");
				},
				
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}

function insertClassRecord(record,recordInfo){
	
	var values;
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id FROM timetable WHERE UniqueID = :UniqueID",

            // paramssubjectName
            {
				'UniqueID': record.timetable_id,
            },

			// success
			function(statement) {
            	values=_.map(statement.getResult().data, function(item) {
					return item.id;
				});
				
				//alert('getTimetableId : '+values[0]);
				//return values[0];
			},
			
			// error
			function error()
			{
				//console.log("Error while trying to create noteEntry item");
			}
	);
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "INSERT INTO class (timetable, cycleDay, periodId, subjectId, Code, lastupdated, RoomId, isSync, UniqueID, Name)"+
				" VALUES (:timetable, :cycleDay, :periodId, :subjectId, :Code, :lastupdated, :RoomId, :isSync, :UniqueID, :ClassName)",

                // paramssubjectName
                {
					'timetable':values[0],
					'cycleDay':record.cycleDays,
					'periodId':recordInfo.periodId,
					'subjectId':recordInfo.subjectId,
					'Code':record.code,
					'lastupdated':getTimestampmilliseconds(),
					'RoomId':parseInt(recordInfo.roomId),
					'isSync':1,
					'UniqueID':record.classId,
					'ClassName' :record.className
                },

				// success
				function(statement) {
				
	            	//console.log("insertintoClass success");
					var classId=statement.getResult().lastInsertRowID;
					/* app.context.trigger(EventName.CREATECLASS, "Timetable Semester 2 from 20130204",
										recordInfo.periodId, record.cycleDays,
										record.subject_name.toUpperCase(), recordInfo.subjectColor); */
					insertNewClassUser(record,classId)
					
				
				},
				
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
}
function syncClass(values)
{
	//console.log("syncClass");
	//console.log(values);
	
}
function getGrades(){
	var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	var schoolid = (app.model.get(ModelName.SCHOOLPROFILE)).get('UniqueID');
	var grades=[];
	var url = SystemSettings.SERVERURL + '/getgrades';
	var data = '{"schoolid":'+schoolid+',"user_id":'+userId+'}';
	$.ajax({
		'url' : url,
		'type': 'POST',
		'data': JSON.stringify(data),
		'dataType' : "json",
		'success': function(result) {
			
			var gradeList = result.result; 
			for(var i=0;i<gradeList.length;i++)
			{
				var grade_obj=new Object();
				grade_obj.id=gradeList[i].Id;
				grade_obj.name=gradeList[i].Name;
				grades.push(grade_obj);
			}
			
			saveGrades(grades);
			
		}, 
		'error': function(response) {
			alert("error grades : "+JSON.stringify(response));
			//console.log("getState error");
			//console.log(response);

		}
		});
	
	return grades;
}

function getState()
{
	
	app.model.set(ModelName.STATES,new diary.collection.StateCollection());
	var country=[];
	var countryStates = {};
	var url=SystemSettings.SERVERURL +"/getstates";
	var data={
		"lastSynch":""
	}
	$.ajax({
		'url' : url,
		'type': 'POST',
		'data': JSON.stringify(data),
		'dataType' : "json",
		'success': function(result) {
			//console.log("getState");
			
			var states=result.states;
			
			for(var i=0;i<states.length;i++)
			{
				var country_obj=new Object();
				
				country_obj.id=states[i].Id;
				country_obj.name=states[i].Name;
				var countrystates=states[i].States;
				
				countryStates.states = countrystates;
				saveStates(countrystates);
				country.push(country_obj);
			}
			
			
			saveCountry(country);
			countryStates.country = country;
		}, 
		'error': function(response) {
			//alert("error getState: "+JSON.stringify(response));
        	if(response.readyState == 0){
        		//alert('calling it again');
        		getState();
        	}
			//console.log("getState error");
			//console.log(response);

		}
		});
	return countryStates;
}

function saveGrades(grade)
{
	var grades = new diary.collection.GradeCollection();
	for(var i=0;i<grade.length;i++)
	{
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "INSERT or replace into grade (UniqueID,name) VALUES(:gradeId,:gradeName)",
                // params
                {
                	'gradeId':grade[i].id,
					'gradeName':grade[i].name
                },

				// success
				function(statement) {
                	var modelGrade = new diary.model.Grade();
                	modelGrade.set("uniqueId",grade[i].id);
                	modelGrade.set("name",grade[i].name)
                	grades.push(modelGrade);
				},
				
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
	}
	app.model.set(ModelName.GRADES, grades);
}


function saveCountry(country)
{
	var countries = new diary.collection.CountryCollection();
	for(var i=0;i<country.length;i++)
	{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "INSERT or replace into Country (UniqueID,name) VALUES(:Countryid,:countryName)",

                // params
                {
                	'Countryid':parseInt(country[i].id),
					'countryName':country[i].name
                },

				// success
				function(statement) {
                	var modelCountry = new diary.model.Country();
                	modelCountry.set("uniqueId",parseInt(country[i].id));
                	modelCountry.set("name",country[i].name)
                	countries.push(modelCountry);
				},
				
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
	}
	app.model.set(ModelName.COUNTRY, countries);
}

function saveStates(states)
{
	var statesCollection= app.model.get(ModelName.STATES)
//	alert("saveStates"+JSON.stringify(states));
	
	for(var i=0;i<states.length;i++)
	{
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
	                // query
	                "INSERT or replace into State (UniqueID,CountryId,Name) VALUES (:UniqueID,:Countryid,:countryName)",
	
	                // params
	                {
						'UniqueID':parseInt(states[i].Id),
	                	'Countryid':parseInt(states[i].CountryId),
						'countryName':states[i].Name
	                },
	
					// success
					function(statement) {
	                	var modelState = new diary.model.State();
	                	modelState.set("uniqueId",parseInt(states[i].Id));
	                	modelState.set("countryId",parseInt(states[i].CountryId))
	                	modelState.set("name",states[i].Name)
	                	statesCollection.push(modelState);
					},
					
					// error
					function error()
					{
						//console.log("Error while trying to create noteEntry item");
					}
			);
	}
	app.model.set(ModelName.STATES, statesCollection);
}

function parseDeletedId(deleteArray)
{
	//console.log("deleteArray");
	//console.log(deleteArray);
    for(var i=0;i<deleteArray.length;i++)
	{
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT UniqueID FROM diaryItem where UniqueID="+deleteArray[i].Id,

                // params
                {
					
                },

				// success
				function(statement) {
				
					
					var values=_.map(statement.getResult().data, function(item) {
						return item.UniqueID;
						});
						//console.log("unique id found=" +values);
					if(values[0])
					{
						DeleteItem("diaryItem", "UniqueId", values[0])
					}
				},
				
				// error
				function error()
				{
					//console.log("Error while trying to create noteEntry item");
				}
		);
       
    }
    deleteArray = null;
    values = null;
}
function getTimestampmilliseconds()
{
	var time=new Date().getTime();
	//alert("time="+time);
	return time;
}

function addClassuserEntry(classuserDetail,classId)
{
	//alert(" addClassuserEntry record"+JSON.stringify(classuserDetail));
	console.log("addClassuserEntry : "+JSON.stringify(classuserDetail));
	//console.log("addClassuserEntry classId");
	var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO ClassUser (classId, userId, cycleDay, code, lastupdated, isSync, createdBy) VALUES (:classId, :userId, :cycleDay, :code, :lastupdated, :isSync, :createdBy)",

            // params
            {
            
			'classId' :classId,
			'userId':userId,
			'cycleDay':classuserDetail.cycleDay,
			'code':classuserDetail.Code,
			'lastupdated':getTimestampmilliseconds(),
			'isSync':0,
			'createdBy': userId
            },
			// success
			function(statement) {
			
				//console.log("addClassuserEntry id="+statement.getResult().lastInsertRowID);
			
			},
			
			// error
			function error()
			{
				//console.log("Error while trying to create ClassUser item");
			}
	);
	
	
}

function getDayOfNotes()
{
	//alert('getDayOfNotes called');
	var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	var url=SystemSettings.SERVERURL +"/getdayofnotes";
	var data={
		SchoolId:schoolId,
		user_id:userId,
		lastSynch:dayOfNote
	}
	console.log("getDayOfNotes url="+url+"  Data="+ JSON.stringify(data));
	$.ajax({
		'url' : url,
		'type': 'POST',
		'data': JSON.stringify(data),
		'dataType' : "json",
		'success': function(result) {
		var timestamp=getTimestampmilliseconds();
			var details=result.details;
			console.log("getdayofnotes result : " + JSON.stringify(result));
			for(var i=0;i<details.length;i++)
			{
				updateDayOfNote(details[i]);
			}
			
			var deletedDON = result.deleted;
            $.each(deletedDON,function(index,serverrecord){
				deleteDayOfNotes(serverrecord);   
            }); 
            
            if(details.length > 0 || deletedDON.length > 0){
            	app.context.trigger(EventName.GENERATECALENDAR, SystemSettings.HARDCODDEDYEAR);
            }
			updateLastUpdate("dayOfNote",timestamp);
			
			
			
		}, 
		'error': function(response) {
			FlurryAgent.logError("DayOfNote Sync Error", "Error in fetching data from getdayofnotes web service",1);
			dataDownloadError();
			console.log("getDayOfNotes : " + JSON.stringify(response));

		}
		});
}

function deleteDayOfNotes(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "DELETE FROM dayOfNote where UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.id
            },

			// success
			function(statement) {
				//alert("DayOfNote deleted successfully");
			              
			},

			// error
			function error()
			{
				//alert("DayOfNote delete Error");
			}
	);
	
}

function updateDayOfNote(record)
{

/*****check if uniqueid exists or not*******/
app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"SELECT id FROM dayOfNote WHERE UniqueID=:UniqueID",

				// params
				{
					'UniqueID':record.id
				},

				// success
				function(statement) {
				var dayofnoteId= _.map(statement.getResult().data, function(item) {
                        return item.id;
                    });
                 if(!dayofnoteId.length)
                 {
                 	checkIdDayOfNotes(record)
                 }
                 else
                 {
                 	updateDayOfnotes(record,dayofnoteId[0])
                 }
                 
					
				},

				// error
				function error()
				{
				//console.log("Error while trying to read school profile");
				}
		);
/******************************************/

}

function checkIdDayOfNotes(record)
{
	var assignedDate=new Rocketboots.date.Day(record.date).getKey();
	assignedDate=parseInt(assignedDate);
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"SELECT id FROM dayOfNote WHERE title=:title AND date=:date",

				// params
				{
					'title':record.title,
					'date':assignedDate
				},

				// success
				function(statement) {
				var dayofnoteId= _.map(statement.getResult().data, function(item) {
                        return item.id;
                    });
                 //console.log("dayofnoteId");
                // console.log(dayofnoteId);
                 if(!dayofnoteId.length)
                 {
                 	insertDayOfnotes(record)
                 }
                 else
                 {
                 	updateDayOfnotes(record,dayofnoteId[0])
                 }
                 
					
				},

				// error
				function error()
				{
				//console.log("Error while trying to read school profile");
				}
		);
}
function updateDayOfnotes(dayOfNote,dayOfNoteId)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"UPDATE dayOfNote SET date=:date, overrideCycleDay=:overrideCycleDay, includeInCycle=:includeInCycle, isGreyedOut=:isGreyedOut, title=:title, overrideCycle=:overrideCycle, UniqueID=:UniqueID,"+
				 " SchoolId=:SchoolId, lastupdated=:lastupdated WHERE id=:dayOfNoteId",
				
						 {
							'date': new Rocketboots.date.Day(dayOfNote.date).getKey(),
							'overrideCycleDay': dayOfNote.overrideCycleDay || false,
							'includeInCycle': dayOfNote.includeInCycle != null ? dayOfNote.includeInCycle : true,
							'isGreyedOut': dayOfNote.isGreyedOut || false,
							'title': dayOfNote.title,
							'overrideCycle': dayOfNote.overrideCycle || false,
							'UniqueID':dayOfNote.id,
							'SchoolId':dayOfNote.schoolId,
							'lastupdated':getTimestampmilliseconds(),
							'dayOfNoteId':dayOfNoteId
							
						},

				// success
				function(statement) {
				//console.log("DAY OF NOTE UPDATED");
					
				},

				// error
				function error()
				{
				console.log("Error while trying to add day of note");
				}
		);
}
function insertDayOfnotes(dayOfNote)
{

app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"INSERT INTO dayOfNote (date, overrideCycleDay, includeInCycle, isGreyedOut, title, overrideCycle, UniqueID, SchoolId, lastupdated) " +
				"VALUES (:date, :overrideCycleDay, :includeInCycle, :isGreyedOut, :title, :overrideCycle, :UniqueID, :SchoolId, :lastupdated)",
				
						 {
							'date': dayOfNote.date != '0000-00-00' ? new Rocketboots.date.Day(dayOfNote.date).getKey() : new Rocketboots.date.Day().getKey(),
							'overrideCycleDay': dayOfNote.overrideCycleDay || false,
							'includeInCycle': dayOfNote.includeInCycle != null ? dayOfNote.includeInCycle : true,
							'isGreyedOut': dayOfNote.isGreyedOut || false,
							'title': dayOfNote.title,
							'overrideCycle': dayOfNote.overrideCycle || false,
							'UniqueID':dayOfNote.id,
							'SchoolId':dayOfNote.schoolId,
							'lastupdated':getTimestampmilliseconds()
							
						},

				// success
				function(statement) {
				//console.log("DAY OF NOTE ADDED");
					
				},

				// error
				function error()
				{
				console.log("Error while trying to add day of note");
				}
		);
	
}
function getSchoolInfo(uniqueId)
{
//alert("here");
//console.log("uniqueId="+uniqueId);
hideSplash=1;
//alert("alert : getSchoolInfo"+hideSplash);
if(!uniqueId)
{
//console.log("inside if");
app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"SELECT UniqueID FROM schoolProfile WHERE id = 1 ",

				// params
				{},

				// success
				function(statement) {
				var school= _.map(statement.getResult().data, function(item) {
                        return item.UniqueID;
                    });
                  //console.log("getSchoolInfo");
                   // console.log(school);
                    GetSchoolInfoServcie(school[0]);
					
				},

				// error
				function error()
				{
				//console.log("Error while trying to read school profile");
				}
		);
}
else
{
//console.log("inside else");
	GetSchoolInfoServcie(uniqueId)
}

}

function GetSchoolInfoServcie(SchoolId)
{
	//alert("GetSchoolInfoServcie");
		//console.log("*******SchoolId********="+SchoolId);
	var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	var url=SystemSettings.SERVERURL +"/getschooldetails";
	//console.log("getDayOfNotes url="+url);
	var data={
		"SchoolId":SchoolId,
		"user_id":userId
	}
	//alert("!sId="+!sId);
	if(!SchoolId)
	{
		return;
	}
	//console.log("getSchoolInfo url="+url+" ,data= "+JSON.stringify(data));
	$.ajax({
		'url' : url,
		'type': 'POST',
		'data': JSON.stringify(data),
		processData: false,
		'dataType' : "json",
		'success': function(result) {
			//console.log("getSchoolInfo");
			//alert("getschooldetails"+JSON.stringify(result));
			if(result.status=="SUCCESS")
			{
				schoolprofileServiceinfo=result.details;
				//Setting AllowSync flag to 1.
				//As per discussion with Sujatha, user can sync data with server ignoring this particular check.
				//So instead of removing all code related to this check, setting above variable to 1 will solve the problem.
				result.details.allowSynch = 1;
				updateSchoolprofile(result.details);
				
				AllowSync = schoolprofileServiceinfo.allowSynch;
				//alert("schoolprofileServiceinfo.allowSynch="+schoolprofileServiceinfo.allowSynch);
				if(timeoutBool)
				{
				//alert("schoolprofileServiceinfo.allowSynch1="+schoolprofileServiceinfo.allowSynch);
				if(schoolprofileServiceinfo.allowSynch=="1")
				{
					//alert("settimeout");
					setTimeout(function() {
								GetLastSync("dayOfNote");
	                  			getDayOfNotes();
								if(!(app.model.get(ModelName.APPLICATIONSTATE).get('appInstallStatus'))){
	                  				timeout();
	                  			}
	            		}, 5000);
	            		timeoutBool=0;
	            		
	         		}
         		}
				
				downloadSchoolBgImageFile();
				
				downloadSchoolLogoFile();
				downloadSchoolImageFile();
			}
			
			
		}, 
		'error': function(response) {
			//alert("getSchoolInfo updatefailed error");
			console.log("getSchoolInfo updatefailed error");
			FlurryAgent.logError("SchoolInfo Error", "Error in fetching data from getSchoolInfo web service",1); 
			GetAllowSync();

		}
		});
	
	downloadUserProfileImage(null);
}
function GetAllowSync()
{
	//console.log("HERE");
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                			// query
			"SELECT AllowSynch FROM schoolProfile WHERE id=1 ",

		 	// params
		 	{ },

	  		// success
   	   		function(statement) {
   	   		 var AllowSynchData= _.map(statement.getResult().data, function(item) {
       			 return item.AllowSynch;
   			 })
   			//alert("GetAllowSync");
   			 console.log(AllowSynchData);
   			 if(AllowSynchData[0]!=null)
   			 {
				 AllowSync=AllowSynchData[0];
   			 	if(AllowSynchData[0]=="1")
   			 	{
   			 		setTimeout(function() {
   			 			if(!(app.model.get(ModelName.APPLICATIONSTATE).get('appInstallStatus'))){
  							timeout();
  						}
  						GetLastSync("dayOfNote");
  						setTimeout(function() {
                  				getDayOfNotes();
                  			},5000);

					}, 5000);
					timeoutBool=0;
   			 	}
   			 }
    
			},

			// error
	  		function error()
	  		{
	  				
	  		}
		);
}
function updateSchoolprofile(schoolProfile)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"UPDATE schoolProfile " +
						"SET " +
						"AllowAutoUpdates = :AllowAutoUpdates ," +
						"RegionalFormat = :RegionalFormat, " +
						"AllowSynch = :AllowSynch, "+
						"Allow_Parent = :Allow_Parent, "+
						"Empower_Type=:Empower_Type, "+
						"includeEmpower =:Include_Empower, "+
						"UniqueID = :UniqueID, "+
						"schoolLogo = :schoolLogo, " +
						"schoolImage = :schoolImage, " + 
						"schoolBgImage = :schoolBgImage, " +
						"name = :name, " +
						"domainName = :domainName, " +
						"theme = :theme " +
						"WHERE id = 1", // there can be only one school profile

				// params
				{
					'AllowAutoUpdates' : schoolProfile.allowAutoUpdate,
					'RegionalFormat' : schoolProfile.regional_Format,
					'AllowSynch' : schoolProfile.allowSynch,
					'UniqueID' : schoolProfile.id,
					'schoolLogo' : schoolProfile.School_Logo,
					'schoolImage' : schoolProfile.School_Image,
					'schoolBgImage' : schoolProfile.school_bgImage,
					'name'		 : schoolProfile.name,
					'domainName' : schoolProfile.DomainName,
					'theme'		 : schoolProfile.theme,
					'Allow_Parent' : schoolProfile.Allow_Parent,
					'Include_Empower' : schoolProfile.include_Empower,
					'Empower_Type' : schoolProfile.Empower_Type
					
				},

				// success
				function success()
				{
					//console.log("schoolProfile update success");
					app.model.get(ModelName.SCHOOLPROFILE).set( { 'AllowAutoUpdates' : schoolProfile.allowAutoUpdate });
					app.model.get(ModelName.SCHOOLPROFILE).set( { 'RegionalFormat' : schoolProfile.regional_Format });
					app.model.get(ModelName.SCHOOLPROFILE).set( { 'AllowSynch' : schoolProfile.allowSynch });
					app.model.get(ModelName.SCHOOLPROFILE).set('allowParents', schoolProfile.Allow_Parent);
					app.model.get(ModelName.SCHOOLPROFILE).set('schoolLogo', schoolProfile.School_Logo);
					app.model.get(ModelName.SCHOOLPROFILE).set('schoolImage', schoolProfile.School_Image);
					app.model.get(ModelName.SCHOOLPROFILE).set('schoolBgImage', schoolProfile.school_bgImage);
					app.model.get(ModelName.SCHOOLPROFILE).set('name', schoolProfile.name);
					app.model.get(ModelName.SCHOOLPROFILE).set('theme', schoolProfile.theme);
					app.model.get(ModelName.SCHOOLPROFILE).set('UniqueID', schoolProfile.id);
					app.model.get(ModelName.SCHOOLPROFILE).set('domainName',schoolProfile.domainName);
					app.model.get(ModelName.SCHOOLPROFILE).set('empowerType',schoolProfile.Empower_Type);
					
					//alert('changed');
					//AllowSync=1;
					
				},

				// error
				function error()
				{
					//console.log("schoolProfile update Error");
				}
		);
}

function saveSchoolInformation(Include_Empower,Empower_Type,Allow_Parent, theme, schoolBgImage, domainName)
 {
 
  app.locator.getService(ServiceName.APPDATABASE).runQuery(
     // query
     "UPDATE schoolProfile SET Empower_Type=:Empower_Type, includeEmpower =:Include_Empower, Allow_Parent = :Allow_Parent, "+
    		 "theme = :theme, schoolBgImage = :schoolBgImage, domainName = :domainName WHERE id = 1 ",

     // params
     {
     	'Include_Empower':Include_Empower,
     	'Empower_Type':Empower_Type,
     	'Allow_Parent' : Allow_Parent,
     	'theme'	: theme,
     	'schoolBgImage' : schoolBgImage,
     	'domainName' : domainName
     },

     // success
     function(statement) {
     	//console.log("school id is set");
     },

     // error
     function error()
     {
     	console.log("Error while trying to read school profile");
     }
   );
 }

function syncUsageLog()
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT * FROM UsageLogSync",

                // params
                {},

                // success
                function(statement) {
                   var date= _.map(statement.getResult().data, function(item) {
                        return item.dateLastSynced;
                    })
					//console.log("syncUsageLog date="+date[0]);
                    if(date.length)
                    {
                       SyncUsageLogFirst(date[0]);
                    }
                    else
                    {
                        synclogs()
                    }
                },

                // error
                function error()
                {
                	//console.log("error");
                }
        );
}
function getUTCTime()
{
    var d=new Date();
    return(d.toUTCArray());
}
function convertTime(Time,Day)
{
//console.log("CONVERT TIME")
	var dayKey=Day.getKey();
	//console.log("dayKey="+dayKey);
	//console.log("Time="+Time);
	var hour=Time.toString().substring(0,2);
	var min=Time.toString().substring(2,4)
	var Year=dayKey.toString().substring(0,4);
	var month=dayKey.toString().substring(4,6);
	var day=dayKey.toString().substring(6)[1];
	var newDate=new Date(Year,month,day,hour,min,0,0);
	//console.log("newDate="+newDate);
	var newhours=newDate.getUTCHours();
	var newmin=newDate.getUTCMinutes();
	//console.log("getUTCHours()="+newDate.getUTCHours())
	//console.log("getUTCMinutes()="+newDate.getUTCMinutes());
	if(newhours.toString().length=="1")
	{
	
		newhours="0"+newhours;
		//console.log("newhours="+newhours);
	}
	if(newmin.toString().length=="1")
	{
		newmin="0"+newmin;
	}
	var returnNewTime=newhours+newmin.toString();
	//console.log("returnNewTime="+returnNewTime);
	return returnNewTime;
}

function toUTC(date) {
    return Date.UTC(
        date.getFullYear()
        , date.getMonth()
        , date.getDate()
        , date.getHours()
        , date.getMinutes()
        , date.getSeconds()
        , date.getMilliseconds()
    );
}
function synclogs()
{
	var date=getUTCTime();
                    	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                			// query
                			"INSERT INTO UsageLogSync (dateLastSynced, id) VALUES ('"+date+"',1) ",

               			 	// params
                		 	{ },

               		  		// success
               	   	   		function(statement) {
                	  		 getRecordforDate(date);
                    
                			},

                			// error
                	  		function error()
                	  		{
                	  				
                	  		}
        				);
}

function SyncUsageLogFirst(time)
{
//console.log("SyncUsageLogFirst time="+time);
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT createdDate FROM usageLog WHERE (synced = 0 AND createdDate='"+time+"') ORDER BY createdDate DESC LIMIT 1",

                // params
                {},

                // success
                function(statement) {
                   var date= _.map(statement.getResult().data, function(item) {
                        return item.createdDate;
                    })
                    //console.log("createdDate=");
                   // console.log(date);
                    if(date.length)
                    {
                        getRecordforDate(date[0]);
                    }
                    else
                    {
                    	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT createdDate FROM usageLog WHERE (synced = 0 AND createdDate >'"+time+"') ORDER BY createdDate DESC LIMIT 1",

                // params
                {},

                // success
                function(statement) {
                   var date= _.map(statement.getResult().data, function(item) {
                        return item.createdDate;
                    })
                   // console.log("createdDate=");
                   // console.log(date);
                    if(date.length)
                    {
                        getRecordforDate(date[0]);
                    }
                    
                    
                },

                // error
                function(statement) {
                   console.log("error");
                }
        );
                    }
                    
                },

                // error
                function(statement) {
                   console.log("error");
                }
        );
}

function getRecordforDate(date)
{
var createdTime=date;
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT * FROM usageLog WHERE (synced = :synced AND createdDate='"+date+"')ORDER BY usageLog.id",

                // params
                { 'synced' : false },

                function success(statement)
                {
                     var date= _.map(statement.getResult().data, function(item) {
                        return item;
                    })
                   // console.log("getRecordforDate===");
                    //console.log(date);
                    var usagelogobj=[];
                    for(var i=0;i<date.length;i++)
                    {
                        var obj={};
                        obj.clientVersion=date[i].clientVersion;
                        obj.createdDate=date[i].createdDate;
                        obj.detail=date[i].detail;
                        obj.id=date[i].id;
                        obj.macAddresses=date[i].macAddresses;
                        obj.schoolConfiguration=date[i].schoolConfiguration;
                        obj.synced=date[i].synced;
                        obj.time=date[i].time;
                        obj.token=date[i].token;
                        obj.type=date[i].type;
                        obj.version=date[i].version;
                        
                        usagelogobj.push(obj);
                        
                    }
                    //console.log(usagelogobj);
                    var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
                     var url = SystemSettings.SERVERURL + "/addusagelogs?"; 
        data = {
        'token' :  systemConfig.get('token'), 
        'macAddresses' : systemConfig.get('macAddresses'),
        'usageLogs' : JSON.stringify(usagelogobj),
        'log_created_time':createdTime,
        'clientVersion' : systemConfig.get('versionNumber'),
		'userid' : app.model.get(ModelName.STUDENTPROFILE).get('UniqueID')
        }
        ajaxType = 'POST'; 
        
        
        //console.log("TheCloud.callService: addusagelogs url=" + url + ", type=" + ajaxType + " data=");
       // console.log(JSON.stringify(data));
                
        $.ajax({
            'url' : url,
            'type': ajaxType,
            'data': JSON.stringify(data),
            'dataType' : "json",
            'success': function(result) {
               // console.log("TheCloud.response addusagelogs: ", JSON.stringify(result));
                //successCallback(result);
                setsync(usagelogobj,createdTime);     
            }, 
            'error': function(response) {
                //console.log("addusagelogs TheCloud.error: ", JSON.stringify(response));
                //errorCallback("Error contacting server");
            }
        });   
                    
                },

                // error
                function(statement) {
                   console.log("error");
                }
        );
}

function setsync(usagelogobj,createdTime)
{
   // console.log("usagelogobj");
   // console.log(usagelogobj)
    for(var i=0;i<usagelogobj.length;i++)
    {
        var id=usagelogobj[i].id;
        app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "UPDATE usageLog SET synced = 1 WHERE id="+id,

                // params
                {},

                // success
                function(statement) {
                  // console.log("synced updated")
                    
                },

                // error
                function(statement) {
                 //  console.log("error");
                }
        );
        
    }
    updateDateSynced(createdTime);
    
}
function updateDateSynced(time)
{
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "UPDATE UsageLogSync SET dateLastSynced = '"+time+"' WHERE id=1",

                // params
                {},

                // success
                function(statement) {
                  // console.log("UsageLogSync updated");
                   SyncUsageLogtime(time);
                    
                },

                // error
                function(statement) {
                  // console.log("error");
                }
        );
}

function SyncUsageLogtime(time)
{
//console.log("time="+time);
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT createdDate FROM usageLog WHERE (synced = 0 AND createdDate>'"+time+"') ORDER BY createdDate DESC LIMIT 1",

                // params
                {},

                // success
                function(statement) {
                   var date= _.map(statement.getResult().data, function(item) {
                        return item.createdDate;
                    })
                   // console.log("createdDate=");
                   // console.log(date);
                    if(date.length)
                    {
                        getRecordforDate(date[0]);
                    }
                    
                },

                // error
                function(statement) {
                  // console.log("error");
                }
        );
}


function deleteClassUser()
{
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT class.id as classId, ClassUser .cycleDay ,ClassUser .UniqueID ,t.name, r.Name , p.title  from class"+
				" JOIN ClassUser ON  class.id=ClassUser .classId"+
				" JOIN timetable as t ON class.timetable =t.id"+
				" JOIN Room AS r ON r.id= class.RoomId "+
				" JOIN period AS p ON p.id=class.periodId"+
				" WHERE ClassUser.isDelete =1 ",

                // params
                {
                
                },

                // success
                function(statement) {
                   var details= _.map(statement.getResult().data, function(item) {
                        return item;
                    })
                   //console.log("deleteClassUser=");
                   //console.log(detail[0]);                   
                  
                   if(details.length)
                  {
                    var detailsArray=[];
                    for(var i=0;i<details.length;i++)
                    {
                   var  detail={
                  		'cycleDay':details[i].cycleDay,
                   		'classUserId':details[i].UniqueID,
                   		'timetableName':details[i].name,
                   		'room_name':details[i].Name,
                   		'period_name':details[i].title
                   }
                   detailsArray.push(detail);
                   }
                    var Data={
                    	'userId':UserId,
                    	'details':detailsArray
                   		
                    }
                    
                    //console.log("delete final data");
                    //console.log(JSON.stringify(Data));
                    	 var url=SystemSettings.SERVERURL +"/deleteclassuser";
                    	 $.ajax({
            					'url' : url,
           						 'type': 'POST',
            				     'data': JSON.stringify(Data),
                                 'dataType' : "json",
                                 'success': function(result) {
                                             //console.log("deleteclassuser success");
                                             //console.log(JSON.stringify(result));
				                    		 for(var i=0;i<details.length;i++)
				                             {
				                    			//alert("deleteclassuser " + details[i].classId);
				                             	deleteClassuserId(details[i].classId);   
				                             }
           							 }, 
           						 'error': function(response) {
           								FlurryAgent.logError("Classuser Sync Error", "Error in fetching data from deleteclassuser web service",1);
                						//console.log("deleteclassuser TheCloud.error: ", JSON.stringify(response));
            							}
        						});
        						
        						
        		}
                   
                    
                    
                },

                // error
                function(statement) {
                  // console.log("error");
                }
        );
}

function getPeriodName(data)
{
	//console.log("getPeriodName");
	//console.log(JSON.stringify(data));
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "SELECT title FROM period WHERE id=:periodId",

                // params
                {
                'periodId':data.periodId
                },

                // success
                function(statement) {
                   
                  // console.log("getPeriodName success");
                   var detail= _.map(statement.getResult().data, function(item) {
                        return item.title;
                    });
                    if(detail.length)
                    {
                   var detailsArray=[];
                   var  detail={
                  		'cycleDay':data.cycleDay,
                   		'classUserId':data.classUserId,
                   		'timetableName':data.timetableName,
                   		'room_name':data.room_name,
                   		'period_name':detail[0]
                   }
                   detailsArray.push(detail);
                    var Data={
                    	'userId':data.userId,
                    	'details':detailsArray
                   		
                    }
                    
                    //console.log("delete final data");
                    //console.log(JSON.stringify(Data));
                    	 var url=SystemSettings.SERVERURL +"/deleteclassuser";
                    	 $.ajax({
            					'url' : url,
           						 'type': 'POST',
								 'timeout' : 120000,
            				     'data': JSON.stringify(Data),
                                 'dataType' : "json",
                                 'success': function(result) {
                                             // console.log("deleteclassuser success");
                                             // console.log(JSON.stringify(result));
                                              deleteClassuserId(data.id);   
           							 }, 
           						 'error': function(response) {
           								FlurryAgent.logError("Classuser Sync Error", "Error in fetching data from deleteclassuser web service",1);
                						//console.log("deleteclassuser TheCloud.error: ", JSON.stringify(response));
            							}
        						});
                    }
                    
                },

                // error
                function(statement) {
                  // console.log("error");
                }
        );
}
function deleteClassuserId(id)
{
app.locator.getService(ServiceName.APPDATABASE).runQuery(
                // query
                "DELETE  FROM ClassUser WHERE id=:classId",

                // params
                {
                'classId':id
                },

                // success
                function(statement) {
                   
                   //console.log("deleteClassUser success");
                  app.locator.getService(ServiceName.APPDATABASE).runQuery(
				// query
				"DELETE FROM class WHERE class.id = :classId",

				// params
				{ 'classId' : id },
				
				// success
				function success()
				{
					//console.log("class deleted");
				},
				
				// error
				function error()
				{
				//console.log("Failed to remove class");
				}
			);
                    
                },

                // error
                function(statement) {
                  // console.log("error");
                }
        );
}

function updateUserProfile(userId,firstName,lastName,dob,telePhone, grade, email, imageUrl)
{
	
	var schoolFormat = app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat');
	console.log("originalFilePath : "+imageUrl);
	console.log("dob : "+dob);
	var imgName = ""
	if(imageUrl != ""){
		var userProfileImg = imageUrl.split("\\");
		imgName = userProfileImg[userProfileImg.length-1];
	}
	

	var date = "";
	var dateOfBirth = "";
		if(dob != ""){
			date = dob.split("/")
			if (schoolFormat == "USA")
				dateOfBirth = date[2] + "-" + date[0] + "-" + date[1];
			else
				dateOfBirth = date[2] + "-" + date[1] + "-" + date[0];
		}
		
		   	
		//console.log("dateOfBirth : "+dateOfBirth);
		var student =  new diary.model.StudentProfile();
		student.set(app.model.get(ModelName.STUDENTPROFILE).attributes);
		student.set('name', firstName);
		student.set('lastName', lastName);
		student.set('dateOfBirth', dateOfBirth);
		student.set('telephone', telePhone);
		student.set('profileImage', imgName);
		
		if(student.isValid()){
			if(imageUrl != ""){
				uploadProfileImage(imageUrl);
			}
		    var data = '{"userId":'+userId+',"FirstName":"'+firstName+'","LastName":"'+lastName+'","DateOfBirth":"'+dateOfBirth+'","Telephone":"'+telePhone+'"}';
		  
			var url=SystemSettings.SERVERURL +"/updateprofile"
		   
		    $.ajax({
	            'url' : url,
	            'type': 'POST',
	            'data': JSON.stringify(data),
	            'dataType' : "json",
	            'success': function(result) {
		    		
				 	$("#spanFirstName").text(firstName);
				 	$("#spanLastName").text(lastName);
				 	$("#spanDob").text(dob);
				 	$("#spanTelephone").text(telePhone);
				 	$("#nameTextBox").val(firstName);
			        $("#lastNameTextBox").val(lastName);
			        $("#dateOfBirthTextBox").val(dob);
					$("#telephoneTextBox").val(telePhone);
					console.log("imgName : "+imgName);
					var studentProfile = app.model.get(ModelName.STUDENTPROFILE);
				 	app.context.trigger(EventName.UPDATESTUDENTPROFILE, student);
				 	
				 	openMessageBox("Profile updated successfully");
				 
				 }, 
	            'error': function(response) {
					FlurryAgent.logError("Profile Update Error", "Error in updating user profile",1);
	                console.log("update user profile error: ", JSON.stringify(response));
	            }
	        });
        }
		else{
			openMessageBox("Invalid user details");
		 	var student = app.model.get(ModelName.STUDENTPROFILE);
		 	var date = (student.get("dateOfBirth") != "" && student.get("dateOfBirth") != null && student.get("dateOfBirth") != "0000-00-00") ? student.get("dateOfBirth").split("-") : "";
		 	var dateOfBirth = "";
		 	if(date != ""){
		 		 if (schoolFormat == "USA")
		 			 dateOfBirth = date[2] + "/" + date[0] + "/" + date[1];
		 		 else
		 			 dateOfBirth = date[2] + "/" + date[1] + "/" + date[0];
			}
		 	//console.log("dateOfBirth 222: "+dateOfBirth);
		 	$("#spanFirstName").text(student.get("name"));
		 	$("#spanLastName").text(student.get("lastName"));
		 	$("#spanDob").text(dateOfBirth);
		 	$("#spanTelephone").text(student.get("telephone"));
            $("#nameTextBox").val(student.get("name"));
            $("#lastNameTextBox").val(student.get("lastName"));
			$("#dateOfBirthTextBox").val(dateOfBirth);
			$("#telephoneTextBox").val(student.get("telephone"));
			console.log("Invalid user details");
		}
	
}
function uploadProfileImage(originalFilePath)
{
	 //console.log("UserId"+UserId);
	 // console.log("originalFilePath2"+originalFilePath);
	 
	    var request="";
	    //console.log("UserId"+UserId);
	    //console.log("originalFilePath2"+originalFilePath);
        url = SystemSettings.SERVERURL + "/uploadimage?userId="+UserId;  
       // console.log("image uplaod="+url);
        boundary = '--------------======-------------------AaB03x';

        request = new air.URLRequest(url);
        request.useCache = false;
        request.contentType = 'multipart/form-data, boundary='+boundary;
        //request.shouldCacheResponse = false;
        request.method='POST';

        buffer = new air.ByteArray();
        file = new air.File(originalFilePath);
        fileStream = new air.FileStream();
        fileStream.open(file, air.FileMode.READ);
        fileContents = new air.ByteArray();
        fileStream.readBytes(fileContents, 0, file.size);
        fileStream.close();
        
        //console.log('fileStream ' + fileStream);
        //console.log('fileContents ' + fileContents);
        
      
        buffer.writeUTFBytes( "--"+boundary+"\r\n" );
        buffer.writeUTFBytes( "content-disposition: form-data; name=\"Filedata\"; filename=\""+file.name+"\"\r\n" );
        buffer.writeUTFBytes( "Content-Transfer-Encoding: binary\r\n" );
        buffer.writeUTFBytes( "Content-Length: "+file.size+"\r\n" );
        buffer.writeUTFBytes( "Content-Type: application/octet-stream\r\n" );
        buffer.writeUTFBytes( "\r\n" );

        buffer.writeBytes(fileContents, 0, fileContents.length);
        buffer.writeUTFBytes( "\r\n--"+boundary+"--\r\n" );

        request.data = buffer;
        var loader = new air.URLLoader();
        loader.addEventListener(air.ProgressEvent.PROGRESS , callback_for_upload_progress);
        loader.addEventListener(air.IOErrorEvent.IO_ERROR , function (e){
	        //console.log("Event Error");
	        //alert("Event Error"+e.text);
	        //air.trace( 'error: '+ e.text );                
        });
        loader.addEventListener(air.Event.COMPLETE, callback_for_upload_finish);
       
        loader.load( request );
       
        
}


function callback_for_upload_progress(event) { 
	    var loaded = event.bytesLoaded; 
	    var total = event.bytesTotal; 
	    var pct = Math.ceil( ( loaded / total ) * 100 ); 
	    //air.trace('Uploaded ' + pct.toString() + '%');
}
function callback_for_upload_finish(event) {
	    var response1=event.currentTarget.data;
	   // console.log("response1 " + JSON.stringify(response1));
	   response1=$.parseJSON(response1);
//console.log("response2 " + JSON.stringify(response1));
	 //   console.log('response2'+response1.detials);
	    
	    
	  if(response1.ErrorCode == '1'){
		  downloadUserProfileImage(response1.detials);
		  updateProfileImage(response1.detials);
	  }
	  else{
		  openMessageBox("Error in updating profile image");
	  }
	
		 	
	  
}
function updateProfileImage(imageName)
{
  	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE  User SET  ProfileImage=:profileImage",
            // params
            {
            	'profileImage' :imageName,
            },
	      // success
	      function(statement) {
	           app.model.get(ModelName.STUDENTPROFILE).set("profileImage", imageName);
	           setTimeout(function(){
		   			console.log("update page");
			  		var activeSubTab = $("#subTabProfileView .tabs_container").find(".active");
					$(".myProfileNavigation").find(".active").click();
			   },7000);
        	
	      },
	      
	      // error
	      function error()
	      {
	       console.log("Error while trying to update user profile image");
	      }
	  );
  }	

function updateclassUser(classModel,subjectColour)
{
  //console.log("updateclassUser");
  //console.log(classModel);
  app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE  ClassUser SET  userId=:userId, cycleDay=:cycleDay, code=:code, lastupdated=:lastupdated, isSync=:isSync WHERE classId=:classId,",

            // params
            {

            
	      'classId' :classModel.id,
	      'userId':UserId,
	      'cycleDay':classModel.get('cycleDay'),
	      'code':subjectColour,
	      'lastupdated':getTimestampmilliseconds(),
	      'isSync':0
            },
      // success
      function(statement) {
      
       // console.log("updateclassUser ");
      
      },
      
      // error
      function error()
      {
       // console.log("Error while trying to create ClassUser item");
      }
  );
  }
  
  
 function getUserDetail()
 {
	
	 var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	
	 	//TODO : call userDetails service
	 	//var url=SystemSettings.SERVERURL +"/Desktop/getprofile"
	 	var url = SystemSettings.SERVERURL + '/userdetails';
	 	var data = '{"user_id":'+userId+',"email_id":""}';
		$.ajax({
	            'url' : url,
	            'type': 'POST',
	            'data': JSON.stringify(data),
	            'dataType' : "json",
	            'success': function(result) {
					 	console.log("getUser details : "+JSON.stringify(result));
					 	var studentDetails = result.user_details;
					 	var address = result.address;
						//alert("alert : getUser details : "+JSON.stringify(address));
						
					 	var addressDetails = new diary.model.Address({
							'street' : address.Address_line_1 + address.Address_line_2,
							'suburb' : address.CityTownSuburb,
							'state' : address.state_name,
							'postcode' : address.PostalCode,
							'country' : address.country_name
						});
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('name',studentDetails.FirstName);
					 	var isActive = false;
					 	if("\1" === studentDetails.Status || studentDetails.Status == '1'){
					 		(app.model.get(ModelName.STUDENTPROFILE)).set('status',true);
					 	} else if("\0" === studentDetails.Status || studentDetails.Status == '0'){
					 		(app.model.get(ModelName.STUDENTPROFILE)).set('status',false);
					 		isActive = true;
					 	}
						(app.model.get(ModelName.STUDENTPROFILE)).set('lastName',studentDetails.LastName);
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('email',studentDetails.Email);
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('telephone',studentDetails.Telephone);
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('dateOfBirth',studentDetails.DateOfBirth);
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('profileImage',studentDetails.ProfileImage);
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('timeZone',studentDetails.TimeZone);
						(app.model.get(ModelName.STUDENTPROFILE)).set('grade',studentDetails.grade_name);
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('address', new diary.model.Address({
					 		'street' : address.Address_line_1 + address.Address_line_2,
							'suburb' : address.CityTownSuburb,
							'state' : address.state_name,
							'postcode' : address.PostalCode,
							'country' : address.country_name
						}));
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('schoolId',studentDetails.SchoolId);
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('UniqueID',studentDetails.Id);
					 	(app.model.get(ModelName.STUDENTPROFILE)).set('EncrptedUserId',studentDetails.EncrptedUserId);
					 	
					 	//alert("app.model.get(ModelName.STUDENTPROFILE) : "+JSON.stringify(app.model.get(ModelName.STUDENTPROFILE)));
	            
					 	//updatestudentprofile((app.model.get(ModelName.STUDENTPROFILE)))
					 	app.context.trigger(EventName.UPDATESTUDENTPROFILE, app.model.get(ModelName.STUDENTPROFILE));
					 	// console.log("getUserDetail TheCloud.error: ", JSON.stringify(app.model.get(ModelName.STUDENTPROFILE)));
					 	if(isActive){
					 		alert('Sorry, your account is not active. Please contact your school administrator.');
							app.context.trigger(EventName.EXITAPPLICATION);
						}
	            }, 
	            'error': function(response) {
	            	//alert("getUser details error : "+JSON.stringify(response));
	                console.log("getUserDetail TheCloud.error: ", JSON.stringify(response));
	                FlurryAgent.logError("Userdetails Sync Error", "Error in fetching data from userdetails web service",1);
	                //dataDownloadError();
	            }
	        });
 }
 
 function updatestudentprofile(details)
 {
 
//console.log("updatestudentprofile");
 //console.log(details)
 	if(details.DateOfBirth)
	 var dob=new Rocketboots.date.Day(details.DateOfBirth).getKey()
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE User SET " +
						"name = :name, " +
						"dateOfBirth = :dob, " +
						"LastName = :LastName "+
						"WHERE id = 1", // id will always be 1

            // params
            {
     			 'name' :details.FirstName,
      			 'dob':dob,
     			 'LastName':details.LastName
            },
      // success
      function(statement) {
      
      //  console.log("updatestudentprofile success ");
        app.model.get(ModelName.STUDENTPROFILE).set('name',details.FirstName);
        app.model.get(ModelName.STUDENTPROFILE).set('dateOfBirth',dob); 
        app.model.get(ModelName.STUDENTPROFILE).set('lastName',details.LastName); 
      
      },
      
      // error
      function error()
      {
        console.log("Error while trying to create ClassUser item");
      }
  );
 }
 
 function disablediaryitemformDay()
 {
 						   $("#dayDiaryItemFormEditPopup .attachment").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup input[type='text']").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup textarea").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup select").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup .startTime").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup .assignedTime").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup .endTime").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup .allDayContainer input").attr("disabled", "disabled");
 }
 function disablediaryitemformWeek()
 {
 	                       $("#dayDiaryItemFormEditPopup .attachment").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup input[type='text']").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup textarea").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup select").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup .startTime").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup .assignedTime").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup .endTime").attr("disabled", "disabled");
                           $("#dayDiaryItemFormEditPopup .allDayContainer input").attr("disabled", "disabled");
 }
 
 
 function deleteClassUser2(id)
 {
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE ClassUser SET isDelete=1 WHERE classId=:classId", // id will always be 1

            // params
            {
     			 'classId' :id
            },
      // success
      function(statement) {
      
        //console.log("deleteClassUser success ");
       
      
      },
      
      // error
      function error()
      {
       // console.log("Error while trying to create ClassUser item");
      }
  );
 }
 
 
 function exportdiaryitems()
 {
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            'SELECT * FROM diaryItem WHERE ifnull (diaryItem.isSync,"")=""',

            // params
            {
            },
      // success
      function(statement) {
      
       var details= _.map(statement.getResult().data, function(item) {
                        return item;
                    })
         console.log("exportData DiaryItem details");
         console.log(details);
         console.log(details.length);           
       for(var i=0;i<details.length;i++)
       {
       		insertUpdateDiaryItem(details[i])
       }
        
       
      
      },
      
      // error
      function error()
      {
       // console.log("Error while trying to create ClassUser item");
      }
  );
 }
 
 function insertUpdateDiaryItem(diaryItem)
 {
	 
	 var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	 		var schoolID=app.model.get(ModelName.SCHOOLPROFILE).get('UniqueID');
 	
		   	app.locator.getService(ServiceName.APPDATABASE).runQuery(
			              // query
							  "UPDATE diaryItem SET lastupdated=:lastupdated,isSync=:isSync,isDelete=:isDelete,createdBy=:createdBy,UserID=:UserID,SchoolId=:SchoolId"+
							  " WHERE diaryItem.id=:id",

							                  // params
							                  {
							                      'lastupdated' : getTimestampmilliseconds(),
							                      'isSync':0,
							  					  'isDelete':0,
							  					  'createdBy':userId,
							  					  'UserID':userId,
							  					  'SchoolId':schoolID,
							  					  'id':diaryItem.id
							                  },
				        // success
				        function(statement) {
				        // console.log("UPDATE  Success");
				        },
      
				        // error
				        function error()
				        {
				         // console.log("Error while trying to create ClassUser item");
				        }
				    );
       
      
     
 }
 function updateNoteEntryRecord()
 {
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            'SELECT * FROM noteEntry WHERE (ifnull (noteEntry.isSynch,"")="")',

            // params
            {
            },
      // success
      function(statement) {
      
       var details= _.map(statement.getResult().data, function(item) {
                        return item;
                    })
         //console.log("exportData details");
      
       for(var i=0;i<details.length;i++)
       {
//        console.log("details while update : "+details[i].toString());           
       		app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            'UPDATE noteEntry SET lastupdated=:lastupdated,isDelete=:isDelete,isSynch=:isSynch  WHERE noteEntry.id=:noteId',

            // params
            {
            'noteId':details[i].id,
            'lastupdated':getTimestampmilliseconds(),
            'isDelete':0,
            'isSynch':0,
            
            },
      // success
      function(statement) {
      
      //console.log("NOTEENTRY UPDATE SUCCESS");
        
      
      },
      
      // error
      function error()
      {
       // console.log("Error NOTEENTRY UPDATE SUCCESS");
      }
  );
       }
        
      
      },
      
      // error
      function error()
      {
       // console.log("Error while trying to create ClassUser item");
      }
  );
 }
 function exportClass()
 {
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            'SELECT * FROM class WHERE ifnull (class.isSync,"")=""',

            // params
            {
            },
      // success
      function(statement) {
      
       var details= _.map(statement.getResult().data, function(item) {
                        return item;
                    })
         //console.log("exportData details");
        // console.log(details);           
       for(var i=0;i<details.length;i++)
       {
       		insertUpdateRoom(details[i],insertRoomSuccess,insertRoomError);
       		function insertRoomSuccess(id)
       		{
       			//alert("insertRoomSuccess"+id)
       			UpdateRoomID(details[i].id,id);
       			
       		}
       		function insertRoomError()
       		{
       			
       		}
       		getSubjectColorCode(details[i].subjectId,getSubjectColorCodeSuccess,getSubjectColorCodeError)
       }
       function getSubjectColorCodeSuccess(Code)
       {
       	//console.log("getSubjectColorCodeSuccess="+Code);
       	AddclassUserEntry(details[i],Code);
       	updateClassRecord(details[i].id,Code);
       }
       function getSubjectColorCodeError()
       {
       	
       }
        
       
      
      },
      
      // error
      function error()
      {
       // console.log("Error while trying to create ClassUser item");
      }
  );
 }
 function AddclassUserEntry(record,Code)
 {
	 console.log(" AddclassUserEntry record 1"+JSON.stringify(record));
 	var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
 	//console.log("SHRUI UserId="+UserId);
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO ClassUser (classId, userId, cycleDay, code, lastupdated, isSync, UniqueID, createdBy) VALUES (:classId, :userId, :cycleDay, :code, :lastupdated, :isSync, :UniqueID, :createdBy)",

            // params
            {
			'classId' :record.id,
			'userId':userId,
			'cycleDay':record.cycleDay,
			'code':Code,
			'lastupdated':getTimestampmilliseconds(),
			'isSync':0,
	    	'UniqueID':null,
	    	'createdBy':record.createdBy
            },
			// success
			function(statement) {
			
				//console.log("AddclassUserEntry id="+statement.getResult().lastInsertRowID);
			
			},
			
			// error
			function error()
			{
				console.log("Error AddclassUserEntry");
			}
	);
 }
 function getSubjectColorCode(subjectId,successCallback,errorCallback)
 {
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT colour FROM subject WHERE id=:subjectId",

								// params
								{
									'subjectId' : subjectId,
								},
     	 // success
      		function(statement) {
      			var subjectColor=_.map(statement.getResult().data, function(item) {
                        return item.colour;
                    })
                    if(subjectColor.length)
                    {
                    	if(subjectColor[0]!=null)
                    	{
                    		successCallback(subjectColor[0]);
                    	}
                    }
      		},
      		function error()
      		{
      			console.log("Room ID update Error");
      		}
     	);
 }
 function UpdateRoomID(classId,RoomID)
 {
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE class SET RoomId=:RoomId WHERE id=:classId",

								// params
								{
									'RoomId' : RoomID,
									'classId' :classId
								},
     	 // success
      		function(statement) {
      			console.log("Room ID update Success");
      		},
      		function error()
      		{
      			console.log("Room ID update Error");
      		}
     	);
 }
 function updateClassRecord(classId,Code)
 {
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE class SET Code=:Code,isSync=:isSync,lastupdated=:lastupdated WHERE id=:classId",

								// params
								{
									'Code' : Code,
									'classId' :classId,
									'isSync':1,
									'lastupdated':getTimestampmilliseconds()
								},
     	 // success
      		function(statement) {
      			//console.log("UPDATE CLASS Success");
      		},
      		function error()
      		{
      			console.log("UPDATE CLASS Success");
      		}
     	);
 }
 function insertUpdateRoom(details,successCallback,errorCallback)
 {
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            'SELECT id FROM Room WHERE Room.Name=:roomName',

            // params
            {
            'roomName':details.room.toUpperCase()
            },
      // success
      function(statement) {
      
       var Roomdetail= _.map(statement.getResult().data, function(item) {
                        return item.id;
                    })
       // console.log("Roomdetail");
       // console.log(Roomdetail);
        
       if(Roomdetail.length)
       {
       		successCallback(Roomdetail[0])
       }
       else
       {
       		app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO Room (SchoolId, CampusId, Name, Code, UniqueID, lastupdated) " +
										"VALUES (:SchoolId, :CampusId, :Name, :Code, :UniqueID, :lastupdated )",

								// params
								{
									'SchoolId' : 44,
									'CampusId' : 2,
									'Name' : details.room.toUpperCase(),
									'Code' : "#0000",
									'UniqueID' : 12,
									'lastupdated' : getTimestampmilliseconds()
								},
     	 // success
      		function(statement) {
      		
      			successCallback(statement.getResult().lastInsertRowID)
      		},
      		function error()
      		{
      		}
     		 );
       }
      
      },
      
      // error
      function error()
      {
       // console.log("Error while trying to create ClassUser item");
      }
  );
 }
 function updateSubjectRecords()
 {
  	app.locator.getService(ServiceName.APPDATABASE).runQuery(
             // query
             "SELECT * FROM subject",

             // params
             {
             },
       // success
       function(statement) {
      
        var details= _.map(statement.getResult().data, function(item) {
                         return item;
                     })
         // console.log("updateSubjectRecords details");
          //console.log(details);           
        for(var i=0;i<details.length;i++)
        {
			var color=details[i].colour;
			var s=isHexCode(color)
			if(s)
			{

			//console.log("color="+color);
			var hexCode=getHexCode(color ? color.toUpperCase() : "");
		 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
		            // query
		            "UPDATE subject SET colour=:colour,name=:name WHERE id=:id",

		            // params
		            {
						'colour':hexCode,
						'id':details[i].id,
						'name':details[i].name.toUpperCase()
		            },
		      // success
		      function(statement) {
      
				 // console.log("update subject success")
       
      
		      },
      
		      // error
		      function error()
		      {
		       // console.log("Error while trying to update subject");
		      }
		  );
		  }
		  
        }
             
       },
      
       // error
       function error()
       {
        // console.log("Error while trying to get subjects");
       }
   );
 }
 function updateClassuserEntry(classuserDetail,classModelId)
 {
 console.log("INSIDE UPDATECLASSUSERENTRY");
// console.log("classuserDetail");
 console.log(classuserDetail);
// console.log(classModelId);
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
		            // query
		            "UPDATE ClassUser SET code=:code,isSync=:issync WHERE classId=:id",

		            // params
		            {
						'code':classuserDetail.Code,
						'id':classModelId,
						'issync':0
		            },
		      // success
		      function(statement) {
      
				  console.log("update class user success")
       
      
		      },
      
		      // error
		      function error()
		      {
		       // console.log("Error while trying to update classUser");
		      }
		  );
 }

function updateAttachments()
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
		            // query
		            "SELECT di.id as diaryid,di.type,di.title,di.description,di.created,di.assignedday,"+
            "di.assignedtime,di.dueday,di.duetime,di.completed,di.subjectid,di.startday,di.starttime,"+
            "di.endtime,di.messagefrom,di.messagefromtext,di.lastupdated,di.UniqueID,at.originalFilePath,"+
            "at.displayName,at.fileType,at.localFileName,at.dateAttached,at.id "+
            "FROM diaryItem as di LEFT OUTER JOIN attachment as at ON at.diaryItemId = di.id WHERE Universal_ID is null",

		            // params
		            {
						'code':classuserDetail.Code,
						'id':classModelId,
						'issync':0
		            },
		      // success
		      function(statement) {
      
				  console.log("update class user success")
      
		      },
      
		      // error
		      function error()
		      {
		       // console.log("Error while trying to update classUser");
		      }
		  );
}

function saveMacAdress(macId)
{
	console.log("saveMacAdress macId="+macId);
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
		            // query
		            "UPDATE systemConfig SET MacId=:MacId WHERE id=1",

		            // params
		            {
						'MacId':macId
						
		            },
		      // success
		      function(statement) {
      
				  console.log("saveMacAdress success")
				  app.model.get(ModelName.SYSTEMCONFIGURATION).set('macAddresses',macId);
       
      
		      },
      
		      // error
		      function error()
		      {
		        console.log("Error while trying to saveMacAdress");
		      }
		  );
}
function getMacAdress(successCallback,errorCallback)
{
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
		            // query
		            "SELECT  MacId FROM systemConfig WHERE id=1",

		            // params
		            { },
		      // success
		      function(statement) {
      
				  console.log("getMacAdress success")
		          var MacId= _.map(statement.getResult().data, function(item) {
		                           return item.MacId;
		                       })
							   console.log(MacId);
				  successCallback(MacId[0]);
      
		      },
      
		      // error
		      function error()
		      {
		        console.log("Error while trying to saveMacAdress");
				errorCallback("Error while trying to saveMacAdress");
		      }
		  );
}

function parseEvents(record)
{
	console.log("inside parseEvents");
	console.log(record);
	checkEventExists(record,parseEventsuccess,parseEventserror);
	
	function parseEventsuccess(id)
	{
		console.log("parseEventsuccess="+id);
		if(id!=null){
			console.log("ifff event != null");
			updateEventRecord(id,record);
		}
			
		else{
			console.log("else event != null");
			insertEventRecord(record);
		}
		
	}
	function parseEventserror(error)
	{
		
	}
	
	record = null;
}

function checkEventExists(record,successCallback,errorCallback)
{
		var startDate = record.startDate;
	    if(startDate==null || startDate==0)
		{
		
		}
		else
		{
			startDate=new Rocketboots.date.Day(startDate).getKey();
			startDate=parseInt(startDate);
		}
	    //console.log("title : "+record.title+"startDay : "+startDate);
	    app.locator.getService(ServiceName.APPDATABASE).runQuery(
		            // query
		            "SELECT id FROM diaryItem WHERE type = 'EVENT' AND (UniqueID = :uniqueid OR (title=:title AND startDay=:startDate))",

		            // params
		            { 
						'title':record.title,
						'startDate': startDate,
						'uniqueid':record.uniqueid,
					},
		      // success
		      function(statement) {
      
	            var eventId= _.map(statement.getResult().data, function(item) {
	                           return item.id;
	                       })
				if(eventId[0]!=null)
				{
				  successCallback(eventId[0]);
			  	}
				else
				{
					successCallback(null);
				}
      
		      },
      
		      // error
		      function error()
		      {
				errorCallback("Error while fetching to events");
		      }
		  );
}

function checkRecordExists(title,startDate,successCallback,errorCallback)
{
	    if(startDate==null || startDate==0)
		{
		
		}
		else
		{
			startDate=new Rocketboots.date.Day(startDate).getKey();
			startDate=parseInt(startDate);
		}
 	app.locator.getService(ServiceName.APPDATABASE).runQuery(
		            // query
		            "SELECT id FROM diaryItem WHERE title=:title AND startDay=:startDate",

		            // params
		            { 
						'title':title,
						'startDate':startDate,
						//'userId':UserId,
					},
		      // success
		      function(statement) {
      
				  console.log("getMacAdress success")
		          var eventId= _.map(statement.getResult().data, function(item) {
		                           return item.id;
		                       })
				console.log("AdminEVENTID="+eventId[0]);
				if(eventId[0]!=null)
				{
				  successCallback(eventId[0]);
			  	}
				else
				{
					successCallback(null);
				}
      
		      },
      
		      // error
		      function error()
		      {
		        console.log("Error while trying to saveMacAdress");
				errorCallback("Error while trying to saveMacAdress");
		      }
		  );
}
function updateEventRecord(id,record)
{
	console.log("updateEventRecord : ");
	var completed=record.completed;
		var assignedDate=record.assignedDate;
		if(assignedDate==null || assignedDate==0)
		{
		
		}
		else
		{
			assignedDate=new Rocketboots.date.Day(record.assignedDate).getKey();
			assignedDate=parseInt(assignedDate);
		}
	
	
		var startDay=record.startDate;
		if(startDay==null || startDay==0)
		{
		
		}
		else
		{
			startDay=new Rocketboots.date.Day(record.startDate).getKey();
			startDay=parseInt(startDay);
		}
	
	
		var dueDate=record.dueDate;
		if(dueDate==null || dueDate==0)
		{
		
		}
		else
		{
			dueDate=new Rocketboots.date.Day(record.dueDate).getKey();
			dueDate=parseInt(dueDate);
		}
	
	
		var assignedTime=record.assignedTime;
		console.log("record.assignedTime="+record.assignedTime);
		assignedTime=formateTimes(record.assignedTime);
		console.log("assignedTime="+assignedTime);
		
		var startTime=record.startTime;
			startTime=formateTimes(record.startTime);
			if(startTime == 0){
				if(record.onTheDay == '1'){
					startTime = null;
				}
			}
		console.log("startTime="+startTime);
	
		var endTime=record.endTime;
		console.log("record.endTime="+record.endTime);
			endTime=formateTimes(record.endTime);
		console.log("update endTime="+endTime);
		if(endTime == 0){
			if(record.onTheDay == '1'){
				endTime = null;
			}
		}
	
		var dueTime=record.dueTime;
			dueTime=formateTimes(record.dueTime);
		console.log("assignedDate=="+assignedDate);
		console.log("startDay=="+startDay);
		console.log("assignedTime=="+assignedTime);
		var subjectid=record.subjectId;
		console.log("subjectid=="+subjectid);
		if(subjectid==0)
		{
			subjectid=null;
		}
		var itemcompleted=record.completed;
		if(itemcompleted==0)
		{
			itemcompleted=null;
		}
		console.log("itemcompleted=="+itemcompleted);
		if(completed.toString() == "0")
		{
			completed=null;
		}
	/*****/
	var locked=null;
	if(record.type=="EVENT")
	{
		if(record.createdBy!=UserId)
		{
			locked=1;
		}
	}
	
	var assignedToMe = 1;
	if(record.publishToClass == '1'){
		assignedToMe = 0;
	}
	console.log("locked=="+locked);
	/*****/
	console.log("update event record : ");
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "UPDATE diaryItem " +
	            "SET hideFromDue = 0, hideFromAssigned = 0, hideFromNotifications = 0, type = :type, title = :title, description = :description, completed = :completed, progress = :progress, EstimatedHours = :EstimatedHours, type_id = :type_id, type_name = :type_name,"+
	                "EstimatedMins = :EstimatedMins, priority = :priority, subjectId = :subjectId, ClassId = :ClassId, SchoolId = :SchoolId, UserID = :UserID, isSync = :isSync, startDay = :startDay, dueDay = :dueDay, "+
	                " assignedDay = :assignedDay, createdBy=:createdBy, created = :created, dueTime = :dueTime, startTime = :startTime, assignedTime = :assignedTime, endTime = :endTime, locked = :locked, "+
	                 " UniqueID = :UniqueID, onTheDay = :onTheDay, assignedtoMe = :assignedToMe, allStudents = :All_Students, allTeachers = :All_Teachers WHERE diaryItem.id  = :id and lower(type) = :typeCheck",

	            // params
	            {
	                	'type' : record.type,
	                    'title' : record.title,
	                    'description' : record.description,
	                    'completed' : completed,
	                    'progress' : record.progress,
	                    'EstimatedHours' : record.estimatedHours,
	                    'EstimatedMins' : record.estimatedMins,
	                    'priority' : record.priority,
	                    'subjectId' : subjectid,
	                    'ClassId' : record.classId,
	                    'SchoolId' : record.schoolId,
	                    'UserID' : record.userid,
	                    'isSync' : 1,
	                    'startDay' : startDay,
	                    'dueDay' : dueDate,
	                    'assignedDay' : assignedDate,
	                    'created' : record.created != ""?record.created:null,
	                    'dueTime' : dueTime!=0? dueTime:null,
		 				//'startTime':startTime!=0?startTime:null,
	                    //change for EZTEST-214
	                    'startTime':startTime,
		 				'assignedTime' :assignedTime!=0?assignedTime:assignedTime,
		 				'endTime':endTime,
		 				'id' : id,
						'createdBy':record.createdBy,
						'locked':locked,
						'UniqueID' : record.uniqueid,
						'onTheDay' : record.onTheDay,
						'assignedToMe' : assignedToMe,
						'typeCheck' : record.type.toLowerCase(),
						'All_Students' : record.All_Students == null ? 1 : record.All_Students,
						'All_Teachers' : record.All_Teachers == null ? 1 : record.All_Teachers,
						'type_id' : record.type_id,
						'type_name' : record.type_name

	            },

				// success
				function success(statement)
				{
	            	record = null;
					console.log("update event success");
				},

				function error(error)
				{
					record = null;
					//console.log("update error");
					//console.log(error);
				}
			);
}

function insertEventRecord(record)
{
	var completed=record.completed;
		var assignedDate=record.assignedDate;
		if(assignedDate==null || assignedDate==0)
		{
		
		}
		else
		{
			assignedDate=new Rocketboots.date.Day(record.assignedDate).getKey();
			assignedDate=parseInt(assignedDate);
		}
	
	
		var startDay=record.startDate;
		if(startDay==null || startDay==0)
		{
		
		}
		else
		{
			startDay=new Rocketboots.date.Day(record.startDate).getKey();
			startDay=parseInt(startDay);
		}
	
	
		var dueDate=record.dueDate;
		if(dueDate==null || dueDate==0)
		{
		
		}
		else
		{
			dueDate=new Rocketboots.date.Day(record.dueDate).getKey();
			dueDate=parseInt(dueDate);
		}
	
	
		var assignedTime=record.assignedTime;
		//console.log("record.assignedTime="+record.assignedTime);
		assignedTime=formateTimes(record.assignedTime);
		//console.log("assignedTime="+assignedTime);
		
		var startTime=record.startTime;
			startTime=formateTimes(record.startTime);
			//change for 214
			if(startTime == 0){
				if(record.onTheDay == '1'){
					startTime = null;
				}
			}
	
		var endTime=record.endTime;
			endTime=formateTimes(record.endTime);
			if(endTime == 0){
				
				if(record.onTheDay == '1'){
					endTime = null;
				}
			}
		
	
		var dueTime=record.dueTime;
			dueTime=formateTimes(record.dueTime);
		var subjectid=record.subjectId;
		if(subjectid==0)
		{
			subjectid=null;
		}
		var itemcompleted=record.completed;
		if(itemcompleted==0)
		{
			itemcompleted=null;
		}
	if(completed.toString() == "0")
	{
		completed=null;
	}
	var locked=null;
	var assignedToMe = 1;
	if(record.publishToClass == '1'){
		assignedToMe = 0;
	}
	//Events created by Admin were getting deleted because of this.
	/*if(record.type=="EVENT")
	{
	if(record.createdBy!=UserId)
	{
		locked=1;
	}
	} */
	/*****/
	console.log("insertEventRecord called"+record.uniqueid);
	console.log("locked 3"+locked);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "INSERT INTO diaryItem (type, title, description, UniqueID, completed, progress, EstimatedHours, allStudents, allTeachers, "+
	            "EstimatedMins, priority, subjectId, ClassId, SchoolId, UserID, isSync, startDay, startTime, endTime, dueDay, "+
	            "assignedDay, created, assignedTime, dueTime, lastupdated, isDelete, createdBy, locked, appType, onTheDay, assignedtoMe, type_id, type_name)" +
	            " VALUES (:type, :title, :description, :UniqueID, :completed, :progress, :EstimatedHours, :All_Students, :All_Teachers, :EstimatedMins, "+
	            ":priority, :subjectId, :ClassId, :SchoolId, :UserID, :isSync, :startDay, :startTime, :endTime, :dueDay, "+
	            ":assignedDay, :created, :assignedTime, :dueTime, :lastupdated, :isDelete, :createdBy, :locked, :appType, :onTheDay, :assignedToMe, :type_id, :type_name)",
		
	            // params
	            {
	                'type' : record.type,
	                'title' : record.title,
	                'description' : record.description,
	                'UniqueID' : record.uniqueid,
	                'completed' : itemcompleted,
	                'progress' : record.progress,
	                'EstimatedHours' : record.estimatedHours,
	                'EstimatedMins' : record.estimatedMins,
	                'priority' : record.priority,
	                'subjectId' : subjectid,
	                'ClassId' : record.classId,
	                'SchoolId' : record.schoolId,
	                'UserID' : record.userid,
	                'isSync' : 1,
	                'startDay' :startDay,
	                //'startTime':startTime!=0 ? startTime:0,
	                //change to solve EZTEST-214
	                'startTime':startTime,
	                'endTime':endTime,
	                'dueDay' :dueDate,
	                'assignedDay':assignedDate,
	                'created' : record.created != '' ? record.created:null,
	                'assignedTime' :assignedTime!=0?assignedTime:assignedTime,
	                'dueTime' : dueTime!=0? dueTime:null,
					'lastupdated':record.lastupdate != '' ? record.lastupdate:null,
					'isDelete':0,
					'createdBy':record.createdBy,
					'locked':locked,
                	'appType' : record.appType,
                	'onTheDay' : record.onTheDay,
                	'assignedToMe' : assignedToMe,
					'All_Students' : record.All_Students == null ? 1 : record.All_Students,
					'All_Teachers' : record.All_Teachers == null ? 1 : record.All_Teachers,
					'type_id' : record.type_id,
					'type_name' : record.type_name
	            },

				// success
				function(statement) {
								record = null;
				                //console.log("statement.getResult().lastInsertRowID="+statement.getResult().lastInsertRowID);
			                
				},

				// error
				function error()
				{
					record = null;
					//console.log("Error while trying to create diary item");
				}
		);
}

function insertNewsRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO News (UniqueID, SchoolId, Title, Body, Tags, CampusIds, Created, Updated, Status, All_Students, All_Teachers)"+
            " VALUES (:UniqueID, :SchoolId, :Title, :Body, :Tags, :CampusIDs, :Created, :Updated, :Status, :All_Students, :All_Teachers)",
		
            // params
            {
                'UniqueID' : record.Id,
                'SchoolId' : record.SchoolId,
                'Title' : record.Title,
                'Body' : record.Body,
                'Tags' : record.Tags,
                'CampusIDs' : record.CampusIDs,
                'Created' : record.Created,
                'Updated' : record.Updated,
                'Status' : 0,
                'All_Students' : record.All_Students == null ? 0 : record.All_Students,
                'All_Teachers' : record.All_Teachers == null ? 0 : record.All_Teachers
            },

			// success
			function(statement) {
				var serverAttachments = record.News_Attachments;
				var id = statement.getResult().lastInsertRowID;
			    $.each(serverAttachments,function(index,serverrecord){
					insertAttachmentRecord(serverrecord, id);            
	            });    
			    
			    if(record.imagePath){
			    	downloadNewsImage(record.imagePath);
			    }
			},

			// error
			function error()
			{
				//alert("News insert Error");
			}
	);
}

function updateNewsRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE News set SchoolId = :SchoolId, Title = :Title, Body = :Body, Tags = :Tags, CampusIds = :CampusIDs, Updated = :Updated, "+
            " Created = :Created, Status = :Status, All_Students = :All_Students, All_Teachers = :All_Teachers WHERE UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.Id,
                'SchoolId' : record.SchoolId,
                'Title' : record.Title,
                'Body' : record.Body,
                'Tags' : record.Tags,
                'CampusIDs' : record.CampusIDs,
                'Updated' : record.Updated,
                'Created' : record.Created,
                'Status' : 0,
                'All_Students' : record.All_Students == null ? 0 : record.All_Students,
                'All_Teachers' : record.All_Teachers == null ? 0 : record.All_Teachers
            },

			// success
			function(statement) {
				deleteNewsAttachmentRecord(record);
				var serverAttachments = record.News_Attachments;
			    $.each(serverAttachments,function(index,serverrecord){
					insertAttachmentRecord(serverrecord);            
	            });       
			    
			    if(record.imagePath){
			    	downloadNewsImage(record.imagePath);
			    }
			},

			// error
			function error()
			{
				//alert("News insert Error");
			}
	);
}

function updateAllClasses(classUniqueId,Code)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE class set Code = :Code WHERE UniqueID = :UniqueID",
		
            // params
            {
            	'Code' : Code,
            	'UniqueID' : classUniqueId
            },

			// success
			function(statement) {
            	//alert("Update class success"); 
			},

			// error
			function error()
			{
				//alert("Update class Error");
			}
	);
}

function insertAttachmentRecord(record, id){
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO News_Attachments (NewsId, Attachments, Created, Updated)"+
            " VALUES (:NewsId, :Attachments, :Created, :Updated)",
		
            // params
            {
                'NewsId' : record.NewsId,
                'Attachments' : record.Attachments,
                'Created' : record.Created,
                'Updated' : record.Updated,
            },

			// success
			function(statement) {
				//alert("News inserted successfully");
			},

			// error
			function error()
			{
				//alert("News insert Error");
			}
	);
}

function insertAnnouncementRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO Announcements (UniqueID, SchoolId, Details, CampusIds, Created, Updated, CreatedBy, All_Students, All_Teachers)"+
            " VALUES (:UniqueID, :SchoolId, :Title, :CampusIDs, :Created, :Updated, :CreatedBy, :All_Students, :All_Teachers)",
		
            // params
            {
                'UniqueID' : record.Id,
                'SchoolId' : record.SchoolId,
                'Title' : record.Details,
                'CampusIDs' : record.campusName,
                'Created' : record.Created,
                'Updated' : record.Updated,
                'CreatedBy' : record.Name,
                'All_Students' : record.All_Students == null ? 0 : record.All_Students,
                'All_Teachers' : record.All_Teachers == null ? 0 : record.All_Teachers
                
            },

			// success
			function(statement) {
				//alert("Announcement inserted successfully");
			              
			},

			// error
			function error()
			{
				//alert("Announcement insert Error");
			}
	);
	
	
}

function updateAnnouncementRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE Announcements SET SchoolId = :SchoolId, Details = :Title, CampusIds = :CampusIDs, Updated = :Updated, " + 
            " Created = :Created, CreatedBy = :CreatedBy, All_Students = :All_Students, All_Teachers = :All_Teachers WHERE UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.Id,
                'SchoolId' : record.SchoolId,
                'Title' : record.Details,
                'CampusIDs' : record.campusName,
                'Updated' : record.Updated,
                'Created' : record.Created,
                'CreatedBy' : record.Name,
                'All_Students' : record.All_Students == null ? 0 : record.All_Students,
                'All_Teachers' : record.All_Teachers == null ? 0 : record.All_Teachers
            },

			// success
			function(statement) {
				//alert("Announcement updated successfully");
			              
			},

			// error
			function error()
			{
				//alert("Announcement update Error");
			}
	);
	
}

function processAnnouncementRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id From Announcements WHERE UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.Id,
            },

			// success
			function(statement) {
				//alert("Announcement inserted successfully");
			    var announcments = _.map(statement.getResult().data, function(item) {
                    return item.id;
               	});  
               	if(announcments.length == 0){
               		insertAnnouncementRecord(record);
               	} else {
               		updateAnnouncementRecord(record);
               	}
			},

			// error
			function error()
			{
				//alert("Announcement insert Error");
			}
	);
	record = null;
	
}

function processNewsRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id From News WHERE UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.Id,
            },

			// success
			function(statement) {
			    var news = _.map(statement.getResult().data, function(item) {
                    return item.id;
               	});  
               	if(news.length == 0){
               		insertNewsRecord(record);
               	} else {
               		updateNewsRecord(record);
               	}    
               	news = null;
			},

			// error
			function error()
			{
				//alert("News insert Error");
			}
	);
	
	
}

function deleteNewsRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "DELETE FROM News where UniqueID = :msgId",
		
            // params
            {
                'msgId' : record.Id
            },

			// success
			function(statement) {
				deleteNewsAttachmentRecord(record);
				//alert("News deleted successfully");
			              
			},

			// error
			function error()
			{
				//alert("News delete Error");
			}
	);
	record = null;
	
}

function deleteNewsAttachmentRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "DELETE FROM News_Attachments where NewsId = :msgId",
		
            // params
            {
                'msgId' : record.Id
            },

			// success
			function(statement) {
				//alert("News_Attachments deleted successfully");
			              
			},

			// error
			function error()
			{
				//alert("News_Attachments delete Error");
			}
	);
	record = null;
}

function deleteAnnouncementRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "DELETE FROM Announcements where UniqueID = :msgId",
		
            // params
            {
                'msgId' : record.Id
            },

			// success
			function(statement) {
				//alert("Announcements deleted successfully");
			              
			},

			// error
			function error()
			{
				//alert("Announcements delete Error");
			}
	);
	record = null;
}

function deleteEventRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "DELETE FROM diaryItem where UniqueID = :eventId",
		
            // params
            {
                'eventId' : record.Id
            },

			// success
			function(statement) {
				//alert("Event deleted successfully");
			              
			},

			// error
			function error()
			{
				//alert("Event delete Error");
			}
	);
	
}

function processDiaryItemTypes(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id From DiaryItemTypes WHERE UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.Id,
            },

			// success
			function(statement) {
				//alert("DiaryItemTypes inserted successfully");
			    var diaryItemTypes = _.map(statement.getResult().data, function(item) {
                    return item.id;
               	});  
               	if(diaryItemTypes.length == 0){
               		insertDiaryItemType(record);
               	} else {
               		diaryItemTypesUpdated = true;
               		updateDiaryItemType(record);
               	}
               	
               	updateDiaryItemTypes();
               	diaryItemTypes = null;
			},

			// error
			function error()
			{
				//alert("DiaryItemTypes insert Error");
			}
	);
	record = null;
}

function updateDiaryItemTypes(){
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
        // query
        "SELECT * FROM DiaryItemTypes",
		// params
		{
			
		},
 	 	// success
  		function(statement) {
		   var itemTypes = _.map(statement.getResult().data, function(item) {
                return item;
           });
			
		   if(itemTypes.length)
           {
             var diaryItemTypesCollection = new diary.collection.DiaryItemTypesCollection();
             for(var index = 0; index < itemTypes.length; index++)
             {
            	var diaryItemType = new diary.model.DiaryItemType();
    			diaryItemType.set("name", itemTypes[index].Name);
    			diaryItemType.set("formTemplate", itemTypes[index].FormTemplate ? itemTypes[index].FormTemplate.toLowerCase() : null);
    			diaryItemType.set("id", itemTypes[index].id);
    			diaryItemType.set("UniqueID", itemTypes[index].UniqueID);
    			diaryItemType.set("isActive", itemTypes[index].Active);
    			diaryItemType.set("iconName", itemTypes[index].Icon);
				
				diaryItemTypesCollection.add(diaryItemType);
				diaryItemType = null;
              }
            
             app.model.get(ModelName.DIARYITEMTYPES).reset(diaryItemTypesCollection.toArray());
             diaryItemTypesCollection = null;
             //console.log("diaryItemTypes collection: "+JSON.stringify(app.model.get(ModelName.DIARYITEMTYPES)));
           }
           
           itemTypes = null;
			          				 
  		},
  		function error()
  		{
  			console.log("Error in mapping DiaryItemTypes");
  		}
 	);
}

function insertDiaryItemType(record)
{
	var isActive = 0;
	if("\1" === record.Active || record.Active == '1'){
		isActive = 1;
	}
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO DiaryItemTypes (UniqueID, SchoolId, Name, Active, Icon, FormTemplate)"+
            " VALUES (:UniqueID, :SchoolId, :Name, :Active, :Icon, :FormTemplate)",
		
            // params
            {
                'UniqueID' : record.Id,
                'SchoolId' : record.SchoolId,
                'Name' : record.Name,
                'Active' : isActive,
                'Icon' : record.Icon,
                'FormTemplate' : record.FormType,
            },

			// success
			function(statement) {
				//alert("DiaryItemType inserted successfully");
			              
			},

			// error
			function error()
			{
				//alert("DiaryItemType insert Error");
			}
	);
	
	
}

function updateDiaryItemType(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE DiaryItemTypes SET SchoolId = :SchoolId, Name = :Name, Active = :Active, " + 
            " Icon = :Icon, FormTemplate = :FormTemplate WHERE UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.Id,
                'SchoolId' : record.SchoolId,
                'Name' : record.Name,
                'Active' : record.Active,
                'Icon' : record.Icon,
                'FormTemplate' : record.FormType,
            },

			// success
			function(statement) {
				//alert("DiaryItemType updated successfully");
			              
			},

			// error
			function error()
			{
				//alert("DiaryItemType update Error");
			}
	);
	
}

function isDiaryItemTypeActive(diaryItemType)
{
	var isActive = false;
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT Active From DiaryItemTypes WHERE Name = :Name",
		
            // params
            {
                'Name' : diaryItemType,
            },

			// success
			function(statement) {
			    var status = _.map(statement.getResult().data, function(item) {
                    return item;
               	});  
               	
               	if(status[0] && status[0].Active == "1"){
               		isActive = true;
               	}
			},

			// error
			function error()
			{
				isActive = false;
				//alert("DiaryItemTypes insert Error");
			}
	);
	
	return isActive;
}

function parseInsertMessageRecord(record)
{
	console.log("record to insert into message : "+JSON.stringify(record));
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "select Id from Messages where UniqueID = :msgId OR (UserId = :userId AND ReceiverId = :receiverId AND Subject = :subject AND Description = :description AND Created = :Created)",
		
            // params
            {
                'msgId' : record.Id,
                //change for new request
                //'userId' : record.NormalUserId,
               // 'receiverId' : record.NormalReceiverId,
                'userId' : record.UserId,
                'receiverId' : record.receiverId,
                'subject' : record.Subject,
                'description' : record.Description,
                'Created' : record.Created
            },

			// success
			function(statement) {
			    var messages = _.map(statement.getResult().data, function(item) {
                    return item;
               });
			   console.log("check duplicate msg found :messages.length "+messages.length);
			   if(messages.length > 0)
               {
				   // update if message exist
               		updateMessageRecord(record);
               } else {
            	   //else insert into messages
               		insertMessageRecord(record);
               }      
			},

			// error
			function error()
			{
				//alert("Message insert Error");
			}
	);
}
function updateMessageRecord(record)
{
	console.log("update message : ");
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE Messages SET isSync = 1, UniqueID = :UniqueID, ParentId = :ParentId "+
            " where UniqueID = :msgId OR (UserId = :userId AND ReceiverId = :receiverId AND Subject = :subject AND Description = :description )",
		
            // params
            {
                'UniqueID' 	: record.Id,
                'ParentId' : record.ParentId,
                //change: for new request
                //'UserId' : record.NormalUserId,
                //'ReceiverId' : record.NormalReceiverId,

               // 'userId' : record.UserId,
              //  'receiverId' : record.receiverId,
              //  'UserName' : record.UserName,
              //  'ReceiverName' : record.receiverName,
                'msgId' 	: record.Id,
                'userId' : record.UserId,
                'receiverId' : record.receiverId,
                'subject' : record.Subject,
                'description' : record.Description
            },

			// success
			function(statement) {
				//alert("Message inserted successfully");
			              
			},

			// error
			function error()
			{
				//alert("Message insert Error");
			}
	);
	
}
function deleteMessageRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "DELETE FROM Messages where UniqueID = :msgId",
		
            // params
            {
                'msgId' : record.Id
            },

			// success
			function(statement) {
				//alert("Message deleted successfully");
			              
			},

			// error
			function error()
			{
				//alert("Message delete Error");
			}
	);
	
}
function insertMessageRecord(record)
{
	console.log("record to insert : "+JSON.stringify(record));
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO Messages (UniqueID, ParentId, UserId, ReceiverId, Subject, Description, Created, IpAddress, AppId, UserName, ReceiverName)"+
            " VALUES (:UniqueID, :ParentId, :UserId, :ReceiverId, :Subject, :Description, :Created, :IpAddress, :AppId, :UserName, :ReceiverName)",
		
            // params
            {
                'UniqueID' : record.Id,
                'ParentId' : record.ParentId,
                // change by amruta : for new respoce
                'UserId' : record.UserId,
                //'ReceiverId' : record.NormalReceiverId,
                'ReceiverId' : record.receiverId,
                'Subject' : record.Subject,
                'Description' : record.Description,
                'Created' : record.Created,
                'IpAddress' : record.IpAddress,
                'AppId' : record.AppId,
                'UserName' : record.SenderName,
                'ReceiverName' : record.receiverName,
                
            },

			// success
			function(statement) {
				//alert("Message inserted successfully");
			              
			},

			// error
			function error()
			{
				//alert("Message insert Error");
			}
	);
}

function insertMessageUser(record)
{
	console.log("record to insert into messageUser : "+JSON.stringify(record));

	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT OR REPLACE INTO messageUser (UniqueID, messageId, receiverId)"+
            " VALUES (:uniqueId, :messageId, :receiverId)",
		
            // params
            {
                'uniqueId' : record.id,
                'messageId' : record.message_id,
                'receiverId' : record.ReceiverId
            },

			// success
			function(statement) {
				//alert("Message inserted successfully");
			              
			},

			// error
			function error()
			{
				//alert("Message insert Error");
			}
	);
}

function insertMyTeachers(){
	var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	var data = data || '{"user_id":'+currentUserId+'}';
 	var url = SystemSettings.SERVERURL + "/teachers?"; 
 	
	$.ajax({
            'url' : url,
            'type': 'POST',
            'data': JSON.stringify(data),
            'dataType' : "json",
            'success': function(result) {
				setTimeout(function(){
					myTeachers = result.teacher_details;
					$.each(myTeachers,function(index,serverrecord){
						insertTeachersRecord(serverrecord);            
		            });
				},1000);
            	
            }, 
            'error': function(response) {
            	console.log("result insertMyTeachers : "+JSON.stringify(response));
            	FlurryAgent.logError("MyTeachers Sync Error", "Error in fetching data from teachers web service",1);
            	dataDownloadError();
            }
        });
}

function insertTeachersRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT OR REPLACE INTO myTeacher (teacher_id, subjectName, phone, status, email, name, image_url, isDeleted)"+
            " VALUES (:teacherId, :subjects, :phone, :status, :email, :name, :imageURL, :isDeleted)",
		
            // params
            {
                'teacherId' : record.NormalUserId,
                'email' 	: record.Email,
                'subjects'	: record.subjects,
                'phone'		: record.Telephone,
                'status'	: record.Status,
                'name'		: record.FirstName + ' '+record.LastName,
                'imageURL'	: record.ProfileImage,	
                'isDeleted' : 0
            },

			// success
			function(statement) {
				//alert("Teacher inserted successfully");
            	if(record.ProfileImage != null){
            		downloadUserProfileImage(record.ProfileImage);
            	}
			},

			// error
			function error()
			{
				openMessageBox("Teacher insert Error");
			}
	);
}

function insertMyParents(){
	var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	var data = data || '{"user_id":'+currentUserId+'}';
 	var url = SystemSettings.SERVERURL + "/getParent?"; 
 	
	$.ajax({
            'url' : url,
            'type': 'POST',
            'data': JSON.stringify(data),
            'dataType' : "json",
            'success': function(result) {
				myParents = result.parent_details;
        		$.each(myParents,function(index,serverrecord){
					
					insertParentsRecord(serverrecord, currentUserId);            
	            });
            	
            }, 
            'error': function(response) {
            	FlurryAgent.logError("Parents Sync Error", "Error in fetching data from getParent web service",1);
            	console.log("result insertMyParents : "+JSON.stringify(response))
            }
        });
}

function insertParentsRecord(record, studentId)
{
	console.log("insertParentsRecord"+JSON.stringify(record));
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT OR REPLACE INTO myParent (parent_id, email, name, studentId, status)"+
            " VALUES (:parentId, :email, :name, :studentId, :status)",
		
            // params
            {
                'parentId'  : record.NormalUserId,
                'email' 	: record.Email,
                'name'		: record.FirstName + ' '+record.LastName,
                'studentId' : studentId,
                'status'	: record.Status
            },

			// success
			function(statement) {
				//alert("Parent inserted successfully");
			              
			},

			// error
			function error()
			{
				openMessageBox("Parent insert Error");
			}
	);
}


//cloud service call to get classes for perticular user
function getClassUsers(data){
  
	var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	
	GetLastSync("getClassUserSyncing"); 
	var data = '{"user_id":"'+currentUserId+'","LastSync":'+usersUpdate+',"user_type":"Teacher"}';
 	console.log("data getclassuser : "+data);
 	url = SystemSettings.SERVERURL + "/getclassuser"; 
 	
	$.ajax({
            'url' : url,
            'type': 'POST',
            'data': JSON.stringify(data),
            'dataType' : "json",
            'success': function(result) {
            	data = null;
            	var lastSyncTime = getTimestampmilliseconds();
				updateLastUpdate("getClassUserSyncing", lastSyncTime);
				//console.log("LastSynchTime : "+result.LastSynchTime);
            	//console.log("result getclassusers : "+JSON.stringify(result));
            	//deleteParent();
        		for(var len = 0 ; len < result.Class_user.length; len++){
        			insertStudent(result.Class_user[len], currentUserId);
        			// to delete students which are deleted from class enrolment 
        			
        			//updateDeletedStudent(result.deleted);
        			var myParents = result.Class_user[len].parent_details;
            		//console.log("myParents : in getclassUsers"+JSON.stringify(myParents));
            		//console.log("myParents : studentId :"+result.Class_user[len].NormalUserId);
        			//delete all parents and insert new updated
        			
        			
            		$.each(myParents,function(index,serverrecord){
    					insertParentsRecord(serverrecord,  result.Class_user[len].NormalUserId);            
    	            });
        		}
        		//console.log("result.deleted.length : "+result.deleted.length);
        		for(var len = 0 ; len < result.deleted.length; len++){
        			//console.log("result.deleted[0]: "+result.deleted[len]);
        			updateDeletedStudent(result.deleted[len]);
        		}
        		
        		for(var len = 0 ; len < result.DeletedParents.length; len++){
        			deleteParentRecord(result.DeletedParents[len]);
        		}
        		
        		$.each(result.UpdatedParents,function(index,serverrecord){
					insertParentsRecord(serverrecord,  serverrecord.StudentId);            
	            });
        		
        		setTimeout(function(){getClassUsersLocal();},1500);
        			
				//var classUserList = mapClassUser(result.Class_user);
				//app.model.set(ModelName.CLASSUSERS, classUserList)
				result = null;
				myParents = null;
	        }, 
            'error': function(response) {
            	console.log("result getclassusers : "+JSON.stringify(response));
            	FlurryAgent.logError("Classusers Sync Error", "Error in fetching data from getclassuser web service",1);
            	dataDownloadError();
            	//alert('response getclassuser: ' + response.readyState);
            	//if(response.readyState == 0){
            		//getClassUsers(data);
            	//}
            }
        });
}
function deleteParent(){
	
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
				
	            // query
	            "DELETE FROM myParent",
	
	            // params
	            {
	            	
	            },
				// success
				function(statement) {
	            	console.log("Parent deleted successfully");
				},
				
				// error
				function error()
				{
					console.log("error in deleting parent");
				}
			);
	
}

function deleteParentRecord(record){
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
			
            // query
            "DELETE FROM myParent where parent_id = :parent_id",

            // params
            {
            	'parent_id'  : record.NormalUserId,
            },
			// success
			function(statement) {
            	console.log("Parent deleted successfully");
			},
			
			// error
			function error()
			{
				console.log("error in deleting parent");
			}
		);

}

function updateDeletedStudent(user){
	//if student then update student table
	//console.log("user : "+JSON.stringify(user));
	if(user.IsStudent){
		app.locator.getService(ServiceName.APPDATABASE).runQuery(
				
	            // query
	            "UPDATE student SET DeletedStatus = 1 WHERE student_id = :userId and class_id = :classId",
	
	            // params
	            {
	            	'userId' :user.NormalUserId,
	            	'classId' :user.ClassId
	            },
				// success
				function(statement) {
	            	console.log("student updated successfully");
				},
				
				// error
				function error()
				{
					console.log("error in updating student");
				}
			);
			
	}
	
}


function getClassUsersLocal(){
	
	//console.log("getClassUsersLocal : ");
	var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT s.*, c.Name as className, c.id FROM student s, class c WHERE s.DeletedStatus = 0 and s.class_id = c.UniqueId and s.teacher_id = :teacherId group by class_id, student_id order by firstName asc ",
			// params
			{
				'teacherId' : currentUserId
			},
     	 // success
      		function(statement) {
				
			   var classUsers = _.map(statement.getResult().data, function(item) {
				     return item;
               });
					
			   if(classUsers.length)
               {
                 var classUsersArray=[];
                 for(var index = 0; index < classUsers.length; index++)
                 {
                	var classUser = new Object();
                	classUser.ClassId =  classUsers[index].class_id;
        			var student = new Object();
					student.FirstName = classUsers[index].firstName;
					student.LastName = classUsers[index].lastName;
					student.class_id = classUsers[index].class_id;
					student.school_id = classUsers[index].school_id;
					student.my_class = classUsers[index].my_class;
					student.updateTime = classUsers[index].updateTime;
					student.email_id = classUsers[index].email_id;
					student.image_url = classUsers[index].image_url;
					student.phone_no = classUsers[index].phone_no;
					student.student_id = classUsers[index].student_id;
					student.teacher_id = classUsers[index].teacher_id;
					student.image = classUsers[index].image;
					student.NormalUserId = classUsers[index].student_id;
					//console.log("classUsers[index].status : "+classUsers[index].status);
					student.Status = classUsers[index].status;
					
					classUser.users = student;
	                classUsersArray.push(classUser);
                  }
                
                 //var classUserList  = mapClassUser(classUsersArray);
                 app.model.set(ModelName.CLASSUSERS, mapClassUser(classUsersArray))
                 //console.log("classUserList getClassUsersLocal : "+JSON.stringify(classUserList));
                 classUsersArray = null;
               }
				          				 
      		},
      		function error()
      		{
      			console.log("Room ID update Error");
      		}
     	);
}


function mapClassUser(classUserList){
	//console.log("classUsers[index].status : "+JSON.stringify(classUserList));
	
      
	var classUsers = new diary.collection.ClassUserCollection();
	
	//var subjectArray = app.model.get(ModelName.SUBJECTS);
	var tempClassUserList = classUserList;
	
	for(var len = 0 ; len < classUserList.length; len++){
		//TODO : get class name from local db for classId
			
			if(classUserList[len] != null){
				var classUser = new diary.model.ClassUser();
				var classId = classUserList[len].ClassId;
				classUser.set("class", classUserList[len].ClassId);
				var studentList = new diary.collection.StudentCollection();
				for(var len1 = 0  ; len1 < tempClassUserList.length; len1++){
					//console.log("tempClassUserList[len1] :  "+JSON.stringify(tempClassUserList[len1]));
					
					if(tempClassUserList[len1] != null && classId == tempClassUserList[len1].ClassId){
						//alert('inside tempClassUserList');
						//console.log("classUserList[len].ClassId : "+tempClassUserList[len1].users.Status);
						
						if(tempClassUserList[len1].users.Status == 1){
							//console.log("if 1: "+tempClassUserList[len1].users.FirstName);
							
							var student = new diary.model.StudentProfile();
							student.set("name",tempClassUserList[len1].users.FirstName);
							student.set("lastName",tempClassUserList[len1].users.LastName);
							student.set("classId",tempClassUserList[len1].users.class_id);
							student.set("schoolId",tempClassUserList[len1].users.school_id);
							student.set("myClass",tempClassUserList[len1].users.my_class);
							student.set("updateTime",tempClassUserList[len1].users.updateTime);
							student.set("email",tempClassUserList[len1].users.email_id);
							student.set("imageUrl",tempClassUserList[len1].users.image_url);
							student.set("telephone",tempClassUserList[len1].users.phone_no);
							student.set("studentId",tempClassUserList[len1].users.student_id);
							student.set("teacherId",tempClassUserList[len1].users.teacher_id);
							student.set("image",tempClassUserList[len1].users.image);
							student.set("UniqueId",tempClassUserList[len1].users.NormalUserId);
							studentList.push(student);
							tempClassUserList[len1] = null;  
						} else {
							tempClassUserList[len1] = null;  
						}
					}
					
				}
				classUser.set("users",studentList)
				classUsers.push(classUser);
				classUser = null;
				studentList = null;
			}
	}
	//console.log("classUsers : "+JSON.stringify(classUsers));
	tempClassUserList = null;
	classUserList = null;
	return classUsers;
}


// function to insert records into DiaryItem_users

function insertDiaryItemUsers(assignedDiaryItems, currentUserId){
	//console.log("insertDiaryItemUsers : "+JSON.stringify(assignedDiaryItems));
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query

            " INSERT OR REPLACE INTO diaryItemUser (item_id, unique_id, updateDate, progress, student_id, user_id, reminder, isDeleted) "+
            " VALUES (:itemId, :uniqueId, :updateDate, :progress, :studentId, :userId, :reminder, :isDeleted)",
           // " ON DUPLICATE KEY UPDATE item_id = :itemId , reminder = :reminder, updateDate =:updateDate, progress =:progress, student_id =:studentId, user_id =:userId ",
           

            // params
            {
                'itemId' : assignedDiaryItems.itemId,
                'uniqueId' : assignedDiaryItems.uniqueid,
                'reminder' : assignedDiaryItems.Reminder,
                'updateDate' : getTimestampmilliseconds(),
                'progress' : assignedDiaryItems.Progress,
                'studentId' : assignedDiaryItems.UserId,
                'userId' : currentUserId,				// created by current user
                'isDeleted' : assignedDiaryItems.IsDeleted
                //'isDeleted' : assignedDiaryItems.isDeleted				
            },

			// success
			function(statement) {
			},

			// error
			function error()
			{
				console.log("Error while inserting diaryItemUser");
			}
	);
	assignedDiaryItems = null;
}


//function to insert records into table student

function insertStudent(classUsers, cuerrentUserId){
	
	//console.log("insert student : "+JSON.stringify(classUsers))
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
         // query
			
         " insert or replace into student (class_id, school_id, my_class, updateTime, email_id, " +
         "						firstName, image_url, lastName, phone_no, student_id, student_name, teacher_id, status, DeletedStatus) "+
         " 				VALUES (:classId, :schoolId, :myClass, :updateTime, :emailId, :firstName, :imageUrl, " +
         "						:lastName, :phoneNo, :studentId, :studentName, :teacherId, :status, :deletedStatus)",

         // params
         {
             'classId' : classUsers.ClassId,
             'schoolId' : classUsers.SchoolId,
             'myClass' : classUsers.ClassId,
             'updateTime' : getTimestampmilliseconds(),
             'emailId' : classUsers.Email,
             'firstName' : classUsers.FirstName,
             'imageUrl' : classUsers.ProfileImage, 	
             'lastName' : classUsers.LastName,
             'phoneNo' : classUsers.Telephone,
             'studentId' : classUsers.NormalUserId,
             'studentName' :classUsers.FirstName,
             'teacherId' : cuerrentUserId,
             'status' : classUsers.Status,
             'deletedStatus' : 0			// default value when adding or replacing will be zero
         },

			// success
			function(statement) {
        	 		console.log("downloading students image"+classUsers.ProfileImage);
        	 		if(classUsers.ProfileImage != null){
        	 			downloadUserProfileImage(classUsers.ProfileImage);
        	 		}
					record = null;
			},

			// error
			function error()
			{
				record = null;
				//console.log("Error while trying to create diary item");
			}
	);
}




function getClassForTeacher(teacherId){
	
	//TODO : change hard coded teacher id
	//TODO : change c.uniqueId by class name
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "select c.UniqueId, c.Name as className, c.id as classId, p.id as periodId, c.subjectId, c.Code, c.timetable from class c,  ClassUser cu "+
			" left outer join period p on p.id = c.periodId where c.UniqueId  = cu.classUniqueId and  cu.userId = :teacherId and cu.isDelete is null group by cu.classUniqueId",
			// params
			{
				'teacherId' : teacherId
			},
     	 // success
      		function(statement) {
				//alert("statement.getResult().data : "+JSON.stringify(statement.getResult().data));
				
			   getClassesForMappingSubjects();
			   var classCollection = _.map(statement.getResult().data, function(item) {
                    return item;
               });
			   if(classCollection.length)
               {
                 var classes = new diary.collection.ClassCollection();
                 for(var index = 0; index < classCollection.length; index++)
                 {
                	 //alert("classCollection[index].className : "+classCollection[index].className);
                	 var modelClass =  new diary.model.Class();
                	 modelClass.set("uniqueId", classCollection[index].UniqueID);
                	 modelClass.set("name", classCollection[index].className);
                	 modelClass.set("classId", classCollection[index].classId);
                	 modelClass.set("subjectId", classCollection[index].subjectId);
                	 modelClass.set("periodId", classCollection[index].periodId);
                	 modelClass.set("Code", classCollection[index].Code);
                	 modelClass.set("timeTableID", classCollection[index].timetable);
                	 classes.push(modelClass);
                  }
                 // console.log("set classes 1 : "+JSON.stringify(classes));
                 app.model.set(ModelName.CLASSES, classes);
               }
				          				 
      		},
      		function error()
      		{
    			FlurryAgent.logError("Class Error","Error in fetching Classes", 1);
      			console.log("Room ID update Error");
      		}
     	);
}
function getAllClasses(){
	var classes = new diary.collection.ClassCollection();
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query 
			// 
            "select c.*, c.id as classId from class c left outer join ClassUser cu on c.UniqueId  = cu.classUniqueId "+
            " where cu.isDelete is null and c.UniqueId != 0 group by c.UniqueId",
            {
				
			},
     	 // success
      		function(statement) {
				getClassesForMappingSubjects();
				
			   _.forEach(statement.getResult().data, function(classCollection) {
			   		 //console.log("classCollection.classId : "+classCollection.classId);
			   		//console.log("classCollection.classId : "+classCollection.id);
                	 var modelClass =  new diary.model.Class();
                	 modelClass.set("uniqueId", classCollection.UniqueID);
                	 modelClass.set("name", classCollection.Name);
                	 modelClass.set("classId", classCollection.classId);
                	 modelClass.set("subjectId", classCollection.subjectId);
                	 modelClass.set("periodId", classCollection.periodId);
                	 modelClass.set("Code", classCollection.Code);
                	 modelClass.set("timeTableID", classCollection.timetable);
                	 classes.push(modelClass);
			   });
			   // console.log("set classes 2 : "+JSON.stringify(classes));
			   app.model.set(ModelName.CLASSES, classes);
			   
      		},
      		function error(err)
      		{
      			var eventParameters = getNavigationEventParameters();
      			FlurryAgent.logError("Class Error","Error in fetching Classes", 1);
      			console.log("get class error"+err);
      		}
     	);
}

function getClassesForMappingSubjects(){
	var classes = new diary.collection.ClassCollection();
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query 
			// 
            "select c.id as classId, c.timetable, c.cycleDay, c.periodId, c.subjectId, c.Name, c.Code,c.UniqueID, c.timetable "+
			" from class c ",
            {
				
			},
     	 // success
      		function(statement) {
			  
			   var objClassCollection = _.map(statement.getResult().data, function(item) {
                    return item;
               });
			   if(objClassCollection.length)
               {
                 var classes = new diary.collection.ClassCollection();
                 for(var index = 0; index < objClassCollection.length; index++)
                 {
                	 var modelClass =  new diary.model.Class();
                	 modelClass.set("cycleDay", objClassCollection[index].cycleDay);
                	 modelClass.set("name", objClassCollection[index].Name);
                	 modelClass.set("classId", objClassCollection[index].UniqueID);
                	 modelClass.set("subjectId", objClassCollection[index].subjectId);
                	 modelClass.set("periodId", objClassCollection[index].periodId);
                	 modelClass.set("Code", objClassCollection[index].Code);
                	 modelClass.set("timeTableID", objClassCollection[index].timetable);
                	
                	 classes.push(modelClass);
                  }
                 //console.log("viewParams classes in function : " + JSON.stringify(classes));
                 app.model.set(ModelName.ALLCLASSES, classes);
               }
			  
      		},
      		function error(err)
      		{
      			console.log("get class error"+err);
      		}
     	);
}

function getDiaryItemForClass(strClassId, teacherId){
	/* var year = SystemSettings.HARDCODDEDYEAR;
	 var fromDate = new Rocketboots.date.Day(year, 0, 1);
     // holds the end date for the scholastic year
     var toDate = new Rocketboots.date.Day(year, 11, 31);
     var fromDay = new Rocketboots.date.Day(fromDate);
	var toDay = new Rocketboots.date.Day(toDate);
	*/
	
	var fromDay = app.model.get(ModelName.APPLICATIONSTATE).get('startDate');
	var toDay = app.model.get(ModelName.APPLICATIONSTATE).get('endDate');
   	
	var classId = parseInt(strClassId);
	var subjects = app.model.get(ModelName.SUBJECTS);
	var diaryItemCollection = new diary.collection.DiaryItemCollection();
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
			"select itemTypes.id AS itemType_id, itemTypes.Name AS itemType_name, di.* from DiaryItem as di LEFT OUTER JOIN DiaryItemTypes itemTypes ON di.type_id = itemTypes.UniqueID " +
			"			where  createdBy = :teacherId " +
			"			AND assignedToMe = 0 " +
			"			AND ClassId = :classId " +
			"			AND isDelete = 0 " +
			"			AND type!= 'EVENT' " +
			"			AND  type!= 'NOTE' " +
			"			AND assignedDay >= :fromDay" +
			"			AND assignedDay <= :toDay " +
			"			AND itemTypes.Active = 1 ",
					
			// params
			{
				'teacherId' : teacherId,
				'classId' : classId,
				'fromDay' : fromDay.getKey(),
				'toDay' : toDay.getKey()
			},
	// success
		function(statement) {
			
	
			// go through each of the found results and convert it to a model
			_.forEach(statement.getResult().data, function(dbDiaryItem) {
					var diaryItem, lastDiaryItemId, type, values;
					//alert("got diaryitem");
					type = dbDiaryItem.type.toUpperCase();
					values = {
						'id' : dbDiaryItem.id,
						'type' : dbDiaryItem.type,
						'title' : dbDiaryItem.title,
						'description' : dbDiaryItem.description,
						'created' : new Date(dbDiaryItem.created),
						'createdBy' : dbDiaryItem.createdBy
					};
	
					if(type == "HOMEWORK" || type == "ASSIGNMENT") {
						diaryItem = new diary.model.Task(_.extend(values, {
							'assignedDay' : Rocketboots.date.Day.parseDayKey(dbDiaryItem.assignedDay),
							'assignedTime' : (dbDiaryItem.assignedTime != null ) ? Rocketboots.date.Time.parseTime(dbDiaryItem.assignedTime) : null,
							'dueDay' : Rocketboots.date.Day.parseDayKey(dbDiaryItem.dueDay),
							'dueTime' : (dbDiaryItem.dueTime != null) ? Rocketboots.date.Time.parseTime(dbDiaryItem.dueTime) : null,
							'completed' : (dbDiaryItem.completed == null || dbDiaryItem.completed=="0") ? null:new Date(dbDiaryItem.completed),
							'subject' : getObjectByValue(subjects, dbDiaryItem.subjectId, 'UniqueID'),
							'AssignedBy' : dbDiaryItem.AssingedBy,
							'showDue' : dbDiaryItem.hideFromDue ? false : true,
							'showAssigned' : dbDiaryItem.hideFromAssigned ? false : true,
							'showNotifications' : dbDiaryItem.hideFromNotifications ? false : true,
							'progress' : dbDiaryItem.Progress ? dbDiaryItem.Progress : 0,
							'localDairyItemTypeId' : dbDiaryItem.itemType_id,
							'dairyItemTypeName' : dbDiaryItem.itemType_name,
							'priority' : dbDiaryItem.priority ? dbDiaryItem.priority : "Low",
							'estimatedHours' : dbDiaryItem.EstimatedHours ? dbDiaryItem.EstimatedHours : 0,
							'estimatedMinutes' : dbDiaryItem.EstimatedMins ? dbDiaryItem.EstimatedMins : 0,
							'uniqueId' : dbDiaryItem.UniqueID
						}));
					}
				
				diaryItemCollection.push(diaryItem);
			});
			
		},
		function error()
		{
			console.log("Error fetching tasks");
		}
	);
	//alert("diaryItemCollection : "+JSON.stringify(diaryItemCollection));
	return diaryItemCollection;
}

function getAllClassUsers(classId){
	classId = parseInt(classId);
	//alert("classId : " + classId);
	var studentCollection = new diary.collection.StudentCollection();
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
			"SELECT * FROM student s where s.DeletedStatus = 0 and s.status = 1 and class_id = :classId order by firstName asc",
			// params
			{
				'classId' : classId
			},
     	 // success
      		function(statement) {
			   var diaryItemUsers = _.map(statement.getResult().data, function(item) {
                    return item;
               });
			 //  alert("alert : "+diaryItemUsers.length);
			   if(diaryItemUsers.length)
               {
               
                 for(var index = 0; index < diaryItemUsers.length; index++)
                 {
        			var student = new diary.model.StudentProfile();
					student.set("name",diaryItemUsers[index].firstName);
					student.set("lastName",diaryItemUsers[index].lastName);
					student.set("classId",diaryItemUsers[index].class_id);
					student.set("schoolId",diaryItemUsers[index].school_id);
					student.set("myClass",diaryItemUsers[index].my_class);
					student.set("updateTime",diaryItemUsers[index].updateTime);
					student.set("email",diaryItemUsers[index].email_id);
					student.set("imageUrl",diaryItemUsers[index].image_url);
					student.set("telephone",diaryItemUsers[index].phone_no);
					student.set("studentId",diaryItemUsers[index].student_id);
					student.set("teacherId",diaryItemUsers[index].teacher_id);
					student.set("image",diaryItemUsers[index].image);
					student.set("progress", null);
					student.set("UniqueID",diaryItemUsers[index].student_id)
					studentCollection.push(student);
                 }
               }
      		},
      		function error()
      		{
      			console.log("get All Class Users Error");
      		}
     	);
		console.log("studentCollection : "+JSON.stringify(studentCollection));
     	return studentCollection;
}

function getDiaryItemUser(itemId, userId){
	//alert("alert : itemId"+itemId+"userId : "+userId);
	itemId = parseInt(itemId);
	var studentCollection = new diary.collection.StudentCollection();
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
			"SELECT s.*, du.reminder,du.progress, du.updateDate  FROM student s INNER JOIN diaryItemUser du ON du.student_id = s.student_id "+
            	" where du.isDeleted = 0 AND s.status = 1 AND du.item_id = :itemId and  s.student_id in (select student_id from diaryItemUser "+
            	" WHERE item_id = :itemId) group by s.student_id  order by CAST(du.progress AS DECIMAL) desc",
			// params
			{
				'itemId' : itemId
			},
     	 // success
      		function(statement) {
			   var diaryItemUsers = _.map(statement.getResult().data, function(item) {
                    return item;
               });
			 //  alert("alert : "+diaryItemUsers.length);
			   if(diaryItemUsers.length)
               {
               
                 for(var index = 0; index < diaryItemUsers.length; index++)
                 {
        			var student = new diary.model.StudentProfile();
					student.set("name",diaryItemUsers[index].firstName);
					student.set("lastName",diaryItemUsers[index].lastName);
					student.set("classId",diaryItemUsers[index].class_id);
					student.set("schoolId",diaryItemUsers[index].school_id);
					student.set("myClass",diaryItemUsers[index].my_class);
					student.set("updateTime",diaryItemUsers[index].updateTime);
					student.set("email",diaryItemUsers[index].email_id);
					student.set("imageUrl",diaryItemUsers[index].image_url);
					student.set("telephone",diaryItemUsers[index].phone_no);
					student.set("studentId",diaryItemUsers[index].student_id);
					student.set("teacherId",diaryItemUsers[index].teacher_id);
					student.set("image",diaryItemUsers[index].image);
					student.set("progress", diaryItemUsers[index].progress);
					//console.log("update date : "+Rocketboots.date.Day.parseDateString(diaryItemUsers[index].updateDate));
					var formatedDate = null;
					if(diaryItemUsers[index].reminder != '0000-00-00 00:00:00'){
						var localDate = convertToLocalDate(diaryItemUsers[index].reminder);
						var dateTime = localDate.split(" ");
						var date = dateTime[0].split("-");
						var time = dateTime[1];
						var convertedTime = convertTimeForReminder(time);
						
						if (app.model.get(ModelName.SCHOOLPROFILE).get('RegionalFormat') == "USA")
							formatedDate = shortMonths[(parseInt(date[1]) - 1)]+' '+date[2]+', '+date[0] +' ' + convertedTime;
						else
							formatedDate = date[2]+' '+shortMonths[(parseInt(date[1]) - 1)]+', '+date[0] +' ' + convertedTime;
						
						//console.log("formatedDate"+formatedDate);
					}
					
					student.set("reminder",formatedDate);
					student.set("UniqueID",diaryItemUsers[index].student_id)
					studentCollection.push(student);
                 }
               }
      		},
      		function error()
      		{
      			console.log("Room ID update Error");
      		}
     	);
		console.log("studentCollection : "+JSON.stringify(studentCollection));
     	return studentCollection;
}
 function convertTimeForReminder(time){
	var includeMinutes = true;
		time = time.split(":");
	    var hour = time[0];
	    var minute = parseInt(time[1]);
	    var minuteString = (minute == 0 ? (includeMinutes ? ":00" : "") : ":" + (minute < 10 ? "0" : "") + minute);
	    //console.log("hour : "+hour+"minute : "+minute+"minuteString :"+minuteString);
	    return (hour < 12 ?
			(hour == 0 ? 12 : hour) + minuteString + " AM" : hour - (hour == 12 ? 0 : 12) + minuteString + " PM" );
   
}
//get assigned users of event
function getEventUsers(eventId){
	//alert("alert : itemId"+itemId+"userId : "+userId);
	itemId = parseInt(itemId);
	var studentCollection = new diary.collection.StudentCollection();
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
			"SELECT s.* FROM student s, eventUser eu where eu.userId = s.student_id and s.status = 1 AND eu.eventId = :itemId AND isDeleted = 0 group by s.student_id",
			// params
			{
				'itemId' : eventId
			},
     	 // success
      		function(statement) {
			   var diaryItemUsers = _.map(statement.getResult().data, function(item) {
                    return item;
               });
			 //  alert("alert : "+diaryItemUsers.length);
			   if(diaryItemUsers.length)
               {
               
                 for(var index = 0; index < diaryItemUsers.length; index++)
                 {
        			var student = new diary.model.StudentProfile();
					student.set("name",diaryItemUsers[index].firstName);
					student.set("lastName",diaryItemUsers[index].lastName);
					student.set("classId",diaryItemUsers[index].class_id);
					student.set("schoolId",diaryItemUsers[index].school_id);
					student.set("myClass",diaryItemUsers[index].my_class);
					student.set("updateTime",diaryItemUsers[index].updateTime);
					student.set("email",diaryItemUsers[index].email_id);
					student.set("imageUrl",diaryItemUsers[index].image_url);
					student.set("telephone",diaryItemUsers[index].phone_no);
					student.set("studentId",diaryItemUsers[index].student_id);
					student.set("teacherId",diaryItemUsers[index].teacher_id);
					student.set("image",diaryItemUsers[index].image);
					student.set("UniqueID",diaryItemUsers[index].student_id)
					studentCollection.push(student);
                 }
               }
				          				 
      		},
      		function error()
      		{
      			console.log("Room ID update Error");
      		}
     	);
		console.log("studentCollection : "+JSON.stringify(studentCollection));
     	return studentCollection;
}
function sendReminder(reminderParam){
	
	var receiverIds = "";
	var msgText = null;
	
	for(var index = 0; index < reminderParam.studentIdList.length; index++){
		if(index < (reminderParam.studentIdList.length - 1))
			receiverIds = receiverIds + reminderParam.studentIdList[index] + ",";
		else
			receiverIds = receiverIds + reminderParam.studentIdList[index] ;

	}
	
	msgText = reminderParam.reminderMsg.replace(/\n/g, "\\n")
									   .replace(/\\'/g, "\\'") 
									   .replace(/\\&/g, "\\&") 
									   .replace(/[\\]/g, "\\\\")
									   .replace(/[\/]/g, '\\/')
									   .replace(/[\"]/g, '\\"');

   /* var date = new Date();
	var monthnow = parseInt(new String(((date.getMonth())+1)));
	monthnow = monthnow == 0 ? "00" : (monthnow < 10 ? "0" : "") + monthnow;
	var minuteString = date.getMinutes();
	minuteString = minuteString == 0 ? "00" : (minuteString < 10 ? "0" : "") + minuteString;
	var secondString = date.getSeconds();
	secondString = secondString == 0 ? "00" : (secondString < 10 ? "0" : "") + secondString;
	var hourString = date.getHours();
	hourString = hourString == 0 ? "00" : (hourString < 10 ? "0" : "") + hourString;
	var dayString = date.getDate();
	dayString = dayString == 0 ? "00" : (dayString < 10 ? "0" : "") + dayString;
	var createdDate = date.getFullYear() +"-"+monthnow+"-"+ dayString +" "+hourString+":"+minuteString+":"+secondString;
	*/
	
	var createdDate = reminderParam.createdDate;
	//console.log("this is reminder date :: "+createdDate);
					
	var data = '{"user_id":'+reminderParam.userId+',"DairyitemId":'+reminderParam.DairyitemId+',"ReceiverIds":"'+receiverIds+'","AppType":"Desktop","AppId":"'+reminderParam.appId+'","Description":"'+msgText+'", "Reminder":"'+createdDate+'"}';
	url = SystemSettings.SERVERURL + "/sendreminder?"; 
	console.log("send reminder : "+data+"url: "+url)
	 	
	 	
	var msgObject = JSON.stringify(data);
	 	
	 	
		$.ajax({
	            'url' : url,
	            'type': 'POST',
	            'data': msgObject,
	            'dataType' : "json",
	            'success': function(result) {
	 				console.log("result of reminder : "+JSON.stringify(result));
	 				if(result.Error){
	 					openMessageBox(result.Error);
	 				}
					updateMessage(result.msgid, result.AppId);
					var assignedUsers = result.assignUserId;
					console.log("assignedUsers : "+assignedUsers.length)
					$.each(assignedUsers, function(index, assignedUser) {
						updateDiaryItemUsersReminder(assignedUser);
	 				});
					//console.log("call click..."+reminderParam.DairyitemId);
					var objectSelectedClass = $("#taskListRow"+reminderParam.DairyitemId)
					objectSelectedClass.addClass("taskListRow");
					objectSelectedClass.removeClass("selected");
					
					$("#taskListRow"+reminderParam.DairyitemId).trigger("click");
	            }, 
	            'error': function(response) {
	            	FlurryAgent.logError("Reminder Sync Error", "Error in fetching data from sendreminder web service",1);
	            	if(response.Error){
	            		openMessageBox(response.Error);
	            	} else if(!window.monitors.network.available){
	            		openMessageBox("Reminder will be delivered once network is available.");
	            	}
	            	
	            	//status = response;
	            }
	        });
		//return status;
}
function updateDiaryItemUsersReminder(assignUsers){
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE diaryItemUser set reminder = :reminder, progress = :progress WHERE item_id = :itemId and student_id = :studentId ",

            // params
            {
            	"reminder" : assignUsers.Reminder,
            	"progress" : assignUsers.Progress,
            	"itemId"	   : assignUsers.itemId,
            	"studentId"	   : assignUsers.NormalUserId
            },

            function success(statement)
            {
            	console.log("reminder updated successfully");
            	
            },

            // error
            function error()
            {
            	console.log("error updating message");
            }
        );
	
}

function updateMessage(msgId, appId){
	//console.log("in updateMessage");
    app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE Messages set isSync = 1, UniqueID = :uniqueId WHERE id = :appId",

            // params
            {
            	"uniqueId" : msgId,
            	"appId" : appId
            },

            function success(statement)
            {
            	console.log("Message updated successfully");
            },

            // error
            function error()
            {
            	console.log("error updating message");
            }
        );
}

//edit existing 
function editDiaryItem(detailsDiaryItem){
	console.log("detailsDiaryItem : "+JSON.stringify(detailsDiaryItem));
	var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
    var macAddress=systemConfig.get('macAddresses');
  
	var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	var schoolID=app.model.get(ModelName.SCHOOLPROFILE).get('UniqueID');

	var assingedUserId = "";
	if(detailsDiaryItem.assignTo != null){
		//console.log("detailsDiaryItem.assignTo : "+detailsDiaryItem.assignTo+"detailsDiaryItem.assignTo length : "+detailsDiaryItem.assignTo.length);
		
		for(var index = 0; index < detailsDiaryItem.assignTo.length; index++){
			if(index < (detailsDiaryItem.assignTo.length - 1))
				assingedUserId = assingedUserId + detailsDiaryItem.assignTo[index].id + ",";
			else
				assingedUserId = assingedUserId +  detailsDiaryItem.assignTo[index].id ;
		}
	}
	
	var details = detailsDiaryItem;
	var assignedDay = "";
	 var assignedTime = "";
	 var dueDay = "";
	 var dueTime = "";
	 var startDay = "";
	 var startTime = "";
	 var description = "";
	 var webLink = "";
	 var estimatedHours = "";
     var estimatedMinutes = "";
     var weight = "";
     
	if(detailsDiaryItem.type != "NOTE"){
		assignedDay = detailsDiaryItem.assignedDay != null ? ChangeDate(detailsDiaryItem.assignedDay.getKey()) : null;
        assignedTime = detailsDiaryItem.assignedTime != null ? changeTime(detailsDiaryItem.assignedTime) : null;
        dueDay = detailsDiaryItem.dueDay != null ? ChangeDate(detailsDiaryItem.dueDay.getKey()) : null;
        dueTime = detailsDiaryItem.dueTime != null ? changeTime(detailsDiaryItem.dueTime): null;
        description = detailsDiaryItem.description;
        webLink = detailsDiaryItem.webLinkDesc;
        estimatedHours = detailsDiaryItem.estimatedHours;
        estimatedMinutes = detailsDiaryItem.estimatedMinutes;
        weight = detailsDiaryItem.weight;
	}
	else{
		assignedDay = detailsDiaryItem.atDay != null ? ChangeDate(detailsDiaryItem.atDay.getKey()) : null;
        assignedTime = detailsDiaryItem.atTime != null ? changeTime(detailsDiaryItem.atTime) : null;
        dueDay = detailsDiaryItem.atDay != null ? ChangeDate(detailsDiaryItem.atDay.getKey()) : null;
        dueTime = detailsDiaryItem.atTime != null ? changeTime(detailsDiaryItem.atTime): null;
        startDay = detailsDiaryItem.atDay != null ? ChangeDate(detailsDiaryItem.atDay.getKey()) : null;
        startTime = detailsDiaryItem.atTime != null ? changeTime(detailsDiaryItem.atTime): null;
        details.entries = details.entries.toJSON();
        
        description =details.entries[0].text;
	}
   
    var created =  detailsDiaryItem.created.getTime();
    var subject = details.subject != null ? details.subject.get("title") : null;
    var type_id =  detailsDiaryItem.dairyItemTypeId;
    var type_name =  detailsDiaryItem.dairyItemTypeName;
    
    //for note
    var priority = 0, completed = 0, progress = 0, onTheDay = 0; assignedToMe  = 1;
    if(details.priority != undefined){
    	priority = details.priority;
    }
    if(details.completed != undefined){
    	completed = details.completed != null ? details.completed.getTime() : null;
    	          
    }
    if(details.progress != undefined){
    	progress = details.progress;
    }
  
    if(!details.assignedtoMe)
    {
    	assignedToMe = 0;
    }
    	
    if(details.onTheDay)
    	onTheDay = 1;
	var timstamp=getTimestampmilliseconds();
	var data = '{"Title":"'+details.title+'", "WebLink":"'+webLink+'","Priority":"'+priority+'","AssignedtoMe":"'+assignedToMe
			+'","created":"'+created+'","user_id":'+userId+',"DueDate":"'+dueDay+'","AssingedUserId":"'+assingedUserId
			+'","OnTheDay":'+onTheDay+',"ClassId":'+details.classId+',"AssignedTime":"'+assignedTime+'","Completed":"'
			+completed+'","ipadd":"'+macAddress+'","AssignedDate":"'+assignedDay+'","SchoolId":"'+schoolId+'","subjectName":"'
			+subject+'","Progress":"'+progress+'%","AppId":'+details.id+',"AppType":"desktop","DueTime":"'+dueTime+'","updated":"'
			+timstamp+'","Type":"'+details.type+'", "Id":'+details.uniqueId+', "Description":"'+description+'", "EstimatedHours":"'
			+estimatedHours+'", "EstimatedMins":"'+estimatedMinutes+'", "Weight":"'+weight+'", "type_id":'+type_id+', "type_name":"'+type_name+'"}';
		
	console.log("data to edit : "+data);
	url = SystemSettings.SERVERURL + "/edititem?"; 
	console.log("call to edit diaryItem");
		$.ajax({
	            'url' : url,
	            'type': 'POST',
	            'data': JSON.stringify(data),
	            'dataType' : "json",
	            
	            'success': function(result) {
					 //console.log(result.Status);
					 var serverrecord = result.dairy_item; 
	            	  //console.log("serverrecord : "+JSON.stringify(serverrecord));
	            	 if(serverrecord.type=="EVENT")
		            {
		              parseEvents(serverrecord);
		            }
		            else{
		            	serverrecord.uniqueid = serverrecord.Id;
		            	 var diaryItemObj = mapDiaryItemForInsert(serverrecord);
		            	 parseInsertResposne(diaryItemObj);
		            	 var assignedUsers = serverrecord.assignUserId;
		 	            //alert("assignedUsers : "+JSON.stringify(assignedUsers));
		 	            $.each(assignedUsers, function(index, assignedUser) {
		 	            		assignedUser.UserId = assignedUser.NormalUserId;
		 						insertDiaryItemUsers(assignedUser, userId);
		 				});
		            }
	            	fetchDairyItems();
	            }, 
	            'error': function(response) {
	            	FlurryAgent.logError("Diary Item Sync Error", "Error in fetching data from edititem web service",1);
	            	//alert("error : "+JSON.stringify(response));
	            	//status = response;
	            }
	        });
		//return status;
	
}

// add new diaryItem
function setDiaryItem(detailsDiaryItem){
	console.log("detailsDiaryItem  setDiaryItem: "+JSON.stringify(detailsDiaryItem)+"id : "+detailsDiaryItem.id);
	var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
    var macAddress=systemConfig.get('macAddresses');
  
	if(detailsDiaryItem.id != null){
		
		var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
		var schoolID=app.model.get(ModelName.SCHOOLPROFILE).get('UniqueID');
	
		var assingedUserId = "";
		for(var index = 0; index < detailsDiaryItem.assignTo.length; index++){
			if(index < (detailsDiaryItem.assignTo.length - 1))
				assingedUserId = assingedUserId + detailsDiaryItem.assignTo[index].id + ",";
			else
				assingedUserId = assingedUserId +  detailsDiaryItem.assignTo[index].id ;
		}
			
		var details = detailsDiaryItem;
		var classes = app.model.get(ModelName.CLASSES);
		var subjects = app.model.get(ModelName.SUBJECTS);
		var classObj = getObjectByValue(classes, details.classId, "uniqueId");
		var subName = null;
		if(classObj){
			subName = getObjectByValue(subjects, classObj.get('subjectId'), "id");
		}
		//var assignedTime = details.assignedTime != null ? details.assignedTime.hour + ":" + details.assignedTime.minute + ":" + 00 : null;
		//var dueTime = details.dueTime != null ? details.dueTime.hour + ":" + details.dueTime.minute + ":" + 00 : null;
		var assignedDay = "";
		 var assignedTime = "";
		 var dueDay = "";
		 var dueTime = "";
		 var startDay = "";
		 var startTime = "";
		 var description = "";
		 var notentryAppId = 0;
		 var webLink = "";
		 var estimatedHours = "";
	     var estimatedMinutes = "";
	     var weight = "";
		 
		if(detailsDiaryItem.type != "NOTE"){
			assignedDay = detailsDiaryItem.assignedDay != null ? ChangeDate(detailsDiaryItem.assignedDay.getKey()) : null;
	        assignedTime = detailsDiaryItem.assignedTime != null ? changeTime(detailsDiaryItem.assignedTime) : null;
	        dueDay = detailsDiaryItem.dueDay != null ? ChangeDate(detailsDiaryItem.dueDay.getKey()) : null;
	        dueTime = detailsDiaryItem.dueTime != null ? changeTime(detailsDiaryItem.dueTime): null;
	        description = detailsDiaryItem.description;
	        webLink = detailsDiaryItem.webLinkDesc;
	        estimatedHours = detailsDiaryItem.estimatedHours;
		    estimatedMinutes = detailsDiaryItem.estimatedMinutes;
		    weight = detailsDiaryItem.weight;
		}
		else{
			assignedDay = detailsDiaryItem.atDay != null ? ChangeDate(detailsDiaryItem.atDay.getKey()) : null;
	        assignedTime = detailsDiaryItem.atTime != null ? changeTime(detailsDiaryItem.atTime) : null;
	        dueDay = detailsDiaryItem.atDay != null ? ChangeDate(detailsDiaryItem.atDay.getKey()) : null;
	        dueTime = detailsDiaryItem.atTime != null ? changeTime(detailsDiaryItem.atTime): null;
	        startDay = detailsDiaryItem.atDay != null ? ChangeDate(detailsDiaryItem.atDay.getKey()) : null;
	        startTime = detailsDiaryItem.atTime != null ? changeTime(detailsDiaryItem.atTime): null;
	        details.entries = details.entries.toJSON();
	        description = details.entries[0].text;
	        notentryAppId = details.entries[0].id;
	       
	        details.onTheDay = detailsDiaryItem.atTime != null ? 0 : 1
		}
		
        var created =  detailsDiaryItem.created.getTime();
        var subject = subName != null ? subName.get("title") : "";
        var type_id =  detailsDiaryItem.dairyItemTypeId;
        var type_name =  detailsDiaryItem.dairyItemTypeName;
        
        //for note
        var priority = 0, completed = 0, progress = 0, onTheDay = 0; assignedToMe  = 1;
        if(details.priority != undefined){
        	priority = details.priority;
        }
        if(details.completed != undefined){
        	completed = details.completed != null ? details.completed.getTime() : null;
        }
        if(details.progress != undefined){
        	progress = details.progress;
        }
      
        
       
        if(!details.assignedtoMe)
        	assignedToMe = 0;
        if(details.onTheDay)
        	onTheDay = 1;
		var timstamp=getTimestampmilliseconds();
		
		var data = '{"Title":"'+details.title+'", "WebLink":"'+webLink+'","Priority":"'+priority+'","AssignedtoMe":"'
				+assignedToMe+'","created":"'+created+'","user_id":'+userId+',"DueDate":"'+dueDay+'","AssingedUserId":"'
				+assingedUserId+'","OnTheDay":'+onTheDay+',"ClassId":'+details.classId+',"AssignedTime":"'+assignedTime
				+'","Completed":"'+completed+'","ipadd":"'+macAddress+'","AssignedDate":"'+assignedDay+'","SchoolId":"'
				+schoolId+'","subjectName":"'+subject+'","Progress":"'+progress+'%","AppId":'+details.id
				+',"AppType":"desktop","DueTime":"'+dueTime+'","updated":"'+timstamp+'","Type":"'+details.type+'", "Description":"'
				+description+'", "NotentryAppId" :'+notentryAppId+', "EstimatedHours":"'+estimatedHours+'", "EstimatedMins":"'
				+estimatedMinutes+'", "Weight":"'+weight+'", "type_id":'+type_id+', "type_name":"'+type_name+'"}';
		console.log("data to create : "+data);
		url = SystemSettings.SERVERURL + "/setdiaryitems?"; 
		 	
			$.ajax({
		            'url' : url,
		            'type': 'POST',
		            'data': JSON.stringify(data),
		            'dataType' : "json",
		            'success': function(result) {
						if(result.ErrorCode==0){
							openMessageBox(result.Error);
						}
						else{
							//alert(result.Status);
							console.log("success : "+JSON.stringify(result));
			            	//status = result;
			            	 var serverrecord = result.dairy_item; 
			            	 
			            	 if(serverrecord.type=="EVENT")
				            {
				              parseEvents(serverrecord);
				            }
				            else{
				            	serverrecord.uniqueid = serverrecord.Id;
				            	 var diaryItemObj = mapDiaryItemForInsert(serverrecord);
				            	 parseInsertResposne(diaryItemObj);
				            	 var assignedUsers = serverrecord.assignUserId;
				            	 var noteEntries = serverrecord.notentry;
				            	  console.log("check noteEntries  : "+JSON.stringify(noteEntries));
				 	            $.each(assignedUsers, function(index, assignedUser) {
				 	            		assignedUser.UserId = assignedUser.NormalUserId;
				 	            		console.log("assignedUsers in setdiaryItems : "+JSON.stringify(assignedUsers));
				 						insertDiaryItemUsers(assignedUser, userId);
				 				});
				 	           $.each(noteEntries, function(index, noteEntry) {
				 	        	  console.log("check noteentry");
				 	        	  var mappedNoteEntry = mapNoteEntry(noteEntry);
								  ParseNoteEntry(mappedNoteEntry);
				 	           });
				            }
			            	fetchDairyItems();
						}
						
		            }, 
		            'error': function(response) {
		            	FlurryAgent.logError("Diary Item Sync Error", "Error in fetching data from setdiaryitems web service",1);
		            	//alert("error : "+JSON.stringify(response));
		            	//status = response;
		            }
		        });
			//return status;
	}
	
}
function editEvent(detailsDiaryItem){
	console.log("detailsEvent: "+JSON.stringify(detailsDiaryItem));
	var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
    var macAddress=systemConfig.get('macAddresses');
  
	var schoolID=app.model.get(ModelName.SCHOOLPROFILE).get('UniqueID');
	var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	
	var assingedUserId = "";
	if(detailsDiaryItem.assignTo != null){
		for(var index = 0; index < detailsDiaryItem.assignTo.length; index++){
			if(index < (detailsDiaryItem.assignTo.length - 1))
				assingedUserId = assingedUserId + detailsDiaryItem.assignTo[index].id + ",";
			else
				assingedUserId = assingedUserId +  detailsDiaryItem.assignTo[index].id ;
		}
	}
	
	var details = detailsDiaryItem;
	var classes = app.model.get(ModelName.CLASSES);
	var subjects = app.model.get(ModelName.SUBJECTS);
	var classObj = getObjectByValue(classes, details.classId, "uniqueId");
	var subName = null;
	if(classObj){
		subName = getObjectByValue(subjects, classObj.get('subjectId'), "id");
	}
	
	var subject = subName != null ? subName.get("title") : "";
     //for note
     var priority = 0, completed = 0, progress = 0, publishToClass = 0, onTheDay = 0;
     if(details.priority != undefined){
     	priority = details.priority;
     }
     if(details.completed != undefined){
     	completed = details.completed;
     }
     if(details.progress != undefined){
     	progress = details.progress;
     }
     if(!details.assignedtoMe)
    	 publishToClass = 1;
     if(details.onTheDay)
        	onTheDay = 1;
	//var classObj = getObjectByValue(classes, details.subject.get("id"), "subjectId")
	
	//var assignedTime = details.assignedTime != null ? details.assignedTime.hour + ":" + details.assignedTime.minute + ":" + 00 : null;
	//var dueTime = details.dueTime != null ? details.dueTime.hour + ":" + details.dueTime.minute + ":" + 00 : null;
	var startDate = detailsDiaryItem.startDay != null ? ChangeDate(detailsDiaryItem.startDay.getKey()) : null;
    var startTime = detailsDiaryItem.startTime != null ? changeTime(detailsDiaryItem.startTime) : null;
    var endDate = detailsDiaryItem.startDay != null ? ChangeDate(detailsDiaryItem.startDay.getKey()) : null;
    var endTime = detailsDiaryItem.endTime != null ? changeTime(detailsDiaryItem.endTime) : null;
    var estimatedHours = detailsDiaryItem.estimatedHours;
    var estimatedMinutes = detailsDiaryItem.estimatedMinutes;
    var created =  detailsDiaryItem.created.getTime();
    var type_id =  detailsDiaryItem.dairyItemTypeId;
    var type_name =  detailsDiaryItem.dairyItemTypeName;
    
    console.log("startDate : "+startDate);
    console.log("startTime : "+startTime);
    console.log("endDate : "+endDate);
    console.log("endTime : "+endTime);
    console.log("estimatedHours : "+estimatedHours);
    console.log("estimatedMinutes : "+estimatedMinutes);
    console.log("created : "+created);
    
	var timstamp=getTimestampmilliseconds();

	console.log("data to edit : ");
	var data = '{"Id":"'+details.uniqueId+'", "Name":"'+details.title+'","Details":"'+details.description+'","publishToClass": "'
			+publishToClass+'","created":"'+created+'","user_id":'+userId+',"EndDate":"'+endDate+'","AssingedUserId":"'+assingedUserId
			+'","OnTheDay":'+onTheDay+',"ClassId":'+details.classId+',"StartTime":"'+startTime+'","Completed":"'+completed+'","ipadd":"'
			+macAddress+'","StartDate":"'+startDate+'","SchoolId":"'+schoolId+'","subjectName":"'+subject+'","Progress":"'+progress
			+'%","AppId":'+details.id+',"AppType":"desktop","EndTime":"'+endTime+'","updated":"'+timstamp+'","Type":"'
			+details.type+'", "type_id":'+type_id+', "type_name":"'+type_name+'"}';
	console.log("data to edit : "+data);
	url = SystemSettings.SERVERURL + "/editevent?"; 
	console.log("call to edit diaryItem");
		$.ajax({
	            'url' : url,
	            'type': 'POST',
	            'data': JSON.stringify(data),
	            'dataType' : "json",
	            'success': function(result) {
					console.log("success : "+JSON.stringify(result));
		       	 	var serverrecord = result.event; 
		       	 	console.log("length : "+serverrecord.length);
		       	 	serverrecord.Type="EVENT";
		       	
		       	 	var eventObj = mapDiaryItemForInsert(serverrecord);
		            parseEvents(eventObj);
		            //insert into event user
		            var assignedUsers = serverrecord.assignUserId;
		            $.each(assignedUsers, function(index, assignedUser) {
						insertEventUsers(assignedUser, userId);
					});
		            fetchDairyItems();
	            }, 
	            'error': function(response) {
	            	FlurryAgent.logError("Event Sync Error", "Error in fetching data from editevent web service",1);
	            	//alert("error : "+JSON.stringify(response));
	            }
	        });
		//return status;
	
}

// add new event
function setEvent(detailsDiaryItem){
	console.log("detailsDiaryItem  setDiaryItem event: "+JSON.stringify(detailsDiaryItem)+"id : "+detailsDiaryItem.id);
	var systemConfig = app.model.get(ModelName.SYSTEMCONFIGURATION);
    var macAddress=systemConfig.get('macAddresses');
  
	if(detailsDiaryItem.id != null){
		
		var userId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
		var schoolID=app.model.get(ModelName.SCHOOLPROFILE).get('UniqueID');
	
		var assingedUserId = "";
		for(var index = 0; index < detailsDiaryItem.assignTo.length; index++){
			if(index < (detailsDiaryItem.assignTo.length - 1))
				assingedUserId = assingedUserId + detailsDiaryItem.assignTo[index].id + ",";
			else
				assingedUserId = assingedUserId +  detailsDiaryItem.assignTo[index].id ;
		}
			
		var details = detailsDiaryItem;
		var classes = app.model.get(ModelName.CLASSES);
		 var subject = details.subject != null ? details.subject.get("title") : null;
	     //for note
	     var priority = 0, completed = 0, progress = 0, publishToClass = 0, onTheDay = 0;
	     if(details.priority != undefined){
	     	priority = details.priority;
	     }
	     if(details.completed != undefined){
	     	completed = details.completed;
	     }
	     if(details.progress != undefined){
	     	progress = details.progress;
	     }
	     if(!details.assignedtoMe)
	    	 publishToClass = 1;
	     if(details.onTheDay)
	        	onTheDay = 1;
		//var classObj = getObjectByValue(classes, details.subject.get("id"), "subjectId")
		
		//var assignedTime = details.assignedTime != null ? details.assignedTime.hour + ":" + details.assignedTime.minute + ":" + 00 : null;
		//var dueTime = details.dueTime != null ? details.dueTime.hour + ":" + details.dueTime.minute + ":" + 00 : null;
		var startDate = detailsDiaryItem.startDay != null ? ChangeDate(detailsDiaryItem.startDay.getKey()) : null;
        var startTime = detailsDiaryItem.startTime != null ? changeTime(detailsDiaryItem.startTime) : null;
        var endDate = detailsDiaryItem.startDay != null ? ChangeDate(detailsDiaryItem.startDay.getKey()) : null;
        var endTime = detailsDiaryItem.endTime != null ? changeTime(detailsDiaryItem.endTime) : null;
        var estimatedHours = detailsDiaryItem.estimatedHours;
        var estimatedMinutes = detailsDiaryItem.estimatedMinutes;
        var created =  detailsDiaryItem.created.getTime();
        var type_id =  detailsDiaryItem.dairyItemTypeId;
        var type_name =  detailsDiaryItem.dairyItemTypeName;
        
        console.log("startDate : "+startDate);
        console.log("startTime : "+startTime);
        console.log("endDate : "+endDate);
        console.log("endTime : "+endTime);
        console.log("estimatedHours : "+estimatedHours);
        console.log("estimatedMinutes : "+estimatedMinutes);
        console.log("created : "+created);
        
		var timstamp=getTimestampmilliseconds();
		
		var data = '{"Name":"'+details.title+'","Details":"'+details.description+'","publishToClass":"'+publishToClass+'","created":"'
				+created+'","user_id":'+userId+',"EndDate":"'+endDate+'","AssingedUserId":"'+assingedUserId+'","OnTheDay":"'
				+onTheDay+'","ClassId":'+details.classId+',"StartTime":"'+startTime+'","Completed":"'+completed+'","ipadd":"'
				+macAddress+'","StartDate":"'+startDate+'","SchoolId":"'+schoolId+'","subjectName":"'+subject+'","Progress":"'
				+progress+'%","AppId":'+details.id+',"AppType":"desktop","EndTime":"'+endTime+'","updated":"'+timstamp+'","Type":"'
				+details.type+'", "type_id":'+type_id+', "type_name":"'+type_name+'"}';
		
		
		
		console.log("data to create for event: "+data);
		url = SystemSettings.SERVERURL + "/setevents?"; 
		 	
			$.ajax({
		            'url' : url,
		            'type': 'POST',
		            'data': JSON.stringify(data),
		            'dataType' : "json",
		            'success': function(result) {
						
						console.log("success : "+JSON.stringify(result));
		            	 var serverrecord = result.event; 
		            	 console.log("length : "+serverrecord.length);
		            	 serverrecord.Type="EVENT";
		            	
		            	 var eventObj = mapDiaryItemForInsert(serverrecord);
			             parseEvents(eventObj);
			             //insert into event user
			             var assignedUsers = serverrecord.assignUserId;
		 	             $.each(assignedUsers, function(index, assignedUser) {
		 						insertEventUsers(assignedUser, userId);
		 				 });
		 	            fetchDairyItems();
		            }, 
		            'error': function(response) {
		            	FlurryAgent.logError("Event Sync Error", "Error in fetching data from setevents web service",1);
		            	//alert("error : "+JSON.stringify(response));
		            	//status = response;
		            }
		        });
			//return status;
	}
	
}

function insertEventUsers(assignedEvent){
	console.log("assignedUser : "+assignedEvent);
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query

            " INSERT OR REPLACE INTO eventUser (uniqueId, eventId,userId, isDeleted) "+
            " VALUES (:uniqueId, :eventId, :userId, :isDeleted)",
            // params
            {
                'uniqueId' : assignedEvent.uniqueid,
                'eventId' : assignedEvent.itemId,
                'userId' : assignedEvent.NormalUserId,
                'isDeleted' : assignedEvent.Isdeleted
                
            },

			// success
			function(statement) {
							//record="";
			},
			// error
			function error()
			{
				//record="";
				console.log("Error while inserting diaryItemUser");
			}
	);
	
}

function mapDiaryItemForInsert(record){
	record.Type = record.Type.toUpperCase();
	console.log("record mapDiaryItemForInsert : "+JSON.stringify(record));
	//console.log("record.Completed : "+record.Completed);
	//console.log("record.Completed : "+ record.Progress);
	// record.Progress
	
	if(record.Type == "EVENT"){
		console.log(" record.StartDate "+ record.StartDate);
		record.Title = record.Name;
		record.Completed = "0";
		record.sub_name = null;
		record.Progress = null;
		record.EstimatedHours = null;
		record.EstimatedMins = null;
		record.DueDate = null;
		record.DueTime = null;
		record.AssignedDate = null;
		record.AssignedtoMe = null;
		record.AssignedTime = null;    
		record.SubjectId = null;  
		record.Description =  record.Details;  
		record.CreatedBy = record.NormalCreatedBy;
		
		

	}
	if(record.Type == "NOTE"){
		record.StartDate = record.AssignedDate,
		record.StartTime = record.AssignedTime,
	}
	if(record.OnTheDay == '1'){
		record.EndTime = null;
		record.DueTime = null;
	}
	
	var diaryItemObj =  {
							"uniqueid":record.Id,
							"ipadd": record.IpAddress,
							"created": record.Created,
							"lastupdate": record.Updated, 
							"subjectName": record.sub_name,
							"completed": record.Completed,
							"progress": record.Progress,
							"estimatedHours": record.EstimatedHours,
							"estimatedMins": record.EstimatedMins,
							"dueDate": record.DueDate,
							"dueTime": record.DueTime,
							"schoolId": record.SchoolId,
							"subjectId": record.SubjectId,
							"weight": record.Weight,
							"classId": record.ClassId,
							"onTheDay": record.OnTheDay,
							"priority": record.Priority,
							"appType": record.AppType,
							"appId": record.AppId,
							"title": record.Title,
							"assignedDate": record.AssignedDate,
							"assignedtoMe": record.AssignedtoMe,
							"assignedTime": record.AssignedTime,
							"startDate" : record.StartDate,
							"startTime" : record.StartTime,
							"endDate" : record.EndDate,
							"endTime" : record.EndTime,
							"description": record.Description,
							"WebLink": record.WebLink,
							"type": record.Type,
							"type_id" : record.type_id,
							"type_name" : record.type_name,
							"assignUserId": record.assignUserId,
							'createdBy' : record.NormalCreatedBy,
							'userid' : record.NormalCreatedBy,
							'publishToClass' : record.publishToClass
							
						};
		console.log("diaryItemObj after mapping: "+JSON.stringify(diaryItemObj));
		return diaryItemObj;
}


function mapNoteEntry(noteEntry){
	var noteEntryObj =  {
		'uniqueid' : noteEntry.Id,
		'ipadd' : noteEntry.IpAddress,
		'noteId' : noteEntry.NoteId,
		'text' : noteEntry.Description,
		'appId' : noteEntry.AppId,
		'appType' : noteEntry.AppType,
		'userid' : null,
		'createddate' : noteEntry.Created,
		'lastupdate' : noteEntry.Updated
	}
	return noteEntryObj;
}
function getParents(){
		
	 	var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	 	var url = SystemSettings.SERVERURL + '/getparent';
	 	var data = '{"user_id":'+userId+'}';
	 	
		$.ajax({
	            'url' : url,
	            'type': 'POST',
	            'data': JSON.stringify(data),
	            'dataType' : "json",
	            'success': function(result) {
			
						//alert("getparents details success : "+JSON.stringify(result));
						console.log("alert : getparents details success : "+JSON.stringify(result));
						var parentDetails = result.parent_details;
						
					  // alert("alert : parentDetails.length"+parentDetails.length);
					   if(parentDetails.length)
		               {
		                 var parents = new diary.collection.ParentCollection();
		                 for(var index = 0; index < parentDetails.length; index++)
		                 {
		                	 console.log("parentDetails[index].NormalUserId : "+parentDetails[index].NormalUserId)
		                	 var modelParent =  new diary.model.Parent();
		                	 modelParent.set("uniqueId", parentDetails[index].NormalUserId);
		                	 modelParent.set("email", parentDetails[index].Email);
		                	 modelParent.set("imageUrl", parentDetails[index].ProfileImage);
		                	 modelParent.set("name", parentDetails[index].FirstName + " " + parentDetails[index].LastName);
		                	 modelParent.set("phone", parentDetails[index].Telephone);
		                	 if(parentDetails[index].Status == 1){
		                	 	parents.push(modelParent);
		                	 }
		                  }
		                 app.model.set(ModelName.PARENTS, parents);
		               }
					   saveParentDetails(result.parent_details);
	            }, 
	            'error': function(response) {
	            	FlurryAgent.logError("Parents Sync Error", "Error in fetching data from getparent web service",1);
	            	//alert("getUser details error : "+JSON.stringify(response));
	            }
	        });
}

function saveParentDetails(parentDetails){
		var UserId=app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
		//alert("save parentDetails");
		for(var index = 0; index < parentDetails.length; index++)
		{
			app.locator.getService(ServiceName.APPDATABASE).runQuery(
	                // query
	                "INSERT or replace into myParent (parent_id ,email, image_url, name, phone,status, isDeleted, studentId) VALUES(:uniqueId,:email,:imageUrl,:name,:phone,:status, :isDeleted, :studentId)",
	                // params
	                {
	                	 'uniqueId' : parentDetails[index].NormalUserId,
						 'email' : parentDetails[index].Email,
						 'imageUrl' : parentDetails[index].ProfileImage,
						 'name' : parentDetails[index].FirstName + " " +parentDetails[index].LastName,
						 'phone' : parentDetails[index].Telephone,
						 'status' : parentDetails[index].Status,
						 'studentId' : UserId,
						 'isDeleted' : 0
	                },
					// success
					function(statement) {
	                	console.log("parents saved successfully");
	                	if(parentDetails[index].ProfileImage != null){
	                		downloadUserProfileImage(parentDetails[index].ProfileImage);
	                	}
					},
					
					// error
					function error()
					{
						console.log("Error while trying to save parent");
					}
			);
		}
}

function sendInvitationToParent(emailId){
	//alert("alert : sendInvitationToParent : "+emailId);
 	var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
 	
 	
 	var url = SystemSettings.SERVERURL + '/sendparentinvitation';
 	var data ='{"user_id":'+userId+',"email":"'+emailId+'"}';
 	
 	//console.log("data for sendparentinvitation :"+ data);
	$.ajax({
            'url' : url,
            'type': 'POST',
            'data': JSON.stringify(data),
            'dataType' : "json",
            'success': function(result) {
            		//console.log("invitation sent result : "+JSON.stringify(result));
            		 
            		 if(result.ErrorCode == 0){
            			 openMessageBox(result.Error);
            		 }
            		 else{
            			 openMessageBox(result.Status);
            		 	var eventParameters = getParentInvitationParameters(emailId);
            		 	//alert("eventParameters : "+JSON.stringify(eventParameters));
            			FlurryAgent.logEvent("Parent Invitation",eventParameters);
            		 }
					 $("#parentEmailId").val("");
            }, 
            'error': function(response) {
            	console.log("Error in sending invitation");
    			FlurryAgent.logError("Parent Invitation Error","Error in sending parent invitation", 1);
            }
        });
}

function sendMessageTeacher(params){
	var receiverIdsCc = "";
	var receiverIdsBcc = "";
	
	var currentUserId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	for(var index = 0; index < params.receiver_cc.length ; index++){
		if(index < (params.receiver_cc.length - 1))
			receiverIdsCc = receiverIdsCc + params.receiver_cc[index] + ",";
		else
			receiverIdsCc = receiverIdsCc + params.receiver_cc[index] ;

	}
	for(var index = 0; index < params.receiver_bcc.length ; index++){
		if(index < (params.receiver_bcc.length - 1))
			receiverIdsBcc = receiverIdsBcc + params.receiver_bcc[index] + ",";
		else
			receiverIdsBcc = receiverIdsBcc + params.receiver_bcc[index] ;

	}
	var data = '{"user_id":'+currentUserId+',"receiver_email":"'+params.to+'","cc_email":"'+receiverIdsCc+'","bcc_email":"'+receiverIdsBcc+'", "subject" : "'+params.subject+'","body_text":"'+params.msgText+'"}';
	console.log("send message params: "+data);
	
	 	url = SystemSettings.SERVERURL + "/sendmail?"; 
	 	
		$.ajax({
	            'url' : url,
	            'type': 'POST',
	            'data': JSON.stringify(data),
	            'dataType' : "json",
	            'success': function(result) {
					console.log("result send mail : "+JSON.stringify(result));
					openMessageBox(result.Error)
	            }, 
	            'error': function(response) {
	            	console.log("message sending failed");
	            	FlurryAgent.logError("Mail Sending Error", "Error in sending mail",1);
	            	//alert(response.Error)
	            	//status = response;
	            }
	        });
	
}
//get message users for message id
function getMessageUsers(id){
	var messageUsers;
	console.log("msg id to get mesguser : "+id);
	 
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT * FROM messageUser WHERE messageId = :id",
                // params
                {
            		'id' : id
                },
                function(statement) {
                	messageUsers =_.map(statement.getResult().data, function(item) {
                         return item;
                     });
				},
				
				// error
				function error()
				{
					console.log("Error while fetching message users");
				}
                // success
               
        );
	console.log("messageUsers : "+JSON.stringify(messageUsers));
	return messageUsers;
	
}
function getObjectByValue(list, attrValue, attribute){
	//console.log("alert : list : "+JSON.stringify(list)+"attrValue : "+attrValue);
	var obj = null;
	for(var len = 0 ; len < list.length; len++){
		if(list.at(len).get(attribute) == attrValue){
			obj =  list.at(len);
		}
	}
	return obj;
}

function getObjectByValues(list, attrValue1, attribute1, attrValue2, attribute2, attrValue3, attribute3){
	//console.log("alert : list : "+JSON.stringify(list)+"attrValue : "+attrValue);
	var obj = null;
	for(var len = 0 ; list && len < list.length; len++){
		if(list.at(len).get(attribute1) == attrValue1 
				&& (list.at(len).get(attribute2) == attrValue2 || list.at(len).get(attribute2) == 0)
				&& list.at(len).get(attribute3) == attrValue3){
			obj =  list.at(len);
		}
	}
	return obj;
}

function prepareWeblinkURL(webLinkURL){
	
	var webLink = '';
	if(webLinkURL != null && webLinkURL != ''){
		if(webLinkURL.substr(0,4) == 'http'){
			webLink = webLinkURL;
		} else {
			webLink = 'http://'+webLinkURL;
		}
	}
	
	return webLink;
}

/* Function to read html files */
function readFile(fileURL)
{
	var fileContent = ''; 
	
	var fileToRead = null;
	var fileStream = null;
	
	
	try {
    	
		if (fileURL.indexOf('app:/') != -1)
		{
			fileURL = fileURL.substr(5);
		}
		
		fileToRead = air.File.applicationDirectory.resolvePath(fileURL); 
    	fileStream = new air.FileStream();
    	
    	fileStream.open(fileToRead, air.FileMode.READ); 
    	
    	fileContent = fileStream.readUTFBytes(fileStream.bytesAvailable);
        
        
    } catch (e) {
        //console.log(e);
    	fileContent = ''; 
    } finally {
    	fileStream.close();
    }
	
	return fileContent;
	
	
}


function updateUserProfileImage(userUniqueId)
{
	openMessageBox('updating profile image for unique id = ' + userUniqueId)
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
			// query
            "UPDATE User SET ProfileImage = null WHERE UniqueID= :UniqueID ",

            // params
            {
            	UniqueID:userUniqueId
            },

            function success(statement)
            {
            	
            },

            // error
            function error()
            {
            
            }
        );
}



/* Check if User profile image is downloaded for not */
function checkUserProfileImage() 
{ 
	var isImageAvailable = false;
	var userProfileImgFile = null;
	var userImageFile = null;
	
	var directoryName = null;
	
	
	userProfileImgFile = app.model.get(ModelName.STUDENTPROFILE).get('profileImage');
	
	if (userProfileImgFile != null)
	{
		directoryName = UserProfileImage.USERPROFILEIMAGEDIR;
		
		// store file in Application Storage
		userImageFile = air.File.applicationStorageDirectory.resolvePath((directoryName + "/" + userProfileImgFile));
		
		if (userImageFile.exists)
		{
			isImageAvailable = true;
		}
	}
	
	return isImageAvailable;
}


/* Check if School Background image is downloaded for not */
function checkSchoolBgImage(schoolBgImgFile) 
{ 
	var isImageAvailable = false;
	
	var schoolImgFile = null;
	
	var directoryName = null;
	
	if (schoolBgImgFile != null)
	{
		
		
		// store file in Application Storage
		schoolImgFile = air.File.applicationStorageDirectory.resolvePath(schoolBgImgFile);
		
		if (schoolImgFile.exists)
		{
			isImageAvailable = true;
		}
	}
	
	return isImageAvailable;
}

function getAssignedUsersByUniqueId(id){
	 var assingedUserId = []
	 console.log(" Di id: "+id);
	  
	 app.locator.getService(ServiceName.APPDATABASE).runQuery(
	            // query
	            "SELECT student_id FROM diaryItemUser WHERE item_id = :id and isDeleted = 0",
	                // params
	                {
	            		'id' : id
	                },
	                function(statement) {
	                 diaryItemUsers =_.map(statement.getResult().data, function(item) {
	                         return item;
	                     });
	                
	                 for(var index = 0; index < diaryItemUsers.length; index++){
	                	 var selectedStudent = {
	                			 	"id" :diaryItemUsers[index].student_id
						 };
	                	 assingedUserId.push(selectedStudent);
	                 }
	                
	                 
	                 
	     },
	    
	    // error
	    function error()
	    {
	     console.log("Error while fetching message users");
	    }
	                // success
	               
	 );
	 
	 return assingedUserId;
}

function getTimeStamp() {
	var time=new Date().getTime();
    return time;
}

function deletePeriodRecord(Value)
{
	  app.locator.getService(ServiceName.APPDATABASE).runQuery(
		     // query
		     "DELETE FROM period WHERE UniqueID = :Value",
		
		     // params
		     {
		     	'Value':Value
		     },
		
		     // success
		     function(statement) {
		     	
		     },
		
		     // error
		     function error()
		     {
		     	
		     }
	   );
}

function deleteContentRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "DELETE FROM contentStorage where UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.Id
            },

			// success
			function(statement) {
				//alert("Content deleted successfully");
			              
			},

			// error
			function error()
			{
				//alert("Content delete Error");
			}
	);
	
}

function dataDownloadError(){
	
	var appInstallStatus = app.model.get(ModelName.APPLICATIONSTATE).get('appInstallStatus');
	if((appInstallStatus == 0 || appInstallStatus == 2) && !hasDataDownoloadFailed){
		hasDataDownoloadFailed = true;
		saveAppInstallStatus(2);
		alert('Sorry, unable to download data due to network issue. Please restart application once internet is available.');
		app.context.trigger(EventName.EXITAPPLICATION);
	}
}

function convertToLocalDate(dbDate){
	
	var dateTime;
	var sentDate = dbDate;
	if(dbDate != null || dbDate != undefined || dbDate != '')
	{
	    dateTime = dbDate.split(" ");
	
	    var date = dateTime[0].split("-");
	    var yyyy = date[0];
	    var mm = date[1];
	    var dd = date[2];
	    
	    var time = dateTime[1].split(":");
	    var hour = time[0];
	    var minute = time[1];
	    var seconds = parseInt(time[2]);
	
	    var gmtDate = new Date(yyyy, mm, dd, hour, minute, seconds);
	    var now = new Date();
	    now.setTime(gmtDate.getTime() - (now.getTimezoneOffset() * 60000))
	    
	    var monthnow = parseInt(new String(((now.getMonth()))));
		monthnow = monthnow == 0 ? "00" : (monthnow < 10 ? "0" : "") + monthnow;
		var minuteString = now.getMinutes();
		minuteString = minuteString == 0 ? "00" : minuteString;
		var secondString = now.getSeconds();
		secondString = secondString == 0 ? "00" : (secondString < 10 ? "0" : "") + secondString;
		var hourString = now.getHours();
		hourString = hourString == 0 ? "00" : (hourString < 10 ? "0" : "") + hourString;
		var dayString = now.getDate();
		dayString = dayString == 0 ? "00" : (dayString < 10 ? "0" : "") + dayString;
	    
	    sentDate = now.getFullYear() +"-"+monthnow+"-"+ dayString +" "+hourString+":"+minuteString+":"+secondString;
	}
	return sentDate;
}

function fetchDairyItems(){
	var startDate = app.model.get(ModelName.APPLICATIONSTATE).get('startDate');
	var endDate = app.model.get(ModelName.APPLICATIONSTATE).get('endDate');
			
   	app.context.trigger(EventName.FETCHDIARYITEMS, startDate, endDate, false, false, false);
}

function getDiaryItemEventParameters(diaryItem){
	
	var user_type = "Student";
	if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
		user_type = "Teacher";
	}
	var gradeId = app.model.get(ModelName.STUDENTPROFILE).get('grade');
	var userId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	var schoolId = app.model.get(ModelName.STUDENTPROFILE).get('schoolId');
	
  	var eventParameters = {};
	eventParameters.title = diaryItem.get('title')
	eventParameters.type = diaryItem.get('dairyItemTypeName')
	//eventParameters.dueDay = "20140101"
	eventParameters.grade = gradeId == null ? "0" : gradeId
	eventParameters.schoolId = ""+schoolId
	eventParameters.userId = ""+userId
	eventParameters.platform = "Desktop"
	eventParameters.role = user_type
	
	return eventParameters;
}

function getClassEventParameters(newClass,room,color){
	
	var user_type = "Student";
	if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
		user_type = "Teacher";
	}
	var gradeId = app.model.get(ModelName.STUDENTPROFILE).get('grade');
	var userId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	var schoolId = app.model.get(ModelName.STUDENTPROFILE).get('schoolId');
	
  	var eventParameters = {};
	eventParameters.name = newClass
	eventParameters.roomName = room
	eventParameters.classColor = color
	eventParameters.grade = gradeId == null ? "0" : gradeId
	eventParameters.schoolId = ""+schoolId
	eventParameters.userId = ""+userId
	eventParameters.platform = "Desktop"
	eventParameters.role = user_type
	
	return eventParameters;
}

function getMessageEventParameters(message){
	
	var user_type = "Student";
	if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
		user_type = "Teacher";
	}
	var gradeId = app.model.get(ModelName.STUDENTPROFILE).get('grade');
	var userId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	var schoolId = app.model.get(ModelName.STUDENTPROFILE).get('schoolId');
	
  	var eventParameters = {};
	eventParameters.title = message.get('title')
	eventParameters.sender = ""+message.get('userId')
	eventParameters.receiver = message.get('receiverId')
	eventParameters.grade = gradeId == null ? "0" : gradeId
	eventParameters.schoolId = ""+schoolId
	eventParameters.userId = ""+userId
	eventParameters.platform = "Desktop"
	eventParameters.role = user_type
	
	return eventParameters;
}

function getNavigationEventParameters(){
	
	var user_type = "Student";
	if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
		user_type = "Teacher";
	}
	var gradeId = app.model.get(ModelName.STUDENTPROFILE).get('grade');
	var userId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	var schoolId = app.model.get(ModelName.STUDENTPROFILE).get('schoolId');
	
	if(!schoolId){
		schoolId = app.model.get(ModelName.SCHOOLPROFILE).get("UniqueID");
	}
	
  	var eventParameters = {};
	eventParameters.grade = gradeId == null ? "0" : gradeId
	eventParameters.schoolId = ""+schoolId
	eventParameters.userId = ""+userId
	eventParameters.platform = "Desktop"
	eventParameters.role = user_type
	
	return eventParameters;
}

function getParentInvitationParameters(emailId){
	
	var user_type = "Student";
	if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
		user_type = "Teacher";
	}
	var gradeId = app.model.get(ModelName.STUDENTPROFILE).get('grade');
	var userId = app.model.get(ModelName.STUDENTPROFILE).get('UniqueID');
	var schoolId = app.model.get(ModelName.STUDENTPROFILE).get('schoolId');
	
  	var eventParameters = {};
  	eventParameters.email = emailId
	eventParameters.grade = gradeId == null ? "0" : gradeId
	eventParameters.schoolId = ""+schoolId
	eventParameters.userId = ""+userId
	eventParameters.role = user_type
	
	return eventParameters;
}

function openMessageBox(message){
	//alert(message);
	$(".black_overlay_callout_message").show();
	$(".dialog-messageContent").html(message);
	$(".dialog-message").show();
}

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function openCalendarAccess()
{
	var domain = app.model.get(ModelName.SCHOOLPROFILE).get("domainName");
	var userId = app.model.get(ModelName.STUDENTPROFILE).get("EncrptedUserId");
	
	if(!userId){
		userId = getUserEncryptedId();
	}
	
	if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
		$(".black_overlay_callout_calandar").show();
	    $("#calendarAccessPopup").show();
	    var link = "http://"+ domain +".prime-planner.com/calendar/?pop=1&popOpt=link&userID="+ userId +"&platform=Desktop"; //"http://www.google.com";
	    var params = {
				"calendarAccessLink": link
			}
	    var popupContent = app.render('calendarAccessDetails', params);
		$(".calendarAccess_container").html(popupContent);
		
//		$('iframe').load(function(){
//		    $("#loadingMessage").hide();
//		    //alert("iframe is done loading")
//		}).show();
	}
	
//	if(app.model.get(ModelName.STUDENTPROFILE).get("isTeacher")){
//		var link = "http://"+ domain +".e-planner.com.au/calendar/?pop=1&popOpt=link&userID="+ userId +"&platform=Desktop"; //"http://www.google.com";
//		//alert("link : " + link);
//		var urlReq = new air.URLRequest(link); 
//		air.navigateToURL(urlReq);
//	}
}

function getReports(){
	var userId = (app.model.get(ModelName.STUDENTPROFILE)).get('UniqueID');
	var macAddress=app.model.get(ModelName.SYSTEMCONFIGURATION).get('macAddresses');
	var schoolId = app.model.get(ModelName.STUDENTPROFILE).get('schoolId');
	if(!schoolId){
		schoolId = app.model.get(ModelName.SCHOOLPROFILE).get("UniqueID");
	}
	
 	var url = SystemSettings.SERVERURL + '/getreports';
 	var data = '{"user_id":"'+userId+'","AppType":"Desktop","ipadd":"'+macAddress+'","SchoolId":"'+schoolId+'"}';
 	
 	
	$.ajax({
        'url' : url,
        'type': 'POST',
        'data': JSON.stringify(data),
        'dataType' : "json",
        'success': function(result) {
		 	console.log("getReports details : "+JSON.stringify(result));
		 	var reportDetails = result.Reports;
			
			if(reportDetails.length)
            {
                 var reports = new diary.collection.ReportCollection();
                 for(var index = 0; index < reportDetails.length; index++)
                 {
                	 processReportsRecord(reportDetails[index]);
                	 var report =  new diary.model.Report();
                	 report.set("UniqueID", reportDetails[index].Id);
                	 report.set("isStudent", reportDetails[index].isStudent);
                	 report.set("ReportName", reportDetails[index].ReportName);
                	 report.set("ReportDescription", reportDetails[index].ReportDescription);
                	 report.set("ReportURL", reportDetails[index].ReportURL);
                	 report.set("isTeacher", reportDetails[index].isTeacher);
                	 report.set("isReportActive", reportDetails[index].isReportActive);
                	 report.set("isSchoolActive", reportDetails[index].isReportSchoolActive);
                	 reports.push(report);
                  }
                 app.model.set(ModelName.REPORTS, reports);
            }
			
			getReportsLocal();
		   //saveReportDetails(result.Reports);
        }, 
        'error': function(response) {
        	getReportsLocal();
        	//alert("getReports error : "+JSON.stringify(response));
        }
    });
}

function getReportsLocal(){
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
        // query
        "SELECT * From Report",
		// params
		{
			
		},
		//success
  		function(statement) {
			
		   var reportDetails = _.map(statement.getResult().data, function(item) {
			     return item;
           });
				
		   if(reportDetails.length)
           {
                var reports = new diary.collection.ReportCollection();
                for(var index = 0; index < reportDetails.length; index++)
                {
	               	 var report =  new diary.model.Report();
	               	 report.set("UniqueID", reportDetails[index].UniqueID);
	               	 report.set("isStudent", reportDetails[index].isStudent);
	               	 report.set("ReportName", reportDetails[index].ReportName);
	               	 report.set("ReportDescription", reportDetails[index].ReportDescription);
	               	 report.set("ReportURL", reportDetails[index].ReportURL);
	               	 report.set("isTeacher", reportDetails[index].isTeacher);
	               	 report.set("isReportActive", reportDetails[index].isReportActive);
	               	 report.set("isSchoolActive", reportDetails[index].isSchoolActive);
	               	 reports.push(report);
                 }
                app.model.set(ModelName.REPORTS, reports);
           }
		   
		   var showReports = reportDetails && reportDetails.length > 0;
		   if(!showReports){
				$(".report_li").hide();
		   } else {
			   //alert('reports precaution');
			   $(".report_li").show();
		   }
			          				 
  		},
  		function error()
  		{
  			
  		}
 	);
}

function insertReport(reportDetails){
	
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "INSERT INTO Report (UniqueID ,isStudent, ReportName, ReportDescription, ReportURL,isTeacher, isReportActive, isSchoolActive)"
			+ " VALUES(:UniqueID,:isStudent,:ReportName,:ReportDescription,:ReportURL,:isTeacher, :isReportActive, :isReportSchoolActive)",
            // params
            {
            	 'UniqueID' : reportDetails.Id,
				 'isStudent' : reportDetails.isStudent,
				 'ReportName' : reportDetails.ReportName,
				 'ReportDescription' : reportDetails.ReportDescription,
				 'ReportURL' : reportDetails.ReportURL,
				 'isTeacher' : reportDetails.isTeacher,
				 'isReportActive' : reportDetails.isReportActive,
				 'isReportSchoolActive' : reportDetails.isReportSchoolActive
            },
			// success
			function(statement) {
            	console.log("Report saved successfully");
			},
			
			// error
			function error()
			{
				console.log("Error while trying to save Report");
			}
	);
}

function processReportsRecord(record)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "SELECT id From Report WHERE UniqueID = :UniqueID",
		
            // params
            {
                'UniqueID' : record.Id,
            },

			// success
			function(statement) {
			    var report = _.map(statement.getResult().data, function(item) {
                    return item.id;
               	});  
               	if(report.length == 0){
               		insertReport(record);
               	} else {
               		updateReport(record);
               	}    
               	report = null;
			},

			// error
			function error()
			{
				//alert("Report parse Error");
			}
	);
}

function updateReport(reportDetails)
{
	app.locator.getService(ServiceName.APPDATABASE).runQuery(
            // query
            "UPDATE Report set isStudent = :isStudent, ReportName = :ReportName, ReportDescription = :ReportDescription, ReportURL = :ReportURL, "+
            " isTeacher = :isTeacher, isReportActive = :isReportActive, isSchoolActive = :isReportSchoolActive WHERE UniqueID = :UniqueID",
		
            // params
            {
            	 'UniqueID' : reportDetails.Id,
				 'isStudent' : reportDetails.isStudent,
				 'ReportName' : reportDetails.ReportName,
				 'ReportDescription' : reportDetails.ReportDescription,
				 'ReportURL' : reportDetails.ReportURL,
				 'isTeacher' : reportDetails.isTeacher,
				 'isReportActive' : reportDetails.isReportActive,
				 'isReportSchoolActive' : reportDetails.isReportSchoolActive
            },

			// success
			function(statement) {
            	
			},

			// error
			function error()
			{
				//alert("Report update Error");
			}
	);
}