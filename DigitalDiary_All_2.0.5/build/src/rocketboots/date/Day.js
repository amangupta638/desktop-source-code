/**
 * $Id: Day.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 *
 * A Day. 
 * Allow for simple day based manipulation without having to worry about Time or Timezones
 * 
 * author - Phil Haeusler
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.date) === "undefined") {
	Rocketboots.date = {};
}

/**
 * Defines the Day class	
 * @constructor
 */
Rocketboots.date.Day = (function() {
	
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
		daysInMonthInNonLeapYear = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
		daysInMonthInLeapYear = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
		dayOfYearOffsetInNonLeapYear = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334],
		dayOfYearOffsetInLeapYear = [0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335],
		daysInYear = 365,
		daysInLeapYear = 366;
	
	/**
	 * Creates a new Day.  Can be created in the following formats
	 * 
	 * Create a Day for today
	 * 		var day = new Rocketboots.date.Day();
	 *	
	 * Create a Day for specified date
	 * 		var day = new Rocketboots.date.Day(2012, 2, 18);	
	 *	also supports consts for smarter code
	 * 		var day = new Rocketboots.date.Day(2012, Rocketboots.date.Day.FEBRUARY, Rocketboots.date.Day.LAST_DAY_OF_MONTH);	
	 *
	 * Create a Day from a Javascript Date object
	 *		var date = new Date(2012, 2, 18);
	 * 		var day = new Rocketboots.date.Day(date);	
	 *	
	 * Create a Day from a time string 
	 * 		var date = "2012-01-01 00:00:00";
	 * 		var day = new Rocketboots.date.Day(date);
	 *
	 * Create a duplicate Day from an existing Day object
	 *		var originalDay = new Rocketboots.date.Day(2012, 2, 18);
	 * 		var day = new Rocketboots.date.Day(originalDay);	
	 * @constructor
	 */
	var day = function(sourceOrYear, month, dayInMonth) {
		//console.log("sourceOrYear"+ sourceOrYear);
		var today = new Date(),
			initialDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0);
		
		if (typeof sourceOrYear == "string") {
			var parsed = sourceOrYear.split(" ")[0];
			initialDate = new Date(parsed.split("-")[0], parsed.split("-")[1] - 1, parsed.split("-")[2]);
		}
		else if (sourceOrYear instanceof Date) {
			initialDate = sourceOrYear;
		}
		else if (sourceOrYear instanceof Rocketboots.date.Day) {
			initialDate = sourceOrYear.getDate();
		}
		else if (typeof(sourceOrYear) === "number") {
			if (dayInMonth == Rocketboots.date.Day.LAST_DAY_OF_MONTH) {
				dayInMonth = Rocketboots.date.Day.daysInMonth(sourceOrYear, month);
			}
			//console.log("sourceOrYear="+sourceOrYear);
			initialDate = new Date(sourceOrYear, month, dayInMonth);
			//console.log("initialDate="+initialDate);
		}
	
		this.date = new Date(initialDate);
		this.year = initialDate.getFullYear();
		
		var dayOfThisYearOffset = Rocketboots.date.Day.dayOfYearOffset(this.year);
		this.dayOfYear = dayOfThisYearOffset[initialDate.getMonth()] + initialDate.getDate();

	};
	
	/**
	 * @static
	 * Parses the day key and constructs a day object
	 * 
	 * @param {!string} dateKey The key that represents a date in the format of YYYYMMDD
	 * @return {Rocketboots.date.Day} The day object for the specified day key
 	 */
	day.parseDayKey = function(dayKey) {
		var year,
			month,
			day;
		
		// check that the given key is valid
		if (!dayKey) {
			//throw new Error("Day Key must be specified, key is not optional");
			return null;
		}
		
		// convert to string, if necessary
		if (typeof dayKey == 'number') {
			dayKey = dayKey.toString();
		}
		
		if (dayKey.length != 8) {
			throw new Error("Day Key is not valid, key must be in the format YYYYMMDD");
		}
		
		// try to parse the given string
		year = dayKey.substr(0, 4);
		month = dayKey.substr(4, 2);
		day = dayKey.substr(6, 2);
				
		// generate and return the day object
		return new Rocketboots.date.Day(parseInt(year, 10), parseInt(month, 10) -1, parseInt(day, 10));
	};

	day.parseDateString = function(dateString) {
		var year, month, day, hour, minutes, seconds;
		if (dateString.length != 19) {
			throw new Error("Date string is not valid, must be in the format YYYY-MM-DD hh:mm:ss");
		}

		year = parseInt(dateString.substr(0, 4), 10); 
		month = parseInt(dateString.substr(5, 2), 10) - 1;
		day = parseInt(dateString.substr(8, 2), 10);
		hour = parseInt(dateString.substr(11, 2), 10);
		minutes = parseInt(dateString.substr(14, 2), 10);
		seconds = parseInt(dateString.substr(17, 2), 10);

		return new Rocketboots.date.Day(new Date(year, month, day, hour, minutes, seconds));
	};
	
	/**
	 * Returns the current Day as a javascript Date without any time components
	 * @return {Date}
	 */
	day.prototype.getDate = function() {
		return new Date(this.date);
	};

	/**
	 * Returns the javascript time (number of ms) for the current Day (at midnight)
	 * @return {Number}
	 */
	day.prototype.getTime = function() {
		return this.getDate().getTime();
	};
	
	day.prototype.getHours = function() {
		return this.getDate().getHours();
	};
	
	/**
	 * Returns the 4 digit year
	 * @returns {Number}
	 */
	day.prototype.getFullYear = function() {
		return this.date.getFullYear();
	};
	
	/**
	 * Returns the month (from 0 to 11)
	 * @returns {Number}
	 */
	day.prototype.getMonth = function() {
		return this.date.getMonth();
	};
	
	/**
	 * Returns the month name (i.e. March)
	 * @returns {String}
	 */
	day.prototype.getMonthName = function() {
		return months[this.date.getMonth()];
	};
	
	/**
	 * Returns the short version of the month name (i.e. Mar)
	 * @returns {String}
	 */
	day.prototype.getMonthShortName = function() {
		return shortMonths[this.date.getMonth()];
	};
	
	/**
	 * Returns the day in the month of the Day
	 * @returns {number}
	 */
	day.prototype.getDayInMonth = function() {
		return this.date.getDate();
	};

	/**
	 * Returns the day of the week (from 0 to 6)
	 * @returns {number}
	 */
	day.prototype.getDay = function() {
		return this.getDate().getDay();
	};
	day.prototype.getYear = function() {
		return this.getDate().getFullYear();;
	};
	
	
	/**
	 * For a given date, get the week number (NB, not the ISO week number)
	 *
	 * Algorithm is to find previous monday, it's week of the year is what we are interested in
	 *
	 * Note that dates in one year can be weeks of previous or next year and in more than 1 year.
	 *
	 * e.g. 2014/12/29 is Monday in week  1 of 2015
	 *      2012/1/1   is Sunday in week 52 of 2011
	 */
	day.prototype.getWeek = function () {
	    // Copy date so don't modify original
	    var d = new Date(this.getDate());
	    var startDate = new Rocketboots.date.Day(app.model.get(ModelName.APPLICATIONSTATE).get('startDate'));
	    d.setHours(0,0,0);
	    // Set to nearest Monday: current date - current day number
	    // Make Sunday's day number 7
	    d.setDate(d.getDate() - (d.getDay()||7));
	    // Get first day of year
	    var yearStart = new Date(startDate.getFullYear(),startDate.getMonth(),startDate.getDayInMonth());
	    // Calculate full weeks to previous monday
	    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);

		if(weekNo < 0){
			return 0;
		}
	    // Return week number
	    return weekNo;
	};
	
	/**
	 * Returns a key representing the day in the format YYYYMMDD.  Useful for Day comparisons
	 * @return {String}
	 */
	day.prototype.getKey = function() {
		var month = this.date.getMonth() + 1,
			date = this.date.getDate(),
			key = this.date.getFullYear();
			
		if (month < 10) {
			key += "0";
		}
		
		key += ("" + month);

		if (date < 10) {
			key += "0";
		}
		
		key += ("" + date);

		return key;
	};
	
	/**
	 * Adds the specified number of days to this Day.  Modifies this day.
	 * @param {Integer} count The number of days (positive or negative) to add
	 * @return {Day} itself
	 */
	day.prototype.addDays = function(count) {
	
		this.dayOfYear +=count;
	
		var daysInThisYear = Rocketboots.date.Day.daysInYear(this.year);
	
		while (this.dayOfYear > daysInThisYear) {
			this.dayOfYear -= daysInThisYear;
			this.year++;
			daysInThisYear = Rocketboots.date.Day.daysInYear(this.year);
		}
		
		while (this.dayOfYear < 0) {
			this.year--;
			daysInThisYear = Rocketboots.date.Day.daysInYear(this.year);
			this.dayOfYear += daysInThisYear;
		}
		
		var dayOfThisYearOffset = Rocketboots.date.Day.dayOfYearOffset(this.year);
		for(var i=dayOfThisYearOffset.length-1; i >= 0; i--) {
			if (this.dayOfYear >= dayOfThisYearOffset[i]) {
				var month = i,
					dayOfMonth = this.dayOfYear - dayOfThisYearOffset[i];
					
				this.date = new Date(this.year, month, dayOfMonth);
				break;
			}
		}
		
		return this;
	};
	
	/**
	 * Determine if the current Day is in a Leap Year
	 * @returns {boolean}
	 */
	day.prototype.isLeapYear = function() {
		return Rocketboots.date.Day.isLeapYear(this.year);
	};
	
	/**
	 * Returns a new Day representing the first day of the year
	 * @return {Rocketboots.date.Day}
	 */
	day.prototype.firstDayOfYear = function() {
		return new Rocketboots.date.Day(this.year, Rocketboots.date.Day.JANUARY, Rocketboots.date.Day.FIRST_DAY_OF_MONTH);
	};
	
	/**
	 * Returns a new Day representing the last day of the year
	 * @return {Rocketboots.date.Day}
	 */
	day.prototype.lastDayOfYear = function() {
		return new Rocketboots.date.Day(this.year, Rocketboots.date.Day.DECEMBER, Rocketboots.date.Day.LAST_DAY_OF_MONTH);
	};
	
	/**
	 * Returns a new Day representing the first day of the month
	 * @return {Rocketboots.date.Day}
	 */
	day.prototype.firstDayOfMonth = function() {
		return new Rocketboots.date.Day(this.year, this.date.getMonth(), Rocketboots.date.Day.FIRST_DAY_OF_MONTH);
	};
	
	/**
	 * Returns a new Day representing the last day of the month
	 * @return {Rocketboots.date.Day}
	 */
	day.prototype.lastDayOfMonth = function() {
		return new Rocketboots.date.Day(this.year, this.date.getMonth(), Rocketboots.date.Day.LAST_DAY_OF_MONTH);
	};

	/**
	 * Include a toString method for debugging.
	 * @return {String} A string representation of this object.
	 */
	day.prototype.toString = function () {
		return "Day " + this.date.toString();
	};
	
	
	/**
	 * @static
	 * (static) Determine if specified year is a leap year
	 * @param {Number} year Year to check
	 * @return {Boolean} True if year is a leap year
	 */
	day.isLeapYear = function(year) {
		return (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
	};

	/**
	 * @static
	 * (static) Determine the number of days in a particular year
	 * @param {Number} year Year to check
	 * @return {Number} Number of days in the year
	 */
	day.daysInYear = function(year) {
		return day.isLeapYear(year) ? daysInLeapYear : daysInYear;
	};
	
	/**
	 * @static
	 * (static) Determine number of days in specified month
	 * @param {Number} year Year to check
	 * @param {Number} month Month to check
	 * @return {Number} Number of days in month
	 */
	day.daysInMonth = function(year, month) {
		var isLeapYear = Rocketboots.date.Day.isLeapYear(year);
		return isLeapYear ? daysInMonthInLeapYear[month] : daysInMonthInNonLeapYear[month];
	};

	/**
	 * @static
	 */
	day.dayOfYearOffset = function(year) {
		var isLeapYear = Rocketboots.date.Day.isLeapYear(year);
		return isLeapYear ? dayOfYearOffsetInLeapYear : dayOfYearOffsetInNonLeapYear;
	};

	
	/**
	 * First day of the month, always 1
	 * @const
	 */
	day.FIRST_DAY_OF_MONTH = 1;
	
	/**
	 * Indicates to the Day() constructor to create a new Day on the last day of the specified Year/Month
	 * @const
	 */
	day.LAST_DAY_OF_MONTH = -1;
	
	/**
	 * January
	 * @const
	 */
	day.JANUARY = 0;

	/**
	 * February
	 * @const
	 */
	day.FEBRUARY = 1;

	/**
	 * March
	 * @const
	 */
	day.MARCH = 2;

	/**
	 * April
	 * @const
	 */
	day.APRIL = 3;

	/**
	 * May
	 * @const
	 */
	day.MAY = 4;

	/**
	 * June
	 * @const
	 */
	day.JUNE = 5;

	/**
	 * July
	 * @const
	 */
	day.JULY = 6;

	/**
	 * August
	 * @const
	 */
	day.AUGUST = 7;

	/**
	 * September
	 * @const
	 */
	day.SEPTEMBER = 8;

	/**
	 * October
	 * @const
	 */
	day.OCTOBER = 9;

	/**
	 * November
	 * @const
	 */
	day.NOVEMBER = 10;

	/**
	 * December
	 * @const
	 */
	day.DECEMBER = 11;
	
	return day;
	
})();
