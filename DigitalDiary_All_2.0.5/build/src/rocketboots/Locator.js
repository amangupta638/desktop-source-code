/**
 * $Id: Locator.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

Rocketboots.Locator = (function() {

	var locator = function() {
		_.extend(this, Backbone.Events);
		
		this.services = {};
	};
	
	locator.prototype.mapService = function(serviceName, service, singleton, allowReplace) {
		if (typeof(service) === "undefined") {
			throw('ERROR: Trying to map undefined service to ' + serviceName);
			return;
		}

		if (typeof(allowReplace) === "undefined") {
			allowReplace = true;
		}
		
		if (typeof(this.services[serviceName]) !== "undefined" && !allowReplace) {
			throw('ERROR: Trying to remap existing service ' + serviceName);
			return;
		}
	
		if (typeof(singleton) === "undefined") {
			singleton = true;
		}
		
		this.services[serviceName] = {
			service: service,
			singleton: singleton,
			instance: null
		};
	};
	
	locator.prototype.getService = function(serviceName) {
		var service;
		var thisService = this.services[serviceName];

		if (typeof(thisService) === "undefined") {
			throw('ERROR: Trying to retrieve unknown service ' + serviceName);
			return;
		}
		
		if (thisService.singleton && thisService.instance != null) {
			return thisService.instance;
		}
	
		try {
			
			service = new thisService.service();
			
			if (thisService.singleton) {
				thisService.instance = service;
			}
			
			return service;
			
		} catch (exception) {
			console.log('Uncaught exception while creating Service');
			console.log(exception);
		}
		
		
	};
	
	return locator;
}).call(this);
