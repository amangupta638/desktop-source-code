/**
 * $Id: AsyncOperation.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.util) === "undefined") {
	Rocketboots.util = {};
}

/**
 * A support class for the AsyncOperationRunner.
 * Defines a single async operation.
 *
 * @see Rocketboots.util.AsyncOperationRunner
 */
Rocketboots.util.AsyncOperation = (function() {

	/**
	 * @constructor
	 *
	 * Specifies a new asynchronous operation.
	 *
	 * The first argument defines the operation that should be run, while the subsequent arguments
	 * define callbacks once the operation has been run.
	 *
	 * The first argument "operationObjectOrFunc", if a function, will be executed as the operation.
	 * If it is specified as an object, it must have a function property "exec", and optionally "args" and "context":
	 * 		exec		{!Function} Called when the operation is run.
	 * 		args		{?Array|Function} Defaults to []. Passed as arguments to the exec function. Does NOT include callback functions.
	 * 					If specified as a function, this will be evaluated when the operation is run. Unless the return value is an array, calling run() will throw.
	 * 		context		{?Object} Defaults to the window object. This will be the 'this' parameter within the scope of the exec function.
	 *
	 * @param operationObjectOrFunc {!Function|Object} The operation to be run.
	 * @param successHandler {!Function} The callback called upon success. Arguments are [operation, handlerArgs]
	 * @param errorHandler {!Function} The callback called upon failure. Arguments are [operation, handlerArgs]
	 * @param handlerContext {?Object} Defaults to the window object.
	 * 		This will be the 'this' parameter within the scope of the callback functions.
	 * 		This may differ from the exec context.
	 */
	var operation = function (operationObjectOrFunc, successHandler, errorHandler, handlerContext) {

		var op = this;

		// Settings
		{
			// Default settings
			var settings = {
					exec: null,
					args: [],
					context: window
				};

			// Override settings
			if (_.isFunction(operationObjectOrFunc)) {
				settings.exec = operationObjectOrFunc;

			} else if (_.isObject(operationObjectOrFunc)) {
				// Read properties - override default properties per-operation
				_.extend(settings, operationObjectOrFunc);

				// Check
				if (!_.isFunction(settings.exec)) {
					throw new Error("Poorly defined operation object - exec must be defined.");
				}
				if (!_.isArray(settings.args) && !_.isFunction(settings.args)) {
					throw new Error("Poorly defined operation object - args must be an array or function.");
				}
				if (!settings.context) {
					throw new Error("Poorly defined operation object - context must be defined.");
				}

			} else {
				throw new Error("Poorly defined operation - must be a function or an object.");
			}

			// Store settings
			op._settings = settings;
		}

		// Handlers
		{
			// Check handlers
			if (!_.isFunction(successHandler)) {
				throw new Error("Success handler must be a function");
			}
			if (!_.isFunction(errorHandler)) {
				throw new Error("Error handler must be a function");
			}

			// Store arguments
			op._successHandler = successHandler;
			op._errorHandler = errorHandler;
			op._handlerContext = handlerContext || window; // defaults to window if unspecified
		}

		// State
		{
			this._hasRun = false;
			this._isRunning = false;
			this._result = null;
			this._success = null;
		}
	};

	/**
	 * @private
	 * Called when the operation completes (with either an error or successfully).
	 */
	operation.prototype._complete = function (success, args) {
		// Update state
		this._result = args;
		this._hasRun = true;
		this._isRunning = false;
		this._success = success;

		// Call handler
		this[success ? '_successHandler' : '_errorHandler'].call(this._handlerContext, this, args);
	};

	/**
	 * Begin execution of the operation.
	 * @throws {Error} Error message if the arguments are specified as a function, and the return value is not of type array.
	 * 				If the arguments are not specified as a function, this cannot be thrown.
	 */
	operation.prototype.run = function () {

		var op = this;

		// State
		{
			// Pre-check
			if (this._isRunning || this._hasRun) {
				throw new Error("Cannot run operation multiple times");
			}
			this._isRunning = true;
		}

		// Construct operation arguments
		var argsWithHandlers = [];
		// Support arguments specified as a function
		var argsFromSettings = _.isFunction(op._settings.args) ? op._settings.args.call(op._settings.context) : op._settings.args;
		if (!_.isArray(argsFromSettings)) throw new Error("Operation arguments (specified as a function) were not valid - the argument definition function must return an array.");

		Rocketboots.util.ArrayUtil.pushAll(argsWithHandlers, argsFromSettings);
		argsWithHandlers.push(function () { op._complete(true, arguments); }); // success
		argsWithHandlers.push(function () { op._complete(false, arguments); }); // error

		// Execute operation
		op._settings.exec.apply(op._settings.context, argsWithHandlers);
	};

	/**
	 * @return {Arguments} The arguments passed into the success or error handlers by the operation exec function.
	 */
	operation.prototype.getResults = function () {
		if (!this._hasRun) throw new Error("Cannot retrieve results before the operation has completed.");
		return this._result;
	};

	/**
	 * @return {Boolean} True if the operation returned success, false otherwise.
	 */
	operation.prototype.success = function () {
		if (!this._hasRun) throw new Error("Cannot retrieve the error state before the operation has completed.");
		return this._success;
	};

	/**
	 * @return {Boolean} True if the operation is currently running, false otherwise.
	 */
	operation.prototype.isRunning = function () {
		return this._isRunning;
	};

	/**
	 * Retrieve the computed settings used for this object.
	 * This is the setting defaults, overwritten with the values passed in at the time of construction.
	 * The returned object is guaranteed to contain "exec", "args" and "context" properties.
	 * @return {Object} The computed settings.
	 */
	operation.prototype.getSettings = function () {
		return _.extend({}, this._settings); // return a copy
	};

	return operation;

})();