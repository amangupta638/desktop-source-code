/**
 * $Id: StringUtil.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") Rocketboots = {};
Rocketboots.util = Rocketboots.util || {};

Rocketboots.util.StringUtil = (function() {

	var HTML_ELLIPSIS = "&hellip;";

	var util = function () {
		throw new Error("Cannot construct util class. All methods are static.");
	};

	/**
	 * Get the words from a string.
	 * @param string {!String} The string to operate upon.
	 * @param trimWords {?Boolean} Optional (true is default). If true, trim each word, ensuring no leading/trailing whitespace.
	 * @param excludeEmpty {?Boolean} Optional (true is default). If true, remove empty strings from the word results.
	 * @return {Array} An array containing the words from the original string.
	 */
	util.getWords = function (string, trimWords, excludeEmpty) {
		var words = string.split(" ");
		// trim individual words
		if (trimWords !== false) words = _.map(words, function (word) { return word.trim(); });
		// remove empty words
		if (excludeEmpty !== false) words = _.filter(words, function (word) { return word.length > 0 });
		return words;
	};

	/**
	 * Ensure the number of characters in a string is not above a certain limit.
	 * If it is above the given limit, an ellipsis will be appended to the end of the result.
	 * @param string {String} The string to operate upon.
	 * @param maxCharacters {Number} The maximum number of characters the resultant string should be (ignoring the ellipsis).
	 * @param fullWordsOnly {?Boolean} Optional (default true).
	 * @param ellipsisString {?String} Optional (default "&hellip;"). The character(s) to append to the result upon condensing.
	 * @return {String} The resultant condensed string.
	 */
	util.condense = function (string, maxCharacters, fullWordsOnly, ellipsisString) {

		// Defaults
		if (typeof fullWordsOnly === "undefined" || fullWordsOnly === null) fullWordsOnly = true;
		if (!ellipsisString) ellipsisString = HTML_ELLIPSIS;

		// Early exit if the string doesn't require condensing
		var str = string.trim();
		if (str.length <= maxCharacters) return str;


		if (fullWordsOnly) {
			var wordsUpper = util.getWords(str.substr(0, maxCharacters + 1), true, false),
				wordsLower = util.getWords(str.substr(0, maxCharacters), true, false),
				wordsResult = (wordsUpper.length > wordsLower.length ? wordsLower : wordsLower.slice(0, wordsLower.length-1));

			// If no words fit, use a partial word
			if (wordsResult.length == 0) {
				str = str.substr(0, maxCharacters);
			} else {
				str = wordsResult.join(" ");
			}
		} else {
			str = str.substr(0, maxCharacters);
		}

		return str + ellipsisString;

	};

	return util;

})();
