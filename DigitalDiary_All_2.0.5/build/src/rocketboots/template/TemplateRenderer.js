/**
 * $Id: TemplateRenderer.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

if (typeof(Rocketboots.template) === "undefined") {
	Rocketboots.template = {};
}

Rocketboots.template.TemplateRenderer = (function() {
	
	/**
	  * Creates a new TemplateRenderer.   Intended to be called as follows
	  *
	  * var myRenderer = new TemplateRenderer({
	  *			templates: {
	  *				templateName: 'filename.mustache', ...
	  *			},
	  *			
	  *			initialize: function() {
	  *				// app can be safely started		
	  *			}
	  *		}).initialize();
	  *
	  * @param {object} defaults Default configuration details
	  * @constructor
	  */
	var templateRenderer = function(defaults) {
		_.extend(this, defaults);
		
		this.loaded = false;
		this.partials = {};	
		//alert("typeof($.mustache)"+typeof($.mustache));
		if (typeof($.mustache) === "undefined") {
			if (typeof(com) === "undefined" ||
				typeof(com.hyakugei) === "undefined" ||
				typeof(com.hyakugei.Mustache) === "undefined") {
				throw new Error("Mustache not available");
			}
			
			this.Mustache = new com.hyakugei.Mustache();
		}
	};

	/**
	 * Initializes the TemplateRenderer.  Invokes loadTemplates on the default.templates value, called defaults.initialized() once complete
	 */	
	templateRenderer.prototype.initialize = function() {
		if (typeof(this.templates) !== "undefined") {
			this.loadTemplates(this.templates);	
		}
		else {
			//console.log('TemplateRenderer.initialize() - nothing to initialize');
		}
		
		return this;
	};
	
	/**
	 * Loads remote templates into this TemplateRenderer.  Invokes defaults.initialized() once completed
	 * @param {object} templates Templates to load
	 *			{
	 *				templateName: 'filename.mustache'
	 *			}
	 */	
	templateRenderer.prototype.loadTemplates = function(templates) {
		var templateRenderer = this,
			loadCount = 0;

		$.each(templates, function(index, value) {
			loadCount++;
			
			$.ajax({
				url: value,
				dataType: 'text',
				success: function(content) {
					templateRenderer.partials[index] = content;
				},
				error: function(e) {
					throw new Error('Unable to load template ' + value);
				},
				complete: function() {
					loadCount--;

					if (loadCount == 0) {
						templateRenderer.loaded = true;
						
						if (typeof(templateRenderer.initialized) !== "undefined") {
							templateRenderer.initialized.apply(templateRenderer);
						}
					}
				}
			});	
		});
	};
	
	/**
	 * Loads remote templates into this TemplateRenderer.  Invokes defaults.initialized() once completed
	 * @param {string} name Template name to render
	 * @param {object} params View parameters to include with render
	 */	
	templateRenderer.prototype.render = function(templateName, view) {
		if (typeof(view) === "undefined") {
			view = {};
			console.log('view undefined - setting to empty');
		}
		
		if (typeof(this.partials[templateName]) === "undefined") {
			throw new Error("Template " + templateName + " not defined");
		}
		
		/*if (typeof($.mustache) === "undefined") {	
			// AS3 injected method
			return this.Mustache.startRender(this.partials[templateName], view, this.partials);
		}*/
		
		console.log('TemplateRenderer.prototype.render->templateName: '+templateName);
		
		return Mustache.render(this.partials[templateName], view, this.partials);

		// Javascript method
		//return $.mustache(this.partials[templateName], view, this.partials);
	};
	
	return templateRenderer;

})();
