/**
 * $Id$
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 *
 * A number of common functions that can be used by a number of different class within the
 * application
 */

/*
 * Determines if the given parameter was specified.  By specified we
 * mean that the parameter is defined and is not null
 * 
 * parameter - The parameter that is being checked
 * 
 * return - True if and only if the parameter is defined and not null otherwise false 
 */
isSpecified = function(parameter) {
	
	// determine if the given parameter was defined and it is not NULL
	return (parameter != undefined && parameter != null);
	
};