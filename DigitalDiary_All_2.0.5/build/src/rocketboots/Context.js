/**
 * $Id: Context.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
if (typeof(Rocketboots) === "undefined") {
	Rocketboots = {};
}

Rocketboots.Context = (function() {

	var context = function() {
		_.extend(this, Backbone.Events);
		this.on("all", processEvent, this);
		
		this.events = {};
	};
	
	context.prototype.mapEvent = function(eventName, command) {
		if (typeof(command) === "undefined") {
			throw('WARNING: Trying to map undefined command to event ' + eventName);
		}

		this.events[eventName] = command;
	};
	
	function processEvent() {
		// Convert the arguments quasi-array into a real array
		var commandArguments = [].slice.apply(arguments);
		var eventName = commandArguments.shift();

		var commandToRun = this.events[eventName];
		if (typeof(commandToRun) === "undefined") {
			//console.log('Event [' + eventName + '] not mapped to command, event ignored for command execution');
			return;
		}
		
		var commandInstance;
	
		try {
			commandInstance = new commandToRun();
		}
		catch (exception) {
			//console.log('Uncaught exception while instantiating Command');
			//console.log(exception);
			return;
		}
			
		try {
			commandInstance.execute.apply(commandInstance, commandArguments);
		}
		catch (exception) {
			//console.log('Uncaught exception while running Command');
			try {
				console.log(exception);
			}
			catch(exception) {
				console.log(' - error while dumping exception', exception);
			}
		}
	}
	
	return context;
}).call(this);
