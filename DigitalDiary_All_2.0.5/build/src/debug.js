
if (typeof(console) === 'undefined') {
	// Create console.log() for when running within an AIR container 
	this.console = {
		log: function() {

			if (!SystemSettings.DEBUG) return;
			
			var objToString = function (obj, startOfLine) {
					// setup
					var string = "", tab = "    ";
					
					// defaults
					if (!startOfLine) {
						startOfLine = "";
					}
					
					// basic types
					if (typeof obj === "undefined") {
						return "undefined";
					} else if (obj === null) {
						return "null";
					} else if (typeof obj === "boolean") {
						return (obj ? "true" : "false");
					} else if (typeof obj === "string") {
						return '"' + obj + '"';
						
					} else if (typeof obj === "object") {
						
						if (Object.prototype.toString.call(obj) == "[object Array]" || Object.prototype.toString.call(obj) == "[object Arguments]" || obj instanceof Backbone.Collection) {
							// array (1 line)
							string = "[";
							_.forEach((obj instanceof Backbone.Collection ? obj.toArray() : obj), function (value) {
								string += (string.length > 1 ? ", " : "") + objToString(value, startOfLine + tab);
							});
							string += "]";
							
						} else {
							var hasAttributes = false;
							// object (multi-line)
							string = "{";
							_.forEach(obj, function (objVal, objKey) {
								string += "\n" + startOfLine + tab + objKey + ": " + objToString(objVal, startOfLine + tab);
							});
							// if no attributes, to toString()
							hasAttributes = string.length > 1;
							if (!hasAttributes && obj.toString() !== "[object Object]") {
								string += " " + obj.toString() + " ";
							}
							string += (hasAttributes ? "\n" + startOfLine : "") + "}";
						}
						
						return string;
					} else {
						return obj; // number
					}
				},
				traceArray = [];
			
			// Log using AIRIntrospector
			if (SystemSettings.DEBUGCONSOLE && SystemSettings.DEBUGCONSOLELOGALL && air.Introspector) {
				try {
					air.Introspector.Console.log.apply(this, arguments);
				} catch (e) {
					// Ignore the logging failure (chances are the logger isn't ready)
				}
			}

			// Log using AIR.trace (to the AIR debugger)
			try {
				_.forEach(arguments, function (argument) {
					traceArray.push((typeof argument === "string" ? argument : objToString(argument)));
				});

				air.trace.apply(this, traceArray);
			} catch (e) {
				// Ignore the failure to log
			}
			
//			var args = [].slice.apply(arguments);
//			air.trace.apply(this, args);
		}
	};
}

// How to intercept an $.ajax() call
var jQueryAjaxFunction = $.ajax;
$.ajax = function(settings) {
	//console.log('$.ajax url=', settings.url);
	jQueryAjaxFunction(settings);
};
