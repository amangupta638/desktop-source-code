import os
import string

srcDir = "../../src/"

# list of html pages that load the javascript
htmlPages = ["dialog.html", "index.html", "notification.html", "splash.html", "token.html"]

# TODO someone thought it was a good idea to have Rocketboots as the package name
# but have a dir called rocketboots (with a lower case r) so remove the the .replace("rocketboots", "Rocketboots")
# once this has been fixed

jsFiles = []
templates = []
classes = []
allJsContents = {}
allTemplateContents = {}

print "*** Checking package and class declarations:"

for dirname, dirnames, filenames in os.walk(srcDir):
	for filename in filenames:
		if filename[-3:] == '.js':
			# have found a js file
			packages = dirname[len(srcDir):]
			className = filename[:-3]
			path = packages + ("/" if len(packages) > 0 else "") + filename
			jsFiles.append(path)
			f = open(srcDir + path)
			contents = f.read()
			allJsContents[path] = contents

			if filename[0].isupper():
				# have found a class source file
				packages = packages.split("/")
				f = open(srcDir + path)
				contents = f.readlines()

				# check package declarations
				pp = ""
				for p in packages:
					pp += p.replace("rocketboots", "Rocketboots")
					found = False
					for line in contents:
						if line.startswith(pp):
							found = True
							break
					if not found:
						print "package declaration " + pp + " not found in " + path
					pp += "."

				# check for the class declaration
				if pp == ".":
					pp = ""
				found = False
				pp = pp + className
				for line in contents:
					if line.startswith(pp):
						found = True
						break
				if not found:
					print "Class declaration " + pp + " not found in " + path
				classes.append(pp)
		elif filename.endswith('.mustache'):
			# have found a mustache template
			templates.append(filename[:-9])
			if not dirname.endswith('templates'):
				print filename + " not in the template directory (" + dirname + ")"
			else :
				f = open(dirname + "/" + filename)
				contents = f.read()
				allTemplateContents[filename] = contents



print "\n*** Checking js files are included in the html files:"

htmlContent = ""
for htmlPage in htmlPages:
	f = open(srcDir + htmlPage)
	contents = f.read()
	htmlContent += contents

for path in jsFiles:
	if string.count(htmlContent, path) == 0:
		print path + " not used in any html files"

print "\n*** Checking classes are used in other js sources:"

for className in classes:
	found = False
	className = className.replace("rocketboots", "Rocketboots")
	for path in allJsContents:
		thisClass = path.replace("/", ".")[:-3].replace("rocketboots", "Rocketboots")
		if thisClass != className and string.count(allJsContents[path], className) > 0:
			found = True
			break
	if not found:
		print "Class " + className + " not used in any js sources"

print "\n*** Checking template usages"

for template in templates:
	countJs = 0
	countTemplate = 0
	for path in allJsContents:
		if string.count(allJsContents[path], template) > 0 :
			countJs += 1
	for path in allTemplateContents:
		if string.count(allTemplateContents[path], template) > 0 :
			countTemplate += 1
	if countJs < 2:
		print "Template " + template + " found " + str(countJs) + " times in js sources and " + str(countTemplate) + " times in templates."
