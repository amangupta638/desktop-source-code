/**
 * Unit Tests for the UpdateDiaryItemOnDropCommand
 * 
 * author - Jane Sivieng
 */
describe("UpdateDiaryItemOnDropCommand - Test Suite", function() {

	describe("Cases for UpdateDiaryItemOnDropCommand Suite", function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// mock manager
			var dummyManager = (function() {

				// counts calls to methods
				var calls = {
					updateDiaryItem : 0
				};

				var service = function() {
				};

				service.prototype.updateDiaryItem = function(diaryItem, attachmentsToAdd, attachmentsToRemove,
						successCallback, errorCallback) {

					calls.updateDiaryItem++;
					successCallback(diaryItem);

				};

				service.prototype.getCalls = function(functionName) {
					return calls[functionName];
				};

				return service;

			})();
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({

				modelClass : diary.model.DiaryModel,

				mapEvents : function() {
					// map events needed for the test
				},

				mapServices : function() {
					// map the services needed for the test
					this.mapService(ServiceName.DIARYITEMMANAGER, dummyManager);
				}

			});

			// init the application context
			app.initialize();

			// setup
			var today = (new Rocketboots.date.Day().getMonth() != 11 || new Rocketboots.date.Day().getDayInMonth() != 31)
					? new Rocketboots.date.Day()
					: new Rocketboots.date.Day().addDays(-10),
				tomorrow = (new Rocketboots.date.Day(today)).addDays(1),

				maths = new diary.model.Subject({'title': "Maths", 'colour': "Blue", 'id': 1}),
				english = new diary.model.Subject({'title': "English", 'colour': "Red", 'id' : 2}),

				timetable = new diary.model.Timetable({
					'startDay' : new Rocketboots.date.Day(today),
					'cycleLength': 2
				}),

				classObjects = [
					{
						'periodId': 1,
						'subject': maths,
						'cycleDay': 1
					}, {
						'periodId': 2,
						'subject': english,
						'cycleDay': 1
					},
					{
						'periodId': 1,
						'subject': english,
						'cycleDay': 2
					}, {
						'periodId': 2,
						'subject': maths,
						'cycleDay': 2
					}
				];

			// Add classes to timetable
			_.forEach(classObjects, function (classObject) {
				timetable.get('classes').add(new diary.model.Class(classObject));
			});

			var periodObjects = [
               {
            	   'id' : 1,
            	   'title': 'Period 1',
            	   'isBreak' : false,
            	   'startTime': new Rocketboots.date.Time(9, 0),
            	   'endTime': new Rocketboots.date.Time(10, 0)
               },
               {
            	   'id' : 2,
            	   'title': 'Period 2',
            	   'isBreak' : false,
            	   'startTime': new Rocketboots.date.Time(10, 0),
            	   'endTime': new Rocketboots.date.Time(11, 0)
               }
			];

			// Add periods to timetable
			_.forEach(periodObjects, function (periodObject) {
				timetable.get('periods').add(new diary.model.Period(periodObject));
			});

			var calendar = new diary.model.Calendar({ 'year' : today.getFullYear() });

			calendar.add(new diary.model.CalendarDay({ 
				'day' : new Rocketboots.date.Day(tomorrow),
				'cycleDay' : 2,
				'timetable' : timetable
			}));

			app.model.set(ModelName.CALENDAR, calendar);

		});

		describe("Cases where command is called with only a date", function() {

			it("should be able to invoke the command on a non-period item", function() {
				var today = new Rocketboots.date.Day(),
                    currentTime = Rocketboots.date.Time.parseTime(today.getDate());

				// add an item to the model
				var attrs = {
					id: 1,
					type: diary.model.DiaryItem.TYPE.HOMEWORK,
					dueDay : today,
                    dueTime : null,
                    assignedDay : today,
                    assignedTime : currentTime,
					title: "UpdateDiaryItemOnDropCommand Test (Homework)",
					subject: new diary.model.Subject({'id': 1, 'title': "English", 'colour': "red"})
				};

                var item = new diary.model.Task(attrs);
				app.model.get(ModelName.DIARYITEMS).add(item);

				var newDate = new Rocketboots.date.Day(today.addDays(5));
				
				// execute the command
				var command = new diary.controller.UpdateDiaryItemOnDropCommand();
				command.execute(item, true, newDate);

				// check mock service calls
				var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
				expect(service.getCalls("updateDiaryItem")).toEqual(1);

				// check item was updated in the app model
				var items = app.model.get(ModelName.DIARYITEMS);
				expect(items.length).toEqual(1);
				expect(items.get(1).get("dueDay").getKey()).toEqual(newDate.getKey());
                expect(items.get(1).get("dueTime")).toEqual(null);
			});

		});

		describe("Cases where command is called with a date and time", function() {

			it("should be able to invoke the command on a period item", function() {
				var today = new Rocketboots.date.Day(),
                    currentTime = Rocketboots.date.Time.parseTime(today.getDate());

				// add an item to the model
				var attrs = {
					id: 1,
					type: diary.model.DiaryItem.TYPE.HOMEWORK,
                    dueDay : today,
                    dueTime : null,
                    assignedDay : today,
                    assignedTime : currentTime,
					title: "UpdateDiaryItemOnDropCommand Test (Homework)",
					subject: new diary.model.Subject({'id': 1, 'title': "English", 'colour': "red"})
				};
				var item = new diary.model.Task(attrs);
				app.model.get(ModelName.DIARYITEMS).add(item);
				
				var newDate = today.addDays(1);
                var newTime = currentTime.addMinutes(30);

				var command = new diary.controller.UpdateDiaryItemOnDropCommand();
				command.execute(item, true, newDate, newTime);
				
				// check mock service calls
				var service = app.locator.getService(ServiceName.DIARYITEMMANAGER);
				expect(service.getCalls("updateDiaryItem")).toEqual(1);

				// check item was updated in the app model
				// only date and period should have changed
				var items = app.model.get(ModelName.DIARYITEMS);
				expect(items.length).toEqual(1);
				expect(items.get(1).get("dueDay").getKey()).toEqual(newDate.getKey());
				expect(items.get(1).get("dueTime")).toEqual(newTime);
				expect(items.get(1).get("subject").get('id')).toEqual(1);
			});

		});

		// TODO: what happens if called without a date and time?
	});

});