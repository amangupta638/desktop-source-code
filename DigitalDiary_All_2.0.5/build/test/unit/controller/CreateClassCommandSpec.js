/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: CreateClassCommandSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

// TODO: Check Subject instances for identity, not just attributes
describe("CreateClassCommand Test Suite", function() {

	//--------------------------------------------------------------------------
	//
	//  Test Data
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  subjects
	//----------------------------------

	var subjects = [ new diary.model.Subject({
		"id" : 1,
		"title" : "Test Subject 1",
		"colour" : "red"
	}), new diary.model.Subject({
		"id" : 2,
		"title" : "Test Subject 2",
		"colour" : "aqua"
	}) ];

	//--------------------------------------------------------------------------
	//
	//  Mock Service Callbacks
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  createSubject
	//----------------------------------

	var createSubjectCallbackNew = function(subject, success, error) {

		// return a new instance of a new subject
		success(new diary.model.Subject({
			"id" : 3,
			"title" : subject.get("title"),
			"colour" : subject.get("colour")
		}));

	};

	var createSubjectCallbackExisting = function(subject, success, error) {

		// return a new instance of an existing subject (find by title)
		var a = 0,
			origSubject;
		for (a in subjects) {
			origSubject = subjects[a];
			if (origSubject.get("title") == subject.get("title")) {
				success(new diary.model.Subject({
					"id" : origSubject.get("id"),
					"title" : origSubject.get("title"),
					"colour" : origSubject.get("colour")
				}));
				break;
			} else {
				error("Can't find subject");
			}
		}

	};

	var createSubjectCallbackError = function(subject, success, error) {

		// return an error
		error("Test error message");

	};

	// ----------------------------------
	//  addClassToTimetable
	//----------------------------------

	var addClassToTimetableCallback = function(timetable, classModel, success,
			error) {

		// add class to timetable
		var classes = timetable.get("classes");
		classes.add(classModel);

		success(timetable, classModel);

	};

	var addClassToTimetableCallbackError = function(timetable, classModel,
			success, error) {

		// return an error
		error("Test error message");

	};

	//--------------------------------------------------------------------------
	//
	//  Mock Services
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  TimetableManager
	//----------------------------------

	var MockTimetableManager = (function() {

		var _me = function() {
		};

		_me.prototype.createSubject = createSubjectCallbackExisting;

		_me.prototype.addClassToTimetable = addClassToTimetableCallback;

		return _me;

	})();

	//--------------------------------------------------------------------------
	//
	//  Mock Application
	//
	//--------------------------------------------------------------------------

	var mockApp = new Rocketboots.Application(
			{
				modelClass : diary.model.DiaryModel,

				mapEvents : function() {
				},

				mapServices : function() {
					this.mapService(ServiceName.TIMETABLEMANAGER,
							MockTimetableManager);
				}
			});

	// init the app
	mockApp.initialize();

	//--------------------------------------------------------------------------
	//
	//  Spies
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  Application Events
	//----------------------------------

	var eventSpy = jasmine.createSpy("EventSpy");
	mockApp.context.on(EventName.CLASSCREATED, eventSpy);

	//----------------------------------
	//  Services
	//----------------------------------

	/**
	 * Setup spies on service methods. Pass in custom callbacks to override the
	 * default callbacks on the mock service.
	 */
	var setupServiceSpies = function(createSubjectCallback,
			addClassToTimetableCallback) {

		// get service instance
		var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

		// configure spies
		spies = {
			"addClassToTimetable" : spyOn(service, "addClassToTimetable"),
			"createSubject" : spyOn(service, "createSubject")
		};

		if (createSubjectCallback == null)
			spies.createSubject.andCallThrough();
		else
			spies.createSubject.andCallFake(createSubjectCallback);

		if (addClassToTimetableCallback == null)
			spies.addClassToTimetable.andCallThrough();
		else
			spies.addClassToTimetable.andCallFake(addClassToTimetableCallback);

	};

	//--------------------------------------------------------------------------
	//
	//  Standard Assertions
	//
	//--------------------------------------------------------------------------

	/**
	 * Assert postconditions succeeded.
	 */
	var assertPostConditionsSuccess = function(expectSubjectCreated) {

		var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

		// postcondition: TimetableManager::createSubject() is called
		expect(service.createSubject.callCount).toEqual(1);
		
		// postcondition: TimetableManager::addClassToTimetable() is called
		expect(service.addClassToTimetable.callCount).toEqual(1);

		// postcondition: "classCreated" event is fired
		expect(eventSpy.callCount).toEqual(1);

		var subjects = app.model.get(ModelName.SUBJECTS);
		var subjectCount = 2;	// original number of subjects
		if (expectSubjectCreated) {
			// postcondition: subject added to app model
			expect(subjects.get(3) instanceof diary.model.Subject).toBeTruthy();
			subjectCount++;
		} else {
			// postcondition: subject not added to app model
			expect(subjects.get(3)).toBeUndefined();
		}

		expect(subjects.length).toEqual(subjectCount);

	};

	/**
	 * Assert postconditions failed.
	 */
	var assertPostConditionsFailure = function(expectSubjectCreated) {

		var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

		// postcondition: TimetableManager::createSubject() is not called
		expect(service.createSubject.callCount).toEqual(0);

		// postcondition: TimetableManager::addClassToTimetable() is not called
		expect(service.addClassToTimetable.callCount).toEqual(0);

		// postcondition: "classCreated" event is not fired
		expect(eventSpy.callCount).toEqual(0);

		// postcondition: no subjects added to or removed from app model
		var subjects = app.model.get(ModelName.SUBJECTS);
		expect(subjects.length).toEqual(2);
		expect(subjects.get(1) instanceof diary.model.Subject).toBeTruthy();
		expect(subjects.get(2) instanceof diary.model.Subject).toBeTruthy();
		expect(subjects.get(3)).toBeUndefined();

	};

	//--------------------------------------------------------------------------
	//
	//  Test Cases / Specs
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  setup / teardown
	//----------------------------------

	beforeEach(function() {
		// populate the test data
		mockApp.model.set(ModelName.SUBJECTS,
				new diary.collection.SubjectCollection(subjects));

		// reset the spy counters
		eventSpy.reset();
		
		// replace global app with our mock app
		app = mockApp;
	});

	//----------------------------------
	//  execute() - success
	//----------------------------------

	describe("CreateClassCommand::execute() - Success cases", function() {

		// subject exists; room provided
		it("should succeed with an existent subject", function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});
			var room = "Test Room";

			// execute the command
			var command = new diary.controller.CreateClassCommand();
			command.execute(timetable, periodId, cycleDay, subject.get("title"),
					room);

			// no exceptions / errors

			// standard postconditions
			assertPostConditionsSuccess(false);

		});

		// subject does not exist; room provided
		it("should succeed with a nonexistent subject", function() {

			// setup service spies
			setupServiceSpies(createSubjectCallbackNew, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});
			var room = "Test Room";

			// execute the command
			var command = new diary.controller.CreateClassCommand();
			command.execute(timetable, periodId, cycleDay, subject.get("title"),
					room);

			// no exceptions / errors

			// standard postconditions
			assertPostConditionsSuccess(true);

		});

		// subject exists; room not provided
		it("should succeed without a 'room' argument", function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();
			command.execute(timetable, periodId, cycleDay,
					subject.get("title"));

			// no exceptions / errors

			// standard postconditions
			assertPostConditionsSuccess(false);

		});

	});

	//----------------------------------
	//  execute() - failure
	//----------------------------------

	describe("CreateClassCommand::execute() - Failure cases", function() {

		// timetable argument is not provided
		it("should fail without the 'timetable' argument", function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];

			// execute the command
			var command = new diary.controller.CreateClassCommand();

			expect(function() {
				command.execute(null, periodId, cycleDay, subject.get("title"));
			}).toThrow("Timetable must be specified");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// periodId argument is not provided
		it("should fail without a 'periodId' argument", function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();

			expect(function() {
				command.execute(timetable, null, cycleDay,
						subject.get("title"));
			}).toThrow("Period Identifier must be specified");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// cycleDay argument is not provided
		it("should fail without a 'cycleDay' argument", function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = 10;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();

			expect(function() {
				command.execute(timetable, periodId, null,
						subject.get("title"));
			}).toThrow("Cycle Day must be specified");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// subjectTitle argument is not provided
		it("should fail without a 'subjectTitle' argument", function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();

			expect(function() {
				command.execute(timetable, periodId, cycleDay, null);
			}).toThrow("Subject Title must be specified");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// timetable argument is not Timetable object
		it("should fail when the 'timetable' argument is not of type "
				+ "'diary.model.Timetable'", function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = {
				"classes" : new diary.collection.ClassCollection([classModel])
			};

			// execute the command
			var command = new diary.controller.CreateClassCommand();

			expect(function() {
				command.execute(timetable, periodId, cycleDay,
						subject.get("title"));
			}).toThrow("Timetable must be of type diary.model.Timetable");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// periodId argument is not numeric
		it("should fail when the 'periodId' argument is not of type 'Number'",
				function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = "string";
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();

			expect(function() {
				command.execute(timetable, periodId, cycleDay,
						subject.get("title"));
			}).toThrow("Period Identifier must be of type number");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// cycleDay argument is not numeric
		it("should fail when the 'cycleDay' argument is not of type 'Number'",
				function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = "string";
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();

			expect(function() {
				command.execute(timetable, periodId, cycleDay,
						subject.get("title"));
			}).toThrow("Cycle Day must be of type number");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// subjectTitle argument is not string
		it("should fail when the 'subjectTitle' argument is not of type "
				+ "'String'", function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();

			expect(function() {
				command.execute(timetable, periodId, cycleDay, 3);
			}).toThrow("Subject Title must be of type string");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// room argument is provided and not string
		it("should fail when the 'room' argument is provided, but not of type "
				+ "'String'", function() {

			// setup service spies
			setupServiceSpies(null, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();

			expect(function() {
				command.execute(timetable, periodId, cycleDay,
						subject.get("title"), 3);
			}).toThrow("Room must be of type string");

			// standard postconditions
			assertPostConditionsFailure();

		});

		// TimetableManager::createSubject() error
		it("should fail when the 'TimetableManager::createSubject()' method "
				+ "fails", function() {

			// setup service spies
			setupServiceSpies(createSubjectCallbackError, null);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();
			command.execute(timetable, periodId, cycleDay,
					subject.get("title"));

			// assert partial postconditions
			var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

			// postcondition: TimetableManager::createSubject() is called
			expect(service.createSubject.callCount).toEqual(1);

			// postcondition: TimetableManager::addClassToTimetable() is not
			// called
			expect(service.addClassToTimetable.callCount).toEqual(0);

			// postcondition: "classCreated" event is not fired
			expect(eventSpy.callCount).toEqual(0);

			// postcondition: no subjects added to or removed from app model
			var appSubjects = app.model.get(ModelName.SUBJECTS);
			expect(appSubjects.length).toEqual(2);
			expect(appSubjects.get(1) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(2) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(3)).toBeUndefined();

		});

		// TimetableManager::addClassToTimetable() error
		it("should fail when the 'TimetableManager::addClassToTimetable()' "
				+ "method fails", function() {

			// setup service spies
			setupServiceSpies(null, addClassToTimetableCallbackError);

			// setup execute arguments
			var periodId = 10;
			var cycleDay = 2;
			var subject = subjects[0];
			var classModel = new diary.model.Class({
				"id" : 20,
				"subject" : subject
			});
			var timetable = new diary.model.Timetable({
				"classes" : new diary.collection.ClassCollection([classModel])
			});

			// execute the command
			var command = new diary.controller.CreateClassCommand();
			command.execute(timetable, periodId, cycleDay,
					subject.get("title"));

			// assert partial postconditions
			var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);

			// postcondition: TimetableManager::createSubject() is called
			expect(service.createSubject.callCount).toEqual(1);

			// postcondition: TimetableManager::addClassToTimetable() is called
			expect(service.addClassToTimetable.callCount).toEqual(1);

			// postcondition: "classCreated" event is not fired
			expect(eventSpy.callCount).toEqual(0);

			// postcondition: no subjects added to or removed from app model
			var appSubjects = app.model.get(ModelName.SUBJECTS);
			expect(appSubjects.length).toEqual(2);
			expect(appSubjects.get(1) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(2) instanceof diary.model.Subject)
					.toBeTruthy();
			expect(appSubjects.get(3)).toBeUndefined();

		});

	});

});