/**
 * Unit Tests for the FetchSubjectsCommand
 * 
 * author - Brian Bason
 */
describe("FetchSubjectsCommand - Test Suite", function() {

	/**
	 * Suite of tests for proper operation of the FetchSubjectsCommand
	 */
	describe("Successful cases for FetchSubjectsCommand Suite" , function() {

		// the dummy timetable manager
		var dummyTimetableManager = (function() {

			var service = function() {};

			// the dummy service of the student profile
			service.prototype.getSubjects = function(successCallback , errorCallback) {

				var subjects = new diary.collection.SubjectCollection();
				
				// add some subject dummy data
				subjects.add(new diary.model.Subject({ 'id' : 1,
													   'title' : "English",
													   'colour' : "White" }));
				
				subjects.add(new diary.model.Subject({ 'id' : 2,
					   								   'title' : "Math",
					   								   'colour' : "Blue" }));
				
				successCallback(subjects);

			};

			return service;

		})();
		
		// setup the env for the the unit test
		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

				modelClass:	diary.model.DiaryModel,
				
				mapEvents: function() {
					// map events needed for the test
				},

				mapServices: function() {
					// map the services needed for the test
					this.mapService(ServiceName.TIMETABLEMANAGER, dummyTimetableManager);
				}

			});

			
			// init the application context
			app.initialize();

		});

		/*
		 * invoke the command
		 */
		it("should be able to invoke the command", function() {

			// holds the command
			var command = new diary.controller.FetchSubjectsCommand();
			
			// execute the command
			command.execute();
			
			// get the subjects collection from the model
			var subjects = app.model.get(ModelName.SUBJECTS);

			// the command should have filled the subjects model
			expect(subjects instanceof diary.collection.SubjectCollection).toBeTruthy();
			expect(subjects.length).toEqual(2);
			
			// get the first subject
			var subject = subjects.get(1);
			
			// the subject that was returned should be the same as the dummy data			
			expect(subject instanceof diary.model.Subject).toBeTruthy();
			expect(subject.id).toEqual(1);
			expect(subject.get("title")).toEqual("English");
			expect(subject.get("colour")).toEqual("White");
			
			// get the second subject
			subject = subjects.get(2);
			
			// the subject that was returned should be the same as the dummy data			
			expect(subject instanceof diary.model.Subject).toBeTruthy();
			expect(subject.id).toEqual(2);
			expect(subject.get("title")).toEqual("Math");
			expect(subject.get("colour")).toEqual("Blue");

		});

	});

});