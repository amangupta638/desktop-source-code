/**
 * Unit Tests for the FetchContentItemsCommand
 * 
 * author - Brian Bason
 */
describe("FetchContentItemsCommand - Test Suite", function() {

	describe("Successful cases for FetchContentItemsCommand Suite" , function() {

		// the dummy content manager
		var dummyContentManager = (function() {

			var service = function() {};

			// the dummy service of the get content from remote location
			service.prototype.getContentFromRemoteLocation = function(manifestName, successCallback , errorCallback) {

				successCallback();

			};

            service.prototype.getStoredContentPackages = function(successCallback, errorCallback) {

                successCallback(new diary.collection.ContentPackageCollection());
            };

            service.prototype.addStoredContentPackage = function() {

            };

            return service;

		})();
		
		var dummyDocumentManager = (function() {
		
			var service = function() {};

			service.prototype.deleteDocumentsNotInPackages = function(contentPackagesToKeep, successCallback, errorCallback) {
				successCallback();
			};

			return service;
			
		})();
		
		
		// setup the env for the the unit test
		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

				modelClass:	diary.model.DiaryModel,
				
				mapEvents: function() {
					// map events needed for the test
				},

				mapServices: function() {
					// map the services needed for the test
					this.mapService(ServiceName.CONTENTMANAGER, dummyContentManager);
					this.mapService(ServiceName.DOCUMENTMANAGER, dummyDocumentManager);
				}

			});

			
			// init the application context
			app.initialize();
			
			app.model.set(ModelName.SCHOOLPROFILE, new diary.model.SchoolProfile());
			
			app.model.set(ModelName.MANIFEST, new diary.model.Manifest({
				'contentMapping': new diary.model.ContentMapping({
					'subItems': {},
					'calendarBanners': new diary.collection.ContentItemCollection(),
					'shortcuts': new diary.collection.ContentItemCollection()
				})
			}));
		});

		/*
		 * invoke the command
		 */
		it("should be able to invoke the command", function() {

			// holds the command
			var command = new diary.controller.FetchContentItemsCommand();
			
			// execute the command
			command.execute();
			
			// get the school profile from the model
			var schoolProfile = app.model.get(ModelName.SCHOOLPROFILE);

			// the delegate should have returned a student profile
			expect(schoolProfile).not.toBeNull();
			expect(schoolProfile.get('contentMapping') instanceof diary.model.ContentMapping).toBeTruthy();

			// get the content mapping from the school profile
			var contentMapping = schoolProfile.get('contentMapping');
			
			// the profile that was returned should be the same as the dummy data
			expect(contentMapping.get('subItems') instanceof Object).toBeTruthy();
			expect(contentMapping.get('calendarBanners') instanceof diary.collection.ContentItemCollection).toBeTruthy();
			expect(contentMapping.get('shortcuts') instanceof diary.collection.ContentItemCollection).toBeTruthy();

		});

	});

});