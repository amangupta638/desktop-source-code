/**
 * Unit Tests for the GenerateCalendarCommandSuite
 * 
 * author - Brian Bason
 */
describe("GenerateCalendarCommandSuite - Test Suite", function() {

	/**
	 * Suite of tests for proper operation of the GenerateCalendarCommandSuite
	 */
	describe("Successful cases for GenerateCalendarCommandSuite" , function() {

		// the dummy calendar manager
		var dummyCalendarManager = (function() {

			var service = function() {};

			// the dummy service of the calendar
			service.prototype.getCalendar = function(year, subjects, successCallback , errorCallback) {

				// the calendar
				var calendar = new diary.model.Calendar({ 'year' : new Date().getFullYear() });
				
				successCallback(calendar);

			};

			return service;

		})();
		
		// setup the env for the the unit test
		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

				modelClass:	diary.model.DiaryModel,
				
				mapEvents: function() {
					// map events needed for the test
				},

				mapServices: function() {
					// map the services needed for the test
					this.mapService(ServiceName.CALENDARMANAGER, dummyCalendarManager);
				}

			});

			
			// init the application context
			app.initialize();

		});

		/*
		 * invoke the command
		 */
		it("should be able to invoke the command", function() {

			// holds the command
			var command = new diary.controller.GenerateCalendarCommand();
			
			// execute the command
			command.execute();
			
			// get the calendar from the application profile
			var calendar = app.model.get(ModelName.CALENDAR);

			// the model should contain the calendar
			expect(calendar).not.toBeNull();
			expect(calendar instanceof diary.model.Calendar).toBeTruthy();

			// the profile that was returned should be the same as the dummy data
			expect(calendar.get("year")).toEqual(new Date().getFullYear());
			expect(calendar.get("calendarDays").length).toEqual(0);


		});

	});

});