/**
 * Unit Tests for the UpdateSubjectCommand
 * 
 * author - Justin Judd
 */
describe("UpdateSubjectCommand - Test Suite", function() {

	describe("Successful cases for UpdateSubjectCommand Suite", function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// mock manager
			var dummyManager = (function() {

				// counts calls to methods
				var calls = {
					updateSubject : 0
				};

				var service = function() {
				};

				service.prototype.updateSubject = function(subject, successCallback, errorCallback) {

					calls.updateSubject++;
					successCallback(subject);

				};

				service.prototype.getCalls = function(functionName) {
					return calls[functionName];
				};

				return service;

			})();

			// configure the application context for the unit test
			app = new Rocketboots.Application({

				modelClass : diary.model.DiaryModel,

				mapEvents : function() {
					// map events needed for the test
				},

				mapServices : function() {
					// map the services needed for the test
					this.mapService(ServiceName.TIMETABLEMANAGER, dummyManager);
				}

			});

			// init the application context
			app.initialize();

		});

		it("should be able to invoke the command", function() {

			var attrs = {
				id: 1,
				title: "updateSubjectCommand Test 1",
				colour: "green"
			};
			var subject = new diary.model.Subject(attrs);

			app.model.get(ModelName.SUBJECTS).add(new diary.model.Subject({ 'id': 1,
																			'title': "Test Subject",
																			'colour': "Red" }));
			
			// execute the command
			var command = new diary.controller.UpdateSubjectCommand();
			command.execute(subject);

			// check mock service calls
			var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);
			expect(service.getCalls("updateSubject")).toEqual(1);

		});

		it("should be able to invoke the command twice", function() {

			var attrs1 = {
				id: 1,
				title: "updateSubjectCommand Test 1",
				colour: "green"
			};
			var subject1 = new diary.model.Subject(attrs1);

			var attrs2 = {
					id: 2,
					title: "updateSubjectCommand Test 2",
					colour: "blue"
			};
			var subject2 = new diary.model.Subject(attrs2);

			app.model.get(ModelName.SUBJECTS).add(new diary.model.Subject({ 'id': 1,
																			'title': "Test Subject 1",
																			'colour': "Red" }));
			
			app.model.get(ModelName.SUBJECTS).add(new diary.model.Subject({ 'id': 2,
																			'title': "Test Subject 2",
																			'colour': "Green" }));
			
			// execute the command twice
			var command1 = new diary.controller.UpdateSubjectCommand();
			command1.execute(subject1);

			var command2 = new diary.controller.UpdateSubjectCommand();
			command2.execute(subject2);

			// check mock service calls
			var service = app.locator.getService(ServiceName.TIMETABLEMANAGER);
			expect(service.getCalls("updateSubject")).toEqual(2);

		});

	});

});