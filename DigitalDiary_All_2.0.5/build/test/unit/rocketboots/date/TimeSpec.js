/**
 * $Id: TimeSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
describe("Time - Test Suite", function() {

	/**
	 * Suite of tests which should create a Time
	 */
	describe("Successful creation of Time" , function() {
		
		beforeEach(function() {
			
		});

		it("should be able to create a Time on specified hour and minute as Integer", function() {
		   
			var time = new Rocketboots.date.Time(9,12);

            expect(time.getHour()).toEqual(9);
            expect(time.getMinute()).toEqual(12);

            time = new Rocketboots.date.Time(0,0);

            expect(time.getHour()).toEqual(0);
            expect(time.getMinute()).toEqual(0);

            time = new Rocketboots.date.Time(23,59);

            expect(time.getHour()).toEqual(23);
            expect(time.getMinute()).toEqual(59);

		});

        it("should be able to create a Time on specified hour and minute as String", function() {

            var time = new Rocketboots.date.Time("9","12");

            expect(time.getHour()).toEqual(9);
            expect(time.getMinute()).toEqual(12);

            time = new Rocketboots.date.Time("0","0");

            expect(time.getHour()).toEqual(0);
            expect(time.getMinute()).toEqual(0);

            time = new Rocketboots.date.Time("23","59");

            expect(time.getHour()).toEqual(23);
            expect(time.getMinute()).toEqual(59);

        });

       it("should be able to parse a Time from a specified HHmm string", function() {

            var time = Rocketboots.date.Time.parseTime("0912");

            expect(time.getHour()).toEqual(9);
            expect(time.getMinute()).toEqual(12);

            time = Rocketboots.date.Time.parseTime("0000");

            expect(time.getHour()).toEqual(0);
            expect(time.getMinute()).toEqual(0);

            time = Rocketboots.date.Time.parseTime("2359");

            expect(time.getHour()).toEqual(23);
            expect(time.getMinute()).toEqual(59);

           time = Rocketboots.date.Time.parseTime("22:30");

           expect(time.getHour()).toEqual(22);
           expect(time.getMinute()).toEqual(30);

           time = Rocketboots.date.Time.parseTime("2:15");

           expect(time.getHour()).toEqual(2);
           expect(time.getMinute()).toEqual(15);

           time = Rocketboots.date.Time.parseTime("04:20");

           expect(time.getHour()).toEqual(4);
           expect(time.getMinute()).toEqual(20);

           time = Rocketboots.date.Time.parseTime("4:2");

           expect(time.getHour()).toEqual(4);
           expect(time.getMinute()).toEqual(2);

        });

        it("should be able to parse a Time from a specified integer value", function() {

            var time = Rocketboots.date.Time.parseTime(912);

            expect(time.getHour()).toEqual(9);
            expect(time.getMinute()).toEqual(12);

            time = Rocketboots.date.Time.parseTime(0);

            expect(time.getHour()).toEqual(0);
            expect(time.getMinute()).toEqual(0);

            time = Rocketboots.date.Time.parseTime(2359);

            expect(time.getHour()).toEqual(23);
            expect(time.getMinute()).toEqual(59);

        });

        it("should be able to convert a Time object to string", function() {

            var time = Rocketboots.date.Time.parseTime(912);

            expect(time.toString()).toEqual("0912");

            time = Rocketboots.date.Time.parseTime(0);

            expect(time.toString()).toEqual("0000");

            time = Rocketboots.date.Time.parseTime(2359);

            expect(time.toString()).toEqual("2359");

        });

        it("should be able to convert a Time object to integer", function() {

            var time = Rocketboots.date.Time.parseTime(912);

            expect(time.toInteger()).toEqual(912);

            time = Rocketboots.date.Time.parseTime(0);

            expect(time.toInteger()).toEqual(0);

            time = Rocketboots.date.Time.parseTime(2359);

            expect(time.toInteger()).toEqual(2359);

        });

	});

	/**
	 * Suite of tests which should fail
	 */
	describe("Failure creation of Time object" , function() {
		
		beforeEach(function() {
			
		});

        it("should fail to create a Time on negative hour as Integer", function() {

            expect(function() {
                new Rocketboots.date.Time(-1,12);
            }).toThrow("Hour is not valid, hour must be a positive integer value");

        });

        it("should fail to create a Time on invalid hour as Integer", function() {

            expect(function() {
                new Rocketboots.date.Time(24,12);
            }).toThrow("Hour is not valid, hour must be between 0 and 23");

        });

        it("should fail to create a Time on invalid minute as Integer", function() {

            expect(function() {
                new Rocketboots.date.Time(1,-12);
            }).toThrow("Minute is not valid, minute must be a positive integer value");

        });

        it("should fail to create a Time on invalid minute as Integer", function() {

            expect(function() {
                new Rocketboots.date.Time(1,60);
            }).toThrow("Minute is not valid, minute must be between 0 and 59");

        });

        it("should fail to create a Time on a negative time as Integer", function() {

            expect(function() {
                Rocketboots.date.Time.parseTime(-1);
            }).toThrow("Time is not valid, time must be a positive integer value");

        });

        it("should fail to create a Time on a invalid time as Integer", function() {

            expect(function() {
                Rocketboots.date.Time.parseTime(2360);
            }).toThrow("Minute is not valid, minute must be between 0 and 59");

        });

        it("should fail to create a Time on a negative time as String", function() {

            expect(function() {
                Rocketboots.date.Time.parseTime("-100");
            }).toThrow("Hour is not valid, hour must be a positive integer value");

        });

        it("should fail to create a Time on a invalid time as String", function() {

            expect(function() {
                Rocketboots.date.Time.parseTime("2360");
            }).toThrow("Minute is not valid, minute must be between 0 and 59");

        });

	});
	
});