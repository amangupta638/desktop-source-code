/**
 * $Id: ViewSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 * Copyright (c) 2013, RocketBoots
 * All rights reserved
 */
describe("View - Test Suite", function() {

	it("Creation with default values", function () {
		
		var view = new Rocketboots.view.View();
		
		expect(view.isVisible).toBeTruthy();
		expect(view.childViews).toEqual({});
		expect(view.lastVisibleChildView).toBeNull();
		
	});
	
	it("Set view visible", function () {
		
		var view = new Rocketboots.view.View();
		expect(view.isVisible).toBeTruthy();
		
		view.setViewVisible(false);
		expect(view.isVisible).toBeFalsy();
		
		view.setViewVisible(true);
		expect(view.isVisible).toBeTruthy();
		
	});
	
	it("Set view visible, with child views", function () {
		
		// Setup
		var view = new Rocketboots.view.View(),
			subview1 = new Rocketboots.view.View(),
			subview2 = new Rocketboots.view.View();
		
		view.childViews = _.extend({}, view.childViews, {
			'subview1': subview1,
			'subview2': subview2
		});
		view.lastVisibleChildView = 'subview1';
		subview2.setViewVisible(false);
		
		// Initial checks
		expect(view.isVisible).toBeTruthy("Initial check, view visible");
		expect(view.childViews.subview1).toEqual(subview1, "Initial check, view->subview1 is the correct instance");
		expect(view.childViews.subview2).toEqual(subview2, "Initial check, view->subview2 is the correct instance");
		expect(subview1.isVisible).toBeTruthy("Initial check, subview1 visible");
		expect(subview2.isVisible).toBeFalsy("Initial check, subview2 invisible");
		expect(view.childViews.subview1.isVisible).toBeTruthy("Initial check, view->subview1 visible");
		expect(view.childViews.subview2.isVisible).toBeFalsy("Initial check, view->subview2 invisible");
		
		// Set to invisible
		view.setViewVisible(false);
		expect(view.isVisible).toBeFalsy("First inivisble, view");
		expect(view.childViews.subview1.isVisible).toBeFalsy("First inivisble, subview1");
		expect(view.childViews.subview2.isVisible).toBeFalsy("First inivisble, subview2");
		
		// Set to visible
		view.setViewVisible(true);
		expect(view.isVisible).toBeTruthy();
		expect(view.childViews.subview1.isVisible).toBeTruthy();
		expect(view.childViews.subview2.isVisible).toBeFalsy();
		
		// Set to invisible, then visible, with a different 'last selected view'
		view.lastVisibleChildView = 'subview2';
		subview1.setViewVisible(false);
		subview2.setViewVisible(true);
		
		expect(view.childViews.subview1.isVisible).toBeFalsy();
		expect(view.childViews.subview2.isVisible).toBeTruthy();

		view.setViewVisible(false);
		expect(view.childViews.subview1.isVisible).toBeFalsy();
		expect(view.childViews.subview2.isVisible).toBeFalsy();

		view.setViewVisible(true);
		expect(view.childViews.subview1.isVisible).toBeFalsy();
		expect(view.childViews.subview2.isVisible).toBeTruthy();
		
	});
	
	it("Set subview visible by name", function () {
		
		// Setup
		var view = new Rocketboots.view.View(),
			subview1 = new Rocketboots.view.View(),
			subview2 = new Rocketboots.view.View();
		
		view.childViews = _.extend({}, view.childViews, {
			'subview1': subview1,
			'subview2': subview2
		});
		
		view.setChildViewVisibleByName('subview1', false);
		expect(subview1.isVisible).toBeFalsy();
		expect(subview2.isVisible).toBeTruthy();
		
		view.setChildViewVisibleByName('subview1', true);
		view.setChildViewVisibleByName('subview2', false);
		expect(subview1.isVisible).toBeTruthy();
		expect(subview2.isVisible).toBeFalsy();
		
	});
	
	it("Hide all subviews except", function () {
		
		// Setup
		var view = new Rocketboots.view.View(),
			subview1 = new Rocketboots.view.View(),
			subview2 = new Rocketboots.view.View();
		
		view.childViews = _.extend({}, view.childViews, {
			'subview1': subview1,
			'subview2': subview2
		});
		
		expect(subview1.isVisible).toBeTruthy();
		expect(subview2.isVisible).toBeTruthy();
		
		view.hideAllViewsExcept(null);
		expect(subview1.isVisible).toBeFalsy();
		expect(subview2.isVisible).toBeFalsy();
		
		view.hideAllViewsExcept('subview2');
		expect(subview1.isVisible).toBeFalsy();
		expect(subview2.isVisible).toBeTruthy();
	});
	
});