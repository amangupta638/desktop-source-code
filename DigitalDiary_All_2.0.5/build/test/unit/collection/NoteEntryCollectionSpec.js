/**
 * Unit Tests for the Note Entry Collection
 * 
 * author - Jane Sivieng
 */
describe("Note Entry Collection - Test Suite", function() {

	/*
	 * note with all entries in time order
	 */
	it("note entries should be in timestamp order", function() {
		
		// create the test note that we will use for the test
		var note = new diary.model.Note(),
			entries = note.get('entries'),
			now = new Date().getTime(),
			previousTimestamp = null;
		
		// these entries are deliberately out of time order
		entries.add(new diary.model.NoteEntry({'timestamp' : new Date(now + 1000) , 'text' : "Hello world"}));
		entries.add(new diary.model.NoteEntry({'timestamp' : new Date(now - 1000) , 'text' : "Hello world"}));
		entries.add(new diary.model.NoteEntry({'timestamp' : new Date(now) , 'text' : "Hello world"}));
		
		expect(note.get("entries")).toEqual(entries);
		expect(note.get("entries").length).toEqual(3);
		
		// Verify ordered correctly
		_.forEach(entries.toArray(), function (noteEntry) {
			// Ensure correct ordering
			if (previousTimestamp != null) {
				if (previousTimestamp.getTime() > noteEntry.get('timestamp').getTime()) {
					expect(false).toBeTruthy("Note entries must be ordered by timestamp");
				}
			}
			
			// Loop
			previousTimestamp = noteEntry.get('timestamp');
		});
		
		// Post-checks
		expect(previousTimestamp).not.toBeNull();
		
	});
});