/**
 * Unit Tests for the Period Model
 * 
 * author - John Pearce
 */
describe("Period - Test Suite", function() {
	
	/**
	 * Suite of tests which should create a timetable
	 */
	describe("Successful creation of Period Suite", function() {
		
		beforeEach(function() {
			
		});
	
		/*
		 * creation of a timetable with default values
		 */
		it("should be able to create a period with default values", function() {
		   
			// create the test timetable that we will use for the test
			var period = new diary.model.Period();
			
			expect(period.get("startTime")).toBeNull();
			expect(period.get("endTime")).toBeNull();
			expect(period.get("title")).toBeNull();
			expect(period.get("cycleDay")).toBeNull();					
			
		});
		
		/*
		 * creation of a timetable with given values
		 */
		it("should be able to create a period with given values", function() {
		   
			var startTime = new Rocketboots.date.Time(0,12),
				endTime = new Rocketboots.date.Time(0,13), // 1 min after start time
				title = "Hello world",
				cycleDay = 1;
			
			// create the test timetable that we will use for the test
			var period = new diary.model.Period({ "startTime" : startTime,
												  "endTime" : endTime,
												  "title" : title,
												  "cycleDay" : cycleDay });
			
			expect(period.get("startTime")).toEqual(startTime);
			expect(period.get("endTime")).toEqual(endTime);
			expect(period.get("title")).toEqual(title);
			expect(period.get("cycleDay")).toEqual(cycleDay);
			
		});
			
	});
	
	/**
	 * Suite of tests which should stop the period from being created
	 */
	describe("Unsuccessful creation of Period Suite" , function() {
		
		// holds the attributes that are to be used for each test
		var attrs;
		// holds the timetable that is to be used for testing
		var period = null;
		
		beforeEach(function() {
			// holds the attributes that are to be assigned during save
			attrs = {startTime : new Rocketboots.date.Time(0,12),
                     endTime : new Rocketboots.date.Time(0,13), // 1 m after start time
					 title     : "Hello world",
					 cycleDay  : 1 };
			
			// create the test diary item that we will use for the test
			period = new diary.model.Period();
			period.url = "test";
		});
		
		/*
		 * period start time not specified
		 */
		it("should fail when period start time is not specified", function() {

			// update the attributes for the test
			attrs.startTime = undefined;
			
			expect(period.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("No start time specified");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * end time before start time
		 */
		it("should fail when period end time before start time", function() {

			// update the attributes for the test
			attrs.endTime = new Rocketboots.date.Time(0,11);
			
			expect(period.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("End time must be after start time");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * cycle length is negative
		 */
		it("should fail when period cycle day negative", function() {

			// update the attributes for the test
			attrs.cycleDay = -1;
			
			expect(period.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Cycle day must not be negative");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
	});
		
});