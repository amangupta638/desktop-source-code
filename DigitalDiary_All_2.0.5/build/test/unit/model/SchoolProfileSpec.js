/**
 * Unit Tests for the School Profile Model
 * 
 * author - Brian Bason
 */
describe("School Profile - Test Suite", function() {
	
	/**
	 * Suite of tests which should create a School Profile
	 */
	describe("Successful creation of School Profile Suite" , function() {
		
		beforeEach(function() {
			
		});
	
		/*
		 * creation of a school profile with default values
		 */
		it("should be able to create a school profile with default values", function() {
		   
			// create the test school profile that we will use for the test
			var schoolProfile = new diary.model.SchoolProfile();
			
			expect(schoolProfile.get("name")).toBeNull();					
			expect(schoolProfile.get("schoolLogo")).toBeNull();					
			expect(schoolProfile.get("schoolImage")).toBeNull();
			expect(schoolProfile.get("schoolMotto")).toBeNull();					
			expect(schoolProfile.get("missionStatement")).toBeNull();					
			expect(schoolProfile.get("diaryTitle")).toBeNull();
			expect(schoolProfile.get("includeEmpower")).toBeFalsy();
			expect(schoolProfile.get("contentMapping") instanceof diary.model.ContentMapping).toBeTruthy();
			
			
		});
		
		/*
		 * creation of a school profile with given values
		 */
		it("should be able to create a school profile with given values", function() {
		   
			// holds the name of the school
			var name = "Test School";
			// holds the school logo
			var schoolLogo = "file://var/lib/images/logo.gif";
			// holds the school image
			var schoolImage = "file://var/lib/images/image.gif";
			// holds the school motto
			var schoolMotto = "This is the motto for the school - buh yah!!!";
			// holds the mission statement
			var missionStatement = "This is the mission statement for the school - yehh!!!";
			// holds the diary title
			var diaryTitle = "Diary Title 2012";
			
			// create the test school profile that we will use for the test
			var schoolProfile = new diary.model.SchoolProfile({ 'name' : name,
															  'schoolLogo' : schoolLogo,
															  'schoolImage' : schoolImage,
															  'schoolMotto' : schoolMotto,
															  'missionStatement' : missionStatement,
															  'diaryTitle' : diaryTitle,
															  'includeEmpower' : true });
			
			expect(schoolProfile.get("name")).toEqual(name);					
			expect(schoolProfile.get("schoolLogo")).toEqual(schoolLogo);					
			expect(schoolProfile.get("schoolImage")).toEqual(schoolImage);
			expect(schoolProfile.get("schoolMotto")).toEqual(schoolMotto);					
			expect(schoolProfile.get("missionStatement")).toEqual(missionStatement);					
			expect(schoolProfile.get("diaryTitle")).toEqual(diaryTitle);
			expect(schoolProfile.get("includeEmpower")).toBeTruthy();
			expect(schoolProfile.get("contentMapping") instanceof diary.model.ContentMapping).toBeTruthy();
			
		});
		
		/*
		 * creation of a school profile with given values
		 */
		it("should be able to create a school profile with partial attributes", function() {
		   
			// holds the name of the school
			var name = "Test School";
			// holds the school logo
			var schoolLogo = "file://var/lib/images/logo.gif";
			// holds the school image
			var schoolImage = "file://var/lib/images/image.gif";
			// holds the school motto
			var schoolMotto = "This is the motto for the school - buh yah!!!";
			// holds the mission statement
			var missionStatement = "This is the mission statement for the school - yehh!!!";
			// holds the diary title
			var diaryTitle = "Diary Title 2012";
			
			// create the test school profile that we will use for the test
			var schoolProfile = new diary.model.SchoolProfile({ 'name' : name,
															  'schoolLogo' : schoolLogo,
															  'schoolImage' : schoolImage,
															  'schoolMotto' : schoolMotto,
															  'missionStatement' : missionStatement,
															  'diaryTitle' : diaryTitle });
			
			expect(schoolProfile.get("name")).toEqual(name);					
			expect(schoolProfile.get("schoolLogo")).toEqual(schoolLogo);					
			expect(schoolProfile.get("schoolImage")).toEqual(schoolImage);
			expect(schoolProfile.get("schoolMotto")).toEqual(schoolMotto);					
			expect(schoolProfile.get("missionStatement")).toEqual(missionStatement);					
			expect(schoolProfile.get("diaryTitle")).toEqual(diaryTitle);
			expect(schoolProfile.get("includeEmpower")).toBeFalsy();
			expect(schoolProfile.get("contentMapping") instanceof diary.model.ContentMapping).toBeTruthy();
			
		});
			
	});
		
});