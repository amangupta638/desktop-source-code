/**
 * Unit Tests for the Attachment Model
 * 
 * author - John Pearce
 */
describe("Attachment - Test Suite", function() {
	
	/**
	 * Suite of tests which should create
	 */
	describe("Successful creation of Attachment Suite", function() {
		
		/*
		 * creation with default values
		 */
		it("should be able to create an attachment with default values", function() {
		   
			// create the attachment that we will use for the test
			var attachment = new diary.model.Attachment();
			
			expect(attachment.get('originalFilePath')).toBeNull();
			expect(attachment.get('displayName')).toBeNull();
			expect(attachment.get('fileType')).toBeNull();
			expect(attachment.get('localFileName')).toBeNull();
			expect(attachment.get('localFilePath')).toBeNull();
			expect(attachment.get('dateAttached')).toBeNull();
			
		});
		
		/*
		 * creation with given values
		 */
		it("should be able to create an attachment with given values", function() {
		   
			var attrs = {
				'originalFilePath': "C:/test.txt",
				'displayName' : "test.txt",
				'fileType' : "plain text",
				'localFileName' : "test1.txt",
				'localFilePath' : "C:/app/test1.txt",
				'dateAttached' : new Date()
			};
			
			// create the attachment that we will use for the test
			var attachment = new diary.model.Attachment(attrs);
			
			_.forEach(attrs, function (value, key) {
				expect(attachment.get(key)).toEqual(value);
			});
			
			expect(attachment.isValid()).toBeTruthy();
			
		});
		
	});
	
});