/**
 * Unit Tests for the Application State Model
 * 
 * author - John Pearce
 */
describe("Application State - Test Suite", function() {
	
	/**
	 * Suite of tests which should create an app state object
	 */
	describe("Successful creation of an Application State object", function() {
		
		beforeEach(function() {
			
		});
	
		/*
		 * creation with default values
		 */
		it("should be able to create an app state with default values", function() {
		   
			// create the test diary item that we will use for the test
			var appState = new diary.model.ApplicationState();
			
			expect(appState.get("tableOfContentsVisible")).toBeFalsy();
			expect(appState.get("selectedDay")).not.toBeNull();
			
		});
		
		/*
		 * creation with given values
		 */
		it("should be able to create an app state with given values", function() {
			
			// create the test diary item that we will use for the test
			var day = new Rocketboots.date.Day(),
				appState = new diary.model.ApplicationState({
					"tableOfContentsVisible" : true,
					"selectedDay" : day
				});
			
			expect(appState.get("tableOfContentsVisible")).toBeTruthy();
			expect(appState.get("selectedDay")).toEqual(day);
			
		});
		
		/*
		 * creation with given values, again
		 */
		it("should be able to create an app state with given values again", function() {
			
			// create the test diary item that we will use for the test
			var day = new Rocketboots.date.Day(2011, 4, 22),
				appState = new diary.model.ApplicationState({ 
					"tableOfContentsVisible" : false,
					"selectedDay" : day
				});
			
			expect(appState.get("tableOfContentsVisible")).toBeFalsy();
			expect(appState.get("selectedDay")).toEqual(day);
			
		});
		
		/*
		 * Test the 'setSelectedDay' method 
		 */
		it("set selected day", function () {
			
			var appState = new diary.model.ApplicationState({
					"selectedDay" : new Rocketboots.date.Day(2012, 5, 20)
				});
			
			expect(appState.get('selectedDay').getDate().getDate()).toEqual(20);
			
			appState.setSelectedDay(new Date(2012, 5, 10));
			
			expect(appState.get('selectedDay').getDate().getDate()).toEqual(10);
			
			appState.setSelectedDay(new Rocketboots.date.Day(2012, 5, 5));
			
			expect(appState.get('selectedDay').getDate().getDate()).toEqual(5);
			
		});
			
	});
	
	/**
	 * Suite of tests which should stop the app state object from being created
	 */
	describe("Unsuccessful creation of an application state object" , function() {
		
		// holds the attributes that are to be used for each test
		var attrs;
		// holds the object that is to be used for testing
		var appState = null;
		
		beforeEach(function() {
			// holds the attributes that are to be assigned during save
			attrs = { "tableOfContentsVisible" : true };
			
			// create the test diary item that we will use for the test
			appState = new diary.model.ApplicationState();
			appState.url = "test";
		});
	
		/*
		 * TOC not specified
		 */
		it("should fail when 'table of contents visible' is not specified", function() {

			// update the attributes for the test
			attrs.tableOfContentsVisible = undefined;
			
			expect(appState.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("'Table of contents visible' must be specified, cannot be NULL");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * TOC type
		 */
		it("should fail when 'table of contents visible' is not a boolean", function() {

			// update the attributes for the test
			attrs.tableOfContentsVisible = "30-Mar-1909";
			
			expect(appState.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("'Table of contents visible' must be of type boolean");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * day not specified
		 */
		it("should fail when 'selected day' is not specified", function() {

			// update the attributes for the test
			attrs.selectedDay = undefined;
			
			expect(appState.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("'Selected day' must be specified, cannot be NULL");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		/*
		 * day type check
		 */
		it("should fail when 'selected day' is not a Day", function() {

			// update the attributes for the test
			attrs.selectedDay = "30-Mar-1909";
			
			expect(appState.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("'Selected day' must be of type Day");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
	});
		
});