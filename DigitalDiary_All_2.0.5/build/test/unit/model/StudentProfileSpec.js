/**
 * Unit Tests for the Student Profile Model
 * 
 * author - Brian Bason
 */
describe("Student Profile - Test Suite", function() {
	
	/**
	 * Suite of tests which should create a Student Profile
	 */
	describe("Successful creation of Student Profile Suite" , function() {
		
		beforeEach(function() {
			
		});
	
		/*
		 * creation of a student profile with default values
		 */
		it("should be able to create a student profile with default values", function() {
		   
			// create the test student profile that we will use for the test
			var studentProfile = new diary.model.StudentProfile();
			
			expect(studentProfile.get("name")).toBeNull();
			expect(studentProfile.get("dateOfBirth")).toBeNull();
			expect(studentProfile.get("address") instanceof diary.model.Address).toBeTruthy();
			expect(studentProfile.get("telephone")).toBeNull();
			expect(studentProfile.get("email")).toBeFalsy();
			expect(studentProfile.get("grade")).toBeNull();
			expect(studentProfile.get("teacher")).toBeNull();
			expect(studentProfile.get("house")).toBeNull();
			expect(studentProfile.get("houseCaptain")).toBeNull();
			expect(studentProfile.get("houseColours")).toBeNull();
			expect(studentProfile.get("age")).toBeNull();
			
		});
		
		/*
		 * creation of a student profile with given values
		 */
		it("should be able to create a student profile with given values", function() {
		   
			// holds the name of the student
			var name = "Test Student";
			// holds the date of birth of the student
			var dateOfBirth = new Date();
			dateOfBirth.setFullYear(dateOfBirth.getFullYear() - 16);
			// holds the address of the student
			var address = new diary.model.Address({ street : "2/92, The Sample Street",
					        					  	suburb : "North Sydney",
					        					  	state: "NSW",
					        					  	postcode: "2060"});
			
			// holds the telephone of the student
			var telephone = "(02) 4325 6789";
			// holds the student email
			var email = "student@school.com";
			
			// create the test student profile that we will use for the test
			var studentProfile = new diary.model.StudentProfile({	"name" : name,
																"dateOfBirth" : dateOfBirth,
																"address" : address,
																"telephone" : telephone,
																"email" : email });
			
			expect(studentProfile.get("name")).toEqual(name);					
			expect(studentProfile.get("dateOfBirth")).toEqual(dateOfBirth);					
			expect(studentProfile.get("address")).toEqual(address);				
			expect(studentProfile.get("telephone")).toEqual(telephone);					
			expect(studentProfile.get("email")).toEqual(email);					
			expect(studentProfile.get("grade")).toBeNull();					
			expect(studentProfile.get("teacher")).toBeNull();
			expect(studentProfile.get("house")).toBeNull();				
			expect(studentProfile.get("houseCaptain")).toBeNull();
			expect(studentProfile.get("houseColours")).toBeNull();
			expect(studentProfile.get("age")).toEqual(16);	
			
		});
			
	});
	
	describe("Unsuccessful validation of Student Profile Suite" , function() {
		
		// holds the studentProfile that is to be used for testing
		var studentProfile,
			attrs;
		
		beforeEach(function() {
			
			// holds the date of birth of the student
			var currentDate = new Date();
            currentDate.setFullYear(currentDate.getFullYear - 16);
            var dateOfBirth = new Rocketboots.date.Day(currentDate);
			// holds the address of the student
			var address = new diary.model.Address({ street : "2/92, The Sample Street",
			  										suburb : "North Sydney",
			  										state: "NSW",
			  										postcode: "2060"});
			
			// holds the attributes that are to be assigned during save
			attrs = {"name" : "Test Student",
					 "dateOfBirth" : dateOfBirth,
					 "address" : address,
					 "telephone" : "(02) 4325 6789",
					 "email" : "student@school.com"};
			
			// create the test student profile that we will use for the test
			studentProfile = new diary.model.StudentProfile();
			studentProfile.url = "test";
		});
		
		it("should fail when name is not string", function() {

			// update the attributes for the test
			attrs.name = 123;
			
			expect(studentProfile.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("name must be of type string");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		it("should fail when name is too long", function() {

			// update the attributes for the test
			attrs.name = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901";
			
			expect(studentProfile.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("name is invalid, attribute must not be more than 100 characters long");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		it("should fail when addres is not diary.model.Address", function() {

			// update the attributes for the test
			attrs.address = "the ansdflkjh asdpoiu";
			
			expect(studentProfile.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Address must be of type diary.model.Address");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
		it("should fail when date of birth is not Rocketboots.date.Day", function() {

			// update the attributes for the test
			attrs.dateOfBirth = 1234;
			
			expect(studentProfile.save(attrs, 
					 {error : function(model, response) {
						 expect(response).toEqual("Date of birth must be of type Rocketboots.date.Day");
					 }})).toBeFalsy("Should have failed validation");
			
		});
		
	});
		
});