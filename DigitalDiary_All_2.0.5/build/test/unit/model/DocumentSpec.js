/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: DocumentSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Document Test Suite", function() {

	// --------------------------------------------------------------------------
	//
	// Test Cases / Specs
	//
	// --------------------------------------------------------------------------

	// ----------------------------------
	// Document() - success
	// ----------------------------------

	describe("Document() - Success cases", function() {

		// defaults
		it("should succeed with defaults", function() {

			// init document
			var doc = new diary.model.Document();

			// no exceptions / errors

			// assert attribute defaults
			expect(doc instanceof diary.model.Document).toBeTruthy();
			expect(doc.get("type")).toBeNull();
			expect(doc.get("title")).toBeNull();
			expect(doc.get("documentId")).toBeNull();

		});

		// valid attributes
		it("should succeed with valid attributes", function() {

			// init document
			var doc = new diary.model.Document();

			// listen for error events
			var eventSpy = jasmine.createSpy("EventSpy");
			doc.on("error", eventSpy);
			eventSpy.reset();

			// set attributes
			var attrs = {
				"type" : diary.model.Document.Types.SCHOOL,
				"title" : "Test Title",
				"documentId" : "thisisatestdocumentid"
			};
			doc.set(attrs);

			// no exceptions / errors
			expect(eventSpy.callCount).toEqual(0);

			// assert expected attributes
			expect(doc instanceof diary.model.Document).toBeTruthy();
			expect(doc.get("type")).toEqual(attrs.type);
			expect(doc.get("title")).toEqual(attrs.title);
			expect(doc.get("documentId")).toEqual(attrs.documentId);

		});

		// valid types
		it("should succeed for each valid type", function() {

			// init document
			var doc = new diary.model.Document();

			// listen for error events
			var eventSpy = jasmine.createSpy("EventSpy");
			doc.on("error", eventSpy);
			eventSpy.reset();

			// set with each type
			var key, type;
			for (key in diary.model.Document.Types) {
				type = diary.model.Document.Types[key];

				// set attributes
				var attrs = {
						"type" : type,
						"title" : "Test Title",
						"documentId" : "thisisatestdocumentid"
				};
				doc.set(attrs);

				// no exceptions / errors
				expect(eventSpy.callCount).toEqual(0);

				// assert expected attributes
				expect(doc instanceof diary.model.Document).toBeTruthy();
				expect(doc.get("type")).toEqual(attrs.type);
				expect(doc.get("title")).toEqual(attrs.title);
				expect(doc.get("documentId")).toEqual(attrs.documentId);
			}

		});

	});

	// ----------------------------------
	// Document() - failure
	// ----------------------------------

	describe("Document() - Failure cases", function() {

		// no type attribute
		it("should fail without a 'type' attribute", function() {

			// init document
			var doc = new diary.model.Document();

			// listen for error events
			var eventSpy = jasmine.createSpy("EventSpy");
			doc.on("error", eventSpy);
			eventSpy.reset();

			// set attributes
			var attrs = {
				"type" : null,
				"title" : "Test Title",
				"documentId" : "thisisatestdocumentid"
			};
			doc.set(attrs);

			// no exceptions / errors
			expect(eventSpy.callCount).toEqual(1);
			expect(eventSpy.mostRecentCall.args[1]).toEqual("No Type specified");

		});

		// no title attribute
		it("should fail without a 'title' attribute", function() {

			// init document
			var doc = new diary.model.Document();

			// listen for error events
			var eventSpy = jasmine.createSpy("EventSpy");
			doc.on("error", eventSpy);
			eventSpy.reset();

			// set attributes
			var attrs = {
				"type" : diary.model.Document.Types.SCHOOL,
				"title" : null,
				"documentId" : "thisisatestdocumentid"
			};
			doc.set(attrs);

			// no exceptions / errors
			expect(eventSpy.callCount).toEqual(1);
			expect(eventSpy.mostRecentCall.args[1]).toEqual("No Title specified");
			
		});

		// no documentId attribute
		it("should fail without a 'documentId' attribute", function() {

			// init document
			var doc = new diary.model.Document();

			// listen for error events
			var eventSpy = jasmine.createSpy("EventSpy");
			doc.on("error", eventSpy);
			eventSpy.reset();

			// set attributes
			var attrs = {
				"type" : diary.model.Document.Types.SCHOOL,
				"title" : "Test Title",
				"documentId" : null
			};
			doc.set(attrs);

			// no exceptions / errors
			expect(eventSpy.callCount).toEqual(1);
			expect(eventSpy.mostRecentCall.args[1]).toEqual(
					"No Document Identifier specified");

		});

		// type is not a valid type
		it("should fail when the 'type' argument is not a valid type",
				function() {

			// init document
			var doc = new diary.model.Document();

			// listen for error events
			var eventSpy = jasmine.createSpy("EventSpy");
			doc.on("error", eventSpy);
			eventSpy.reset();

			// set attributes
			var attrs = {
				"type" : "YOURMOM",
				"title" : "Test Title",
				"documentId" : "thisisatestdocumentid"
			};
			doc.set(attrs);

			// no exceptions / errors
			expect(eventSpy.callCount).toEqual(1);
			expect(eventSpy.mostRecentCall.args[1]).toEqual("Type is invalid");

		});

		// title not a string
		it("should fail when the 'title' argument is not of type 'String'",
				function() {

			// init document
			var doc = new diary.model.Document();

			// listen for error events
			var eventSpy = jasmine.createSpy("EventSpy");
			doc.on("error", eventSpy);
			eventSpy.reset();

			// set attributes
			var attrs = {
				"type" : diary.model.Document.Types.SCHOOL,
				"title" : 0,
				"documentId" : "thisisatestdocumentid"
			};
			doc.set(attrs);

			// no exceptions / errors
			expect(eventSpy.callCount).toEqual(1);
			expect(eventSpy.mostRecentCall.args[1]).toEqual(
					"Title must be of type string");

		});

		// documentId not a string
		it("should fail when the 'documentId' argument is not of type 'String'",
				function() {

			// init document
			var doc = new diary.model.Document();

			// listen for error events
			var eventSpy = jasmine.createSpy("EventSpy");
			doc.on("error", eventSpy);
			eventSpy.reset();

			// set attributes
			var attrs = {
				"type" : diary.model.Document.Types.SCHOOL,
				"title" : "Test Title",
				"documentId" : 0
			};
			doc.set(attrs);

			// no exceptions / errors
			expect(eventSpy.callCount).toEqual(1);
			expect(eventSpy.mostRecentCall.args[1]).toEqual(
					"Document Identifier must be of type string");

		});

	});

});