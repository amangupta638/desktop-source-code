/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: DiaryItemSQLDelegateSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Diary Item SQL Delegate", function() {

	// holds the dummy app database layer that is required for testing
	var dummyApplicationDatabase = (function() {

		var service = function() {};

		service.prototype.runQuery = function(text, parameters, successCallback, errorCallback) {

			// return the dummy SQL Statement
			successCallback(new air.SQLStatement());

		};

		service.prototype.runScripts = function(script, successCallback, errorCallback) {

			// return the dummy response
			successCallback();

		};

		return service;

	})();

	beforeEach(function() {
		// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
		air = new Object();

		air.SQLStatement = function() {
		};

		air.SQLStatement.prototype.execute = function() {

		};

		// configure the application context for the unit test
		app = new Rocketboots.Application({
	
           mapEvents: function() {
               // map events needed for the test
           },

           mapServices: function() {
        	   // map the services needed for the test
        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
           },

           registerTemplates: {
               // register templates, if needed
           }
	
	    });

		// init the application context
		app.initialize();

	});

	describe("Get a number of diary items for a date range" , function() {

		var today = new Rocketboots.date.Day();
		var pastDay = new Rocketboots.date.Day().addDays(-2);
		var futureDay = new Rocketboots.date.Day().addDays(2);

		// setup the env for the the unit test
		beforeEach(function() {

			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				// return the dummy data for the diary item
				return {'data' : [
					// Task
					{
						'id' : 1,
						'type' : "homework",
						'title' : "Test Task",
						'description' : null,
						'created' : new Date(),
						'assignedDay' : pastDay.getKey(),
						'assignedTime' : 1004,
						'dueDay' : futureDay.getKey(),
						'dueTime' : 1230,
						'completed' : null,
						'subjectId': 2,
						'startDay' : null,
						'startTime' : null,
						'endTime' : null,
						'locked' : null,
						'messageFrom' : null,
						'messageFromText' : null,
						'responseRequired' : null
					},

					// Event
					{
						'id' : 2,
						'type' : "event",
						'title' : "Test Event",
						'description' : null,
						'created' : new Date(),
						'assignedDay' : null,
						'assignedTime' : null,
						'dueDay' : null,
						'dueTime' : null,
						'completed' : null,
						'subjectId': null,
						'startDay' : today.getKey(),
						'startTime' : null,
						'endTime' : null,
						'locked' : false,
						'messageFrom' : null,
						'messageFromText' : null,
						'responseRequired' : null
					},

					// Note
					{
						'id' : 3,
						'type' : "note",
						'title' : "Test Note",
						'description' : null,
						'created' : new Date(),
						'assignedDay' : null,
						'assignedTime' : null,
						'dueDay' : null,
						'dueTime' : null,
						'completed' : null,
						'subjectId': null,
						'startDay' : pastDay.getKey(),
						'startTime' : null,
						'endTime' : null,
						'locked' : null,
						'messageFrom' : null,
						'messageFromText' : null,
						'responseRequired' : null
					},

					// Message
					{
						'id' : 4,
						'type' : "message",
						'title' : "Test Message",
						'description' : "This is the message.",
						'created' : new Date(),
						'assignedDay' : null,
						'assignedTime' : null,
						'dueDay' : null,
						'dueTime' : null,
						'completed' : null,
						'subjectId': null,
						'startDay' : futureDay.getKey(),
						'startTime' : null,
						'endTime' : null,
						'locked' : null,
						'messageFrom' : diary.model.Message.FROM.TEACHER,
						'messageFromText' : "Mrs. Jones",
						'responseRequired' : true
					}
				]};

			};

		});

		it("should be able to return all diary items for a date range", function() {
		
			// holds the diary item delegate that is being tested
			var delegate = new diary.service.DiaryItemSQLDelegate();

			// the success function
			function success(diaryItems) {
				expect(diaryItems instanceof diary.collection.DiaryItemCollection).toBeTruthy();
				expect(diaryItems.size()).toBe(4);

				var diaryItem1 = diaryItems.get(1);
				expect(diaryItem1 instanceof diary.model.Task).toBeTruthy();
				expect(diaryItem1.id).toEqual(1);
				expect(diaryItem1.get("type")).toEqual(diary.model.DiaryItem.TYPE.HOMEWORK);
				expect(diaryItem1.get("title")).toEqual("Test Task");
				expect(diaryItem1.get("assignedDay").getKey()).toEqual(pastDay.getKey());
                expect(diaryItem1.get("assignedTime").toInteger()).toEqual(1004);
                expect(diaryItem1.get("dueDay").getKey()).toEqual(futureDay.getKey());
                expect(diaryItem1.get("dueTime").toInteger()).toEqual(1230);
                expect(diaryItem1.get("completed")).toBeNull();
                expect(diaryItem1.get("subject").get("id")).toEqual(2);
                expect(diaryItem1.get("subject").get("title")).toEqual("U1 Math");
                expect(diaryItem1.get("subject").get("colour")).toEqual("Red");

				var diaryItem2 = diaryItems.get(2);
				expect(diaryItem2 instanceof diary.model.Event).toBeTruthy();
				expect(diaryItem2.id).toEqual(2);
				expect(diaryItem2.get("title")).toEqual("Test Event");
				expect(diaryItem2.get("startDay").getKey()).toEqual(today.getKey());
				expect(diaryItem2.get("startTime")).toBeNull();
				expect(diaryItem2.get("endTime")).toBeNull();
				expect(diaryItem2.get("locked")).toBeFalsy();

				var diaryItem3 = diaryItems.get(3);
				expect(diaryItem3 instanceof diary.model.Note).toBeTruthy();
				expect(diaryItem3.id).toEqual(3);
				expect(diaryItem3.get("title")).toEqual("Test Note");
				expect(diaryItem3.get("atDay").getKey()).toEqual(pastDay.getKey());
				expect(diaryItem3.get("atTime")).toBeNull();

				var diaryItem4 = diaryItems.get(4);
				expect(diaryItem4 instanceof diary.model.Message).toBeTruthy();
				expect(diaryItem4.id).toEqual(4);
				expect(diaryItem4.get("title")).toEqual("Test Message");
				expect(diaryItem4.get("description")).toEqual("This is the message.");
				expect(diaryItem4.get("atDay").getKey()).toEqual(futureDay.getKey());
				expect(diaryItem4.get("atTime")).toBeNull();
				expect(diaryItem4.get("messageFrom")).toEqual(diary.model.Message.FROM.TEACHER);
				expect(diaryItem4.get("messageFromText")).toEqual("Mrs. Jones");
				expect(diaryItem4.get("responseRequired")).toBeTruthy();
			}

			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// create a dummy collection of subjects
			var subjects = new diary.collection.SubjectCollection();
			subjects.add(new diary.model.Subject({ 'id' : 2,
												   'title' : "U1 Math",
												   'colour' : "Red" }));

			// get the diary items from the delegate
			delegate.getDiaryItemsForDayRange(new Date(), new Date(), subjects, success, error);

		});

	});

	describe("Get an empty list of diary items" , function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {

				// return the dummy data for the task
				return {'data' : null };

			};

		});

		it("should be able to return an empty collection of diary items", function() {
		
			// holds the delegate that is being tested
			var delegate = new diary.service.DiaryItemSQLDelegate();

			// the success function
			function success(diaryItems) {
				// the delegate should have returned a collection of diary items
				expect(diaryItems instanceof diary.collection.DiaryItemCollection).toBeTruthy();

				// the delegate should have returned 0 diary items from the dummy application layer
				expect(diaryItems.size()).toBe(0);
			}

			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// create a dummy collection of subjects
			var subjects = new diary.collection.SubjectCollection();
			subjects.add(new diary.model.Subject({ 'id' : 2,
												   'title' : "U1 Math",
												   'colour' : "Red" }));

			// get the diary items from the delegate
			delegate.getDiaryItemsForDayRange(new Date(), new Date(), subjects, success, error);

		});

	});

	describe("Delete a diary item" , function() {

		var successCall = false;

		beforeEach(function() {

		});

		it("should be able to delete a diary item", function() {

			// holds the delegate that is being tested
			var delegate = new diary.service.DiaryItemSQLDelegate();

			// the success function
			function success() {
				// this is the success call
				successCall = true;
			}

			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// delete the diary item
			delegate.deleteDiaryItem(1, success, error);
			expect(successCall).toBeTruthy();

		});

	});

	describe("Create a diary item" , function() {

		var successCall = false;

		beforeEach(function() {
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {

				// return the dummy data for the task
				return {'lastInsertRowID' : 1 };

			};
		});

		it("should be able to create a diary item", function() {

			// holds the delegate that is being tested
			var delegate = new diary.service.DiaryItemSQLDelegate();

			// the success function
			function success(diaryItemId) {
				// this is the success call
				if (diaryItemId == 1) {
					successCall = true;
				}
			}

			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// create a diary item
			var diaryItem = new diary.model.Event({	"date" : new Rocketboots.date.Day().addDays(1).getDate(),
													"startTime" : null,
													"endTime" : null,
													"period" : 1,
													"title" : 'title',
													"locked" : false,
													"description" : 'description' });

			delegate.createDiaryItem(diaryItem, success, error);
			expect(successCall).toBeTruthy();

		});

	});

	describe("Invalid function parameters" , function() {

		// setup the env for the the unit test
		beforeEach(function() {

			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {

				// return the dummy data for the task
				return {'data' : null };

			};

		});

		it("should fail if start date is not specified for get Diary Items", function() {
		
			// holds the delegate that is being tested
			var delegate = new diary.service.DiaryItemSQLDelegate();

			// the success function
			function success(diaryItems) {
				throw "Test failed - operation should not have been successful";
			}

			// the error function
			function error(message) {
				throw "Test failed - operation should not have failed with error";
			}

			// the test function
			var testFunction = function() {
				// get the diary items from the delegate
				delegate.getDiaryItemsForDayRange(null, new Date(), success, error);
			};

			// execute the test
			expect(testFunction).toThrow("From date must be defined, date is not optional");

		});

		it("should fail if end date is not specified for get Diary Items", function() {
			
			// holds the delegate that is being tested
			var delegate = new diary.service.DiaryItemSQLDelegate();

			// the success function
			function success(diaryItems) {
				throw "Test failed - operation should not have been successful";
			}

			// the error function
			function error(message) {
				throw "Test failed - operation should not have failed with error";
			}

			// the test function
			var testFunction = function() {
				// get the diary items from the delegate
				delegate.getDiaryItemsForDayRange(new Date(), null, success, error);
			};

			// execute the test
			expect(testFunction).toThrow("To date must be defined, date is not optional");

		});

		it("should fail if end date is smaller than start date for get Diary Items", function() {
			
			// holds the delegate that is being tested
			var delegate = new diary.service.DiaryItemSQLDelegate();

			// the success function
			function success(diaryItems) {
				throw "Test failed - operation should not have been successful";
			}

			// the error function
			function error(message) {
				throw "Test failed - operation should not have failed with error";
			}

			// the test function
			var testFunction = function() {

				// set the end date before the start date
				var startDate = new Date();
				var endDate = new Date(startDate.getTime() - 1);

				// get the diary items from the delegate
				delegate.getDiaryItemsForDayRange(startDate, endDate, success, error);
			};

			// execute the test
			expect(testFunction).toThrow("To date is not valid, date must be after from date");

		});

		it("should fail diary item id is not defined for delete diary item", function() {
			// holds the task delegate that is being tested
			var delegate = new diary.service.DiaryItemSQLDelegate();

			expect(function() { delegate.deleteDiaryItem(); }).toThrow("Diary Item identifier must be defined, identifier is not optional");

		});

	});

});
