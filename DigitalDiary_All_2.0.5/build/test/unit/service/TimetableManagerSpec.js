/**
 * Unit Tests for the Timetable Manager
 * 
 * author - John Pearce
 */
describe("Timetable Manager - Test Suite", function() {
	
	describe("Creation of a subject" , function() {
		
		// holds the task delegate that is being used before testing
		var dummySubjectDelegate = (function() {
			
			return function() {
				
				this.subjectExistsWithTitle = function(title, successCallback, errorCallback) {
					successCallback(null);
				};
				
				this.createSubject = function(subject, successCallback, errorCallback) {
					successCallback(1);
				};
				
				this.updateSubject = function(subject, successCallback, errorCallback) {
					successCallback();
				};
			};
		
		})();
		
		// holds the task delegate that is being used before testing
		var dummyTimetableDelegate = (function() {
			
			return function() {
				
			};
				
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.TIMETABLEDELEGATE, dummyTimetableDelegate);
	        	   this.mapService(ServiceName.SUBJECTDELEGATE, dummySubjectDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should be able to create a subject which doesn't exist", function() {
		   
			// holds the task manager
			var manager = new diary.service.TimetableManager(),
				successCalled = false;
			
			// the success function
			function success(subject) {
				// the manager should have returned a subject
				expect(subject instanceof diary.model.Subject).toBeTruthy();
				expect(subject.get("id")).toEqual(1);
				expect(subject.get("title")).toEqual("Test Subject");
				expect(subject.get("colour")).toEqual("Red");
				successCalled = true;
			}
			
			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			var subject = new diary.model.Subject({ 'title' : "Test Subject",
												    'colour' : "Red" });
			
			// create a subject if unique
			manager.createSubject(subject, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
		it("should return subject back when trying to create unique subject", function() {
			
			var oldSubject = new diary.model.Subject({ 'id' : 2, 
				  'title' : "Test Subject",
				  'colour' : "Blue" });
			
			// inject the test function in the delegate
			app.locator.getService(ServiceName.SUBJECTDELEGATE).subjectExistsWithTitle = function(title, successCallback, errorCallback) {
				successCallback(oldSubject);
			};
			
			// holds the manager that is being tested
			var manager = new diary.service.TimetableManager(),
				successCalled = false;
			
			// the success function
			function success() {
				// the manager should have returned a subject
				expect(oldSubject instanceof diary.model.Subject).toBeTruthy();
				expect(oldSubject.get("id")).toEqual(2);
				expect(oldSubject.get("title")).toEqual("Test Subject");
				expect(oldSubject.get("colour")).toEqual("Blue");
				successCalled = true;
			}
			
			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			var newSubject = new diary.model.Subject({ 'title' : "Test Subject",
												    'colour' : "Red" });
			
			// create a subject if unique
			manager.createSubject(oldSubject, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
	});
	
	describe("Deletion of a subject" , function() {
		
		// holds the task delegate that is being used before testing
		var dummySubjectDelegate = (function() {
			
			return function() {
				
				this.isSubjectInUse = function(subjectId, successCallback, errorCallback) {
					successCallback(false);
				};
				
				this.deleteSubject = function(subjectId, successCallback, errorCallback) {
					successCallback(1);
				};
			};
		
		})();
		
		// holds the timetable delegate that is being used before testing
		var dummyTimetableDelegate = (function() {
			
			return function() {
				
			};
				
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.TIMETABLEDELEGATE, dummyTimetableDelegate);
	        	   this.mapService(ServiceName.SUBJECTDELEGATE, dummySubjectDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should be able to delete a subject which is not in use", function() {
		   
			// holds the task manager
			var manager = new diary.service.TimetableManager(),
				successCalled = false;
			
			// the success function
			function success(wasDeleted) {
				expect(wasDeleted).toBeTruthy();
				successCalled = true;
			}
			
			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			var subject = new diary.model.Subject({ 'id' : 1,
													'title' : "Test Subject",
													'colour' : "Red" });
			
			// delete a subject which is not used
			manager.deleteSubject(subject, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
		it("should not delete subject since it is in use", function() {
			
			// inject the test function in the delegate
			app.locator.getService(ServiceName.SUBJECTDELEGATE).isSubjectInUse = function(subjectId, successCallback, errorCallback) {
				successCallback(true);
			};
			
			// holds the task manager
			var manager = new diary.service.TimetableManager(),
				successCalled = false;
			
			// the success function
			function success(wasDeleted) {
				expect(wasDeleted).toBeFalsy();
				successCalled = true;
			}
			
			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			var subject = new diary.model.Subject({ 'id' : 1,
													'title' : "Test Subject",
													'colour' : "Red" });
			
			// delete a subject which is not used
			manager.deleteSubject(subject, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
	});
	
	describe("Update a subject" , function() {
		
		// holds the task delegate that is being used before testing
		var dummySubjectDelegate = (function() {
			
			return function() {
				
				this.subjectExistsWithTitle = function(title, successCallback, errorCallback) {
					successCallback(null);
				};
				
				this.updateSubject = function(subject, successCallback, errorCallback) {
					successCallback();
				};
				
			};
		
		})();
		
		// holds the timetable delegate that is being used before testing
		var dummyTimetableDelegate = (function() {
			
			return function() {
				
			};
				
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.TIMETABLEDELEGATE, dummyTimetableDelegate);
	        	   this.mapService(ServiceName.SUBJECTDELEGATE, dummySubjectDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should be able to update a subject", function() {
		   
			// holds the timetable manager
			var manager = new diary.service.TimetableManager(),
				successCalled = false;
			
			// the success function
			function success() {
				successCalled = true;
			}
			
			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			var subject = new diary.model.Subject({ 'id' : 1,
													'title' : "Test Subject",
													'colour' : "Red" });
			
			// update a subject
			manager.updateSubject(subject, success, error);
			expect(successCalled).toBeTruthy();
			
		});
		
	});
	
	describe("Getter methods" , function() {
		
		// holds the subject delegate that is being used before testing
		var dummySubjectDelegate = (function() {
			
			return function() {
				
				this.getSubjects = function(successCallback, errorCallback) {
					
					var subjects = new diary.collection.SubjectCollection();
					
					// add some subject dummy data
					subjects.add(new diary.model.Subject({ 'id' : 1,
														   'title' : "English",
														   'colour' : "White" }));
					
					subjects.add(new diary.model.Subject({ 'id' : 2,
						   								   'title' : "Math",
						   								   'colour' : "Blue" }));
					
					successCallback(subjects);
					
				};
				
			};
		
		})();
		
		// holds the task delegate that is being used before testing
		var dummyTimetableDelegate = (function() {
			
			return function() {
				
				this.getCurrentTimetable = function(day, successCallback, errorCallback) {
					
					var periods = new diary.collection.PeriodCollection();
					periods.add(new diary.model.Period({ 'startDate' : new Date() }));
					periods.add(new diary.model.Period({ 'startDate' : new Date() }));
					
					// return a timetable
					successCallback(new diary.model.Timetable({
						'startDay' : new Rocketboots.date.Day(),
						'periods' : periods,
						'cycleLength' : 10
					}));
					
				};
				
				this.getTimetables = function(startDay, endDay, successCallback, errorCallback) {
					
					// create the timetable collection that is to be returned
					var timetables = new diary.collection.TimetableCollection();
					
					// create the test periods
					var periods = new diary.collection.PeriodCollection();
					periods.add(new diary.model.Period({ 'startDate' : new Date() }));
					periods.add(new diary.model.Period({ 'startDate' : new Date() }));
					
					timetables.add(new diary.model.Timetable({ 'startDay' : startDay,
															   'periods' : periods,
															   'cycleLength' : 10
					}));
					
					timetables.add(new diary.model.Timetable({ 'startDay' : startDay,
															   'periods' : periods,
															   'cycleLength' : 5
					}));
					
					// return a timetable
					successCallback(timetables);
					
				};
				
			};
				
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.TIMETABLEDELEGATE, dummyTimetableDelegate);
	        	   this.mapService(ServiceName.SUBJECTDELEGATE, dummySubjectDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should be able to return subjects", function() {
			   
			// holds the timetable manager
			var manager = new diary.service.TimetableManager(),
				successCall = false;
			
			// the success function
			function success(subjects) {
				// the manager should have returned a collection of tasks
				expect(subjects instanceof diary.collection.SubjectCollection).toBeTruthy();
				expect(subjects.length).toEqual(2);
				
				// get the first subject
				var subject = subjects.get(1);
				
				// the subject that was returned should be the same as the dummy data			
				expect(subject instanceof diary.model.Subject).toBeTruthy();
				expect(subject.id).toEqual(1);
				expect(subject.get("title")).toEqual("English");
				expect(subject.get("colour")).toEqual("White");
				
				// get the second subject
				subject = subjects.get(2);
				
				// the subject that was returned should be the same as the dummy data			
				expect(subject instanceof diary.model.Subject).toBeTruthy();
				expect(subject.id).toEqual(2);
				expect(subject.get("title")).toEqual("Math");
				expect(subject.get("colour")).toEqual("Blue");
				successCall = true;
			}
			
			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// try to get the subject
			manager.getSubjects(success, error);
			expect(successCall).toBeTruthy();
			
		});
		
		it("should be able to return current timetable", function() {
		   
			// holds the task manager
			var manager = new diary.service.TimetableManager();

			var listener = {
				// the success function
				'success' : function (timetable) {
					// the manager should have returned a collection of tasks
					expect(timetable instanceof diary.model.Timetable).toBeTruthy();
					expect(timetable.get("periods") instanceof diary.collection.PeriodCollection).toBeTruthy();
					expect(timetable.get("periods").size()).toEqual(2);
				},

				// error function
				'error' : function (message) {
					throw "Test failed - no error should have been produced";
				}
			};

			spyOn(listener, "success");
			
			// try to get the overdue tasks
			manager.getCurrentTimetable(new Rocketboots.date.Day(), listener.success, listener.error);

			expect(listener.success).toHaveBeenCalled();
			
		});
		
		it("should be able to return timetables in period", function() {
			   
			// holds the start date
			var startDay = new Rocketboots.date.Day(2012, 0, 1);
			// holds the end date
			var endDay = new Rocketboots.date.Day(2012, 11, 31);
			
			// holds the task manager
			var manager = new diary.service.TimetableManager();

			var listener = {
				// the success function
				'success' : function (timetables) {
					// the manager should have returned a collection of tasks
					expect(timetables instanceof diary.collection.TimetableCollection).toBeTruthy();
					expect(timetables.length).toEqual(2);
				},

				// error function
				'error' : function (message) {
					throw "Test failed - no error should have been produced";
				}
			};

			spyOn(listener, "success");
			
			// try to get the calendar for the period
			manager.getTimetables(startDay, endDay, listener.success, listener.error);

			expect(listener.success).toHaveBeenCalled();
			
		});
		
	});
	
});