/**
 * A mock SQL Statement used for testing.
 * 
 * Usage example:
 * <code>
 * 	app = mock.appInitialize({
 * 		'sqlStatementResults' : [
 * 			{'number' : 1},
 * 			{'number' : 1}
 * 		]
 * 	});
 * </code>
 * 
 * author - John Pearce
 */

air = {};

mock = {};

mock.dummyApplicationDatabase = (function() {
	
	var service = function() {
		this.sqlStatement = new air.SQLStatement();
		console.log("Mock dummy database created", this);
	};
	
	
	service.prototype.runQuery = function(text, parameters, successCallback, errorCallback) {
		
		// return the dummy SQL Statement
		this.sqlStatement.queryText = text;
		successCallback(this.sqlStatement);
		
	};
	
	service.prototype.dropDatabase = function () {};
	
	return service;
	
})();

mock.appInitialize = function (appDefaults) {
	
	var mockApp = null;
	
	// reset air namespace
	air.SQLStatement = mock.SQLStatement;
	
	if (appDefaults && appDefaults.sqlStatementResults) {
		mock.SQLStatementResults = appDefaults.sqlStatementResults;
	}
	
	appDefaults = _.extend({}, {
			mapEvents: function() {
				// map events needed for the test
			},
			
			mapServices: function() {
				// map the services needed for the test
				this.mapService(ServiceName.APPDATABASE, mock.dummyApplicationDatabase);
			},
			
			registerTemplates: {
				// register templates, if needed
			}
		}, appDefaults);
	
	mockApp = new Rocketboots.Application(appDefaults);
	
	// init the application context
	mockApp.initialize();
	
	console.log("Mock application initialised: ", mockApp);
	
	return mockApp;
	
};

mock.SQLStatementResults = [];

mock.SQLStatement = (function () {
	
	/**
	 * @constructor
	 */
	var _statement = function (queryText) {
		
		this.queryText = queryText;
		this.getResultCallCount = 0;
		
		console.log("Mock SQL Statement created", this);
		
	};
	
	_statement.prototype.execute = function () {
		
	};
	
	_statement.prototype.getResult = function () {
		
		// default result
		var result = {
			'data': []
		};
		
		// use a global map to determine which hard-coded result to return
		if (this.getResultCallCount < mock.SQLStatementResults.length) {
			result.data = mock.SQLStatementResults[this.getResultCallCount];
		};
		
		++this.getResultCallCount;
		
		// logging
		console.log("Mock SQL query #" + this.getResultCallCount + ":\n", this.queryText, "\nResult data: ", result.data);
		
		return result;
		
	};
	
	return _statement;
	
})();
