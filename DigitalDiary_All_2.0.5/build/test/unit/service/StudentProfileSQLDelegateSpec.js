/**
 * Unit Tests for the Student Profile Delegate
 * 
 * author - Brian Bason
 */
describe("Student Profile SQL Delegate - Test Suite", function() {
	
	// holds the dummy app database layer that is required for testing
	var dummyApplicationDatabase = (function() {
		
		var service = function() {};
		
		service.prototype.runQuery = function(text, parameters, successCallback, errorCallback) {
			
			// return the dummy SQL Statement
			successCallback(new air.SQLStatement());
			
		};
		
		return service;
		
	})();
	
	beforeEach(function() {
		// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
		air = new Object();

		air.SQLStatement = function() {
		};
		
		air.SQLStatement.prototype.execute = function() {
		};
		
		// configure the application context for the unit test
		app = new Rocketboots.Application({
	           
           mapEvents: function() {
               // map events needed for the test
           },
           
           mapServices: function() {
        	   // map the services needed for the test
        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
           },
           
           registerTemplates: {
               // register templates, if needed
           }
	           
	    });
		
		// init the application context
		app.initialize();
		
	});
	
	describe("Get a student profile record" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : [{ 'id' : 1,
									'name' : "Test User",
									'dateOfBirth' : new Rocketboots.date.Day(1970, 1, 1).getKey(),
									'addressStreet' : "Test House, Test Street",
									'addressSuburb' : "Test Town",
									'addressState' : "NSW",
									'addressPostcode' : "2000",
									'telephone' : "(02) 4325 6789",
									'email' : "test@gmail.com",
									'grade' : "U1",
									'teacher' : null,
									'house' : null,
									'houseCaptain' : null,
									'houseColours' : null }] };
									
			};
			
		});
	
		it("should be able to return the student profile", function() {
		   
			// holds the student profile delegate that is being tested
			var delegate = new diary.service.StudentProfileSQLDelegate();
			
			// the success function
			function success(studentProfile) {
				// the delegate should have returned a student profile
				expect(studentProfile).not.toBeNull();
				expect(studentProfile instanceof diary.model.StudentProfile).toBeTruthy();
				
				// the profile that was returned should be the same as the dummy data			
				expect(studentProfile.id).toEqual(1);
				expect(studentProfile.get("name")).toEqual("Test User");
				expect(studentProfile.get("dateOfBirth").getKey()).toEqual(new Rocketboots.date.Day(1970, 1, 1).getKey());
				expect(studentProfile.get("address")).not.toBeNull();
				expect(studentProfile.get("address").get("street")).toEqual("Test House, Test Street");
				expect(studentProfile.get("address").get("suburb")).toEqual("Test Town");
				expect(studentProfile.get("address").get("state")).toEqual("NSW");
				expect(studentProfile.get("address").get("postcode")).toEqual("2000");
				expect(studentProfile.get("telephone")).toEqual("(02) 4325 6789");
				expect(studentProfile.get("email")).toEqual("test@gmail.com");
				expect(studentProfile.get("grade")).toEqual("U1");			
				expect(studentProfile.get("teacher")).toBeNull();
				expect(studentProfile.get("house")).toBeNull();
				expect(studentProfile.get("houseCaptain")).toBeNull();
				expect(studentProfile.get("houseColours")).toBeNull();
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// get the student profile from the delegate
			delegate.getStudentProfile(success, error);
			
		});
		
	});
	
	describe("Get no record of student profile" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : null };
				
			};
			
		});
	
		it("should be able to return a null for an nonexistent profile", function() {
		   
			// holds the student profile delegate that is being tested
			var delegate = new diary.service.StudentProfileSQLDelegate();
			
			// the success function
			function success(studentProfile) {
				// the delegate should have returned a NULL since no student profile
				// exists
				expect(studentProfile).toBeNull();
			}
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// get the student profile from the delegate
			delegate.getStudentProfile(success, error);
			
		});
		
	});
	
	describe("Failure while trying to get student profile" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummyApplicationDatabase = (function() {
			
			var service = function() {};
			
			service.prototype.runQuery = function(text, parameters, successCallback, errorCallback) {
				
				// simulate an error from the DB layer
				errorCallback();
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
			air = new Object();

			air.SQLStatement = function() {
			};
			
			air.SQLStatement.prototype.execute = function() {
			};
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
						
		});

	});
	
	describe("Update a student profile record" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy count data for the updated profile
				return {'data' : [{ 'count' : 1 }] };
									
			};
			
		});
	
		it("should be able to update the student profile", function() {
		   
			// holds the student profile delegate that is being tested
			var delegate = new diary.service.StudentProfileSQLDelegate();
			
			// create a dummy profile
			var studentProfile = new diary.model.StudentProfile({ 'name' : "Test User",
																  'dateOfBirth' : new Date(0).getTime(),
																  'address' : new diary.model.Address({ 'street' : "Test House, Test Street",
																	  								    'suburb' : "Test Town",
																	  								    'state' : "NSW",
																	  								    'postcode' : "2000" }),
																  'telephone' : "(02) 4325 6789",
																  'email' : "test@gmail.com",
																  'grade' : "U1",
																  'teacher' : null,
																  'house' : null,
																  'houseCaptain' : null,
																  'houseColours' : null
																});
			
			// the success function
			function success() {}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// get the student profile from the delegate
			delegate.updateStudentProfile(studentProfile, success, error);
			
		});
		
	});
		
});