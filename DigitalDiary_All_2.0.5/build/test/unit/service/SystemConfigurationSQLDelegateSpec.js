/**
 * Unit Tests for the System Configuration Delegate
 * 
 * author - Jane Sivieng
 */
describe("System Configuration SQL Delegate - Test Suite", function() {
	
	// holds the dummy app database layer that is required for testing
	var dummyApplicationDatabase = (function() {
		
		var service = function() {};
		
		service.prototype.runQuery = function(text, parameters, successCallback, errorCallback) {
			
			// return the dummy SQL Statement
			successCallback(new air.SQLStatement());
			
		};
		
		return service;
		
	})();
	
	beforeEach(function() {
		// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
		air = new Object();

		air.SQLStatement = function() {
		};
		
		air.SQLStatement.prototype.execute = function() {
		};
		
		// configure the application context for the unit test
		app = new Rocketboots.Application({
	           
           mapEvents: function() {
               // map events needed for the test
           },
           
           mapServices: function() {
        	   // map the services needed for the test
        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
           },
           
           registerTemplates: {
               // register templates, if needed
           }
	           
	    });
		
		// init the application context
		app.initialize();
		
	});
	
	describe("Get a system configuration record" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : [{ 'id' : 1,
									'token' : "TOKEN" }] };
									
			};
			
		});
	
		it("should be able to return the system configuration", function() {
		   
			// holds the system configuration delegate that is being tested
			var delegate = new diary.service.SystemConfigurationSQLDelegate();
			
			// the success function
			function success(systemConfiguration) {
				// the delegate should have returned a system configuration
				expect(systemConfiguration).not.toBeNull();
				expect(systemConfiguration instanceof diary.model.SystemConfiguration).toBeTruthy();
				
				// the profile that was returned should be the same as the dummy data			
				expect(systemConfiguration.id).toEqual(1);
				expect(systemConfiguration.get("token")).toEqual("TOKEN");
			}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// get the system configuration from the delegate
			delegate.getSystemConfiguration(success, error);
			
		});
		
	});
	
	describe("Get no record of system configuration" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy data for the task
				return {'data' : null };
				
			};
			
		});
	
		it("should be able to return a null for an nonexistent system configuration", function() {
		   
			// holds the system configuration delegate that is being tested
			var delegate = new diary.service.SystemConfigurationSQLDelegate();
			
			// the success function
			function success(systemConfiguration) {
				// the delegate should have returned a NULL since no system configuration
				// exists
				expect(systemConfiguration).toBeNull();
			}
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// get the system configuration from the delegate
			delegate.getSystemConfiguration(success, error);
			
		});
		
	});
	
	describe("Failure while trying to get system configuration" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummyApplicationDatabase = (function() {
			
			var service = function() {};
			
			service.prototype.runQuery = function(text, parameters, successCallback, errorCallback, errorMessage) {
				
				// simulate an error from the DB layer
				errorCallback(errorMessage);
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// Mock out the air.SQLConnection, air.SQLEvent, air.SQLErrorEvent and SQLStatement classes
			air = new Object();

			air.SQLStatement = function() {
			};
			
			air.SQLStatement.prototype.execute = function() {
			};
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.APPDATABASE, dummyApplicationDatabase);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
						
		});
	
		it("should be able to generate error for failure", function() {
		   
			// holds the system configuration delegate that is being tested
			var delegate = new diary.service.SystemConfigurationSQLDelegate();
			
			// the success function
			function success(systemConfiguration) {
				throw "Test failed - operation should not have been successful";
			}
			
			// the error function
			function error(message) {
				expect(message).toEqual("Error while trying to read system configuration");
			}
			
			// get the system configuration from the delegate
			delegate.getSystemConfiguration(success, error);
			
		});
		
	});
	
	describe("Update a system configuration record" , function() {
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// the dummy function for the unit test
			air.SQLStatement.prototype.getResult = function() {
				
				// return the dummy count data for the updated profile
				return {'data' : [{ 'count' : 1 }] };
									
			};
			
		});
	
		it("should be able to update the system configuration", function() {
		   
			// holds the system configuration delegate that is being tested
			var delegate = new diary.service.SystemConfigurationSQLDelegate();
			
			// create a dummy profile
			var systemConfiguration = new diary.model.SystemConfiguration({ 'token' : "TOKEN" });
			
			// the success function
			function success() {}
			
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
	
			// get the system configuration from the delegate
			delegate.updateSystemConfiguration(systemConfiguration, success, error);
			
		});
		
	});
		
});