/**
 * Unit Tests for the Student Manager
 * 
 * author - Brian Bason
 */
describe("Student Manager - Test Suite", function() {
	
	describe("Successful read of student profile" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummyStudentDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the student profile
			service.prototype.getStudentProfile = function(successCallback, errorCallback) {
				
				var studentProfile = new diary.model.StudentProfile({
					 								 'id' : 1,
					 								 'name' : "Test User",
					 								 'dateOfBirth' : new Rocketboots.date.Day(1970, 1, 1),
					 								 'address' : new diary.model.Address({
					 									 								'street' : "Test House, Test Street",
					 									 								'suburb' : "Test Town",
					 									 								'state' : "NSW",
					 									 								'postcode' : "2000"
					 								 								   }),
					 								 'telephone' : "(02) 4325 6789",
					 								 'email' : "test@gmail.com",
					 								 'grade' : "U1",
					 								 'teacher' : null,
					 								 'house' : null,
					 								 'houseCaptain' : null,
					 								 'houseColours' : null
													});
				
				successCallback(studentProfile);
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.STUDENTPROFILEDELEGATE, dummyStudentDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		
		it("should be able to return the student profile", function() {
		   
			// holds the student manager
			var manager = new diary.service.StudentManager();

			// the success function
			function success(studentProfile) {
			
				// the manager should have returned a student profile
				expect(studentProfile instanceof diary.model.StudentProfile).toBeTruthy();
				
				// the profile that was returned should be the same as the dummy data			
				expect(studentProfile.id).toEqual(1);
				expect(studentProfile.get("name")).toEqual("Test User");
				expect(studentProfile.get("dateOfBirth").getKey()).toEqual(new Rocketboots.date.Day(1970, 1, 1).getKey());
				expect(studentProfile.get("address")).not.toBeNull();
				expect(studentProfile.get("address").get("street")).toEqual("Test House, Test Street");
				expect(studentProfile.get("address").get("suburb")).toEqual("Test Town");
				expect(studentProfile.get("address").get("state")).toEqual("NSW");
				expect(studentProfile.get("address").get("postcode")).toEqual("2000");
				expect(studentProfile.get("telephone")).toEqual("(02) 4325 6789");
				expect(studentProfile.get("email")).toEqual("test@gmail.com");
				expect(studentProfile.get("grade")).toEqual("U1");			
				expect(studentProfile.get("teacher")).toBeNull();
				expect(studentProfile.get("house")).toBeNull();
				expect(studentProfile.get("houseCaptain")).toBeNull();
				expect(studentProfile.get("houseColours")).toBeNull();
			
			};
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// get the student profile from the manager
			manager.getStudentProfile(success, error);
		});
		
		it("should be able to populate the student profile", function() {
		   
			// holds the student profile that is to be updated
			var studentProfile = new diary.model.StudentProfile();
			
			// holds the student manager
			var manager = new diary.service.StudentManager();

			// the success function
			function success() {
				
				// the profile that was returned should be the same as the dummy data			
				expect(studentProfile.id).toEqual(1);
				expect(studentProfile.get("name")).toEqual("Test User");
				expect(studentProfile.get("dateOfBirth").getKey()).toEqual(new Rocketboots.date.Day(1970, 1, 1).getKey());
				expect(studentProfile.get("address")).not.toBeNull();
				expect(studentProfile.get("address").get("street")).toEqual("Test House, Test Street");
				expect(studentProfile.get("address").get("suburb")).toEqual("Test Town");
				expect(studentProfile.get("address").get("state")).toEqual("NSW");
				expect(studentProfile.get("address").get("postcode")).toEqual("2000");
				expect(studentProfile.get("telephone")).toEqual("(02) 4325 6789");
				expect(studentProfile.get("email")).toEqual("test@gmail.com");
				expect(studentProfile.get("grade")).toEqual("U1");			
				expect(studentProfile.get("teacher")).toBeNull();
				expect(studentProfile.get("house")).toBeNull();
				expect(studentProfile.get("houseCaptain")).toBeNull();
				expect(studentProfile.get("houseColours")).toBeNull();
				
			};
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}
			
			// get the student profile from the manager
			manager.getStudentProfile(success, error, studentProfile);
		});
		
	});

	describe("No student profile is found in data source" , function() {
			
		// holds the dummy app database layer that is required for testing
		var dummyStudentDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the student profile
			service.prototype.getStudentProfile = function(successCallback, errorCallback) {
				
				successCallback(null);
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.STUDENTPROFILEDELEGATE, dummyStudentDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should generate error if no student profile is found", function() {
		   
			// holds the student manager
			var manager = new diary.service.StudentManager();

			// success function
			function success() {
				throw "Test failed - Should have produced an error";
			}
			
			// error function
			function error(message) {
				expect(message).toEqual("No student profile was found, profile should be defined");
			}
			
			// get the student profile from the manager
			manager.getStudentProfile(success, error);
			
		});
		
	});
	
	describe("Error while trying to read student profile from data source" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummyStudentDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the school profile
			service.prototype.getStudentProfile = function(success, error) {
				
				error("Error while trying to get student profile");
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.STUDENTPROFILEDELEGATE, dummyStudentDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should generate error if an error occurs while trying to read student profile", function() {
		   
			// holds the student manager
			var manager = new diary.service.StudentManager();
			
			// the success function
			function success() {
				throw "Test failed - Should have produced an error";
			}
			
			// the error function
			function error(message) {
				expect(message).toEqual("Error while trying to get student profile");
			}
			
			// try to get the student profile
			manager.getStudentProfile(success, error);
			
		});
		
	});
	
	describe("Trying to set student profile into the data source" , function() {
		
		// holds the dummy app database layer that is required for testing
		var dummyStudentDelegate = (function() {
			
			var service = function() {};
			
			// the dummy service of the update student profile
			service.prototype.updateStudentProfile = function(studentProfile, success, error) {
				
				success();
				
			};
			
			return service;
			
		})();
		
		// setup the env for the the unit test
		beforeEach(function() {
			
			// configure the application context for the unit test
			app = new Rocketboots.Application({
		           
	           mapEvents: function() {
	               // map events needed for the test
	           },
	           
	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.STUDENTPROFILEDELEGATE, dummyStudentDelegate);
	           },
	           
	           registerTemplates: {
	               // register templates, if needed
	           }
		           
		    });
			
			// init the application context
			app.initialize();
			
		});
		
		it("should be able to update student profile", function() {
		   
			// holds the student manager
			var manager = new diary.service.StudentManager();
			
			// create a dummy profile
			var studentProfile = new diary.model.StudentProfile({ 'name' : "Test User",
																  'dateOfBirth' : new Date(0),
																  'address' : new diary.model.Address({ 'street' : "Test House, Test Street",
																	  								    'suburb' : "Test Town",
																	  								    'state' : "NSW",
																	  								    'postcode' : "2000" }),
																  'telephone' : "(02) 4325 6789",
																  'email' : "test@gmail.com",
																  'grade' : "U1",
																  'teacher' : null,
																  'house' : null,
																  'houseCaptain' : null,
																  'houseColours' : null
																});
			
			// the error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			var success = jasmine.createSpy();

			// try to get the student profile
			manager.setStudentProfile(studentProfile, success, error);

			expect(success).toHaveBeenCalled();
		});
		
	});
		
});