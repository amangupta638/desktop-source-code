/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: DiaryItemManagerSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Diary Item Manager", function() {

	var dummyAttachment = new diary.model.Attachment({
			'originalFilePath' : "test-source-dir/test-file.txt",
			'displayName' : "test-file.txt",
			'fileType' : "text",
			'localFileName' : "test-file.txt",
			'localFilePath' : "test-dir/test-file.txt",
			'dateAttached' : new Date()
		}),

		dummyDiaryItemDelegate = (function() {

			return function() {

				this.updateDiaryItem = function (diaryItem, successCallback, errorCallback) {
					successCallback();
				};

			};

		})(),

		dummyAttachmentFileDelegate = (function () {

			return function () {

				this.dummySuccess = true;

				this.upload = function (sourcePath, successCallback, errorCallback) {
					if (this.dummySuccess) {
						successCallback(dummyAttachment);
					} else {
						errorCallback();
					}
				};

				this.resolveLocalFilePaths = function (attachments) {
					return this.dummySuccess;
				};

				this.open = function (attachment) {
					return this.dummySuccess;
				};

				this.destroy = function (attachment, successCallback, errorCallback) {
					if (this.dummySuccess) {
						successCallback();
					} else {
						errorCallback();
					}
				};

			};

		})(),

		dummyAttachmentDBDelegate = (function () {

			return function () {

				this.dummySuccess = true;

				this.getAttachmentsForDiaryItems = function (diaryItems, successCallback, errorCallback) {
					if (this.dummySuccess) {
						successCallback(diaryItems);
					} else {
						errorCallback();
					}
				};

				this.addAttachmentToDiaryItem = function(attachment, diaryItemId, successCallback, errorCallback) {
					if (this.dummySuccess) {
						successCallback();
					} else {
						errorCallback();
					}
				};

				this.removeAttachment = function(attachment, successCallback, errorCallback) {
					if (this.dummySuccess) {
						successCallback();
					} else {
						errorCallback();
					}
				};

			};

		})();

	describe("Successful cases for deletion of a diary item with attachments Suite" , function() {

		// holds the diary item delegate that is being used before testing
		var dummyDiaryItemDelegate = (function() {

			return function() {

				this.deleteDiaryItem = function(diaryItemId, successCallback, errorCallback) {
					successCallback();
				};

			};

		})();

		// holds the attachment DB delegate that is being used before testing
		var dummyAttachmentDBDelegate = (function() {

			return function() {

				// dummy method for get attachments for diary item
				this.removeAttachment = function(attachment, successCallback, errorCallback) {
					if (successCallback) {
						successCallback();
					}
				};

			};

		})();

		// holds the attachment file delegate that is being used before testing
		var dummyAttachmentFileDelegate = (function() {

			return function() {
				// dummy method to destroy an attachment
				this.destroy = function(attachment, successCallback, errorCallback) {
					if (successCallback) {
						successCallback();
					}
				};
			};

		})();

		// setup the env for the the unit test
		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

	           mapEvents: function() {
	               // map events needed for the test
	           },

	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.DIARYITEMDELEGATE, dummyDiaryItemDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTFILEDELEGATE, dummyAttachmentFileDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTDBDELEGATE, dummyAttachmentDBDelegate);
	           },

	           registerTemplates: {
	               // register templates, if needed
	           }

		    });

			// init the application context
			app.initialize();

		});

		it("should be able to delete a diary item with attachments", function() {

			// holds the Diary Item manager
			var manager = new diary.service.DiaryItemManager(),
				successful = false;

			// the success function
			function success() {
				// the manager should return a success operation
				successful = true;
			}

			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// create a dummy diary item that is to be deleted
			var diaryItem = new diary.model.Event({	"id" : 1,
													"date" : new Rocketboots.date.Day().addDays(1).getDate(),
													"startTime" : null,
													"endTime" : null,
													"period" : 1,
													"title" : 'title',
													"locked" : false });

			var attachments = diaryItem.get('attachments');

			// fill in some attachments
			attachments.add(new diary.model.Attachment({ 'id' : 1,
												  		 'originalFilePath' : "Test Path",
												  		 'displayName' : "Attachment Name 1",
												  		 'fileType' : "pdf",
												  		 'localFileName' : "Test Path Local",
												  		 'localFilePath' : "/dev/files",
												  		 'dateAttached' : new Date() }));

			attachments.add(new diary.model.Attachment({ 'id' : 2,
		  		 										 'originalFilePath' : "Test Path",
												  		 'displayName' : "Attachment Name 2",
												  		 'fileType' : "pdf",
												  		 'localFileName' : "Test Path Local",
												  		 'localFilePath' : "/dev/files",
												  		 'dateAttached' : new Date() }));

			attachments.add(new diary.model.Attachment({ 'id' : 3,
												  		 'originalFilePath' : "Test Path",
												  		 'displayName' : "Attachment Name 3",
												  		 'fileType' : "pdf",
												  		 'localFileName' : "Test Path Local",
												  		 'localFilePath' : "/dev/files",
												  		 'dateAttached' : new Date() }));

			// try to delete the diary item
			manager.deleteDiaryItem(diaryItem, success, error);
			expect(successful).toBeTruthy();


		});

	});

	describe("Successful cases for deletion of a diary item without attachments Suite" , function() {

		// holds the diary item delegate that is being used before testing
		var dummyDiaryItemDelegate = (function() {

			return function() {

				this.deleteDiaryItem = function(diaryItemId, successCallback, errorCallback) {
					successCallback();
				};

			};

		})();

		// holds the attachment DB delegate that is being used before testing
		var dummyAttachmentDBDelegate = (function() {

			return function() {

				// dummy method for get attachments for diary item
				this.removeAttachment = function(attachment, successCallback, errorCallback) {
					if (successCallback) {
						successCallback();
					}
				};

			};

		})();

		// holds the attachment file delegate that is being used before testing
		var dummyAttachmentFileDelegate = (function() {

			return function() {
				// dummy method to destroy an attachment
				this.destroy = function(attachment, successCallback, errorCallback) {
					if (successCallback) {
						successCallback();
					}
				};
			};

		})();

		// setup the env for the the unit test
		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

	           mapEvents: function() {
	               // map events needed for the test
	           },

	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.DIARYITEMDELEGATE, dummyDiaryItemDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTFILEDELEGATE, dummyAttachmentFileDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTDBDELEGATE, dummyAttachmentDBDelegate);
	           },

	           registerTemplates: {
	               // register templates, if needed
	           }

		    });

			// init the application context
			app.initialize();

		});

		it("should be able to delete a diary item with no attachments", function() {

			// holds the Diary Item manager
			var manager = new diary.service.DiaryItemManager(),
				successful = false;

			// the success function
			function success() {
				// the manager should return a success operation
				successful = true;
			}

			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// create a dummy diary item that is to be deleted
			var diaryItem = new diary.model.Event({	"id" : 1,
													"date" : new Rocketboots.date.Day().addDays(1).getDate(),
													"startTime" : null,
													"endTime" : null,
													"period" : 1,
													"title" : 'title',
													"locked" : false });

			// try to delete the diary item
			manager.deleteDiaryItem(diaryItem, success, error);
			expect(successful).toBeTruthy();


		});

	});


	describe("Successful cases for creation of a diary item Suite" , function() {

		// holds the diary item delegate that is being used before testing
		var dummyDiaryItemDelegate = (function() {

			var noteEntryId = 0;

			return function() {

				this.createDiaryItem = function(diaryItem, successCallback, errorCallback) {
					successCallback(1);
				};

				this.appendNoteEntryToNote = function(noteId, noteEntry,  successCallback, errorCallback) {
					successCallback(++noteEntryId);
				};

			};

		})();

		// setup the env for the the unit test
		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

	           mapEvents: function() {
	               // map events needed for the test
	           },

	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.DIARYITEMDELEGATE, dummyDiaryItemDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTFILEDELEGATE, dummyAttachmentFileDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTDBDELEGATE, dummyAttachmentDBDelegate);
	           },

	           registerTemplates: {
	               // register templates, if needed
	           }

		    });

			// init the application context
			app.initialize();

		});

		it("should be able to create a diary item with attachments", function() {

			var attachmentId = 0;

			var dueDate = new Rocketboots.date.Day();
			dueDate.addDays(1);

			// holds the Diary Item manager
			var manager = new diary.service.DiaryItemManager(),

			// holds the diary item that is to be created
				diaryItem = new diary.model.Event({	"atDay" : new Rocketboots.date.Day(dueDate),
													"startTime" : null,
													"endTime" : null,
													"period" : 1,
													"title" : 'title',
													"locked" : false,
													"description" : 'description'});

			// holds the list of attachments that are to be linked to the diary item
				attachments = ["/dev/files/homework1.pdf","/dev/files/homework2.pdf","/dev/files/homework3.pdf"];

			// simulate the add attachment to diary item
			manager.addAttachmentToDiaryItem = function(pathToAttachment, diaryItemId, successCallback, errorCallback) {
				if (successCallback) {

					// create an attachment model
					var attachment = new diary.model.Attachment({ 'id' : ++attachmentId,
														  		  'originalFilePath' : pathToAttachment,
														  		  'displayName' : "Attachment Name 1",
														  		  'fileType' : "pdf",
														  		  'localFileName' : "Test Path Local",
														  		  'localFilePath' : "/dev/files",
														  		  'dateAttached' : new Date() });

					successCallback(attachment);
				}
			};

			// the success function
			function success(diaryItem) {
				expect(diaryItem instanceof diary.model.Event).toBeTruthy();
				expect(diaryItem.get("id")).toEqual(1);
				expect(diaryItem.get("atDay").getKey()).toEqual(dueDate.getKey());
				expect(diaryItem.get("startTime")).toBeNull();
				expect(diaryItem.get("endTime")).toBeNull();
				expect(diaryItem.get("period")).toEqual(1);
				expect(diaryItem.get("title")).toEqual("title");
				expect(diaryItem.get("locked")).toBeFalsy();
				expect(diaryItem.get("description")).toEqual("description");
				expect(diaryItem.get("attachments").length).toEqual(3);
			}

			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// try to create a diary item
			manager.createDiaryItem(diaryItem, attachments, success, error);

		});

		it("should be able to create a diary item without attachments", function() {

			var dueDate = new Rocketboots.date.Day();
			dueDate.addDays(1);

			// holds the Diary Item manager
			var manager = new diary.service.DiaryItemManager(),

			// holds the diary item that is to be created
				diaryItem = new diary.model.Event({	"atDay" : new Rocketboots.date.Day(dueDate),
													"startTime" : null,
													"endTime" : null,
													"period" : 1,
													"title" : 'title',
													"locked" : false,
													"description" : 'description' });

			// simulate the add attachment to diary item
			manager.addAttachmentToDiaryItem = function(pathToAttachment, diaryItemId, successCallback, errorCallback) {
				if (successCallback) {
					successCallback();
				}
			};

			// the success function
			function success(diaryItem) {
				expect(diaryItem instanceof diary.model.Event).toBeTruthy();
				expect(diaryItem.get("id")).toEqual(1);
				expect(diaryItem.get("atDay").getKey()).toEqual(dueDate.getKey());
				expect(diaryItem.get("startTime")).toBeNull();
				expect(diaryItem.get("endTime")).toBeNull();
				expect(diaryItem.get("period")).toEqual(1);
				expect(diaryItem.get("title")).toEqual("title");
				expect(diaryItem.get("locked")).toBeFalsy();
				expect(diaryItem.get("description")).toEqual("description");
				expect(diaryItem.get("attachments").length).toEqual(0);
			}

			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// try to create a diary item
			manager.createDiaryItem(diaryItem, undefined, success, error);

		});

		it("should be able to create a diary item with note entries", function() {

			var dueDate = new Rocketboots.date.Day();
			dueDate.addDays(1);

			// holds the Diary Item manager
			var manager = new diary.service.DiaryItemManager(),

			// holds the diary item that is to be created
				diaryItem = new diary.model.Note({ "atDay" : dueDate,
					 							   "title" : "Test Event" });

			diaryItem.get('entries').add(new diary.model.NoteEntry({ 'timestamp' : new Date(),
																	 'text' : 'text 1' }));

			diaryItem.get('entries').add(new diary.model.NoteEntry({ 'timestamp' : new Date(),
				 													 'text' : 'text 2' }));

			// simulate the add attachment to diary item
			manager.addAttachmentToDiaryItem = function(pathToAttachment, diaryItemId, successCallback, errorCallback) {
				if (successCallback) {
					successCallback();
				}
			};

			// the success function
			function success(diaryItem) {
				expect(diaryItem instanceof diary.model.Note).toBeTruthy();
				expect(diaryItem.get("id")).toEqual(1);
				expect(diaryItem.get("atDay")).toEqual(dueDate);
				expect(diaryItem.get("title")).toEqual("Test Event");
				expect(diaryItem.get("attachments").length).toEqual(0);
				expect(diaryItem.get("entries").length).toEqual(2);

				_.forEach(diaryItem.get("entries").toArray(), function(entry) {
					expect(diaryItem.get('id') == 1 || diaryItem.get('id') == 2).toBeTruthy();
				});
			}

			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// try to create a diary item
			manager.createDiaryItem(diaryItem, undefined, success, error);

		});

	});


	describe("Successful cases for retrival of diary items Suite" , function() {

		// holds the diary item delegate that is being used before testing
		var dummyDiaryItemDelegate = (function() {

			return function() {

				this.getDiaryItemsForDayRange = function(fromDate, toDate, successCallback, errorCallback) {

					// create a diary item collection
					var diaryItems = new diary.collection.DiaryItemCollection();

					// fill in some task
					diaryItems.add(new diary.model.Task({ "id" : 1,
														  "date" : new Date(),
                                                          "timeDue" : null,
														  "period" : 1,
														  "title" : "Overdue Task 1",
														  "type" : diary.model.DiaryItem.TYPE.HOMEWORK,
														  "subject" : new diary.model.Subject({'title': "U1 Math", 'colour': "Red" }) }));

					diaryItems.add(new diary.model.Task({ "id" : 2,
														  "date" : new Date(),
                                                          "timeDue" : new Rocketboots.date.Time(12,30),
														  "period" : 1,
														  "title" : "Overdue Task 2",
														  "type" : diary.model.DiaryItem.TYPE.ASSIGNMENT,
														  "subject" : new diary.model.Subject({'title': "U1 English", 'colour': "Blue" }) }));

					// an event
					diaryItems.add(new diary.model.Event({ "id" : 3,
						  								   "date" : new Date(),
						  								   "period" : 1,
						  								   "title" : "Event" }));

					// a message
					diaryItems.add(new diary.model.Message({ "id" : 4,
						"date" : new Date(),
						"period" : 1,
						"title" : "Test Message",
						"from" : diary.model.Message.FROM.TEACHER,
						"fromText" : "Mrs. Jones",
						"responseRequired" : true }));

					// return all the diary items
					successCallback(diaryItems);

				};

			};

		})();

		// setup the env for the the unit test
		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

	           mapEvents: function() {
	               // map events needed for the test
	           },

	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.DIARYITEMDELEGATE, dummyDiaryItemDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTFILEDELEGATE, dummyAttachmentFileDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTDBDELEGATE, dummyAttachmentDBDelegate);
	           },

	           registerTemplates: {
	               // register templates, if needed
	           }

		    });

			// init the application context
			app.initialize();

		});

		it("should be able to return all Diary Items for a Date Range", function() {

			// holds the Diary Item manager
			var manager = new diary.service.DiaryItemManager();

			// the success function
			function success(overdueTasks) {
				// the manager should have returned a collection of diary items
				expect(overdueTasks instanceof diary.collection.DiaryItemCollection).toBeTruthy();

				// the manager should have returned 4 diary from the dummy delegate
				expect(overdueTasks.size()).toBe(4);

				// check that the first two are Tasks
				expect(overdueTasks.get(1) instanceof diary.model.Task).toBeTruthy();
				expect(overdueTasks.get(2) instanceof diary.model.Task).toBeTruthy();

				// the third one is an Event
				expect(overdueTasks.get(3) instanceof diary.model.Event).toBeTruthy();

				// the last one is an Message
				expect(overdueTasks.get(4) instanceof diary.model.Message).toBeTruthy();
			}

			// error function
			function error(message) {
				throw "Test failed - no error should have been produced";
			}

			// try to get the diary items
			manager.getDiaryItemsForDayRange(new Date(), new Date(), success, error);

		});

	});

	describe("Diary item attachment management", function () {

		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

	           mapEvents: function() {
	               // map events needed for the test
	           },

	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.DIARYITEMDELEGATE, dummyDiaryItemDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTFILEDELEGATE, dummyAttachmentFileDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTDBDELEGATE, dummyAttachmentDBDelegate);
	           },

	           registerTemplates: {
	               // register templates, if needed
	           }

		    });

			// init the application context
			app.initialize();

		});

		it("should be able to attach a file to a diary item", function () {

			var manager = new diary.service.DiaryItemManager();

			manager.addAttachmentToDiaryItem("test path", 1, function () {}, function () {
				expect(false).toBeTruthy("Should not fail to add attachment");
			});

		});

		it("should fail to add attachment when filesystem delegate fails", function () {

			var manager = new diary.service.DiaryItemManager();
			app.locator.getService(ServiceName.ATTACHMENTFILEDELEGATE).dummySuccess = false;

			manager.addAttachmentToDiaryItem("test path", 1, function () {
				expect(false).toBeTruthy("Should fail to add attachment when filesystem delegate fails");
			}, function () {});

		});

		it("should fail to add attachment when DB delegate fails", function () {

			var manager = new diary.service.DiaryItemManager();
			app.locator.getService(ServiceName.ATTACHMENTDBDELEGATE).dummySuccess = false;

			manager.addAttachmentToDiaryItem("test path", 1, function () {
				expect(false).toBeTruthy("Should fail to add attachment when filesystem delegate fails");
			}, function () {});

		});

		it("should be able to remove an attachment from a diary item", function () {

			var manager = new diary.service.DiaryItemManager();

			manager.removeAttachmentFromDiaryItem(new diary.model.DiaryItem(), new diary.model.Attachment(), function () {}, function () {
				expect(false).toBeTruthy("Should not fail to remove attachment");
			});

		});

		it("should be fail to remove an attachment when DB delegate fails", function () {

			var manager = new diary.service.DiaryItemManager();
			app.locator.getService(ServiceName.ATTACHMENTDBDELEGATE).dummySuccess = false;

			manager.removeAttachmentFromDiaryItem(new diary.model.DiaryItem(), new diary.model.Attachment(), function () {
				expect(false).toBeTruthy("Should fail to remove attachment");
			}, function () {});

		});

		it("should be able to remove an attachment when filesystem delegate fails", function () {

			var manager = new diary.service.DiaryItemManager();
			app.locator.getService(ServiceName.ATTACHMENTFILEDELEGATE).dummySuccess = false;

			manager.removeAttachmentFromDiaryItem(new diary.model.DiaryItem(), new diary.model.Attachment(), function () {}, function () {
				expect(false).toBeTruthy("Should not fail to remove attachment");
			});

		});

		it("should be able to open an attachment on a diary item", function () {

			var manager = new diary.service.DiaryItemManager();

			expect(manager.openAttachmentOnDiaryItem(new diary.model.DiaryItem(), new diary.model.Attachment())).toBeTruthy("Should be able to open attachment");

		});

		it("should fail to open an attachment on a diary item when filesystem delegate fails", function () {

			var manager = new diary.service.DiaryItemManager();
			app.locator.getService(ServiceName.ATTACHMENTFILEDELEGATE).dummySuccess = false;

			expect(manager.openAttachmentOnDiaryItem(new diary.model.DiaryItem(), new diary.model.Attachment())).toBeFalsy("Should be not able to open attachment when filesystem delegate fails");

		});

	});

	describe("Diary item updates", function () {

		beforeEach(function() {

			// configure the application context for the unit test
			app = new Rocketboots.Application({

	           mapServices: function() {
	        	   // map the services needed for the test
	        	   this.mapService(ServiceName.DIARYITEMDELEGATE, dummyDiaryItemDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTFILEDELEGATE, dummyAttachmentFileDelegate);
	        	   this.mapService(ServiceName.ATTACHMENTDBDELEGATE, dummyAttachmentDBDelegate);
	           }

		    });

			// init the application context
			app.initialize();

		});

		it("should be able to update diary item and attachments", function () {

			var diaryItem = new diary.model.Event({
					"title": "Test event",
					"date": new Rocketboots.date.Day().getDate()
				}),
				manager = new diary.service.DiaryItemManager(),
				attachmentsToRemove = new diary.collection.AttachmentCollection();

			attachmentsToRemove.add(new diary.model.Attachment());

			expect(manager.updateDiaryItem(diaryItem, ["test-path/test-file.txt"], attachmentsToRemove, function (diaryItem, errorMessages) {
				// check response parameters
				expect(diaryItem instanceof diary.model.DiaryItem).toBeTruthy();
				if (errorMessages) {
					expect(errorMessages.length).toEqual(0, "Errors");
				} else {
					expect(errorMessages).toBeNull("Errors");
				}

			}, function (errorMessage) {
				// fail
				expect(false).toBeTruthy(errorMessage);
			}));

		});

	});

});
