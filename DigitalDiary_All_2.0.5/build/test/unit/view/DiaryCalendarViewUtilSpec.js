/**
 * (c) 2013 Product Dynamics All Rights Reserved
 * $Id: DiaryCalendarViewUtilSpec.js 10857 2013-03-01 00:48:01Z justin.judd $
 */

describe("Diary Calendar View Utility", function() {

	// TODO: add all combinations of items
	describe("Diary Item and Calendar Day mappings", function() {

		var calendar = null,
			diaryItems = null;

		beforeEach(function() {

			var day = new Rocketboots.date.Day(new Date(2012, 0, 1)),
				day2 = new Rocketboots.date.Day(new Date(2012, 0, 2)),
				maths = new diary.model.Subject({'title': "Maths", 'colour': "Blue"}),
				english = new diary.model.Subject({'title': "English", 'colour': "Red"}),

				timetable = new diary.model.Timetable({
					'startDay': new Rocketboots.date.Day(2012, 0, 1),
					'cycleLength': 5
				}),

				periodObjects = [
					{
						"id": 1,
						"startTime" : new Rocketboots.date.Time(9),
						"endTime" : new Rocketboots.date.Time(10),
						"title" : "Period 1",
						"cycleDay" : null
					}, {
						"id": 2,
						"startTime" : new Rocketboots.date.Time(10),
						"endTime" : new Rocketboots.date.Time(11),
						"title" : "Period 2",
						"cycleDay" : null
					}
				],

				classObjects = [
					{
						'periodId': 1,
						'subject': maths,
						'cycleDay': 1
					}, {
						'periodId': 2,
						'subject': english,
						'cycleDay': 1
					},
					{
						'periodId': 1,
						'subject': english,
						'cycleDay': 2
					}, {
						'periodId': 2,
						'subject': maths,
						'cycleDay': 2
					}
				],

				taskObjects = [
					// Day 1, Period 1
					{
						"id" : 11,
						"type" : diary.model.DiaryItem.TYPE.ASSIGNMENT,
						"title" : "Assignment 1",
						"assignedDay" : new Rocketboots.date.Day(day).addDays(-1),
						"assignedTime" : new Rocketboots.date.Time(10),
						"dueDay" : new Rocketboots.date.Day(day),
						"dueTime" : new Rocketboots.date.Time(9, 30),
						"subject" : english
					}, {
						"id" : 12,
						"type" : diary.model.DiaryItem.TYPE.HOMEWORK,
						"title" : "Homework 1",
						"assignedDay" : new Rocketboots.date.Day(day).addDays(-1),
						"assignedTime" : new Rocketboots.date.Time(10),
						"dueDay" : new Rocketboots.date.Day(day),
						"dueTime" : new Rocketboots.date.Time(9),
						"subject" : maths
					}, {
						"id" : 13,
						"type" : diary.model.DiaryItem.TYPE.HOMEWORK,
						"title" : "Homework 2",
						"assignedDay" : new Rocketboots.date.Day(day).addDays(-1),
						"assignedTime" : new Rocketboots.date.Time(10, 0),
						"dueDay" : new Rocketboots.date.Day(day),
						"dueTime" : new Rocketboots.date.Time(9, 30),
						"subject" : maths
					}, {
						"id" : 14,
						"type" : diary.model.DiaryItem.TYPE.HOMEWORK,
						"title" : "Homework 3",
						"assignedDay" : new Rocketboots.date.Day(day).addDays(-1),
						"assignedTime" : new Rocketboots.date.Time(10, 0),
						"dueDay" : new Rocketboots.date.Day(day),
						"dueTime" : new Rocketboots.date.Time(9, 30),
						"subject" : maths
					},
					// Day 1, Period 2
					{
						"id" : 15,
						"type" : diary.model.DiaryItem.TYPE.ASSIGNMENT,
						"title" : "Assignment 2",
						"assignedDay" : new Rocketboots.date.Day(day).addDays(-1),
						"assignedTime" : new Rocketboots.date.Time(10, 0),
						"dueDay" : new Rocketboots.date.Day(day),
						"dueTime" : new Rocketboots.date.Time(10, 0),
						"subject" : english
					}
				], eventObjects = [
					// Day 1, Period 1
					{
						"id" : 51,
						"title" : "Test Event",
						"startDay" : new Rocketboots.date.Day(day),
						"startTime" : null,
						"endTime" : null
					},
					// Day 2, Period 1
					{
						"id" : 52,
						"title" : "Test Event 2",
						"startDay" :  new Rocketboots.date.Day(day2),
						"startTime" : new Rocketboots.date.Time(9, 0),
						"endTime" : new Rocketboots.date.Time(10, 0)
					}
				], noteObjects = [
					// Day 1, Period 1
					{
						"id" : 21,
						"title" : "Test Note",
						"atDay" : new Rocketboots.date.Day(day),
						"atTime" : new Rocketboots.date.Time(9, 30),
						"entries" : new diary.collection.NoteEntryCollection()
					}
				];

			// Add note entry to note
			noteObjects[0].entries.add(new diary.model.NoteEntry({'timestamp' : new Date() , 'text' : "Hello world"}));

			// Add periods to timetable
			_.forEach(periodObjects, function (periodObject) {
				timetable.get('periods').add(new diary.model.Period(periodObject));
			});

			// Add classes to timetable
			_.forEach(classObjects, function (classObject) {
				timetable.get('classes').add(new diary.model.Class(classObject));
			});

			// Create calendar
			calendar = new diary.model.Calendar({ 'year' : 2012 });

			// Fill calendar
			for (var dayDate = new Rocketboots.date.Day(2012, 0, 1) ; dayDate.getFullYear() == 2012 ; dayDate.addDays(1)) {
				calendar.add(new diary.model.CalendarDay({
					'day' : new Rocketboots.date.Day(dayDate),
					'cycleDay' : ((dayDate.getTime() - (new Date(2012, 0, 1)).getTime()) / 3600000 / 24) % timetable.get('cycleLength') + 1,
					'timetable' : timetable
				}));
			}

			// Create diary items
			diaryItems = new diary.collection.DiaryItemCollection();
			_.forEach(taskObjects, function (taskObject) {
				diaryItems.add(new diary.model.Task(taskObject));
			});
			_.forEach(eventObjects, function (eventObject) {
				diaryItems.add(new diary.model.Event(eventObject));
			});
			_.forEach(noteObjects, function (noteObject) {
				diaryItems.add(new diary.model.Note(noteObject));
			});

		});

		/**
		 * No-crash tests
		 */
		it("Map periods for days, without crashes", function () {
			diary.view.DiaryCalendarViewUtil.mapPeriodsInDay(diaryItems, calendar.getDay(1, 1));
			diary.view.DiaryCalendarViewUtil.mapPeriodsInDay(diaryItems, calendar.getDay(2, 25));
			// TODO: fix error
			// diary.view.DiaryCalendarViewUtil.mapPeriodsInDay(diaryItems, calendar.getDay(12, 31));
		});

		/**
		 * Test handling day where no timetable specified
		 */
		it("Map period for day without timetable specified", function () {
			var calendarDay = calendar.getDay(1, 1),
				mappedDay = null;

			// remove timetable
			calendarDay.set({
				'timetable': null
			});

			// map day
			mappedDay = diary.view.DiaryCalendarViewUtil.mapPeriodsInDay(diaryItems, calendarDay);

			// check results
			expect(mappedDay).not.toBeNull();
			expect(mappedDay.periods.length).toEqual(8); // should be 8 pseudo periods
		});

		/**
		 * Test correct data construction
		 */
		it("Map periods for day 1 Jan (cycle day 1), with all correct data", function () {
			var day = diary.view.DiaryCalendarViewUtil.mapPeriodsInDay(diaryItems, calendar.getDay(1, 1), 100);
			var dayKey = calendar.getDay(1,1).get('day').getKey();

			// basics
			expect(day.cycleday).toEqual(1, "Cycle day");

			// periods
			expect(day.periods.length).toEqual(4, "Number of periods");

			// diary items (all-day)
			expect(day.hasEvents).toBeTruthy("has events");
			expect(day.hasItems).toBeFalsy("has items");
			expect(day.events.length).toEqual(1, "Non-period event count");
			expect(day.events[0].title).toEqual("Test Event", "Non-period event title");
			expect(day.items.length).toEqual(0, "Non-period items count");

			// period 1
			var period1 = day.periods[1];

			expect(period1.periodTitle).toEqual("Period 1");
			expect(period1.subjectName).toEqual("Maths");
			expect(period1.subjectColour).toEqual("Blue");
			expect(period1.hasEvents).toBeFalsy("Period 1 has no Events");
			expect(period1.hasItems).toBeTruthy("Period 1 has Items");
			expect(period1.events.length).toEqual(0, "Period 1 Event count");
			expect(period1.items.length).toEqual(5, "Period 1 Item count");
			// first sort by time
			expect(period1.items[0].title).toEqual("Homework 1");
			// next sort by title
			expect(period1.items[1].title).toEqual("Assignment 1");
			expect(period1.items[2].title).toEqual("Homework 2");
			expect(period1.items[3].title).toEqual("Homework 3");
			expect(period1.items[4].title).toEqual("Test Note");
			expect(period1.items[4].noteEntries.length).toEqual(1, "Period 1 note entry count");
			expect(period1.items[4].noteEntries[0].text).toEqual("Hello world", "Period 1 first note entry text body");
			expect(period1.dayKey).toEqual(dayKey);

			// period 2
			var period2 = day.periods[2];
			expect(period2.periodTitle).toEqual("Period 2");
			expect(period2.subjectName).toEqual("English");
			expect(period2.subjectColour).toEqual("Red");
			expect(period2.hasEvents).toBeFalsy("Period 2 has no Events");
			expect(period2.hasItems).toBeTruthy("Period 2 has Items");
			expect(period2.events.length).toEqual(0, "Period 2 Event count");
			expect(period2.items.length).toEqual(1, "Period 2 Item count");
			expect(period2.items[0].title).toEqual("Assignment 2");
			expect(period2.dayKey).toEqual(dayKey);
		});

		it("Map periods for 2 Jan (cycle day 2), with all correct data", function () {
			var day = diary.view.DiaryCalendarViewUtil.mapPeriodsInDay(diaryItems, calendar.getDay(1, 2), 50, 40);
			var dayKey = calendar.getDay(1,2).get('day').getKey();

			// basics
			expect(day.cycleday).toEqual(2, "Cycle day");

			// periods
			expect(day.periods.length).toEqual(4, "Number of periods");

			// diary items (all-day)
			expect(day.hasEvents).toBeFalsy("has events");
			expect(day.hasItems).toBeFalsy("has items");
			expect(day.events.length).toEqual(0, "Non-period event count");
			expect(day.items.length).toEqual(0, "Non-period item count");

			// period 1
			var period1 = day.periods[1];
			expect(period1.periodTitle).toEqual("Period 1");
			expect(period1.subjectName).toEqual("English");
			expect(period1.subjectColour).toEqual("Red");
			expect(period1.hasEvents).toBeTruthy("Period one has Events");
			expect(period1.hasItems).toBeFalsy("Period one has no Items");
			expect(period1.events.length).toEqual(1, "Period 1 Event count");
			expect(period1.events[0].title).toEqual("Test Event 2");
			expect(period1.items.length).toEqual(0, "Period 1 Item count");
			expect(period1.dayKey).toEqual(dayKey);

			// period 2
			var period2 = day.periods[2];
			expect(period2.periodTitle).toEqual("Period 2");
			expect(period2.subjectName).toEqual("Maths");
			expect(period2.subjectColour).toEqual("Blue");
			expect(period2.hasEvents).toBeFalsy("Period 2 has no Events");
			expect(period2.hasItems).toBeFalsy("Period 2 has no Items");
			expect(period2.events.length).toEqual(0, "Period 2 Event count");
			expect(period2.items.length).toEqual(0, "Period 2 Item count");
			expect(period2.dayKey).toEqual(dayKey);
		});

	});

});