/**
 * Unit Tests for the View FileSystem Utility
 * 
 * @author John Pearce
 */
describe("View Filesystem Utility - Test Suite", function() {
	
	var testFileName = "About Xcode and iOS SDK",
		testFileExtension = "pdf",
		testFilePath = "/Developer/" + testFileName + "." + testFileExtension;
	
	beforeEach(function () {
		
		air = {
			'File': function (filePath) {
				return {
					'name' : testFileName + '.' + testFileExtension,
					'extension' : testFileExtension
				};
			}
		};
		
	});
	
	it("should be able to get a display name with or without extension", function () {
		
		expect(diary.view.air.ViewFilesystemUtil.displayNameForFilePath(testFilePath)).toEqual(testFileName + "." + testFileExtension);
		expect(diary.view.air.ViewFilesystemUtil.displayNameForFilePath(testFilePath, true)).toEqual(testFileName + "." + testFileExtension);
		expect(diary.view.air.ViewFilesystemUtil.displayNameForFilePath(testFilePath, false)).toEqual(testFileName);
		
	});
	
});
